module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      screens: {
        'portrait': { 'raw': '(orientation: portrait)' },
        'touch': {'raw': '(pointer: coarse)'}
      }
    }
  },
  plugins: []
}
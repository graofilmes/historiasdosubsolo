import staticAdapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/kit/vite';
import autoprefixer from 'autoprefixer';
import path from "path";

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: vitePreprocess({
    postcss: {
      plugins: [autoprefixer]
    }
  }),

	kit: {
      adapter: staticAdapter({
        pages: 'build',
        assets: 'build',
        fallback: 'index.html',
        precompress: false
      }),
      alias: {
        $data: path.resolve("./src/data"),
        $stores: path.resolve("./src/stores")
      },
      appDir: 'internal'
    },
};

export default config;

import home from './home.json'
import page1 from './1.json'
import page2 from './2.json'
import page3 from './3.json'
import page4 from './4.json'
import page5 from './5.json'
import page6 from './6.json'
import page7 from './7.json'
import page8 from './8.json'
import page9 from './9.json'
import page10 from './10.json'
import page11 from './11.json'
import page12 from './12.json'
import page13 from './13.json'
import page14 from './14.json'

export default {
  home,
  page1,
  page2,
  page3,
  page4,
  page5,
  page6,
  page7,
  page8,
  page9,
  page10,
  page11,
  page12,
  page13,
  page14
}
export const getGoogleURL = (SRC = '') => SRC.split('/')[5]
export const getVimeoID = (SRC = '') => SRC.split('/')[3]
export const getVimeoKey = (SRC = '') => SRC.split('/')[4]
export const timeToSeconds = (time = 0): string => {
  const hours = Math.floor(time / 3600);
  const minutes = Math.floor(time % 3600 / 60);
  const seconds = Math.floor(time % 3600 % 60);
  return `${hours > 0 ? hours + ':' : ''}${minutes >= 0 ? minutes < 10 ? '0' + minutes : minutes : 0}:${seconds >= 10 ? seconds : seconds >= 0 ? '0' + seconds : '00'}`
}


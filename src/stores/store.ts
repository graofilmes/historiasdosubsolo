import { browser } from '$app/environment'
import { writable } from 'svelte/store'

export const lastViewed = writable<number>(0)
export const viewedChapters = writable<number[]>(
                                browser && localStorage
                                  ? JSON.parse(localStorage.getItem('viewedChapters')) || []
                                  : []
                              )

viewedChapters.subscribe((value) => {
  if (browser && localStorage) localStorage.viewedChapters = JSON.stringify(value)
})
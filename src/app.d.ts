/// <reference types="@sveltejs/kit" />

// See https://kit.svelte.dev/docs/typescript
// for information about these interfaces
declare namespace App {
	interface Locals {}

	interface Platform {}

	interface Session {}

	interface Stuff {}
}

interface currentTime {
  duration: number
  percent: number
  seconds: number
}

interface subtitle {
  id: number
  start: number
  end: number
  text: string
}

interface documentsData {
  title: string
  txt: string
  bg: string
  links: linksData[]
}

interface linksData {
  type: string
  title: string
  assets: assets[]
}

interface extrasOptions {
  type: string
  title: string
  assets: assets[]
}

interface assets {
  name: string
  credit: string
  date: string
  src: string
  srt: string
  alt: string
}
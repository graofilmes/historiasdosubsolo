O ano era 1974, no Brasil ano de eleição e reafirmação da ditadura militar, que completava 10 anos no poder. Em Alagoas, Divaldo Suruagy é o escolhido pelo general Ernesto Geisel, presidente da República, para governar o Estado de Alagoas. Sem voto popular, Suruagy assume o poder, após ter seu nome homologado pelos deputados estaduais da ARENA, a Aliança Renovadora Nacional, em votação indireta, longe das urnas e da cidadania. 

Em Maceió, na restinga do Pontal da Barra, avançam as obras de implantação da fábrica de cloro-soda, o campo de salmoura e o terminal marítimo. Embora a operação da Salgema Indústrias Químicas S/A só tenha iniciado em 1977, acidentes na planta industrial já haviam sido registrados bem antes. O primeiro fato documentado pela imprensa alagoana ocorreu em setembro de 1976, quando explodiu um reservatório de salmoura, instalado na área de mineração. Houve o registro da morte de um operário. 

No dia 27 de outubro de 1977, quando a empresa começou a operar, houve o segundo acidente, notificado como “vazamento de cloro durante partida da fábrica com as duas casas de células”. Como consequência, quatro adultos e dez crianças compareceram ao ambulatório da Salgema, para serem atendidos. Dois dias depois desta ocorrência, registrou-se a “continuação do vazamento de cloro na casa de células”, fato também notificado pela imprensa. 

Segundo o jornalista Joaldo Gomes, nesta ocorrência as consequências levaram à "reclamação de Dona Cícera sobre os filhos, moradores do Pontal da Barra, e ainda 14 pessoas vitimadas”. Seis meses depois, em abril de 1980, virou notícia a “a explosão de cilindro de cloro transportado em caminhão, em Paulo Jacinto”. Pelo que foi apurado e noticiado, 50 pessoas foram socorridas na cidade naquele município, outras 28 pessoas receberam atendimento na antiga Unidade de Emergência de Maceió, hoje HGE.”

Já no dia 31 de março de 1982, foi noticiado no jornal Tribuna de Alagoas a explosão do reator de DCE, que trata do dicloroetano, composto químico organoclorado importante como intermediário na produção do monômero cloreto de vinila, o principal precursor para a produção do polímero PVC. É um líquido incolor com um odor semelhante ao clorofórmio. Trata-se de um produto inflamável e produz gases tóxicos e irritantes. O acidente gerou confusão e princípio de pânico entre os moradores do Pontal da Barra. 

Após a explosão, as chamas alcançaram cerca de 15 metros. A polícia cercou lugares estratégicos e o Corpo de Bombeiros foi chamado para debelar o fogo. “Não tive condições de voltar logo para casa, pois era fumaça preta e também não sabia o que estava acontecendo”, disse José Pedro de Lima, casado e pai de três filhos, morador do bairro do Trapiche da Barra, ao ser entrevistado pela reportagem da Tribuna de Alagoas. 

Menos de um mês depois, no dia 27 de abril daquele ano, a Gazeta de Alagoas publicou mais um acidente. Desta vez vazamento de gás cloro, resultado de um curto circuito na subestação que alimenta a bomba de lubrificação do compressor de cloro e da bomba d'água. Houve dez vítimas, segundo apurou o jornal.

Em agosto de 1984, ocorreu novo vazamento de cloro no sistema de compressão. Quatro pessoas receberam atendimento no ambulatório da Salgema. Em janeiro do ano seguinte ocorreu mais um vazamento de cloro na área de compressão. A Gazeta de Alagoas relatou casos de asfixia e irritação em diversas pessoas. O fato chegou a ser denunciado por um conselheiro, em reunião do Conselho Estadual de Proteção Ambiental de Alagoas, o Cepram.

Na madrugada do dia 26 de maio de 1995 houve um vazamento de dicloretano armazenado em um dos gigantescos tanques na planta industrial da Salgema Indústrias Químicas no Pontal da Barra. Em nota, quatro dias após o vazamento o IMA emite nota de esclarecimento: “O acidente foi gerado pela corrosão dos parafusos de aço inoxidável que fixavam uma porta de inspeção - flange do tanque 029-A, que armazena cerca de 12 mil toneladas de dicloretano”.

É importante salientar que o volume de produto informado, constante na nota, teve como fonte a própria empresa. Não havia estrutura oficial à época capaz de fiscalizar e checar tais dados. O episódio gerou severos protestos de parlamentares e do Movimento pela Vida, que denunciaram a gravidade da situação e o iminente  risco para a lagoa Mundaú e para a população. 

No ano seguinte, março de 1996, quase um ano após o acidente na planta da Salgema, outra grave ocorrência foi registrada. Vazaram 6 toneladas de dicloretano de uma tubulação intermediária entre a Salgema e a Companhia Petroquímica Camaçari (CPC), na cabeça da primeira ponte da rodovia Divaldo Suruagy. Dessa vez com efluentes perigosos vazados diretamente para o canal da Lagoa Mundaú. 

Até o ano de 1996 a Salgema teria provocado 23 acidentes, inclusive com mortes. Segundo levantamento efetuado pelo Sindicato dos Trabalhadores em Indústrias Químicas de Alagoas, o levantamento foi repercutido pelo jornal O Diário de 17 de abril de 1996. A contabilidade do Sindiquímica envolveu o Pólo Cloroquímico e a planta industrial da Salgema, na restinga do Pontal da Barra.

As declarações do Sindiquímica em relação à  Salgema, que virou Trikem e que se tornou Braskem, sempre foram observadas com especial atenção, pois boa parte da base da categoria trabalha na empresa. A recorrência destes acidentes também foram combustível para outras reações, ainda nos meados dos anos 70, quando o governo autorizou o início das obras da empresa em Maceió.

Nos anos seguintes ao da implantação, os protestos retornavam sempre que ocorria algum acidente.
Dez anos após, em 1985, as mobilizações voltaram a ganhar corpo após o anúncio da intenção de ampliação da capacidade operacional da empresa e da instalação do Pólo Cloroquímico em Marechal Deodoro.

O Movimento pela Vida foi fundado no início da década de 1980 pelo Sindicato dos Jornalistas Profissionais de Alagoas. Com coordenação ampliada, além do Sindicato, também faziam parte o Instituto dos Arquitetos do Brasil, seccional Alagoas; União Estadual dos Estudantes; Sociedade Alagoana de Defesa dos Direitos Humanos e Sociedade dos Engenheiros Agrônomos de Alagoas; O Movimento pela Vida torna-se voz em defesa do meio ambiente. 

A localização da Salgema Indústrias Químicas S/A e sua duplicação, a implantação do Pólo Cloroquímico próximo às lagoas e o Terminal De Granéis Químicos na restinga do Pontal da Barra se converteram rapidamente em pauta principal do movimento. 

Os jornais, em março de 1985, já consideravam como fato consumado a ampliação: “A Salgema Indústria Químicas de Alagoas vai duplicar sua produção de cloro, diocleratano, soda cáustica e hidrogênio, para atender à demanda do Polo Alcoolquímico do Estado, a ser inaugurado em meados do ano que vem. A produção de cloro passará para 440 mil toneladas/ano, diocletano para 300 mil toneladas, soda em torno de 450 mil toneladas e hidrogênio para 120 mil toneladas/ano”, divulgou o Jornal do Brasil de 31 de março de 1985.

Entretanto, o pedido de autorização não teria a tramitação tranquila desejada pela empresa. Após ser protocolado no governo estadual, cresceu a mobilização dos moradores dos bairros adjacentes e de ambientalistas contra a permissão. Preocupado com os possíveis arranhões políticos, o governador Divaldo Suruagy tratou rapidamente de coletivizar a decisão. Sua primeira iniciativa foi a de criar uma comissão para analisar o pedido. Divulgou que acataria a decisão tomada pelo coletivo, “seja ela qual for”. (Jornal do Brasil de 21 de abril de 1985). A comissão era formada por técnicos e representantes do PDS, PFL, PTB, PDT e PCB. O PMDB não aceitou o convite, mas alguns dos seus parlamentares participavam das reuniões.

Foram os parlamentares do PMDB, com o apoio do Sindicato dos Jornalistas e da Sociedade de Defesa dos Direitos Humanos, que adotaram as primeiras medidas concretas contra a duplicação e pleiteando a relocação da planta industrial. Um projeto de Lei com essa finalidade chegou a ser aprovado na Assembleia Legislativa, mas foi vetado pelo governador.

Como o governo de Alagoas não desejava desagradar nem os acionistas da Salgema, nem a população, anunciou, no dia 31 de maio de 1985, que realizaria em Maceió um plebiscito para saber se os maceioenses desejavam a duplicação da produção da Salgema. A consulta foi marcada para o dia 7 de julho.

A mobilização contra a duplicação ganhou, em 1985, o reforço expressivo do “Movimento pela Vida”, uma organização composta por cientistas, ecologistas e políticos. Foi essa entidade que no dia 20 de abril de 1985 decidiu que realizaria um ato público contra a Salgema no dia 7 de maio. Participavam também o Programa de Meio-Ambiente da Universidade Federal de Alagoas, representado pelo professor José Geraldo Marques; Instituto dos Arquitetos do Brasil; Sindicato dos Jornalistas; Sociedade Alagoana dos Direitos Humanos e dos DCEs da Ufal e do Cesmac. A passeata ocorreu somente no dia 17 de maio e contou com a presença de mil pessoas, segundo o Jornal do Brasil do dia seguinte.

Considerando o evento como esvaziado, o informante anotou que isso aconteceu porque horas antes o professor Evilasio Soriano, “coordenador do Polo Cloroquímico, da Salgema Indústrias Químicas S/A”, participou de um debate na televisão apresentando os argumentos positivos para a ampliação.

Na Câmara Municipal de Maceió, um grupo de vereadores do PMDB conseguiu fazer aprovar Projeto de Resolução, apresentado pelo vereador Edberto Ticianeli, criando Comissão Especial de Inquérito (CEI) para investigar os riscos reais para Maceió que a ampliação da planta industrial do Pontal da Barra trazia. Várias audiências foram realizadas, mas os interessados na apuração eram minoria e os resultados foram inexpressivos.

“A Salgema decidiu interromper as obras de terraplanagem, nas quais já tinha investido o equivalente a 10 milhões de dólares. A Bahia está interessada em desenvolver uma produção cloroquímica”, respondeu, ameaçando o Jornal do Brasil. Em meados de junho, surgiu a notícia de que o plebiscito seria adiado para o dia 7 de julho. O adiamento era justificado pela necessidade de mais tempo para o Tribunal Regional Eleitoral organizá-lo. Mas claramente o fator político falava mais alto. Diante de inúmeras pressões, incluindo a de que a instalação da empresa se daria na Bahia, o plebiscito foi cancelado ainda no final do mês de junho. 

Com as eleições municipais marcadas para 15 de novembro, Divaldo Suruagy conseguiu adiar a decisão até o início de dezembro, quando foi oficializada a vitória de Djalma Falcão (PMDB) sobre João Sampaio, da coligação PFL/PDS. Como existia a possibilidade do prefeito oposicionista interferir no processo, o governo do Estado autorizou a duplicação.

### Fontes:
1. Cavalcante, Joaldo. Salgema: do erro à tragédia - Maceió: Editora CESMAC, 2020.

2. Ticianeli, Edberto. Salgema e o movimento contra a ampliação de 1985. História de Alagoas,2019. Disponível em: {https://www.historiadealagoas.com.br/salgema-e-o-movimento-contra-a-ampliacao-de-1985.html}. Acesso e: 07/03/2022.


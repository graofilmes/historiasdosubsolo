﻿A firma Salgema Indústrias Químicas Ltda surgiu em 1966 com a participação da Euluz S/A e Euvaldo Luz. Ainda em 1966 a Union Carbide também declarou o interesse em participar do projeto. Entretanto, em 1968, quando a Sudene autorizou a sua participação, que era de 50%, esta empresa se retirou e entrou em seu lugar a DuPont, também autorizada pela Sudene.

Segundo o professor Abel Tenório Cavalcante, pesquisas realizadas em 1968 pelos técnicos da Petrobras, Álvaro A. Teixeira e Luiz A. R. Saldanha, “comprovaram para a área de Maceió, uma reserva de 0,5 bilhão de toneladas de sal e para a área do Aeroporto dos Palmares-Barra de Santo Antônio, uma reserva de 20 bilhões de toneladas“.
Em 1971, no Governo Garrastazu Médici, foi a vez do BNDE (a partir de 1982, BNDES) aderir ao projeto. Nesse período Euvaldo Luz detinha 45% das ações, o BNDE controlava outros 10% e a DuPont possuía 45%. Preparando a estatização da empresa, o governo, por meio do BNDE, duplicou o capital, que era de 70 milhões para 140 milhões de dólares.

O Grupo Euvaldo Luz não teve como acompanhar esse nível de investimento e se retirou, vendendo sua participação para o próprio BNDE. Estas ações depois foram repassadas para a Petroquisa em junho de 1975, que passou a dividir com a DuPont o controle da estatal Salgema Indústrias Químicas S/A.

A construção da fábrica de cloro-soda, o campo de salmoura e o terminal marítimo, em Maceió, tiveram início em 1974. A produção comercial só teve início em fevereiro de 1977 e a unidade de dicloretano, em 1979.

### Fontes:
1. CAVALCANTE, Joaldo. Salgema: do erro à tragédia - Maceió: Editora CESMAC, 2020.
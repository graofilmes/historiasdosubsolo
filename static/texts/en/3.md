﻿The company Salgema Indústrias Químicas Ltda was founded in 1966 with the participation of Euluz S/A and Euvaldo Luz. In 1966, Union Carbide also declared its interest in participating in the project. However, in 1968, when Sudene authorized its participation, which was 50%, this company withdrew and DuPont, also authorized by Sudene, took its place.

According to Professor Abel Tenório Cavalcante, research carried out in 1968 by Petrobras technicians, Álvaro A. Teixeira and Luiz A. R. Saldanha, “proved for the Maceió area a reserve of 0.5 billion tons of salt and for the Airport area dos Palmares-Barra de Santo Antônio, a reserve of 20 billion tons“.
In 1971, during the Garrastazu Médici government, it was the turn of the BNDE (from 1982 onwards, BNDES) to join the project. During this period Euvaldo Luz held 45% of the shares, BNDE controlled another 10% and DuPont held 45%. Preparing for the nationalization of the company, the government, through the BNDE, doubled the capital, which was from 70 million to 140 million dollars.

The Euvaldo Luz Group was unable to keep up with this level of investment and withdrew, selling its stake to the BNDE itself. These shares were later transferred to Petroquisa in June 1975, which began to share control of state-owned Salgema Indústrias Químicas S/A with DuPont.

The construction of the chlorine-soda plant, the brine field and the maritime terminal, in Maceió, began in 1974. Commercial production only started in February 1977 and the dichlorethane unit, in 1979.

### Sources:
1. CAVALCANTE, Joaldo. Salgema: do erro à tragédia - Maceió: Editora CESMAC, 2020.
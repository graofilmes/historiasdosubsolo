Between the shock and the confirmation of the cause, the specter of the massive collapse forced many families to move hastily, leaving a good part of their lives behind. The reason was only confirmed almost a year after the tremor reported by residents, through surveys carried out by the Geological Survey of Brazil (CPRM): the extraction of rock salt by the petrochemical company Braskem, installed in Alagoas for over 45 years. The exploration, according to the document endorsed by more than 50 researchers, was carried out in an inadequate way that destabilized underground cavities, causing the ground to sink and cracks.

Once the cause was confirmed, a real race against time was started, both on the part of public agencies and by the residents who had to look for another place to live, even if temporarily. The rental of real estate in nearby neighborhoods jumped in the amount charged due to sudden demand. On the other hand, the City Hall decided to suspend construction and development licensing processes and the value of real estate in the affected areas dropped dramatically. Works were paralyzed and shops closed due to lack of customers.

Essential services started the training phase for major catastrophes. All widely publicized by the Government of Alagoas and the City Hall of the capital, as part of the necessary measures to reassure the most affected population.

Data confirmed by the Municipal Health Department of Maceió, from a survey carried out in 2019, in partnership with the Ministry of Health with residents and former residents of the affected areas, show that more than 50% of respondents showed signs of severe mental suffering and impaired activities. daily. The research sample showed that, as a sad symbiosis between resident and property, both succumbed to the catastrophe and mourning began to be experienced frequently.

The questionnaires were applied by the Municipality of Maceió, through the Municipal Health Department, in partnership with the Ministry of Health, which sent a team to Alagoas. The surveys were guided by the World Health Organization (WHO) screening of symptoms of severe mental distress and functional impairment for disasters. The questionnaire evaluated socioeconomic data, population profile, health conditions, quality of life and habits, in addition to risk perception.

According to the Municipal Health Department, the data are dynamic, since many residents are leaving the affected areas with the help of the mining company identified as the cause of the problem, which launched a program of financial compensation and relocation of residents.

### Sources:
1. [Agência Tatu](https://www.agenciatatu.com.br/noticia/em-ruinas/)


The project to implement Salgema Indústrias Químicas S/A, in the restinga of Pontal da Barra, began in 1974, during the government of Afrânio Lages, who had been appointed to the position four years earlier by General Médici. It was during this period that the design of the chlor-alkali plant, the brine field and the maritime terminal advanced. The choice of the site for the construction of the industry took place during the government of Divaldo Suruagy and the coordinator of the implantation project was the engineer Beroaldo Maia Gomes.

In 1982, in a statement given to journalist and deputy Mendonça Neto, Beroaldo revealed part of the backstage of the negotiations regarding the choice of the Pontal da Barra restinga for the implementation of Salgema. Beroaldo revealed that the choice of site for Salgema went through a group of technicians from the USA, including the vice president of DuPont. “They thought that the only suitable place, possible at the time, would be where Salgema is installed today. It was there or nowhere else. I still suggested other areas, but it was not possible to dissuade them. They guaranteed that there would be no risks to the population,” Beroaldo reported.

Mendonça maintained his position in relation to Salgema until the term of state deputy won in the 1982 elections. In this legislature, he even staged, in the plenary of the Legislative Power, the use of a protective mask in case of a lethal gas leak, for example of chlorine. Later on, when he took over the Planning Department, he tried to speed up the approval of the duplication of chlorine production by the same company he had fought so hard. To do so, he resorted to maneuvers to run over the regiment of the State Environmental Protection Council, a fact recorded in the Tribuna de Alagoas, edition of July 19, 1987.

Who authorized?

According to the Federal Constitution, article 176, deposits, in mining or not, and other mineral resources constitute a distinct property of the soil. They belong to the Union and the concessionaire is guaranteed ownership of the mining product. In this way, it is up to the concessionaire to indemnify the owner in case the use of the subsoil causes damage to the surface layers.


According to the architect and urban planner Caroline Gonçalves, we can come to understand Braskem as an urban land owner after the agreements have been signed. After confirmation of the subsidence of the land, which took place without social participation, the property has been transferred to Braskem (formerly Salgema). After the payment is made, she becomes the owner of these lots.


Even at the time of its installation, in the 70's, it was the federal competence to allow the extraction activity in accordance with the Mining Code (1967). At the state level, there was an Executive Secretariat for Pollution Control in Alagoas under the command of Biologist and Environmentalist José Geraldo Marques, who opposed the installation of Salgema for mining activities, pointing out studies that cited the risk of subsidence.

In an interview, José Geraldo Marques states that he was surprised by the drilling already taking place: “One fine day, people were already talking about the implantation of a chlorine soda factory, but everything was still very undefined, until suddenly, DuPont comes here and says 'now I want'. When the shareholding control changes, I participate in a seminar that will define the Chlorochemical Pole at the Federal University of Alagoas and one of the company's representatives said to anyone who would listen, 'It's no use you kicking because it's easier for Maceió to leave Salgema than Salgema leave Maceió'. When we took over the secretariat, we made the decision 'Pontal da Barra NO!'. As there was no licensing system, there was no environmental inspection or institutionalized system for implementation. The license for any ‘thing’ had to go through an environmental agency. The only one that existed was the Executive Secretariat for Pollution Control. So we made the decision: if any request comes here, the secretariat does not grant the license, however, the fact is that no request ever arrived. We made this decision and exposed it publicly. One fine day we learned that the dunes were breaking down. I went there myself, but when I arrived it was already late, the company was already installed”.

It is important to point out that at that time the Licensing System was quite rudimentary. This definition would only occur in the early 1980s with the National Environmental Policy (PMNA) - Law No. 6938 of 1981 - Provides for the National Environmental Policy, its purposes and mechanisms of formulation and application. Thus, Salgema only obtained regularization and operating licenses in 1986, ten years after its installation.

Currently, the three spheres are responsible for allowing and monitoring the exploration activities of companies such as Braskem (formerly Salgema):

* National Mining Agency (ANM): Authorize, grant and permit mining in compliance with the Mining Code and Mining Regulatory Norms;

* Instituto do Meio Ambiente de Alagoas (IMA): Environmental Licensing, which deals with the operation of the salt wells and the brine pipeline that takes the raw material to the plant;

* Maceió City Hall: The authorization for the operation of mining activity in relation to its locational aspect, taking into account urban zoning and municipal laws.

It is important to point out that Salgema's industrial plant is located in an urban area, as well as extraction wells are located in urban areas.

### Sources:
1. Cavalcante, Joaldo. Salgema: do erro à tragédia - Maceió: Editora CESMAC, 2020.


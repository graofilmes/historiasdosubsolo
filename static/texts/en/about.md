# THE CASE

In March 2018, after heavy rains, an earthquake was felt in the Pinheiro neighborhood in Maceió - AL. In addition to the tremors, there were subsidence in the ground, cracks in buildings and cracks and craters in the streets. Soon the tragedy spread to the neighborhoods of Mutange, Bom Parto and Bebedouro and later to the Farol neighborhood.

After studies and analyses, with the direct involvement of 52 researchers, the Geological Survey of Brazil - CGB/CPRM concluded that the mineral extraction of rock salt, practiced by the petrochemical company Braskem, is responsible for the subsidence in the soil that resulted in damage to the city.

Rock salt extraction in Maceió, around Lagoa Mundaú, has been taking place since 1970. Today there are 35 extraction wells in the urban area, all of which have been deactivated following an agreement signed with the Public Ministry, in order to prevent the worsening of an already critical situation. In it, Braskem was obliged to acknowledge its responsibility. Approximately 60 thousand people had to leave their homes in 2020, in the midst of the Covid 19 pandemic, but the compensation calculation does not measure the proportion of the tragedy.

Ownership of the affected area, consisting of three kilometers of lagoon shoreline and 300 hectares of priceless urban area, is under the responsibility of Braskem itself. The public debate about what will be done continues without transparency and without broad popular participation.

Four years after the tragedy, there is still much to know, predict and solve. There is no consensus on the subject in its geotechnical aspects. There is a lack of transparency in the management process of the affected areas. Judicial settlements and compensation, despite being slowly implemented, are seen as insufficient to offset the moral and social impacts of the tragedy.

Braskem's image, however, has not been torn apart like the emotions of people from Alagoas. Even after the incident, the company leads in ESG awards and in the first quarter recorded a net profit of BRL 3.9 billion and sales revenue of BRL 26.7 billion.

If this continues, the result could be disastrous for the city.

# THE INTERACTIVE DOCUMENTARY

This interactive documentary offers a virtual environment where multimedia content is organized (texts, videos, audios, images, documents and external links) compiling the history and documents related to the Pinheiro/Braskem Case up to the year 2022. Divided into 14 didactic chapters , you can access the contents by scrolling through an immersive timeline or explore the archives directly in the document room.

Five interviewees alternate their speeches between the 14 chapters:

Abel Galindo Marques (Civil Engineer / Specialist in Geotechnics and Geology / UFAL)
Diego Rodrigues (Political scientist / Specialist in environmental impact assessment / UNIT)
Edberto Ticianeli (Historian)
Isadora Padilha (Architect / IDEAL)
José Geraldo Marques (Geologist / Environmentalist / former resident of the Pinheiro neighborhood)

The project was born from the meeting of a group of artists at the Pinheiro Hackathon, an event managed by Evelyn Gomes and mentored by Glauber Xavier. In it, together with Grão Filmes and Caranto Mídia, we worked and matured the idea that became this webdoc, which was then contemplated in the sebrae/fapeal public notice. The result constitutes the present interactive film, which aims to contribute to debates and research on what is the greatest socio-environmental tragedy in an urban area in the world to date. As a group and as individuals, our desire is for researchers, journalists and interested parties to understand the size of this tragedy which, in addition to the obvious damage to the environment and the urban structure, affected the emotions of around 60,000 people and will be marked as a scar in all state of Alagoas.

Our solidarity with all people directly and indirectly affected by this tragedy.


# DATASHEET

## Sponsorship:
- Sebrae
- Fapeal

## Coproduction:
- Caranto Average
- Grão Filmes
- LabHacker
- Healthy Subversives

## Project management:
- Evelyn Gomes

## Search:
- Gabriela Araujo
- Nina Magalhaes
- Octávio Lemos
- Rafhael Barbosa

## Direction and interactive script:
- Eduardo Liron
- Evelyn Gomes
- Glauber Xavier

## Direction and audiovisual script:
- Glauber Xavier

## Script Consulting:
- Octávio Lemos

## Schedule:
- Eduardo Liron

## UX:
- Glauber Xavier

## UX Assistance:
- Ulysses Ribas
- Evelyn Gomes

## Design:
- Ulysses Ribas

## Sound assembly and design:
- Glauber Xavier

## Narration:
- Valeria Nunes

## Photos:
- Gabriela Araújo
- Public Archive of Alagoas
- Josival Monteiro
- Glauber Xavier
- Renata Baracho
- Mayra Costa

## Executive production:
- William Caesar
- Octávio Lemos

## Audio-visual file:
- Caranto Media
- Nucleo Zero
- North Star
- Sambacaitá Productions
- Healthy Subversives
- Media vehicles:
- Armadillo Agency
- Metropolises
- Web Newspaper
- Alagoas Gazette
- The Backstage
- Extra Newspaper
- InvestNews BR
- Investigation Record Reporter
- Journal of Pernambuco
- SP Tribune
- The Journalist's Voice
- Online sheet
- Exam
- AL Tv
- G1
- G! AL
- Estadão Alagoas
- Brazil in fact
- TV Pajuçara
- Tab UOL
- Court of Justice of Alagoas
- Media Caete
- AP News
- Tribune Today
- Each minute
- Capital Letter


## Thanks:
- Valeria Nunes
- Abel Galindo Marques
- Diego Rodriguez
- Edberto Ticianeli
- Isadora Padilha
- Carlos Nealdo dos Santos
- Daphne Broom
- Fernando Coelho
- Dilma de Carvalho
- Rikartiany Cardoso
- Jose Fernando
- Jose Geraldo Marques
- Bianca Pyl
- Edgar Zanella Alvarenga
- Elena Volpato
- Fabrizio Zuardi
- Gabriel Fedel
- Mobi Yabiku
- Alice Jardim
- March Lorraine
- Renata Baracho
-Mayra Costa
- Ramon Santos

##
- This project is made in open source and can be accessed by [GitLab](https://gitlab.com/graofilmes/historiasdosubsolo).

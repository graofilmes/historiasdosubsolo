Faced with the complexity and the need to respond to the population, the Municipality of Maceió decreed an “Emergency Situation” in December 2018. In April 2019, the Geological Survey of Brazil (CPRM) released a report informing that the extraction of rock salt made by Braskem was the main cause for the appearance of cracks, fissures and tremors in the neighborhoods of Maceió.

“The caves created by Braskem's mining that are opening in the neighborhoods of Pinheiro, Mutange, Bom Parto and Bebedouro are larger than a football field. The geological problems in them have nothing to do with faults in plate tectonics. What happens is the result of mining without due technical care”. The statement is from one of the greatest geological researchers in Alagoas and a master at the Federal University of Alagoas (UFAL), Professor Abel Galindo Marques. The data were released at an International Geology Congress.

"It was barbaric to exploit rock salt in the urban area of ​​Maceió", said the researcher, stating that the square mines in collapse measure between 100 and 200 meters in length and "are larger than the dimension of a football field" ( FIFA's pitch for the field is 105 by 68 meters). The round mines have the same diameter footage, and the height varies between 80 and 150 meters.

By analyzing technical reports that coincide with the studies of Professor Abel Galindo, the Federal Court determined the definitive stoppage of mineral exploration in urban areas. Braskem confirms the end of exploration in the 35 mines and carries out the plugging work. The professor also disapproved of the technique of trying to contain the landslides by injecting water with strong pressure into the caves.

Braskem admits that it is currently working with this technique. Abel Galindo himself saw the design of filling with solid material. By his assessment, each mine will take more than a year to fill.

For the work to be completed in five years or so, you will need to hire several companies. He ruled out, however, that the problems are linked to the geological fault, as the company claimed two years ago. In demonstrating the thesis, he explained that tectonic plates are deep, below three thousand meters (3 km). The exploration took place at a depth that varies between 900 and 1,200 meters.

### Sources:
1. [GazetaWeb](https://www.gazetaweb.com/noticias/geral/bairros-afetados-por-mineracao-tem-cavernas-maiores-que-campo-de-futebol)

# O CASO

Em março de 2018, após fortes chuvas, um tremor de terra foi sentido no bairro do Pinheiro em Maceió - AL. Além dos tremores, houveram afundamentos do solo, rachaduras nos imóveis e fendas e crateras se abriram nas ruas. Logo a tragédia se estendeu pelos bairros do Mutange, Bom Parto e Bebedouro e mais tarde pelo bairro do Farol.

Após estudos e análises, com envolvimento direto de 52 pesquisadores, o Serviço Geológico do Brasil - CGB/CPRM concluiu que a extração mineral de sal-gema, praticada pela petroquímica Braskem, é responsável pelas subsidências no solo que resultaram nos danos à cidade.

A extração de sal-gema em Maceió, no entorno da Lagoa Mundaú, acontece desde 1970. Hoje existem 35 poços de extração em área urbana, todos desativados após acordo assinado com o Ministério Público, visando evitar a piora de uma situação já crítica. Nele, a Braskem foi obrigada a reconhecer sua responsabilidade. Aproximadamente 60 mil pessoas tiveram que deixar suas casas no ano de 2020, em plena pandemia da Covid 19, mas o cálculo indenizatório não dimensiona a proporção da tragédia.

A posse da área afetada, composta por três quilômetros de orla lagunar e 300 hectares de área urbana de valor inestimável, está sob responsabilidade da própria Braskem. O debate público sobre o que será feito segue sem transparência e sem ampla participação popular.

Quatro anos após a tragédia, ainda há muito o que se saber, prever e solucionar. Não há consenso sobre o assunto em seus aspectos geotécnicos. Falta transparência do processo de gestão das áreas afetadas. Os acordos judiciais e indenizações, apesar de estarem sendo lentamente executados, são vistos como insuficiente para compensar os impactos morais e sociais da tragédia.

A imagem da Braskem, contudo, não foi rachada como o emocional das pessoas alagoanas. Mesmo depois do ocorrido, a empresa lidera nos prêmios ESGs e no primeiro trimestre registra lucro líquido de R$ 3,9 bilhões e receita de vendas de R$ 26,7 bilhões.

Se continuar dessa forma, o resultado pode ser desastroso para a cidade.

# O DOCUMENTÁRIO INTERATIVO

Esse documentário interativo oferece um ambiente virtual onde se encontram organizados conteúdos multimídia (textos, vídeos, áudios, imagens, documentos e links externos) compilando a história e os documentos relativos ao Caso Pinheiro/Braskem até o ano de 2022. Dividido em 14 capítulos didáticos, você pode acessar os conteúdos percorrendo uma linha do tempo imersiva ou explorar os arquivos diretamente na sala de documentos.

Cinco entrevistados intercalam suas falas entre os 14 capítulos:

Abel Galindo Marques (Engenheiro civil / Esp. em Geotecnia e Geologia / UFAL)
Diego Rodrigues (Cientista político / Esp. em avaliação de impactos ambientais / UNIT)
Edberto Ticianeli (Historiador)
Isadora Padilha (Arquiteta / IDEAL)
José Geraldo Marques (Geólogo / Ambientalista / ex morador do bairro do Pinheiro)

O projeto nasceu do encontro de um grupo de artistas no Hackathon Pinheiro, evento gerido por Evelyn Gomes e sob mentoria de Glauber Xavier. Nele, juntos com a Grão Filmes e a Caranto Mídia, trabalhamos e maturamos a ideia que veio a se tornar esse webdoc, que em seguida foi contemplado no edital sebrae/fapeal. O resultado constitui no presente filme interativo, que tem por foco contribuir nos debates e pesquisas acerca dessa que é a maior tragédia socio-ambiental em área urbana do mundo até hoje. Como grupo e como pessoas, nosso desejo é que pesquisadores, jornalistas e interessados possam compreender o tamanho dessa tragédia que, além dos evidentes danos ao meio ambiente e à estrutura urbana, atingiu o emocional de cerca de 60.000 pessoas e ficará marcada como uma cicatriz em todo estado de Alagoas.

Nossa solidariedade a todas as pessoas afetadas direta e indiretamente por essa tragédia.


# FICHA TÉCNICA

## Patrocínio:
- Sebrae
- Fapeal

## Coprodução:
- Caranto Média
- Grão Filmes
- LabHacker
- Saudáveis Subversivos

## Gestão do projeto:
- Evelyn Gomes

## Pesquisa:
- Gabriela Araujo
- Nina Magalhães
- Octávio Lemos
- Rafhael Barbosa

## Direção e Roteiro interativo:
- Eduardo Liron
- Evelyn Gomes
- Glauber Xavier

## Direção e roteiro audiovisual:
- Glauber Xavier

## Consultoria de roteiro:
- Octávio Lemos

## Programação:
- Eduardo Liron

## UX:
- Glauber Xavier

## Assistência de UX:
- Ulysses Ribas
- Evelyn Gomes

## Design:
- Ulysses Ribas

## Montagem e desenho de som:
- Glauber Xavier

## Narração:
- Valéria Nunes

## Fotos:
- Gabriela Araújo
- Arquivo Público de Alagoas
- Josival Monteiro
- Glauber Xavier
- Renata Baracho
- Mayra Costa

## Produção Executiva:
- Guilherme César
- Octávio Lemos

## Arquivo audiovisual:
- Caranto Media
- Núcleo Zero
- Estrela do Norte
- Sambacaitá Produções
- Saudáveis Subversivos
- Veículos de mídias:
- Agência Tatu
- Metrópoles
- Gazeta Web
- Gazeta de Alagoas
- O Bastidor
- Jornal Extra
- InvestNews BR
- Repórter Record Investigação
- Diário de Pernambuco
- Tribuna de SP
- A Voz do Jornalista
- Folha Online
- Exame
- AL Tv
- G1
- G! AL
- Estadão Alagoas
- Brasil de Fato
- TV Pajuçara
- Tab UOL
- Tribunal de Justiça de Alagoas
- Mídia Caeté
- AP News
- Tribuna Hoje
- Cada Minuto
- Carta Capital


## Agradecimentos:
- Valéria Nunes
- Abel Galindo Marques
- Diego Rodrigues
- Edberto Ticianeli
- Isadora Padilha
- Carlos Nealdo dos Santos
- Daphne Besen
- Fernando Coelho
- Dilma de Carvalho
- Rikartiany Cardoso
- José Fernando
- José Geraldo Marques
- Bianca Pyl
- Edgar Zanella Alvarenga
- Elena Volpato
- Fabricio Zuardi
- Gabriel Fedel
- Mobi Yabiku
- Alice Jardim
- Marcia Lorene
- Renata Baracho
- Mayra Costa
- Ramon Santos

##
- Esse projeto é realizado em código aberto e pode ser acessado pelo [GitLab](https://gitlab.com/graofilmes/historiasdosubsolo).

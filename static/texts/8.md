Diante da complexidade e da necessidade de dar respostas à população, a Prefeitura de Maceió decretou “Situação de Emergência” em dezembro de 2018. Em abril de 2019, o Serviço Geológico do Brasil (CPRM) divulgou relatório informando que a extração de sal-gema feita pela Braskem foi a principal causa para o surgimento de rachaduras, fissuras e tremores nos bairros de Maceió.

“As cavernas criadas pela mineração da Braskem que estão se abrindo nos bairros do Pinheiro, Mutange, Bom Parto e Bebedouro têm dimensões maiores que as de um campo de futebol. Os problemas geológicos nelas não têm relação com falhas em placas tectônicas. O que ocorre é o resultado da mineração sem o devido cuidado técnico”. A afirmação é de um dos maiores pesquisadores geológicos de Alagoas e mestre da Universidade Federal de Alagoas (UFAL), professor Abel Galindo Marques. Os dados foram divulgados em um Congresso Internacional de Geologia. 
 
"Foi uma barbaridade praticada para explorar sal-gema na área urbana de Maceió", disse o pesquisador ao afirmar que as minas quadradas em desmoronamento medem entre 100 e 200 metros de extensão e "são maiores que a dimensão de um campo de futebol" (a metragem da Fifa para campo é de 105 por 68 metros). As minas redondas têm a mesma metragem de diâmetro, e a altura varia entre 80 a 150 metros.
 
Ao analisar relatórios técnicos que coincidem com os estudos do professor Abel Galindo, a Justiça Federal determinou a paralisação definitiva da exploração do mineral em áreas urbanas. A Braskem confirma o fim da exploração nas 35 minas e executa o trabalho de tamponamento. O professor também reprovou a técnica de tentar conter os desmoronamentos com a injeção de água com forte pressão nas cavernas.
 
A Braskem admite que trabalha com esta técnica no momento. O próprio Abel Galindo viu o projeto de preenchimento com material sólido. Pela avaliação dele, cada mina levará mais de um ano para ser preenchida.
 
Para o trabalho ser concluído em cinco anos ou pouco mais, será preciso contratar várias empresas. Ele descartou, porém, que os problemas estejam ligados à falha geológica, como alegava a empresa há dois anos. Ao demonstrar a tese, explicou que as placas tectônicas são profundas, abaixo de três mil metros (3 km). A exploração ocorria a uma profundidade que varia entre 900 e 1.200 metros.
 
### Fontes:
1. [GazetaWeb](https://www.gazetaweb.com/noticias/geral/bairros-afetados-por-mineracao-tem-cavernas-maiores-que-campo-de-futebol)

1
00:00:04,800 --> 00:00:06,240
Um dos piores crimes

2
00:00:06,240 --> 00:00:07,440
socioambientais

3
00:00:07,440 --> 00:00:09,440
da história do Brasil e do mundo

4
00:00:09,440 --> 00:00:10,560
foi transformado

5
00:00:10,560 --> 00:00:12,120
em capital imobiliário.

6
00:00:12,120 --> 00:00:13,960
Não apenas não houve punição

7
00:00:13,960 --> 00:00:16,000
até o momento, como a Braskem

8
00:00:16,000 --> 00:00:16,800
sai da história

9
00:00:16,800 --> 00:00:18,360
lucrando dezenas de bilhões

10
00:00:18,360 --> 00:00:19,560
de reais.

11
00:00:19,880 --> 00:00:22,160
O que ela executou com indenizações

12
00:00:22,160 --> 00:00:23,040
não se compara

13
00:00:23,040 --> 00:00:25,400
ao que realiza em lucro.

14
00:00:25,400 --> 00:00:26,600
O "fantasma"

15
00:00:26,600 --> 00:00:28,200
de uma possível falência

16
00:00:28,200 --> 00:00:29,640
assombra a todos

17
00:00:29,640 --> 00:00:31,080
e é usado como argumento

18
00:00:31,080 --> 00:00:32,560
que impossibilitaria a empresa

19
00:00:32,560 --> 00:00:35,080
de seguir pagando o que deve.

20
00:00:35,080 --> 00:00:37,240
A empresa alimenta-se de uma imagem

21
00:00:37,240 --> 00:00:38,240
de que é essencial

22
00:00:38,240 --> 00:00:39,680
à economia alagoana,

23
00:00:39,680 --> 00:00:42,120
mas isso não se comprova em dados.

24
00:00:42,120 --> 00:00:43,680
A mineradora e petroquímica,

25
00:00:43,680 --> 00:00:44,640
portanto,

26
00:00:44,640 --> 00:00:45,400
já instalada

27
00:00:45,400 --> 00:00:47,760
em área de grande valor ambiental,

28
00:00:47,760 --> 00:00:48,960
acaba de se apropriar

29
00:00:48,960 --> 00:00:51,080
de três quilômetros de orla

30
00:00:51,080 --> 00:00:53,400
e 300 hectares de área urbana

31
00:00:53,400 --> 00:00:54,800
em uma das melhores regiões

32
00:00:54,800 --> 00:00:55,560
de Maceió.

33
00:00:55,840 --> 00:00:56,960
E o debate público

34
00:00:56,960 --> 00:00:58,880
sobre o que será feito dessa área

35
00:00:59,080 --> 00:01:00,160
segue sendo feito

36
00:01:00,160 --> 00:01:01,400
sem transparência

37
00:01:01,400 --> 00:01:03,000
e ampla participação popular.

38
00:01:03,600 --> 00:01:05,760
Se continuar dessa forma,

39
00:01:05,760 --> 00:01:07,200
o resultado pode ser

40
00:01:07,200 --> 00:01:09,040
desastroso para a cidade.


1
00:00:01,751 --> 00:00:03,503
O local escolhido para instalação

2
00:00:03,503 --> 00:00:06,756
da Salgema Indústrias Químicas S.A.

3
00:00:06,756 --> 00:00:09,134
foi a restinga do Pontal da Barra

4
00:00:09,134 --> 00:00:10,885
próximo ao encontro das águas do mar

5
00:00:10,885 --> 00:00:12,679
com a Lagoa Mundaú

6
00:00:12,679 --> 00:00:16,391
na região sul da cidade de Maceió.

7
00:00:16,391 --> 00:00:17,934
A empresa não levou em conta

8
00:00:17,934 --> 00:00:19,519
a expansão urbana da capital,

9
00:00:19,686 --> 00:00:21,312
o ecossistema lagunar,

10
00:00:21,312 --> 00:00:22,689
o zoneamento industrial

11
00:00:22,689 --> 00:00:24,733
e o potencial turístico da região.


1
00:00:02,160 --> 00:00:03,920
The implementation project

two
00:00:03,920 --> 00:00:06,800
of Salgema Industrias Químicas S.A.

3
00:00:06,800 --> 00:00:08,800
in the restinga of Pontal da Barra,

4
00:00:08,800 --> 00:00:12,480
started in 1974

5
00:00:12,480 --> 00:00:15,080
during the Afrânio Lages government,

6
00:00:15,080 --> 00:00:16,480
who had been nominated

7
00:00:16,480 --> 00:00:17,480
four years ago

8
00:00:17,480 --> 00:00:20,000
for the post by General Medici

9
00:00:20,000 --> 00:00:21,080
and it was defined

10
00:00:21,080 --> 00:00:23,720
during the Divaldo Suruagy government,

11
00:00:23,720 --> 00:00:25,800
under the coordination of the engineer

12
00:00:25,800 --> 00:00:27,440
Beroaldo Maia Gomes.

13
00:00:29,480 --> 00:00:31,160
Interviews and reports

14
00:00:31,160 --> 00:00:32,920
reveal that the choice of location

15
00:00:32,920 --> 00:00:34,560
passed by a group of technicians

16
00:00:34,560 --> 00:00:36,920
from the United States,

17
00:00:36,920 --> 00:00:39,280
including the vice president of DuPont.

18
00:00:39,840 --> 00:00:42,320
They found the only suitable place

19
00:00:42,320 --> 00:00:44,800
would be where today Salgema is installed.

20
00:00:45,800 --> 00:00:48,360
"Was it there or nowhere else"

21
00:00:48,840 --> 00:00:49,920
 reported Beroaldo.


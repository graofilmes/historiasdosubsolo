1
00:00:01,751 --> 00:00:03,503
The location chosen for installation

two
00:00:03,503 --> 00:00:06,756
of Salgema Industrias Químicas S.A.

3
00:00:06,756 --> 00:00:09,134
was the restinga of Pontal da Barra

4
00:00:09,134 --> 00:00:10,885
close to the waters of the sea

5
00:00:10,885 --> 00:00:12,679
with Lagoa Mundaú

6
00:00:12,679 --> 00:00:16,391
in the southern region of the city of Maceió.

7
00:00:16,391 --> 00:00:17,934
The company did not take into account

8
00:00:17,934 --> 00:00:19,519
the urban expansion of the capital,

9
00:00:19,686 --> 00:00:21,312
the lagoon ecosystem,

10
00:00:21,312 --> 00:00:22,689
industrial zoning

11
00:00:22,689 --> 00:00:24,733
and the tourist potential of the region.


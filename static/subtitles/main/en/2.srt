1
00:00:03,920 --> 00:00:06,172
When the probes drilled into the ground

two
00:00:06,172 --> 00:00:08,508
in the mangrove areas of Lagoa Mundaú

3
00:00:08,508 --> 00:00:10,760
in 1941

4
00:00:10,760 --> 00:00:13,805
the objective of the National Petroleum Council

5
00:00:13,805 --> 00:00:16,433
it was oil prospecting.

6
00:00:16,433 --> 00:00:17,600
the contracted firm

7
00:00:17,600 --> 00:00:20,228
did not have success with oil,

8
00:00:20,228 --> 00:00:22,939
but found a bed of rock salt

9
00:00:22,939 --> 00:00:24,441
under the soil of Maceió.


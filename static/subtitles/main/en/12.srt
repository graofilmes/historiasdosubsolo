1
00:00:02,800 --> 00:00:04,880
By mid-2022,

two
00:00:05,360 --> 00:00:06,840
approximately 15 thousand

3
00:00:06,840 --> 00:00:09,240
homes were destroyed,

4
00:00:09,240 --> 00:00:11,000
an area of ​​300 hectares,

5
00:00:11,000 --> 00:00:13,720
almost 5 thousand entrepreneurs

6
00:00:14,000 --> 00:00:15,840
who, in addition to losing their income,

7
00:00:15,840 --> 00:00:17,360
were forced to resign

8
00:00:17,360 --> 00:00:19,920
more than 20 thousand workers.

9
00:00:19,920 --> 00:00:21,960
Approximately 60 thousand residents

10
00:00:21,960 --> 00:00:23,640
were expelled

11
00:00:23,640 --> 00:00:25,680
not knowing when, where

12
00:00:25,680 --> 00:00:28,040
and how they will restart their lives.

13
00:00:28,800 --> 00:00:30,560
The places that Maceioans

14
00:00:30,560 --> 00:00:31,480
already got used to it

15
00:00:31,480 --> 00:00:33,480
to call "ghost districts"

16
00:00:33,840 --> 00:00:35,600
are dystopian scenarios

17
00:00:35,600 --> 00:00:37,840
of accelerated degradation.

18
00:00:38,560 --> 00:00:40,680
of abandoned houses,

19
00:00:40,680 --> 00:00:42,400
the old residents took

20
00:00:42,400 --> 00:00:45,480
doors, tiles, windows, floors

21
00:00:45,680 --> 00:00:47,720
and everything that could have any value.

22
00:00:48,920 --> 00:00:51,080
Workers paid by Braskem

23
00:00:51,080 --> 00:00:51,880
passed then

24
00:00:51,880 --> 00:00:53,120
to seal with concrete

25
00:00:53,120 --> 00:00:53,880
the real estate

26
00:00:53,880 --> 00:00:55,960
that now belong to the company.

27
00:00:55,960 --> 00:00:57,040
All under a fort

28
00:00:57,040 --> 00:00:58,400
surveillance system

29
00:00:58,400 --> 00:00:59,720
and private security,

30
00:00:59,720 --> 00:01:01,280
24 hours a day.


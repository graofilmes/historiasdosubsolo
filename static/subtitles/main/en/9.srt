1
00:00:03,440 --> 00:00:04,720
confirmed data

two
00:00:04,720 --> 00:00:06,280
by the Municipal Secretary

3
00:00:06,280 --> 00:00:07,800
of Health of Maceio,

4
00:00:07,800 --> 00:00:09,480
from a survey carried out

5
00:00:09,480 --> 00:00:10,840
in 2019,

6
00:00:10,840 --> 00:00:12,600
partnership of the Ministry of Health

7
00:00:12,600 --> 00:00:13,520
with residents

8
00:00:13,520 --> 00:00:14,520
and former residents

9
00:00:14,520 --> 00:00:16,360
of the affected areas,

10
00:00:16,360 --> 00:00:17,400
show that more

11
00:00:17,400 --> 00:00:19,520
than 50% of respondents

12
00:00:20,000 --> 00:00:21,360
showed evidence

13
00:00:21,360 --> 00:00:23,320
of severe mental suffering

14
00:00:23,320 --> 00:00:24,400
and commitment

15
00:00:24,400 --> 00:00:26,080
 of daily activities.

16
00:00:29,520 --> 00:00:30,960
The research clipping

17
00:00:30,960 --> 00:00:32,360
showed that,

18
00:00:32,360 --> 00:00:34,240
like a sad symbiosis

19
00:00:34,240 --> 00:00:35,680
between resident and property,

20
00:00:36,240 --> 00:00:38,680
both succumbed to catastrophe

21
00:00:38,680 --> 00:00:39,880
and the mourning became

22
00:00:39,880 --> 00:00:41,840
frequently experienced.


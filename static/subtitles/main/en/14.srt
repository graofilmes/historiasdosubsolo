1
00:00:07,480 --> 00:00:08,400
there are still many

two
00:00:08,400 --> 00:00:10,680
unanswered questions

3
00:00:10,680 --> 00:00:12,640
when you think about the future of Maceió

4
00:00:12,640 --> 00:00:14,720
and neighborhoods directly affected

5
00:00:14,760 --> 00:00:17,640
for the Braskem tragedy.

6
00:00:17,640 --> 00:00:19,280
Some solution paths

7
00:00:19,280 --> 00:00:21,640
for part of the problems already exist

8
00:00:21,960 --> 00:00:24,000
and were pointed out:

9
00:00:24,000 --> 00:00:25,360
mine deactivation

10
00:00:25,360 --> 00:00:26,920
inside the city,

11
00:00:26,920 --> 00:00:28,720
filling technologies

12
00:00:28,720 --> 00:00:29,280
from soil

13
00:00:29,280 --> 00:00:30,240
of some of them

14
00:00:30,240 --> 00:00:31,440
and other engineering

15
00:00:31,440 --> 00:00:33,800
to reduce the impact and risks

16
00:00:34,080 --> 00:00:36,480
of new accidents.

17
00:00:36,480 --> 00:00:38,280
But the fact is that a lot

18
00:00:38,280 --> 00:00:40,320
it's still impossible to know,

19
00:00:40,320 --> 00:00:42,680
to predict and solve.

20
00:00:42,680 --> 00:00:44,480
Starting with instability

21
00:00:44,480 --> 00:00:46,760
soil in the affected areas.

22
00:00:46,760 --> 00:00:48,640
There is no consensus on the matter

23
00:00:48,640 --> 00:00:51,000
in its geotechnical aspects

24
00:00:51,000 --> 00:00:52,520
and precisely because of that

25
00:00:52,520 --> 00:00:54,960
there is an obvious and urgent demand

26
00:00:54,960 --> 00:00:58,200
for investments in research.

27
00:00:58,200 --> 00:00:59,480
On the other hand,

28
00:00:59,480 --> 00:01:01,480
the current shareholders of Braskem

29
00:01:01,840 --> 00:01:02,680
move

30
00:01:02,680 --> 00:01:04,880
to enforce legal agreements

31
00:01:05,320 --> 00:01:07,160
and indemnities

32
00:01:07,160 --> 00:01:08,640
while they prepare the sale

33
00:01:08,640 --> 00:01:09,360
from a company

34
00:01:09,360 --> 00:01:10,960
that continues to make a profit,

35
00:01:10,960 --> 00:01:13,480
but with an incalculable liability,

36
00:01:13,480 --> 00:01:14,840
which makes the operation difficult.


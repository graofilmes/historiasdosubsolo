1
00:00:04,760 --> 00:00:07,080
In 1996,

two
00:00:07,080 --> 00:00:09,160
with the change of administration,

3
00:00:09,160 --> 00:00:10,400
in the previous year,

4
00:00:10,400 --> 00:00:11,920
the petrochemical Salgema

5
00:00:11,920 --> 00:00:14,080
is renamed Trikem

6
00:00:14,080 --> 00:00:16,000
and works in an integrated way

7
00:00:16,000 --> 00:00:18,720
to the Petrochemical OPP.

8
00:00:18,720 --> 00:00:21,120
Even in 1996,

9
00:00:21,120 --> 00:00:22,680
Odebrecht joins

10
00:00:22,680 --> 00:00:23,880
to the Mariani Group

11
00:00:23,880 --> 00:00:26,440
to create the Proppet.

12
00:00:26,440 --> 00:00:28,520
In partnership with the Mariani group,

13
00:00:28,520 --> 00:00:31,560
Odebrecht acquires control of Copene

14
00:00:31,560 --> 00:00:34,400
Camaçari Petrochemical Center

15
00:00:34,400 --> 00:00:35,480
in Bahia,

16
00:00:35,480 --> 00:00:36,920
and from Polialden.

17
00:00:37,680 --> 00:00:38,720
In the same year,

18
00:00:38,720 --> 00:00:39,960
starts a process

19
00:00:39,960 --> 00:00:41,720
asset integration

20
00:00:41,720 --> 00:00:42,920
At first

21
00:00:42,920 --> 00:00:44,720
and second generations,

22
00:00:44,720 --> 00:00:47,920
unprecedented action in Brazil.

23
00:00:47,920 --> 00:00:49,040
In 2002,

24
00:00:49,040 --> 00:00:50,320
from the integration

25
00:00:50,320 --> 00:00:52,240
of six companies

26
00:00:52,240 --> 00:00:53,160
Copene,

27
00:00:53,160 --> 00:00:54,320
OPP,

28
00:00:54,320 --> 00:00:55,320
Trikem,

29
00:00:55,320 --> 00:00:56,680
nitrocarbon,

30
00:00:56,680 --> 00:00:57,880
proppet

31
00:00:57,880 --> 00:00:59,120
and Polialden,

32
00:00:59,120 --> 00:01:01,040
Braskem S.A. is born.

33
00:01:01,040 --> 00:01:02,120
creating in alagoas

34
00:01:02,120 --> 00:01:03,560
the Braskem Unit

35
00:01:03,560 --> 00:01:04,720
Chlorine Soda.


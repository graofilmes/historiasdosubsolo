1
00:00:07,160 --> 00:00:10,040
On March 3, 2018

two
00:00:10,040 --> 00:00:11,280
at 2:30 pm,

3
00:00:11,280 --> 00:00:12,920
residents of Maceio,

4
00:00:12,920 --> 00:00:14,200
capital of alagoas,

5
00:00:14,200 --> 00:00:15,720
feel earthquakes

6
00:00:15,720 --> 00:00:18,160
in some neighborhoods of the city.

7
00:00:18,160 --> 00:00:20,000
The epicenter of the quakes

8
00:00:20,000 --> 00:00:21,640
happened in Pinheiro,

9
00:00:21,640 --> 00:00:23,080
little more neighborhood

10
00:00:23,080 --> 00:00:24,440
of 19 thousand inhabitants

11
00:00:24,440 --> 00:00:25,320
located

12
00:00:25,320 --> 00:00:26,320
at 5km

13
00:00:26,320 --> 00:00:27,480
from the city centre.

14
00:00:28,200 --> 00:00:29,640
The phenomenon caused

15
00:00:29,640 --> 00:00:31,040
street sinkings

16
00:00:31,040 --> 00:00:32,440
and cracks in buildings,

17
00:00:32,440 --> 00:00:34,520
to the dismay of the residents.

18
00:00:35,400 --> 00:00:36,720
On the day of the event,

19
00:00:36,720 --> 00:00:37,440
teams

20
00:00:37,440 --> 00:00:38,760
from the Fire Department

21
00:00:38,760 --> 00:00:40,160
and Civil Defense

22
00:00:40,160 --> 00:00:41,360
went to the place

23
00:00:41,360 --> 00:00:42,960
to guide the population

24
00:00:42,960 --> 00:00:44,080
about procedures

25
00:00:44,080 --> 00:00:45,480
that should be adopted

26
00:00:45,480 --> 00:00:47,360
that moment.

27
00:00:47,760 --> 00:00:48,840
In that day,

28
00:00:48,840 --> 00:00:50,280
tremors were recorded

29
00:00:50,280 --> 00:00:51,840
by the Seismological Laboratory

30
00:00:51,840 --> 00:00:53,080
from the Federal University

31
00:00:53,080 --> 00:00:54,480
from Rio Grande do Norte,

32
00:00:54,480 --> 00:00:56,160
who accused the magnitude

33
00:00:56,160 --> 00:00:58,640
2.5 degrees on the Richter scale.


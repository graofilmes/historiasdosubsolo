1
00:00:02,040 --> 00:00:03,760
In December 2019

two
00:00:03,760 --> 00:00:05,000
the term was signed

3
00:00:05,000 --> 00:00:06,760
agreement for support

4
00:00:06,760 --> 00:00:09,120
in the evacuation of risk areas,

5
00:00:09,600 --> 00:00:11,680
which defined Braskem's autonomy

6
00:00:12,120 --> 00:00:13,120
for negotiation

7
00:00:13,120 --> 00:00:14,920
of indemnity values

8
00:00:14,920 --> 00:00:15,880
 directly

9
00:00:15,880 --> 00:00:17,760
with the people who own

10
00:00:17,760 --> 00:00:20,200
of the affected properties.

11
00:00:20,200 --> 00:00:22,080
The parties that signed the decision

12
00:00:22,080 --> 00:00:22,880
were:

13
00:00:22,880 --> 00:00:25,320
Federal Public Ministry,

14
00:00:25,880 --> 00:00:27,880
State Public Ministry,

15
00:00:28,280 --> 00:00:30,280
State Public Defender's Office

16
00:00:30,800 --> 00:00:32,920
and the Public Defender's Office,

17
00:00:33,200 --> 00:00:34,760
together with Braskem.


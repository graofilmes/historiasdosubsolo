1
00:00:04,800 --> 00:00:06,240
One of the worst crimes

two
00:00:06,240 --> 00:00:07,440
socio-environmental

3
00:00:07,440 --> 00:00:09,440
of the history of Brazil and the world

4
00:00:09,440 --> 00:00:10,560
was transformed

5
00:00:10,560 --> 00:00:12,120
in real estate capital.

6
00:00:12,120 --> 00:00:13,960
Not only was there no punishment

7
00:00:13,960 --> 00:00:16,000
to date, as Braskem

8
00:00:16,000 --> 00:00:16,800
get out of history

9
00:00:16,800 --> 00:00:18,360
making tens of billions

10
00:00:18,360 --> 00:00:19,560
of reais.

11
00:00:19,880 --> 00:00:22,160
What she performed with indemnities

12
00:00:22,160 --> 00:00:23,040
does not compare

13
00:00:23,040 --> 00:00:25,400
what it does for profit.

14
00:00:25,400 --> 00:00:26,600
The ghost"

15
00:00:26,600 --> 00:00:28,200
of a possible bankruptcy

16
00:00:28,200 --> 00:00:29,640
haunts everyone

17
00:00:29,640 --> 00:00:31,080
and is used as an argument

18
00:00:31,080 --> 00:00:32,560
that would make it impossible for the company

19
00:00:32,560 --> 00:00:35,080
to continue paying what you owe.

20
00:00:35,080 --> 00:00:37,240
The company feeds on an image

21
00:00:37,240 --> 00:00:38,240
of what is essential

22
00:00:38,240 --> 00:00:39,680
to the economy of Alagoas,

23
00:00:39,680 --> 00:00:42,120
but this is not supported by data.

24
00:00:42,120 --> 00:00:43,680
The mining and petrochemical company,

25
00:00:43,680 --> 00:00:44,640
therefore,

26
00:00:44,640 --> 00:00:45,400
already installed

27
00:00:45,400 --> 00:00:47,760
in an area of ​​great environmental value,

28
00:00:47,760 --> 00:00:48,960
just appropriated

29
00:00:48,960 --> 00:00:51,080
three kilometers from the shore

30
00:00:51,080 --> 00:00:53,400
and 300 hectares of urban area

31
00:00:53,400 --> 00:00:54,800
in one of the best regions

32
00:00:54,800 --> 00:00:55,560
from Maceió.

33
00:00:55,840 --> 00:00:56,960
And the public debate

34
00:00:56,960 --> 00:00:58,880
about what will be done in this area

35
00:00:59,080 --> 00:01:00,160
continues to be done

36
00:01:00,160 --> 00:01:01,400
no transparency

37
00:01:01,400 --> 00:01:03,000
and broad popular participation.

38
00:01:03,600 --> 00:01:05,760
If you continue like this,

39
00:01:05,760 --> 00:01:07,200
the result can be

40
00:01:07,200 --> 00:01:09,040
disastrous for the city.


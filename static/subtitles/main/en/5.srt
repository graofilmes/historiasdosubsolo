1
00:00:07,200 --> 00:00:08,760
Although Salgema's operation

two
00:00:09,080 --> 00:00:11,200
Chemical Industries S.A.

3
00:00:11,200 --> 00:00:12,760
has actually started

4
00:00:12,760 --> 00:00:14,840
in 1977,

5
00:00:15,440 --> 00:00:17,440
accidents at the industrial plant

6
00:00:17,440 --> 00:00:20,000
had been registered long before.

7
00:00:21,360 --> 00:00:22,760
The first documented fact

8
00:00:22,760 --> 00:00:24,120
by the Alagoas press

9
00:00:24,120 --> 00:00:27,520
took place in September 1976,

10
00:00:27,960 --> 00:00:28,640
when it exploded

11
00:00:28,640 --> 00:00:30,320
a brine reservoir

12
00:00:30,320 --> 00:00:33,200
installed in the mining area.

13
00:00:33,200 --> 00:00:34,040
There was the record

14
00:00:34,040 --> 00:00:35,360
of the death of a worker.

15
00:00:36,080 --> 00:00:38,000
Reactions to the installation

16
00:00:38,000 --> 00:00:39,080
started.


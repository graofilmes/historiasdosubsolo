1
00:00:07,200 --> 00:00:08,760
Embora a operação da Salgema

2
00:00:09,080 --> 00:00:11,200
Indústrias Químicas S.A.

3
00:00:11,200 --> 00:00:12,760
tenha efetivamente começado

4
00:00:12,760 --> 00:00:14,840
em 1977,

5
00:00:15,440 --> 00:00:17,440
acidentes na planta industrial

6
00:00:17,440 --> 00:00:20,000
já haviam sido registrados bem antes.

7
00:00:21,360 --> 00:00:22,760
O primeiro fato documentado

8
00:00:22,760 --> 00:00:24,120
pela imprensa alagoana

9
00:00:24,120 --> 00:00:27,520
ocorreu em setembro de 1976,

10
00:00:27,960 --> 00:00:28,640
quando explodiu

11
00:00:28,640 --> 00:00:30,320
um reservatório de salmoura

12
00:00:30,320 --> 00:00:33,200
instalado na área da mineração.

13
00:00:33,200 --> 00:00:34,040
Houve o registro

14
00:00:34,040 --> 00:00:35,360
da morte de um operário.

15
00:00:36,080 --> 00:00:38,000
As reações contra a instalação

16
00:00:38,000 --> 00:00:39,080
iniciaram-se.


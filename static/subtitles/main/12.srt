1
00:00:02,800 --> 00:00:04,880
Até meados de 2022,

2
00:00:05,360 --> 00:00:06,840
aproximadamente 15 mil

3
00:00:06,840 --> 00:00:09,240
residências foram destruídas,

4
00:00:09,240 --> 00:00:11,000
uma área de 300 hectares,

5
00:00:11,000 --> 00:00:13,720
quase 5 mil empreendedores

6
00:00:14,000 --> 00:00:15,840
que, além de perderem sua renda,

7
00:00:15,840 --> 00:00:17,360
foram obrigados a demitir

8
00:00:17,360 --> 00:00:19,920
mais de 20 mil trabalhadores.

9
00:00:19,920 --> 00:00:21,960
Aproximadamente 60 mil moradores

10
00:00:21,960 --> 00:00:23,640
foram expulsos

11
00:00:23,640 --> 00:00:25,680
sem saber quando, onde

12
00:00:25,680 --> 00:00:28,040
e como vão recomeçar suas vidas.

13
00:00:28,800 --> 00:00:30,560
Os locais que os maceioenses

14
00:00:30,560 --> 00:00:31,480
já se acostumaram

15
00:00:31,480 --> 00:00:33,480
a chamar de "bairros fantasmas"

16
00:00:33,840 --> 00:00:35,600
são cenários distópicos

17
00:00:35,600 --> 00:00:37,840
de uma acelerada degradação.

18
00:00:38,560 --> 00:00:40,680
Das casas abandonadas,

19
00:00:40,680 --> 00:00:42,400
os antigos moradores levaram

20
00:00:42,400 --> 00:00:45,480
portas, telhas, vidraças, pisos

21
00:00:45,680 --> 00:00:47,720
e tudo que pudesse ter algum valor.

22
00:00:48,920 --> 00:00:51,080
Operários pagos pela Braskem

23
00:00:51,080 --> 00:00:51,880
passaram então

24
00:00:51,880 --> 00:00:53,120
a lacrar com concreto

25
00:00:53,120 --> 00:00:53,880
os imóveis

26
00:00:53,880 --> 00:00:55,960
que agora pertencem à empresa.

27
00:00:55,960 --> 00:00:57,040
Tudo sob um forte

28
00:00:57,040 --> 00:00:58,400
sistema de vigilância

29
00:00:58,400 --> 00:00:59,720
e segurança privada,

30
00:00:59,720 --> 00:01:01,280
24 horas por dia.


1
00:00:04,760 --> 00:00:07,080
Em 1996,

2
00:00:07,080 --> 00:00:09,160
com a mudança de administração,

3
00:00:09,160 --> 00:00:10,400
no ano anterior,

4
00:00:10,400 --> 00:00:11,920
a petroquímica Salgema

5
00:00:11,920 --> 00:00:14,080
passa a se chamar Trikem

6
00:00:14,080 --> 00:00:16,000
e atua de forma integrada

7
00:00:16,000 --> 00:00:18,720
à OPP Petroquímica.

8
00:00:18,720 --> 00:00:21,120
Ainda em 1996,

9
00:00:21,120 --> 00:00:22,680
a Odebrecht se associa

10
00:00:22,680 --> 00:00:23,880
ao Grupo Mariani

11
00:00:23,880 --> 00:00:26,440
para criar a Proppet.

12
00:00:26,440 --> 00:00:28,520
Em parceria com o grupo Mariani,

13
00:00:28,520 --> 00:00:31,560
a Odebrecht adquire o controle da Copene

14
00:00:31,560 --> 00:00:34,400
Central Petroquímica de Camaçari

15
00:00:34,400 --> 00:00:35,480
na Bahia,

16
00:00:35,480 --> 00:00:36,920
e da Polialden.

17
00:00:37,680 --> 00:00:38,720
No mesmo ano,

18
00:00:38,720 --> 00:00:39,960
inicia um processo

19
00:00:39,960 --> 00:00:41,720
de integração de ativos

20
00:00:41,720 --> 00:00:42,920
de primeira

21
00:00:42,920 --> 00:00:44,720
e segunda gerações,

22
00:00:44,720 --> 00:00:47,920
ação inédita no Brasil.

23
00:00:47,920 --> 00:00:49,040
Em 2002,

24
00:00:49,040 --> 00:00:50,320
a partir da integração

25
00:00:50,320 --> 00:00:52,240
de seis empresas

26
00:00:52,240 --> 00:00:53,160
Copene,

27
00:00:53,160 --> 00:00:54,320
OPP,

28
00:00:54,320 --> 00:00:55,320
Trikem,

29
00:00:55,320 --> 00:00:56,680
Nitrocarbono,

30
00:00:56,680 --> 00:00:57,880
Proppet

31
00:00:57,880 --> 00:00:59,120
e Polialden,

32
00:00:59,120 --> 00:01:01,040
nasce a Braskem S.A.

33
00:01:01,040 --> 00:01:02,120
criando em Alagoas

34
00:01:02,120 --> 00:01:03,560
a Unidade Braskem

35
00:01:03,560 --> 00:01:04,720
Cloro Soda.


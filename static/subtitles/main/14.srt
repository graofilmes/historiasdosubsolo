1
00:00:07,480 --> 00:00:08,400
Ainda são muitas

2
00:00:08,400 --> 00:00:10,680
perguntas sem respostas

3
00:00:10,680 --> 00:00:12,640
quando se pensa no futuro de Maceió

4
00:00:12,640 --> 00:00:14,720
e dos bairros diretamente afetados

5
00:00:14,760 --> 00:00:17,640
pela tragédia da Braskem.

6
00:00:17,640 --> 00:00:19,280
Alguns caminhos de solução

7
00:00:19,280 --> 00:00:21,640
para parte dos problemas já existem

8
00:00:21,960 --> 00:00:24,000
e foram apontados:

9
00:00:24,000 --> 00:00:25,360
desativação das minas

10
00:00:25,360 --> 00:00:26,920
dentro da cidade,

11
00:00:26,920 --> 00:00:28,720
tecnologias de preenchimento

12
00:00:28,720 --> 00:00:29,280
do solo

13
00:00:29,280 --> 00:00:30,240
de algumas delas

14
00:00:30,240 --> 00:00:31,440
e outras engenharias

15
00:00:31,440 --> 00:00:33,800
para diminuir o impacto e os riscos

16
00:00:34,080 --> 00:00:36,480
de novos acidentes.

17
00:00:36,480 --> 00:00:38,280
Mas o fato é que muita coisa

18
00:00:38,280 --> 00:00:40,320
ainda é impossível de saber,

19
00:00:40,320 --> 00:00:42,680
de prever e de solucionar.

20
00:00:42,680 --> 00:00:44,480
A começar pela instabilidade

21
00:00:44,480 --> 00:00:46,760
do solo nas áreas afetadas.

22
00:00:46,760 --> 00:00:48,640
Não há consenso sobre o assunto

23
00:00:48,640 --> 00:00:51,000
em seus aspectos geotécnicos

24
00:00:51,000 --> 00:00:52,520
e justamente por isso

25
00:00:52,520 --> 00:00:54,960
há uma demanda evidente e urgente

26
00:00:54,960 --> 00:00:58,200
por investimentos em pesquisa.

27
00:00:58,200 --> 00:00:59,480
Por outro lado,

28
00:00:59,480 --> 00:01:01,480
os atuais acionistas da Braskem

29
00:01:01,840 --> 00:01:02,680
se movimentam

30
00:01:02,680 --> 00:01:04,880
para executar acordos judiciais

31
00:01:05,320 --> 00:01:07,160
e indenizações

32
00:01:07,160 --> 00:01:08,640
enquanto preparam a venda

33
00:01:08,640 --> 00:01:09,360
de uma companhia

34
00:01:09,360 --> 00:01:10,960
que segue dando lucro,

35
00:01:10,960 --> 00:01:13,480
mas com um passivo incalculável,

36
00:01:13,480 --> 00:01:14,840
o que dificulta a operação.


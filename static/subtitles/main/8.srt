1
00:00:02,040 --> 00:00:03,760
Em dezembro de 2019

2
00:00:03,760 --> 00:00:05,000
foi assinado o termo

3
00:00:05,000 --> 00:00:06,760
de acordo para o apoio

4
00:00:06,760 --> 00:00:09,120
na desocupação das áreas de risco,

5
00:00:09,600 --> 00:00:11,680
que definia autonomia à Braskem

6
00:00:12,120 --> 00:00:13,120
para negociação

7
00:00:13,120 --> 00:00:14,920
dos valores indenizatórios

8
00:00:14,920 --> 00:00:15,880
 diretamente

9
00:00:15,880 --> 00:00:17,760
com as pessoas proprietárias

10
00:00:17,760 --> 00:00:20,200
dos imóveis afetados.

11
00:00:20,200 --> 00:00:22,080
As partes que assinaram a decisão

12
00:00:22,080 --> 00:00:22,880
foram:

13
00:00:22,880 --> 00:00:25,320
Ministério Público Federal,

14
00:00:25,880 --> 00:00:27,880
Ministério Público Estadual,

15
00:00:28,280 --> 00:00:30,280
Defensoria Pública do Estado

16
00:00:30,800 --> 00:00:32,920
e a Defensoria Pública da União,

17
00:00:33,200 --> 00:00:34,760
juntamente com a Braskem.


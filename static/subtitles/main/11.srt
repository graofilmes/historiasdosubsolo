1
00:00:04,600 --> 00:00:06,880
O Serviço Geológico do Brasil

2
00:00:06,880 --> 00:00:08,480
(CPRM)

3
00:00:08,480 --> 00:00:09,920
concluiu que

4
00:00:09,920 --> 00:00:11,280
a principal causa

5
00:00:11,280 --> 00:00:13,000
para o surgimento das rachaduras

6
00:00:13,000 --> 00:00:14,680
nos bairros do Pinheiro,

7
00:00:14,680 --> 00:00:16,560
Mutange e Bebedouro

8
00:00:16,560 --> 00:00:19,080
é a atividade da Braskem na região

9
00:00:19,080 --> 00:00:21,440
para extração de sal-gema.

10
00:00:23,160 --> 00:00:25,160
As atividades de mineração

11
00:00:25,160 --> 00:00:26,720
ao longo de quatro décadas

12
00:00:26,720 --> 00:00:29,320
nas minas sob a cidade de Maceió

13
00:00:29,320 --> 00:00:31,200
retiraram sal-gema

14
00:00:31,200 --> 00:00:32,160
o equivalente

15
00:00:32,160 --> 00:00:35,440
 a três estádios do Maracanã.


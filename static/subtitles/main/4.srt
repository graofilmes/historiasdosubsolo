1
00:00:02,160 --> 00:00:03,920
O Projeto de implantação

2
00:00:03,920 --> 00:00:06,800
da Salgema Indústrias Químicas S.A.

3
00:00:06,800 --> 00:00:08,800
na restinga do Pontal da Barra,

4
00:00:08,800 --> 00:00:12,480
iniciou-se em 1974

5
00:00:12,480 --> 00:00:15,080
durante o Governo Afrânio Lages,

6
00:00:15,080 --> 00:00:16,480
que havia sido nomeado

7
00:00:16,480 --> 00:00:17,480
quatro anos antes

8
00:00:17,480 --> 00:00:20,000
para o cargo pelo General Médici

9
00:00:20,000 --> 00:00:21,080
e foi definido

10
00:00:21,080 --> 00:00:23,720
durante o Governo Divaldo Suruagy,

11
00:00:23,720 --> 00:00:25,800
sob a coordenação do engenheiro

12
00:00:25,800 --> 00:00:27,440
Beroaldo Maia Gomes.

13
00:00:29,480 --> 00:00:31,160
Entrevistas e reportagens

14
00:00:31,160 --> 00:00:32,920
revelam que a escolha do local

15
00:00:32,920 --> 00:00:34,560
passou por um grupo de técnicos

16
00:00:34,560 --> 00:00:36,920
procedentes dos Estados Unidos,

17
00:00:36,920 --> 00:00:39,280
incluindo o vice presidente da DuPont.

18
00:00:39,840 --> 00:00:42,320
Eles acharam que o único lugar adequado

19
00:00:42,320 --> 00:00:44,800
seria onde hoje a Salgema está instalada.

20
00:00:45,800 --> 00:00:48,360
"Era lá ou em nenhum outro lugar"

21
00:00:48,840 --> 00:00:49,920
 relatou Beroaldo.


1
00:00:05,160 --> 00:00:07,240
Diante da complexidade

2
00:00:07,240 --> 00:00:08,560
e da necessidade

3
00:00:08,560 --> 00:00:09,640
em dar respostas

4
00:00:09,640 --> 00:00:10,920
para a população,

5
00:00:10,920 --> 00:00:12,440
a Prefeitura de Maceió

6
00:00:12,440 --> 00:00:14,520
decreta “Situação de Emergência”.

7
00:00:16,080 --> 00:00:18,600
Após as análises preliminares

8
00:00:19,040 --> 00:00:20,960
realizadas pelas instituições

9
00:00:20,960 --> 00:00:22,560
envolvidas no caso,

10
00:00:22,560 --> 00:00:23,480
foi concluído

11
00:00:23,480 --> 00:00:25,880
que o fenômeno ocorrido no Pinheiro

12
00:00:25,880 --> 00:00:27,920
nunca havia sido registrado antes

13
00:00:28,560 --> 00:00:29,360
em área urbana

14
00:00:29,360 --> 00:00:31,240
de nenhuma outra cidade no Brasil.


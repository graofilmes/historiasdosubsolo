1
00:00:03,920 --> 00:00:06,172
Quando as sondas perfuraram o solo

2
00:00:06,172 --> 00:00:08,508
nas áreas de mangue da Lagoa Mundaú

3
00:00:08,508 --> 00:00:10,760
em 1941

4
00:00:10,760 --> 00:00:13,805
o objetivo do Conselho Nacional do Petróleo

5
00:00:13,805 --> 00:00:16,433
era a prospecção de petróleo.

6
00:00:16,433 --> 00:00:17,600
A firma contratada

7
00:00:17,600 --> 00:00:20,228
não teve sucesso com o petróleo,

8
00:00:20,228 --> 00:00:22,939
mas encontrou um leito de sal-gema

9
00:00:22,939 --> 00:00:24,441
sob o solo de Maceió.


1
00:00:03,440 --> 00:00:04,720
Dados confirmados

2
00:00:04,720 --> 00:00:06,280
pela Secretaria Municipal

3
00:00:06,280 --> 00:00:07,800
de Saúde de Maceió,

4
00:00:07,800 --> 00:00:09,480
de um levantamento realizado

5
00:00:09,480 --> 00:00:10,840
em 2019,

6
00:00:10,840 --> 00:00:12,600
parceria do Ministério da Saúde

7
00:00:12,600 --> 00:00:13,520
com moradores

8
00:00:13,520 --> 00:00:14,520
e ex-moradores

9
00:00:14,520 --> 00:00:16,360
das áreas atingidas,

10
00:00:16,360 --> 00:00:17,400
mostram que mais

11
00:00:17,400 --> 00:00:19,520
de 50% dos entrevistados

12
00:00:20,000 --> 00:00:21,360
apresentaram indícios

13
00:00:21,360 --> 00:00:23,320
de sofrimento mental grave

14
00:00:23,320 --> 00:00:24,400
e comprometimento

15
00:00:24,400 --> 00:00:26,080
 de atividades diárias.

16
00:00:29,520 --> 00:00:30,960
O recorte da pesquisa

17
00:00:30,960 --> 00:00:32,360
mostrou que,

18
00:00:32,360 --> 00:00:34,240
como uma triste simbiose

19
00:00:34,240 --> 00:00:35,680
entre morador e imóvel,

20
00:00:36,240 --> 00:00:38,680
ambos sucumbiram à catástrofe

21
00:00:38,680 --> 00:00:39,880
e o luto passou a ser

22
00:00:39,880 --> 00:00:41,840
vivenciado com frequência.


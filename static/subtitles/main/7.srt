1
00:00:07,160 --> 00:00:10,040
No dia 3 de março de 2018

2
00:00:10,040 --> 00:00:11,280
às 14:30,

3
00:00:11,280 --> 00:00:12,920
moradores de Maceió,

4
00:00:12,920 --> 00:00:14,200
capital de Alagoas,

5
00:00:14,200 --> 00:00:15,720
sentem tremores de terra

6
00:00:15,720 --> 00:00:18,160
em alguns bairros da cidade.

7
00:00:18,160 --> 00:00:20,000
O epicentro dos abalos

8
00:00:20,000 --> 00:00:21,640
aconteceu no Pinheiro,

9
00:00:21,640 --> 00:00:23,080
bairro de pouco mais

10
00:00:23,080 --> 00:00:24,440
de 19 mil habitantes

11
00:00:24,440 --> 00:00:25,320
localizado

12
00:00:25,320 --> 00:00:26,320
à 5km

13
00:00:26,320 --> 00:00:27,480
do centro da cidade.

14
00:00:28,200 --> 00:00:29,640
O fenômeno causou

15
00:00:29,640 --> 00:00:31,040
afundamentos em ruas

16
00:00:31,040 --> 00:00:32,440
e fissuras em prédios,

17
00:00:32,440 --> 00:00:34,520
para desespero dos moradores.

18
00:00:35,400 --> 00:00:36,720
No dia do evento,

19
00:00:36,720 --> 00:00:37,440
equipes

20
00:00:37,440 --> 00:00:38,760
do Corpo de Bombeiros

21
00:00:38,760 --> 00:00:40,160
e da Defesa Civil

22
00:00:40,160 --> 00:00:41,360
foram até o local

23
00:00:41,360 --> 00:00:42,960
para orientar a população

24
00:00:42,960 --> 00:00:44,080
sobre procedimentos

25
00:00:44,080 --> 00:00:45,480
que deveriam ser adotados

26
00:00:45,480 --> 00:00:47,360
naquele momento.

27
00:00:47,760 --> 00:00:48,840
Naquele dia,

28
00:00:48,840 --> 00:00:50,280
os tremores foram registrados

29
00:00:50,280 --> 00:00:51,840
pelo Laboratório Sismológico

30
00:00:51,840 --> 00:00:53,080
da Universidade Federal

31
00:00:53,080 --> 00:00:54,480
do Rio Grande do Norte,

32
00:00:54,480 --> 00:00:56,160
que acusou a magnitude

33
00:00:56,160 --> 00:00:58,640
de 2,5 graus na escala Richter.


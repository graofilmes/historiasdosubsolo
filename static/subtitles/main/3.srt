1
00:00:03,712 --> 00:00:04,337
A empresa

2
00:00:04,337 --> 00:00:07,173
Salgema Indústrias Químicas S.A.

3
00:00:07,173 --> 00:00:10,301
surgiu em 1966

4
00:00:10,301 --> 00:00:13,221
com a participação da Euluz S.A.

5
00:00:13,221 --> 00:00:15,098
e Euvaldo Luz.

6
00:00:15,098 --> 00:00:17,142
Em 1968

7
00:00:17,559 --> 00:00:18,476
a DuPont

8
00:00:18,476 --> 00:00:19,769
teve participação

9
00:00:19,769 --> 00:00:21,396
autorizada pela Sudene.

10
00:00:22,105 --> 00:00:23,773
Em seguida o BNDE

11
00:00:23,773 --> 00:00:25,275
adere ao projeto

12
00:00:25,275 --> 00:00:26,693
tornando impossível

13
00:00:26,693 --> 00:00:28,111
a participação

14
00:00:28,111 --> 00:00:29,738
do grupo Euvaldo Luz.

15
00:00:30,905 --> 00:00:33,199
Suas ações foram repassadas

16
00:00:33,366 --> 00:00:34,701
para a Petroquisa

17
00:00:34,701 --> 00:00:37,245
que passou a dividir com a DuPont

18
00:00:37,245 --> 00:00:38,496
o controle da estatal

19
00:00:38,496 --> 00:00:41,082
Salgema Indústrias Químicas S.A.


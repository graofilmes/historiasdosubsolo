1
00:00:00,039 --> 00:00:01,960
No caso da duplicação

2
00:00:01,960 --> 00:00:05,000
tem algumas coisas também muito interessantes.

3
00:00:07,760 --> 00:00:09,159
<b>A Salgema</b>

4
00:00:10,000 --> 00:00:14,079
pagou uma propaganda

5
00:00:14,079 --> 00:00:17,159
na TV, aqui no estado

6
00:00:18,320 --> 00:00:21,639
<b>Que era uma citação de Jacques Cousteau.</b>

7
00:00:23,480 --> 00:00:25,639
<b>Aparecia Jacques Cousteau</b>

8
00:00:26,440 --> 00:00:28,880
Jacques Cousteau falando

9
00:00:28,880 --> 00:00:31,119
fazendo um discurso.

10
00:00:31,880 --> 00:00:34,599
e eles embasando o discurso deles

11
00:00:34,599 --> 00:00:37,400
encima do discurso de Jacques Cousteau.

12
00:00:39,679 --> 00:00:40,840
<b>Não custou nada.</b>

13
00:00:41,880 --> 00:00:46,199
Eu tinha contato com a fundação Jacques Cousteau

14
00:00:47,039 --> 00:00:48,519
<b>nos Estados Unidos.</b>

15
00:00:49,159 --> 00:00:52,440
<b>Imediatamente eu mandei para eles.</b>

16
00:00:53,519 --> 00:00:56,760
Então eles mandaram me pedir todos os dados.

17
00:00:56,760 --> 00:00:57,960
Eu mandei.

18
00:00:58,559 --> 00:00:59,719
Era ilegal.

19
00:00:59,719 --> 00:01:02,639
Eles tinham que pagar direitos autorais

20
00:01:02,639 --> 00:01:04,599
a Jacques Cousteau

21
00:01:04,599 --> 00:01:07,760
e Jacques Cousteau não autorizava

22
00:01:07,760 --> 00:01:10,519
aquel tipo de associação.

23
00:01:11,320 --> 00:01:13,320
Está no jornal também, está na imprensa.

24
00:01:13,320 --> 00:01:15,599
Houve vários esforços

25
00:01:15,599 --> 00:01:18,320
para convencer a sociedade.

26
00:01:18,920 --> 00:01:22,320
Um deles foi um discurso que eles criaram

27
00:01:22,320 --> 00:01:25,840
de que a gente não entendia o que era duplicação.

28
00:01:28,000 --> 00:01:30,800
Gente, eu entendi desde o começo.

29
00:01:30,800 --> 00:01:34,800
Eu sabia que não era duplicação da área.

30
00:01:35,519 --> 00:01:37,800
Então eles criaram essa narrativa

31
00:01:37,800 --> 00:01:40,239
e trabalharam muito em cima dela.

32
00:01:40,239 --> 00:01:42,679
Era duplicação

33
00:01:42,679 --> 00:01:45,039
<b>no volume de produção</b>

34
00:01:45,760 --> 00:01:46,880
<b>que foi feita.</b>

35
00:01:47,320 --> 00:01:50,559
Duplicação do impacto na verdade, não é?

36
00:01:51,639 --> 00:01:53,239
<b>Duplicou os impactos.</b>


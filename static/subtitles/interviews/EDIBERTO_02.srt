1
00:00:00,041 --> 00:00:02,252
Se havia medo com relação

2
00:00:02,252 --> 00:00:06,423
à planta industrial instalada
já no início, em meados dos anos 70

3
00:00:07,173 --> 00:00:11,803
acho que em 77 começou a produzir
a planta industrial da Salgema

4
00:00:12,470 --> 00:00:14,556
imagina agora com a duplicação da...

5
00:00:15,473 --> 00:00:17,600
pelo menos da parte de biocloretano

6
00:00:17,600 --> 00:00:20,520
a planta de Biocloretano ia ser duplicada.
Como foi!

7
00:00:21,771 --> 00:00:24,024
Aí surgiram algumas reações maiores.

8
00:00:25,525 --> 00:00:29,112
Havia também uma reação
com relação ao despejo de água

9
00:00:29,112 --> 00:00:32,323
ou água utilizada
para o resfriamento de caldeiras.

10
00:00:32,574 --> 00:00:37,245
Eu não entendo exatamente o processo mas
havia o medo da poluição na lagoa Mundaú,

11
00:00:38,163 --> 00:00:41,624
porque a planta industrial da Salgema
era a melhor planta do mundo.

12
00:00:42,625 --> 00:00:43,334
Ela tem...

13
00:00:43,334 --> 00:00:46,463
Ela está a poucos quilômetros

14
00:00:47,380 --> 00:00:49,716
da fonte de matéria prima

15
00:00:49,716 --> 00:00:52,093
da salmoura que é retirada dos poços.

16
00:00:53,094 --> 00:00:55,430
Ela tem energia barata

17
00:00:55,430 --> 00:00:58,516
e ela tem o alcool também próximo

18
00:00:59,225 --> 00:01:02,937
o que as usinas produzem o alcool
de onde você vai tirar o eteno

19
00:01:03,605 --> 00:01:09,027
para formar o componente
que é um dos mais caros da produção.

20
00:01:10,070 --> 00:01:13,490
Então toda essa facilidade...

21
00:01:13,490 --> 00:01:15,575
impôs à Alagoas...

22
00:01:15,784 --> 00:01:17,952
e havia ameaça na época,
chantagens:

23
00:01:17,952 --> 00:01:21,039
"Se vocês não quiserem
a gente leva pra Sergipe!"

24
00:01:21,039 --> 00:01:23,833
Praticamente o Governo Federal...

25
00:01:23,958 --> 00:01:27,170
os militares impuseram à Alagoas
a instalação da planta.

26
00:01:28,254 --> 00:01:30,590
E assim ela começou a funcionar
com todo esse risco

27
00:01:30,590 --> 00:01:35,428
até as reações que já
em meados dos anos 80 quando houve...

28
00:01:36,763 --> 00:01:41,518
já havia também uma crescente
preocupação ecológica que era incipiente em AL

29
00:01:41,726 --> 00:01:43,520
até aquele momento.

30
00:01:43,520 --> 00:01:46,481
Havia claro as primeiras reações ecológicas.

31
00:01:46,773 --> 00:01:48,858
Elas combatiam...

32
00:01:49,192 --> 00:01:51,778
o que as usinas jogavam nos rios.

33
00:01:54,280 --> 00:01:56,991
Esse ubproduto da produção do açúcar

34
00:01:56,991 --> 00:02:00,328
que era jogado nos rios
e poluia os rios.

35
00:02:00,328 --> 00:02:02,205
e matava os peixes, etc.

36
00:02:02,539 --> 00:02:04,666
Já haviam uma preocupações desse tipo.

37
00:02:05,875 --> 00:02:09,170
Mas é a partir do surgimento
de um núcleo de ambientalistas

38
00:02:09,170 --> 00:02:11,756
na Secretaria de Planejamento
do Governo do Estado

39
00:02:12,465 --> 00:02:16,427
que se expande essa compreensão ecológica em Alagoas num nível mais ativo

40
00:02:16,803 --> 00:02:21,015
num nível mais participativo que redunda
nesse movimento dos anos 80.

41
00:02:21,266 --> 00:02:24,936
Mas a principal entidade que surgiu
foi a partir de Nivaldo Miranda

42
00:02:24,936 --> 00:02:27,939
com o Movimento pela Vida
no início dos anos 80.

43
00:02:28,439 --> 00:02:30,984
É quando surge o Movimento Pela Vida

44
00:02:31,526 --> 00:02:36,322
que vai ser a principal entidade
ligada a esse meio.

45
00:02:36,322 --> 00:02:40,743
Além da montagem
nas secretarias de governo do Estado.

46
00:02:43,621 --> 00:02:46,624
Das secretários de Meio Ambiente
que também começam a atuar

47
00:02:46,624 --> 00:02:49,794
e agregam profissionais
com essa compreensão.

48
00:02:50,628 --> 00:02:52,213
Tivemos várias manifestações,

49
00:02:52,213 --> 00:02:53,840
agora a principal foi esse ato

50
00:02:54,215 --> 00:02:56,092
que como vocês viram:
foi esvaziado.

51
00:02:56,426 --> 00:02:59,679
O ato é pequeno, relativamente pequeno
para um problema tão grande

52
00:02:59,804 --> 00:03:01,806
porque Maceió...

53
00:03:01,806 --> 00:03:05,476
as pessoas achavam que o problema era só
de quem morava no trapiche e no Pontal

54
00:03:05,476 --> 00:03:08,980
e o resto da cidade
não tinha muita preocupação

55
00:03:08,980 --> 00:03:11,065
porque como o problema
era a explosão

56
00:03:11,065 --> 00:03:14,903
ou a possibilidade de vazamento do gás

57
00:03:14,903 --> 00:03:18,823
e isso ficaria restrito a uma área
muito em torno da planta industrial.

58
00:03:19,574 --> 00:03:20,783
Não havia preocupação

59
00:03:20,783 --> 00:03:23,161
e por coincidência no mesmo dia do ato

60
00:03:23,161 --> 00:03:26,080
ou no dia anterior, não lembro.

61
00:03:26,247 --> 00:03:29,000
Eu acho que foi Evilásio Soriano
que era o secretário do Planejamento

62
00:03:29,000 --> 00:03:31,586
ele foi à rede de televisão e...

63
00:03:32,503 --> 00:03:35,048
fez uma intervenção
explicando que não havia risco

64
00:03:35,048 --> 00:03:39,802
ou seja: tecnicamente, entre aspas,
foi explicado que não haveria risco algum

65
00:03:40,595 --> 00:03:43,640
e que era um absurdo o protesto
que não tinha sentido e que...

66
00:03:45,141 --> 00:03:47,852
por aí... não me lembro
exatamente os detalhes da intervenção

67
00:03:47,852 --> 00:03:51,022
que ele fez
mas isso também ajudou a esvaziar o ato.

68
00:03:51,689 --> 00:03:55,318
Mas como era era um movimento nascente
e essa consciência ecológica

69
00:03:55,318 --> 00:03:57,737
não tinha a força que tem hoje

70
00:03:58,488 --> 00:04:02,909
então era assim.
O ato politicamente ele não...

71
00:04:02,992 --> 00:04:04,619
Ele conseguiu alguma repercussão

72
00:04:04,619 --> 00:04:08,331
porque o sindicato dos jornalistas
estava participando

73
00:04:09,457 --> 00:04:10,959
e outros profissionais de comunicação

74
00:04:10,959 --> 00:04:15,880
conseguiram reverberar
essa manifestação.

75
00:04:16,089 --> 00:04:18,132
Ou o conteúdo dela pelo menos.

76
00:04:18,132 --> 00:04:19,926
A Sociedade de Defesa
dos Direitos Humanos,

77
00:04:19,926 --> 00:04:21,886
o Movimento pela Vida.

78
00:04:22,595 --> 00:04:23,680
Vários sindicatos

79
00:04:23,680 --> 00:04:26,474
principalmente dos jornalistas
eu me lembro.

80
00:04:27,100 --> 00:04:28,893
personalidades

81
00:04:29,602 --> 00:04:30,687
da política alagoana

82
00:04:30,687 --> 00:04:32,689
deputados, vereadores

83
00:04:33,564 --> 00:04:35,900
eu me lembro
que havia toda uma participação

84
00:04:36,067 --> 00:04:38,236
e uma preocupação de alguns segmentos

85
00:04:38,236 --> 00:04:40,113
e até agora

86
00:04:40,321 --> 00:04:42,991
a participação do público,
das pessoas

87
00:04:42,991 --> 00:04:46,703
fora os que ja tinham certo engajamento,
os estudantes de agronomia.

88
00:04:46,703 --> 00:04:49,914
Na época eles tinham uma certa
compreensão dessa questão

89
00:04:49,914 --> 00:04:51,582
e se destacavam alguns deles

90
00:04:52,333 --> 00:04:54,794
dessa área de biologia.

91
00:04:55,503 --> 00:04:58,965
Alguns já começaram a surgir
do ponto de vista técnico.

92
00:04:59,674 --> 00:05:01,342
A pessoa estudava na universidade

93
00:05:01,342 --> 00:05:02,176
o aluno lá

94
00:05:02,176 --> 00:05:05,305
e ele aprendia que aquilo
tinha uma consequência

95
00:05:05,305 --> 00:05:07,140
então começaram a surgir

96
00:05:07,140 --> 00:05:10,393
o José Geraldo Marques
que teve um papel importante

97
00:05:10,393 --> 00:05:11,644
nesse período

98
00:05:12,687 --> 00:05:13,563
de denúncia

99
00:05:13,563 --> 00:05:16,399
que ele é um profissional,
um técnico dessa área.

100
00:05:17,650 --> 00:05:20,111
Então acho que foi mais ou menos isso:

101
00:05:20,320 --> 00:05:22,488
O ato não foi tão grande não.
Foi pequeno.


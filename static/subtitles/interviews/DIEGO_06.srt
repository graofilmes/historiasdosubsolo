1
00:00:00,542 --> 00:00:02,836
Os danos foram calculados

2
00:00:05,005 --> 00:00:08,049
e se chegou a um valor X

3
00:00:08,717 --> 00:00:10,301
Não sei se é por conta desse

4
00:00:10,301 --> 00:00:13,847
conjunto de experiências que eu tenho

5
00:00:15,015 --> 00:00:17,642
na área de avaliação
impactos de mineração.

6
00:00:20,228 --> 00:00:22,689
Eu o considero muito baixo

7
00:00:23,148 --> 00:00:27,944
e principalmente... aí mas Diego
você fica batendo nessa tecla de avaliação

8
00:00:28,361 --> 00:00:33,324
e a previsão de impactos, não sei o que.
Porque isso é importante e importante?

9
00:00:34,534 --> 00:00:36,828
porque quando você vai usar depois

10
00:00:36,828 --> 00:00:40,498
para chegar lá na ponta
do cálculo de uma indenização

11
00:00:40,999 --> 00:00:44,294
quanto que deve ser revertido
em projetos sociais etc.

12
00:00:45,003 --> 00:00:47,464
Se você usa um cálculo com uma metodologia...

13
00:00:51,760 --> 00:00:56,639
que digamos que ela seja menos rigorosa

14
00:00:57,182 --> 00:01:00,727
digamos com a avaliação dos impactos.

15
00:01:01,394 --> 00:01:05,356
E aí eu não estou falando
necessariamente das instituições ainda

16
00:01:05,982 --> 00:01:11,154
mas se você usa uma metodologia...
 que ela é feita basicamente uma questão de:

17
00:01:11,154 --> 00:01:13,823
Quem fez entende?

18
00:01:13,823 --> 00:01:17,327
Entende de avaliação de impacto?
Que é uma coisa simples.

19
00:01:18,078 --> 00:01:20,705
Entende o que são as metodologias.

20
00:01:21,331 --> 00:01:23,833
Provavelmente vai considerar baixo o valor.

21
00:01:25,794 --> 00:01:28,046
E isso, novamente eu falo

22
00:01:28,088 --> 00:01:31,132
é um tipo de padrão só que no caso de Maceió

23
00:01:31,883 --> 00:01:36,638
pelas características demográficas
o impacto que vai ter na cidade

24
00:01:37,472 --> 00:01:40,100
Eu acho que foi a estimativa mais

25
00:01:40,934 --> 00:01:43,937
 mais baixa possível.

26
00:01:43,937 --> 00:01:46,481
Se a gente tivesse uma escala
de baixa média e alta

27
00:01:47,857 --> 00:01:52,028
porque eu não. Aí eu tenho que reconhecer
que eu não li a metodologia.

28
00:01:52,403 --> 00:01:54,864
Porque isso não está claro no documento.

29
00:01:55,281 --> 00:01:57,700
Que são os procedimentos
o cálculo que é usado.

30
00:01:57,992 --> 00:02:00,662
Quais são os indicadores utilizados etc.

31
00:02:01,871 --> 00:02:04,374
Mas o resultado eu considerei muito baixo.

32
00:02:05,166 --> 00:02:07,585
Provavelmente esses impactos eles precisam

33
00:02:07,585 --> 00:02:09,838
se dimensionar de forma muito maior

34
00:02:11,297 --> 00:02:15,093
do que é só localizar.
Aquilo ali são as áreas diretamente afetadas.

35
00:02:15,510 --> 00:02:17,720
Você tem mais casos.

36
00:02:17,720 --> 00:02:21,224
Então não sei, como
eu estou no Comitê Gestor

37
00:02:21,933 --> 00:02:24,144
agora participando

38
00:02:24,144 --> 00:02:26,896
a gente está lidando com isso
durante um período de tempo.

39
00:02:27,522 --> 00:02:29,899
Então essa já é uma preocupação
 do Comitê Gestor

40
00:02:30,900 --> 00:02:33,695
Ele dá por exemplo onde investir esse recurso.

41
00:02:33,987 --> 00:02:36,489
O cuidado que tem que ver.
Porque é um temor que eu tenho

42
00:02:38,575 --> 00:02:39,742
e é um temor mesmo

43
00:02:39,742 --> 00:02:41,411
que os projetos sejam usados

44
00:02:41,411 --> 00:02:45,456
na Ponta Verde, na Pajuçara, na Jatiúca.

45
00:02:46,791 --> 00:02:49,169
Ah Diego, mas não pode. Mas tem, tem, tem sim.

46
00:02:49,169 --> 00:02:51,337
Ah Diego, mas será?  Não tem será aquiu coisa nenhuma.

47
00:02:52,505 --> 00:02:56,593
Porque é muito comum
o padrão dos atingidos

48
00:02:56,593 --> 00:02:59,971
dos impactados não correrem diretamente
os benefícios.

49
00:03:01,264 --> 00:03:05,518
E isso vai demandar o que?
Vai demandar pesquisa para identificar exatamente

50
00:03:05,852 --> 00:03:09,522
onde estão esses impactados
e essas impactadas

51
00:03:09,522 --> 00:03:12,483
porque senão o risco é bater

52
00:03:12,942 --> 00:03:17,155
la Ponta Verde recurso de projeto
que deveria ser voltado

53
00:03:17,488 --> 00:03:19,324
para quem mora no Mutange.

54
00:03:19,324 --> 00:03:21,784
E assim: mesmo o melhor das intenções

55
00:03:21,993 --> 00:03:25,788
por parte do Ministério Público
Defensoria Pública, etc

56
00:03:26,289 --> 00:03:29,500
Se você não estabelece o nível de parceria
muito claro

57
00:03:30,418 --> 00:03:33,296
com outras instituições da sociedade civil

58
00:03:33,296 --> 00:03:35,298
e aqui eu não estou dizendo que
a Braskem não possa participar.

59
00:03:35,465 --> 00:03:37,800
Participe. Traga. Venha.

60
00:03:38,009 --> 00:03:39,802
Porque ia ser bem né?
Para ver o tamanho da encrenca.

61
00:03:40,345 --> 00:03:43,681
Mas se você não rastreia exatamente

62
00:03:44,349 --> 00:03:48,937
o risco desses benefícios,
eles se tornaram difusos

63
00:03:49,103 --> 00:03:51,022
no mal sentido, é enorme.

64
00:03:51,814 --> 00:03:53,650
Hoje é a minha maior preocupação

65
00:03:53,650 --> 00:03:56,236
no Comitê Gestor.

66
00:03:56,236 --> 00:03:59,697
O Ministério Público, ele também,
o papel dele foi

67
00:04:00,657 --> 00:04:02,242
não somos nós
vamos decidir

68
00:04:02,617 --> 00:04:03,534
Isso é uma iniciativa

69
00:04:03,743 --> 00:04:06,454
importante porque ela é uma iniciativa

70
00:04:06,454 --> 00:04:09,040
de prestação de contas

71
00:04:10,250 --> 00:04:13,169
e transparência, que na ciência política

72
00:04:13,461 --> 00:04:16,172
eles vão chamar de accountability

73
00:04:17,006 --> 00:04:19,842
então é extremamente importante
que é accountability com função social.

74
00:04:21,970 --> 00:04:26,724
Mas a Braskem não tem a menor gerência
sobre esse caso.

75
00:04:27,141 --> 00:04:31,604
É só o recurso
que eu falo que é pouco para o tamanho

76
00:04:31,938 --> 00:04:34,732
o que seriam os danos extra patrimoniais

77
00:04:34,732 --> 00:04:37,193
causados na cidade.


1
00:00:00,280 --> 00:00:01,880
<b>A perfuração se faz</b>

2
00:00:02,119 --> 00:00:03,519
<b>como a petrobraz faz</b>

3
00:00:03,519 --> 00:00:06,119
<b>pra furar poços de petróleo</b>

4
00:00:06,119 --> 00:00:08,239
<b>ou, se não, mais fácil</b>

5
00:00:08,559 --> 00:00:11,199
<b>como todas as empresas de poços artesianos</b>

6
00:00:11,920 --> 00:00:13,119
<b>perfuram o terreno.</b>

7
00:00:13,920 --> 00:00:16,280
<b> Com uma broca que sai perfurando.</b>

8
00:00:16,280 --> 00:00:18,559
<b>Pois bem, no caso da Salgema</b>

9
00:00:18,559 --> 00:00:22,320
<b>essa broca tem mais ou menos uns 30cm de</b>

10
00:00:23,360 --> 00:00:25,039
<b>diametro, né? É uma broca.</b>

11
00:00:25,800 --> 00:00:27,800
<b>E essa broca vai furando o terreno</b>

12
00:00:27,800 --> 00:00:29,440
<b>vai furando as rochas</b>

13
00:00:29,440 --> 00:00:31,320
<b>e vai carregando consigo</b>

14
00:00:31,320 --> 00:00:32,360
<b>vai puxando</b>

15
00:00:32,360 --> 00:00:34,719
<b>acoplados a essa broca, dois tubos</b>

16
00:00:35,000 --> 00:00:38,320
<b>um com mais ou menos uns 12cm de diametro</b>

17
00:00:38,320 --> 00:00:39,360
<b>e o outro tubo de 30cm.</b>

18
00:00:39,360 --> 00:00:41,360
<b>tudo preso lá.</b>

19
00:00:41,920 --> 00:00:44,320
<b>Vão sendo carregados, puxados por essa broca.</b>

20
00:00:44,320 --> 00:00:46,280
<b>A broca vai furando as rochas</b>

21
00:00:47,039 --> 00:00:49,119
<b>num processo de lavagem</b>

22
00:00:49,119 --> 00:00:49,920
<b>de água, né?</b>

23
00:00:49,920 --> 00:00:52,920
<b>Porque sem água não consegue furar</b>

24
00:00:52,920 --> 00:00:54,639
<b>por causa da temperatura</b>

25
00:00:54,639 --> 00:00:56,159
<b>com a abrasão é muito grande.</b>

26
00:00:56,639 --> 00:00:58,480
<b>Então vai furando</b>

27
00:00:58,480 --> 00:01:01,480
<b>do mesmo jeito que faz com poço artesiano.</b>

28
00:01:01,480 --> 00:01:02,039
<b> Do mesmo jeito.</b>

29
00:01:02,039 --> 00:01:05,039
<b>Só que no caso do poço artesiano não é tão profundo, né?</b>

30
00:01:05,039 --> 00:01:06,880
<b>Então vai furando, furando e furando...</b>

31
00:01:06,880 --> 00:01:10,800
<b>E à partir de mais ou menos 950m</b>

32
00:01:10,800 --> 00:01:13,239
<b>começa a ocorrência de sal-gema.</b>

33
00:01:14,360 --> 00:01:17,360
<b>tecnicamente falando chama-se de Halita.</b>

34
00:01:18,039 --> 00:01:20,320
<b>Quando vocês ouvirem falar de halita, é sal-gema.</b>

35
00:01:20,320 --> 00:01:22,639
<b>É a rocha salina Sal-Gema.</b>

36
00:01:22,639 --> 00:01:25,639
<b>Aí começa a acontecer a 950m mais ou menos.</b>

37
00:01:25,639 --> 00:01:28,039
<b>Começa a acontecer ainda um pouco contaminada</b>

38
00:01:28,039 --> 00:01:30,559
<b>e à partir de 1000m ela já está bem legal</b>

39
00:01:30,559 --> 00:01:31,840
<b>bem mais pura.</b>

40
00:01:32,199 --> 00:01:34,599
<b>A espessura dessa camada</b>

41
00:01:34,599 --> 00:01:36,960
<b>na média é em torno de 250m .</b>

42
00:01:38,000 --> 00:01:41,000
<b>A espessuara da camada de sal que está lá em baixo</b>

43
00:01:42,239 --> 00:01:44,079
<b>250m é a média</b>

44
00:01:44,079 --> 00:01:45,880
<b>então, em um canto dá 200m</b>

45
00:01:45,880 --> 00:01:47,079
<b>em outro canto dá 280m...</b>

46
00:01:47,280 --> 00:01:50,599
<b>O máximo é 280m, então 250m seria um bom valior.</b>

47
00:01:51,000 --> 00:01:52,480
<b>Aí vai furando.</b>

48
00:01:52,480 --> 00:01:55,480
<b>A broca entrou na camada do sal</b>

49
00:01:55,519 --> 00:01:58,519
<b>e vai furando, vai furando</b>

50
00:01:58,519 --> 00:02:03,199
<b>até chegar à base da rocha salina, a 250m.</b>

51
00:02:04,840 --> 00:02:08,920
<b>A rocha salina é igual esses solventes</b>

52
00:02:08,960 --> 00:02:11,480
<b>tipo um Sonrisal</b>

53
00:02:11,480 --> 00:02:14,480
<b>ou outros...</b>

54
00:02:16,400 --> 00:02:17,840
<b>Efervecente, não é?</b>

55
00:02:17,840 --> 00:02:19,800
<b>É a mesma coisa. não pode ver água!</b>

56
00:02:20,760 --> 00:02:23,840
<b>Ela, no estado natural, sem ninguém mechendo nela</b>

57
00:02:23,840 --> 00:02:27,000
<b>ela tem a resistÊncia de um excelente concreto</b>

58
00:02:28,119 --> 00:02:30,480
<b>um concreto duro!</b>

59
00:02:30,480 --> 00:02:32,519
<b>Eu sei que vocês não sabem mas um engenheiro sabe</b>

60
00:02:32,760 --> 00:02:34,639
<b>seria um concreto de 22mpa.</b>

61
00:02:34,960 --> 00:02:36,679
<b>Altamente resistente.</b>

62
00:02:37,199 --> 00:02:38,320
<b>É um concreto muito bom.</b>

63
00:02:39,000 --> 00:02:40,360
<b>Mas só que</b>

64
00:02:40,360 --> 00:02:43,079
<b>quando a água vai batendo, ela vai...</b>

65
00:02:44,039 --> 00:02:45,519
<b>Quando chega no final, lá</b>

66
00:02:45,519 --> 00:02:49,880
<b>bem pertinho já de bater no fundo da camada</b>

67
00:02:50,599 --> 00:02:54,400
<b>já a 1250m aproximadamente, estão entendendo?</b>

68
00:02:54,960 --> 00:02:57,280
<b>Aí ele para de descer</b>

69
00:02:57,280 --> 00:02:59,239
<b>e aquele jato</b>

70
00:02:59,719 --> 00:03:02,039
<b>o jato da água</b>

71
00:03:02,440 --> 00:03:06,519
<b>começa a ficar circulando</b>

72
00:03:06,960 --> 00:03:08,000
<b>circulando</b>

73
00:03:08,000 --> 00:03:11,960
<b> e à medida que vai circulando, vai formando a salmoura.</b>

74
00:03:12,880 --> 00:03:15,880
<b>O sal que era rocha começa a se tornar em líquido.</b>

75
00:03:16,039 --> 00:03:18,519
<b>Aí começa a ficar sal com água.</b>

76
00:03:18,920 --> 00:03:20,000
<b>Sal com água</b>

77
00:03:20,760 --> 00:03:22,840
<b>que aí a gente chama de salmoura.</b>

78
00:03:23,239 --> 00:03:25,480
<b>Vocês se lembram que eu disse que são dois tubos:</b>

79
00:03:25,480 --> 00:03:28,239
<b>um de 12cm e um de 30cm.</b>

80
00:03:28,840 --> 00:03:29,920
<b>Estão lá juntinho.</b>

81
00:03:30,000 --> 00:03:33,039
<b>A àgua pra perfurar vai pelo tubo de 12cm</b>

82
00:03:34,000 --> 00:03:34,559
<b>certo?</b>

83
00:03:34,719 --> 00:03:36,320
<b>Aí quando chega lá embaixo</b>

84
00:03:36,320 --> 00:03:38,480
<b>e começa a formar aquele monte de água lá</b>

85
00:03:38,480 --> 00:03:40,079
<b>a sal-gema, né?</b>

86
00:03:40,079 --> 00:03:42,119
<b>Não, me desculpem, a salmoura...</b>

87
00:03:43,599 --> 00:03:45,840
<b>Aquilo ali é um ambiente fechado ali embaixo</b>

88
00:03:45,840 --> 00:03:46,880
<b>coonfinado</b>

89
00:03:46,880 --> 00:03:48,000
<b>que não tem pra onde ir.</b>

90
00:03:48,000 --> 00:03:49,400
<b>Aí, por onde ela vai?</b>

91
00:03:49,400 --> 00:03:51,840
<b>Pelo tubo pequeno está</b>

92
00:03:51,840 --> 00:03:53,440
<b>entrando a água sob pressão</b>

93
00:03:53,440 --> 00:03:55,400
<b>então tem o tubo grande que está lá.</b>

94
00:03:55,559 --> 00:03:59,880
<b>Aí a sal-gema vem subindo pelo tubo grande.</b>

95
00:04:00,440 --> 00:04:03,559
<b>A sal-gema que eu digo é a salmoura.</b>

96
00:04:03,800 --> 00:04:04,679
<b>Ela sobe</b>

97
00:04:04,679 --> 00:04:06,039
<b>e vai bater na superfície</b>

98
00:04:06,039 --> 00:04:09,480
<b>e da superfície é mandada lá pra indústria.</b>

99
00:04:09,639 --> 00:04:10,360
<b>Entendeu?</b>

100
00:04:10,360 --> 00:04:12,159
<b>Esse é o processo.</b>

101
00:04:13,800 --> 00:04:15,480
<b>Agora, tem um detalhe importante.</b>

102
00:04:15,480 --> 00:04:18,920
<b>E o tamannho desse diâmetro?</b>

103
00:04:18,920 --> 00:04:21,760
<b>Porque isso aí vai circulando e vai</b>

104
00:04:21,760 --> 00:04:23,280
<b>aumentando cada vez mais</b>

105
00:04:23,280 --> 00:04:25,480
<b>e aí quando chega num tamanho</b>

106
00:04:25,480 --> 00:04:26,880
<b>do diametro que eles querem</b>

107
00:04:26,880 --> 00:04:29,159
<b>ao invez de jogar água</b>

108
00:04:29,159 --> 00:04:32,400
<b>no lugar dela vai entrar agora um óleo</b>

109
00:04:32,400 --> 00:04:33,480
<b>um óleo</b>

110
00:04:34,880 --> 00:04:37,119
<b>um óleo sob pressão, também</b>

111
00:04:37,119 --> 00:04:39,679
<b>e esse óleo tem o poder de</b>

112
00:04:39,679 --> 00:04:42,679
<b>chegar naquela parede onde está já...</b>

113
00:04:42,679 --> 00:04:44,960
<b>Onde se parou de dissolver...</b>

114
00:04:44,960 --> 00:04:47,000
<b>Esse óleo vai impermeabilizar aquela parede</b>

115
00:04:47,000 --> 00:04:48,719
<b>pra não permitir mais</b>

116
00:04:48,719 --> 00:04:50,679
<b>que se aumente mais esse diâmetro, entendeu?</b>


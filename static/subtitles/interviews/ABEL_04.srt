1
00:00:00,239 --> 00:00:04,320
O senhor, se recorda do desse dia do terremoto?

2
00:00:04,320 --> 00:00:06,760
O senhor estava em que bairro quando ele aconteceu?

3
00:00:06,880 --> 00:00:08,360
Eu não estava lá não.

4
00:00:08,360 --> 00:00:10,280
eu fui no dia 3 de março

5
00:00:10,280 --> 00:00:13,960
isso aí tá registrado em tudo que é lugar.

6
00:00:13,960 --> 00:00:16,880
Eu tenho inclusive uma cunhada

7
00:00:17,599 --> 00:00:20,480
<b>Irmã da minha esposa, que já faleceu</b>

8
00:00:21,360 --> 00:00:23,719
Que estava lá em cima do olho do furacão.

9
00:00:23,719 --> 00:00:29,320
Ela morava ali bem pertinho do colégio.

10
00:00:30,119 --> 00:00:32,000
<b>Do famoso colégio</b>

11
00:00:33,239 --> 00:00:34,079
<b>do conselho</b>

12
00:00:36,360 --> 00:00:38,920
que é um patrimônio histórico.

13
00:00:38,920 --> 00:00:41,119
Era, não é, porque a Braskem acabou

14
00:00:41,119 --> 00:00:42,119
com o colégio do conselho.

15
00:00:42,119 --> 00:00:44,159
Não só ele mas com outros patrimônios históricos, não é?

16
00:00:44,880 --> 00:00:47,440
Então a minha cunhada morava lá

17
00:00:47,440 --> 00:00:48,679
 e ela me contou.

18
00:00:48,679 --> 00:00:50,599
Não só ela, como centenas de pessoas:

19
00:00:50,599 --> 00:00:51,639
Abel

20
00:00:51,639 --> 00:00:53,480
eu saí correndo dentro de casa

21
00:00:53,480 --> 00:00:54,880
porque foi um estrondo!

22
00:00:54,880 --> 00:00:56,199
pensei que estava acabando tudo

23
00:00:57,039 --> 00:00:59,000
Além daquela região, ali em cima mesmo

24
00:00:59,000 --> 00:01:02,079
todo mundo saiu para a rua, apavorado

25
00:01:02,079 --> 00:01:04,760
pensando que estava caindo tudo!

26
00:01:05,840 --> 00:01:06,360
<b>Foi isso aí.</b>

27
00:01:06,920 --> 00:01:08,199
<b>O terremoto foi tal que</b>

28
00:01:08,760 --> 00:01:11,199
ali em Cruz das Almas

29
00:01:11,199 --> 00:01:13,920
quem conhece Maceió sabe onde é a rua

30
00:01:13,920 --> 00:01:16,440
chama rua do do Paulo César.

31
00:01:17,119 --> 00:01:19,320
Nacasa do Paulo César lá em cima

32
00:01:19,320 --> 00:01:21,320
tem duas torres grandes.

33
00:01:21,320 --> 00:01:23,480
Essas duas torres balançaram tanto

34
00:01:23,480 --> 00:01:24,559
que todo mundo de lá...

35
00:01:24,559 --> 00:01:26,519
E eu estou falando em Cruz das Almas!

36
00:01:26,519 --> 00:01:29,280
Elas balançaram tanto que o povo de lá

37
00:01:29,280 --> 00:01:31,719
todo mundo saiu dos pés de assustado

38
00:01:31,719 --> 00:01:32,880
e chamaram a defesa civil.

39
00:01:32,880 --> 00:01:34,639
A defesa civil foi lá então está tudo registrado.


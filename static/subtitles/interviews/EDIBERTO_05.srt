1
00:00:00,039 --> 00:00:02,280
<b>Maceió é uma laguna.</b>

2
00:00:02,280 --> 00:00:03,920
<b>É uma restinga.</b>

3
00:00:05,039 --> 00:00:07,239
<b>O que tem de enteder é o seguinte:</b>

4
00:00:07,239 --> 00:00:09,880
<b> Você imagina há 1000</b>

5
00:00:09,880 --> 00:00:11,159
<b>2000 anos?</b>

6
00:00:11,159 --> 00:00:14,840
<b> Maceió é o rio Mundaú</b>

7
00:00:16,039 --> 00:00:17,039
<b>chegando ao mar.</b>

8
00:00:17,920 --> 00:00:19,960
Entre barreiras.

9
00:00:19,960 --> 00:00:23,679
Ele vem pelo leito do rio Mundaú

10
00:00:23,679 --> 00:00:25,800
passa ali em Satuba e vem o rio

11
00:00:25,800 --> 00:00:29,840
vem colocar sua água aqui no Oceano Atlântico.

12
00:00:31,079 --> 00:00:32,079
Com o passar do tempo

13
00:00:32,079 --> 00:00:34,679
esse rio, ele vai criando a sua boca

14
00:00:34,679 --> 00:00:35,920
a sua foz.

15
00:00:35,920 --> 00:00:37,280
Ele vai abrindo

16
00:00:37,280 --> 00:00:40,800
e ele vai derrubando barreiras à esquerda e à direita.

17
00:00:41,960 --> 00:00:44,800
De acordo com a corrente de água, ele vai abrindo a foz.

18
00:00:45,599 --> 00:00:47,239
Mas há um momento em que

19
00:00:47,239 --> 00:00:49,760
quando começa a diminuir o volume de água

20
00:00:49,760 --> 00:00:53,280
<b>que vinha das cabeceiras desses rios todos</b>

21
00:00:55,159 --> 00:00:56,440
o material tufosos

22
00:00:56,440 --> 00:00:58,760
que é trazido por algum momento no inverno

23
00:00:59,119 --> 00:01:00,400
<b>ele vai se depositando.</b>

24
00:01:01,280 --> 00:01:03,280
Então, à medida que ele vai se depositando

25
00:01:03,280 --> 00:01:08,119
e o vento leste vai trazendo do mar,  da praia

26
00:01:08,119 --> 00:01:09,920
<b>a areia</b>

27
00:01:09,920 --> 00:01:13,599
isso vai se consolidando numa mistura

28
00:01:13,599 --> 00:01:16,119
na área mais litorânea.

29
00:01:16,119 --> 00:01:17,559
Na área mais interna

30
00:01:17,559 --> 00:01:20,400
<b>é o barro da barreira que cai em cima da lama.</b>

31
00:01:21,199 --> 00:01:23,159
Então essa formação

32
00:01:23,159 --> 00:01:25,360
ela vai crescendo em direção

33
00:01:25,360 --> 00:01:28,119
ao que hoje é o Pontal da Barra

34
00:01:28,119 --> 00:01:30,159
<b>e vai fechando a foz</b>

35
00:01:30,159 --> 00:01:31,119
<b>e forma a lagoa.</b>

36
00:01:31,119 --> 00:01:33,800
Então a lagoa é uma foz fechada

37
00:01:33,800 --> 00:01:37,079
com uma vagazinha saindo lá na Barra Nova agora

38
00:01:37,079 --> 00:01:39,199
<b>porque o volume de água diminuiu.</b>

39
00:01:40,239 --> 00:01:42,840
Então essa essa formação é

40
00:01:42,840 --> 00:01:46,719
próximo do litoral, as dunas

41
00:01:46,719 --> 00:01:48,719
formadas pelo movimento de areia

42
00:01:48,719 --> 00:01:51,360
Quem sabe a engenharia

43
00:01:51,360 --> 00:01:52,760
entende que essa areia da praia

44
00:01:52,760 --> 00:01:54,480
ela tem uma compactação excelente.

45
00:01:54,480 --> 00:01:57,239
 Então ela se pode observar o seguinte

46
00:01:57,239 --> 00:02:00,199
você encontra prédios construídos no Sobral

47
00:02:01,000 --> 00:02:02,800
<b>porque tem areia.</b>

48
00:02:03,360 --> 00:02:04,880
Agora vai na Ponta Grossa

49
00:02:04,880 --> 00:02:06,840
enquanto um prédio com três andares.

50
00:02:08,480 --> 00:02:09,599
Sabe por que não tem?

51
00:02:09,599 --> 00:02:10,800
Não tem!

52
00:02:10,800 --> 00:02:13,159
Nenhum de vocês, eu tenho certeza, percebeu isso.

53
00:02:14,239 --> 00:02:16,039
<b>Não é difícil na Ponta Grossa</b>

54
00:02:16,039 --> 00:02:17,800
Vergel, Coreia

55
00:02:17,800 --> 00:02:19,440
Por que se construir afunda.

56
00:02:19,440 --> 00:02:20,639
Vai embora.

57
00:02:21,679 --> 00:02:23,480
<b>Quando eles estavam fazendo</b>

58
00:02:25,679 --> 00:02:27,079
<b>a  via expressa</b>

59
00:02:27,079 --> 00:02:29,119
<b>o aterro ali, como é que chama?</b>

60
00:02:29,920 --> 00:02:31,239
O dique-estrada.

61
00:02:31,519 --> 00:02:33,239
 Era um dique, porque a lagoa invadia.

62
00:02:33,480 --> 00:02:34,400
Quando eles cavaram ali

63
00:02:34,400 --> 00:02:36,599
teve um dia que uma máquina ia descendo e indo embora.

64
00:02:36,599 --> 00:02:38,360
<b>Ela bateu na lama mais mole.</b>

65
00:02:38,840 --> 00:02:40,920
<b>Tiveram que guinchar ela com outra máquina.</b>

66
00:02:40,920 --> 00:02:42,400
Mas ela ia embora.

67
00:02:42,880 --> 00:02:44,199
Se você cavar....

68
00:02:44,199 --> 00:02:45,239
Na Ponta Grossa

69
00:02:45,239 --> 00:02:47,079
se você cavar no inverno um metro

70
00:02:47,079 --> 00:02:48,440
dá água!

71
00:02:48,440 --> 00:02:50,320
As fossas

72
00:02:50,320 --> 00:02:52,000
elas ficam eternamente

73
00:02:52,000 --> 00:02:54,199
<b>com a metade de água na Ponta Grossa.</b>

74
00:02:55,039 --> 00:02:57,360
Eu fui morador da Ponta Grossa durante 30 anos.

75
00:02:57,360 --> 00:02:59,599
Então aquela região dali

76
00:02:59,599 --> 00:03:01,800
é  região do material tufoso.

77
00:03:01,800 --> 00:03:04,239
E a região mais litorânea

78
00:03:04,239 --> 00:03:06,559
é a que conseguiu compactar melhor

79
00:03:06,559 --> 00:03:08,320
<b>então você tem as dunas.</b>

80
00:03:08,320 --> 00:03:10,440
Então o Pontal da Barra e o Trapiche

81
00:03:10,440 --> 00:03:14,639
eram parte dessa área mais litorânea

82
00:03:14,639 --> 00:03:16,440
mas da areia

83
00:03:16,440 --> 00:03:19,360
 então ali predominavam os coqueirais.

84
00:03:20,360 --> 00:03:22,480
Os coqueiros iam até lá no final.

85
00:03:22,480 --> 00:03:25,000
Mais para dentro, em direção à lagoa

86
00:03:25,000 --> 00:03:28,039
<b>você tem essa vegetação de margem de lagoa, né?</b>

87
00:03:28,800 --> 00:03:29,480
Essa...

88
00:03:31,079 --> 00:03:33,719
Há algumas amendoeiras, algumas coisa desse tipo

89
00:03:33,719 --> 00:03:37,039
mas não, não tinha grandes matas.

90
00:03:37,039 --> 00:03:39,239
Ali nunca existiu grande mata

91
00:03:39,239 --> 00:03:41,559
porque o tipo de solo

92
00:03:41,559 --> 00:03:43,800
da parte mais interna não suporta e o

93
00:03:43,800 --> 00:03:47,280
<b>e o do litoral predominou o coqueiro.</b>

94
00:03:48,239 --> 00:03:50,280
A rua que eu morei, a rua Guaicurus

95
00:03:50,280 --> 00:03:52,400
e a Rua da Assembleia

96
00:03:52,400 --> 00:03:57,360
nesse período de 67, 68

97
00:03:57,360 --> 00:04:00,400
<b>foi aterrada com as areias das dunas.</b>

98
00:04:00,400 --> 00:04:02,000
Então essas areias das dunas serviram

99
00:04:02,000 --> 00:04:04,199
para muitos aterros e foram sendo retiradas.

100
00:04:04,519 --> 00:04:06,760
Sobraram as as dunas do Pontal.

101
00:04:07,320 --> 00:04:08,960
As dunas do Sobral

102
00:04:08,960 --> 00:04:11,960
<b>elas foram todas eliminadas.</b>

103
00:04:12,239 --> 00:04:14,719
É importante detectar essa formação dessa

104
00:04:14,719 --> 00:04:18,559
dessa origem do rio Mundaú, derrubando barreiras

105
00:04:19,119 --> 00:04:21,239
porque toda aquela barreira de Bebedouro

106
00:04:21,239 --> 00:04:22,920
de Mutange...

107
00:04:22,920 --> 00:04:25,400
Cambona, Mutange e Bebedouro.

108
00:04:26,239 --> 00:04:27,559
A cabeça daquela barreira

109
00:04:27,559 --> 00:04:29,559
ela já caiu várias vezes aqui né?

110
00:04:29,559 --> 00:04:30,960
Durante a história né?

111
00:04:30,960 --> 00:04:32,280
Ela era bem mais adiantada

112
00:04:32,280 --> 00:04:33,880
imagine no outro lado, de Coqueiro Seco

113
00:04:33,880 --> 00:04:34,920
e Santa Luzia do Norte.

114
00:04:35,119 --> 00:04:36,400
Essa que perdeu mesmo

115
00:04:36,400 --> 00:04:39,280
vinha bem mais pra cá.

116
00:04:39,800 --> 00:04:41,800
<b>Então ela foi se distanciando.</b>

117
00:04:43,920 --> 00:04:46,079
É exatamente porque o rio, ele tende

118
00:04:46,079 --> 00:04:48,800
a pegar a corrente marítima litorânea

119
00:04:48,800 --> 00:04:51,920
ela é Norte-Sul, descendo.

120
00:04:51,920 --> 00:04:55,599
Então, quando o rio vai que ele sai no mar, ele vai sendo

121
00:04:55,599 --> 00:04:57,119
<b>ele vai sendo puxado pra direita</b>

122
00:04:57,119 --> 00:04:58,079
<b>sempre pra direita.</b>

123
00:04:58,519 --> 00:04:59,480
Pra vocês terem uma ideia

124
00:04:59,480 --> 00:05:00,480
um dos primeiros

125
00:05:00,480 --> 00:05:02,960
um dos canais que sobrou depois dessa

126
00:05:04,639 --> 00:05:07,360
dessa compactação de Maceió

127
00:05:07,360 --> 00:05:09,880
ele era o canal da levada que vocês estão vendo hoje ainda.

128
00:05:09,880 --> 00:05:11,000
ainda o restinho que está lá né?

129
00:05:11,000 --> 00:05:12,239
Hoje é um rio com esgoto.

130
00:05:12,679 --> 00:05:15,280
Mas aquele canal, ele passava pela Praça do Pirulito

131
00:05:15,280 --> 00:05:16,199
e ia sair na praia.

132
00:05:17,480 --> 00:05:18,800
No final da Dias Cabral.

133
00:05:19,320 --> 00:05:20,760
Aquilo era um canal

134
00:05:21,679 --> 00:05:22,480
que fechou.

135
00:05:23,599 --> 00:05:25,800
Aí foi fechando, eles foram aterrando

136
00:05:25,800 --> 00:05:29,239
<b>dura até meados do século 19</b>

137
00:05:29,239 --> 00:05:31,159
<b>aquilo ali era um charco.</b>

138
00:05:31,840 --> 00:05:32,920
<b>Lama</b>

139
00:05:33,519 --> 00:05:35,159
<b>E depois eles foram aterrando.</b>

140
00:05:36,079 --> 00:05:36,599
Então

141
00:05:37,519 --> 00:05:39,320
essa formação de Maceió

142
00:05:39,320 --> 00:05:42,320
de cair, barreira de acomodação de solo...

143
00:05:42,760 --> 00:05:44,039
<b>Quando eu fiz a postagem</b>

144
00:05:45,719 --> 00:05:48,360
sobre a salgema

145
00:05:48,360 --> 00:05:51,519
uma pessoa de São Paulo que disse:

146
00:05:51,519 --> 00:05:54,400
eu fui morador de Bebedouro

147
00:05:54,400 --> 00:05:55,440
próximo da ponte.

148
00:05:56,119 --> 00:05:58,920
E quando eu saí em 1960  ou em torno disso

149
00:05:58,920 --> 00:06:00,400
não me lembro exatamente a data.

150
00:06:02,000 --> 00:06:05,599
a casa do meu pai ia afundando e afundando na lama

151
00:06:05,599 --> 00:06:08,440
e, assim, a gente ia reconstruindo ela pra cima, né?

152
00:06:08,440 --> 00:06:10,519
Como acontece na Brejal hoje!

153
00:06:10,519 --> 00:06:13,880
Na Brejal esse fenômeno permanece,

154
00:06:13,880 --> 00:06:16,639
então essa região abaixo, ali é toda tufosa.

155
00:06:17,239 --> 00:06:18,719
<b>Então acomodação é fácil.</b>

156
00:06:19,320 --> 00:06:20,280
<b>Qualquer peso em cima...</b>

157
00:06:20,280 --> 00:06:21,119
<b>Você bota...</b>

158
00:06:21,119 --> 00:06:22,599
<b>O dique-estrada tem um peso</b>

159
00:06:22,599 --> 00:06:24,400
<b>O dique-estrada  quando afunda</b>

160
00:06:24,400 --> 00:06:25,519
<b>as laterais sobem.</b>

161
00:06:26,960 --> 00:06:28,519
<b>São fenômenos...</b>

162
00:06:29,400 --> 00:06:31,320
<b>Ou seja, Maceió</b>

163
00:06:31,320 --> 00:06:32,480
<b>uma parte de Maceió</b>

164
00:06:33,159 --> 00:06:34,159
flutua.

165
00:06:34,159 --> 00:06:37,039
É uma jangada em cima de uma lama mole.

166
00:06:37,639 --> 00:06:39,719
Imagine mais ou menos essa figura.

167
00:06:39,719 --> 00:06:41,480
Essa parte de em direção à lagoa

168
00:06:41,480 --> 00:06:43,199
<b>a parte mais em direção ao mar, não.</b>

169
00:06:43,199 --> 00:06:45,039
<b>Essa está bem mais consolidada.</b>

170
00:06:45,679 --> 00:06:48,079
Então, compreender esses movimentos

171
00:06:48,079 --> 00:06:51,559
também ajuda a entender alguns fenômenos anteriores

172
00:06:51,559 --> 00:06:54,119
em torno dessa barreira dali

173
00:06:54,119 --> 00:06:56,480
de Mutange, da Cambona...

174
00:06:57,480 --> 00:06:59,079
Ela naturalmente

175
00:06:59,079 --> 00:07:02,800
se não houvesse cavernas, se não houvesse salgema

176
00:07:02,800 --> 00:07:04,559
com o processo de erosão...

177
00:07:04,559 --> 00:07:06,199
Por exemplo: a Gruta do Padre.

178
00:07:06,199 --> 00:07:09,039
Os mais antigos dizem o seguinte

179
00:07:09,039 --> 00:07:11,119
que ali no caldinho do Vieira

180
00:07:11,119 --> 00:07:12,559
ali tinha uma lagoa no inverno

181
00:07:12,559 --> 00:07:14,760
formava uma lagoa

182
00:07:14,760 --> 00:07:17,000
e ela descia em direção ao CEPA

183
00:07:17,000 --> 00:07:19,920
e do CEPA ela descia em direção à barreira

184
00:07:19,920 --> 00:07:21,639
e foi rasgando a barreira

185
00:07:21,639 --> 00:07:22,920
 formou a Gruta do Padre.

186
00:07:22,920 --> 00:07:24,920
Isso era um movimento de águas de inverno.

187
00:07:25,519 --> 00:07:27,039
<b>Você imagina essa água infiltrando?</b>

188
00:07:27,719 --> 00:07:31,519
Ela vai tirando o caulim da argila e do barro

189
00:07:31,519 --> 00:07:33,000
que é quem dá a liga

190
00:07:33,000 --> 00:07:34,840
e vai ficando uma areia.

191
00:07:35,639 --> 00:07:38,000
Imagine milhares e milhares
de anos.

192
00:07:38,000 --> 00:07:39,519
São fenômenos naturais

193
00:07:39,519 --> 00:07:42,280
ue demoram séculos e séculos para acontecer.

194
00:07:42,760 --> 00:07:45,760
<b>Mas há acomodações naturais desse processo.</b>

195
00:07:46,440 --> 00:07:49,800
Então, se já tinha essa possibilidade

196
00:07:49,800 --> 00:07:51,440
imagine com cavernas embaixo.

197
00:07:52,079 --> 00:07:53,639
<b>Ideia sensacional, né?</b>

198
00:07:53,960 --> 00:07:54,840
Cavar e...

199
00:07:54,840 --> 00:07:57,760
E ainda há uma falha numa placa tectônica.

200
00:07:58,360 --> 00:07:59,679
Então, eu acho que ali...

201
00:07:59,679 --> 00:08:00,679
Eu não sou técnico

202
00:08:00,679 --> 00:08:02,840
mas são três fenômenos atuando.

203
00:08:03,760 --> 00:08:05,000
<b>A falha tectônica</b>

204
00:08:05,360 --> 00:08:06,119
as cavernas

205
00:08:06,519 --> 00:08:09,639
As cavernas e essa cabeça de barreira

206
00:08:09,639 --> 00:08:11,159
que historicamente ela vai...

207
00:08:11,159 --> 00:08:13,119
Os lençois freáticos vão aflorando

208
00:08:13,119 --> 00:08:14,960
na lateral da barreira

209
00:08:14,960 --> 00:08:16,479
e levando o material.

210
00:08:16,479 --> 00:08:18,439
A bebedouro tinha um valor

211
00:08:18,439 --> 00:08:19,560
porque que era fonte de água potável.

212
00:08:20,000 --> 00:08:22,399
As pessoas de Maceió beberam muitos anos

213
00:08:22,399 --> 00:08:23,479
muitas décadas

214
00:08:23,840 --> 00:08:26,920
a água que os carroceiros iam buscar

215
00:08:26,920 --> 00:08:30,840
nas fontes que saíam ali no Mutange, na Cambona

216
00:08:32,159 --> 00:08:34,079
<b>era fonte de água. </b>

217
00:08:34,079 --> 00:08:35,720
<b>no Bebedouro</b>


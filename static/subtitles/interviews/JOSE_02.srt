1
00:00:00,400 --> 00:00:03,719
Eu cheguei a ser nomeado

2
00:00:03,719 --> 00:00:08,840
como técnico em  planejamento.

3
00:00:09,320 --> 00:00:13,400
Foi a maneira que o professor José de Melo Gomes encontrou

4
00:00:13,400 --> 00:00:15,880
inclusive de me remunerar

5
00:00:16,519 --> 00:00:18,599
<b>porque eu passei esse tempo todo</b>

6
00:00:18,639 --> 00:00:21,119
O governador  Suruagy já tinha me dito:

7
00:00:21,119 --> 00:00:23,079
Olha, outro problema é esse

8
00:00:23,079 --> 00:00:25,880
<b>eu não posso lhe remunerar.</b>

9
00:00:27,880 --> 00:00:28,920
E eu aceitei.

10
00:00:28,920 --> 00:00:32,639
Aí o professor José de Melo Gomes encontrou essa via.

11
00:00:33,199 --> 00:00:37,400
Então eu fui enquadrado como técnico em planejamento

12
00:00:37,400 --> 00:00:41,760
na Secretaria do Planejamento do Estado.

13
00:00:41,760 --> 00:00:47,360
Nunca houve uma nomeação oficial

14
00:00:47,360 --> 00:00:50,400
para que eu respondesse

15
00:00:50,400 --> 00:00:53,599
<b>por nada no meio ambiente, não é?</b>

16
00:00:54,440 --> 00:00:56,679
E na realidade, isso criou um impasse

17
00:00:56,679 --> 00:01:01,320
que foi um impasse para o governador Suruagy

18
00:01:01,320 --> 00:01:04,519
<b>como ele não tinha me nomeado</b>

19
00:01:05,400 --> 00:01:07,760
<b>ele não podia me demitir.</b>

20
00:01:08,639 --> 00:01:10,559
<b>Vocês estão entendendo?</b>

21
00:01:11,760 --> 00:01:14,960
Então, enquanto ele não podia me demitir

22
00:01:14,960 --> 00:01:19,039
eu simplesmente ficava respondendo de fato.

23
00:01:19,719 --> 00:01:24,079
<b>Eu tinha documentos, tenho documentos.</b>

24
00:01:24,480 --> 00:01:27,440
Ele encaminhando para mim oficialmente

25
00:01:27,440 --> 00:01:28,880
com a assinatura dele

26
00:01:28,880 --> 00:01:30,280
nunca tinha me nomeado

27
00:01:30,280 --> 00:01:33,000
mas encaminhando com a assinatura dele.

28
00:01:33,000 --> 00:01:37,199
E documentos dirigidos a mim

29
00:01:37,199 --> 00:01:40,400
<b>na qualidade de Secretário de Controle da Poluição.</b>

30
00:01:40,880 --> 00:01:44,320
Então eu estava perfeitamente coberto

31
00:01:44,320 --> 00:01:45,760
legalmente coberto.

32
00:01:46,440 --> 00:01:48,800
Isso foi o que me deu o poder de resistência

33
00:01:48,800 --> 00:01:50,519
que as pessoas perguntam:

34
00:01:50,519 --> 00:01:54,320
Mas como é que você resistiu àquelas pressões todas?

35
00:01:54,320 --> 00:01:59,320
Ao assédio moral violento que essa algema patrocinou?

36
00:01:59,840 --> 00:02:01,119
<b>Eu digo: Olha, gente, eu...</b>

37
00:02:02,559 --> 00:02:03,519
<b>Estava coberto.</b>


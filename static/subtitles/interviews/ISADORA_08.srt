1
00:00:00,239 --> 00:00:02,400
<b>Uma coisa interessante pra gente acompanhar</b>

2
00:00:02,400 --> 00:00:05,840
<b>são as compensaões de direitos difusos.</b>

3
00:00:05,840 --> 00:00:07,440
<b>Então, por exemplo</b>

4
00:00:07,440 --> 00:00:10,679
<b>hoje existe um comitê estabelecido</b>

5
00:00:10,679 --> 00:00:12,320
<b>pelo Ministério Público Federal</b>

6
00:00:12,320 --> 00:00:14,280
<b>para trabalhar essa questão.</b>

7
00:00:14,280 --> 00:00:16,920
<b>Já há uma previsão de recursos para isso.</b>

8
00:00:16,920 --> 00:00:19,599
<b>Então como vai ser utilizado esse recurso?</b>

9
00:00:19,599 --> 00:00:21,719
<b>E aí entra uma questão muito importante</b>

10
00:00:21,719 --> 00:00:26,400
<b>porque assim, existe o dano para a cidade</b>

11
00:00:26,400 --> 00:00:27,960
<b>e isso realmente tem que passar</b>

12
00:00:27,960 --> 00:00:30,440
<b>pelo poder público municipal</b>

13
00:00:30,480 --> 00:00:31,400
<b>mas tem que passar</b>

14
00:00:31,400 --> 00:00:32,920
<b>pelo poder público municipal</b>

15
00:00:32,920 --> 00:00:34,880
<b>dentro de um contexto mais amplo</b>

16
00:00:34,880 --> 00:00:36,719
<b>que é o que na cidade</b>

17
00:00:36,719 --> 00:00:38,639
<b>é importante para todo mundo?</b>

18
00:00:39,239 --> 00:00:40,840
<b>Mas aos moradores</b>

19
00:00:40,840 --> 00:00:42,559
<b>existem danos concretos.</b>

20
00:00:43,760 --> 00:00:46,039
<b>Vou dar um exemplo bem bem por alto:</b>

21
00:00:47,000 --> 00:00:47,880
<b>O quintal cultural.</b>

22
00:00:47,880 --> 00:00:50,880
<b>O quintal cultural, é uma instituição cultural</b>

23
00:00:50,880 --> 00:00:55,840
<b>que existe a 15 anos, mais ou menos, na cidade</b>

24
00:00:55,840 --> 00:00:59,400
<b>com atuação reconhecida por todo mundo.</b>

25
00:00:59,400 --> 00:01:04,280
<b>Uma atuação num âmbito coletivo da cultura.</b>

26
00:01:04,280 --> 00:01:06,159
<b>Uma atuação que é reconhecida</b>

27
00:01:06,159 --> 00:01:07,840
<b>como entidade pública, inclusive</b>

28
00:01:07,840 --> 00:01:09,320
<b>já há muitos anos</b>

29
00:01:09,320 --> 00:01:12,519
<b>e é uma instituição que foi marcada.</b>

30
00:01:12,519 --> 00:01:15,760
<b>Está lá marcada como talvez tendo que sair</b>

31
00:01:15,760 --> 00:01:17,800
<b>da sua sede no Bom-Parto.</b>

32
00:01:20,360 --> 00:01:21,920
<b>O quintal cultural é uma instituição</b>

33
00:01:21,920 --> 00:01:23,679
<b>que, em termos de direitos difusos</b>

34
00:01:23,679 --> 00:01:25,440
<b>trabalha com direito à cultura</b>

35
00:01:25,440 --> 00:01:27,639
<b>na região onde está.</b>

36
00:01:28,239 --> 00:01:31,840
<b>Inclusive, hoje, trabalha com direitos humanos</b>

37
00:01:31,840 --> 00:01:33,599
<b>fazendo um trabalho de prevenção à violência</b>

38
00:01:33,599 --> 00:01:35,079
<b>trabalha com crianças</b>

39
00:01:35,079 --> 00:01:38,199
<b>e hoje faz um trabalho até maior do que isso</b>

40
00:01:38,199 --> 00:01:41,320
<b>porque hoje representa a CUFA no estado</b>

41
00:01:41,320 --> 00:01:44,519
<b>e, portanto, trabalha com Maceió inteira</b>

42
00:01:44,519 --> 00:01:46,880
<b>trabalha com favelas de Maceió inteiro</b>

43
00:01:46,880 --> 00:01:48,599
<b>com favelas de outros outros municípios.</b>

44
00:01:50,559 --> 00:01:52,679
<b>Então há a saída o quintal dalí</b>

45
00:01:52,679 --> 00:01:55,440
<b>o quintal está tendo que se mudar dalí.</b>

46
00:01:56,599 --> 00:01:57,599
<b>Ele tem que ser compensado</b>

47
00:01:57,599 --> 00:01:59,400
<b>e não é só pela perda da sede.</b>

48
00:02:00,159 --> 00:02:02,079
<b>Não é só pela perda patrimonial</b>

49
00:02:02,079 --> 00:02:03,440
<b>que seria a indenização.</b>

50
00:02:03,440 --> 00:02:08,519
<b>Não é só pela questão do direito patrimonial</b>

51
00:02:08,519 --> 00:02:09,920
<b>do quintal em si.</b>

52
00:02:09,920 --> 00:02:12,599
<b>Mas é pelo direito difuso, o direito cultural.</b>

53
00:02:12,599 --> 00:02:14,519
<b>Então, como é que você compensa</b>

54
00:02:14,519 --> 00:02:16,480
<b>a retirada de uma instituição cultural</b>

55
00:02:16,480 --> 00:02:18,000
<b>de uma área onde ela atua</b>

56
00:02:18,000 --> 00:02:20,280
<b>onde ela faz um papel primordial</b>

57
00:02:20,280 --> 00:02:21,440
<b>de prevenção à violência?</b>

58
00:02:21,440 --> 00:02:24,840
<b>À perda disso numa região</b>

59
00:02:24,840 --> 00:02:26,400
<b>como é que você compensa isso?</b>

60
00:02:27,880 --> 00:02:29,519
<b>Então esse é um direito difuso</b>

61
00:02:29,519 --> 00:02:30,480
<b>é um delito cultural</b>

62
00:02:30,480 --> 00:02:32,960
<b>é um direito humano a ser compensado.</b>

63
00:02:34,199 --> 00:02:38,119
<b>Então como é que a empresa vai atuar nisso?</b>

64
00:02:38,119 --> 00:02:40,880
<b>Como é que esse comitê vai direcionar</b>

65
00:02:40,880 --> 00:02:44,159
<b>os recursos que estão previstos para isso?</b>

66
00:02:44,840 --> 00:02:46,039
<b>Para essa atuação?</b>

67
00:02:46,039 --> 00:02:49,159
<b>Quem tem que definir essa escuta?</b>

68
00:02:50,000 --> 00:02:51,960
<b>Porque nesse caso, então, nós estamos falando</b>

69
00:02:51,960 --> 00:02:53,960
<b>de algo que afeta diretamente</b>

70
00:02:53,960 --> 00:02:55,920
<b>as áreas que vão ser desocupadas.</b>

71
00:02:55,920 --> 00:02:59,000
<b>É uma perda para as áreas</b>

72
00:02:59,000 --> 00:03:02,000
<b>então quem tem que dizer como esse recurso</b>

73
00:03:02,000 --> 00:03:04,960
<b>nas áreas tem que ser investido</b>

74
00:03:04,960 --> 00:03:07,199
<b>é quem tá sofrendo a perda.</b>

75
00:03:08,440 --> 00:03:09,519
<b>E aí, nesse caso</b>

76
00:03:09,519 --> 00:03:10,960
<b>como vai ser a escuta?</b>

77
00:03:10,960 --> 00:03:14,920
<b>como vai se estabelecar esse canal de escuta?</b>

78
00:03:14,920 --> 00:03:17,079
<b>como vai ser estabelecido com a população</b>

79
00:03:17,079 --> 00:03:19,360
<b>a participação direta</b>

80
00:03:19,360 --> 00:03:23,039
<b>em relação ao emprego desse recurso?</b>

81
00:03:23,880 --> 00:03:25,039
<b> Essa é uma questão.</b>

82
00:03:25,039 --> 00:03:27,320
<b>E é uma questão que na minha opinião</b>

83
00:03:27,320 --> 00:03:28,800
<b>não vem sendo tratada com cuidado.</b>

84
00:03:29,599 --> 00:03:31,599
<b>Eu não vou falar das organizações em si</b>

85
00:03:31,599 --> 00:03:33,079
<b>porque eu não me sinto muito capaz.</b>

86
00:03:33,079 --> 00:03:34,599
<b>Seria melhor se alguém realmente</b>

87
00:03:34,599 --> 00:03:35,679
<b>pudesse acompanhar isso de perto</b>

88
00:03:35,679 --> 00:03:37,519
<b>mas com relação ao direito difuso</b>

89
00:03:37,519 --> 00:03:38,760
<b>isso eu posso falar.</b>

90
00:03:38,760 --> 00:03:40,480
<b>Ao direito difuso, ao direito cultural</b>

91
00:03:40,480 --> 00:03:43,480
<b>ao direito ambiental, aos direitos humanos</b>

92
00:03:43,480 --> 00:03:44,960
<b>Isso, na minha opinião</b>

93
00:03:44,960 --> 00:03:46,559
<b>não vem sendo bem considerado.</b>


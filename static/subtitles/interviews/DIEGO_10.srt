1
00:00:00,333 --> 00:00:04,254
Eu acho que as três coisas.

2
00:00:05,088 --> 00:00:08,258
Uma coisa não anula outra
porque é uma tragédia

3
00:00:08,258 --> 00:00:10,844
porque é... vidas que...

4
00:00:12,095 --> 00:00:14,431
laços que foram perdidos,
vidas que foram perdidas

5
00:00:14,431 --> 00:00:17,642
já têm casos de vidas perdidas
por conta disso.

6
00:00:17,851 --> 00:00:23,440
Você tem uma ausência de monitoramento

7
00:00:23,440 --> 00:00:26,943
e controle o que permitiu
que essa situação ocorresse.

8
00:00:27,402 --> 00:00:29,904
Isso pode ser tipificado. Pode.

9
00:00:30,530 --> 00:00:33,575
Não sei se vocês vão conversar
com alguém do direito especificamente.

10
00:00:34,075 --> 00:00:37,245
Mas esses pontos que eu levantei

11
00:00:37,245 --> 00:00:39,956
que você precisa e principalmente

12
00:00:40,957 --> 00:00:45,378
porque a alegação era: mas é um...

13
00:00:46,046 --> 00:00:49,799
digamos que a operação...
não digamos, ela vem de antes

14
00:00:51,009 --> 00:00:53,470
da Constituição de 88

15
00:00:53,470 --> 00:00:56,514
de uma série de normativos
que vão orientar por exemplo:

16
00:00:56,514 --> 00:00:58,391
o licenciamento ambiental, etc

17
00:00:58,391 --> 00:00:59,809
Mas isso não exime.

18
00:01:00,685 --> 00:01:04,189
isso não exime, porque a atividade econômica
ela continua a existir.

19
00:01:04,773 --> 00:01:06,900
Então você precisa se adequar.

20
00:01:06,900 --> 00:01:08,526
Eu acho que está aí

21
00:01:09,694 --> 00:01:15,116
a busca que foi da assessoria jurídica
da Braskem para fazer os acordos.

22
00:01:16,076 --> 00:01:18,536
Porque disse assim: olha
a gente deveria ter feito isso

23
00:01:18,536 --> 00:01:21,456
e o Relatório de Impacto Ambiental...

24
00:01:21,456 --> 00:01:24,876
porque são várias atividades,
então tinha como ter mais de uma licença.

25
00:01:25,627 --> 00:01:30,507
Mesmo que você seja generoso
do ponto de vista de desburocratizar

26
00:01:31,466 --> 00:01:34,177
pra cá, como atividade a mesma

27
00:01:34,677 --> 00:01:39,724
não importa se vai ter
3, 4, 5, 6, 7 pontos de extração. Não interessa

28
00:01:40,350 --> 00:01:42,769
mas você tem que ter um relatório
de impacto ambiental

29
00:01:43,561 --> 00:01:45,939
que diga respeito a que? À extração

30
00:01:46,397 --> 00:01:48,983
identificando os pontos obviamente.

31
00:01:48,983 --> 00:01:53,279
Agora, o que ocorre?
Você não tem documento

32
00:01:53,279 --> 00:01:57,075
e principalmente uma convocação
depois que você

33
00:01:57,408 --> 00:02:01,121
o risco começa a ser identificado
para conversar com a população.

34
00:02:01,412 --> 00:02:05,625
Então, sim, eu acho que não é
minha alçada, o direito, mas eu vejo

35
00:02:05,667 --> 00:02:08,795
sim um ponto de vista críme
e também vejo desastre porque

36
00:02:08,795 --> 00:02:13,383
como eu disse pra vocês não é só ali
naquela área diretamente afetada

37
00:02:14,217 --> 00:02:17,178
você tem áreas que vão ser afetadas também

38
00:02:17,178 --> 00:02:21,182
de influência direta
e que vão afetar. É o aluguel que vai afetar

39
00:02:22,392 --> 00:02:22,976
vidas

40
00:02:22,976 --> 00:02:26,354
Como eu disse por exemplo
pessoas que vão desenvolver

41
00:02:26,354 --> 00:02:29,607
extrassolares ou desenvolver

42
00:02:29,899 --> 00:02:32,652
patologias que não existiam.

43
00:02:33,111 --> 00:02:36,114
Então você tem
um caráter de desastre humanitário mesmo.

44
00:02:37,448 --> 00:02:40,368
Então acho que esses três,
 essas três categorias.

45
00:02:40,368 --> 00:02:44,038
Elas se somam à perversidade da situação.


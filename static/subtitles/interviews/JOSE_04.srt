1
00:00:00,760 --> 00:00:04,639
<b>Ali era um campo de dunas.</b>

2
00:00:05,960 --> 00:00:11,880
<b>Um dos campos de dunas mais belos do país.</b>

3
00:00:13,360 --> 00:00:16,400
Nós temos ele registrado ainda muito pouco

4
00:00:16,400 --> 00:00:20,400
<b>no livro de Ecologia de Mário Guimarães Ferri. </b>

5
00:00:20,400 --> 00:00:26,239
<b>Eram dunas muito altas, muito belas.</b>

6
00:00:26,239 --> 00:00:29,320
Era uma região realmente muito bonita,

7
00:00:29,320 --> 00:00:30,519
aquela dali

8
00:00:31,559 --> 00:00:34,639
e como o bioma: Restinga.

9
00:00:35,800 --> 00:00:38,519
<b>Ali é uma ponta de restinga.</b>

10
00:00:39,480 --> 00:00:43,079
<b>Por princípio em ciência ambiental.</b>

11
00:00:43,079 --> 00:00:47,440
<b>ponta de restinga é frágil.</b>

12
00:00:48,239 --> 00:00:50,639
Logo, não se presta

13
00:00:50,639 --> 00:00:54,559
pra implantação de indústria desse porte.
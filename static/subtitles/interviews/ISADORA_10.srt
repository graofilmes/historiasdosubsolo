1
00:00:00,800 --> 00:00:02,840
<b>Bom, tem várias questões aí.</b>

2
00:00:02,840 --> 00:00:05,960
<b>Primeiro que isso impacta o planejamento da cidade.</b>

3
00:00:05,960 --> 00:00:07,320
<b>Você vai ter de repente</b>

4
00:00:07,320 --> 00:00:11,119
<b>toda uma população deslocada parta outras áreas</b>

5
00:00:11,119 --> 00:00:14,199
<b>e essas áreas vão receber uma sobrecarga</b>

6
00:00:14,199 --> 00:00:15,480
<b>que não estava prevista.</b>

7
00:00:15,480 --> 00:00:16,639
<b>Então elas vão receber</b>

8
00:00:16,639 --> 00:00:19,000
<b>sobrecarga na infraestrutura, por exemplo.</b>

9
00:00:19,000 --> 00:00:20,880
<b>Então você vai ter que ter um reforço.</b>

10
00:00:20,880 --> 00:00:23,199
<b>Você vai ter de ter um reforço de todas as redes.</b>

11
00:00:23,199 --> 00:00:24,239
<b>Você vai ter de ter um reforço</b>

12
00:00:24,239 --> 00:00:26,840
<b>da rede de abastecimento de água</b>

13
00:00:26,840 --> 00:00:28,719
<b>da rede elétrica</b>

14
00:00:28,719 --> 00:00:31,360
<b>da drenagem urbana</b>

15
00:00:31,360 --> 00:00:32,519
<b>da mobilidade</b>

16
00:00:32,559 --> 00:00:35,400
<b>de linhas de ônibus</b>

17
00:00:35,400 --> 00:00:39,639
<b>até o tráfego das vias de maneira geral.</b>

18
00:00:39,639 --> 00:00:43,519
<b>Tudo isso é impactado pelos novos moradores</b>

19
00:00:43,519 --> 00:00:49,039
<b>então a tendência é ter expansão imobiliária</b>

20
00:00:49,039 --> 00:00:51,039
<b>em outras áreas de Maceió.</b>

21
00:00:51,760 --> 00:00:53,960
<b>Não precisaria ser assim</b>

22
00:00:53,960 --> 00:00:56,400
<b>porque nós poderíamos aproveitar...</b>

23
00:00:56,400 --> 00:00:58,840
<b>Quando eu estava no IAB eu falava isso, não é?</b>

24
00:00:58,840 --> 00:01:01,639
<b>Eu falava. Desde 2019 que eu dizia</b>

25
00:01:01,639 --> 00:01:05,239
<b>sobre fazer essa retirada das pessoas</b>

26
00:01:05,239 --> 00:01:06,320
<b>de maneira planejada.</b>

27
00:01:06,320 --> 00:01:08,920
<b>Ou seja, nós temos áreas de Maceió</b>

28
00:01:08,920 --> 00:01:10,320
<b>bairros de Maceió</b>

29
00:01:10,320 --> 00:01:11,639
<b>que estão esvaziados</b>

30
00:01:11,639 --> 00:01:13,519
<b>que estão com pouca ocupação.</b>

31
00:01:13,519 --> 00:01:16,599
<b>Então porque não começar a trabalhar</b>

32
00:01:16,599 --> 00:01:18,280
<b>com o mercado imobiliário</b>

33
00:01:18,280 --> 00:01:21,519
<b>uma linha de microcrédito</b>

34
00:01:21,519 --> 00:01:22,960
<b>uma linha de financiamento em que se</b>
<b>privilegiasse.</b>

35
00:01:22,960 --> 00:01:25,159
<b>em que se privilegiasse</b>

36
00:01:25,239 --> 00:01:27,159
<b>o estabelecimento de</b>

37
00:01:27,159 --> 00:01:29,079
<b>empreendimentos imobiliários nessas áreas?</b>

38
00:01:29,079 --> 00:01:32,119
<b>E você tivesse as pessoas</b>

39
00:01:32,119 --> 00:01:34,400
<b>ao mesmo tempo que elas estivessem saindo</b>

40
00:01:34,400 --> 00:01:36,639
<b>vocÊ tivesse à ocupação de de lugares</b>

41
00:01:36,639 --> 00:01:39,920
<b>onde já há infra-estrutura instalada, por exemplo.</b>

42
00:01:39,920 --> 00:01:42,119
<b>E com isso, você teria </b>

43
00:01:43,199 --> 00:01:43,880
<b>vamos dizer assim</b>

44
00:01:43,880 --> 00:01:47,880
<b>você teria a facilidade</b>

45
00:01:47,880 --> 00:01:51,800
<b>e o bom resultado de ocupar áreas</b>

46
00:01:51,800 --> 00:01:53,119
<b>que já têm infra-estrutura</b>

47
00:01:53,119 --> 00:01:55,280
<b>mas que estavam com pouca ocupação</b>

48
00:01:55,280 --> 00:01:56,360
<b>com pouco adensamento</b>

49
00:01:56,360 --> 00:02:00,840
<b>e assim passaria a influenciar positivamente</b>

50
00:02:00,840 --> 00:02:02,360
<b>a vida urbana desses lugares</b>

51
00:02:02,360 --> 00:02:04,599
<b>que passariam a receber novos moradores</b>

52
00:02:04,599 --> 00:02:06,519
<b>e com isso, a nova ocupação geraria</b>

53
00:02:06,519 --> 00:02:07,599
<b>novas dinâmicas.</b>
<b>então, mas isso precisaria de de 11.</b>
<b>Planejamento do poder público junto AA.</b>

54
00:02:08,000 --> 00:02:11,760
<b>Mas isso precisaria de um planejamento</b>

55
00:02:11,760 --> 00:02:15,800
<b>do poder público municipal</b>

56
00:02:15,800 --> 00:02:18,559
<b>junto ao poder público federal, por exemplo.</b>

57
00:02:18,559 --> 00:02:22,199
<b>Teria que começar um diálogo</b>

58
00:02:22,199 --> 00:02:25,400
<b>consttituisse um diálogo com com o poder federal</b>

59
00:02:25,400 --> 00:02:28,679
<b>para esses empreendimentos serem aprovados</b>

60
00:02:28,679 --> 00:02:31,400
<b>para poder haver uma</b>

61
00:02:31,400 --> 00:02:35,400
<b>linha diferenciada de crédito voltada para Maceió.</b>

62
00:02:35,400 --> 00:02:37,119
<b>E isso se justificaria</b>

63
00:02:37,119 --> 00:02:39,400
<b>justamente pelo fato de ser</b>

64
00:02:39,400 --> 00:02:41,840
<b>a maior tragédia urbana</b>

65
00:02:41,840 --> 00:02:43,880
<b>ambiental-urbana da história do país.</b>

66
00:02:43,880 --> 00:02:45,880
<b>Se justificaria</b>

67
00:02:45,920 --> 00:02:47,599
<b>que o poder público federal</b>

68
00:02:47,599 --> 00:02:49,280
<b>lançasse para Maceió</b>

69
00:02:49,280 --> 00:02:51,000
<b>um olhar diferenciado</b>

70
00:02:51,000 --> 00:02:52,840
<b>em relação ao restante do país.</b>

71
00:02:52,840 --> 00:02:56,280
<b>Que é outra coisa que a gente precisa tocar aqui</b>

72
00:02:56,280 --> 00:02:58,639
<b>que é: o que não aconteceu até agora?</b>

73
00:02:58,639 --> 00:03:01,119
<b>Se vocês pararem para pensar</b>

74
00:03:01,119 --> 00:03:05,119
<b>até agora, nós não tivemos o poder público federal</b>

75
00:03:05,119 --> 00:03:08,840
<b>voltado para Maceió com um olhar diferenciado.</b>

76
00:03:08,840 --> 00:03:11,440
<b>Como é que eu tô tendo numa cidade</b>

77
00:03:11,440 --> 00:03:14,360
<b>num estado do país, numa capital do país</b>

78
00:03:14,360 --> 00:03:16,079
<b>a maior tragédia de urbano-ambiental do país</b>

79
00:03:16,079 --> 00:03:18,920
<b>e eu não tenho poder público federal</b>

80
00:03:18,920 --> 00:03:21,280
<b>voltado com os seus ministérios  pra cá?</b>

81
00:03:21,280 --> 00:03:24,639
<b>Para dizer: olha, vamos pensar juntos</b>

82
00:03:24,639 --> 00:03:26,599
<b>no que fazer?</b>

83
00:03:27,519 --> 00:03:29,159
<b>O que fazer em Maceió?</b>

84
00:03:29,840 --> 00:03:32,000
<b>E aí, você vê, é o quanto</b>

85
00:03:32,000 --> 00:03:34,239
<b>a questão da visibilidade influencia nisso</b>

86
00:03:34,239 --> 00:03:35,360
<b>porque nós falamos da visibilidade</b>

87
00:03:35,360 --> 00:03:37,599
<b>dos lugares mais pobres.</b>

88
00:03:37,599 --> 00:03:39,760
<b>Que eles tinham dificuldade de acessar a mídia.</b>

89
00:03:39,760 --> 00:03:42,000
<b>Maceió é uma capital </b>

90
00:03:42,719 --> 00:03:44,000
<b>de um estado.</b>

91
00:03:44,000 --> 00:03:46,280
<b>De um dos estados mais pobres do país</b>

92
00:03:47,000 --> 00:03:50,719
<b>de uma região que é uma das pobres do país também</b>

93
00:03:51,559 --> 00:03:53,960
<b>e que, portanto, não tem a visibilidade</b>

94
00:03:53,960 --> 00:03:57,239
<b>que grandes centros urbanos do país têm.</b>

95
00:03:57,239 --> 00:04:01,800
<b>Se essa tragédia fosse no Rio de Janeiro</b>

96
00:04:01,800 --> 00:04:05,639
<b>em São Paulo, em Minas Gerais</b>

97
00:04:05,639 --> 00:04:07,320
<b>nós teríamos já</b>

98
00:04:07,320 --> 00:04:10,400
<b>todos os ministérios voltados para esse lugar</b>

99
00:04:10,400 --> 00:04:14,119
<b>propondo, cada um na sua área</b>

100
00:04:14,119 --> 00:04:15,599
<b>o que se poderia fazer</b>

101
00:04:15,599 --> 00:04:19,199
<b>para minimizar a questão.</b>

102
00:04:20,159 --> 00:04:22,280
<b>Desde o início, desde o início.</b>

103
00:04:23,360 --> 00:04:24,920
<b>E nós temos provas disso</b>

104
00:04:24,920 --> 00:04:27,679
<b>porque nós vimos isso acontecer em Minas</b>

105
00:04:27,679 --> 00:04:29,800
<b>com as tragédias relacionadas à Vale do Rio Doce.</b>

106
00:04:31,199 --> 00:04:33,639
<b>Então assim, nós não temos isso aqui.</b>

107
00:04:33,639 --> 00:04:35,679
<b>nós vemos um grande silêncio</b>

108
00:04:35,679 --> 00:04:38,280
<b>por parte do poder público federal</b>

109
00:04:38,280 --> 00:04:39,679
<b>em relação ao que está acontecendo aqui.</b>

110
00:04:39,679 --> 00:04:40,960
<b>Nós vimos durante muito tempo</b>

111
00:04:40,960 --> 00:04:44,119
<b>um grande silêncio da mídia geral, nacional</b>

112
00:04:44,119 --> 00:04:45,239
<b>sobre o que ocorre aqui.</b>

113
00:04:45,239 --> 00:04:47,400
<b>Vemos, em parte até hoje</b>

114
00:04:47,400 --> 00:04:49,559
<b>porque o que sai na mídia nacional</b>

115
00:04:49,559 --> 00:04:51,119
<b>em relação ao que acontece aqui</b>

116
00:04:51,119 --> 00:04:53,639
<b>é muito pouco, muito, muito pouco.</b>

117
00:04:53,639 --> 00:04:56,199
<b>É muito pequeno em relação ao tamanho da tragédia.</b>

118
00:04:57,760 --> 00:05:00,239
<b>E vemos em relação aos poderes constituídos</b>

119
00:05:00,239 --> 00:05:02,159
<b>essa postura inoperante.</b>

120
00:05:03,679 --> 00:05:06,880
<b>Eu até acho que isso deveria partir</b>

121
00:05:06,880 --> 00:05:10,559
<b>como provocação dos nossos representantes locais.</b>

122
00:05:10,559 --> 00:05:13,280
<b>Já deveria ter tido há muito tempo</b>

123
00:05:13,280 --> 00:05:16,199
<b>uma postura mais proativa da prefeitura</b>

124
00:05:16,199 --> 00:05:18,519
<b>postura mais proativa do governo do estado.</b>

125
00:05:18,519 --> 00:05:20,840
<b>E uma postura mais proativa de nossos políticos</b>

126
00:05:20,840 --> 00:05:23,199
<b>dos deputados federais e senadores</b>

127
00:05:23,199 --> 00:05:24,360
<b>em relação a isso.</b>

128
00:05:24,360 --> 00:05:26,400
<b>De provocar isso o tempo inteiro.</b>

129
00:05:26,400 --> 00:05:30,360
<b>Começar a estabelecer essa interlocução</b>

130
00:05:30,360 --> 00:05:34,079
<b>para políticas públicas específicas para cá.</b>

131
00:05:34,079 --> 00:05:34,119
<b>Específicas para cá?</b>

132
00:05:34,840 --> 00:05:38,119
<b>Específicas! A gente está falando de 60 mil pessoas.</b>

133
00:05:39,559 --> 00:05:41,639
<b>Isso deveria ter provocado</b>

134
00:05:41,639 --> 00:05:44,880
<b>uma série de políticas públicas específicas.</b>

135
00:05:44,880 --> 00:05:47,159
<b>Desde políticas públicas urbanas</b>

136
00:05:47,159 --> 00:05:48,719
<b>até políticas públicas ambientais</b>

137
00:05:48,719 --> 00:05:53,119
<b>passando por políticas públicas econômicas</b>

138
00:05:53,119 --> 00:05:55,519
<b>sabe, todo tipo de política pública</b>

139
00:05:55,519 --> 00:05:58,920
<b>deveria ter sido estabelecida, voltada para cá.</b>

140
00:05:58,920 --> 00:05:59,559
<b>Para esse lugar.</b>

141
00:05:59,559 --> 00:06:01,119
<b>Para trabalhar com essa região.</b>


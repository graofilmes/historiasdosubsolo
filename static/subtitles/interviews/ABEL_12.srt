1
00:00:00,320 --> 00:00:00,920
<b>Não tem preço.</b>

2
00:00:00,920 --> 00:00:03,079
<b>É uma tragédia mesmo, uma tragédia grande.</b>

3
00:00:03,079 --> 00:00:03,920
<b>Muito grande!</b>

4
00:00:03,920 --> 00:00:06,039
<b>Tem pessoas até que desequilibraram</b>

5
00:00:06,039 --> 00:00:08,000
<b>entraram em depressão profunda.</b>

6
00:00:08,000 --> 00:00:10,639
<b>Muita coisa brava aconteceu</b>

7
00:00:12,960 --> 00:00:15,199
<b>Mas pra Maceió foi uma tragédia total.</b>

8
00:00:15,800 --> 00:00:17,039
<b>Pessoas que...</b>

9
00:00:17,039 --> 00:00:19,960
<b>Só de maquinários investidos, cento e tantos mil reais</b>

10
00:00:20,400 --> 00:00:23,039
<b>E vieram com dez mil reais pra dar às pessoas.</b>

11
00:00:23,880 --> 00:00:24,840
<b>E assim a gente está vendo.</b>

12
00:00:24,840 --> 00:00:28,199
<b>A mais famosa</b>

13
00:00:28,199 --> 00:00:30,559
<b>é a menina do balé, não é, a Eliana</b>

14
00:00:30,559 --> 00:00:34,199
<b>a pobre coitada gastou não sei quanto</b>

15
00:00:34,199 --> 00:00:36,000
<b>pediu dinheiro emprestado no banco</b>

16
00:00:36,000 --> 00:00:38,480
<b>e ficou devendo, não sei quanto...</b>

17
00:00:38,480 --> 00:00:39,840
<b>Foi 60 ou 80 mil.</b>

18
00:00:39,840 --> 00:00:41,280
<b>Não sei quanto, ela que sabe bem.</b>

19
00:00:41,280 --> 00:00:43,079
<b>Vocês podem até entrevistar ela.</b>

20
00:00:43,079 --> 00:00:44,599
<b>Vieram com dez contos pra ela</b>

21
00:00:44,599 --> 00:00:46,280
<b>dez mil reais!</b>

22
00:00:47,480 --> 00:00:48,400
<b>Uma mixaria.</b>

23
00:00:49,280 --> 00:00:51,320
<b>E o pessoal das padaria?</b>

24
00:00:52,440 --> 00:00:55,400
<b>Uma fortuna pra se instalar em outro canto.</b>

25
00:00:57,039 --> 00:01:00,119
<b>Dez mil não dá para nada, gente!</b>

26
00:01:00,119 --> 00:01:03,000
<b>Pra quem tem negócio, não dá para nada.</b>

27
00:01:03,000 --> 00:01:07,719
<b>Você tem de recomeçar tudo, nova freguesia</b>

28
00:01:07,719 --> 00:01:09,519
<b>novos equipamentos, novos tudo.</b>

29
00:01:09,519 --> 00:01:11,159
<b>Não dá pra nada dez mil reais.</b>

30
00:01:11,159 --> 00:01:12,000
<b>Isso é uma tragédia, né?</b>

31
00:01:12,639 --> 00:01:14,800
<b>Tem casos que foram realmente bem...</b>

32
00:01:15,480 --> 00:01:18,920
<b>que financeiramente os donos ficaram satisfeitos, não é?</b>

33
00:01:18,920 --> 00:01:20,199
<b>Tem casos.</b>

34
00:01:21,000 --> 00:01:22,239
<b>Mas tem outros casos...</b>

35
00:01:22,239 --> 00:01:23,880
<b>Eu fiquei sabendo de casos</b>

36
00:01:23,880 --> 00:01:26,079
<b>que o cara esperava na casa dele</b>

37
00:01:26,079 --> 00:01:29,440
<b>uma casa de, não sei quantos, 500m², 600m²</b>

38
00:01:29,440 --> 00:01:32,320
<b>esperava pelo menos um milhão e meio</b>

39
00:01:32,320 --> 00:01:34,800
<b> vieram com, sei lá, 600 reais.</b>

40
00:01:35,599 --> 00:01:37,960
<b>Uuma mixaria em relação ao valor da casa.</b>

41
00:01:38,159 --> 00:01:40,199
<b>Alí atrás de casa do Zé Lopes, tinha um sítio enorme</b>

42
00:01:40,199 --> 00:01:41,960
<b>bonito, cheio de fruteiras</b>

43
00:01:41,960 --> 00:01:46,280
<b>que tinha tinha mais de 150m, além do muro.</b>

44
00:01:46,880 --> 00:01:48,159
<b>Dos fundos da casa Lopes</b>

45
00:01:48,159 --> 00:01:49,599
<b>tinha mais de 150m pra trás</b>

46
00:01:49,599 --> 00:01:50,840
<b>pra onde chegava a lagoa.</b>

47
00:01:50,840 --> 00:01:54,159
<b>E tinha um sítio maravilhoso, Dr José Lopes</b>

48
00:01:54,159 --> 00:01:56,119
<b>que ele criava tudo ali tinha muita Fronteira.</b>

49
00:01:56,119 --> 00:02:00,559
<b>Aí, gente, hoje alí tem mais de 2m de água.</b>

50
00:02:01,280 --> 00:02:02,440
<b>2 metros que afundou!</b>


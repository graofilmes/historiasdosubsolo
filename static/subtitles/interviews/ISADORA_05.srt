1
00:00:00,000 --> 00:00:01,119
<b>Hoje são cinco bairros</b>

2
00:00:01,119 --> 00:00:02,800
<b>afetados pela mineração em Maceió:</b>

3
00:00:02,800 --> 00:00:04,360
<b>Pinheiro</b>

4
00:00:04,360 --> 00:00:05,760
<b>Bebedouro</b>

5
00:00:05,760 --> 00:00:07,159
<b>Mutange</b>

6
00:00:07,159 --> 00:00:08,159
<b>Bom Parto</b>

7
00:00:08,159 --> 00:00:08,920
<b>e Farol.</b>

8
00:00:08,920 --> 00:00:12,360
<b>Farol é o último que entrou só agora no comecinho de 2021.</b>

9
00:00:13,679 --> 00:00:16,679
<b>Então, desses cinco bairros</b>

10
00:00:17,159 --> 00:00:18,199
<b>três</b>

11
00:00:18,440 --> 00:00:20,599
<b>Bebedouro, Mutange e Bom Parto</b>

12
00:00:20,599 --> 00:00:22,400
<b>são bairros lagunares.</b>

13
00:00:22,400 --> 00:00:23,719
<b>São bairros</b>

14
00:00:23,719 --> 00:00:26,280
<b>que estão banhados pela lagoa Mundaú</b>

15
00:00:26,280 --> 00:00:28,559
<b>e que estão passando por esse afundamento.</b>

16
00:00:28,559 --> 00:00:30,440
<b>E dos cinco bairros</b>

17
00:00:30,440 --> 00:00:32,599
<b>nós sabemos agora recentemente</b>

18
00:00:32,599 --> 00:00:34,480
<b>saiu um artigo recentemente</b>

19
00:00:35,280 --> 00:00:37,639
<b>a parte mais grave do afundamento...</b>

20
00:00:37,639 --> 00:00:38,639
<b>Porque o que acontece com esses bairros </b>

21
00:00:38,639 --> 00:00:39,760
<b>é que eles estão afundando</b>

22
00:00:39,960 --> 00:00:41,800
<b>pelos buracos deixados pela mineração.</b>

23
00:00:42,239 --> 00:00:43,559
<b>Nós sabemos agora...</b>

24
00:00:43,559 --> 00:00:44,719
<b>Tudo começou pelo Pinheiro</b>

25
00:00:44,719 --> 00:00:47,000
<b>foi o primeiro bairro a ter essas questões</b>

26
00:00:47,000 --> 00:00:49,599
<b>mas nós sabemos agora que na verdade</b>

27
00:00:49,599 --> 00:00:52,400
<b>a pior parte desse afundamento</b>

28
00:00:52,400 --> 00:00:54,880
<b>está nesses bairros lagunares.</b>

29
00:00:54,880 --> 00:00:58,199
<b>Nesses bairros onde a mineração</b>

30
00:00:58,199 --> 00:01:01,880
<b>deixou cavidades abaixo da lagoa.</b>

31
00:01:02,800 --> 00:01:05,960
<b>A situação é muito mais grave</b>

32
00:01:05,960 --> 00:01:07,159
<b>do que no bairro do Pinheiro.</b>

33
00:01:07,159 --> 00:01:08,679
<b>Bebedouro, por exemplo</b>

34
00:01:08,679 --> 00:01:10,559
<b>é um dos bairros mais antigos de Maceió.</b>

35
00:01:10,840 --> 00:01:13,519
<b>É um dos bairros mais antigos de povoamento.</b>

36
00:01:13,519 --> 00:01:15,840
<b>Maceió tem um histórico</b>

37
00:01:15,840 --> 00:01:18,119
<b>que remonta ao século XVIII</b>

38
00:01:18,119 --> 00:01:20,000
<b>e Bebedouro é um dos bairros</b>

39
00:01:20,000 --> 00:01:22,960
<b>que se a gente procura nos primeiros registros</b>

40
00:01:22,960 --> 00:01:24,280
<b>em relação a Maceió</b>

41
00:01:24,280 --> 00:01:26,159
<b>é um dos bairros que é apomntado</b>

42
00:01:26,159 --> 00:01:27,679
<b>já no século XVIII</b>

43
00:01:27,679 --> 00:01:30,400
<b>em relatos de viajantes.</b>

44
00:01:30,840 --> 00:01:33,519
<b>Então surge como um núcleo de povoamento</b>

45
00:01:33,519 --> 00:01:37,079
<b>justamente ao redor desse núcleo de água.</b>

46
00:01:38,280 --> 00:01:40,440
<b>O primeiro riacho que serviu</b>

47
00:01:40,440 --> 00:01:42,639
<b>como abastecimento de água na cidade</b>

48
00:01:42,639 --> 00:01:43,880
<b>foi o Riacho do Silva.</b>

49
00:01:43,880 --> 00:01:46,159
<b>Um riacho que existe até hoje em Bebedouro.</b>

50
00:01:46,159 --> 00:01:47,280
<b>Está hoje poluido. </b>

51
00:01:47,280 --> 00:01:50,719
<b>Não é mais uma fonte de abastecimento</b>

52
00:01:50,719 --> 00:01:53,679
<b>mas foi a primeira fonte de abastecimeno conhecida de Maceió</b>

53
00:01:53,679 --> 00:01:55,559
<b>Eu tenho uma teoria de que esses núcleos de povoamento</b>

54
00:01:55,559 --> 00:01:57,559
<b>eles são todos vinculados a antigos portos</b>

55
00:01:57,880 --> 00:01:59,199
<b>antigos portos lagunares</b>

56
00:01:59,199 --> 00:02:02,039
<b>porque o que a gente sabe</b>

57
00:02:02,039 --> 00:02:06,079
<b>é que Maceió começou num núcleo central</b>

58
00:02:06,079 --> 00:02:08,719
<b>que era abastecido por um canal, o Canal da Levada</b>

59
00:02:08,719 --> 00:02:09,840
<b>e daí vem o nome do bairro</b>

60
00:02:09,840 --> 00:02:12,000
<b>que foi o terceiro bairro de povoamento em Maceió.</b>

61
00:02:12,159 --> 00:02:14,360
<b>Era um núcleo de abastecimento interno, doméstico</b>

62
00:02:14,360 --> 00:02:16,599
<b>e esse abastecimento ele vinha pela lagoa.</b>

63
00:02:16,719 --> 00:02:18,840
<b>Então o que era abastecimento doméstico</b>

64
00:02:18,840 --> 00:02:20,760
<b>do núcleo urbano de Maceió</b>

65
00:02:20,760 --> 00:02:23,079
<b>eram translados</b>

66
00:02:23,079 --> 00:02:24,679
<b>eram rotas lagunares</b>

67
00:02:24,679 --> 00:02:27,679
<b>de abastecimento de mercadorias.</b>

68
00:02:27,679 --> 00:02:29,559
<b>De transporte e abastecimento de mercadorias.</b>

69
00:02:29,559 --> 00:02:31,920
<b>Então as mercadorias vinham pela lagoa</b>

70
00:02:31,920 --> 00:02:33,440
<b>elas vinham através de barcos</b>

71
00:02:33,599 --> 00:02:35,239
<b>de embarcações pela lagoa</b>

72
00:02:35,239 --> 00:02:38,960
<b>e eram deixadas em pequenos ancoradouros</b>

73
00:02:39,000 --> 00:02:40,360
<b>ao longo da margem lagunar.</b>

74
00:02:40,360 --> 00:02:42,559
<b>O principal deles, o maior deles</b>

75
00:02:42,559 --> 00:02:44,920
<b>era o Porto da Levada.</b>

76
00:02:44,920 --> 00:02:47,679
<b>Você entrava pelo canal e chegava no Porto da Levada</b>

77
00:02:47,679 --> 00:02:50,800
<b>que era vizinho ao núcleo urbano principal</b>

78
00:02:50,800 --> 00:02:52,639
<b>da cidade que era o núcleo central.</b>

79
00:02:52,639 --> 00:02:55,639
<b>Daí surgiu o chamado Maceió.</b>

80
00:02:56,079 --> 00:02:59,880
<b>Mas junto desse porto principal</b>

81
00:02:59,880 --> 00:03:01,280
<b>tinham outros pequenos portos.</b>

82
00:03:01,280 --> 00:03:04,760
<b>Tinha um porto que era bebedouro, por exemplo.</b>

83
00:03:05,119 --> 00:03:07,079
<b>E esse porto, a minha teoria</b>

84
00:03:07,079 --> 00:03:10,079
<b>é que ele deu origem ao que seria</b>

85
00:03:10,079 --> 00:03:11,880
<b>primeiro ao núcleo de povoamento de Bebedouro</b>

86
00:03:11,880 --> 00:03:13,800
<b>e depois ao bairro de Bebedouro.</b>

87
00:03:13,800 --> 00:03:15,719
<b>Bebedouro tem a ver com água também.</b>

88
00:03:15,719 --> 00:03:18,880
<b>Bebedouro: local ondes e pára para beber</b>

89
00:03:18,880 --> 00:03:21,320
<b>e dar de beber, especialmente aos animais.</b>

90
00:03:21,320 --> 00:03:22,719
<b>E isso é um registro histórico.</b>

91
00:03:22,719 --> 00:03:25,639
<b>Então há o registro histórico de que pros viajantes</b>

92
00:03:25,639 --> 00:03:27,559
<b>havia uma estrada que saia de Maceió</b>

93
00:03:27,559 --> 00:03:30,440
<b>em direção norte da cidade</b>

94
00:03:30,440 --> 00:03:33,559
<b>para outros municípios no vale do Mundaú.</b>

95
00:03:33,800 --> 00:03:37,119
<b>E essa estrada era uma estrada chamada</b>

96
00:03:37,119 --> 00:03:38,960
<b>de Estrada de Bebedouro</b>

97
00:03:38,960 --> 00:03:41,440
<b>porque os viajantes saiam</b>

98
00:03:41,440 --> 00:03:43,320
<b>e nesse riacho</b>

99
00:03:43,320 --> 00:03:47,599
<b>paravam pra beber água ou dar de beber aos seus animais.</b>

100
00:03:49,760 --> 00:03:53,440
<b>Esse núcleo surge ao redor dessa relação com a água.</b>

101
00:03:53,840 --> 00:03:56,119
<b>Maceió tem vários núcleos urbanos</b>

102
00:03:56,119 --> 00:03:58,400
<b>que vão surgir na sua relação com a água.</b>


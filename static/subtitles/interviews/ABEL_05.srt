1
00:00:00,039 --> 00:00:03,679
<b>Essa distância entre as mina é o que se chama de pilares.</b>

2
00:00:03,679 --> 00:00:05,320
<b>Você faz um buraco aqui</b>

3
00:00:05,320 --> 00:00:06,599
<b>outro buraco alí</b>

4
00:00:06,599 --> 00:00:08,639
<b>aí fica aqui o meio sem ter buraco.</b>

5
00:00:08,639 --> 00:00:10,519
<b>Esse meio que está sustentando o que tá em cima.</b>

6
00:00:11,159 --> 00:00:11,800
<b>Entendeu?</b>

7
00:00:11,800 --> 00:00:12,920
<b>Você faz um buraco aqui</b>

8
00:00:12,920 --> 00:00:15,320
<b>e você faz duas grandes cavernas</b>

9
00:00:15,920 --> 00:00:16,719
<b>e no meio</b>

10
00:00:16,719 --> 00:00:19,400
<b>esse meio aí é o mínimo que você tem que deixar</b>

11
00:00:19,400 --> 00:00:21,039
<b>para poder sustentar o que está em cima.</b>

12
00:00:21,719 --> 00:00:22,559
<b>Mas só que esse...</b>

13
00:00:22,559 --> 00:00:24,400
<b>Quando você aumenta o diâmetro</b>

14
00:00:24,400 --> 00:00:26,480
<b>aí essa distância sai diminuindo.</b>

15
00:00:26,480 --> 00:00:28,400
<b>Então a distância que seria para ser</b>

16
00:00:28,400 --> 00:00:29,519
<b>nas minhas contas</b>

17
00:00:29,519 --> 00:00:31,920
<b>em torno de 100m, dee 90m a 100m</b>

18
00:00:32,599 --> 00:00:36,280
<b>as distância ficaram, eu diria que de 70% pelo menos</b>

19
00:00:36,960 --> 00:00:38,239
<b>à distância de zero</b>

20
00:00:38,239 --> 00:00:40,039
<b>porque as Minas se juntaram.</b>

21
00:00:41,000 --> 00:00:42,039
<b>Nessas minas que se juntaram</b>

22
00:00:42,039 --> 00:00:44,599
<b>cabe folgadamente um Maracanã dentro</b>

23
00:00:45,360 --> 00:00:47,360
<b>no buraco ficou lá embaixo.</b>

24
00:00:47,920 --> 00:00:48,760
<b>Folgadamente.</b>

25
00:00:48,760 --> 00:00:52,000
<b>Então tinha de zero a 10m ou 15m.</b>

26
00:00:52,000 --> 00:00:53,920
<b>Uma coisa absurda!</b>

27
00:00:53,920 --> 00:00:56,519
<b>Então foi com esse encurtamento</b>

28
00:00:56,519 --> 00:00:59,800
<b>com esse estreitamento dos pilares de sustentação</b>

29
00:00:59,800 --> 00:01:01,199
<b>que rompeu</b>

30
00:01:01,199 --> 00:01:03,119
<b>rompeu e deu terremoto.</b>

31
00:01:04,239 --> 00:01:08,039
<b>E essa mesma condição está aí e pode acontecer</b>

32
00:01:08,039 --> 00:01:10,599
<b>a qualquer momento poderá acontecer</b>

33
00:01:10,599 --> 00:01:13,880
<b>e eu digo isso baseado em relatórios</b>

34
00:01:13,880 --> 00:01:16,840
<b>até nos próprios relatórios do instituto alemão</b>

35
00:01:16,840 --> 00:01:18,159
<b>e de outros também. </b>

36
00:01:18,159 --> 00:01:21,320
<b>É porque a Braskem contratou</b>

37
00:01:21,320 --> 00:01:25,079
<b>por exigência da Agência Nacional de Mineração</b>

38
00:01:25,079 --> 00:01:27,840
<b>contratou empresas do mundo inteiro.</b>

39
00:01:27,840 --> 00:01:30,119
<b>Tem empresa Americana, empresa italiana</b>

40
00:01:30,119 --> 00:01:32,400
<b>empresa francesa, empresa holandesa</b>

41
00:01:32,400 --> 00:01:34,679
<b>empresa alemã, não é?</b>

42
00:01:34,760 --> 00:01:36,519
<b>Tem pelo menos umas 9 ou 10 empresas</b>

43
00:01:36,519 --> 00:01:38,760
<b>que emitiram esses relatórios</b>

44
00:01:38,760 --> 00:01:40,840
<b>e eu tenho quase todos eles.</b>

45
00:01:40,840 --> 00:01:43,840
<b>É de domínio público, nãe é?</b>

46
00:01:43,840 --> 00:01:45,440
<b>Depois que chega na ANM</b>

47
00:01:45,440 --> 00:01:47,239
<b>e alguns meses é de domínio público.</b>


1
00:00:00,000 --> 00:00:00,000
<b>Nós estamos trabalhando.</b>

2
00:00:00,000 --> 00:00:00,000
<b>É desde o início dessa crise da Braskem</b>

3
00:00:00,000 --> 00:00:00,000
<b> que nós temos um trabalho</b>

4
00:00:00,000 --> 00:00:00,000
<b>de esclarecimento muito forte.</b>

5
00:00:00,000 --> 00:00:00,000
<b>O que acontece nesses lugares</b>

6
00:00:00,000 --> 00:00:00,000
<b>tem um lado muito técnico</b>

7
00:00:00,000 --> 00:00:00,000
<b>que é muito ruim de compreender.</b>

8
00:00:00,000 --> 00:00:00,000
<b>Muito ruim de compreenção pelas pessoas.</b>

9
00:00:00,000 --> 00:00:00,000
<b>Você tem mapas que são lançados</b>

10
00:00:00,000 --> 00:00:00,000
<b>que são atualizados</b>

11
00:00:00,000 --> 00:00:00,000
<b>e que são informações técnicas</b>

12
00:00:00,000 --> 00:00:00,000
<b>com questões geológicas, por exemplo</b>

13
00:00:00,000 --> 00:00:00,000
<b>e as pessoas leigas têm dificuldade de interpretar isso.</b>

14
00:00:00,000 --> 00:00:00,000
<b>Então desde o início de 2018</b>

15
00:00:00,000 --> 00:00:00,000
<b>nós temos tido uma atuação</b>

16
00:00:00,000 --> 00:00:00,000
<b>muito à partir de mim pela arquitetura e urbanismo</b>

17
00:00:00,000 --> 00:00:00,000
<b>de esclarecer muitas pessoas.</b>

18
00:00:00,000 --> 00:00:00,000
<b>Eu atuei, por exemplo...</b>

19
00:00:00,000 --> 00:00:00,000
<b>Eu fui a vários dos eventos</b>

20
00:00:00,000 --> 00:00:00,000
<b>a várias audiências</b>

21
00:00:00,000 --> 00:00:00,000
<b>desde aquela maior</b>

22
00:00:00,000 --> 00:00:00,000
<b>que foi a que a CPRM realmente definiu</b>

23
00:00:00,000 --> 00:00:00,000
<b>a culpa da Braskem em relação ao fenômeno</b>

24
00:00:00,000 --> 00:00:00,000
<b>mas também as menores que foram</b>

25
00:00:00,000 --> 00:00:00,000
<b>organizadas pelos moradores</b>

26
00:00:00,000 --> 00:00:00,000
<b>ou pela universidade.</b>

27
00:00:00,000 --> 00:00:00,000
<b>Eu estive na UFAL</b>

28
00:00:00,000 --> 00:00:00,000
<b>em eventos que a UFAL realizou</b>

29
00:00:00,000 --> 00:00:00,000
<b>pra esclarecer a comunidade acadêmica.</b>

30
00:00:00,000 --> 00:00:00,000
<b>Eu participei de momentos</b>

31
00:00:00,000 --> 00:00:00,000
<b>em que a Igreja Batista do Pinheiro</b>

32
00:00:00,000 --> 00:00:00,000
<b>chamou especialistas</b>

33
00:00:00,000 --> 00:00:00,000
<b>chamou o professora Abel, por exemplo.</b>

34
00:00:00,000 --> 00:00:00,000
<b>Foi feita uma mesa, pelo "Aqui Fora"</b>

35
00:00:00,000 --> 00:00:00,000
<b>lá no quntal cultural</b>

36
00:00:00,000 --> 00:00:00,000
<b>eu estava na mesa participando.</b>

37
00:00:00,000 --> 00:00:00,000
<b>Então, enfim. Eu atuei tanto esclarecendo</b>

38
00:00:00,000 --> 00:00:00,000
<b>como servi de ponte</b>

39
00:00:00,000 --> 00:00:00,000
<b>entre moradores e esses técnicos</b>

40
00:00:00,000 --> 00:00:00,000
<b>como atuei em alguns momentos, inclusive</b>

41
00:00:00,000 --> 00:00:00,000
<b>me oferecendo com serviços mesmo.</b>

42
00:00:00,000 --> 00:00:00,000
<b>Eu tinha um amigo que estava com os pais</b>

43
00:00:00,000 --> 00:00:00,000
<b>morando alí na área do sanatório.</b>

44
00:00:00,000 --> 00:00:00,000
<b>E era muito comum ali naquela região</b>

45
00:00:00,000 --> 00:00:00,000
<b>eu conversei com pessoas daquela região...</b>

46
00:00:00,000 --> 00:00:00,000
<b>Eles não tinham o título de posse dos imóveis.</b>

47
00:00:00,000 --> 00:00:00,000
<b>Elstavam lá há décadas</b>

48
00:00:00,000 --> 00:00:00,000
<b>mas nunca tinham tido a preocupação</b>

49
00:00:00,000 --> 00:00:00,000
<b>de estabelecer o seu título de posse</b>

50
00:00:00,000 --> 00:00:00,000
<b>o seu instrumento formal de posse do imóvel.</b>

51
00:00:00,000 --> 00:00:00,000
<b>O meu amigo mesmo, os pais dele não tinham</b>

52
00:00:00,000 --> 00:00:00,000
<b>e moravam lá desde que ele tinha nascido.</b>

53
00:00:00,000 --> 00:00:00,000
<b>Fazia quase 40 anos que ele morava lá</b>

54
00:00:00,000 --> 00:00:00,000
<b>mas não tinha um título de posse dos pais no imóvel.</b>

55
00:00:00,000 --> 00:00:00,000
<b>Então eu me ofereci pra fazer uma plata básica do imóvel</b>

56
00:00:00,000 --> 00:00:00,000
<b>pra eles poderem entrar com usucapião, por exemplo.</b>

57
00:00:00,000 --> 00:00:00,000
<b>Pra poder ter acesso</b>

58
00:00:00,000 --> 00:00:00,000
<b>aos serviços intermediados pelo Ministério Público</b>

59
00:00:00,000 --> 00:00:00,000
<b>que exigia isso.</b>

60
00:00:00,000 --> 00:00:00,000
<b>O Ministério público estabeleceu um escritório</b>

61
00:00:00,000 --> 00:00:00,000
<b>com uma série de serviços</b>

62
00:00:00,000 --> 00:00:00,000
<b>mas pra ter acesso você tinha de comprovar</b>

63
00:00:00,000 --> 00:00:00,000
<b>que você era dono do imóvel.</b>

64
00:00:00,000 --> 00:00:00,000
<b>E, legalmente, jurídicamente</b>

65
00:00:00,000 --> 00:00:00,000
<b>você só comprova com um título de posse.</b>

66
00:00:00,000 --> 00:00:00,000
<b>Contrato de compra e venda não é título de posse.</b>

67
00:00:00,000 --> 00:00:00,000
<b>Então, me ofereci pra isso.</b>

68
00:00:00,000 --> 00:00:00,000
<b>Me ofereci pra um outro amigo</b>

69
00:00:00,000 --> 00:00:00,000
<b>que tinha de sair de lá</b>

70
00:00:00,000 --> 00:00:00,000
<b>e precisava reformar o apartamento</b>

71
00:00:00,000 --> 00:00:00,000
<b>pra poder tocar a vida.</b>

72
00:00:00,000 --> 00:00:00,000
<b>Então me ofereci pra fazer o serviço</b>

73
00:00:00,000 --> 00:00:00,000
<b>num valor abaixo da tabela de arquitetura e urbanismo.</b>

74
00:00:00,000 --> 00:00:00,000
<b>Não pude fazer de graça porque</b>

75
00:00:00,000 --> 00:00:00,000
<b>eu teria de pagar pra fazer o levantamento do imóvel</b>

76
00:00:00,000 --> 00:00:00,000
<b>eu tinha de ter outra pessoa comigo</b>

77
00:00:00,000 --> 00:00:00,000
<b>tinha de pagar um estagiário.</b>

78
00:00:00,000 --> 00:00:00,000
<b>Mas eu ofereci um serviço abaixo da tabela</b>

79
00:00:00,000 --> 00:00:00,000
<b>pra ele podeer fazer a reforma do apartamento.</b>

80
00:00:00,000 --> 00:00:00,000
<b>Eu fui com um amigo que também era morador</b>

81
00:00:00,000 --> 00:00:00,000
<b>pra fazer avaliação de imóvel.</b>

82
00:00:00,000 --> 00:00:00,000
<b>Assim, eu me ofereci pra várias coisas</b>

83
00:00:00,000 --> 00:00:00,000
<b>ao longo do processo</b>

84
00:00:00,000 --> 00:00:00,000
<b>e o IDEAL atuou fazendo divulgação e esclarecimentos</b>

85
00:00:00,000 --> 00:00:00,000
<b>e fazendo divulgação de eventos</b>

86
00:00:00,000 --> 00:00:00,000
<b>que iam discutir sobre a questão pra moradores.</b>

87
00:00:00,000 --> 00:00:00,000
<b>Nós fizemos, em 2019</b>

88
00:00:00,000 --> 00:00:00,000
<b>um hackathon.</b>

89
00:00:00,000 --> 00:00:00,000
<b>Eu era do IAB na época ainda</b>

90
00:00:00,000 --> 00:00:00,000
<b>IAB é o Instituto Arquitetos do Brasil</b>

91
00:00:00,000 --> 00:00:00,000
<b>Eu era do IAB Alagoas, era a presidente.</b>

92
00:00:00,000 --> 00:00:00,000
<b>Já tinha sido por duas gestões vice-presidente</b>

93
00:00:00,000 --> 00:00:00,000
<b>e nessa gestão fui presidente do IAB.</b>

94
00:00:00,000 --> 00:00:00,000
<b>E o IAB puxou junto com a ONU habtat</b>

95
00:00:00,000 --> 00:00:00,000
<b>com o pessoal da ONU habtat aqui em Maceió</b>

96
00:00:00,000 --> 00:00:00,000
<b>e junto com o Detran Alagoas e a UNIT</b>

97
00:00:00,000 --> 00:00:00,000
<b>puxou um evento que era dentro</b>

98
00:00:00,000 --> 00:00:00,000
<b>do circuito urbano da ONU internacional</b>

99
00:00:00,000 --> 00:00:00,000
<b>nós puxamos um evento que foi chamado na época de</b>

100
00:00:00,000 --> 00:00:00,000
<b>encontro dos pensadores urbanos de Alagoas</b>

101
00:00:00,000 --> 00:00:00,000
<b>e esse evento tinha três eixos</b>

102
00:00:00,000 --> 00:00:00,000
<b>e cada instituição respondia por um eixo.</b>

103
00:00:00,000 --> 00:00:00,000
<b>Um dos eixos era resiliência urbana</b>

104
00:00:00,000 --> 00:00:00,000
<b>e quem ficou responsável por esse eixo era o IAB.</b>

105
00:00:00,000 --> 00:00:00,000
<b>Eu sugeri que nós focássemos esse eixo inteiro</b>

106
00:00:00,000 --> 00:00:00,000
<b>na questão dos bairros que na época eram três:</b>

107
00:00:00,000 --> 00:00:00,000
<b>Era o Pinheiro e tinham acabado de entrar</b>

108
00:00:00,000 --> 00:00:00,000
<b>Bebedouro e Mutange.</b>

109
00:00:00,000 --> 00:00:00,000
<b>Então eu sugeri que nós focássemos</b>

110
00:00:00,000 --> 00:00:00,000
<b>nesses três bairros a questão da resiliência urbana.</b>

111
00:00:00,000 --> 00:00:00,000
<b>Fizemos uma mesa com participação da polícia civil</b>

112
00:00:00,000 --> 00:00:00,000
<b>com participação de representantes dos moradores</b>

113
00:00:00,000 --> 00:00:00,000
<b>com participação da universidade...</b>

114
00:00:00,000 --> 00:00:00,000
<b>Então assim, foi uma mesa que reuniu</b>

115
00:00:00,000 --> 00:00:00,000
<b>vários pontos de vista sobre a questão</b>

116
00:00:00,000 --> 00:00:00,000
<b>e nós fizemos uma atividade que foi um hackathon.</b>

117
00:00:00,000 --> 00:00:01,960
<b>Nós estamos trabalhando</b>

118
00:00:01,960 --> 00:00:06,199
<b>desde o início dessa crise da Braskem.</b>

119
00:00:06,600 --> 00:00:08,039
<b>Aqui nós temos um trabalho</b>

120
00:00:08,039 --> 00:00:10,840
<b>de esclarecimento</b>

121
00:00:10,840 --> 00:00:12,199
<b>muito forte.</b>

122
00:00:12,199 --> 00:00:14,159
<b>O que acontece nesses lugares é que</b>

123
00:00:14,159 --> 00:00:15,199
<b>tem um lado muito técnico</b>

124
00:00:15,199 --> 00:00:16,600
<b>que é muito ruim de compreender.</b>

125
00:00:16,600 --> 00:00:18,680
<b>Que é muito ruim de compreenção pelas pessoas.</b>

126
00:00:18,680 --> 00:00:21,479
<b>Então você tem mapas que são lançados</b>

127
00:00:21,479 --> 00:00:22,720
<b>que são atualizados</b>

128
00:00:22,720 --> 00:00:24,560
<b>que são informações técnicas</b>

129
00:00:25,560 --> 00:00:28,319
<b>de questões geológicas, por exemplo</b>

130
00:00:28,319 --> 00:00:31,319
<b>e as pessoas leigas têm dificuldade de interpretar isso.</b>

131
00:00:31,800 --> 00:00:33,079
<b>Então desde que começou</b>

132
00:00:33,079 --> 00:00:34,800
<b>desde o início de 2018</b>

133
00:00:34,800 --> 00:00:36,880
<b>Nós temos tido uma atuação</b>

134
00:00:36,880 --> 00:00:39,840
<b>muito à partir de mim pela arquitetura e urbanismo</b>

135
00:00:39,840 --> 00:00:42,119
<b>de esclarecer muitas pessoas.</b>

136
00:00:42,119 --> 00:00:44,239
<b>Eu atuei, por exemplo</b>

137
00:00:44,239 --> 00:00:46,079
<b>Eu fui a todas</b>

138
00:00:46,079 --> 00:00:48,640
<b>todas não, mas a várias das...</b>

139
00:00:48,640 --> 00:00:50,239
<b>Vários dos eventos.</b>

140
00:00:50,239 --> 00:00:51,640
<b>Várias audiências.</b>

141
00:00:52,279 --> 00:00:54,680
<b>Desde aquela maior que foi aqui no CPRM</b>

142
00:00:54,680 --> 00:00:55,800
<b>e que realmente definiu</b>

143
00:00:55,800 --> 00:00:57,640
<b>a culpa da Braskem em relação ao fenômeno</b>

144
00:00:57,640 --> 00:00:58,720
<b>mas também outras menores</b>

145
00:00:58,720 --> 00:01:00,159
<b>que foi movendo os moradores</b>

146
00:01:00,479 --> 00:01:02,479
<b>ou pela universidade, eu estive na UFAL</b>

147
00:01:02,479 --> 00:01:04,319
<b>em eventos que a UFAL realizou</b>

148
00:01:04,319 --> 00:01:06,760
<b>pra esclarecer a comunidade acadêmica.</b>

149
00:01:06,760 --> 00:01:07,800
<b>Eu estive</b>

150
00:01:07,800 --> 00:01:09,560
<b>eu participei de momentos em que a</b>

151
00:01:09,560 --> 00:01:11,159
<b>Igreja Batista do Pinheiro</b>

152
00:01:11,159 --> 00:01:12,359
<b>chamou especialistas</b>

153
00:01:12,359 --> 00:01:14,159
<b>chamou o professor Abel, por exemplo</b>

154
00:01:14,159 --> 00:01:15,359
<b>Eu fui... </b>

155
00:01:15,359 --> 00:01:17,439
<b>Foi feito uma mesa pelo Aqui Fora</b>

156
00:01:17,439 --> 00:01:18,560
<b>lá no quintal cultural</b>

157
00:01:18,560 --> 00:01:20,279
<b>eu estava na mesa participando.</b>

158
00:01:20,279 --> 00:01:21,199
<b>Então enfim</b>

159
00:01:21,199 --> 00:01:22,840
<b>eu atuei tanto esclarecendo</b>

160
00:01:22,840 --> 00:01:24,720
<b>como servindo de ponte</b>

161
00:01:24,720 --> 00:01:27,319
<b>entre moradores e esses técnicos</b>

162
00:01:27,319 --> 00:01:29,800
<b>como atuei em alguns momentos inclusve</b>

163
00:01:29,800 --> 00:01:32,279
<b>me oferecend com serviços mesmo</b>

164
00:01:33,039 --> 00:01:35,399
<b>Eu tinha um amigo que estava</b>

165
00:01:35,399 --> 00:01:36,600
<b>com os pais morando</b>

166
00:01:36,600 --> 00:01:37,800
<b>ali na área do sanatório</b>

167
00:01:37,800 --> 00:01:39,560
<b>e era muito comum naquela região</b>

168
00:01:39,560 --> 00:01:41,239
<b>conversei com pesss daqula região</b>

169
00:01:41,239 --> 00:01:43,880
<b>que não tinham o título de posse dos iméveis.</b>

170
00:01:44,880 --> 00:01:46,760
<b>Estavam lá há decadas</b>

171
00:01:46,760 --> 00:01:49,920
<b>mas nunca tinham tido a preocupação</b>

172
00:01:50,199 --> 00:01:53,119
<b>de estabelecer</b>

173
00:01:53,119 --> 00:01:54,479
<b>o seu título de posse</b>

174
00:01:54,479 --> 00:01:56,960
<b>o seu instrumeto formal de posse o imóvel.</b>

175
00:01:57,600 --> 00:01:59,079
<b>Então, o meu amigo msmo</b>

176
00:01:59,079 --> 00:01:59,920
<b>os pais delenão tinham.</b>

177
00:01:59,920 --> 00:02:01,279
<b>Moravam lá desde que ele tinha nascido</b>

178
00:02:01,279 --> 00:02:02,760
<b>fazia quase quarenta anos</b>

179
00:02:02,760 --> 00:02:03,880
<b>que ele morava lá</b>

180
00:02:04,039 --> 00:02:06,279
<b>mas não tinha o título de posse dos pais no imóvel</b>

181
00:02:06,279 --> 00:02:08,920
<b>Então me ofereci pra fazer uma planta básica do imóvel</b>

182
00:02:08,920 --> 00:02:10,399
<b>pra eles poderem entrar com usucapião</b>

183
00:02:10,399 --> 00:02:11,039
<b>por exemplo</b>

184
00:02:11,359 --> 00:02:13,760
<b>Pra poder ter acesso</b>

185
00:02:13,760 --> 00:02:16,560
<b>aos serviços intermediados pelo Ministério Público</b>

186
00:02:16,560 --> 00:02:18,079
<b>que exigiam isso.</b>

187
00:02:18,079 --> 00:02:19,800
<b>O Ministério Público estabeleceu um escritório</b>

188
00:02:19,800 --> 00:02:21,279
<b>com uma série de serviços</b>

189
00:02:21,279 --> 00:02:22,520
<b>mas pra você ter acesso</b>

190
00:02:22,520 --> 00:02:24,079
<b>você tinha de comprovar que você era dono do imóvel.</b>

191
00:02:24,079 --> 00:02:25,880
<b>E legalmente, juridicamente</b>

192
00:02:25,880 --> 00:02:27,359
<b>você só comprova com o Título de Posse.</b>

193
00:02:27,720 --> 00:02:29,920
<b>Contrato de Compra e Venda não é Título de Posse.</b>

194
00:02:29,920 --> 00:02:31,279
<b>Então me ofereci pra isso</b>

195
00:02:31,279 --> 00:02:31,960
<b>ofereci</b>

196
00:02:31,960 --> 00:02:33,920
<b>a um outro amigo que tinha de sair de lá</b>

197
00:02:33,920 --> 00:02:35,760
<b>e precisava reformar o apartamento</b>

198
00:02:35,760 --> 00:02:38,600
<b>pra poder tocar a vida.</b>

199
00:02:38,600 --> 00:02:41,159
<b>Eu me ofereci a fazer o serviço</b>

200
00:02:41,159 --> 00:02:44,720
<b>por um valor abaixo da tabela da arquitetura e urbanismo.</b>

201
00:02:44,720 --> 00:02:46,680
<b>Não pude fazer de graça porque senão</b>

202
00:02:46,680 --> 00:02:47,560
<b>eu ia ter de pagar</b>

203
00:02:47,560 --> 00:02:49,479
<b>pra fazer o levantamento do imóvel</b>

204
00:02:49,479 --> 00:02:50,600
<b>e eu tinha de ter uma pessoa comigo</b>

205
00:02:50,600 --> 00:02:51,680
<b>eu teria de pagar um estágiário.</b>

206
00:02:51,680 --> 00:02:54,239
<b>Mas eu ofereci o serviço num valor abaixo da tabela</b>

207
00:02:54,239 --> 00:02:56,640
<b>pra poder fazer a reforma do apartamento.</b>

208
00:02:57,319 --> 00:02:58,600
<b>Eu fui com uma amiga</b>

209
00:02:58,600 --> 00:02:59,560
<b>que também era moradora</b>

210
00:02:59,560 --> 00:03:01,359
<b>fazer avaliação de imóvel.</b>

211
00:03:01,359 --> 00:03:02,760
<b>Eu me ofereci pra várias coisas</b>

212
00:03:02,760 --> 00:03:04,079
<b>ao longo do processo</b>

213
00:03:04,079 --> 00:03:05,920
<b>e o IDEAL atuou</b>

214
00:03:05,920 --> 00:03:08,000
<b>fazendo divulgação de esclarecimentos</b>

215
00:03:08,000 --> 00:03:11,239
<b>fazendo divulgação de eventos que iam discutir</b>

216
00:03:11,239 --> 00:03:13,600
<b>sobre a questão com moradores.</b>

217
00:03:13,600 --> 00:03:16,319
<b>Nós fizemos, em 2019</b>

218
00:03:16,319 --> 00:03:17,399
<b>um Hackathon</b>

219
00:03:17,399 --> 00:03:18,479
<b>promovemos um Hackathon.</b>

220
00:03:18,479 --> 00:03:20,319
<b>Eu era do IAB na época ainda</b>

221
00:03:20,319 --> 00:03:23,000
<b>IAB é o Instituto Arquitetos do Brasil</b>

222
00:03:23,000 --> 00:03:24,720
<b>Eu era do IAB Alagoas, era presidente</b>

223
00:03:24,720 --> 00:03:27,039
<b>já tinha sido duas gestões vice-presidente</b>

224
00:03:27,039 --> 00:03:29,119
<b>e nessa gestão fui presidente do IAB.</b>

225
00:03:29,119 --> 00:03:32,479
<b>E o IAB puxou junto com a ONU Habtat</b>

226
00:03:32,479 --> 00:03:34,439
<b>com o pessoal da ONU Habtat aqui em Maceió</b>

227
00:03:34,439 --> 00:03:38,279
<b>e junto com o DETRAN Alagoas</b>

228
00:03:38,279 --> 00:03:39,239
<b>e a Unit</b>

229
00:03:39,239 --> 00:03:43,439
<b>puxou um evento que era dentro do circuito urbano da ONU</b>

230
00:03:43,439 --> 00:03:45,600
<b>internacional.</b>

231
00:03:45,600 --> 00:03:48,039
<b>Nós puxamos um evento que foi chamado na época</b>

232
00:03:48,039 --> 00:03:51,520
<b>de Encontro dos Pensadores Urbanos de Alagoas</b>

233
00:03:51,520 --> 00:03:54,239
<b>e esse evento tinha três eixos</b>

234
00:03:54,239 --> 00:03:56,399
<b>cada instituição respondia por um eixo</b>

235
00:03:56,760 --> 00:03:59,600
<b>um dos eixos era resiliência urbana</b>

236
00:03:59,600 --> 00:04:02,039
<b>e quem ficou responsável por esse eixo foi o IAB.</b>

237
00:04:02,039 --> 00:04:06,159
<b>Eu sugeri que nós focassemos esse eixo inteiro</b>

238
00:04:06,159 --> 00:04:07,479
<b>na questão dos bairros</b>

239
00:04:07,479 --> 00:04:09,600
<b>que na epoca eram três.</b>

240
00:04:09,600 --> 00:04:10,600
<b>Era o Pinheiro</b>

241
00:04:10,600 --> 00:04:14,399
<b>e tinha acabado de entrar Bebedouro e Mutange</b>

242
00:04:14,399 --> 00:04:17,399
<b>Então eu sugeri que nós focássemos</b>

243
00:04:17,399 --> 00:04:20,760
<b>nesses três bairros a questão da resiliência urbana</b>

244
00:04:20,760 --> 00:04:22,119
<b>fizemos uma mesa</b>

245
00:04:22,119 --> 00:04:24,880
<b>tendo a participação da defesa civil</b>

246
00:04:24,880 --> 00:04:29,279
<b>a participação de representantes dos moradores</b>

247
00:04:29,279 --> 00:04:31,920
<b>a participação da universidade</b>

248
00:04:31,920 --> 00:04:34,479
<b>então assim, forma-se que reuniu varios</b>

249
00:04:34,479 --> 00:04:36,000
<b>pontos de vista da questão</b>

250
00:04:36,000 --> 00:04:37,880
<b>e nós fizemos uma atividade</b>

251
00:04:37,880 --> 00:04:39,359
<b>que foi o Hackathon.</b>


1
00:00:01,334 --> 00:00:02,877
O primeiro impacto ali

2
00:00:02,877 --> 00:00:05,547
foi no Trapiche
porque o Trapiche era um bairro.

3
00:00:06,631 --> 00:00:08,717
Que seria uma espécie de hoje

4
00:00:08,717 --> 00:00:12,554
uma ponta verde. Uma classe média alta
estava se deslocando para lá

5
00:00:14,347 --> 00:00:16,641
morada à beira mar.

6
00:00:16,641 --> 00:00:19,394
Próximo do centro da cidade

7
00:00:19,394 --> 00:00:21,479
havia uma valorização enorme do bairro

8
00:00:22,605 --> 00:00:24,774
com inclusive o Trapichão foi para lá.

9
00:00:24,941 --> 00:00:27,569
Em 70 foi inaugurado o Trapichão.

10
00:00:28,528 --> 00:00:31,740
Eu acho que em...

11
00:00:31,740 --> 00:00:34,367
dez anos depois, 79, 78, por aí

12
00:00:34,367 --> 00:00:36,828
foi inaugurado a unidade de emergência

13
00:00:38,329 --> 00:00:39,914
e havia um deslocamento

14
00:00:39,914 --> 00:00:43,251
para aquela área
o quartel do Batalhão Metropolitano

15
00:00:43,460 --> 00:00:46,421
e de policiamento
ostensivo também na área do Trapiche.

16
00:00:47,130 --> 00:00:50,508
Então era um bairro expansão
e como meu pai

17
00:00:50,550 --> 00:00:54,387
foi corretor de imóveis e vendeu terrenos
ali eu tinha essa compreensão

18
00:00:54,512 --> 00:00:58,224
de que era um...
e tinha muitos amigos lá também

19
00:00:59,142 --> 00:01:02,187
havia o surf nascendo em Maceió

20
00:01:02,187 --> 00:01:05,315
O surf nasceu no Trapiche praticamente ali
na Praia do Sobral, Trapiche.

21
00:01:06,399 --> 00:01:06,691
Então

22
00:01:06,691 --> 00:01:11,571
havia toda uma valorização daquela área
e quando surgiram as primeiras informações

23
00:01:11,571 --> 00:01:14,783
sobre a implantação da Salgema
lá no Pontal

24
00:01:15,742 --> 00:01:18,036
houve claro uma reação a isso

25
00:01:18,203 --> 00:01:21,414
houve uma discussão inicial.

26
00:01:21,414 --> 00:01:26,252
E isso claro não vivíamos numa época

27
00:01:26,252 --> 00:01:32,175
em que a democracia não era a moda
e isso foi sufocado.

28
00:01:32,300 --> 00:01:35,595
Não houve grandes reações
porque havia todo um discurso

29
00:01:35,595 --> 00:01:40,058
de que o desenvolvimento seria..
iria compensar e que havia uma segurança.

30
00:01:40,725 --> 00:01:43,269
Os estudos iniciais o Beroaldo Maia Gomes

31
00:01:43,269 --> 00:01:46,272
que acompanhou esse processo

32
00:01:46,272 --> 00:01:48,358
inicial, ele já é falecido.

33
00:01:49,609 --> 00:01:50,652
Ele teve...

34
00:01:50,652 --> 00:01:54,030
recebeu uma equipe dos Estados Unidos
que veio analisar a localização.

35
00:01:55,115 --> 00:01:58,910
E eles garantiram que não havia risco
algum. então eles que compraram essa...

36
00:01:58,910 --> 00:02:02,497
O Beroaldo tentou tirar de lá né.?
Ainda tentou tirar. Ele...

37
00:02:03,039 --> 00:02:05,708
se esforçou para colocar em outro
local mas não conseguiu.

38
00:02:06,709 --> 00:02:08,962
Aí com a implantação da Salgema

39
00:02:08,962 --> 00:02:12,590
o maior e o risco que ficou foi...
a dúvida era...

40
00:02:13,800 --> 00:02:15,135
Vai explodir?

41
00:02:15,677 --> 00:02:19,305
Até o primeiro momento era o medo
da explosão. Então quem morava na vizinhança

42
00:02:19,639 --> 00:02:22,475
claro que tinha medo das
explosões, de ser atingindo

43
00:02:22,976 --> 00:02:26,271
principalmente
os moradores do Pontal da Barra.

44
00:02:27,647 --> 00:02:31,151
A vila de pescadores então,
não era o que é hoje. Era bem menor

45
00:02:31,151 --> 00:02:34,696
e eram geralmente só de pescadores,
com pouco artesanato.

46
00:02:36,364 --> 00:02:39,367
Mas aí um segundo momento
era que, não era a explosão.

47
00:02:39,742 --> 00:02:42,954
Então aí a coisa piorou
porque se houvesse um vazamento

48
00:02:43,037 --> 00:02:45,748
do gás clorídrico

49
00:02:46,332 --> 00:02:49,878
esse gás ele se espalharia
dependendo da época do ano

50
00:02:50,295 --> 00:02:53,590
Se tivesse soprando um vento sul
ou sudeste

51
00:02:54,215 --> 00:02:57,260
esse gás seria levado em direção
ao Trapiche da Barra

52
00:02:57,343 --> 00:03:01,181
Prado, naquela região ali,
acho que chegaria na Coreia.

53
00:03:02,348 --> 00:03:03,266
E imagine

54
00:03:03,266 --> 00:03:05,351
o gás clorídrico quando você respira.

55
00:03:06,311 --> 00:03:10,440
Você tem... a nossa mucosa
tem água, o H2O.

56
00:03:11,149 --> 00:03:13,735
Então haveria uma junção desse.

57
00:03:14,485 --> 00:03:17,822
desse oxigênio
e haveria a formação do ácido clorídrico.

58
00:03:18,990 --> 00:03:22,869
Então você morreria queimando por dentro
com ácido. Onde passasse o gás

59
00:03:23,369 --> 00:03:26,581
ia se transformando em ácido
e você morreria queimando por dentro.

60
00:03:27,540 --> 00:03:28,791
Pelo ácido.

61
00:03:29,375 --> 00:03:32,295
Isso trouxe um certo medo também.

62
00:03:32,295 --> 00:03:35,423
Além do medo da explosão
havia essa preocupação

63
00:03:35,423 --> 00:03:39,636
com o ácido clorídrico.
Mas tudo isso foi sendo superado.

64
00:03:40,345 --> 00:03:44,766
E depois de um certo tempo até 82
se não me engano

65
00:03:44,766 --> 00:03:48,770
quando houve uma explosão e morreu
um funcionário terceirizado.

66
00:03:50,271 --> 00:03:54,317
Essa coisa voltou novamente
a ser discutida e voltou a ser discutida e

67
00:03:55,568 --> 00:03:57,612
e daí em diante

68
00:03:58,071 --> 00:04:00,990
como coincidia isso com a iniciativa

69
00:04:01,741 --> 00:04:06,663
de duplicar a produção da planta
de diocloretano

70
00:04:07,080 --> 00:04:11,251
porque quando a Salgema começou a trabalhar
ela cobrava o Polo Cloroquímico.

71
00:04:12,418 --> 00:04:14,462
Aí quando surgiu o Polo Cloroquímico

72
00:04:14,462 --> 00:04:17,215
o Polo Cloroquímico
cobrava a duplicação da Salgema

73
00:04:18,091 --> 00:04:20,885
Ou seja: a quantidade fornecida
era tão pouca que não permitia

74
00:04:20,885 --> 00:04:23,263
então esse jogo de lá e cá.

75
00:04:24,430 --> 00:04:28,685
E foi quando surgiu então essa a notícia
de que haveria duplicação

76
00:04:28,685 --> 00:04:29,644
aí houve uma reação.


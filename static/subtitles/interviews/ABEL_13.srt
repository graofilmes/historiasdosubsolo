1
00:00:00,519 --> 00:00:02,880
<b>Para se fazer uma mineração dessas</b>

2
00:00:02,880 --> 00:00:06,079
<b>primeiro se faz um projeto piloto.</b>

3
00:00:06,079 --> 00:00:07,960
<b>Mas sabem como é esse projeto piloto, gente?</b>

4
00:00:07,960 --> 00:00:11,440
<b>vai lá no campo e perfura lá mil metros</b>

5
00:00:11,440 --> 00:00:13,880
<b>e faz então as cavernas.</b>

6
00:00:13,880 --> 00:00:17,800
<b>Isso calculado, tudo calculado.</b>

7
00:00:17,800 --> 00:00:20,760
<b>Aí faz uma caverna aqui, outra lá a 100m</b>

8
00:00:20,760 --> 00:00:22,199
<b>e faz ali pelo menos três.</b>

9
00:00:22,199 --> 00:00:23,880
<b>outra a 80m e tal</b>

10
00:00:24,639 --> 00:00:27,880
<b>Para ver as consequências na superfície.</b>

11
00:00:29,440 --> 00:00:30,559
<b>Projeto piloto.</b>

12
00:00:31,199 --> 00:00:33,280
<b>É só para a experiência.</b>

13
00:00:34,400 --> 00:00:35,880
<b>Adivinha se foi feito isso aqui?</b>

14
00:00:38,079 --> 00:00:41,119
<b>Mesmo sabendo que era necessário fazer</b>

15
00:00:41,119 --> 00:00:42,480
<b>vamos dizer que no começo não</b>

16
00:00:42,480 --> 00:00:43,880
<b>vamos dizer que havia.... </b>

17
00:00:43,880 --> 00:00:46,159
<b>Talvez lá os Americanos já sabiam</b>

18
00:00:46,159 --> 00:00:48,119
<b>visivelmente, a quantos anos atrás...</b>

19
00:00:48,119 --> 00:00:50,599
<b>Os Americanos estão muito na frente da gente, né?</b>

20
00:00:50,599 --> 00:00:54,199
<b>Aliás, a Braskem aqui é toda estrangeira, né?</b>

21
00:00:54,199 --> 00:00:58,559
<b>A consultora é estrangeira, né?</b>

22
00:00:59,519 --> 00:01:01,559
<b>Então aí, por que pelo  menos</b>

23
00:01:01,559 --> 00:01:03,679
<b>de vinte anos pra cá não procurou mudar?</b>

24
00:01:03,679 --> 00:01:07,239
<b>pelo menos frear os tamanhos dos diametros, não é verdade?</b>


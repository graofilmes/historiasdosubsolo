1
00:00:01,292 --> 00:00:04,045
Tem um problema muito grave que cerca

2
00:00:04,713 --> 00:00:07,257
tudo o que a gente poderia chamar

3
00:00:07,507 --> 00:00:09,843
de licenciamento da obra

4
00:00:10,552 --> 00:00:11,761
que é o que?

5
00:00:12,220 --> 00:00:14,514
Mineração. Toda a atividade
de mineração

6
00:00:15,432 --> 00:00:18,893
é uma atividade que produz impactos.

7
00:00:19,227 --> 00:00:21,896
Toda atividade humana produz impactos.

8
00:00:21,896 --> 00:00:23,314
Mas no caso da mineração,

9
00:00:23,314 --> 00:00:24,649
ela produz impactos

10
00:00:24,733 --> 00:00:27,444
que são pactos com magnitude

11
00:00:27,819 --> 00:00:28,987
seja média ou alta.

12
00:00:29,362 --> 00:00:30,572
Nunca é baixa.

13
00:00:31,656 --> 00:00:35,076
E no caso específico da Braskem

14
00:00:36,119 --> 00:00:38,538
o licenciamento ambiental

15
00:00:38,538 --> 00:00:42,083
que permitiu essa mineração.

16
00:00:42,959 --> 00:00:45,086
Ela ocorreu num período prévio

17
00:00:45,712 --> 00:00:47,922
à Constituição de 1988.

18
00:00:49,090 --> 00:00:50,800
Porque isso é importante:

19
00:00:50,800 --> 00:00:53,595
Porque a partir da Constituição de 1988

20
00:00:53,928 --> 00:00:56,931
todos os procedimentos que vão orientar

21
00:00:56,931 --> 00:01:00,226
o licenciamento ambiental
tiveram sustentação constitucional.

22
00:01:01,019 --> 00:01:05,065
Embora a resolução CONAMA date ainda
do período da ditadura militar no Brasil

23
00:01:06,191 --> 00:01:08,193
Acho que 1980

24
00:01:09,903 --> 00:01:10,820
as orientações

25
00:01:10,820 --> 00:01:13,907
que vão permitir a gente
pensar por exemplo:

26
00:01:13,907 --> 00:01:16,367
sobre esses atingidos,

27
00:01:16,367 --> 00:01:17,077
direitos

28
00:01:17,077 --> 00:01:20,997
as consultas às populações que
poderiam ser afetadas

29
00:01:21,414 --> 00:01:22,582
não ocorreu.
Não ocorreu de forma que a gente

30
00:01:22,624 --> 00:01:25,376
Não ocorreu de forma que a gente

31
00:01:25,376 --> 00:01:27,087
pense de maneira inclusiva como eu disse.

32
00:01:27,087 --> 00:01:27,837
Como eu disse.

33
00:01:28,713 --> 00:01:31,883
Essas áreas,
esses bairros já eram povoados.

34
00:01:32,884 --> 00:01:34,219
Só que esse tipo de mineração...

35
00:01:34,552 --> 00:01:35,929
a mineração tem várias formas.

36
00:01:35,929 --> 00:01:37,889
Mas essa mineração em especial

37
00:01:38,973 --> 00:01:40,517
ela ocorre

38
00:01:40,558 --> 00:01:42,519
não no nível da superfície

39
00:01:42,727 --> 00:01:43,436
mas abaixo da superfície.

40
00:01:44,813 --> 00:01:46,564
Antes você tinha moradores ali.

41
00:01:46,564 --> 00:01:48,900
E aí quais são as maiores falhas

42
00:01:49,526 --> 00:01:51,152
de todo esse problema?

43
00:01:51,152 --> 00:01:54,531
É que quando você vai obter uma licença ambiental,

44
00:01:55,240 --> 00:01:57,534
você tem de entregar o estudo de impacto ambiental.

45
00:01:58,827 --> 00:02:01,329
E você precisa entregar
um relatório de impacto ambiental

46
00:02:01,579 --> 00:02:04,541
enquanto o estudo de impacto ambiental
precisa ser técnico,

47
00:02:05,750 --> 00:02:08,378
tem que ser bem rigoroso e técnico

48
00:02:08,461 --> 00:02:11,965
o Relatório de Impacto Ambiental
ele tem que ter uma linguagem acessível

49
00:02:12,799 --> 00:02:15,969
que eu falo com meus alunos que você
tem que fazer com que sua avó compreenda

50
00:02:16,761 --> 00:02:19,514
o tamanho do problema que possa existir.

51
00:02:19,764 --> 00:02:20,890
E aí aproveitando

52
00:02:21,224 --> 00:02:23,852
esse: o tamanho do problema que possa existir

53
00:02:24,978 --> 00:02:28,356
uma das maiores falhas envolvendo
nesse caso do Braskem

54
00:02:28,648 --> 00:02:32,235
é que você tem que fazer
a identificação dos impactos.

55
00:02:33,653 --> 00:02:36,072
E você precisa fazer uma previsão nos impactos.

56
00:02:36,781 --> 00:02:38,408
Precisa prever

57
00:02:39,033 --> 00:02:40,660
então a partir dessa minha atividade

58
00:02:40,910 --> 00:02:42,495
que tipo de impacto pode ocorrer?

59
00:02:44,539 --> 00:02:45,415
Quais são os impactos?

60
00:02:46,291 --> 00:02:49,794
E aí você precisa criar medidas de mitigação,

61
00:02:49,794 --> 00:02:51,462
de contenção,

62
00:02:51,462 --> 00:02:52,463
redução.

63
00:02:52,463 --> 00:02:54,883
Você tem que criar formas
de fazer com que

64
00:02:54,883 --> 00:02:57,427
esses impactos sejam minimizados.

65
00:02:58,678 --> 00:02:59,804
Mas além disso

66
00:02:59,804 --> 00:03:02,932
você precisa informar a população.

67
00:03:04,058 --> 00:03:06,186
E isso demanda o que? Monitoramento.

68
00:03:07,020 --> 00:03:09,814
Porque você precisa informar
a população dos riscos

69
00:03:10,481 --> 00:03:13,985
que aquela atividade traz.

70
00:03:15,361 --> 00:03:17,488
Quais são os riscos que essa atividade traz?

71
00:03:18,573 --> 00:03:21,701
E aí quando a gente vai observar
o comportamento da empresa

72
00:03:22,702 --> 00:03:24,996
com os moradores

73
00:03:24,996 --> 00:03:27,832
esses riscos. A previsão dos impactos

74
00:03:27,832 --> 00:03:28,875
esses riscos.

75
00:03:29,000 --> 00:03:31,753
O detalhamento desses riscos
e o monitoramento

76
00:03:33,463 --> 00:03:36,049
não ocorreu. Não ocorreu.

77
00:03:36,799 --> 00:03:39,886
Então tem muito morador e muita moradora

78
00:03:40,053 --> 00:03:43,056
que simplesmente não tinha uma noção clara

79
00:03:43,806 --> 00:03:46,059
que morava na área de risco.

80
00:03:47,268 --> 00:03:49,979
Isso não é responsabilidade do morador

81
00:03:50,438 --> 00:03:53,399
daquela moradora que está ali há 60 anos.

82
00:03:54,108 --> 00:03:57,195
Isso é responsabilidade da empresa

83
00:03:57,195 --> 00:03:59,364
que produz os impactos.

84
00:03:59,822 --> 00:04:01,532
E o órgão ambiental,

85
00:04:01,658 --> 00:04:03,159
o orgão licenciador

86
00:04:03,159 --> 00:04:04,827
precisa fazer o monitoramento.

87
00:04:05,453 --> 00:04:09,374
A depender do tipo de empreendimento
você tem que pensar.

88
00:04:09,707 --> 00:04:13,044
Você deve fazer isso
por uma questão, até normativo

89
00:04:13,419 --> 00:04:16,089
que você deve pensar a cadeia de riscos

90
00:04:17,131 --> 00:04:19,175
e o que ocorreu

91
00:04:19,300 --> 00:04:22,762
efetivamente, foi que os impactos

92
00:04:23,304 --> 00:04:24,681
que foram causados

93
00:04:24,681 --> 00:04:25,723
e estão gerando

94
00:04:25,723 --> 00:04:28,351
toda essa adiversidade agora

95
00:04:28,559 --> 00:04:31,604
vieram por conta do afundamento do solo.

96
00:04:33,731 --> 00:04:34,440
Então a Braskem

97
00:04:34,440 --> 00:04:38,194
ela não tinha efetivamente políticas

98
00:04:38,611 --> 00:04:41,990
do ponto de vista de responsabilidade socioambiental

99
00:04:43,074 --> 00:04:45,868
com os moradores.

100
00:04:45,868 --> 00:04:47,954
Porque você tem que prevê.

101
00:04:48,621 --> 00:04:49,956
É por isso que eu reforçei aqui:

102
00:04:49,956 --> 00:04:51,749
você tem que fazer a identificação do impacto.

103
00:04:52,208 --> 00:04:53,835
Você tem que fazer a previsão.

104
00:04:53,835 --> 00:04:56,212
Você tem que prever os impactos.

105
00:04:56,212 --> 00:04:58,464
E você deve depois, no final das contas

106
00:04:58,464 --> 00:05:00,842
fazer uma avaliação da importância desses impactos.

107
00:05:00,842 --> 00:05:04,429
Porque você tem que escalonar:
O que é prioridade?

108
00:05:05,555 --> 00:05:08,474
Essa prioridade tem que refletir la nas comunidades.

109
00:05:09,684 --> 00:05:11,311
E você tem que: a empresa....

110
00:05:11,311 --> 00:05:12,729
Não é porque a empresa é boazinha.

111
00:05:13,354 --> 00:05:16,065
É porque ela deve ter um canal de diálogo

112
00:05:16,065 --> 00:05:18,693
mas não é de monólogo
que é o caso da Braskem.

113
00:05:19,569 --> 00:05:21,946
Não sei se vai mudar com esse desastre

114
00:05:22,196 --> 00:05:24,032
mas geralmente o que ocorre?

115
00:05:24,032 --> 00:05:25,950
são monólogos

116
00:05:26,617 --> 00:05:29,454
em que a empresa está aqui
 e ela dita.

117
00:05:29,996 --> 00:05:33,791
geralmente alguns recursos institucionais
por exemplo:

118
00:05:34,709 --> 00:05:37,879
se eu falo para vocês
que a resolução de conflito

119
00:05:38,588 --> 00:05:40,340
é uma coisa positiva.

120
00:05:40,548 --> 00:05:42,425
ela é

121
00:05:42,425 --> 00:05:44,510
a busca por consensos etc.

122
00:05:44,802 --> 00:05:47,388
Só que isso também pode ser usado
contra as comunidades.

123
00:05:47,972 --> 00:05:49,307
Isso não é usado contra a empresa.

124
00:05:49,474 --> 00:05:52,101
Isso aí tá prolongando um processo

125
00:05:53,353 --> 00:05:56,773
para esgotar a solidariedade.

126
00:05:57,106 --> 00:05:59,317
Porque aí você divide para reinar.

127
00:05:59,317 --> 00:06:01,861
Você esgota a solidariedade do grupo.

128
00:06:02,945 --> 00:06:05,448
E aí você reduz uma série de outras situações

129
00:06:05,448 --> 00:06:07,867
por exemplo: valores de indenização,

130
00:06:08,451 --> 00:06:10,787
recursos sociais.
Vamos chamar assim.

131
00:06:10,787 --> 00:06:14,874
por exemplo: que por conta dos impactos produzidos

132
00:06:14,874 --> 00:06:19,003
 a empresa vai ser responsabilizada
pela construção de:

133
00:06:19,796 --> 00:06:22,048
equipamentos públicos sociais.

134
00:06:22,048 --> 00:06:23,466
Investimentos por exemplo

135
00:06:23,800 --> 00:06:27,095
numa poupança social
que vai investir na formação

136
00:06:27,095 --> 00:06:29,013
das crianças e adolescentes.

137
00:06:29,013 --> 00:06:30,765
Então é uma série de iniciativas que são possíveis.

138
00:06:31,099 --> 00:06:33,518
Mas geralmente isso não ocorre.

139
00:06:33,518 --> 00:06:38,022
Você esgota a solidariedade do grupo
para fazer com que cada um comece a pensar

140
00:06:38,940 --> 00:06:40,858
no benefício individual

141
00:06:40,858 --> 00:06:45,279
e isso aí é aplicar alguns procedimentos

142
00:06:47,115 --> 00:06:48,366
aprendidos lá no...

143
00:06:49,075 --> 00:06:50,827
na sala de aula de psicologia.

144
00:06:50,827 --> 00:06:54,580
A gente está vendo que assim...
que faltou profissionalismo da empresa,

145
00:06:55,706 --> 00:06:58,334
porque toda essa parte de AIA...

146
00:06:58,835 --> 00:07:04,507
e me espanta,
me espanta principalmente a falta de...

147
00:07:05,883 --> 00:07:08,553
quando você vai falar dessa parte de AIA

148
00:07:08,553 --> 00:07:09,720
é a parte mais importante.

149
00:07:09,720 --> 00:07:10,721
Não tem nenhuma outra.

150
00:07:11,931 --> 00:07:14,434
A parte mais importante
é essa que determina para vocês

151
00:07:14,434 --> 00:07:17,812
porque ela é muito...
ela tem relação direta com o bem estar

152
00:07:18,479 --> 00:07:22,191
porque os impactos bióticos,
 eles são: não são tão significativos

153
00:07:22,483 --> 00:07:25,570
quanto os impactos físicos e antrópicos
que são os impactos

154
00:07:26,654 --> 00:07:28,531
na população.

155
00:07:28,614 --> 00:07:30,783
E aí a gente vê o que?

156
00:07:30,783 --> 00:07:32,410
A gente vê uma falta de...

157
00:07:32,410 --> 00:07:36,539
de realmente de profissionalismo
e profissionalismo da empresa.

158
00:07:36,956 --> 00:07:39,750
de realmente fazer

159
00:07:39,750 --> 00:07:44,797
uma identificação clara, uma previsão clara,
uma avaliação de importância clara dos impactos

160
00:07:44,797 --> 00:07:47,884
os meios de reduzir.
Quais seriam os maiores riscos...

161
00:07:48,759 --> 00:07:52,263
que aquele empreendimento
vai tcausar aquelas comunidades.

162
00:07:53,139 --> 00:07:56,559
porque o que ocorreu basicamente
foi uma reação a toque de caixa

163
00:07:58,060 --> 00:08:02,231
ou seja quase uma uma reação voluntarista

164
00:08:02,648 --> 00:08:07,236
que isso infelizmente foi acabar
por exemplo atingindo outras esferas.


1
00:00:00,208 --> 00:00:01,626
Eu sou favorável

2
00:00:01,626 --> 00:00:04,004
a transformar realmente
fazer um grande corredor

3
00:00:04,045 --> 00:00:05,255
pegar até a...

4
00:00:05,255 --> 00:00:07,340
parte do Ibama, que não é tão longe

5
00:00:07,340 --> 00:00:08,216
o Parque Municipal ali

6
00:00:08,216 --> 00:00:11,344
e fazer um grande, uma grande área verde

7
00:00:12,512 --> 00:00:13,722
na região.

8
00:00:14,639 --> 00:00:17,100
O que valerá provavelmente
como eu já comentei aqui

9
00:00:18,143 --> 00:00:19,310
ah, tem...

10
00:00:19,310 --> 00:00:21,479
tem áreas por exemplo: que o risco é menor

11
00:00:21,521 --> 00:00:22,605
tem áreas por exemplo: que não...

12
00:00:22,605 --> 00:00:24,816
não foram identificados como áreas
por exemplo

13
00:00:26,401 --> 00:00:29,487
em que o afundamento do solo ele vai...

14
00:00:30,071 --> 00:00:32,490
ele vai ocorrer etc.

15
00:00:33,283 --> 00:00:34,117
Aí é uma decisão

16
00:00:34,117 --> 00:00:36,995
porque aí é uma decisão
que não é só técnica no final das contas.

17
00:00:37,120 --> 00:00:38,997
É uma decisão política também.

18
00:00:38,997 --> 00:00:43,168
Então ou você toma a decisão
a partir de um critério de risco.

19
00:00:44,335 --> 00:00:47,881
Ou você toma uma decisão
a partir de um critério por exemplo

20
00:00:48,506 --> 00:00:53,845
puramente econômico ou seja transformar
algumas áreas por exemplo como áreas

21
00:00:54,846 --> 00:00:56,890
recreativas

22
00:00:56,890 --> 00:00:59,267
ou até áreas com ocupação regular.

23
00:01:00,560 --> 00:01:04,689
Só que isso vai gerar um efeito perverso

24
00:01:05,523 --> 00:01:08,109
que ainda é claro
que é um sentimento de animosidade.

25
00:01:08,318 --> 00:01:12,781
E eu acho injusto quando por exemplo
alguns moradores identificarem

26
00:01:12,781 --> 00:01:17,827
que onde eram suas casas agora vai estar
uns belos casarões lá

27
00:01:17,827 --> 00:01:20,455
se por exemplo voltar a ter destinação

28
00:01:22,457 --> 00:01:23,833
para moradia.

29
00:01:23,833 --> 00:01:27,087
Eu vou defender assim, até eu ser abduzido

30
00:01:27,212 --> 00:01:29,506
ser levado para Marte

31
00:01:30,048 --> 00:01:32,342
que o ideal é você fazer uma área verde

32
00:01:33,134 --> 00:01:35,804
e aí nas áreas onde por exemplo
você pode não ter

33
00:01:36,846 --> 00:01:39,057
o risco por exemplo: é baixo

34
00:01:39,057 --> 00:01:42,685
a condição geológica
é propícia para o afundamento.

35
00:01:43,895 --> 00:01:46,439
Aí você tem equipamentos públicos recreativos

36
00:01:46,523 --> 00:01:49,526
mas públicos, não privados.

37
00:01:49,526 --> 00:01:54,405
Mas se na hora você...
quem está tomando a decisão vê

38
00:01:54,405 --> 00:01:58,326
que há uma possibilidade de 20 e 30 anos
você aumentar o risco. Nem isso.

39
00:01:58,743 --> 00:02:00,370
Só uma área verde mesmo.

40
00:02:01,037 --> 00:02:02,330
E aí essa área verde

41
00:02:02,747 --> 00:02:06,709
pode ter áreas com destinação
recreativa ou não

42
00:02:07,710 --> 00:02:08,461
E aí assim...

43
00:02:08,461 --> 00:02:09,712
e o pior sabe que o que é que é?

44
00:02:10,046 --> 00:02:11,631
o pior você pode fazer...
 como um carro onde você pode criar

45
00:02:11,631 --> 00:02:13,466
economicamente você pode criar...

46
00:02:14,008 --> 00:02:16,010
um ativo econômico com isso.

47
00:02:16,177 --> 00:02:19,264
Por exemplo: capacidade
de sequestro de carbono numa área enorme

48
00:02:19,556 --> 00:02:22,976
dá para fazer um projeto
de Mecanismo de Desenvolvimento Limpo.

49
00:02:23,393 --> 00:02:26,271
Dá para você fazer, até de certa forma

50
00:02:26,646 --> 00:02:29,065
Nem um MDS seria o ideal. Um reddit.

51
00:02:29,399 --> 00:02:30,859
Então assim: tem...

52
00:02:32,193 --> 00:02:35,280
talvez a magnitude não permita
mas você...

53
00:02:35,280 --> 00:02:40,160
há possibilidades de você
otimizar economicamente essas áreas verdes

54
00:02:40,326 --> 00:02:42,287
uma infraestrutura verde.


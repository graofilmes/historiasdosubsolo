1
00:00:00,750 --> 00:00:04,629
Eu trabalho nessa área de avaliação
de impactos ambientais

2
00:00:05,588 --> 00:00:08,091
e minhas pesquisas e estudos iniciais

3
00:00:08,091 --> 00:00:11,261
eram sobre avaliação
de impacto da mineração.

4
00:00:12,262 --> 00:00:16,891
Só que no caso específico,
eu trabalhei com casos da mineração

5
00:00:17,392 --> 00:00:20,937
na Amazônia brasileira na Colômbia
e no Peru

6
00:00:22,397 --> 00:00:26,651
e tive uma experiência socialmente,

7
00:00:26,651 --> 00:00:28,862
posso até falar um pouco

8
00:00:29,696 --> 00:00:34,659
desgastante, pelo tipo de...
que é um padrão quando envolve a mineração

9
00:00:35,326 --> 00:00:38,288
e seja mineração médio ou grande escala.

10
00:00:38,621 --> 00:00:40,206
Ela tem um padrão que...

11
00:00:40,874 --> 00:00:43,460
quando você acompanha você
vê que afeta muito

12
00:00:43,918 --> 00:00:45,003
muito

13
00:00:46,588 --> 00:00:49,257
moradores das áreas. Que a gente em AIA.

14
00:00:49,841 --> 00:00:53,053
Na Avaliação de Impactos Ambientais
a gente chama de moradores...

15
00:00:53,219 --> 00:00:56,222
seja da área diretamente afetada
o da área de influência

16
00:00:56,222 --> 00:00:59,017
direta ou indireta e...

17
00:01:00,727 --> 00:01:04,022
eu parei, eu me cansei. Para falar a verdade...

18
00:01:05,023 --> 00:01:07,692
com o tema.

19
00:01:07,692 --> 00:01:09,778
Até que nesse caso aqui

20
00:01:09,778 --> 00:01:14,824
de Maceió eu particularmente não esperava
um caso de mineração

21
00:01:14,824 --> 00:01:18,203
que gerasse um impacto tão significativo

22
00:01:18,203 --> 00:01:22,290
com a magnitude que tomou
e que tomou aqui em Maceió. Aqui na cidade.

23
00:01:22,999 --> 00:01:25,210
Porque geralmente esses...

24
00:01:25,502 --> 00:01:28,213
casos, eles ocorrem em áreas mais afastadas.

25
00:01:28,963 --> 00:01:31,633
Não são em áreas urbanas por exemplo.

26
00:01:32,675 --> 00:01:36,679
E eu acabei recebendo um convite em 2019

27
00:01:36,805 --> 00:01:38,056
eu acho, eu não lembro.

28
00:01:38,723 --> 00:01:41,726
para participar de uma iniciativa da

29
00:01:41,726 --> 00:01:44,813
ONU Habitat aqui
no seu escritório aqui em Maceió

30
00:01:45,522 --> 00:01:48,149
para participar do que seria o Encontro
de Pensadores Urbanos

31
00:01:48,900 --> 00:01:53,530
e era para tratar justamente desse caso
chamado caso do Pinheiro.

32
00:01:53,571 --> 00:01:56,282
Mas vamos chamar o caso da Braskem
que foi a...

33
00:01:56,825 --> 00:01:59,994
porque a gente quando fala
do bairro ou dos bairros.

34
00:02:00,954 --> 00:02:03,289
Na verdade
a gente tem que dar nome ao boi

35
00:02:03,289 --> 00:02:06,668
porque quem está produzindo o impacto né?
Quem produziu impacto?

36
00:02:08,128 --> 00:02:10,338
E aí eu aceitei o convite.

37
00:02:10,463 --> 00:02:14,509
Mas quando você aceita o convite,
você fala mas eu vou retomar do tema de pesquisa?

38
00:02:15,635 --> 00:02:19,639
Só que essa experiência que eu tive...
eu tive um projeto financiado

39
00:02:19,639 --> 00:02:23,935
pelo CNPq em 2013
que me permitiu ir aos locais de mineração:

40
00:02:24,435 --> 00:02:28,106
Tem alguns padrões que surgem.
Como eu havia comentado

41
00:02:28,982 --> 00:02:31,651
e aqui eu não havia feito
pesquisa de campo ainda.

42
00:02:31,651 --> 00:02:35,029
Nenhum levantamento,
mas ou levantei algumas hipóteses

43
00:02:35,780 --> 00:02:41,035
Que era oque? Como era uma mineração em área urbana:
O que provavelmente iria ocorrer?

44
00:02:42,829 --> 00:02:46,291
Você ia ter um aumento
na notificação de transtorno de ansiedade,

45
00:02:46,583 --> 00:02:50,003
um aumento nos casos,
por exemplo relatados, de depressão

46
00:02:50,420 --> 00:02:54,215
um aumento nos casos
notificados de suicídio

47
00:02:54,424 --> 00:02:56,634
ideação suicida

48
00:02:57,927 --> 00:03:00,972
entre outros problemas, como por exemplo,
o aumento dos casos de hipertensão

49
00:03:01,139 --> 00:03:05,351
uma série de outras situações
que são impactos na saúde. Por conta do quê?

50
00:03:06,186 --> 00:03:10,190
Que esse caso aqui da Braskem
que atingiu esses bairros

51
00:03:11,065 --> 00:03:14,402
é numa área que era previamente
já densamente povoada

52
00:03:15,195 --> 00:03:17,989
então não era uma área
que começou a ser ocupada

53
00:03:18,406 --> 00:03:20,783
depois que as autorizações

54
00:03:21,451 --> 00:03:23,870
para mineração teriam sido dadas.

55
00:03:24,162 --> 00:03:26,831
Já existiam pessoas lá.

56
00:03:26,831 --> 00:03:29,209
E quando comecei minimamente

57
00:03:29,667 --> 00:03:33,963
a observar o que seriam as variáveis
nesse caso dos indicadores

58
00:03:33,963 --> 00:03:36,007
eu observei: olha vai acontecer isso.


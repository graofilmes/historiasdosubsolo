1
00:00:00,039 --> 00:00:03,119
Eu tinha passado 20 anos

2
00:00:03,119 --> 00:00:05,880
sediado na Bahia.

3
00:00:06,519 --> 00:00:09,960
Eu feito concurso lá para a universidade

4
00:00:10,679 --> 00:00:13,159
<b>e estava na Bahia sediado.</b>

5
00:00:13,880 --> 00:00:15,360
Mas durante esse período todo

6
00:00:15,360 --> 00:00:17,199
eu viajei muito porque

7
00:00:19,039 --> 00:00:22,239
eu fui muito convidado para congressos internacionais

8
00:00:22,239 --> 00:00:24,760
pra tratar de outros assuntos não é?

9
00:00:24,760 --> 00:00:26,960
Só para você ter uma ideia

10
00:00:26,960 --> 00:00:32,280
eu fui a Israel a convite e fui ao Irã

11
00:00:33,119 --> 00:00:35,440
também a convite.

12
00:00:35,440 --> 00:00:40,599
Mas minha sede era Feira de Santana

13
00:00:43,119 --> 00:00:43,840
<b>na Bahia.</b>

14
00:00:44,639 --> 00:00:48,119
Então, quando eu completei 50 anos

15
00:00:48,119 --> 00:00:50,239
de atividade no magistério

16
00:00:52,239 --> 00:00:54,599
trabalhando em pesquisa científica

17
00:00:54,599 --> 00:00:56,800
com ecologia e meio ambiente

18
00:00:56,800 --> 00:00:58,760
aí eu resolvi me aposentar.

19
00:01:00,679 --> 00:01:02,039
<b>Pedi a aposentadoria</b>

20
00:01:02,960 --> 00:01:04,480
<b>lá em Feira de Santana.</b>

21
00:01:05,199 --> 00:01:06,960
Não tinha nenhum motivo

22
00:01:06,960 --> 00:01:08,800
a não ser sentimental

23
00:01:08,800 --> 00:01:10,440
de voltar para Maceió.

24
00:01:11,280 --> 00:01:13,440
Nós tínhamos a casa do Pinheiro

25
00:01:13,440 --> 00:01:15,360
que era a nossa casa de família

26
00:01:15,360 --> 00:01:17,360
desde que nós casamos

27
00:01:17,360 --> 00:01:21,119
e de repouso e refúgio em férias

28
00:01:21,119 --> 00:01:23,519
 e diversão dos meus filhos

29
00:01:23,519 --> 00:01:25,159
que adoravam Maceió.

30
00:01:25,159 --> 00:01:27,360
E minha esposa sempre teve

31
00:01:27,360 --> 00:01:29,599
uma vinculação muito forte

32
00:01:30,280 --> 00:01:31,840
emocionalmente com Maceió

33
00:01:31,840 --> 00:01:33,119
inclusive familiar.

34
00:01:33,119 --> 00:01:36,320
Foi uma coisa meio intuitiva, aí voltei.

35
00:01:38,599 --> 00:01:40,639
Quando for mais ou menos com 1 mês

36
00:01:40,639 --> 00:01:42,119
que eu estava aqui

37
00:01:42,880 --> 00:01:44,519
<b>na casa do Pinheiro</b>

38
00:01:45,599 --> 00:01:49,480
então, olhe, eu faço cesta, não é?

39
00:01:49,480 --> 00:01:51,920
Estava fazendo minha cesta

40
00:01:52,920 --> 00:01:56,239
<b>quando eu escutei um estrondo.</b>

41
00:01:57,320 --> 00:02:02,199
Gente, não foi som de terremoto normal

42
00:02:02,199 --> 00:02:04,639
se é que existe terremoto normal.

43
00:02:04,639 --> 00:02:07,360
Foi um estrondo

44
00:02:07,360 --> 00:02:12,000
mas um estrondo muito alto, muito forte.

45
00:02:13,119 --> 00:02:14,480
<b>Eu não sabia o que era.</b>

46
00:02:15,159 --> 00:02:16,480
Então me levantei

47
00:02:16,480 --> 00:02:21,000
e cheguei na sacada é do mezanino, lá em casa

48
00:02:21,000 --> 00:02:25,519
e fiz a chamada da família.

49
00:02:25,519 --> 00:02:27,840
E minha esposa: Betinha, presente!

50
00:02:27,840 --> 00:02:30,800
Hortência presente! João Maurício, presente!

51
00:02:30,800 --> 00:02:32,760
Mas está todo mundo em casa?

52
00:02:32,760 --> 00:02:35,199
Então eu vou voltar a dormir.

53
00:02:38,039 --> 00:02:40,159
<b>E volte. Logo depois</b>

54
00:02:40,960 --> 00:02:43,639
a imprensa começou a noticiar

55
00:02:43,639 --> 00:02:48,639
na televisão que tinha havido um terremoto

56
00:02:48,639 --> 00:02:55,320
registrado com a potência de 2,1 e 2,9 por aí

57
00:02:55,320 --> 00:02:57,320
o que não é nada é normal.


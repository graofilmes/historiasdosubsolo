1
00:00:00,667 --> 00:00:02,669
Eu não quero que eese lugares se recuperem não

2
00:00:02,669 --> 00:00:03,795
sendo bem sincero

3
00:00:03,795 --> 00:00:07,841
eu quero que as pessoas tenham condições
de se recuperar

4
00:00:08,591 --> 00:00:12,053
por conta de que são áreas de risco.

5
00:00:13,263 --> 00:00:16,057
Aí você tem umas classificações...
tem uma técnica

6
00:00:16,933 --> 00:00:18,685
que a gente usa
que é de kernel

7
00:00:18,685 --> 00:00:21,354
que a gente separa por cores as áreas

8
00:00:21,354 --> 00:00:24,774
então por exemplo:
 mais vermelho vai ser mais risco, etc

9
00:00:25,608 --> 00:00:28,778
é uma área que ela não pode ter uma finalidade

10
00:00:29,654 --> 00:00:33,033
como foi historicamente usada
aquele espaço.

11
00:00:35,076 --> 00:00:39,330
A única coisa que eu considero
que seria mais adequado

12
00:00:39,414 --> 00:00:41,458
é realmente transformar aquela área.

13
00:00:42,333 --> 00:00:45,920
Toda essa área atingida numa área mata verde

14
00:00:47,005 --> 00:00:47,881
Então a criação de...

15
00:00:48,548 --> 00:00:51,634
uma grande área verde
que vai melhorar a qualidade

16
00:00:51,634 --> 00:00:54,679
do microclima na região

17
00:00:54,679 --> 00:00:56,431
até na cidade altera

18
00:00:58,558 --> 00:01:01,352
pelo menos, a priori.

19
00:01:01,352 --> 00:01:05,065
E aí eu estou trabalhando aqui, estou falando
para vocês com o princípio da precaução

20
00:01:06,191 --> 00:01:09,819
que é aquele velho ditado:
cachorro mordido de cobra tem medo de linguiça

21
00:01:10,070 --> 00:01:11,780
então não dá

22
00:01:11,780 --> 00:01:15,575
do ponto de vista do planejamento urbano
você destinar

23
00:01:15,909 --> 00:01:19,871
áreas que foram consideradas
como áreas de risco para moradia novamente

24
00:01:19,871 --> 00:01:23,041
igual por exemplo: lixão
que é uma coisa que acontece no Brasil

25
00:01:23,583 --> 00:01:26,586
o lixão está desativado
aí as pessoas constroem em cima.

26
00:01:26,920 --> 00:01:28,797
De repente acontece uma explosão no local.

27
00:01:29,339 --> 00:01:30,715
Agora, o que eu acredito

28
00:01:30,715 --> 00:01:32,467
que aí eu acho que falta

29
00:01:34,344 --> 00:01:35,929
mais diálogo

30
00:01:35,929 --> 00:01:38,598
principalmente ouvir de forma comunitária

31
00:01:39,307 --> 00:01:41,267
porque ouvir individualmente

32
00:01:41,643 --> 00:01:44,479
mas ouvir de forma comunitária
o que essas comunidades

33
00:01:44,854 --> 00:01:47,690
elas gostariam dos espaços.

34
00:01:48,066 --> 00:01:53,530
Porque um dos impactos sociais
mais tenebrosos da mineração

35
00:01:53,571 --> 00:01:56,282
é a quebra dos laços.

36
00:01:56,324 --> 00:01:57,951
entre as pessoas.

37
00:01:58,201 --> 00:02:00,120
Estão gente que viveu 30 anos

38
00:02:00,120 --> 00:02:06,417
conhece a vida da vizinha, o papagaio,
sabe que o vizinho ali chegou sexta-feira

39
00:02:06,459 --> 00:02:09,504
vai tomar cervejinha em frente de casa
conversando com os amigos

40
00:02:09,504 --> 00:02:12,132
todos esses laços são partidos.
São quebrados

41
00:02:12,465 --> 00:02:15,760
em especial quando a gente começa
a escalonar a idade.

42
00:02:16,177 --> 00:02:19,764
Então pegou a partir de tal idade
o impacto ele fica até maior.

43
00:02:20,807 --> 00:02:21,391
Então

44
00:02:22,308 --> 00:02:25,019
a falta daquela casa, a falta da rua
ele que sabe.

45
00:02:25,770 --> 00:02:27,897
Só que eu acho que a única forma
de você reduzir

46
00:02:27,897 --> 00:02:30,191
porque já é um impacto severo

47
00:02:31,651 --> 00:02:34,696
mas eu tenho a impressão de que
a única forma de você reduzir

48
00:02:34,696 --> 00:02:37,323
é você fazer reuniões de grupo

49
00:02:37,323 --> 00:02:40,869
com a comunidade, onde a comunidade
vem dizer o que ela gostaria.

50
00:02:41,202 --> 00:02:43,830
Só que infelizmente não foi o que aconteceu

51
00:02:43,830 --> 00:02:48,960
as indenizações são individuais.
Então você recebe pela sua casa.

52
00:02:49,419 --> 00:02:53,548
aí você pode comprar uma casa lá no...
sei lá: na Graça Torta

53
00:02:54,507 --> 00:02:57,093
ou você vai comprar lá no Biu

54
00:02:57,093 --> 00:02:59,846
ou você vai comprar sei lá onde.

55
00:03:00,305 --> 00:03:02,473
E aí se rompe.

56
00:03:02,473 --> 00:03:06,561
Mas eu acho que assim
quando eu faço um exercício de imaginação

57
00:03:07,020 --> 00:03:10,273
imaginar aquele espaço como um espaço

58
00:03:11,232 --> 00:03:16,279
em algumas áreas, até recreativo
ou seja com acesso à população

59
00:03:16,613 --> 00:03:20,742
podendo aproveitar aqueles espaços,
em algumas, não é em toda não, definitivamente.

60
00:03:21,534 --> 00:03:24,245
Mas eu não consigo imaginar

61
00:03:24,245 --> 00:03:26,789
fazer uma projeção do ponto de vista

62
00:03:27,582 --> 00:03:31,628
dos impactados e das impactadas
por conta do seguinte ponto.

63
00:03:33,671 --> 00:03:35,256
O pessoal usa muito pior do que eu

64
00:03:35,256 --> 00:03:38,635
por exemplo não sou dessa área de antropologia,
sociologia, sei lá

65
00:03:38,635 --> 00:03:39,886
tem o negócio do lugar de fala

66
00:03:40,511 --> 00:03:44,307
eu não consigo, eu imaginar...

67
00:03:44,974 --> 00:03:48,561
porque eu não sou diretamente atingido.

68
00:03:50,396 --> 00:03:52,982
E nem influência direta, ainda eu considero

69
00:03:54,192 --> 00:03:55,318
indireta sim.

70
00:03:56,027 --> 00:03:58,821
Então esse exercício
acho que da minha parte

71
00:03:58,821 --> 00:04:02,283
como pesquisador
ainda mais com a área técnica etc.

72
00:04:02,450 --> 00:04:05,245
E acho que no exercício
ele tem que ser de solidariedade

73
00:04:05,578 --> 00:04:08,623
principalmente para indicar:
vale a pena investir

74
00:04:11,793 --> 00:04:15,421
em algo mais comunitário, para se decidir
por exemplo as prioridades

75
00:04:16,047 --> 00:04:19,133
porque as prioridades elas
precisam ser individuais.

76
00:04:19,467 --> 00:04:22,220
Porque cada um e cada uma tem características próprias

77
00:04:22,428 --> 00:04:26,015
Mas quando você reduz ou até elimina

78
00:04:26,224 --> 00:04:28,601
esse, essa pertença.

79
00:04:28,601 --> 00:04:33,314
Esse sentimento de pertencer àquela igreja
que você frequentou durante tantos anos

80
00:04:34,274 --> 00:04:36,651
a sua filha foi batizada.

81
00:04:36,651 --> 00:04:40,405
Ou então um terreiro que você frequentou
junto com a sua mãe

82
00:04:40,989 --> 00:04:44,534
sua mãe não está mais viva
e você está dando continuidade. Desapareceu!

83
00:04:45,118 --> 00:04:49,497
então é uma série de espaços que são espaços
de convivência

84
00:04:49,497 --> 00:04:52,500
de criação de laços afetivos, que vão embora.

85
00:04:52,500 --> 00:04:54,252
Você só consegue minimizar isso

86
00:04:54,711 --> 00:04:56,170
não recuperar

87
00:04:56,671 --> 00:04:58,965
se você também trabalha de forma comunitária

88
00:04:59,215 --> 00:05:03,094
não colocando todo mundo
junto necessariamente.

89
00:05:03,678 --> 00:05:05,471
Mas você, é como se você...

90
00:05:06,389 --> 00:05:09,809
identificasse, e não é tão difícil sabe?
Isso é o que me deixa mais inconformado.

91
00:05:10,143 --> 00:05:13,396
É você indicar os grupos
e trabalhar com os grupos

92
00:05:13,396 --> 00:05:18,735
Vai custar? Para quem produziu um impacto desse
e teve superávit esses anos aí.

93
00:05:18,943 --> 00:05:20,945
Lucro. Ou seja: teve lucro. seja que terminou

94
00:05:21,279 --> 00:05:23,406
pegando a receita e a despesa

95
00:05:23,573 --> 00:05:25,700
um investimento desse é baixo.

96
00:05:25,700 --> 00:05:28,369
Mas não é feito.
Porque não é o padrão da mineração.

97
00:05:28,870 --> 00:05:33,041
Você fazer esse
tipo de orquestração comunitária

98
00:05:33,708 --> 00:05:36,836
porque quanto mais você divide

99
00:05:36,836 --> 00:05:39,380
mais você potencializa conflitos de interesses.

100
00:05:40,214 --> 00:05:42,592
E aí quem se beneficia no final

101
00:05:43,301 --> 00:05:46,971
e aqui em Maceió
e já são anos e anos que eu ouço

102
00:05:47,638 --> 00:05:51,684
planos de readequação
urbana dessas áreas.

103
00:05:53,478 --> 00:05:55,730
É uma oportunidade.

104
00:05:55,938 --> 00:05:58,691
Eu não vou dizer que todos por exemplo
que tem uma ideia

105
00:05:58,691 --> 00:06:02,695
como essa
estão mal intencionados só para lucrar

106
00:06:04,072 --> 00:06:06,115
mas de boas intenções o inferno está lotado.

107
00:06:08,284 --> 00:06:09,911
E aí se você...

108
00:06:09,911 --> 00:06:12,663
porque se a pessoa fala assim:
Diego, eu não trabalho com avaliação de risco.

109
00:06:14,040 --> 00:06:17,752
"Eu quero é o desenvolvimento
ali daquela área etc"

110
00:06:17,752 --> 00:06:21,381
Eu digo olhe:  a gente esta conversando
com parâmetros diferenciados.

111
00:06:22,215 --> 00:06:24,842
Porque antes de falar de desenvolvimento
eu vou pensar em risco.

112
00:06:26,803 --> 00:06:28,763
É igual você colocar um...

113
00:06:29,013 --> 00:06:33,226
um prédio ao lado
por exemplo de um shopping etc.

114
00:06:34,060 --> 00:06:36,437
Desde que o lixão esteja desativado.
Aquela área ali...

115
00:06:37,105 --> 00:06:38,398
pode ser a área de risco

116
00:06:39,357 --> 00:06:40,525
Esse caso:

117
00:06:41,275 --> 00:06:43,277
a gente está falando de afundamento do solo.

118
00:06:44,946 --> 00:06:47,615
A gente está falando por exemplo de risco

119
00:06:48,408 --> 00:06:50,410
de por exemplo uma coisa simples

120
00:06:50,410 --> 00:06:52,578
para a pessoa que vai assistir o documentoário entender

121
00:06:52,578 --> 00:06:54,831
é uma parede desabar em você.

122
00:06:57,250 --> 00:06:58,501
Ah Diego: você está sendo muito drástico

123
00:06:58,501 --> 00:07:02,797
mas o dever do pesquisador
de AIA de Avaliação de Impactos Ambientais

124
00:07:02,797 --> 00:07:05,550
como eu falei pra vocês: é ser um dramático

125
00:07:05,925 --> 00:07:09,095
Não é? Um dramático...
Por isso é tem que ter uma...

126
00:07:09,095 --> 00:07:10,763
Por isso a equipe é multidisciplinar

127
00:07:11,013 --> 00:07:14,892
porque tem que ter alguém mais otimista
ali na equipe para equilibrar

128
00:07:15,643 --> 00:07:19,605
mas do ponto de vista das áreas afetadas.

129
00:07:19,981 --> 00:07:23,860
Sim existem áreas
por exemplo que o risco é baixo.

130
00:07:24,694 --> 00:07:28,739
Só que você você tem que trabalhar
no nível de probabilidade

131
00:07:28,739 --> 00:07:32,118
quando você por exemplo
começa a redesenhar o espaço urbano.

132
00:07:32,660 --> 00:07:35,496
Você tem que projetar isso
para 20, 30 anos.

133
00:07:36,372 --> 00:07:39,792
E você acha mesmo que essas áreas
não vão crescer em cima de áreas de risco.

134
00:07:40,668 --> 00:07:42,670
Esse é um ponto.
Depois...

135
00:07:44,297 --> 00:07:47,925
qual garantia técnica
que o Serviço Geológico Brasileiro

136
00:07:47,925 --> 00:07:51,137
foi muito taxativo que aquelas áreas
não deveriam ser ocupadas mais.

137
00:07:52,388 --> 00:07:54,056
Por exemplo essas áreas

138
00:07:54,056 --> 00:07:57,185
que eram de risco baixo
subirem para um risco médio.

139
00:07:58,769 --> 00:07:59,228
Então:

140
00:08:00,271 --> 00:08:03,232
no final das contas
Tá aí. é isso aí: geralmente a palavra que surge

141
00:08:03,900 --> 00:08:08,321
para tentar: não vamos desenvolver

142
00:08:08,988 --> 00:08:11,157
aí a palavra desenvolvimento

143
00:08:11,407 --> 00:08:13,618
que é um conceito super interessante

144
00:08:14,869 --> 00:08:19,207
ela passa a ser pessimamente utilizada
e geralmente com o desenvolvimento

145
00:08:19,207 --> 00:08:23,002
para quem né?
 a gente tem que perguntar isso.


1
00:00:00,320 --> 00:00:02,159
<b>O olho do furacão está aqui, ó!</b>

2
00:00:02,159 --> 00:00:03,159
<b>Está vendo aqui, mina 7?</b>

3
00:00:03,159 --> 00:00:06,480
<b>Mina 19 e mina 18.</b>

4
00:00:06,480 --> 00:00:09,119
<b>Isso aqui, no primeiro momento</b>

5
00:00:09,920 --> 00:00:11,840
<b>Tinham, eu medi o raio disso aqui</b>

6
00:00:11,840 --> 00:00:16,440
<b>deu 200 e poucos, 200 e tantos metros, que eu medi</b>

7
00:00:16,440 --> 00:00:20,039
<b>dava para caber aqui 2 Maracanãs.</b>

8
00:00:20,039 --> 00:00:22,880
<b>nessa área aqui tá vendo?</b>

9
00:00:24,440 --> 00:00:26,440
<b>Porque juntaram os 3 Minas</b>

10
00:00:27,079 --> 00:00:28,119
<b>Essas três minas aqui, está vendo?</b>

11
00:00:28,119 --> 00:00:30,119
<b>Aí ela foi desabando, não é?</b>

12
00:00:30,119 --> 00:00:31,800
<b>Foi enchendo aquele problema para vocês.</b>

13
00:00:32,599 --> 00:00:35,400
<b>Aí quando os</b>

14
00:00:36,639 --> 00:00:38,400
<b>foram feitos estudos</b>

15
00:00:38,400 --> 00:00:40,400
<b>com os sonares, não é?</b>

16
00:00:40,400 --> 00:00:42,679
<b>O sonar é que ela câmera que desce</b>

17
00:00:42,679 --> 00:00:44,280
<b>e filma tud o que está lá embaixo.</b>

18
00:00:44,280 --> 00:00:45,199
<b>Pelo som do Sonar, não é?</b>

19
00:00:45,800 --> 00:00:48,800
<b>Então, quando os alemães</b>

20
00:00:50,679 --> 00:00:52,199
<b>entraram nessa conversa</b>

21
00:00:52,199 --> 00:00:56,599
<b>dentro daqui já tinha inclusive 450m</b>

22
00:00:56,599 --> 00:00:58,239
<b>porque ele foi desabando, não é? E tal.</b>

23
00:00:58,239 --> 00:01:00,360
<b>Então, isso aqui são 3 minas</b>

24
00:01:00,360 --> 00:01:01,800
<b>vocês tão vendo aí as 3 minas.</b>

25
00:01:01,800 --> 00:01:05,599
<b>Aqui tem 2, vocês estão vendo aqui? A 9 e a 12.</b>

26
00:01:06,719 --> 00:01:09,320
<b>E assim mesmo, além de se juntarem</b>

27
00:01:09,320 --> 00:01:12,320
<b>você vê que a 3, a 13 tá tudo juntinho aqui, não é?</b>

28
00:01:13,360 --> 00:01:15,239
<b>Olha a 13 e a 16 como estão.</b>

29
00:01:15,239 --> 00:01:16,400
<b>Está vendo aqui, que pertinho?</b>

30
00:01:16,400 --> 00:01:18,239
<b>Aí tem mais aqui embaixo aqui, ó.</b>

31
00:01:18,239 --> 00:01:19,679
<b>A 20 e a 21.</b>

32
00:01:19,679 --> 00:01:21,119
<b>se juntaram também.</b>

33
00:01:21,119 --> 00:01:22,400
<b>Se juntaram.</b>

34
00:01:22,400 --> 00:01:25,519
<b>Olha aqui a 29 como tá pertinho dessa aqui.</b>

35
00:01:25,519 --> 00:01:26,519
<b>Olha a 27!</b>

36
00:01:26,519 --> 00:01:29,800
<b>Tudo muito longe dos 100m</b>

37
00:01:29,800 --> 00:01:32,199
<b>que eu acho que seria o seguro.</b>

38
00:01:33,079 --> 00:01:34,320
<b>Você está vendo aqui!</b>

39
00:01:34,320 --> 00:01:36,800
<b>Olha a 23 e a 15!</b>

40
00:01:36,800 --> 00:01:38,880
<b>Olha aqui a 22 e a 23. Tá vendo como está?</b>

41
00:01:39,599 --> 00:01:41,000
<b>Aqui, juntinhos, bem juntinho.</b>

42
00:01:42,039 --> 00:01:43,639
<b>Aqui a distância é praticamente zero</b>

43
00:01:43,639 --> 00:01:45,159
<b>E aqui vai. </b>

44
00:01:45,159 --> 00:01:47,079
<b>Então, você tem poucas minas.</b>

45
00:01:47,920 --> 00:01:49,480
<b>Por exemplo, essas minas</b>

46
00:01:49,480 --> 00:01:52,840
<b>entre a 24 e a 34, dá uma distância razoável</b>

47
00:01:54,320 --> 00:01:56,079
<b>acredita que deve estar em torno de 100m.</b>

48
00:01:56,079 --> 00:01:58,440
<b>Também para mina 11</b>

49
00:01:58,440 --> 00:02:00,679
<b>também está em torno aí de uns 80m a 100m.</b>

50
00:02:00,679 --> 00:02:02,320
<b>Tá bom!</b>

51
00:02:02,320 --> 00:02:03,880
<b>Só isso aqui qeu se salva</b>

52
00:02:03,880 --> 00:02:06,440
<b>mas o resto, olha aqui, ó!</b>

53
00:02:07,159 --> 00:02:11,000
<b>Isso aqui está tudo tudo uma coisa horrível!</b>

54
00:02:11,320 --> 00:02:13,119
<b>O que está afundando</b>

55
00:02:13,119 --> 00:02:14,119
<b>é daqui...</b>

56
00:02:16,960 --> 00:02:18,719
<b>Dessa mina 11...</b>

57
00:02:18,719 --> 00:02:23,119
<b>O IMA está aquí. Eu sempre dou a referência do IMA.</b>

58
00:02:23,119 --> 00:02:26,280
<b>Então é do IMA até o colégio Bom Conselho</b>

59
00:02:26,280 --> 00:02:29,559
<b>que está aqui, bem pertinho da 32.</b>

60
00:02:29,559 --> 00:02:32,800
<b>É essa área aqui que está afundando</b>

61
00:02:32,800 --> 00:02:35,719
<b>começou milimetricamente há 20 anos atrás</b>

62
00:02:35,719 --> 00:02:37,920
<b>e agora tá centímetros por ano</b>

63
00:02:37,920 --> 00:02:40,480
<b>na faixa de uns 25cm por ano, por aí.</b>

64
00:02:41,119 --> 00:02:44,960
<b>Essa aqui é a área de ocorrência</b>

65
00:02:44,960 --> 00:02:46,760
<b>do sal-gema.</b>

66
00:02:47,800 --> 00:02:50,280
<b>Veja que ela...</b>

67
00:02:50,280 --> 00:02:52,440
<b>Foi até bom mostrar aqui para vocês...</b>

68
00:02:52,440 --> 00:02:54,360
<b>É porque houve aquele absurdo</b>

69
00:02:54,360 --> 00:02:57,039
<b>que disseram que toda a Marcela ia afundar.</b>

70
00:02:57,920 --> 00:03:01,599
<b>Só afunda onde tem sal-gema</b>

71
00:03:01,599 --> 00:03:03,480
<b>e a área de exploração do sal-gema é aqui.</b>

72
00:03:03,480 --> 00:03:05,719
<b>Essa é a área que realmente está afundando.</b>

73
00:03:05,719 --> 00:03:10,159
<b>As minas vão afundando e tal</b>

74
00:03:10,159 --> 00:03:13,039
<b>E vai puxando, vai dando tração aqui, né?</b>

75
00:03:13,039 --> 00:03:14,800
<b>Então essa parte que passa aqui</b>

76
00:03:14,800 --> 00:03:16,960
<b>da Fernandes Lima, é apenas 400m.</b>

77
00:03:16,960 --> 00:03:19,159
<b>Mesmo assim</b>

78
00:03:19,159 --> 00:03:22,079
<b>a espessura que chega aqui é muito pequena.</b>

79
00:03:22,079 --> 00:03:24,159
<b>Aqui estão as minas, algumas minas</b>

80
00:03:24,159 --> 00:03:26,480
<b>a mina mais próxima, digamos a 2</b>

81
00:03:26,480 --> 00:03:28,559
<b>está aqui, tá vendo?</b>

82
00:03:28,559 --> 00:03:30,719
<b>Aqui aqui termina 2, tá certo?</b>

83
00:03:30,800 --> 00:03:32,679
<b>Então a mina 2 é e ssa aqui é a mais avançada.</b>

84
00:03:32,679 --> 00:03:35,039
<b>Está aqui a mina 2, tá vendo? Mina 2.</b>

85
00:03:40,679 --> 00:03:43,320
<b>Sal-gema ainda ocorre até aqui.</b>

86
00:03:43,320 --> 00:03:44,679
<b>Está vendo aqui?</b>

87
00:03:44,679 --> 00:03:46,800
<b>Aqui está  a Fernandes Lima, aqui em cima.</b>

88
00:03:50,000 --> 00:03:55,159
<b>Aí você vê que aqui a sal-gema acaba aqui.</b>

89
00:03:55,880 --> 00:03:56,599
<b>400m.</b>

90
00:03:56,599 --> 00:03:58,719
<b>Essa parte daqui ela não tem força nenhuma.</b>

91
00:03:58,719 --> 00:04:00,599
<b>E esse mapa é o mapa atual</b>

92
00:04:00,599 --> 00:04:03,480
<b>que por aqui tem sim alguma coisa</b>

93
00:04:03,480 --> 00:04:05,280
<b>algumas fissuras e tal...</b>

94
00:04:06,639 --> 00:04:09,559
<b>Onde chega alguma residência que tenha</b>

95
00:04:09,559 --> 00:04:12,920
<b>alguma fissura, é coisa muito pequena aqui</b>

96
00:04:12,920 --> 00:04:14,079
<b>coisas pequenas.</b>

97
00:04:14,079 --> 00:04:18,000
<b>Mas você vai se afastando da mina</b>

98
00:04:18,000 --> 00:04:19,880
<b>vai ficando cada vez menos. </b>

99
00:04:19,880 --> 00:04:22,320
<b>Vai ficando cada vez menor o efeito. </b>

100
00:04:22,320 --> 00:04:23,920
<b>Essa áea que esá aqui rm vermelho</b>

101
00:04:23,920 --> 00:04:24,599
<b>é a área do pânico.</b>

102
00:04:24,599 --> 00:04:28,199
<b>As pessoas, muita gente me telefona</b>

103
00:04:28,199 --> 00:04:29,480
<b>e me manda mensagem</b>

104
00:04:29,480 --> 00:04:31,239
<b> já vieram aqui.</b>

105
00:04:31,840 --> 00:04:35,719
<b>500m depois da área atual.</b>

106
00:04:35,719 --> 00:04:39,039
<b>Então isso aqui não tem absolutamente nada</b>

107
00:04:39,039 --> 00:04:40,880
<b>não tem a menor chance de...</b>

108
00:04:40,880 --> 00:04:43,559
<b>Afundar é só aqui!</b>

109
00:04:43,559 --> 00:04:44,800
<b>Isso aqui é rachadura</b>

110
00:04:44,800 --> 00:04:46,519
<b>e aqui, se acontecer</b>

111
00:04:46,519 --> 00:04:48,039
<b>se acontecer</b>

112
00:04:48,039 --> 00:04:49,960
<b>é fissuras muito insignificantes. </b>

113
00:04:49,960 --> 00:04:51,079
<b>Se acontecer.</b>

114
00:04:51,079 --> 00:04:52,760
<b>Agora, é claro que a Braskem</b>

115
00:04:52,760 --> 00:04:53,880
<b>tem que pagar tudo isso aqui.</b>

116
00:04:54,800 --> 00:04:55,719
<b>Tem que pagar tudo!</b>

117
00:04:55,719 --> 00:04:57,559
<b>Porque foi desvalorizado.</b>

118
00:04:57,559 --> 00:04:59,039
<b>Quem é que quer comprar um imóvel aqui?</b>

119
00:04:59,960 --> 00:05:01,159
<b>Ninguém!</b>

120
00:05:01,440 --> 00:05:03,800
<b>Força, até que ponto tem a força de fluência?</b>

121
00:05:03,800 --> 00:05:05,280
<b>Tá vendo aqui, ó?</b>

122
00:05:05,280 --> 00:05:07,199
<b>Ela vai ficando mole</b>

123
00:05:07,199 --> 00:05:08,320
<b>vai amolescendo</b>

124
00:05:08,320 --> 00:05:09,679
<b> e vai descendo aqui, ó</b>

125
00:05:09,679 --> 00:05:12,239
<b>não só aqui como aqui também vai descendo</b>

126
00:05:12,239 --> 00:05:13,800
<b>e faz rachaduras aqui encima.</b>

127
00:05:13,800 --> 00:05:17,039
<b>É a força tectônica, mais da força da gravidade também. </b>

128
00:05:17,039 --> 00:05:20,360
<b>Se desde desde 90 pelo menos 90,</b>

129
00:05:20,360 --> 00:05:25,679
<b>quando esse pessoal publicou o trabalho lá em Houston</b>

130
00:05:25,840 --> 00:05:29,239
<b>e já sabia que o diâmetro ideal</b>

131
00:05:29,239 --> 00:05:31,159
<b>seria em torno de 50m, não é?</b>

132
00:05:31,719 --> 00:05:33,119
<b>E que a distância devia ser</b>

133
00:05:33,119 --> 00:05:34,360
<b>pelo menos setenta e e tantos</b>

134
00:05:34,360 --> 00:05:35,519
<b>metros de distância não é?</b>

135
00:05:35,840 --> 00:05:37,000
<b>Se tivesse seguido isso...</b>

136
00:05:37,000 --> 00:05:40,519
<b>Isso eu estou falando do consultor máscara.</b>

137
00:05:40,519 --> 00:05:42,280
<b>Se pelo menos de lá já tivesse dito:</b>

138
00:05:42,280 --> 00:05:43,840
<b>não, não vai ter mais.</b>

139
00:05:43,840 --> 00:05:49,239
<b>Jamais vamos fazer diâmetro  acima de 750m...</b>

140
00:05:49,239 --> 00:05:51,079
<b>Se tivesse feito isso, não...</b>

141
00:05:51,079 --> 00:05:52,639
<b>E também tem outra coisa, não é?</b>

142
00:05:52,639 --> 00:05:55,039
<b>Tinha o problema também</b>

143
00:05:55,039 --> 00:05:57,039
<b>das falhas geológicas, não é?</b>

144
00:05:57,880 --> 00:05:58,920
<b>Aqui tem falha!</b>

145
00:05:58,920 --> 00:06:01,679
<b>Então não dá para fazer aqui porque tem falha.</b>

146
00:06:02,480 --> 00:06:03,760
<b>Teria evitado, não é?</b>

147
00:06:05,199 --> 00:06:06,280
<b>Porque muitas minas dessas</b>

148
00:06:06,280 --> 00:06:09,400
<b>foram feitas agora, na década de 80.</b>

149
00:06:09,400 --> 00:06:11,480
<b>Digo, década de 90!</b>

150
00:06:12,719 --> 00:06:16,079
<b>Nem 90, feito agora</b>

151
00:06:16,079 --> 00:06:20,679
<b>para cá de 2005, 2006, 2008</b>

152
00:06:20,679 --> 00:06:22,960
<b>foi feito minas recentes. 2010, entendeu?</b>

153
00:06:22,960 --> 00:06:25,239
<b> Infelizmente, a relação entre</b>

154
00:06:25,239 --> 00:06:29,599
<b>o Ministério da Minas e Energia</b>

155
00:06:29,599 --> 00:06:31,000
<b> é do governo federal, né?</b>

156
00:06:31,719 --> 00:06:33,000
<b>E as mineradoras</b>

157
00:06:33,000 --> 00:06:35,039
<b>eu diria nos ultimos 20 anos</b>

158
00:06:35,039 --> 00:06:36,400
<b>sempre foi muito</b>

159
00:06:37,760 --> 00:06:38,360
<b>horrível</b>

160
00:06:38,360 --> 00:06:42,079
<b>até certo ponto, até irresponsável, não é?</b>

161
00:06:42,079 --> 00:06:43,599
<b>Taí brumadinho</b>

162
00:06:43,599 --> 00:06:46,159
<b>taí Mariana, que não me deixam mentir.</b>

163
00:06:46,960 --> 00:06:48,960
<b>E agora está aqui em Maceió, não é?</b>


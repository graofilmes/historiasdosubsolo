1
00:00:00,000 --> 00:00:01,400
<b>May 03, 2018</b>

2
00:00:01,400 --> 00:00:03,840
<b>It is what I call "Wake up, Maceió"!</b>

3
00:00:04,360 --> 00:00:06,119
<b>That is when things blew up.</b>

4
00:00:06,119 --> 00:00:09,000
<b>That is when...</b>

5
00:00:09,000 --> 00:00:11,360
<b>That is when everyone got involved,</b>

6
00:00:11,360 --> 00:00:14,679
<b>especially CPRM.</b>

7
00:00:15,280 --> 00:00:18,719
<b>The great CPRM! They did a great job</b>

8
00:00:19,159 --> 00:00:22,400
<b>and I was honored to participate in it.</b>

9
00:00:22,800 --> 00:00:26,159
<b>I had many meetings with them here in my office.</b>

10
00:00:26,599 --> 00:00:28,760
<b>And then,</b>

11
00:00:29,320 --> 00:00:30,599
<b>in 2019</b>

12
00:00:30,599 --> 00:00:34,360
<b>I think it was May 9, 2019,</b>

13
00:00:34,360 --> 00:00:39,239
<b>CPRM brilliantly delivered</b>

14
00:00:39,239 --> 00:00:41,159
<b>a scathing report</b>

15
00:00:41,599 --> 00:00:45,840
<b>stating exactly who was to blame for it all:</b>

16
00:00:45,840 --> 00:00:47,519
<b>Braskem.</b>

17
00:00:47,960 --> 00:00:49,599
<b>And when I accused Braskem</b>

18
00:00:49,599 --> 00:00:51,440
<b>in March 2018</b>

19
00:00:51,440 --> 00:00:53,519
<b>many colleagues called me crazy!</b>

20
00:00:54,440 --> 00:00:59,000
<b>Around March 10 and 15</b>

21
00:00:59,000 --> 00:01:02,360
<b>there was a big CREA meeting</b>

22
00:01:03,360 --> 00:01:06,360
<b>and there were five speakers.</b>

23
00:01:06,360 --> 00:01:10,880
<b>The CREA auditorium was packed!</b>

24
00:01:11,519 --> 00:01:13,360
<b>There were five speakers:</b>

25
00:01:13,920 --> 00:01:16,920
<b>Professor Abel Galindo and other four.</b>

26
00:01:16,920 --> 00:01:20,039
<b>The other four were defending Braskem</b>

27
00:01:20,039 --> 00:01:21,280
<b>by saying Braskem</b>

28
00:01:21,280 --> 00:01:23,320
<b>had nothing to do with it.</b>

29
00:01:23,440 --> 00:01:25,119
<b>Absolutely nothing.</b>

30
00:01:25,320 --> 00:01:27,920
<b>And this denial</b>

31
00:01:27,920 --> 00:01:30,440
<b>is what got to me.</b>

32
00:01:31,320 --> 00:01:34,320
<b>I did not accept it at all.</b>

33
00:01:34,320 --> 00:01:37,119
<b>So I spoke up,</b>

34
00:01:37,119 --> 00:01:39,960
<b>showed the evidence,</b>

35
00:01:39,960 --> 00:01:42,400
<b>the reason why I thought it was Braskem,</b>

36
00:01:42,840 --> 00:01:44,119
<b>that the problem was deep down,</b>

37
00:01:44,119 --> 00:01:45,840
<b>a meter deep.</b>

38
00:01:45,840 --> 00:01:47,360
<b>Not above.</b>

39
00:01:47,360 --> 00:01:50,360
<b>Then all that nonsense started,</b>

40
00:01:50,360 --> 00:01:52,639
<b>saying that it was</b>

41
00:01:52,639 --> 00:01:54,480
<b>a drainage problem on the surface,</b>

42
00:01:54,480 --> 00:01:58,119
<b>and that there would be endorheic basins.</b>

43
00:01:58,119 --> 00:01:59,519
<b>An endorheic basin is</b>

44
00:01:59,519 --> 00:02:03,760
<b>a low part of</b>

45
00:02:03,760 --> 00:02:05,320
<b>where water collects.</b>

46
00:02:05,320 --> 00:02:07,159
<b>And that had nothing to do with it.</b>

47
00:02:07,159 --> 00:02:08,719
<b>The problem was coming,</b>

48
00:02:08,719 --> 00:02:12,079
<b>I had been experiencing it since 2010!</b>

49
00:02:12,760 --> 00:02:14,480
<b>Although in 2010...</b>

50
00:02:14,480 --> 00:02:17,840
<b>In 2010 I saw that crack in the ground.</b>

51
00:02:18,159 --> 00:02:19,440
<b>It was the first time I saw that</b>

52
00:02:19,440 --> 00:02:20,360
<b>on the ground,</b>

53
00:02:20,599 --> 00:02:22,280
<b>and the cracked buildings too.</b>

54
00:02:22,280 --> 00:02:24,960
<b>But honestly that year</b>

55
00:02:24,960 --> 00:02:27,320
<b>I didn't know exactly</b>

56
00:02:27,320 --> 00:02:28,320
<b>what that crack was.</b>

57
00:02:28,320 --> 00:02:29,079
<b>I didn't know.</b>

58
00:02:29,320 --> 00:02:31,840
<b>I thought it was a soil retraction problem</b>

59
00:02:31,840 --> 00:02:33,679
<b>due to the water there.</b>

60
00:02:33,880 --> 00:02:35,559
<b>But then cracks started to appear</b>

61
00:02:35,559 --> 00:02:37,760
<b>in higher plains!</b>

62
00:02:37,920 --> 00:02:39,960
<b>Where there was no accumulation of water.</b>

63
00:02:41,360 --> 00:02:42,480
<b>No accumulation at all!</b>

64
00:02:42,480 --> 00:02:43,639
<b>So now what?</b>

65
00:02:43,920 --> 00:02:48,239
<b>What about the ones defending that</b>

66
00:02:48,239 --> 00:02:50,159
<b>it was a lower area,</b>

67
00:02:50,159 --> 00:02:52,679
<b>an area that accumulated water during winter,</b>

68
00:02:52,679 --> 00:02:53,960
<b>that did not have good drainage?</b>

69
00:02:54,079 --> 00:02:57,679
<b>It was all baseless talk.</b>

70
00:02:58,719 --> 00:03:00,519
<b>They certainly don't like when say it,</b>

71
00:03:00,519 --> 00:03:01,480
<b>but I have to say this:</b>

72
00:03:01,480 --> 00:03:02,639
<b>completely baseless!</b>

73
00:03:02,920 --> 00:03:06,000
<b>There were five ways of evaluating</b>

74
00:03:06,000 --> 00:03:07,239
<b>and studying the problem,</b>

75
00:03:07,239 --> 00:03:10,760
<b>there were five different methods that CPRM presented.</b>

76
00:03:10,920 --> 00:03:12,280
<b>And there you have it!</b>

77
00:03:12,280 --> 00:03:13,719
<b>People started saying:</b>

78
00:03:13,719 --> 00:03:15,480
<b>See what Professor Abel said?</b>

79
00:03:15,519 --> 00:03:17,000
<b>See what Professor Abel said all along?</b>

80
00:03:17,000 --> 00:03:17,760
<b>See?</b>

81
00:03:18,159 --> 00:03:21,800
<b>And still,</b>

82
00:03:21,800 --> 00:03:25,639
<b>before that they came with a report</b>

83
00:03:25,639 --> 00:03:26,800
<b>from Houston.</b>

84
00:03:26,800 --> 00:03:29,119
<b>There were also reports from Brazil.</b>

85
00:03:29,119 --> 00:03:31,199
<b>And from Oxford.</b>

86
00:03:31,199 --> 00:03:34,039
<b>They brought a bunch of reports, all of them inaccurate!</b>

87
00:03:34,039 --> 00:03:35,880
<b>And I ditched them all.</b>

88
00:03:36,360 --> 00:03:39,599
<b>In the end, Braskem took the fall, right?</b>

89
00:03:40,119 --> 00:03:41,280
<b>They did!</b>

90
00:03:42,199 --> 00:03:44,199
<b>Now, as far as I know,</b>

91
00:03:44,199 --> 00:03:47,280
<b>they are spending</b>

92
00:03:47,280 --> 00:03:48,719
<b>over ten billion reais</b>

93
00:03:48,719 --> 00:03:49,960
<b>on a problem they said</b>

94
00:03:49,960 --> 00:03:51,440
<b>had nothing to do with them.</b>


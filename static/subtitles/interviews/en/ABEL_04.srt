1
00:00:00,239 --> 00:00:04,320
Do you remember the day of the earthquake?

2
00:00:04,320 --> 00:00:06,760
What neighborhood were you in when it happened?

3
00:00:06,880 --> 00:00:08,360
I wasn't there.

4
00:00:08,360 --> 00:00:10,280
I went on March 3,

5
00:00:10,280 --> 00:00:13,960
this is recorded everywhere.

6
00:00:13,960 --> 00:00:16,880
I even have a sister-in-law,

7
00:00:17,599 --> 00:00:20,480
<b>my wife's sister, who's passed away,</b>

8
00:00:21,360 --> 00:00:23,719
who was up there in the eye of the hurricane.

9
00:00:23,719 --> 00:00:29,320
She lived right by the school.

10
00:00:30,119 --> 00:00:32,000
<b>The famous one,</b>

11
00:00:33,239 --> 00:00:34,079
<b>from the council,</b>

12
00:00:36,360 --> 00:00:38,920
a historical heritage.

13
00:00:38,920 --> 00:00:41,119
It used to be, but not anymore, Braskem ended

14
00:00:41,119 --> 00:00:42,119
the school.

15
00:00:42,119 --> 00:00:44,159
Not only the school, but other historical heritage, right?

16
00:00:44,880 --> 00:00:47,440
So, my sister-in-law lived there

17
00:00:47,440 --> 00:00:48,679
and she told me.

18
00:00:48,679 --> 00:00:50,599
Not only her, but hundreds of others:

19
00:00:50,599 --> 00:00:51,639
Abel,

20
00:00:51,639 --> 00:00:53,480
I ran out of the house

21
00:00:53,480 --> 00:00:54,880
when I heard a bang!

22
00:00:54,880 --> 00:00:56,199
I thought it was the end.

23
00:00:57,039 --> 00:00:59,000
Beyond that region, high up there,

24
00:00:59,000 --> 00:01:02,079
everyone went out into the street, terrified,

25
00:01:02,079 --> 00:01:04,760
thinking everything was falling apart!

26
00:01:05,840 --> 00:01:06,360
<b>That's what happened.</b>

27
00:01:06,920 --> 00:01:08,199
<b>The earthquake was such that</b>

28
00:01:08,760 --> 00:01:11,199
in Cruz das Almas,

29
00:01:11,199 --> 00:01:13,920
anyone who knows Maceió knows

30
00:01:13,920 --> 00:01:16,440
where the street Rua do Paulo César is.

31
00:01:17,119 --> 00:01:19,320
At Paulo César's place, high in the plains,

32
00:01:19,320 --> 00:01:21,320
there are two towers.

33
00:01:21,320 --> 00:01:23,480
Those two towers shook so much that

34
00:01:23,480 --> 00:01:24,559
everyone there...

35
00:01:24,559 --> 00:01:26,519
And I'm talking about Cruz das Almas!

36
00:01:26,519 --> 00:01:29,280
They shook so much that the people there,

37
00:01:29,280 --> 00:01:31,719
everyone, jumped in fright

38
00:01:31,719 --> 00:01:32,880
and called civil defense.

39
00:01:32,880 --> 00:01:34,639
Civil defense went there so everything is on record.


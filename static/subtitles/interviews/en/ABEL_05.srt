1
00:00:00,039 --> 00:00:03,679
<b>This distance between the mines is what we call pillars.</b>

2
00:00:03,679 --> 00:00:05,320
<b>You dig a hole there,</b>

3
00:00:05,320 --> 00:00:06,599
<b>another there,</b>

4
00:00:06,599 --> 00:00:08,639
<b>and this middle has no hole.</b>

5
00:00:08,639 --> 00:00:10,519
<b>This middle supports what is on top.</b>

6
00:00:11,159 --> 00:00:11,800
<b>Got it?</b>

7
00:00:11,800 --> 00:00:12,920
<b>You dig a hole here</b>

8
00:00:12,920 --> 00:00:15,320
<b>and you make two big caverns,</b>

9
00:00:15,920 --> 00:00:16,719
<b>and in the middle,</b>

10
00:00:16,719 --> 00:00:19,400
<b>this middle is the least you have to leave</b>

11
00:00:19,400 --> 00:00:21,039
<b>to support what is on top.</b>

12
00:00:21,719 --> 00:00:22,559
<b>But for this one...</b>

13
00:00:22,559 --> 00:00:24,400
<b>When you increase the diameter,</b>

14
00:00:24,400 --> 00:00:26,480
<b>you shorten the distance.</b>

15
00:00:26,480 --> 00:00:28,400
<b>So the distance that was supposed to be,</b>

16
00:00:28,400 --> 00:00:29,519
<b>according to my calculations,</b>

17
00:00:29,519 --> 00:00:31,920
<b>around 100m, from 90m to 100m</b>

18
00:00:32,599 --> 00:00:36,280
<b>the distances were, I would say, at least 70%</b>

19
00:00:36,960 --> 00:00:38,239
<b>at a distance of zero,</b>

20
00:00:38,239 --> 00:00:40,039
<b>because the mines joined.</b>

21
00:00:41,000 --> 00:00:42,039
<b>In these mines that came together,</b>

22
00:00:42,039 --> 00:00:44,599
<b>you can easily fit a Maracanã</b>

23
00:00:45,360 --> 00:00:47,360
<b>in the hole down there.</b>

24
00:00:47,920 --> 00:00:48,760
<b>Easily.</b>

25
00:00:48,760 --> 00:00:52,000
<b>So there was zero to 10m or 15m.</b>

26
00:00:52,000 --> 00:00:53,920
<b>It was ludicrous!</b>

27
00:00:53,920 --> 00:00:56,519
<b>So this shortening,</b>

28
00:00:56,519 --> 00:00:59,800
<b>this narrowing of the supporting pillars</b>

29
00:00:59,800 --> 00:01:01,199
<b>broke down</b>

30
00:01:01,199 --> 00:01:03,119
<b>and caused an earthquake.</b>

31
00:01:04,239 --> 00:01:08,039
<b>And that same condition is there</b>

32
00:01:08,039 --> 00:01:10,599
<b>and it can happen at any time.</b>

33
00:01:10,599 --> 00:01:13,880
<b>I say this based on reports,</b>

34
00:01:13,880 --> 00:01:16,840
<b>even the German institute's own reports</b>

35
00:01:16,840 --> 00:01:18,159
<b>and others as well. </b>

36
00:01:18,159 --> 00:01:21,320
<b>All that because Braskem hired,</b>

37
00:01:21,320 --> 00:01:25,079
<b>as required by the National Mining Agency,</b>

38
00:01:25,079 --> 00:01:27,840
<b>companies from all over the world.</b>

39
00:01:27,840 --> 00:01:30,119
<b>There are American companies, Italian companies,</b>

40
00:01:30,119 --> 00:01:32,400
<b>French companies, Dutch companies,</b>

41
00:01:32,400 --> 00:01:34,679
<b>German companies, right?</b>

42
00:01:34,760 --> 00:01:36,519
<b>There are at least 9 or 10 companies</b>

43
00:01:36,519 --> 00:01:38,760
<b>that issued these reports</b>

44
00:01:38,760 --> 00:01:40,840
<b>and I have almost all of them.</b>

45
00:01:40,840 --> 00:01:43,840
<b>It's in public domain, isn't it</b>?</b>

46
00:01:43,840 --> 00:01:45,440
<b>After it arrives at ANM and</b>

47
00:01:45,440 --> 00:01:47,239
<b>after a few months, it is in public domain.</b>


1
00:00:00,208 --> 00:00:01,626
I'm in favor of

2
00:00:01,626 --> 00:00:04,004
a real transformation,
of making a big corridor

3
00:00:04,045 --> 00:00:05,255
reaching up to...

4
00:00:05,255 --> 00:00:07,340
part of Ibama, which is not far...

5
00:00:07,340 --> 00:00:08,216
the Municipal Park there...

6
00:00:08,216 --> 00:00:11,344
and make a big green area

7
s00:00:12,512 --> 00:00:13,722
in the region.

8
00:00:14,639 --> 00:00:17,100
Which will probably be worth it
as I mentioned here...

9
00:00:18,143 --> 00:00:19,310
oh, there are...

10
00:00:19,310 --> 00:00:21,479
there are areas, for example,
where the risk is lower.

11
00:00:21,521 --> 00:00:22,605
There are areas, for example,
that have not...

12
00:00:22,605 --> 00:00:24,816
have not been identified as areas

13
00:00:26,401 --> 00:00:29,487
where the subsidence of the ground will...

14
00:00:30,071 --> 00:00:32,490
it will occur.

15
00:00:33,283 --> 00:00:34,117
That's a decision...

16
00:00:34,117 --> 00:00:36,995
because at the end of the day,
it's not just a technical decision.

17
00:00:37,120 --> 00:00:38,997
It's a political one as well.

18
00:00:38,997 --> 00:00:43,168
So either you make the decision
based on a risk criterion,

19
00:00:44,335 --> 00:00:47,881
or you make a decision based on
a, for example,

20
00:00:48,506 --> 00:00:53,845
purely economic criterion, that is,
transform some areas into

21
00:00:54,846 --> 00:00:56,890
recreational ones

22
00:00:56,890 --> 00:00:59,267
or even into areas with
regular occupation.

23
00:01:00,560 --> 00:01:04,689
This will generate a perverse effect

24
00:01:05,523 --> 00:01:08,109
that is clearly a feeling of animosity.

25
00:01:08,318 --> 00:01:12,781
And I think it's unfair when,
for example, some residents identify that

26
00:01:12,781 --> 00:01:17,827
where their homes used to be, there will now be
some beautiful mansions there,

27
00:01:17,827 --> 00:01:20,455
if, for example, they return to

28
00:01:22,457 --> 00:01:23,833
housing.

29
00:01:23,833 --> 00:01:27,087
I will defend this
until I am abducted,

30
00:01:27,212 --> 00:01:29,506
taken to Mars,

31
00:01:30,048 --> 00:01:32,342
that the ideal thing to do is
to make a green area.

32
00:01:33,134 --> 00:01:35,804
And then, in areas where
you might not have risk,

33
00:01:36,846 --> 00:01:39,057
where it is low,

34
00:01:39,057 --> 00:01:42,685
the geological condition
is conducive to subsidence.

35
00:01:43,895 --> 00:01:46,439
Then you have recreational
public equipment,

36
00:01:46,523 --> 00:01:49,526
but public, not private.

37
00:01:49,526 --> 00:01:54,405
But if at the time you...
whoever is making the decision sees that

38
00:01:54,405 --> 00:01:58,326
there is a possibility of increasing
the risk in 20, 30 years, you don't even need to do that.

39
00:01:58,743 --> 00:02:00,370
Just a green area is enough.

40
00:02:01,037 --> 00:02:02,330
And this green area

41
00:02:02,747 --> 00:02:06,709
can have recreational purposes or not.

42
00:02:07,710 --> 00:02:08,461
And then...

43
00:02:08,461 --> 00:02:09,712
And you wanna know the worst part?

44
00:02:10,046 --> 00:02:11,631
The worst you can do...
like with a car, you can create,

45
00:02:11,631 --> 00:02:13,466
economically, you can create...

46
00:02:14,008 --> 00:02:16,010
an economic asset with it.

47
00:02:16,177 --> 00:02:19,264
For example, regarding the ability
to sequester carbon in a huge area,

48
00:02:19,556 --> 00:02:22,976
it is possible to carry out
a Clean Development Mechanism project.

49
00:02:23,393 --> 00:02:26,271
You can even do, in a way,

50
00:02:26,646 --> 00:02:29,065
not even an MDS. A reddit.

51
00:02:29,399 --> 00:02:30,859
So, like, there is...

52
00:02:32,193 --> 00:02:35,280
Maybe the magnitude of it
is too ambicious,

53
00:02:35,280 --> 00:02:40,160
but it's possible to economically
optimize these green areas,

54
00:02:40,326 --> 00:02:42,287
create a green infrastructure.


1
00:00:00,559 --> 00:00:02,280
<b>People used to ask me</b>

2
00:00:02,280 --> 00:00:04,039
<b>back in the second half of 2018,</b>

3
00:00:04,039 --> 00:00:06,280
<b>in my lectures and meetings:</b>

4
00:00:07,159 --> 00:00:08,400
<b>So what's the solution, Professor?</b>

5
00:00:08,400 --> 00:00:09,840
<b>The solution is to backfill.</b>

6
00:00:11,039 --> 00:00:12,800
<b>Backfill all the holes,</b>

7
00:00:12,800 --> 00:00:13,519
<b>fill in the caves.</b>

8
00:00:14,639 --> 00:00:16,239
<b>Sand, gravel, whatever.</b>

9
00:00:17,360 --> 00:00:18,039
<b>They're doing it now.</b>

10
00:00:18,920 --> 00:00:21,280
<b>It's what people do all over the world.</b>

11
00:00:22,000 --> 00:00:23,880
<b>Now, don't be mistaken.</b>

12
00:00:23,880 --> 00:00:25,880
<b>Even if you backfill,</b>

13
00:00:25,880 --> 00:00:28,239
<b>it will take a long time to stabilize,</b>

14
00:00:28,239 --> 00:00:29,599
<b>because of the voids.</b>

15
00:00:29,599 --> 00:00:31,639
<b>You are pumping sand,</b>

16
00:00:32,679 --> 00:00:34,480
<b>and the sand stays there,</b>

17
00:00:34,480 --> 00:00:37,280
<b>but it is not cemented.</b>

18
00:00:39,440 --> 00:00:40,280
<b>I really wish</b>

19
00:00:40,280 --> 00:00:43,280
<b>they had injected sand with cement.</b>

20
00:00:44,920 --> 00:00:45,920
<b>I work like that,</b>

21
00:00:47,480 --> 00:00:48,480
<b>with mortar.</b>

22
00:00:49,800 --> 00:00:52,280
<b>But it is ten times more expensive.</b>

23
00:00:53,840 --> 00:00:57,159
<b>So they're pumping sand.</b>

24
00:00:58,239 --> 00:01:01,559
<b>But the sand will be left there</b>

25
00:01:02,360 --> 00:01:03,519
<b>to thicken.</b>

26
00:01:04,480 --> 00:01:06,400
<b>So it won't happen overnight.</b>

27
00:01:06,400 --> 00:01:09,039
<b>You think backfilling the mine solves it?</b>

28
00:01:09,039 --> 00:01:09,960
<b>No, it doesn't work that way.</b>

29
00:01:10,239 --> 00:01:13,519
<b>It will take a long time.</b>

30
00:01:14,199 --> 00:01:15,960
<b>There are 35 mines! So now what?</b>

31
00:01:16,679 --> 00:01:18,000
<b>Yeah, folks, but there's a study that--</b>

32
00:01:18,000 --> 00:01:19,480
<b>and I agree with that--</b>

33
00:01:19,480 --> 00:01:20,800
<b>studies show that</b>

34
00:01:20,800 --> 00:01:23,960
<b>many mines will fill themselves.</b>

35
00:01:25,199 --> 00:01:26,280
<b>Which is actually happening</b>

36
00:01:26,280 --> 00:01:27,280
<b>in some of them.</b>

37
00:01:27,880 --> 00:01:29,000
<b>So, there's a slight</b>

38
00:01:29,000 --> 00:01:31,039
<b>possibility, and I'm just guessing,</b>

39
00:01:31,039 --> 00:01:34,679
<b>that out of all these mines,</b>

40
00:01:34,679 --> 00:01:37,239
<b>maybe half, at most, need filling.</b>

41
00:01:37,239 --> 00:01:38,519
<b>Because the rest of them...</b>

42
00:01:38,519 --> 00:01:39,840
<b>Some are...</b>

43
00:01:39,840 --> 00:01:41,519
<b>If you monitor some of those</b>

44
00:01:41,519 --> 00:01:43,760
<b>that are completely in the salt</b>

45
00:01:43,760 --> 00:01:50,519
<b>and don't have faults running through them...</b>

46
00:01:50,519 --> 00:01:52,599
<b>These completely in the salt</b>

47
00:01:52,599 --> 00:01:55,679
<b>and in good condition,</b>

48
00:01:55,760 --> 00:01:56,519
<b>These mines</b>

49
00:01:57,199 --> 00:02:00,199
<b>can simply be stabilized through pressure.</b>

50
00:02:01,320 --> 00:02:03,280
<b>Because the environment</b>

51
00:02:03,280 --> 00:02:05,400
<b>and the shape they're in allows that.</b>

52
00:02:05,400 --> 00:02:07,280
<b>To be stabilized through pressure.</b>

53
00:02:07,280 --> 00:02:10,280
<b>Of course it will have to be monitored</b>

54
00:02:11,159 --> 00:02:12,639
<b>permanently.</b>

55
00:02:12,639 --> 00:02:16,480
<b>It is now up to the National Mining Agency</b>

56
00:02:16,480 --> 00:02:21,360
<b>to keep tabs on this monitoring, right?</b>

57
00:02:22,000 --> 00:02:22,760
<b>So that's how it will play out.</b>

58
00:02:22,760 --> 00:02:27,199
<b>Part of the mines will be stabilized by this process.</b>

59
00:02:27,199 --> 00:02:28,199
<b>Those that are...</b>

60
00:02:28,199 --> 00:02:29,519
<b>But look, guys...</b>

61
00:02:30,480 --> 00:02:32,559
<b>You can rest assured of one thing:</b>

62
00:02:32,559 --> 00:02:35,679
<b>Nowadays, the relationship between</b>

63
00:02:35,679 --> 00:02:37,400
<b>the National Mining Agency</b>

64
00:02:37,400 --> 00:02:40,000
<b>and Braskem</b>

65
00:02:40,760 --> 00:02:43,119
<b>is, well...</b>

66
00:02:43,119 --> 00:02:46,239
<b>That "good old pals" relationship is over, you know?</b>

67
00:02:46,840 --> 00:02:49,559
<b>That lasted up to 2018....</b>

68
00:02:49,559 --> 00:02:53,440
<b>But then things went downhill</b>

69
00:02:53,440 --> 00:02:57,800
<b>and now the Agency is more demanding.</b>

70
00:02:57,800 --> 00:02:59,000
<b>I have some reports</b>

71
00:02:59,000 --> 00:03:02,360
<b>and ANM is coming down on them, hard.</b>


1
00:00:00,519 --> 00:00:01,920
<b>Olhe, como eu participei</b>

2
00:00:01,920 --> 00:00:03,800
<b>do plano diretor de transporte em Maceió</b>

3
00:00:03,800 --> 00:00:06,880
<b>e na época a gente fez várias pesquisas</b>

4
00:00:06,880 --> 00:00:08,800
<b>as chamadas ODs e PDs, não é?</b>

5
00:00:09,599 --> 00:00:12,599
<b>As ODs era "origem e destino", ou seja</b>

6
00:00:13,199 --> 00:00:14,840
<b>você parava os carros na rua e dizia:</b>

7
00:00:14,840 --> 00:00:16,280
<b>você vem de onde e vai para onde?</b>

8
00:00:16,920 --> 00:00:19,039
<b>Você imagina, milhares de carros foram parados</b>

9
00:00:19,039 --> 00:00:20,079
<b>em determinados horários</b>

10
00:00:20,079 --> 00:00:21,199
<b>determinados locais</b>

11
00:00:21,679 --> 00:00:23,400
<b>e com isso, isso ia para um mapa</b>

12
00:00:23,960 --> 00:00:26,559
<b>e cada carro aparecia como uma linha vermelha</b>

13
00:00:26,840 --> 00:00:27,559
<b>ou azul.</b>

14
00:00:28,199 --> 00:00:29,679
<b>Então aí você botava isso no mapa</b>

15
00:00:29,679 --> 00:00:31,920
<b>e você via a tendência do deslocamentos.</b>

16
00:00:32,519 --> 00:00:34,039
<b>Foi isso que determinou que Maceió</b>

17
00:00:34,039 --> 00:00:36,519
<b>deveria ter o seu principal vetor de crescimento</b>

18
00:00:36,519 --> 00:00:37,800
<b>seria em torno da Fernanda Lima.</b>

19
00:00:38,639 --> 00:00:39,719
<b>Ela vai em direção</b>

20
00:00:40,360 --> 00:00:41,840
<b>em Maceió, à esquerda...</b>

21
00:00:42,559 --> 00:00:43,400
<b>Maceió é um "T".</b>

22
00:00:44,440 --> 00:00:46,280
<b>Você tem um "T" que termina</b>

23
00:00:46,280 --> 00:00:47,960
<b>no portal da barra</b>

24
00:00:48,320 --> 00:00:50,880
<b>e um "T" que termina aqui em Ipioca.</b>

25
00:00:51,559 --> 00:00:52,679
<b>Você tem a lagoa</b>

26
00:00:53,360 --> 00:00:55,599
<b>e tem toda essa área do Vale do jacarecica.</b>

27
00:00:55,599 --> 00:00:56,239
<b>Com Barreiras</b>

28
00:00:56,239 --> 00:00:58,679
<b>ou áreas de de de preservação</b>

29
00:00:58,679 --> 00:00:59,760
<b>por causa de água.</b>

30
00:01:00,719 --> 00:01:02,480
<b>Então, só tem o corpo do "T"</b>

31
00:01:02,480 --> 00:01:04,840
<b>subindo em direção a Messias.</b>

32
00:01:05,800 --> 00:01:08,360
<b>Então já ocupo o Benedito Bentes</b>

33
00:01:08,360 --> 00:01:10,000
<b>e daqui a pouco engole Satuba</b>

34
00:01:10,000 --> 00:01:11,199
<b>vai engolir o Rio Largo</b>

35
00:01:11,199 --> 00:01:14,800
<b>vai até Messias e em torno do aeroporto começa a crescer.</b>

36
00:01:16,320 --> 00:01:17,840
<b>Olha, isso identificado</b>

37
00:01:17,840 --> 00:01:19,519
<b>e isso, foi identificado pela pesquisa</b>

38
00:01:20,199 --> 00:01:22,920
<b>Você, então, determina os vetores de transporte.</b>

39
00:01:23,960 --> 00:01:25,960
<b>Foi o jeito que elaborou</b>

40
00:01:25,960 --> 00:01:27,920
<b>os três eixos de transporte, não é?</b>

41
00:01:28,519 --> 00:01:29,840
<b>Então você teria:</b>

42
00:01:30,000 --> 00:01:30,480
<b>Um</b>

43
00:01:31,079 --> 00:01:33,239
<b>paralelo À Fernanda Lima, pela lagoa.</b>

44
00:01:33,920 --> 00:01:35,760
<b>O sino alí da....</b>

45
00:01:36,719 --> 00:01:37,800
<b>Confusão caríssima.</b>

46
00:01:37,800 --> 00:01:40,440
<b>porque você tem que utilizar</b>

47
00:01:41,360 --> 00:01:43,519
<b>uma tecnologia de</b>

48
00:01:44,639 --> 00:01:47,119
<b>de colunas e de Placas de</b>

49
00:01:48,239 --> 00:01:50,159
<b>enfiadas no solo não é, destaques.</b>

50
00:01:52,000 --> 00:01:54,239
<b>Até mais ou menos Satuba.</b>

51
00:01:54,239 --> 00:01:56,280
<b>e você teria a chamada via expressa.</b>

52
00:01:57,480 --> 00:01:59,360
<b>Que é aquele pedaço que hoje existe</b>

53
00:01:59,360 --> 00:02:04,440
<b>que já foi reduzido a uma viela, nãe é?</b>

54
00:02:06,920 --> 00:02:08,800
<b>Um beco estreito.</b>

55
00:02:08,800 --> 00:02:11,400
<b>Eela continuaria pelo Vale do Reginaldo</b>

56
00:02:11,400 --> 00:02:12,599
<b>e sairia aqui na avenida.</b>

57
00:02:14,599 --> 00:02:16,840
<b>Que é com duas vias paralelaa, em torno.</b>

58
00:02:17,639 --> 00:02:19,519
<b>Então, ali naquele posto de gasolina</b>

59
00:02:19,519 --> 00:02:20,559
<b>quando vocês vem pela via express</b>

60
00:02:20,559 --> 00:02:22,760
<b>que chega ali, perto do Vitória Paladar, o antigo</b>

61
00:02:23,320 --> 00:02:24,480
<b>Ali ele continuaria.</b>

62
00:02:24,480 --> 00:02:25,519
<b>O projeto era  esse.</b>

63
00:02:25,519 --> 00:02:28,480
<b>Então você teria saindo de Maceió</b>

64
00:02:28,480 --> 00:02:30,320
<b>uma via central em torno da Fernanda Lima</b>

65
00:02:30,480 --> 00:02:31,400
<b>uma pela lagoa</b>

66
00:02:31,400 --> 00:02:34,119
<b>e outra paralela ao Vale do Reginaldo</b>

67
00:02:34,119 --> 00:02:36,119
<b>em direção a onde Hoje está</b>

68
00:02:36,119 --> 00:02:37,639
<b>mais ou menos o Benedito Bentes.</b>

69
00:02:40,079 --> 00:02:41,199
<b>Essas três vias seriam as vias que que
ligariam.</b>

70
00:02:41,199 --> 00:02:43,239
<b>seriam as vias que ligariam...</b>

71
00:02:43,760 --> 00:02:45,679
<b>Maceió seria uma cidade</b>

72
00:02:48,199 --> 00:02:49,599
<b>não radial</b>

73
00:02:50,159 --> 00:02:51,760
<b>mas ela comprida, não é?</b>

74
00:02:52,119 --> 00:02:53,280
<b>Ela seria um...</b>

75
00:02:54,800 --> 00:02:57,320
<b>Uma formação diferente em direção ao aeroporto.</b>

76
00:02:59,920 --> 00:03:01,719
<b>A via expressa não deu certo</b>

77
00:03:01,719 --> 00:03:03,519
<b>não terminou, não concluiu.</b>

78
00:03:03,760 --> 00:03:04,960
<b>A da lagoa muito menos</b>

79
00:03:04,960 --> 00:03:06,079
<b>por causa do custo.</b>

80
00:03:06,079 --> 00:03:08,119
<b>Ficou a Fernandes Lima.</b>

81
00:03:08,599 --> 00:03:12,320
<b>Ficou um pouco general...</b>

82
00:03:12,960 --> 00:03:15,000
<b>General Moneiro, não é?</b>

83
00:03:15,239 --> 00:03:17,039
<b>A General Hermes, a continumação da General Hermes</b>

84
00:03:17,039 --> 00:03:18,039
<b>lá para Bebedouro.</b>

85
00:03:19,960 --> 00:03:22,840
<b>Então, ela hoje não vai mais poder ser utilizada.</b>

86
00:03:22,840 --> 00:03:25,960
<b>então esse é um dos grandes problemas de Maceió.</b>

87
00:03:26,639 --> 00:03:29,559
<b>Porque o trem já não passa lá, não é?</b>

88
00:03:30,440 --> 00:03:31,039
<b>O trilho.</b>

89
00:03:31,880 --> 00:03:33,559
<b>Então aquilo ali, quem vai ficar ali?</b>

90
00:03:34,239 --> 00:03:35,639
<b>e cair e morrer gente.</b>

91
00:03:35,639 --> 00:03:36,760
<b>A qualquer momento pode afundar</b>

92
00:03:37,239 --> 00:03:40,199
<b>Então se você está retirando casas</b>

93
00:03:40,199 --> 00:03:41,559
<b>habitantes da região</b>

94
00:03:42,039 --> 00:03:43,000
<b>nada vai ficar ali</b>

95
00:03:43,000 --> 00:03:45,000
<b>inclusive eu vejo assim:</b>

96
00:03:45,000 --> 00:03:46,880
<b>não, tem que preservar o imóvel tal</b>

97
00:03:46,880 --> 00:03:48,039
<b>ele não pode ser derrubado.</b>

98
00:03:48,599 --> 00:03:50,119
<b>Mas ele vai ser derrubado.</b>

99
00:03:50,119 --> 00:03:53,440
<b>Pode não ser pela prefeitura</b>

100
00:03:53,440 --> 00:03:55,480
<b>ou pela salgema.</b>

101
00:03:55,679 --> 00:03:57,639
<b>Mas naquilo ali ninguém vai investir</b>

102
00:03:57,639 --> 00:04:00,000
<b>porque a qualquer momento pode cair!</b>

103
00:04:00,679 --> 00:04:01,760
<b>Então é uma área...</b>

104
00:04:02,079 --> 00:04:04,320
<b>A parte superior, do Pinheiro</b>

105
00:04:04,920 --> 00:04:06,480
<b>e uma parte já do Farol</b>

106
00:04:07,119 --> 00:04:08,880
<b>ela também não pode ser habitada mais.</b>

107
00:04:09,840 --> 00:04:11,360
<b>Claro, não edificantes, ou seja</b>

108
00:04:11,599 --> 00:04:13,000
<b>você não pode construir mais nada aí</b>

109
00:04:13,000 --> 00:04:14,000
<b>porque é risco.</b>

110
00:04:14,960 --> 00:04:16,760
<b>Então essa área vai ter que receber um uso</b>

111
00:04:16,760 --> 00:04:19,400
<b>provisoriamente enquanto ela não colapsa</b>

112
00:04:19,400 --> 00:04:21,239
<b>se é que ela vai colapsar um dia</b>

113
00:04:21,239 --> 00:04:22,880
<b>ou se encontra uma forma de...</b>

114
00:04:23,719 --> 00:04:25,400
<b>Dizem que é um custo muito alto</b>

115
00:04:25,400 --> 00:04:29,159
<b>injetar material embaixo para dar sustentação...</b>

116
00:04:29,960 --> 00:04:33,880
<b>Até lá, ela vai ficar uma espécie de uma floresta urbana.</b>

117
00:04:34,920 --> 00:04:36,039
<b>A tendência é essa.</b>

118
00:04:36,039 --> 00:04:37,000
<b>É uma floresta urbana.</b>

119
00:04:38,400 --> 00:04:39,679
<b>As raízes das árvores</b>

120
00:04:39,679 --> 00:04:44,679
<b>elas tendem a criar uma certa malha de sustentação.</b>

121
00:04:44,800 --> 00:04:46,679
<b>O que pode atrasar</b>

122
00:04:46,679 --> 00:04:48,880
<b>diminuir ou até impedir</b>

123
00:04:48,880 --> 00:04:50,960
<b>o colapso dessa região.</b>

124
00:04:51,360 --> 00:04:54,000
<b>Mas eu não entendo dessa área...</b>

125
00:04:54,440 --> 00:04:55,639
<b>Estou especulando agora</b>

126
00:04:55,639 --> 00:04:57,559
<b>que ela não vai mais poder ser usada.</b>

127
00:04:57,559 --> 00:04:58,800
<b>Não vai. Nem ela</b>

128
00:04:58,800 --> 00:05:01,440
<b> nem um trecho da General Hermes</b>

129
00:05:01,800 --> 00:05:03,719
<b>deste o ponto até a beira da lagoa.</b>

130
00:05:04,360 --> 00:05:06,679
<b>Ou seja, se Maceió tinha um projeto</b>

131
00:05:06,679 --> 00:05:08,440
<b>de três vias paralelas</b>

132
00:05:09,239 --> 00:05:11,840
<b>agora sobra uma, praticamente</b>

133
00:05:12,599 --> 00:05:15,119
<b>e isso tem um impacto muito grande na cidade.</b>

134
00:05:15,239 --> 00:05:16,480
<b>O problema da moradia</b>

135
00:05:16,480 --> 00:05:18,280
<b>eu sei que impacta pela questão cultural</b>

136
00:05:18,280 --> 00:05:20,920
<b>da relação que as pessoas tem com a sua casa</b>

137
00:05:20,920 --> 00:05:23,119
<b>com a sua rua, com a sua vizinhança.</b>

138
00:05:23,760 --> 00:05:27,679
<b>E isso é é duro de enfrentar.</b>

139
00:05:28,320 --> 00:05:30,679
<b>Mas havendo a mudança das pessoas</b>

140
00:05:30,679 --> 00:05:32,599
<b>o mais importante é hoje preservar a vida</b>

141
00:05:32,599 --> 00:05:33,920
<b>sai de lá.</b>

142
00:05:33,960 --> 00:05:35,639
<b>Não pode ficar, tem que preservar a vida.</b>

143
00:05:36,400 --> 00:05:37,800
<b>O que vai fazer depois?</b>

144
00:05:38,119 --> 00:05:39,480
<b>É acomodar a cidade</b>

145
00:05:39,480 --> 00:05:41,360
<b>e vai precisar de muitos investmentos</b>

146
00:05:41,360 --> 00:05:43,880
<b>Por exemplo, esse metrô de superfície</b>

147
00:05:43,880 --> 00:05:46,119
<b>que poderia ir pela Fernanda Lima até Messias</b>

148
00:05:46,719 --> 00:05:47,679
<b>Pela Fernandes Lima</b>

149
00:05:47,719 --> 00:05:49,920
<b>Isso não pode atrazar.</b>

150
00:05:50,119 --> 00:05:51,920
<b>Há uma tentativa de colocar ele</b>

151
00:05:51,920 --> 00:05:53,599
<b>passando pelo shopping e subindo</b>

152
00:05:53,599 --> 00:05:55,360
<b>lá pela Ladeira do Óleo</b>

153
00:05:56,559 --> 00:05:58,199
<b>para chegar lá em cima também</b>

154
00:05:58,199 --> 00:05:58,960
<b>é outra opção.</b>

155
00:05:58,960 --> 00:06:01,280
<b>Não estou descartando nenhuma delas agora.</b>

156
00:06:02,199 --> 00:06:03,880
<b>Agora, tem que se encontrar a forma</b>

157
00:06:06,320 --> 00:06:10,159
<b>de manter essa cidade longitudinal interligada.</b>

158
00:06:12,159 --> 00:06:15,679
<b>E o impacto social e cultural</b>

159
00:06:15,679 --> 00:06:18,239
<b>da perda do Pinheiro, ela existe.</b>

160
00:06:18,679 --> 00:06:21,239
<b>Mas ela vai impactar toda a cidade</b>

161
00:06:21,239 --> 00:06:23,159
<b>na questão dos transportes.</b>

162
00:06:24,000 --> 00:06:26,079
<b>Passado a pandemia, voltando às aulas</b>

163
00:06:26,079 --> 00:06:28,639
<b>voltando o pleno emprego, voltando tudo isso</b>

164
00:06:29,199 --> 00:06:31,320
<b>você imagina a Fernandes Lima como vai ficar?</b>

165
00:06:31,320 --> 00:06:32,719
<b>Intransitável!</b>

166
00:06:32,719 --> 00:06:36,840
<b>Então nós temos gargalos importantes</b>

167
00:06:36,840 --> 00:06:41,039
<b>a serem resolvidos urgentemente! Urgentemente!</b>

168
00:06:43,239 --> 00:06:45,159
<b>Como vai se controlar</b>

169
00:06:45,159 --> 00:06:47,000
<b>ocupação do solo nessas áreas</b>

170
00:06:47,000 --> 00:06:47,960
<b>é outro problema.</b>

171
00:06:49,239 --> 00:06:51,039
<b>A cidade vai precisar gastar muito.</b>

172
00:06:52,039 --> 00:06:54,880
<b>Para poder se readequar</b>

173
00:06:54,880 --> 00:06:56,599
<b>pelo menos ao que tinha.</b>

174
00:06:57,840 --> 00:06:59,039
<b>Ou aproveitar</b>

175
00:06:59,039 --> 00:07:00,559
<b>e já investir mais</b>

176
00:07:00,559 --> 00:07:03,719
<b>e fazer o que devia ter feito há mais de 30 anos.</b>

177
00:07:05,239 --> 00:07:08,159
<b>Ou seja, entender que a cidade é longitudinal</b>

178
00:07:09,679 --> 00:07:11,400
<b>do litoral para o interior.</b>

179
00:07:11,880 --> 00:07:14,280
<b>Não é um formato radial, como São Paulo, por exemplo.</b>

180
00:07:15,280 --> 00:07:17,199
<b>Mas aqui não tem jeito.</b>

181
00:07:18,320 --> 00:07:19,559
<b>Você tem o vale do Mundaú</b>

182
00:07:19,559 --> 00:07:20,599
<b>se imitando por um lado</b>

183
00:07:20,599 --> 00:07:21,880
<b>o vale do Jacarecica</b>

184
00:07:21,880 --> 00:07:22,519
<b>por outro...</b>

185
00:07:22,519 --> 00:07:24,880
<b>A cidade, vai crescer numa direção só</b>

186
00:07:25,559 --> 00:07:27,199
<b>e a gente não preparou para isso, não é?</b>

187
00:07:27,840 --> 00:07:30,239
<b>Ela vai ter que pagar por todos os prejuízos.</b>

188
00:07:30,280 --> 00:07:30,960
<b>É natural.</b>

189
00:07:32,039 --> 00:07:33,440
<b>Uma questão aí da justiça</b>

190
00:07:33,440 --> 00:07:35,039
<b>de como vai se dar esse pagamento.</b>

191
00:07:35,840 --> 00:07:37,039
<b>Agora mesmo aqui vizinho</b>

192
00:07:37,039 --> 00:07:38,760
<b>está sendo demolido o antigo prédio</b>

193
00:07:38,760 --> 00:07:40,800
<b>do conselho regional de medicina.
eles tinham.</b>

194
00:07:40,800 --> 00:07:42,800
<b>Eles tinham construído um novo lá no Pinheiro.</b>

195
00:07:43,480 --> 00:07:45,199
<b>Como acho que deve ter sido já indenizado</b>

196
00:07:45,199 --> 00:07:46,639
<b>Eles já devem estar aqui.</b>

197
00:07:46,639 --> 00:07:48,679
<b>Estão derrubando para reconstruir aqui</b>

198
00:07:48,679 --> 00:07:50,159
<b>uma nova sede.</b>

199
00:07:50,880 --> 00:07:52,000
<b>A cidade se movimenta</b>

200
00:07:52,000 --> 00:07:53,119
<b>vai se adequar a isso.</b>

201
00:07:54,760 --> 00:07:56,199
<b>Agora, o trânsito</b>

202
00:07:56,880 --> 00:07:59,000
<b>impacta a indústria, o comércio</b>

203
00:07:59,000 --> 00:08:00,719
<b>impacta tudo, o trabalho.</b>

204
00:08:01,280 --> 00:08:02,559
<b>Isso tem que ser resolvido rapidamente.</b>


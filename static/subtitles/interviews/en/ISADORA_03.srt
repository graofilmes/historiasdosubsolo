1
00:00:00,000 --> 00:00:00,000
<b>The idea was to call residents</b>

2
00:00:00,000 --> 00:00:00,000
<b>or people who had a connection with the place</b>

3
00:00:00,000 --> 00:00:00,000
<b>and present solutions to issues</b>

4
00:00:00,000 --> 00:00:00,000
<b>that residents deemed important.</b>

5
00:00:00,000 --> 00:00:00,000
<b>We weren't looking for...</b>

6
00:00:00,000 --> 00:00:00,000
<b>We just knew there were many issues,</b>

7
00:00:00,000 --> 00:00:00,000
<b>from infrastructure to... Anyway.</b>

8
00:00:00,000 --> 00:00:00,000
<b>The idea was:</b>

9
00:00:00,000 --> 00:00:00,000
<b>what did the residents</b>

10
00:00:00,000 --> 00:00:00,000
<b>need at the moment?</b>

11
00:00:00,000 --> 00:00:00,000
<b>Then we called</b>

12
00:00:00,000 --> 00:00:00,000
<b>and prioritized calling residents</b>

13
00:00:00,000 --> 00:00:00,000
<b>to be there,</b>

14
00:00:00,000 --> 00:00:00,000
<b>but it was open to any participant.</b>

15
00:00:00,000 --> 00:00:00,000
<b>So when the projects came to life,</b>

16
00:00:00,000 --> 00:00:00,000
<b>six groups,</b>

17
00:00:00,000 --> 00:00:00,000
<b>six projects were presented,</b>

18
00:00:00,000 --> 00:00:00,000
<b>and five were awarded.</b>

19
00:00:00,000 --> 00:00:00,000
<b>The idea was that they would</b>

20
00:00:00,000 --> 00:00:00,000
<b>receive mentorship from SEBRAE.</b>

21
00:00:00,000 --> 00:00:00,000
<b>In that moment, one of the awarded projects,</b>

22
00:00:00,000 --> 00:00:00,000
<b>one of them was Stories from the Underground,</b>

23
00:00:00,000 --> 00:00:00,000
<b>which was super interesting,</b>

24
00:00:00,000 --> 00:00:00,000
<b>because back then we were...</b>

25
00:00:00,000 --> 00:00:00,000
<b>The project was competing in a selection</b>

26
00:00:00,000 --> 00:00:00,000
<b>and it was going to present a pitch the next day.</b>

27
00:00:00,000 --> 00:00:00,000
<b>So on the same day</b>

28
00:00:00,000 --> 00:00:00,000
<b>the project was approved, we wrote an article,</b>

29
00:00:00,000 --> 00:00:00,000
<b>sent the link...</b>

30
00:00:00,000 --> 00:00:00,000
<b>I know the project presented this award,</b>

31
00:00:00,000 --> 00:00:00,000
<b>so we were very proud</b>

32
00:00:00,000 --> 00:00:00,000
<b>to be part of this story.</b>

33
00:00:00,000 --> 00:00:00,000
<b>We played a tiny part,</b>

34
00:00:00,000 --> 00:00:00,000
<b>but were very proud of it.</b>

35
00:00:00,279 --> 00:00:03,239
<b>The idea was to call residents</b>

36
00:00:03,239 --> 00:00:05,920
<b>or people who had a connection with the place</b>

37
00:00:05,920 --> 00:00:08,640
<b>and present solutions</b>

38
00:00:08,640 --> 00:00:11,399
<b>to issues the residents deemed important.</b>

39
00:00:11,399 --> 00:00:13,520
<b>We weren't looking for...</b>

40
00:00:13,520 --> 00:00:15,119
<b>but we knew there were many issues</b>

41
00:00:15,119 --> 00:00:17,399
<b>from infrastructure to... Anyway.</b>

42
00:00:17,399 --> 00:00:20,279
<b>The idea was: what did the residents</b>

43
00:00:20,279 --> 00:00:22,159
<b>need at the moment?</b>

44
00:00:22,600 --> 00:00:25,800
<b>So we prioritized</b>

45
00:00:25,800 --> 00:00:28,159
<b>calling residents to be there</b>

46
00:00:28,439 --> 00:00:30,199
<b>but it was open to any participant.</b>

47
00:00:30,439 --> 00:00:32,039
<b>So when the projects came to life,</b>

48
00:00:32,039 --> 00:00:33,479
<b>six groups,</b>

49
00:00:33,479 --> 00:00:35,680
<b>six projects were presented,</b>

50
00:00:35,920 --> 00:00:39,640
<b>and five were awarded.</b>

51
00:00:39,640 --> 00:00:42,640
<b>The idea was for them to receive mentorship from SEBRAE.</b>

52
00:00:43,239 --> 00:00:45,399
<b>In that moment,</b>

53
00:00:45,399 --> 00:00:48,399
<b>one of the awarded projects was Stories from the Underground.</b>

54
00:00:48,920 --> 00:00:51,920
<b>It was super interesting because back then</b>

55
00:00:51,920 --> 00:00:57,039
<b>the project was competing in a selection</b>

56
00:00:57,039 --> 00:00:59,520
<b>and it was going to present a pitch the next day.</b>

57
00:00:59,520 --> 00:01:02,359
<b>So on the same day the project was approved,</b>

58
00:01:02,359 --> 00:01:03,640
<b>we wrote an article,</b>

59
00:01:03,640 --> 00:01:05,720
<b>sent the link.</b>

60
00:01:05,720 --> 00:01:09,960
<b>I know the project presented this award,</b>

61
00:01:09,960 --> 00:01:12,840
<b>So we were very proud to be part of this story.</b>

62
00:01:12,840 --> 00:01:14,439
<b>We played a tiny part,</b>

63
00:01:14,439 --> 00:01:15,840
<b>but we were very proud of it.</b>


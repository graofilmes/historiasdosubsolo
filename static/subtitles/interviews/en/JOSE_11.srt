1
00:00:00,239 --> 00:00:02,800
<b>When we were running out of possibilities...</b>

2
00:00:02,800 --> 00:00:05,400
<b>While on the environment coordination.</b>

3
00:00:05,400 --> 00:00:09,400
<b>We were running out of possibilities,</b>

4
00:00:09,400 --> 00:00:11,239
<b>that's when I found...</b>

5
00:00:11,840 --> 00:00:14,800
<b>because we gave off this idea of:</b>

6
00:00:14,800 --> 00:00:18,679
<b>it's an army!</b>

7
00:00:18,679 --> 00:00:24,000
<b>Because many scientists and technical friends of ours</b>

8
00:00:24,000 --> 00:00:28,599
<b>began to collaborate with us anonymously</b>

9
00:00:28,599 --> 00:00:31,920
<b>and provide us with precious data.</b>

10
00:00:33,840 --> 00:00:38,280
<b>Among them, an extremely competent geologist.</b>

11
00:00:38,280 --> 00:00:41,079
<b>He shared his studies with us</b>

12
00:00:41,639 --> 00:00:44,159
<b>and in them he predicted</b>

13
00:00:44,159 --> 00:00:48,639
<b>that if proper care weren't taken</b>

14
00:00:48,639 --> 00:00:51,199
<b>the soil would subside.</b>

15
00:00:52,519 --> 00:00:55,199
<b>I immediately reported</b>

16
00:00:56,400 --> 00:00:59,800
<b>the risk of subsidence</b>

17
00:01:00,559 --> 00:01:05,679
<b>and we presented a possible mitigation.<</b>

18
00:01:05,679 --> 00:01:06,920
<b>It's not a solution,</b>

19
00:01:06,920 --> 00:01:08,480
<b>but in environmental science</b>

20
00:01:08,480 --> 00:01:10,320
<b>we also work with mitigation.</b>

21
00:01:10,320 --> 00:01:12,840
<b>So, a possible mitigation</b>

22
00:01:13,760 --> 00:01:18,960
<b>would be to constantly monitor</b>

23
00:01:19,679 --> 00:01:20,880
<b>the mines.</b>

24
00:01:21,519 --> 00:01:24,599
<b>Which they say they're doing</b>

25
00:01:24,599 --> 00:01:26,880
<b>and have done now.</b>

26
00:01:26,880 --> 00:01:29,880
<b>But it had to be done early on</b>

27
00:01:29,880 --> 00:01:33,679
<b>and monitored by the environmental agency.</b>

28
00:01:34,440 --> 00:01:37,199
<b>When I was at the environmental agency,</b>

29
00:01:37,199 --> 00:01:39,760
<b>we demanded reports.</b>

30
00:01:40,599 --> 00:01:42,119
<b>But look into it:</b>

31
00:01:42,800 --> 00:01:44,039
<b>after I left,</b>

32
00:01:45,079 --> 00:01:49,199
<b>Salgema never forwarded reports</b>

33
00:01:49,199 --> 00:01:52,320
<b>to the environmental agency of this state.</b>


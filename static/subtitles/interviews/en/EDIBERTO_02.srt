1
00:00:00,041 --> 00:00:02,252
If there was fear in relation

2
00:00:02,252 --> 00:00:06,423
to the industrial plant installed
in the beginning, in the mid-70s, 

3
00:00:07,173 --> 00:00:11,803
I think that in 77 the industrial plant
of Salgema began to produce...

4
00:00:12,470 --> 00:00:14,556
Imagine it now with the duplication of...

5
00:00:15,473 --> 00:00:17,600
at least the part of biochlorethane,

6
00:00:17,600 --> 00:00:20,520
the Biochlorethane plant was going to be duplicated.
As it was!

7
00:00:21,771 --> 00:00:24,024
Then came some bigger reactions.

8
00:00:25,525 --> 00:00:29,112
There was also a backlash
regarding the dumping of water

9
00:00:29,112 --> 00:00:32,323
or water used for cooling boilers.

10
00:00:32,574 --> 00:00:37,245
I don't exactly understand the process,
but there was a fear of pollution in the Mundaú lagoon, 

11
00:00:38,163 --> 00:00:41,624
because the Salgema industrial plant
was the best plant in the world.

12
00:00:42,625 --> 00:00:43,334
It has...

13
00:00:43,334 --> 00:00:46,463
It is just a few kilometers

14
00:00:47,380 --> 00:00:49,716
from the source of raw material

15
00:00:49,716 --> 00:00:52,093
for the brine that is taken from the wells.

16
00:00:53,094 --> 00:00:55,430
It has cheap energy

17
00:00:55,430 --> 00:00:58,516
and it also has alcohol nearby,

18
00:00:59,225 --> 00:01:02,937
what the plants produce, the alcohol
from which you will extract ethylene

19
00:01:03,605 --> 00:01:09,027
to form the component that is
one of the most expensive in production.

20
00:01:10,070 --> 00:01:13,490
So all this ease...

21
00:01:13,490 --> 00:01:15,575
imposed on Alagoas...

22
00:01:15,784 --> 00:01:17,952
and there were threats
at the time, blackmail:

23
00:01:17,952 --> 00:01:21,039
"If you don't want it,
we'll take it to Sergipe!"

24
00:01:21,039 --> 00:01:23,833
The Federal Government basically...

25
00:01:23,958 --> 00:01:27,170
The military imposed on Alagoas
the installation of the plant.

26
00:01:28,254 --> 00:01:30,590
And so it started to work
with all that risk,

27
00:01:30,590 --> 00:01:35,428
with the reactions that already
existed in the mid-80s, when...

28
00:01:36,763 --> 00:01:41,518
There was also a growing ecological
concern incipient in Alagoas

29
00:01:41,726 --> 00:01:43,520
until that moment.

30
00:01:43,520 --> 00:01:46,481
There were, of course, the first ecological reactions.

31
00:01:46,773 --> 00:01:48,858
They fought against...

32
00:01:49,192 --> 00:01:51,778
what the plants threw into the rivers.

33
00:01:54,280 --> 00:01:56,991
This product of sugar production

34
00:01:56,991 --> 00:02:00,328
was thrown into the rivers
and polluted them,

35
00:02:00,328 --> 00:02:02,205
killed the fish, etc.

36
00:02:02,539 --> 00:02:04,666
There were already such concerns.

37
00:02:05,875 --> 00:02:09,170
But it is from the emergence of
a core of environmentalists

38
00:02:09,170 --> 00:02:11,756
in the Planning Department
of the State Government,

39
00:02:12,465 --> 00:02:16,427
that this ecological understanding expands
in Alagoas at a more active level,

40
00:02:16,803 --> 00:02:21,015
at a more participatory level
that results in this movement of the 1980s.

41
00:02:21,266 --> 00:02:24,936
But the main entity that emerged
was from Nivaldo Miranda

42
00:02:24,936 --> 00:02:27,939
with the "Movimento pela Vida"
in the early 80s.

43
00:02:28,439 --> 00:02:30,984
It is when this "movement for life" appears,

44
00:02:31,526 --> 00:02:36,322
which will be the main entity
linked to this medium.

45
00:02:36,322 --> 00:02:40,743
In addition to the assembly
in the state government departments.

46
00:02:43,621 --> 00:02:46,624
From the Environment secretariats
that also start to act

47
00:02:46,624 --> 00:02:49,794
and bring professionals
with this understanding.

48
00:02:50,628 --> 00:02:52,213
We had several manifestations,

49
00:02:52,213 --> 00:02:53,840
now the main one was this act,

50
00:02:54,215 --> 00:02:56,092
which, as you saw, was emptied.

51
00:02:56,426 --> 00:02:59,679
The act is small, relatively small
for such a big problem,

52
00:02:59,804 --> 00:03:01,806
because Maceió...

53
00:03:01,806 --> 00:03:05,476
People thought that the problem was only
for those who lived in Trapiche and Pontal,

54
00:03:05,476 --> 00:03:08,980
and the rest of the city
didn't care as much,

55
00:03:08,980 --> 00:03:11,065
because the problem was the explosion

56
00:03:11,065 --> 00:03:14,903
or the possibility of a gas leak,

57
00:03:14,903 --> 00:03:18,823
and this would be restricted to
an area closer to the industrial plant.

58
00:03:19,574 --> 00:03:20,783
There was no concern,

59
00:03:20,783 --> 00:03:23,161
and coincidentally,
on the same day of the act

60
00:03:23,161 --> 00:03:26,080
or the day before, I can't remember.

61
00:03:26,247 --> 00:03:29,000
I think it was Evilásio Soriano,
who was the Secretary of Planning,

62
00:03:29,000 --> 00:03:31,586
he went on TV and...

63
00:03:32,503 --> 00:03:35,048
He made an intervention
saying that there was no risk,

64
00:03:35,048 --> 00:03:39,802
that is, technically, quote unquote,
it was said that there would be no risk

65
00:03:40,595 --> 00:03:43,640
and that the protest was absurd,
that it made no sense and that...

66
00:03:45,141 --> 00:03:47,852
something like that... I can't remember
the particulars of his intervention

67
00:03:47,852 --> 00:03:51,022
but that also helped to deflate the act.

68
00:03:51,689 --> 00:03:55,318
But it was an insipient movement
and this ecological awareness

69
00:03:55,318 --> 00:03:57,737
did not have the strength it has today,

70
00:03:58,488 --> 00:04:02,909
so it was like that.
Politically, the act didn't... 

71
00:04:02,992 --> 00:04:04,619
It had some repercussions

72
00:04:04,619 --> 00:04:08,331
because the journalists' union
was participating

73
00:04:09,457 --> 00:04:10,959
and other communication professionals

74
00:04:10,959 --> 00:04:15,880
were able to reverberate
this manifestation.

75
00:04:16,089 --> 00:04:18,132
Or its content at least.

76
00:04:18,132 --> 00:04:19,926
The Society for the Defense
of Human Rights,

77
00:04:19,926 --> 00:04:21,886
the "Movimento pela Vida".

78
00:04:22,595 --> 00:04:23,680
Several unions,

79
00:04:23,680 --> 00:04:26,474
I remember mainly the journalists'one,

80
00:04:27,100 --> 00:04:28,893
public figures

81
00:04:29,602 --> 00:04:30,687
of Alagoas politics,

82
00:04:30,687 --> 00:04:32,689
state representatives, city councillors,

83
00:04:33,564 --> 00:04:35,900
I remember that
there was a whole participation

84
00:04:36,067 --> 00:04:38,236
and a concern of some segments.

85
00:04:38,236 --> 00:04:40,113
Even now,

86
00:04:40,321 --> 00:04:42,991
the participation of the people,

87
00:04:42,991 --> 00:04:46,703
apart from those who already had
a certain engagement, agronomy students.

88
00:04:46,703 --> 00:04:49,914
At the time, they had a certain
understanding of this issue,

89
00:04:49,914 --> 00:04:51,582
and some of them stood out

90
00:04:52,333 --> 00:04:54,794
in the field of biology.

91
00:04:55,503 --> 00:04:58,965
Some even begun to emerge
from a technical point of view.

92
00:04:59,674 --> 00:05:01,342
Someone would go to university,

93
00:05:01,342 --> 00:05:02,176
a student,

94
00:05:02,176 --> 00:05:05,305
and they learned that
it had a consequence.

95
00:05:05,305 --> 00:05:07,140
So people got involved,

96
00:05:07,140 --> 00:05:10,393
like José Geraldo Marques,
who played an important role

97
00:05:10,393 --> 00:05:11,644
in this period

98
00:05:12,687 --> 00:05:13,563
of denouncement,

99
00:05:13,563 --> 00:05:16,399
he is a professional,
a technician in the field.

100
00:05:17,650 --> 00:05:20,111
So I think it went something like this.

101
00:05:20,320 --> 00:05:22,488
The act wasn't that big.
It was small.


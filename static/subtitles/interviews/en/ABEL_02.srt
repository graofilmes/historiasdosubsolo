1
00:00:00,280 --> 00:00:01,719
<b>My involvement</b>

2
00:00:01,719 --> 00:00:04,880
<b>with this Braskem problem,</b>

3
00:00:04,880 --> 00:00:08,519
<b>this disastrous mining problem...</b>

4
00:00:09,280 --> 00:00:11,679
<b>My involvement was precisely because</b>

5
00:00:14,440 --> 00:00:16,719
<b>fissures, cracks started to appear</b>

6
00:00:16,719 --> 00:00:19,199
<b>in houses and buildings in Pinheiro.</b>

7
00:00:19,199 --> 00:00:20,880
<b>That is where it began.</b>

8
00:00:20,880 --> 00:00:22,599
<b>And the staff,</b>

9
00:00:22,599 --> 00:00:24,960
<b>whenever there were problems with the building,</b>

10
00:00:24,960 --> 00:00:26,199
<b>they called me</b>

11
00:00:26,920 --> 00:00:29,079
<b>and invited me to see it.</b>

12
00:00:29,079 --> 00:00:30,159
<b>So that is where it began. </b>

13
00:00:30,159 --> 00:00:32,880
<b>Then I noticed this issue back in 2010.</b>

14
00:00:34,400 --> 00:00:37,519
<b>"Wake up, Maceió" happened in 2018,</b>

15
00:00:37,519 --> 00:00:39,719
<b>with the earthquake.</b>

16
00:00:40,599 --> 00:00:43,559
<b>In 2010, it started at Jardim Acácia.</b>

17
00:00:44,199 --> 00:00:46,320
<b>I was called to see</b>

18
00:00:46,320 --> 00:00:48,239
<b>two cracked buildings</b>

19
00:00:48,239 --> 00:00:49,559
<b>and that crack.</b>

20
00:00:49,559 --> 00:00:52,280
<b>That crack in the floor, nobody even cared about it.</b>

21
00:00:53,239 --> 00:00:54,559
<b>I looked at it too and said:</b>

22
00:00:54,559 --> 00:00:57,360
<b>must be from soil irrigation.</b>

23
00:00:57,360 --> 00:01:01,960
<b>This is part of it, but then, in 2000...</b>

24
00:01:01,960 --> 00:01:03,480
<b>A little after that...</b>

25
00:01:03,480 --> 00:01:06,360
<b>2013, 2015, for example... 2014, 2015...</b>

26
00:01:06,360 --> 00:01:08,360
<b>Then other houses,</b>

27
00:01:09,320 --> 00:01:11,320
<b>houses in the higher plains,</b>

28
00:01:11,320 --> 00:01:14,559
<b>in the higher parts of Pinheiro, cracked!</b>

29
00:01:15,440 --> 00:01:17,079
<b>Then another, and another...</b>

30
00:01:17,079 --> 00:01:18,480
<b>They kept calling me to see them,</b>

31
00:01:18,480 --> 00:01:20,360
<b>including to strengthen the foundations</b>

32
00:01:20,360 --> 00:01:22,000
<b>of one of these houses,</b>

33
00:01:22,000 --> 00:01:23,960
<b>and the cracks still came back.</b>

34
00:01:23,960 --> 00:01:25,360
<b>So I go, whoa!</b>

35
00:01:26,000 --> 00:01:29,519
<b>That was before the earthquake happened.</b>

36
00:01:30,360 --> 00:01:31,480
<b>Some houses I reinforced</b>

37
00:01:31,480 --> 00:01:32,800
<b>the foundation twice.</b>

38
00:01:34,119 --> 00:01:38,239
<b>Houses whose soil</b>

39
00:01:38,239 --> 00:01:40,239
<b>I knew like the back of my hand...</b>

40
00:01:40,920 --> 00:01:44,039
<b>On that soil you could</b>

41
00:01:44,039 --> 00:01:45,000
<b>easily put a 4 or 5-story building</b>

42
00:01:45,000 --> 00:01:46,880
<b>without any trouble,</b>

43
00:01:46,880 --> 00:01:49,000
<b>with the foundation</b>

44
00:01:49,000 --> 00:01:51,360
<b>at the same depth as the house!</b>

45
00:01:52,079 --> 00:01:55,000
<b>Even a one-story house cracked!</b>


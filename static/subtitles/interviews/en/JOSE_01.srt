1
00:00:00,559 --> 00:00:03,400
I was getting my Master's

2
00:00:03,400 --> 00:00:04,880
in Ecology

3
00:00:04,880 --> 00:00:07,559
at the Federal University of Rio de Janeiro.

4
00:00:08,840 --> 00:00:10,400
When I lived in Rio,

5
00:00:10,400 --> 00:00:12,880
I was very friendly with Cacá Diegues,

6
00:00:12,880 --> 00:00:16,559
I used to go to his apartment.

7
00:00:17,079 --> 00:00:19,119
<b>He was still married to Nara Leão.</b>

8
00:00:19,719 --> 00:00:22,159
We also became very good friends

9
00:00:22,159 --> 00:00:26,599
and Cacá told me

10
00:00:26,599 --> 00:00:28,840
Divaldo Suruagy

11
00:00:28,840 --> 00:00:33,039
had just been elected governor of Alagoas,

12
00:00:33,039 --> 00:00:36,679
and Cacá was excited because

13
00:00:37,599 --> 00:00:38,639
<b>he read an interview with him.</b>

14
00:00:39,400 --> 00:00:43,559
Him saying he would make an ecological Alagoas

15
00:00:43,559 --> 00:00:44,760
or something like that.

16
00:00:46,119 --> 00:00:49,880
So of course I was pretty excited too.

17
00:00:50,599 --> 00:00:53,280
Then I sent Suruagy a card

18
00:00:53,280 --> 00:00:57,400
congratulating him on the interview.

19
00:00:59,320 --> 00:01:03,360
So Suruagy invited me to...

20
00:01:03,360 --> 00:01:05,559
<b>He wanted to interview me.</b>

21
00:01:06,400 --> 00:01:08,840
<b>So, on one of my trips to Maceió,</b>

22
00:01:12,519 --> 00:01:13,639
<b>he invited me.</b>

23
00:01:13,679 --> 00:01:17,039
<b>He was setting up his secretariat</b>

24
00:01:17,800 --> 00:01:20,440
and he invited me for an interview

25
00:01:20,440 --> 00:01:22,159
at the Legislative Assembly.

26
00:01:23,199 --> 00:01:24,360
<b>So I went there.</b>

27
00:01:24,360 --> 00:01:26,760
<b>It was a really good interview.</b>

28
00:01:28,000 --> 00:01:29,840
Divaldo Suruagy,

29
00:01:29,840 --> 00:01:32,880
whatever the differences... 

30
00:01:33,320 --> 00:01:34,519
<b>But he was a lord.</b>

31
00:01:34,519 --> 00:01:36,239
<b>He was a diplomat.</b>

32
00:01:36,800 --> 00:01:38,320
<b>He was very polite.</b>

33
00:01:39,760 --> 00:01:41,800
And then, at the time,

34
00:01:41,800 --> 00:01:48,239
he said he'd like me to take on

35
00:01:49,599 --> 00:01:53,599
something related to the environment and ecology

36
00:01:53,599 --> 00:01:54,960
here in Alagoas.

37
00:01:55,960 --> 00:01:58,000
So I said I'd like to, 

38
00:01:58,000 --> 00:02:00,599
although I was getting my master's, 

39
00:02:00,599 --> 00:02:03,760
but I could very well interrupt for a while

40
00:02:03,760 --> 00:02:05,679
and then I would come here,

41
00:02:05,679 --> 00:02:07,920
because there was nothing here,

42
00:02:07,920 --> 00:02:11,599
absolutely nothing

43
00:02:11,599 --> 00:02:15,000
regarding environmental protection.

44
00:02:15,920 --> 00:02:17,679
<b>Then he told me</b>

45
00:02:21,599 --> 00:02:23,280
he wanted to nominate me

46
00:02:23,280 --> 00:02:26,840
to a secretariat that already existed

47
00:02:26,840 --> 00:02:28,199
and nobody knew about.

48
00:02:29,519 --> 00:02:33,960
It was called the Executive Secretariat for Pollution Control.

49
00:02:34,880 --> 00:02:39,920
It had been created on the initiative of the Navy

50
00:02:39,920 --> 00:02:42,320
here in Alagoas.

51
00:02:43,440 --> 00:02:46,320
It was created, legally even,

52
00:02:46,320 --> 00:02:47,599
but it was kept asleep.

53
00:02:47,960 --> 00:02:50,039
<b>Nobody had put it to work.</b>

54
00:02:50,599 --> 00:02:55,360
My nomination was considered problematic.

55
00:02:56,639 --> 00:02:58,760
There were two names

56
00:02:58,760 --> 00:03:04,719
<b>that the military establishment of the time</b>

57
00:03:07,519 --> 00:03:09,440
did not look favorably on

58
00:03:09,440 --> 00:03:11,639
setting up his secretariat.

59
00:03:13,559 --> 00:03:16,199
One was from a fantastic person

60
00:03:16,199 --> 00:03:18,760
with whom I came to work later,

61
00:03:18,760 --> 00:03:21,519
Professor José de Melo Gomes,

62
00:03:21,519 --> 00:03:23,559
and the other was mine.

63
00:03:25,280 --> 00:03:28,159
But it was just in case.

64
00:03:28,159 --> 00:03:32,719
So he had no way of making an official nomination.

65
00:03:34,559 --> 00:03:35,840
<b>I would take over,</b>

66
00:03:35,840 --> 00:03:39,840
<b>I would actually answer</b>

67
00:03:41,599 --> 00:03:43,000
<b>for the secretariat,</b>

68
00:03:43,000 --> 00:03:46,000
<b>and we'd build...</b>

69
00:03:46,880 --> 00:03:48,519
But there wasn't even a room

70
00:03:48,519 --> 00:03:50,400
where we could stay.

71
00:03:50,679 --> 00:03:52,199
Team? None!

72
00:03:52,920 --> 00:03:58,800
So I started trying and getting everything.

73
00:04:00,639 --> 00:04:03,199
Then I started worrying

74
00:04:03,199 --> 00:04:05,760
about a possible minimal team

75
00:04:05,760 --> 00:04:07,480
that worked.

76
00:04:07,840 --> 00:04:10,360
People didn't know it was a minimal team

77
00:04:10,360 --> 00:04:12,840
and they thought it was a maximum team.

78
00:04:13,559 --> 00:04:14,599
It was a top team

79
00:04:14,599 --> 00:04:15,920
in terms of...

80
00:04:15,920 --> 00:04:20,079
of giving, enthusiasm and everything.

81
00:04:20,599 --> 00:04:25,639
That's when José Roberto came in,

82
00:04:25,639 --> 00:04:30,559
who later became secretary, occupy the IMA,

83
00:04:31,880 --> 00:04:36,760
<b>and a guy named Osvaldo Viegas.</b>

84
00:04:37,280 --> 00:04:40,719
So the secretariat that existed on paper

85
00:04:40,719 --> 00:04:42,639
and that I actually started to occupy

86
00:04:42,639 --> 00:04:46,280
was called the Executive Secretariat...

87
00:04:46,280 --> 00:04:49,280
<b>It was the Secretariat for Pollution Control.</b>

88
00:04:49,800 --> 00:04:52,360
And I was the Executive Secretary

89
00:04:52,360 --> 00:04:56,199
of Pollution Control of the State of Alagoas.

90
00:04:57,559 --> 00:04:59,639
This went on for a long time,

91
00:04:59,639 --> 00:05:03,360
until Professor José de Melo Gomes

92
00:05:03,360 --> 00:05:04,639
created...

93
00:05:06,000 --> 00:05:08,119
it wasn't the Environment Secretariat back then,

94
00:05:08,119 --> 00:05:11,000
it was the Environmental Coordination,

95
00:05:11,000 --> 00:05:14,039
<b>and he invited me to take over.</b>

96
00:05:15,280 --> 00:05:16,920
He invited our team,

97
00:05:16,920 --> 00:05:20,199
and then we went with the enthusiasm    which was our highest priority at the time.

98
00:05:20,199 --> 00:05:23,960
we were embedded in, involved,

99
00:05:23,960 --> 00:05:28,400
already knowing that we had a war ahead of us,

100
00:05:28,400 --> 00:05:31,559
the implementation of Salgema,

101
00:05:31,559 --> 00:05:34,760
which was our highest priority at the time.

102
00:05:35,599 --> 00:05:39,639
The Secretariat for Pollution Control

103
00:05:39,639 --> 00:05:42,840
was created in 1974.

104
00:05:44,320 --> 00:05:48,000
The creation of the environmental coordination,

105
00:05:48,000 --> 00:05:50,039
if I'm not mistaken,

106
00:05:50,039 --> 00:05:53,920
because I already have the right to...

107
00:05:55,000 --> 00:05:59,400
<b>I have a lot piled on in my memory.</b>

108
00:05:59,679 --> 00:06:02,719
<b>But, if I'm not mistaken, it was in 1976.</b>


1
00:00:00,519 --> 00:00:01,559
<b>I got there</b>

2
00:00:02,679 --> 00:00:04,519
<b>and there was that device, right, Otávio?</b>

3
00:00:05,280 --> 00:00:08,000
<b>It was a device, wasn't it?</b>

4
00:00:08,719 --> 00:00:11,039
<b>But, in my democratic faith,</b>

5
00:00:11,679 --> 00:00:14,199
<b>I went in.</b>

6
00:00:15,639 --> 00:00:17,559
<b>When entering, they told me:</b>

7
00:00:17,559 --> 00:00:19,800
<b>look, there's a list of people</b>

8
00:00:19,800 --> 00:00:21,880
<b>who can and cannot enter.</b>

9
00:00:23,119 --> 00:00:25,119
<b>I replied: so look for my name on it.</b>

10
00:00:25,960 --> 00:00:29,039
<b>So they look here and there, all over,</b>

11
00:00:29,039 --> 00:00:30,480
<b>and said: sorry,</b>

12
00:00:30,480 --> 00:00:33,519
<b>your name is not allowed to enter.</b>

13
00:00:34,639 --> 00:00:38,960
<b>So I sent for the people who were organizing it</b>

14
00:00:38,960 --> 00:00:41,679
<b>and said: look, guys, I'm barred here,</b>

15
00:00:41,679 --> 00:00:43,840
<b>I can't get in.</b>

16
00:00:44,639 --> 00:00:47,800
<b>Look, it's all coordinated by the federal police.</b>

17
00:00:47,800 --> 00:00:50,320
<b>You knew, didn't you, Octávio?</b>

18
00:00:50,320 --> 00:00:53,039
<b>All coordinated by the federal police.</b>

19
00:00:53,039 --> 00:00:55,360
<b>So we're calling the federal police.</b>

20
00:00:55,360 --> 00:00:59,280
<b>If they authorize it, you can come in.</b>

21
00:01:01,119 --> 00:01:03,280
<b>Then came a federal policeman.</b>

22
00:01:05,639 --> 00:01:06,199
<b>Look,</b>

23
00:01:07,280 --> 00:01:10,440
<b>the nicest guy.</b>

24
00:01:10,440 --> 00:01:14,440
<b>I've known these people</b>

25
00:01:14,440 --> 00:01:16,599
<b>since the dictatorship.</b>

26
00:01:17,400 --> 00:01:20,280
<b>Very well trained, aren't they?</b>

27
00:01:20,960 --> 00:01:22,880
<b>But also the most proper,</b>

28
00:01:22,880 --> 00:01:25,480
<b>an extremely polite man.</b>

29
00:01:26,119 --> 00:01:27,639
<b>And he said: look,</b>

30
00:01:27,639 --> 00:01:29,599
<b>officially, you cannot enter,</b>

31
00:01:31,400 --> 00:01:37,000
<b>but -- and he spoke very politely, very delicately,</b>

32
00:01:37,000 --> 00:01:40,119
<b>and said: I'll let you enter.</b>

33
00:01:41,199 --> 00:01:43,400
<b>But, Octávio, this is what you don't know.</b>

34
00:01:43,400 --> 00:01:48,559
<b>But look, under the condition you remain silent</b>

35
00:01:48,559 --> 00:01:50,280
<b>and don't make a fuss.</b>

36
00:01:52,159 --> 00:01:53,559
<b>Was I silent, Octávio?</b>

37
00:01:54,320 --> 00:01:55,519
<b>No!</b>

38
00:01:55,519 --> 00:01:57,400
<b>Did I make a fuss?</b>

39
00:01:59,159 --> 00:01:59,679
<b>Yes!</b>

40
00:01:59,679 --> 00:02:00,519
<b>That's how it goes.</b>

41
00:02:01,559 --> 00:02:03,840
<b>But wasn't it a public hearing?</b>

42
00:02:03,840 --> 00:02:06,639
<b>Yes, but they were trying to bar residents</b>

43
00:02:06,639 --> 00:02:08,159
<b>with that excuse.</b>

44
00:02:08,159 --> 00:02:09,599
<b>Jut a few people?</b>

45
00:02:09,599 --> 00:02:12,079
<b>No, many of them!</b>

46
00:02:12,679 --> 00:02:16,000
<b>It was a previously organized list.</b>

47
00:02:16,000 --> 00:02:17,960
<b>Of unpopular people?</b>

48
00:02:17,960 --> 00:02:22,440
<b>No! Of those authorized to come in.</b>

49
00:02:23,559 --> 00:02:26,800
<b>So if you weren't on the list, you couldn't come in.</b>

50
00:02:27,679 --> 00:02:29,760
<b>So my name wasn't on the list.</b>


1
00:00:00,375 --> 00:00:02,293
When a dam bursts,

2
00:00:02,293 --> 00:00:04,546
you clearly see the before and after.

3
00:00:04,838 --> 00:00:06,297
The dam ruptured,

4
00:00:06,840 --> 00:00:09,384
and that brought

5
00:00:09,384 --> 00:00:13,096
all the disastrous consequences.

6
00:00:13,888 --> 00:00:16,933
Except for Maceió.
The case of Maceió

7
00:00:16,933 --> 00:00:20,228
is one we call "sui generis".

8
00:00:20,562 --> 00:00:23,314
It is considered very bad
because of the magnitude

9
00:00:23,815 --> 00:00:26,901
of mining in urban places,
in smaller towns.

10
00:00:27,569 --> 00:00:31,322
Peixoto de Azevedo in Mato Grosso
is one of those.

11
00:00:31,531 --> 00:00:33,199
But this case right here...

12
00:00:33,950 --> 00:00:37,662
This case involves

13
00:00:37,662 --> 00:00:39,914
an impact that comes

14
00:00:39,914 --> 00:00:41,499
in waves.

15
00:00:41,750 --> 00:00:44,377
This disaster

16
00:00:44,377 --> 00:00:45,503
doesn't happen in a certain day.

17
00:00:45,503 --> 00:00:47,047
It has a peak,

18
00:00:47,464 --> 00:00:49,841
like when that quake happened.

19
00:00:49,841 --> 00:00:52,552
But it's not enough,

20
00:00:52,552 --> 00:00:55,180
so people continue to live there.

21
00:00:55,722 --> 00:00:58,099
Cracks gradually appeared

22
00:00:58,975 --> 00:01:01,186
in some areas

23
00:01:02,479 --> 00:01:03,772
of the neighborhoods.

24
00:01:03,772 --> 00:01:06,107
Then it starts to sink.

25
00:01:07,192 --> 00:01:09,277
But it doesn't happen all at once.

26
00:01:09,277 --> 00:01:11,654
And that from the point of view

27
00:01:11,905 --> 00:01:14,783
of those running a health impact assessment,

28
00:01:15,533 --> 00:01:19,245
it's just as bad as the disaster.

29
00:01:19,954 --> 00:01:22,082
And so many immediate deaths...

30
00:01:23,291 --> 00:01:24,125
Why?

31
00:01:24,125 --> 00:01:25,752
Because it's cummulative.

32
00:01:26,711 --> 00:01:28,755
So people start to live in tension,

33
00:01:29,756 --> 00:01:31,091
and it affects them.

34
00:01:31,091 --> 00:01:32,467
Hence those hypotheses

35
00:01:32,759 --> 00:01:33,510
that I raised:

36
00:01:33,510 --> 00:01:35,261
anxiety disorders,

37
00:01:35,845 --> 00:01:37,305
depression,

38
00:01:37,305 --> 00:01:39,641
suicidal ideation,

39
00:01:40,225 --> 00:01:42,644
suicide attempts,

40
00:01:42,644 --> 00:01:44,687
suicide cases, they'll all increase

41
00:01:44,938 --> 00:01:46,064
among residents.

42
00:01:46,815 --> 00:01:49,317
So it is a set of factors

43
00:01:49,317 --> 00:01:51,653
that imply,

44
00:01:51,653 --> 00:01:54,656
such as the situation of the neighborhood,

45
00:01:54,656 --> 00:01:55,698
of the neighborhoods.

46
00:01:56,032 --> 00:01:57,325
It's a unique situation,

47
00:01:57,325 --> 00:02:01,579
from a community point of view
that were directly affected

48
00:02:01,579 --> 00:02:04,791
by an economic activity such as mining.

49
00:02:05,333 --> 00:02:09,963
What I can assure you is that
there is no case registered

50
00:02:10,713 --> 00:02:11,798
in a capital

51
00:02:12,423 --> 00:02:16,636
with a population that exceeds, for example,
400 thousand inhabitants. Such as Maceió.

52
00:02:17,178 --> 00:02:20,807
So if we loose the metropolitan
area, that's what I'm talking about.

53
00:02:21,224 --> 00:02:22,642
An area of direct influence.

54
00:02:22,642 --> 00:02:24,644
Because it's not just Pinheiro, Mutange,

55
00:02:24,978 --> 00:02:27,730
Bom Parto, Bebedouro that will be affected.

56
00:02:28,398 --> 00:02:30,984
You also have areas of direct influence,

57
00:02:31,484 --> 00:02:33,278
so Pitanguinha will be affected,

58
00:02:33,278 --> 00:02:34,696
Gruta de Lourdes will be affected.

59
00:02:35,405 --> 00:02:38,366
This affects access roads to the city.

60
00:02:38,908 --> 00:02:41,828
This area of ​​direct influence will increase,

61
00:02:42,287 --> 00:02:44,414
it will reach where I am: Jatiúca,

62
00:02:44,956 --> 00:02:48,626
because road access will be directly impaired.

63
00:02:49,627 --> 00:02:53,506
So there's no case in the world
of this magnitude

64
00:02:54,424 --> 00:02:56,759
that compares

65
00:02:57,343 --> 00:03:01,347
to this Braskem case,
because, at the end of the day,

66
00:03:01,347 --> 00:03:04,392
Braskem is buying. They're paying damages, right?

67
00:03:05,310 --> 00:03:07,729
The residents and all those houses,

68
00:03:08,104 --> 00:03:09,856
those people who have stories

69
00:03:10,231 --> 00:03:14,819
and lived there for 20, 30, 60 years.

70
00:03:14,819 --> 00:03:16,487
They give up space

71
00:03:17,113 --> 00:03:19,908
and spaces have memory,
we know they do.

72
00:03:20,658 --> 00:03:22,285
They give up

73
00:03:22,619 --> 00:03:23,661
that space,

74
00:03:24,078 --> 00:03:26,080
and that turns into something.

75
00:03:26,706 --> 00:03:29,751
So this case of Maceió
is a case...

76
00:03:30,585 --> 00:03:32,253
a case like no other.

77
00:03:33,296 --> 00:03:35,215
Unparalleled.

78
00:03:35,215 --> 00:03:36,633
When we make a comparison,

79
00:03:36,674 --> 00:03:39,344
we have to compare what is comparable.

80
00:03:39,344 --> 00:03:42,347
There's no parameter in this magnitude.

81
00:03:42,430 --> 00:03:43,264
There isn't.


1
00:00:01,334 --> 00:00:02,877
The first impact there

2
00:00:02,877 --> 00:00:05,547
was in Trapiche,
because Trapiche was a neighborhood

3
00:00:06,631 --> 00:00:08,717
that today would be like

4
00:00:08,717 --> 00:00:12,554
a green point. An upper middle class
was moving there,

5
00:00:14,347 --> 00:00:16,641
home by the sea...

6
00:00:16,641 --> 00:00:19,394
Close to the city center

7
00:00:19,394 --> 00:00:21,479
there was a huge appreciation of the neighborhood,

8
00:00:22,605 --> 00:00:24,774
with even Trapichão moving there.

9
00:00:24,941 --> 00:00:27,569
In 1970, Trapichão was inaugurated.

10
00:00:28,528 --> 00:00:31,740
I think...

11
00:00:31,740 --> 00:00:34,367
ten years later, around 1979, 1978,

12
00:00:34,367 --> 00:00:36,828
the emergency unit was inaugurated

13
00:00:38,329 --> 00:00:39,914
and there was a displacement

14
00:00:39,914 --> 00:00:43,251
to that area,
the Metropolitan Battalion headquarters

15
00:00:43,460 --> 00:00:46,421
and ostensive policing
also in the Trapiche area.

16
00:00:47,130 --> 00:00:50,508
So it was an expanding neighborhood.
And because my father

17
00:00:50,550 --> 00:00:54,387
was a realtor and sold land there,
I had this understanding

18
00:00:54,512 --> 00:00:58,224
that it was a...
and I had many friends there too.

19
00:00:59,142 --> 00:01:02,187
Surf was being born in Maceió.

20
00:01:02,187 --> 00:01:05,315
Surfing was born in Trapiche,
there in Praia do Sobral, Trapiche.

21
00:01:06,399 --> 00:01:06,691
So...

22
00:01:06,691 --> 00:01:11,571
there was a whole appreciation of that area.
And when the first information

23
00:01:11,571 --> 00:01:14,783
about the implementation of Salgema
in Pontal appeared,

24
00:01:15,742 --> 00:01:18,036
there was a clear reaction to it,

25
00:01:18,203 --> 00:01:21,414
there was an initial discussion.

26
00:01:21,414 --> 00:01:26,252
And, of course, that wasn't a time

27
00:01:26,252 --> 00:01:32,175
when democracy wasn't in vogue,
so that was stifled.

28
00:01:32,300 --> 00:01:35,595
There were no big reactions
because there was a whole discourse

29
00:01:35,595 --> 00:01:40,058
that the development would... 
would pay off and that it was all safe.

30
00:01:40,725 --> 00:01:43,269
Initial studies, Beroaldo Maia Gomes,

31
00:01:43,269 --> 00:01:46,272
who followed this

32
00:01:46,272 --> 00:01:48,358
initial process, he's deceased,

33
00:01:49,609 --> 00:01:50,652
he had...

34
00:01:50,652 --> 00:01:54,030
he received a team from the United States
who came to analyze the location.

35
00:01:55,115 --> 00:01:58,910
And they assured that there was no
risk. And they bought it...

36
00:01:58,910 --> 00:02:02,497
Beroaldo tried to move it, right?
He tried to stop it. He...

37
00:02:03,039 --> 00:02:05,708
tried hard to move it somewhere
else, but couldn't.

38
00:02:06,709 --> 00:02:08,962
Then, with the implementation of Salgema, 

39
00:02:08,962 --> 00:02:12,590
the biggest risk was...
the question was...

40
00:02:13,800 --> 00:02:15,135
Will it explode?

41
00:02:15,677 --> 00:02:19,305
First it was the fear of the explosion.
So those who lived in the neighborhood,

42
00:02:19,639 --> 00:02:22,475
of course, were afraid of
the explosions, of being hit,

43
00:02:22,976 --> 00:02:26,271
especially the residents
of Pontal da Barra.

44
00:02:27,647 --> 00:02:31,151
And the fishing village
used to be different, much smaller,

45
00:02:31,151 --> 00:02:34,696
and they were usually fishermen,
with little handicraft.

46
00:02:36,364 --> 00:02:39,367
But then, the next fear
wasn't the explosion.

47
00:02:39,742 --> 00:02:42,954
Then things got worse because
if there was a leak

48
00:02:43,037 --> 00:02:45,748
of hydrochloric gas,

49
00:02:46,332 --> 00:02:49,878
this gas would spread,
depending on the time of year...

50
00:02:50,295 --> 00:02:53,590
If there was a south or
southeast wind blowing,

51
00:02:54,215 --> 00:02:57,260
this gas would be taken
towards Trapiche da Barra,

52
00:02:57,343 --> 00:03:01,181
Prado, that region there,
I think it would reach Coreia.

53
00:03:02,348 --> 00:03:03,266
So imagine

54
00:03:03,266 --> 00:03:05,351
breathing hydrochloric gas.

55
00:03:06,311 --> 00:03:10,440
You have... our mucosa
has water, H2O.

56
00:03:11,149 --> 00:03:13,735
There'd be a combination with...

57
00:03:14,485 --> 00:03:17,822
with this oxygen...
And it would form hydrochloric acid.

58
00:03:18,990 --> 00:03:22,869
Then you would burn to death
from the inside with acid. Wherever there was gas,

59
00:03:23,369 --> 00:03:26,581
it would turn into acid and
you would die burning inside.

60
00:03:27,540 --> 00:03:28,791
By the acid.

61
00:03:29,375 --> 00:03:32,295
That brought a certain fear as well.

62
00:03:32,295 --> 00:03:35,423
Besides fearing an explosion,
there was this worry

63
00:03:35,423 --> 00:03:39,636
with hydrochloric acid.
But all this was brushed aside.

64
00:03:40,345 --> 00:03:44,766
Later on, in 1982,
if I'm not mistaken,

65
00:03:44,766 --> 00:03:48,770
there was an explosion and
an outsourced employee died.

66
00:03:50,271 --> 00:03:54,317
Then it was brought to the table
again and again and...

67
00:03:55,568 --> 00:03:57,612
and from then on...

68
00:03:58,071 --> 00:04:00,990
As this coincided with the initiative

69
00:04:01,741 --> 00:04:06,663
to double the production of
the diochlorethane plant,

70
00:04:07,080 --> 00:04:11,251
because when Salgema started working
it charged for the Chlorochemical Complex.

71
00:04:12,418 --> 00:04:14,462
So when the Chlorochemical Complex started,

72
00:04:14,462 --> 00:04:17,215
it demanded the duplication of Salgema.

73
00:04:18,091 --> 00:04:20,885
In other words: the supply was
so small that it didn't allow

74
00:04:20,885 --> 00:04:23,263
for this back and forth game.

75
00:04:24,430 --> 00:04:28,685
And that's when the news came out
that there would be a duplication,

76
00:04:28,685 --> 00:04:29,644
and there was a reaction.


1
00:00:00,519 --> 00:00:02,880
<b>To carry out such mining,</b>

2
00:00:02,880 --> 00:00:06,079
<b>you draw a pilot project.</b>

3
00:00:06,079 --> 00:00:07,960
<b>Are you familiar with this pilot project?</b>

4
00:00:07,960 --> 00:00:11,440
<b>You go out there in the field and drill a thousand meters,</b>

5
00:00:11,440 --> 00:00:13,880
<b>then make the caves.</b>

6
00:00:13,880 --> 00:00:17,800
<b>It's planned, everything is planned.</b>

7
00:00:17,800 --> 00:00:20,760
<b>So you make a cave here, another there at 100m,</b>

8
00:00:20,760 --> 00:00:22,199
<b>and make at least three.</b>

9
00:00:22,199 --> 00:00:23,880
<b>Others at 80m.</b>

10
00:00:24,639 --> 00:00:27,880
<b>To see the consequences on the surface.</b>

11
00:00:29,440 --> 00:00:30,559
<b>A pilot project.</b>

12
00:00:31,199 --> 00:00:33,280
<b>Just as an experiment.</b>

13
00:00:34,400 --> 00:00:35,880
<b>Guess if they did that?</b>

14
00:00:38,079 --> 00:00:41,119
<b>Even though they knew it was necessary.</b>

15
00:00:41,119 --> 00:00:42,480
<b>Let's say they didn't at first,o</b>

16
00:00:42,480 --> 00:00:43,880
<b>let's say there was... </b>

17
00:00:43,880 --> 00:00:46,159
<b>Maybe the Americans knew it already,</b>

18
00:00:46,159 --> 00:00:48,119
<b>visibly, how many years ago...</b>

19
00:00:48,119 --> 00:00:50,599
<b>Americans are way ahead of us, aren't they?</b>

20
00:00:50,599 --> 00:00:54,199
<b>By the way, Braskem is all foreign, right?</b>

21
00:00:54,199 --> 00:00:58,559
<b>The consultant is foreign, right?</b>

22
00:00:59,519 --> 00:01:01,559
<b>So, why haven't they tried</b>

23
00:01:01,559 --> 00:01:03,679
<b>to change in the last twenty years?</b>

24
00:01:03,679 --> 00:01:07,239
<b>At least make smaller diameters, right?</b>


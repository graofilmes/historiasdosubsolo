1
00:00:00,000 --> 00:00:01,251
Mining and sustainability

2
00:00:01,251 --> 00:00:03,461
are a contradiction in terms.

3
00:00:04,504 --> 00:00:06,756
Mining...

4
00:00:07,048 --> 00:00:09,634
"So you are against it, Diego".

5
00:00:10,969 --> 00:00:14,514
I will say that with the economic model
that we live in,

6
00:00:15,140 --> 00:00:19,894
highly dependent on mineral resources,
mining will occur. It will happen.

7
00:00:21,229 --> 00:00:24,858
So what's the issue?
With this case in mind,

8
00:00:25,900 --> 00:00:27,902
the issue is when the State,

9
00:00:28,611 --> 00:00:31,031
through its licensing bodies,

10
00:00:31,072 --> 00:00:35,118
exempts itself from the responsibility
of carrying out an assessment

11
00:00:36,453 --> 00:00:37,120
truly

12
00:00:37,120 --> 00:00:39,831
judicious on the impacts
of this activity.

13
00:00:40,415 --> 00:00:44,377
And when civil society doesn't
reap the benefits at all.

14
00:00:45,170 --> 00:00:49,007
I myself am a long-time advocate
that a city like Maceió

15
00:00:49,340 --> 00:00:53,678
should be paying Braskem royalties
to each resident here

16
00:00:54,721 --> 00:00:55,805
in the city.

17
00:00:56,639 --> 00:00:59,768
The less investment you have

18
00:00:59,768 --> 00:01:03,354
in the area of ​​monitoring
and environmental control by the State,

19
00:01:04,064 --> 00:01:07,192
and the smaller
the participation of civil society,

20
00:01:08,902 --> 00:01:11,404
the costs of mining

21
00:01:11,863 --> 00:01:14,783
are even greater than the benefits.

22
00:01:16,201 --> 00:01:17,160
So...

23
00:01:18,078 --> 00:01:20,580
It's a huge dilemma.

24
00:01:21,164 --> 00:01:23,833
So what's easier?
Not having any mining?

25
00:01:23,958 --> 00:01:27,003
Will you loose income?
Yes, you will.

26
00:01:27,003 --> 00:01:28,338
What will take its place?

27
00:01:28,338 --> 00:01:30,423
People ask me that.

28
00:01:31,174 --> 00:01:34,594
They ask:
"What will replace it?"

29
00:01:34,594 --> 00:01:38,681
And you don't have an alternative,
Brazil is becoming a big,

30
00:01:38,681 --> 00:01:41,851
a big farm, a big...

31
00:01:43,103 --> 00:01:45,188
I don't know, a big Serra Pelada.

32
00:01:46,147 --> 00:01:48,316
It's all about exporting commodities, right?

33
00:01:48,316 --> 00:01:50,610
We are de-industrializing more and more.

34
00:01:51,361 --> 00:01:54,405
So when we start to depend
more and more

35
00:01:54,948 --> 00:01:58,576
on primary extraction activities

36
00:01:58,993 --> 00:02:02,497
such as commodities,
less are the chances of development.

37
00:02:03,623 --> 00:02:07,001
And then even this development
is often confused with growth.

38
00:02:08,086 --> 00:02:11,506
So for example: mining doesn't create
many direct and indirects jobs.

39
00:02:12,382 --> 00:02:14,509
Those who generate direct employment

40
00:02:14,509 --> 00:02:17,470
on a larger scale
are small and medium-sized companies,

41
00:02:18,263 --> 00:02:22,809
which do not receive the same affection
from public bodies, for example.

42
00:02:23,434 --> 00:02:26,062
That means you have to lower the...

43
00:02:26,688 --> 00:02:28,481
And we're leaving your question a little bit...

44
00:02:28,481 --> 00:02:31,943
but you have to lower the taxation
of small and medium-sized companies

45
00:02:31,943 --> 00:02:33,444
immensely.

46
00:02:34,279 --> 00:02:38,616
because these are the ones that
will generate a positive externality

47
00:02:38,992 --> 00:02:40,410
and increase jobs.

48
00:02:40,785 --> 00:02:44,080
What about mining, Diego?
Is that the end of mining?

49
00:02:44,706 --> 00:02:47,417
I'll keep it real:
No, it isn't.

50
00:02:48,835 --> 00:02:51,171
So what we have to do

51
00:02:52,338 --> 00:02:54,757
is to enjoy

52
00:02:55,049 --> 00:02:57,886
these situations birthed by mining,

53
00:02:57,886 --> 00:02:59,637
these disasters,

54
00:03:00,221 --> 00:03:03,558
and put civil society
directly aimed at these companies

55
00:03:04,184 --> 00:03:06,895
to gain some control.
That's the only way,

56
00:03:07,979 --> 00:03:09,606
Because the State,

57
00:03:09,814 --> 00:03:12,692
the government, be it Federal or
State Government,

58
00:03:12,692 --> 00:03:15,904
they might think
everything is fine,

59
00:03:16,362 --> 00:03:18,489
since income is coming in.

60
00:03:18,656 --> 00:03:20,533
But is it really?

61
00:03:20,533 --> 00:03:22,994
That resident, Mrs. Maria

62
00:03:24,037 --> 00:03:26,873
from Mutange.
Has she ever reaped

63
00:03:27,123 --> 00:03:29,709
any benefit directly from mining?
Never.

64
00:03:30,418 --> 00:03:35,757
Now, the issue at hand is:
will a mining company set

65
00:03:35,757 --> 00:03:38,384
a deliberative committee?
Because an advisory one...

66
00:03:40,345 --> 00:03:43,473
Now, a deliberative commitee

67
00:03:44,224 --> 00:03:46,935
for the activities it will

68
00:03:46,935 --> 00:03:48,478
perform from now on?

69
00:03:48,478 --> 00:03:50,021
That's the big question.

70
00:03:50,021 --> 00:03:51,522
And it's a feasable one.

71
00:03:52,023 --> 00:03:53,524
Because the government won't
be willing to loose any income.

72
00:03:53,524 --> 00:03:56,152
Oh, won't they? So what are we
going to work with?

73
00:03:56,152 --> 00:04:00,823
So where's civil society in that comoany?
This certification...

74
00:04:01,074 --> 00:04:05,578
ESG, it's been used by shareholders,

75
00:04:05,662 --> 00:04:08,039
because it's all about the shareholders.

76
00:04:08,623 --> 00:04:11,084
How can this company have such certificate

77
00:04:11,709 --> 00:04:14,212
and pay twice as much

78
00:04:15,296 --> 00:04:16,631
compensation for personal injury?

79
00:04:16,631 --> 00:04:21,678
It didn't even reach 500 million.
Not even 400 million reais.

80
00:04:22,720 --> 00:04:26,266
Braskem shareholders had
a good laugh, in my opinion.

81
00:04:26,557 --> 00:04:30,186
When...  which was the amount for

82
00:04:31,813 --> 00:04:33,523
individual compensation.

83
00:04:33,606 --> 00:04:36,067
I'm talking about
personal injury compensation.

84
00:04:37,193 --> 00:04:40,863
We expected around 500 million reais

85
00:04:41,322 --> 00:04:43,700
of personal injury compensation.
How much was it?

86
00:04:45,159 --> 00:04:46,744
But Diego, you have to act
professionally,

87
00:04:46,744 --> 00:04:48,663
you can't laugh.

88
00:04:50,290 --> 00:04:51,708
It didn't even reach 500 million.

89
00:04:51,708 --> 00:04:53,584
So it reached 300? No.

90
00:04:54,335 --> 00:04:55,503
Go lower.

91
00:04:56,546 --> 00:04:57,463
So...

92
00:04:59,090 --> 00:05:02,135
This social control is key,

93
00:05:02,135 --> 00:05:04,262
because mining will stay in place.

94
00:05:04,846 --> 00:05:08,016
Now, with civil society participation or not?

95
00:05:08,016 --> 00:05:09,892
That is the question.


1
00:00:00,239 --> 00:00:02,400
<b>An interesting thing for us to follow</b>

2
00:00:02,400 --> 00:00:05,840
<b>is the compensation of diffuse rights.</b>

3
00:00:05,840 --> 00:00:07,440
<b>So, for example,</b>

4
00:00:07,440 --> 00:00:10,679
<b>today there is a committee set up</b>

5
00:00:10,679 --> 00:00:12,320
<b>by the Federal Public Prosecutor's Office</b>

6
00:00:12,320 --> 00:00:14,280
<b>to work on this issue.</b>

7
00:00:14,280 --> 00:00:16,920
<b>There's been resource forecasting for this.</b>

8
00:00:16,920 --> 00:00:19,599
<b>So how will this resource be used?</b>

9
00:00:19,599 --> 00:00:21,719
<b>And here comes a very important question...</b>

10
00:00:21,719 --> 00:00:26,400
<b>Because like, there is damage to the city,</b>

11
00:00:26,400 --> 00:00:27,960
<b>and this really has to be</b>

12
00:00:27,960 --> 00:00:30,440
<b>in the hands of the municipal public authority,</b>

13
00:00:30,480 --> 00:00:31,400
<b>but really it has to be</b>

14
00:00:31,400 --> 00:00:32,920
<b>in the hands of the public authority</b>

15
00:00:32,920 --> 00:00:34,880
<b>within a broader context,</b>

16
00:00:34,880 --> 00:00:36,719
<b>which is: what in this city</b>

17
00:00:36,719 --> 00:00:38,639
<b>matters most to people?</b>

18
00:00:39,239 --> 00:00:40,840
<b>To the residents,</b>

19
00:00:40,840 --> 00:00:42,559
<b>there are real damages.</b>

20
00:00:43,760 --> 00:00:46,039
<b>I'll give you a very good example:</b>

21
00:00:47,000 --> 00:00:47,880
<b>Quintal Cultural.</b>

22
00:00:47,880 --> 00:00:50,880
<b>Quintal Cultural is a cultural institution</b>

23
00:00:50,880 --> 00:00:55,840
<b>that's been in the city for 15 years, give or take,</b>

24
00:00:55,840 --> 00:00:59,400
<b>with activities recognized by everyone.</b>

25
00:00:59,400 --> 00:01:04,280
<b>It acts in a collective scope of culture.</b>

26
00:01:04,280 --> 00:01:06,159
<b>It's been recognized</b>

27
00:01:06,159 --> 00:01:07,840
<b>as a public entity even,</b>

28
00:01:07,840 --> 00:01:09,320
<b>for many years now.</b>

29
00:01:09,320 --> 00:01:12,519
<b>And this institution has been marked.</b>

30
00:01:12,519 --> 00:01:15,760
<b>It's marked as perhaps having to leave</b>

31
00:01:15,760 --> 00:01:17,800
<b>its headquarters in Bom-Parto.</b>

32
00:01:20,360 --> 00:01:21,920
<b>Quintal Cultural is an institution</b>

33
00:01:21,920 --> 00:01:23,679
<b>which, in terms of diffuse rights,</b>

34
00:01:23,679 --> 00:01:25,440
<b>works with the right to culture</b>

35
00:01:25,440 --> 00:01:27,639
<b>in the region where it is located.</b>

36
00:01:28,239 --> 00:01:31,840
<b>Actually, it even works with human rights now,</b>

37
00:01:31,840 --> 00:01:33,599
<b>doing violence prevention work.</b>

38
00:01:33,599 --> 00:01:35,079
<b>It works with children,</b>

39
00:01:35,079 --> 00:01:38,199
<b>going even bigger than that,</b>

40
00:01:38,199 --> 00:01:41,320
<b>because it now represents CUFA in the state,</b>

41
00:01:41,320 --> 00:01:44,519
<b>and therefore works with all of Maceió.</b>

42
00:01:44,519 --> 00:01:46,880
<b>It works with favelas throughout Maceió</b>

43
00:01:46,880 --> 00:01:48,599
<b>and with favelas from other municipalities.</b>

44
00:01:50,559 --> 00:01:52,679
<b>Now Quintal has to leave,</b>

45
00:01:52,679 --> 00:01:55,440
<b>it has to move out.</b>

46
00:01:56,599 --> 00:01:57,599
<b>So they should be compensated,</b>

47
00:01:57,599 --> 00:01:59,400
<b>and not just because of their headquarters.</b>

48
00:02:00,159 --> 00:02:02,079
<b>They should be compensated</b>

49
00:02:02,079 --> 00:02:03,440
<b>not just for the equity loss,</b>

50
00:02:03,440 --> 00:02:08,519
<b>not just because of property rights</b>

51
00:02:08,519 --> 00:02:09,920
<b>of Quintal per se,</b>

52
00:02:09,920 --> 00:02:12,599
<b>but because of their diffuse rights, cultural rights.</b>

53
00:02:12,599 --> 00:02:14,519
<b>So how do you compensate</b>

54
00:02:14,519 --> 00:02:16,480
<b>removing a cultural institution</b>

55
00:02:16,480 --> 00:02:18,000
<b>of its field of work,</b>

56
00:02:18,000 --> 00:02:20,280
<b>where it plays a major role</b>

57
00:02:20,280 --> 00:02:21,440
<b>in violence prevention?</b>

58
00:02:21,440 --> 00:02:24,840
<b>The loss it represents to the region,</b>

59
00:02:24,840 --> 00:02:26,400
<b>how do you make up for that?</b>

60
00:02:27,880 --> 00:02:29,519
<b>This is a diffuse right,</b>

61
00:02:29,519 --> 00:02:30,480
<b>a cultural offense,</b>

62
00:02:30,480 --> 00:02:32,960
<b>a human right to be compensated.</b>

63
00:02:34,199 --> 00:02:38,119
<b>So how is the company going to act on it?</b>

64
00:02:38,119 --> 00:02:40,880
<b>How will this committee direct</b>

65
00:02:40,880 --> 00:02:44,159
<b>the resources foreseen for this?</b>

66
00:02:44,840 --> 00:02:46,039
<b>For this work?</b>

67
00:02:46,039 --> 00:02:49,159
<b>Who defines it?</b>

68
00:02:50,000 --> 00:02:51,960
<b>Because we're talking about</b>

69
00:02:51,960 --> 00:02:53,960
<b>something that directly affects</b>

70
00:02:53,960 --> 00:02:55,920
<b>the areas that will be vacated.</b>

71
00:02:55,920 --> 00:02:59,000
<b>It's a loss for the region,</b>

72
00:02:59,000 --> 00:03:02,000
<b>This resource has to</b>

73
00:03:02,000 --> 00:03:04,960
<b>be invested in the areas</b>

74
00:03:04,960 --> 00:03:07,199
<b>however the people suffering the loss see fit.</b>

75
00:03:08,440 --> 00:03:09,519
<b>In this case,</b>

76
00:03:09,519 --> 00:03:10,960
<b>how will their opinion be heard?</b>

77
00:03:10,960 --> 00:03:14,920
<b>How will this listening channel be established?</b>

78
00:03:14,920 --> 00:03:17,079
<b>How will the population</b>

79
00:03:17,079 --> 00:03:19,360
<b>have direct participation</b>

80
00:03:19,360 --> 00:03:23,039
<b>in relation to the use of this resource?</b>

81
00:03:23,880 --> 00:03:25,039
<b>That's a question,</b>

82
00:03:25,039 --> 00:03:27,320
<b>one that hasn't been</b>

83
00:03:27,320 --> 00:03:28,800
<b>carefully addressed.</b>

84
00:03:29,599 --> 00:03:31,599
<b>I'm not talking about the organizations,</b>

85
00:03:31,599 --> 00:03:33,079
<b>I don't feel very capable of doing so.</b>

86
00:03:33,079 --> 00:03:34,599
<b>It would be best if someone</b>

87
00:03:34,599 --> 00:03:35,679
<b>followed this up close.</b>

88
00:03:35,679 --> 00:03:37,519
<b>Now, in regards to the diffuse rights,</b>

89
00:03:37,519 --> 00:03:38,760
<b>that I can talk about.</b>

90
00:03:38,760 --> 00:03:40,480
<b>Diffuse rights, cultural rights,</b>

91
00:03:40,480 --> 00:03:43,480
<b>environmental law, human rights...</b>

92
00:03:43,480 --> 00:03:44,960
<b>In my opinion, that's what</b>

93
00:03:44,960 --> 00:03:46,559
<b>hasn't been taken into account.</b>


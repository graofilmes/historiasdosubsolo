1
00:00:00,000 --> 00:00:01,119
<b>Today there are five neighborhoods</b>

2
00:00:01,119 --> 00:00:02,800
<b>affected by the mining in Maceió:</b>

3
00:00:02,800 --> 00:00:04,360
<b>Pinheiro,</b>

4
00:00:04,360 --> 00:00:05,760
<b>Bebedouro,</b>

5
00:00:05,760 --> 00:00:07,159
<b>Mutange,</b>

6
00:00:07,159 --> 00:00:08,159
<b>Bom Parto</b>

7
00:00:08,159 --> 00:00:08,920
<b>and Farol.</b>

8
00:00:08,920 --> 00:00:12,360
<b>Farol is the last one to enter just now at the beginning of 2021.</b>

9
00:00:13,679 --> 00:00:16,679
<b>Out of these five neighborhoods,</b>

10
00:00:17,159 --> 00:00:18,199
<b>three,</b>

11
00:00:18,440 --> 00:00:20,599
<b>Bebedouro, Mutange and Bom Parto,</b>

12
00:00:20,599 --> 00:00:22,400
<b>are lagoon neighborhoods.</b>

13
00:00:22,400 --> 00:00:23,719
<b>Neighborhoods</b>

14
00:00:23,719 --> 00:00:26,280
<b>bathed by the Mundaú lagoon</b>

15
00:00:26,280 --> 00:00:28,559
<b>and going through this subsidence.</b>

16
00:00:28,559 --> 00:00:30,440
<b>And of the five neighborhoods,</b>

17
00:00:30,440 --> 00:00:32,599
<b>we now know...b>

18
00:00:32,599 --> 00:00:34,480
<b>An article came out recently,</b>

19
00:00:35,280 --> 00:00:37,639
<b>the most serious part of the subsidence...</b>

20
00:00:37,639 --> 00:00:38,639
<b>Because what happens to these neighborhoods</b>

21
00:00:38,639 --> 00:00:39,760
<b>is that they are sinking</b>

22
00:00:39,960 --> 00:00:41,800
<b>through the holes left by mining.</b>

23
00:00:42,239 --> 00:00:43,559
<b>Now we know...</b>

24
00:00:43,559 --> 00:00:44,719
<b>It started in Pinheiro,</b>

25
00:00:44,719 --> 00:00:47,000
<b>it was the first neighborhood.</b>

26
00:00:47,000 --> 00:00:49,599
<b>But we know now that actually,</b>

27
00:00:49,599 --> 00:00:52,400
<b>the worst land subsidence</b>

28
00:00:52,400 --> 00:00:54,880
<b>is in these lagoon neighborhoods.</b>

29
00:00:54,880 --> 00:00:58,199
<b>These neighborhoods where mining</b>

30
00:00:58,199 --> 00:01:01,880
<b>has left cavities below the lagoon.</b>

31
00:01:02,800 --> 00:01:05,960
<b>The situation is much worse</b>

32
00:01:05,960 --> 00:01:07,159
<b>than that of Pinheiro.</b>

33
00:01:07,159 --> 00:01:08,679
<b>Bebedouro, for example,</b>

34
00:01:08,679 --> 00:01:10,559
<b>is one of the oldest neighborhoods of Maceió.</b>

35
00:01:10,840 --> 00:01:13,519
<b>One of the oldest settlement neighborhoods.</b>

36
00:01:13,519 --> 00:01:15,840
<b>Maceió has a history</b>

37
00:01:15,840 --> 00:01:18,119
<b>that dates back to the 18th century,</b>

38
00:01:18,119 --> 00:01:20,000
<b>and Bebedouro is a neighborhood</b>

39
00:01:20,000 --> 00:01:22,960
<b>that if we look for the first records</b>

40
00:01:22,960 --> 00:01:24,280
<b>of Maceió,</b>

41
00:01:24,280 --> 00:01:26,159
<b>it is a neighborhood shown</b>

42
00:01:26,159 --> 00:01:27,679
<b>since the 18th century</b>

43
00:01:27,679 --> 00:01:30,400
<b>in traveler reports.</b>

44
00:01:30,840 --> 00:01:33,519
<b>So it appears as a settlement nucleus</b>

45
00:01:33,519 --> 00:01:37,079
<b>right around this water nucleus.</b>

46
00:01:38,280 --> 00:01:40,440
<b>The first creek that served</b>

47
00:01:40,440 --> 00:01:42,639
<b>as a water supply in the city</b>

48
00:01:42,639 --> 00:01:43,880
<b>was Riacho do Silva.</b>

49
00:01:43,880 --> 00:01:46,159
<b>A creek that still exists in Bebedouro.</b>

50
00:01:46,159 --> 00:01:47,280
<b>And it's now polluted. </b>

51
00:01:47,280 --> 00:01:50,719
<b>It is no longer a water source,</b>

52
00:01:50,719 --> 00:01:53,679
<b>but it was Maceió's first known water source.</b>

53
00:01:53,679 --> 00:01:55,559
<b>I have a theory that these settlement centers</b>

54
00:01:55,559 --> 00:01:57,559
<b>are all linked to old ports,</b>

55
00:01:57,880 --> 00:01:59,199
<b>old lagoon ports.</b>

56
00:01:59,199 --> 00:02:02,039
<b>Because what we know</b>

57
00:02:02,039 --> 00:02:06,079
<b>is that Maceió began in a central nucleus</b>

58
00:02:06,079 --> 00:02:08,719
<b>that was supplied by a canal, the Canal da Levada,</b>

59
00:02:08,719 --> 00:02:09,840
<b>and hence the name of the neighborhood</b>

60
00:02:09,840 --> 00:02:12,000
<b>that was the third settlement neighborhood in Maceió.</b>

61
00:02:12,159 --> 00:02:14,360
<b>It was an internal supply center, domestic,</b>

62
00:02:14,360 --> 00:02:16,599
<b>and this supply came through the lagoon.</b>

63
00:02:16,719 --> 00:02:18,840
<b>So what was the domestic supply</b>

64
00:02:18,840 --> 00:02:20,760
<b>of the urban core of Maceió</b>

65
00:02:20,760 --> 00:02:23,079
<b> were transfers,</b>

66
00:02:23,079 --> 00:02:24,679
<b>they were lagoon routes</b>

67
00:02:24,679 --> 00:02:27,679
<b>for supplying goods.</b>

68
00:02:27,679 --> 00:02:29,559
<b>transporting and supplying goods.</b>

69
00:02:29,559 --> 00:02:31,920
<b>So the goods came across the lagoon</b>

70
00:02:31,920 --> 00:02:33,440
<b>via boats,</b>

71
00:02:33,599 --> 00:02:35,239
<b>boats across the lagoon,</b>

72
00:02:35,239 --> 00:02:38,960
<b>and were left in small moorings</b>

73
00:02:39,000 --> 00:02:40,360
<b>along the lagoon margin.</b>

74
00:02:40,360 --> 00:02:42,559
<b>The main port, the biggest one,</b>

75
00:02:42,559 --> 00:02:44,920
<b>was Porto da Levada.</b>

76
00:02:44,920 --> 00:02:47,679
<b>You entered through the canal and arrived at Porto da Levada,</b>

77
00:02:47,679 --> 00:02:50,800
<b>which was next to the main urban nucleus of the city,</b>

78
00:02:50,800 --> 00:02:52,639
<b>which was the central nucleus.</b>

79
00:02:52,639 --> 00:02:55,639
<b>Hence the so-called Maceió.</b>

80
00:02:56,079 --> 00:02:59,880
<b>But next to that main port</b>

81
00:02:59,880 --> 00:03:01,280
<b>there were other small ports.</b>

82
00:03:01,280 --> 00:03:04,760
<b>There was a port called Bebedouro, for example.</b>

83
00:03:05,119 --> 00:03:07,079
<b>And this port, my theory is</b>

84
00:03:07,079 --> 00:03:10,079
<b>that it gave rise to what would</b>

85
00:03:10,079 --> 00:03:11,880
<b>first be the settlement nucleus of Bebedouro</b>

86
00:03:11,880 --> 00:03:13,800
<b>and then the neighborhood of Bebedouro.</b>

87
00:03:13,800 --> 00:03:15,719
<b>Bebedouro relates to water as well.</b>

88
00:03:15,719 --> 00:03:18,880
<b>Bebedouro: a drinking fountain, you drink</b>

89
00:03:18,880 --> 00:03:21,320
<b>and give water, especially to animals.</b>

90
00:03:21,320 --> 00:03:22,719
<b>And this is a historical record.</b>

91
00:03:22,719 --> 00:03:25,639
<b>here is the historical record that for travelers</b>

92
00:03:25,639 --> 00:03:27,559
<b>there was a road that left Maceió</b>

93
00:03:27,559 --> 00:03:30,440
<b>towards the north of the city,</b>

94
00:03:30,440 --> 00:03:33,559
<b>to other municipalities in the Mundaú valley.</b>

95
00:03:33,800 --> 00:03:37,119
<b>And this road was a road called</b>

96
00:03:37,119 --> 00:03:38,960
<b>Estrada do Bebedouro</b>

97
00:03:38,960 --> 00:03:41,440
<b>because travelers would go out</b>

98
00:03:41,440 --> 00:03:43,320
<b>and stop at this creek</b>

99
00:03:43,320 --> 00:03:47,599
<b>to drink water or to give it to their animals.</b>

100
00:03:49,760 --> 00:03:53,440
<b>This core arises from this relation to water.</b>

101
00:03:53,840 --> 00:03:56,119
<b>Maceió has several urban centers</b>

102
00:03:56,119 --> 00:03:58,400
<b>that emerge from its relationship with water.</b>


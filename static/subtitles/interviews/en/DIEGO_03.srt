1
00:00:01,292 --> 00:00:04,045
There's a very serious problem that surrounds

2
00:00:04,713 --> 00:00:07,257
everything that we could call

3
00:00:07,507 --> 00:00:09,843
the licensing of the work,

4
00:00:10,552 --> 00:00:11,761
which is what?

5
00:00:12,220 --> 00:00:14,514
Mining. The whole mining
operation

6
00:00:15,432 --> 00:00:18,893
is an activity that produces impacts.

7
00:00:19,227 --> 00:00:21,896
Every human activity has impacts.

8
00:00:21,896 --> 00:00:23,314
But in the case of mining,

9
00:00:23,314 --> 00:00:24,649
it produces impacts

10
00:00:24,733 --> 00:00:27,444
of a medium or high

11
00:00:27,819 --> 00:00:28,987
magnitude.

12
00:00:29,362 --> 00:00:30,572
Never a low one.

13
00:00:31,656 --> 00:00:35,076
And in the specific case of Braskem,

14
00:00:36,119 --> 00:00:38,538
the environmental licensing

15
00:00:38,538 --> 00:00:42,083
allowed for this mining.

16
00:00:42,959 --> 00:00:45,086
It took place in a period prior to

17
00:00:45,712 --> 00:00:47,922
the 1988 Constitution.

18
00:00:49,090 --> 00:00:50,800
Why is it important?

19
00:00:50,800 --> 00:00:53,595
Because as of the 1988 Constitution,

20
00:00:53,928 --> 00:00:56,931
all the procedures that will guide

21
00:00:56,931 --> 00:01:00,226
environmental licensing
have had constitutional support.

22
00:01:01,019 --> 00:01:05,065
Although the CONAMA resolution still dates
from the military dictatorship in Brazil,

23
00:01:06,191 --> 00:01:08,193
I think 1980,

24
00:01:09,903 --> 00:01:10,820
the guidelines

25
00:01:10,820 --> 00:01:13,907
that will allow us to think,
for example,

26
00:01:13,907 --> 00:01:16,367
about those affected,

27
00:01:16,367 --> 00:01:17,077
rights,

28
00:01:17,077 --> 00:01:20,997
consultations with the populations
that could be affected,

29
00:01:21,414 --> 00:01:22,582
did not occur.
They didn't happen in a way that...

30
00:01:22,624 --> 00:01:25,376
It didn't happen in a way that

31
00:01:25,376 --> 00:01:27,087
we consider inclusive,

32
00:01:27,087 --> 00:01:27,837
as I said.

33
00:01:28,713 --> 00:01:31,883
These areas,
these neighborhoods were already populated.

34
00:01:32,884 --> 00:01:34,219
But this kind of mining...

35
00:01:34,552 --> 00:01:35,929
Mining takes many forms.

36
00:01:35,929 --> 00:01:37,889
But this particular mining

37
00:01:38,973 --> 00:01:40,517
takes place

38
00:01:40,558 --> 00:01:42,519
not at surface level,

39
00:01:42,727 --> 00:01:43,436
but below the surface.

40
00:01:44,813 --> 00:01:46,564
Before, you had residents there.

41
00:01:46,564 --> 00:01:48,900
And then what are the biggest flaws

42
00:01:49,526 --> 00:01:51,152
of all this problem?

43
00:01:51,152 --> 00:01:54,531
When you want to obtain an environmental license,

44
00:01:55,240 --> 00:01:57,534
you have to deliver the environmental impact study.

45
00:01:58,827 --> 00:02:01,329
And you need to file
an environmental impact report.

46
00:02:01,579 --> 00:02:04,541
While the environmental impact study
needs to be technical,

47
00:02:05,750 --> 00:02:08,378
it has to be very rigorous and technical,

48
00:02:08,461 --> 00:02:11,965
the Environmental Impact Report
has to have an accessible language

49
00:02:12,799 --> 00:02:15,969
to, as I tell my students,
make your grandmother understand

50
00:02:16,761 --> 00:02:19,514
the gravity of problems that may arise.

51
00:02:19,764 --> 00:02:20,890
And then, taking advantage

52
00:02:21,224 --> 00:02:23,852
of the size of the problem,

53
00:02:24,978 --> 00:02:28,356
one of the biggest failures involving
the Braskem case

54
00:02:28,648 --> 00:02:32,235
is that you have to do
the identification of impacts.

55
00:02:33,653 --> 00:02:36,072
And you have to predict the impacts.

56
00:02:36,781 --> 00:02:38,408
You have to predict

57
00:02:39,033 --> 00:02:40,660
from my activity

58
00:02:40,910 --> 00:02:42,495
what kind of impact can occur?

59
00:02:44,539 --> 00:02:45,415
What are the impacts?

60
00:02:46,291 --> 00:02:49,794
And then you have to create mitigation measures,

61
00:02:49,794 --> 00:02:51,462
containment ones,

62
00:02:51,462 --> 00:02:52,463
reduction measures.

63
00:02:52,463 --> 00:02:54,883
You have to create ways

64
00:02:54,883 --> 00:02:57,427
to minimize these impacts.

65
00:02:58,678 --> 00:02:59,804
But beyond that,

66
00:02:59,804 --> 00:03:02,932
you need to inform the population.

67
00:03:04,058 --> 00:03:06,186
And that demands what? Monitoring.

68
00:03:07,020 --> 00:03:09,814
Because you need to inform
the population of the risks

69
00:03:10,481 --> 00:03:13,985
that that activity brings.

70
00:03:15,361 --> 00:03:17,488
What are the risks that activity brings?

71
00:03:18,573 --> 00:03:21,701
And then when we are going to observe
the company's behavior 

72
00:03:22,702 --> 00:03:24,996
with the residents,

73
00:03:24,996 --> 00:03:27,832
these risks, the forecast of impacts,

74
00:03:27,832 --> 00:03:28,875
these risks.

75
00:03:29,000 --> 00:03:31,753
The detailing of these risks
and the monitoring

76
00:03:33,463 --> 00:03:36,049
did not occur.

77
00:03:36,799 --> 00:03:39,886
So there are a lot of residents
and a lot of female residents

78
00:03:40,053 --> 00:03:43,056
who simply did not have a clear idea

79
00:03:43,806 --> 00:03:46,059
that they lived in the risk area.

80
00:03:47,268 --> 00:03:49,979
This is not the responsibility of that resident

81
00:03:50,438 --> 00:03:53,399
who has been there for 60 years.

82
00:03:54,108 --> 00:03:57,195
This is the responsibility of the company

83
00:03:57,195 --> 00:03:59,364
producing the impacts.

84
00:03:59,822 --> 00:04:01,532
And the environmental agency,

85
00:04:01,658 --> 00:04:03,159
the licensing agency,

86
00:04:03,159 --> 00:04:04,827
needs to monitor.

87
00:04:05,453 --> 00:04:09,374
Depending on the type of venture,
you have to ponder.

88
00:04:09,707 --> 00:04:13,044
You must do it 
as a matter, even normative,

89
00:04:13,419 --> 00:04:16,089
that you think about the chain of risks

90
00:04:17,131 --> 00:04:19,175
and what happened

91
00:04:19,300 --> 00:04:22,762
effectively, was that the impacts

92
00:04:23,304 --> 00:04:24,681
that were caused

93
00:04:24,681 --> 00:04:25,723
and are generating

94
00:04:25,723 --> 00:04:28,351
all this activity now

95
00:04:28,559 --> 00:04:31,604
thatcame because of the subsidence of the soil.

96
00:04:33,731 --> 00:04:34,440
So Braskem

97
00:04:34,440 --> 00:04:38,194
did not really have policies

98
00:04:38,611 --> 00:04:41,990
from a socio-environmental responsibility
point of view

99
00:04:43,074 --> 00:04:45,868
with the residents.

100
00:04:45,868 --> 00:04:47,954
Because you have to foresee.

101
00:04:48,621 --> 00:04:49,956
That's why I reinforced it here:

102
00:04:49,956 --> 00:04:51,749
you have to identify the impact.

103
00:04:52,208 --> 00:04:53,835
You have to make the prediction.

104
00:04:53,835 --> 00:04:56,212
You have to predict the impacts.

105
00:04:56,212 --> 00:04:58,464
At the end of the day, you must

106
00:04:58,464 --> 00:05:00,842
assess the importance of these impacts.

107
00:05:00,842 --> 00:05:04,429
Because you have to scale:
What is a priority?

108
00:05:05,555 --> 00:05:08,474
This priority has to be reflected in the communities.

109
00:05:09,684 --> 00:05:11,311
And you have to... The companies...

110
00:05:11,311 --> 00:05:12,729
It's not because the company is nice.

111
00:05:13,354 --> 00:05:16,065
It's because it must have a dialogue channel,

112
00:05:16,065 --> 00:05:18,693
not a monologue,
like in the Braskem case.

113
00:05:19,569 --> 00:05:21,946
I don't know if it will change with this disaster,

114
00:05:22,196 --> 00:05:24,032
but usually what happens?

115
00:05:24,032 --> 00:05:25,950
They're monologues

116
00:05:26,617 --> 00:05:29,454
in which the company is here
and it dictates

117
00:05:29,996 --> 00:05:33,791
usually some institutional resources,
for example:

118
00:05:34,709 --> 00:05:37,879
if I tell you that
conflict resolution

119
00:05:38,588 --> 00:05:40,340
is a positive thing,

120
00:05:40,548 --> 00:05:42,425
it is

121
00:05:42,425 --> 00:05:44,510
the search for consensus etc.

122
00:05:44,802 --> 00:05:47,388
But this can also be used
against communities.

123
00:05:47,972 --> 00:05:49,307
This is not held against the company.

124
00:05:49,474 --> 00:05:52,101
This is prolonging a process

125
00:05:53,353 --> 00:05:56,773
to exhaust solidarity.

126
00:05:57,106 --> 00:05:59,317
Because then you divide to conquer.

127
00:05:59,317 --> 00:06:01,861
You deplete group solidarity.

128
00:06:02,945 --> 00:06:05,448
And then you reduce a series of other situations,

129
00:06:05,448 --> 00:06:07,867
for example, compensation amounts,

130
00:06:08,451 --> 00:06:10,787
social resources...
Let's call it that.

131
00:06:10,787 --> 00:06:14,874
For example: that due to the impacts produced,

132
00:06:14,874 --> 00:06:19,003
the company will be responsible
for the construction

133
00:06:19,796 --> 00:06:22,048
of public social facilities.

134
00:06:22,048 --> 00:06:23,466
Investments, for example,


135
00:06:23,800 --> 00:06:27,095
in social savings that
will invest in the training

136
00:06:27,095 --> 00:06:29,013
of children and adolescents.

137
00:06:29,013 --> 00:06:30,765
So it's a series of possible initiatives,

138
00:06:31,099 --> 00:06:33,518
but this usually doesn't happen.

139
00:06:33,518 --> 00:06:38,022
You deplete group solidarity to get everyone
to start thinking

140
00:06:38,940 --> 00:06:40,858
about individual benefit,

141
00:06:40,858 --> 00:06:45,279
and that's applying some procedures

142
00:06:47,115 --> 00:06:48,366
learned in...

143
00:06:49,075 --> 00:06:50,827
in the psychology classroom.

144
00:06:50,827 --> 00:06:54,580
We are seeing that...
that the company lacked professionalism,

145
00:06:55,706 --> 00:06:58,334
because this whole AIA part...

146
00:06:58,835 --> 00:07:04,507
frightens me,
and especially the lack of...

147
00:07:05,883 --> 00:07:08,553
when were you going to talk about this part of AIA?

148
00:07:08,553 --> 00:07:09,720
It's the most important part.

149
00:07:09,720 --> 00:07:10,721
There's no other.

150
00:07:11,931 --> 00:07:14,434
The most important part
is the one that determines for you,

151
00:07:14,434 --> 00:07:17,812
because it is so...
it is directly related to well-being

152
00:07:18,479 --> 00:07:22,191
because biotic impacts are not
as significant

153
00:07:22,483 --> 00:07:25,570
as physical and anthropogenic impacts,
which are the impacts

154
00:07:26,654 --> 00:07:28,531
on the population.

155
00:07:28,614 --> 00:07:30,783
And then what do we see?

156
00:07:30,783 --> 00:07:32,410
We see a lack of...

157
00:07:32,410 --> 00:07:36,539
really professionals and
professionalism of the company.

158
00:07:36,956 --> 00:07:39,750
to really make

159
00:07:39,750 --> 00:07:44,797
a clear identification, a clear prediction,
an assessment of the clear importance of impacts

160
00:07:44,797 --> 00:07:47,884
to the reduction.
What would be the biggest risks...

161
00:07:48,759 --> 00:07:52,263
that that project will cause
in those communities?

162
00:07:53,139 --> 00:07:56,559
because what basically happened
was a fast reaction,

163
00:07:58,060 --> 00:08:02,231
that is, almost a voluntary reaction,

164
00:08:02,648 --> 00:08:07,236
which unfortunately ended up,
for example, reaching other spheres.


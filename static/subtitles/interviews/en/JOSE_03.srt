1
00:00:00,079 --> 00:00:02,440
I'm a very intuitive person,

2
00:00:02,440 --> 00:00:04,519
but that wasn't intuition,

3
00:00:04,519 --> 00:00:06,000
nor a revelation,

4
00:00:06,000 --> 00:00:07,480
none of that.

5
00:00:08,559 --> 00:00:10,159
In terms of science,

6
00:00:10,159 --> 00:00:12,440
I'm very rational.

7
00:00:13,199 --> 00:00:15,800
I want evidence, facts,

8
00:00:15,800 --> 00:00:18,719
and rational analysis.

9
00:00:20,639 --> 00:00:23,639
<b>Testable hypotheses and all that.</b>

10
00:00:24,239 --> 00:00:26,800
When I saw the proposal,

11
00:00:26,800 --> 00:00:30,480
which was still the Salgema proposal,

12
00:00:30,480 --> 00:00:33,000
but the one already on paper.

13
00:00:33,519 --> 00:00:35,840
<b>So I said: look,</b>

14
00:00:36,800 --> 00:00:40,519
If I'm the secretary for pollution control, 

15
00:00:40,519 --> 00:00:43,960
I'd give a negative opinion on the spot.

16
00:00:45,199 --> 00:00:50,719
They threw hydrochloric acid at will,

17
00:00:50,719 --> 00:00:54,239
there on the pier,

18
00:00:54,239 --> 00:00:59,000
and the excuse was that the sea dilutes everything, right?

19
00:00:59,760 --> 00:01:04,480
<b>I know the sea of ​​mud</b>

20
00:01:06,280 --> 00:01:09,199
dilutes everything, right?

21
00:01:09,199 --> 00:01:11,320
But another pollution problem

22
00:01:11,320 --> 00:01:12,519
that appears in the flowchart,

23
00:01:12,519 --> 00:01:15,079
it's in the flowchart to this day,

24
00:01:15,960 --> 00:01:18,119
<b>is the chlorine-soda industry.</b>

25
00:01:18,719 --> 00:01:21,440
<b>There's no way around it.</b>

26
00:01:22,320 --> 00:01:25,079
As well intentioned as it may be,

27
00:01:25,079 --> 00:01:29,000
they're fugitive fumes.

28
00:01:29,760 --> 00:01:32,719
<b>In production itself, in the process,</b>

29
00:01:33,280 --> 00:01:35,719
you inevitably get

30
00:01:35,719 --> 00:01:38,440
chlorine fumes.

31
00:01:39,239 --> 00:01:42,039
We've had them for all these years.

32
00:01:42,039 --> 00:01:44,840
You know we had problems in Pontal.

33
00:01:46,360 --> 00:01:48,559
So, from the beginning,

34
00:01:49,119 --> 00:01:50,800
my opinion was:

35
00:01:50,800 --> 00:01:53,440
No, not happening.

36
00:01:53,440 --> 00:01:57,760
They relied heavily on the technological ignorance

37
00:01:57,760 --> 00:01:59,360
of the state of Alagoas.

38
00:02:00,920 --> 00:02:02,280
And when they realized

39
00:02:02,280 --> 00:02:04,840
we weren't so ignorant after all,

40
00:02:04,840 --> 00:02:07,920
because we weren't guessing,

41
00:02:08,599 --> 00:02:11,880
we issued technical opinions,

42
00:02:11,880 --> 00:02:15,880
scientifically based ones, right?

43
00:02:15,880 --> 00:02:19,599
Then they went for it.


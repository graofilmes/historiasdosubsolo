1
00:00:00,039 --> 00:00:01,679
<b>For the first time</b>

2
00:00:01,679 --> 00:00:03,920
<b>there was a march</b>

3
00:00:04,920 --> 00:00:06,119
in Maceió.

4
00:00:06,119 --> 00:00:09,119
It's well documented by the press.

5
00:00:10,920 --> 00:00:12,360
There was a march

6
00:00:12,360 --> 00:00:15,480
and it ended there,

7
00:00:15,480 --> 00:00:19,239
at the entrance to Rua do Comércio,

8
00:00:19,239 --> 00:00:21,480
which was completely different from today.

9
00:00:22,000 --> 00:00:23,679
There was a platform there

10
00:00:23,679 --> 00:00:26,920
and people gathered, and gathered, and gathered,

11
00:00:26,920 --> 00:00:29,159
and we held a rally.

12
00:00:29,559 --> 00:00:31,400
Anivaldo Miranda spoke,

13
00:00:31,400 --> 00:00:33,480
Kátia Born spoke,

14
00:00:33,480 --> 00:00:35,639
she encouraged me and I spoke.


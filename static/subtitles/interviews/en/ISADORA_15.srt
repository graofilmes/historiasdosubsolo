1
00:00:00,079 --> 00:00:01,719
<b>Maceió isn't a fair city.</b>

2
00:00:01,719 --> 00:00:03,039
<b>Maceió isn't a healthy city.</b>

3
00:00:03,039 --> 00:00:04,880
<b>Maceió isn't a sustainable city.</b>

4
00:00:06,239 --> 00:00:08,360
<b>I say that in every way.</b>

5
00:00:08,360 --> 00:00:10,599
<b>Maceió isn't a righteous city.</b>

6
00:00:11,280 --> 00:00:12,880
<b>Maceió isn't a fair city,</b>

7
00:00:12,880 --> 00:00:16,400
<b>because there's a double standard</b>

8
00:00:16,400 --> 00:00:17,920
<b>for those who have money</b>

9
00:00:17,920 --> 00:00:18,840
<b>and those who don't,</b>

10
00:00:18,840 --> 00:00:22,000
<b>from a municipal government point of view.</b>

11
00:00:22,000 --> 00:00:23,719
<b>We've seen it happen many times.</b>

12
00:00:23,719 --> 00:00:25,719
<b>Maceió isn't a fair city,</b>

13
00:00:25,719 --> 00:00:28,280
<b>because there's an investment gap</b>

14
00:00:28,280 --> 00:00:30,760
<b>between higher income areas</b>

15
00:00:30,760 --> 00:00:31,800
<b>and lower income ones.</b>

16
00:00:31,800 --> 00:00:32,880
<b>And that's a fact!</b>

17
00:00:32,880 --> 00:00:34,880
<b>So, Maceió isn't a fair city</b>

18
00:00:34,880 --> 00:00:37,679
<b>in terms of public management.</b>

19
00:00:37,679 --> 00:00:39,000
<b>Maceió isn't a fair city</b>

20
00:00:39,000 --> 00:00:42,280
<b>in terms of investment.</b>

21
00:00:42,440 --> 00:00:43,760
<b>Maceió isn't a fair city</b>

22
00:00:43,760 --> 00:00:46,679
<b>in terms of public policy,</b>

23
00:00:46,679 --> 00:00:48,599
<b>access to public policy..</b>

24
00:00:48,599 --> 00:00:50,360
<b>Some time ago, we had</b>

25
00:00:50,360 --> 00:00:52,760
<b>a jigger outbreak in the lagoon region</b>

26
00:00:55,000 --> 00:00:56,800
<b>and the Public Prosecutor's Office</b>

27
00:00:56,800 --> 00:00:59,000
<b>had to force the city hall to</b>

28
00:00:59,000 --> 00:01:02,360
<b>place its health programs</b>

29
00:01:02,360 --> 00:01:03,679
<b>on the edge of the lagoon.</b>

30
00:01:03,679 --> 00:01:05,360
<b>As if that wasn't their obligation.</b>

31
00:01:06,920 --> 00:01:07,880
<b>So no,</b>

32
00:01:07,880 --> 00:01:09,519
<b>Maceió isn't a fair city</b>

33
00:01:09,519 --> 00:01:11,559
<b>and Maceió isn't a healthy city,</b>

34
00:01:11,559 --> 00:01:14,039
<b>because if you have people</b>

35
00:01:14,039 --> 00:01:17,719
<b>facing a jigger outbreak,</b>

36
00:01:18,519 --> 00:01:20,840
<b>there's no way these people are healthy.</b>

37
00:01:22,440 --> 00:01:24,159
<b>If you have people</b>

38
00:01:24,159 --> 00:01:26,840
<b>obligated to leave their homes</b>

39
00:01:26,840 --> 00:01:29,559
<b>without even being able to participate in</b>

40
00:01:29,559 --> 00:01:31,280
<b>what will be proposed for the areas</b>

41
00:01:31,280 --> 00:01:32,440
<b>they are leaving...</b>

42
00:01:32,440 --> 00:01:35,039
<b>So Maceió isn't a healthy city.</b>

43
00:01:35,039 --> 00:01:37,400
<b>And it's not a sustainable city</b>

44
00:01:37,400 --> 00:01:39,960
<b>because if you have asphalt</b>

45
00:01:39,960 --> 00:01:42,639
<b>being laid on a street that leads to the beach,</b>

46
00:01:44,920 --> 00:01:46,880
<b>on a residential street,</b>

47
00:01:46,880 --> 00:01:48,480
<b>with local traffic,</b>

48
00:01:49,280 --> 00:01:50,320
<b>and that leads to the beach...</b>

49
00:01:50,320 --> 00:01:54,079
<b>So if you implement a solution</b>

50
00:01:54,079 --> 00:01:56,239
<b>like soil waterproofing</b>

51
00:01:57,000 --> 00:01:59,840
<b>in an area with obvious environmental characteristics,</b>

52
00:01:59,840 --> 00:02:01,360
<b>then Maceió isn't sustainable.</b>

53
00:02:01,360 --> 00:02:05,480
<b>So the fight is for more democratic administrations,</b>

54
00:02:05,480 --> 00:02:07,480
<b>because we're not</b>

55
00:02:07,480 --> 00:02:08,559
<b>a democratic city.</b>

56
00:02:10,159 --> 00:02:12,960
<b>So the fight is for more democratic administrations.</b>

57
00:02:12,960 --> 00:02:14,480
<b>Democratic administrations</b>

58
00:02:14,480 --> 00:02:17,400
<b>tend to lead us into</b>

59
00:02:17,400 --> 00:02:21,599
<b>fairer, healthier and more sustainable cities.</b>


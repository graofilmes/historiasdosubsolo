1
00:00:00,079 --> 00:00:02,559
<b>A good part of the region</b>

two
00:00:02,559 --> 00:00:06,719
<b>is destroyed</b>

3
00:00:06,719 --> 00:00:09,320
<b>as if it were a bombardment.</b>

4
00:00:10,519 --> 00:00:14,119
<b>Then there is no way to recover.</b>

5
00:00:15,239 --> 00:00:17,880
<b>This is a lost cause.</b>

6
00:00:19,119 --> 00:00:23,480
<b>While the recovery program</b>

7
00:00:23,480 --> 00:00:25,719
<b>that was presented</b>

8
00:00:26,360 --> 00:00:29,039
<b>contains very large flaws.</b>

9
00:00:29,840 --> 00:00:31,920
<b>It is a program</b>

10
00:00:31,920 --> 00:00:36,039
<b>that doesn't even need a deeper analysis.</b>

11
00:00:37,039 --> 00:00:41,079
<b>It has parts that are entirely unfeasible.</b>

12
00:00:41,079 --> 00:00:42,559
<b>Unfeasible!</b>

13
00:00:42,559 --> 00:00:44,400
<b>Even if there is</b>

14
00:00:44,400 --> 00:00:48,360
<b>or that there was the best of intentions.</b>

15
00:00:49,519 --> 00:00:50,480
<b>So no.</b>

16
00:00:52,480 --> 00:00:54,679
<b>I even make one...</b>

17
00:00:54,679 --> 00:00:56,800
<b>I even made a proposal</b>

18
00:00:57,599 --> 00:01:00,519
<b>which I think steemed a little</b>

19
00:01:00,519 --> 00:01:03,639
<b>from my anarchic phase that surfaced:</b>

20
00:01:04,559 --> 00:01:10,079
<b>instead of going to Germany</b>

21
00:01:11,440 --> 00:01:15,119
<b>to see the Concentration Camps,</b>

22
00:01:16,559 --> 00:01:18,960
<b>why not transform this area</b>

23
00:01:18,960 --> 00:01:21,000
<b>in a tourist attraction?</b>

24
00:01:21,960 --> 00:01:22,480
<b>Right?</b>

25
00:01:23,239 --> 00:01:24,440
<b>Let's show</b>

26
00:01:24,440 --> 00:01:27,159
<b>as a tourist attraction</b>

27
00:01:27,159 --> 00:01:31,679
<b>how it was possible to bomb without bombs</b>

28
00:01:31,679 --> 00:01:34,000
<b>an entire area of the city!</b>

29
00:01:36,119 --> 00:01:39,079
<b>So, in terms of recovery,</b>

30
00:01:39,719 --> 00:01:43,559
<b>I am totally pessimistic.</b>

31
00:01:43,559 --> 00:01:45,960
<b>With the data I had,</b>

32
00:01:45,960 --> 00:01:48,599
<b>I began to see that it really wasn't</b>

33
00:01:48,599 --> 00:01:50,320
<b>safe at all.</b>

34
00:01:50,320 --> 00:01:53,159
<b>Especially after I woke up</b>

35
00:01:53,159 --> 00:01:56,519
<b>and my house had been spray-painted by the city.</b>

36
00:01:57,280 --> 00:02:00,440
<b>Guys, seriously, this was done in Germany</b>

37
00:02:00,440 --> 00:02:03,280
<b>for the Jews to leave their homes!</b>

38
00:02:03,280 --> 00:02:04,639
<b>It was a code</b>

39
00:02:04,639 --> 00:02:08,320
<b>that later I would know the meaning.</b>

40
00:02:08,320 --> 00:02:10,960
<b>But I hadn't given any authorization</b>

41
00:02:10,960 --> 00:02:13,360
<b>and my house was graffitied.</b>

42
00:02:13,360 --> 00:02:17,880
<b>That is, its value had already plummeted.</b>

43
00:02:19,199 --> 00:02:21,039
<b>So I decided,</b>

44
00:02:21,039 --> 00:02:24,480
<b>mostly in order to protect my family,</b>

45
00:02:24,480 --> 00:02:27,400
<b>to become an environmental refugee.</b>

46
00:02:28,679 --> 00:02:33,400
<b>So I left the house and we migrated.</b>

47
00:02:33,400 --> 00:02:38,079
<b>We are environmental refugees.</b>

48
00:02:38,760 --> 00:02:41,719
<b>Braskem made us a proposal</b>

49
00:02:43,480 --> 00:02:46,239
<b>well below the house's market value.</b>

50
00:02:47,119 --> 00:02:49,960
<b>But this is what our lawyer advised:</b>

51
00:02:50,760 --> 00:02:51,400
<b>take it.</b>

52
00:02:52,800 --> 00:02:55,960
<b>Take it, because if you don't get it now,</b>

53
00:02:55,960 --> 00:03:00,719
<b>you'll be stuck in court for 10, 15 years,</b>

54
00:03:00,719 --> 00:03:02,239
<b>who knows.</b>

55
00:03:03,119 --> 00:03:04,480
<b>So we took it.</b>

56
00:03:05,639 --> 00:03:11,159
<b>We really had our home</b>

57
00:03:11,960 --> 00:03:15,039
<b>compulsorily delivered,</b>

58
00:03:15,039 --> 00:03:17,960
<b>because it was compulsory, to Braskem.</b>

59
00:03:18,599 --> 00:03:22,599
<b>House and land are now owned by Braskem.</b>

60
00:03:23,239 --> 00:03:27,599
<b>I have always used technical-scientific language.</b>

61
00:03:27,599 --> 00:03:30,880
<b>There is a high probability</b>

62
00:03:31,559 --> 00:03:33,920
<b>that this happens.</b>

63
00:03:34,519 --> 00:03:36,360
<b>But the phenomena are very dynamic.</b>

64
00:03:36,360 --> 00:03:39,000
<b>The situation has changed a lot</b>

65
00:03:40,239 --> 00:03:41,840
<b>since the beginning,</b>

66
00:03:42,639 --> 00:03:46,119
<b>including the situations down there.</b>

67
00:03:46,760 --> 00:03:49,159
<b>According to Professor Abel Galindo,</b>

68
00:03:49,159 --> 00:03:51,440
<b>and I really respect his opinion,</b>

69
00:03:51,440 --> 00:03:53,039
<b>of Professor Abel Galindo,</b>

70
00:03:53,039 --> 00:03:54,760
<b>because he works</b>

71
00:03:54,760 --> 00:03:57,239
<b>with report data.</b>

72
00:03:57,239 --> 00:03:59,079
<b>Lately, he's been working</b>

73
00:03:59,079 --> 00:04:02,159
<b>with the data provided by Braskem</b>

74
00:04:02,960 --> 00:04:04,519
<b>in which</b>

75
00:04:04,519 --> 00:04:06,239
<b>I, particularly,</b>

76
00:04:06,239 --> 00:04:08,199
<b>don't really trust.</b>

77
00:04:08,199 --> 00:04:09,719
<b>But Professor Abel</b>

78
00:04:09,719 --> 00:04:11,119
<b>can filter it, I think.</b>

79
00:04:12,320 --> 00:04:17,440
<b>So, according to him, there was a resettlement</b>

80
00:04:18,239 --> 00:04:21,000
<b>right down there</b>

81
00:04:21,000 --> 00:04:27,119
<b>and that initial possibility of collapse,</b>

82
00:04:27,880 --> 00:04:33,320
<b>of a fast collapse,</b>

83
00:04:33,320 --> 00:04:37,719
<b>fortunately seems distant.</b>

84
00:04:38,639 --> 00:04:39,840
<b>Momentarily,</b>

85
00:04:39,840 --> 00:04:43,039
<b>because these things change so easily.</b>

86
00:04:44,199 --> 00:04:49,239
<b>Now, cracks still exist</b>

87
00:04:49,239 --> 00:04:51,960
<b>in the houses and on the streets.</b>

88
00:04:52,760 --> 00:04:56,840
<b>Cracks will continue to exist</b>

89
00:04:56,840 --> 00:04:58,199
<b>and to expand</b>

90
00:04:58,920 --> 00:05:01,280
<b>in the streets and in the houses.</b>

91
00:05:02,000 --> 00:05:03,639
<b>This probability...</b>

92
00:05:03,639 --> 00:05:05,960
<b>Is it a high probability?</b>

93
00:05:05,960 --> 00:05:07,360
<b>No, it's exorbitant!</b>

94
00:05:08,639 --> 00:05:10,119
<b>The danger is, then</b>

95
00:05:10,119 --> 00:05:13,320
<b>transferred, as per new data,</b>

96
00:05:13,320 --> 00:05:16,360
<b>to the region of Bebedouro, next to the lagoon.</b>

97
00:05:18,039 --> 00:05:20,880
<b>Where it was predicted</b>

98
00:05:20,880 --> 00:05:26,000
<b>and where I expressed my doubts from the start</b>

99
00:05:26,000 --> 00:05:29,079
<b>that there would really be a collapse</b>

100
00:05:29,079 --> 00:05:31,199
<b>in the region of the lagoons.</b>

101
00:05:31,199 --> 00:05:35,760
<b>An intense, strong and fast.</b>

102
00:05:36,480 --> 00:05:37,880
<b>With a great landslide,</b>

103
00:05:37,880 --> 00:05:39,599
<b>a large subsidence.</b>

104
00:05:41,000 --> 00:05:42,320
<b>Nowadays, Professor Abel</b>

105
00:05:42,320 --> 00:05:44,760
<b>questions this as well.</b>

106
00:05:44,760 --> 00:05:46,679
<b>What he reaffirms</b>

107
00:05:46,679 --> 00:05:48,880
<b>and that is completely undeniable,</b>

108
00:05:48,880 --> 00:05:51,400
<b>because you can see with your own eyes,</b>

109
00:05:51,400 --> 00:05:55,639
<b>from my drone you can see very well...</b>

110
00:05:57,280 --> 00:05:59,239
<b>What is foreseen is that it has...</b>

111
00:05:59,239 --> 00:06:02,239
<b>And it's a very high probability!</b>

112
00:06:02,239 --> 00:06:06,119
<b>That whole Bebedouro area</b>

113
00:06:06,119 --> 00:06:09,960
<b>between, more or less, the IMA</b>

114
00:06:09,960 --> 00:06:12,000
<b>and the Bom Conselho school,</b>

115
00:06:12,000 --> 00:06:15,559
<b>within a relatively short time,</b>

116
00:06:15,559 --> 00:06:17,559
<b>it will be utterly destroyed.</b>


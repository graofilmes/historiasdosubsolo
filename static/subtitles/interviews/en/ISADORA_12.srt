1
00:00:00,199 --> 00:00:01,480
<b>First I'd say</b>

2
00:00:01,480 --> 00:00:04,679
<b>that, unfortunately, this isn't unusual.</b>

3
00:00:06,079 --> 00:00:08,159
<b>This isn't unusual.</b>

4
00:00:08,159 --> 00:00:13,159
<b>This has been seen in other tragedies.</b>

5
00:00:13,159 --> 00:00:15,239
<b>I'm not sure you know that movie,</b>

6
00:00:15,239 --> 00:00:16,320
<b>Erin Brockovich.</b>

7
00:00:17,320 --> 00:00:18,960
<b>When the tragedy hits,</b>

8
00:00:18,960 --> 00:00:20,880
<b>the first thing the company does,</b>

9
00:00:20,880 --> 00:00:21,880
<b>don't know if you remember this,</b>

10
00:00:21,880 --> 00:00:23,840
<b>is to offer to buy the house.</b>

11
00:00:26,280 --> 00:00:27,559
<b>So this isn't unusual.</b>

12
00:00:27,559 --> 00:00:29,559
<b>The first they do is</b>

13
00:00:29,559 --> 00:00:31,400
<b>offer to buy the house</b>

14
00:00:31,400 --> 00:00:33,559
<b>for above market value</b>

15
00:00:34,119 --> 00:00:36,840
<b>as a sort of compensation.</b>

16
00:00:38,280 --> 00:00:39,519
<b>So this isn't unusual.</b>

17
00:00:40,559 --> 00:00:43,119
<b>I have some suspicions...</b>

18
00:00:44,000 --> 00:00:45,599
<b>First, I think that</b>

19
00:00:46,679 --> 00:00:48,320
<b>the Federal Public Prosecutor's Office</b>

20
00:00:48,320 --> 00:00:49,239
<b>has a committee</b>

21
00:00:49,239 --> 00:00:51,079
<b>that has been in charge of this from the start.</b>

22
00:00:51,079 --> 00:00:52,760
<b>There is a committee from the start</b>
<b>dealing with this, right? With an agreement?</b>
<b>It started with serious people,</b>
<b>a task force of serious people.</b>
<b>They are serious people,</b>
<b>known for a long time, and I think that...</b>
<b>I believe that</b>
<b>the public prosecutor must be in</b>
<b>a very delicate situation.</b>

23
00:00:52,760 --> 00:00:54,000
<b>It's a committee of serious people,</b>

24
00:00:54,000 --> 00:00:55,719
<b>it's a task force of serious people.</b>

25
00:00:55,719 --> 00:00:57,760
<b>Stranieri, Kaspary and such...</b>

26
00:00:57,760 --> 00:00:58,599
<b>Serious people,</b>

27
00:00:58,599 --> 00:01:01,039
<b>known for a long time.</b>

28
00:01:01,039 --> 00:01:03,280
<b>I believe that</b>

29
00:01:03,280 --> 00:01:05,559
<b>the Federal Public Prosecutor's Office</b>

30
00:01:05,559 --> 00:01:08,559
<b>must be in a very delicate situation.</b>

31
00:01:09,159 --> 00:01:11,119
<b>Because this tragedy</b>

32
00:01:11,119 --> 00:01:14,920
<b>it already has,</b>

33
00:01:14,920 --> 00:01:16,039
<b>financially speaking,</b>

34
00:01:16,039 --> 00:01:18,400
<b>a very significant configuration.</b>

35
00:01:18,400 --> 00:01:20,840
<b>And for now it doesn't show signs</b>

36
00:01:20,840 --> 00:01:24,599
<b>of decrease in terms of impact,/b>

37
00:01:24,599 --> 00:01:26,199
<b>it only increases.</b>

38
00:01:26,199 --> 00:01:28,960
<b>I remember reading once</b>

39
00:01:28,960 --> 00:01:34,400
<b>that the financial commitments</b>

40
00:01:34,400 --> 00:01:35,960
<b>Braskem was assuming</b>

41
00:01:36,599 --> 00:01:37,840
<b>with the tragedy,</b>

42
00:01:37,840 --> 00:01:39,320
<b>I think last year,</b>

43
00:01:39,320 --> 00:01:41,480
<b>they were already reaching</b>

44
00:01:41,480 --> 00:01:45,159
<b>50% of the company's assets.</b>

45
00:01:46,320 --> 00:01:48,000
<b>I remember seeing this somewhere.</b>

46
00:01:50,960 --> 00:01:52,519
<b>It's a delicate balance, isn't it?</b>

47
00:01:52,519 --> 00:01:54,840
<b>Between ensuring that the company</b>

48
00:01:54,840 --> 00:01:57,400
<b>pays everything it has to pay</b>

49
00:01:57,400 --> 00:02:00,400
<b>in terms of fair compensation</b>

50
00:02:00,400 --> 00:02:03,559
<b>and not going bankrupt,</b>

51
00:02:03,559 --> 00:02:05,000
<b>because if it goes bankrupt</b>

52
00:02:05,000 --> 00:02:05,920
<b>it doesn't pay anything else,</b>

53
00:02:08,440 --> 00:02:10,800
<b>I think there must be a great fear</b>

54
00:02:10,800 --> 00:02:13,239
<b>on the part of the Federal
Public Prosecutor's Office</b>

55
00:02:13,239 --> 00:02:15,239
<b>that this will happen.</b>

56
00:02:15,239 --> 00:02:17,159
<b>They must be trying to find</b>

57
00:02:17,159 --> 00:02:18,840
<b>a balance all the time</b>

58
00:02:18,840 --> 00:02:22,480
<b>on how to guarantee this fair compensation</b>

59
00:02:22,480 --> 00:02:23,559
<b>and, at the same time,</b>

60
00:02:23,559 --> 00:02:25,280
<b>not make the company unfeasible</b>

61
00:02:25,280 --> 00:02:26,800
<b>so that it doesn't go bankrupt.</b>

62
00:02:26,800 --> 00:02:28,239
<b>Because if it goes bankrupt,</b>

63
00:02:30,880 --> 00:02:31,360
<b>that's it...</b>

64
00:02:32,480 --> 00:02:33,280
<b>It goes bankrupt...</b>

65
00:02:33,280 --> 00:02:35,199
<b>You see the social security debt</b>

66
00:02:35,199 --> 00:02:38,079
<b>that Vale has had for many decades</b>

67
00:02:38,079 --> 00:02:40,280
<b>and that it hasn't paid, because it went bankrupt.</b>

68
00:02:40,719 --> 00:02:42,159
<b>And in relation to Braskem</b>

69
00:02:42,159 --> 00:02:46,840
<b>becoming the owner of this territory,</b>

70
00:02:46,840 --> 00:02:48,960
<b>I see it with gret concern.</b>

71
00:02:49,440 --> 00:02:50,840
<b>In my opinion,</b>

72
00:02:50,840 --> 00:02:53,719
<b>if we work this area...</b>

73
00:02:53,719 --> 00:02:55,159
<b>First we have to see if...</b>

74
00:02:55,159 --> 00:02:57,360
<b>I'm not so sure legally</b>

75
00:02:57,360 --> 00:02:59,920
<b>what can and cannot be reversed.</b>

76
00:03:00,639 --> 00:03:02,519
<b>If these agreements were intermediated</b>

77
00:03:02,519 --> 00:03:03,920
<b>by the Federal Public Prosecutor's Office,</b>

78
00:03:03,920 --> 00:03:05,280
<b>can they be reversed</b>

79
00:03:05,280 --> 00:03:07,440
<b>in relation to what has already been defined?</b>

80
00:03:07,440 --> 00:03:08,639
<b>I'm not sure about that.</b>

81
00:03:08,639 --> 00:03:12,880
<b>I think, I believe it cannot,</b>

82
00:03:12,880 --> 00:03:14,840
<b>for the sake of legal certainty.</b>

83
00:03:15,199 --> 00:03:16,599
<bI believe once you settle an agreement,</b>

84
00:03:16,599 --> 00:03:18,000
<b>it's settled.</b>

85
00:03:18,000 --> 00:03:21,639
<b>But what would I like to happen?</b>

86
00:03:21,639 --> 00:03:23,039
<b>First, I would like...</b>

87
00:03:23,039 --> 00:03:24,960
<b>this territory not to be left to Braskem.</b>

88
00:03:24,960 --> 00:03:25,880
<b>First of all,</b>

89
00:03:25,880 --> 00:03:26,840
<b>I think that the company shouldn't</b>

90
00:03:26,840 --> 00:03:28,760
<b>remain the owner of this territory.</b>

91
00:03:29,960 --> 00:03:32,480
<b>Because they destroyed it.</b>

92
00:03:32,480 --> 00:03:35,400
<b>They shouldn't have a right to a place</b>

93
00:03:35,400 --> 00:03:36,239
<b>it destroyed.</b>

94
00:03:36,239 --> 00:03:37,519
<b>That's the first thing.</b>

95
00:03:37,519 --> 00:03:38,960
<b>The second is:</b>

96
00:03:38,960 --> 00:03:40,639
<b>if this agreement cannot be reversed,</b>

97
00:03:40,639 --> 00:03:41,920
<b>what I would like is</b>

98
00:03:41,920 --> 00:03:44,079
<b>for it to be obligated.</b>

99
00:03:44,079 --> 00:03:46,960
<b>Do you know when you own a territory</b>

100
00:03:46,960 --> 00:03:49,119
<b>and turn that territory into an app?</b>

101
00:03:50,760 --> 00:03:53,599
<b>Transforming a territory into app is something for life.</b>

102
00:03:54,880 --> 00:03:57,719
<b>Afterwards, even if you want to change, you can't.</b>

103
00:03:57,719 --> 00:03:59,760
<b>Even if your descendants want to change,</b>

104
00:03:59,760 --> 00:04:00,599
<b>they cannot.</b>

105
00:04:01,920 --> 00:04:04,159
<b>There are legal mechanisms</b>

106
00:04:04,159 --> 00:04:05,440
<b>for Braskem,</b>

107
00:04:05,440 --> 00:04:08,320
<b>even if it remains the owner of these areas,</b>

108
00:04:08,320 --> 00:04:11,559
<b>it must transform these areas</b>

109
00:04:11,559 --> 00:04:13,920
<b>into areas of collective usufruct.</b>


1
00:00:00,320 --> 00:00:02,159
<b>This is the eye of the storm!</b>

2
00:00:02,159 --> 00:00:03,159
<b>See here, mine 7?</b>

3
00:00:03,159 --> 00:00:06,480
<b>Mine 19 and mine 18.</b>

4
00:00:06,480 --> 00:00:09,119
<b>This here at the first moment had...</b>

5
00:00:09,920 --> 00:00:11,840
<b>I measured the radius here,</b>

6
00:00:11,840 --> 00:00:16,440
<b>it was 200 something meters that I measured,</b>

7
00:00:16,440 --> 00:00:20,039
<b>enough to fit 2 Maracanãs</b>

8
00:00:20,039 --> 00:00:22,880
<b>in this area here, see?</b>

9
00:00:24,440 --> 00:00:26,440
<b>Because they joined three mines,</b>

10
00:00:27,079 --> 00:00:28,119
<b>these three here, see?</b>

11
00:00:28,119 --> 00:00:30,119
<b>But then it collapsed, didn't it?</b>

12
00:00:30,119 --> 00:00:31,800
<b>It was a problem just piling up.</b>

13
00:00:32,599 --> 00:00:35,400
<b>Then when the...</b>

14
00:00:36,639 --> 00:00:38,400
<b>when sonar studies</b>

15
00:00:38,400 --> 00:00:40,400
<b>were done, right?</b>

16
00:00:40,400 --> 00:00:42,679
<b>The sonar is that camera that goes down</b>

17
00:00:42,679 --> 00:00:44,280
<b>and films everything that is down there.</b>

18
00:00:44,280 --> 00:00:45,199
<b>By the sonar sound, right?</b>

19
00:00:45,800 --> 00:00:48,800
<b>So, when the Germans</b>

20
00:00:50,679 --> 00:00:52,199
<b>entered this conversation,</b>

21
00:00:52,199 --> 00:00:56,599
<b>there were already 450m inside here,</b>

22
00:00:56,599 --> 00:00:58,239
<b>because it was collapsing, right? And such.</b>

23
00:00:58,239 --> 00:01:00,360
<b>So, this here is three mines,</b>

24
00:01:00,360 --> 00:01:01,800
<b>you can see the three mines there.</b>

25
00:01:01,800 --> 00:01:05,599
<b>There are two here, you see? Both 9 and 12.</b>

26
00:01:06,719 --> 00:01:09,320
<b>And just like that, in addition to coming together,</b>

27
00:01:09,320 --> 00:01:12,320
<b>you see that 3, 13 are all together here, right?</b>

28
00:01:13,360 --> 00:01:15,239
<b>Look at 13 and 16 as they are.</b>

29
00:01:15,239 --> 00:01:16,400
<b>You see here, how close?</b>

30
00:01:16,400 --> 00:01:18,239
<b>There's more down here, look.</b>

31
00:01:18,239 --> 00:01:19,679
<b>20 and 21</b>

32
00:01:19,679 --> 00:01:21,119
<b>came together as well.</b>

33
00:01:21,119 --> 00:01:22,400
<b>Became one.</b>

34
00:01:22,400 --> 00:01:25,519
<b>Look how 29 is close to this one.</b>

35
00:01:25,519 --> 00:01:26,519
<b>Look at 27!</b>

36
00:01:26,519 --> 00:01:29,800
<b>All very far from the 100m</b>

37
00:01:29,800 --> 00:01:32,199
<b>that I think would be safe.</b>

38
00:01:33,079 --> 00:01:34,320
<b>You see this here!</b>

39
00:01:34,320 --> 00:01:36,800
<b>Look at 23 and 15!</b>

40
00:01:36,800 --> 00:01:38,880
<b>Look at 22 and 23. See how it is now?</b>

41
00:01:39,599 --> 00:01:41,000
<b>Here, together, very close together.</b>

42
00:01:42,039 --> 00:01:43,639
<b>Here the distance is practically zero.</b>

43
00:01:43,639 --> 00:01:45,159
<b>And so it goes.</b>

44
00:01:45,159 --> 00:01:47,079
<b>So, you have few mines.</b>

45
00:01:47,920 --> 00:01:49,480
<b>For example, these mines</b>

46
00:01:49,480 --> 00:01:52,840
<b>between 24 and 34, that's a reasonable distance,</b>

47
00:01:54,320 --> 00:01:56,079
<b>I believe it's around 100m.</b>

48
00:01:56,079 --> 00:01:58,440
<b>Also mine 11</b>

49
00:01:58,440 --> 00:02:00,679
<b>is also around 80m to 100m.</b>

50
00:02:00,679 --> 00:02:02,320
<b>That's good!</b>

51
00:02:02,320 --> 00:02:03,880
<b>This is the only one that saves,</b>

52
00:02:03,880 --> 00:02:06,440
<b>but the rest, look here, look!</b>

53
00:02:07,159 --> 00:02:11,000
<b>This here is all a horrible thing!</b>

54
00:02:11,320 --> 00:02:13,119
<b>The ones that are sinking</b>

55
00:02:13,119 --> 00:02:14,119
<b>are here...</b>

56
00:02:16,960 --> 00:02:18,719
<b>From mine 11...</b>

57
00:02:18,719 --> 00:02:23,119
<b>IMA is here. I always give the IMA reference.</b>

58
00:02:23,119 --> 00:02:26,280
<b>So it's from IMA up to the Bom Conselho school,</b>

59
00:02:26,280 --> 00:02:29,559
<b>which is here close to 32.</b>

60
00:02:29,559 --> 00:02:32,800
<b>This area here started sinking</b>

61
00:02:32,800 --> 00:02:35,719
<b>millimeters 20 years ago</b>

62
00:02:35,719 --> 00:02:37,920
<b>and now it's centimeters per year</b>

63
00:02:37,920 --> 00:02:40,480
<b>in the range of about 25cm per year.</b>

64
00:02:41,119 --> 00:02:44,960
<b>This here is the area where</b>

65
00:02:44,960 --> 00:02:46,760
<b>rock salt occurs.</b>

66
00:02:47,800 --> 00:02:50,280
<b>See how it's...</b>

67
00:02:50,280 --> 00:02:52,440
<b>It's actually good to show you this...</b>

68
00:02:52,440 --> 00:02:54,360
<b>Yeah, because there was that nonsense</b>

69
00:02:54,360 --> 00:02:57,039
<b>when they said all of Marcela was going to sink.</b>

70
00:02:57,920 --> 00:03:01,599
<b>It only sinks where there is rock salt,</b>

71
00:03:01,599 --> 00:03:03,480
<b>and the rock salt exploration area is here.</b>

72
00:03:03,480 --> 00:03:05,719
<b>This is the area that is really sinking.</b>

73
00:03:05,719 --> 00:03:10,159
<b>The mines are sinking</b>

74
00:03:10,159 --> 00:03:13,039
<b>and pulling and giving traction here, right?</b>

75
00:03:13,039 --> 00:03:14,800
<b>So this part that goes through here</b>

76
00:03:14,800 --> 00:03:16,960
<b>from Fernandes Lima, it's just 400m.</b>

77
00:03:16,960 --> 00:03:19,159
<b>Even so,</b>

78
00:03:19,159 --> 00:03:22,079
<b>the thickness that arrives here is very small.</b>

79
00:03:22,079 --> 00:03:24,159
<b>Here are the mines, some mines.</b>

80
00:03:24,159 --> 00:03:26,480
<b>The closest one, let's say 2,</b>

81
00:03:26,480 --> 00:03:28,559
<b>is here, see?</b>

82
00:03:28,559 --> 00:03:30,719
<b>Here's the end of 2, okay?</b>

83
00:03:30,800 --> 00:03:32,679
<b>So mine 2 is this one, it's the most advanced.</b>

84
00:03:32,679 --> 00:03:35,039
<b>Mine 2 is here, see? Mine 2.</b>

85
00:03:40,679 --> 00:03:43,320
<b>Rock salt still occurs up to here.</b>

86
00:03:43,320 --> 00:03:44,679
<b>See this?</b>

87
00:03:44,679 --> 00:03:46,800
<b>Here's Fernandes Lima, up here.</b>

88
00:03:50,000 --> 00:03:55,159
<b>Then you see that the rock salt ends here.</b>

89
00:03:55,880 --> 00:03:56,599
<b>400m.</b>

90
00:03:56,599 --> 00:03:58,719
<b>This part here has no strength.</b>

91
00:03:58,719 --> 00:04:00,599
<b>And this is the current map</b>

92
00:04:00,599 --> 00:04:03,480
<b>showing there is something here,</b>

93
00:04:03,480 --> 00:04:05,280
<b>some cracks and such...</b>

94
00:04:06,639 --> 00:04:09,559
<b>Reaching houses that have</b>

95
00:04:09,559 --> 00:04:12,920
<b>a crack, it's a very small thing here,</b>

96
00:04:12,920 --> 00:04:14,079
<b>small things.</b>

97
00:04:14,079 --> 00:04:18,000
<b>But as you move away from the mine,</b>

98
00:04:18,000 --> 00:04:19,880
<b>it becomes less and less damaged.</b>

99
00:04:19,880 --> 00:04:22,320
<b>The effect gets smaller and smaller.</b>

100
00:04:22,320 --> 00:04:23,920
<b>This area here in red,</b>

101
00:04:23,920 --> 00:04:24,599
<b>this is the panic zone.</b>

102
00:04:24,599 --> 00:04:28,199
<b>People, a lot of people call me</b>

103
00:04:28,199 --> 00:04:29,480
<b>and send me messages,</b>

104
00:04:29,480 --> 00:04:31,239
<b>that have already come here.</b>

105
00:04:31,840 --> 00:04:35,719
<b>500m past the current area.</b>

106
00:04:35,719 --> 00:04:39,039
<b>So this here has absolutely nothing,</b>

107
00:04:39,039 --> 00:04:40,880
<b>not even the slightest chance of...</b>

108
00:04:40,880 --> 00:04:43,559
<b>The sinking is just here.</b>

109
00:04:43,559 --> 00:04:44,800
<b>This here is a crack</b>

110
00:04:44,800 --> 00:04:46,519
<b>and here, if it happens,</b>

111
00:04:46,519 --> 00:04:48,039
<b>if it happens,</b>

112
00:04:48,039 --> 00:04:49,960
<b>they'd be very insignificant cracks.</b>

113
00:04:49,960 --> 00:04:51,079
<b>If it happens.</b>

114
00:04:51,079 --> 00:04:52,760
<b>Now, obviously Braskem</b>

115
00:04:52,760 --> 00:04:53,880
<b>has to pay for all of this.</b>

116
00:04:54,800 --> 00:04:55,719
<b>Pay for everything!</b>

117
00:04:55,719 --> 00:04:57,559
<b>Because it was undervalued.</b>

118
00:04:57,559 --> 00:04:59,039
<b>Who wants to buy a property here?</b>

119
00:04:59,960 --> 00:05:01,159
<b>Nobody!</b>

120
00:05:01,440 --> 00:05:03,800
<b>Strength, what's the creep strength?</b>

121
00:05:03,800 --> 00:05:05,280
<b>See it here?</b>

122
00:05:05,280 --> 00:05:07,199
<b>It gets softer</b>

123
00:05:07,199 --> 00:05:08,320
<b>and softer,</b>

124
00:05:08,320 --> 00:05:09,679
<b>and it sinks here, look.</b>

125
00:05:09,679 --> 00:05:12,239
<b>It sinks not only here, but here as well,</b>

126
00:05:12,239 --> 00:05:13,800
<b>and makes cracks up here.</b>

127
00:05:13,800 --> 00:05:17,039
<b>It's the tectonic force, more of the force of gravity as well.</b>

128
00:05:17,039 --> 00:05:20,360
<b>If since the 90s, at least the 90s,</b>

129
00:05:20,360 --> 00:05:25,679
<b>when these people published their work there in Houston,</b>

130
00:05:25,840 --> 00:05:29,239
<b>and we knew the ideal diameter</b>

131
00:05:29,239 --> 00:05:31,159
<b>would be around 50m, right?</b>

132
00:05:31,719 --> 00:05:33,119
<b>And that the distance should be</b>

133
00:05:33,119 --> 00:05:34,360
<b>at least seventy something</b>

134
00:05:34,360 --> 00:05:35,519
<b>meters away, right?</b>

135
00:05:35,840 --> 00:05:37,000
<b>If they had followed that...</b>

136
00:05:37,000 --> 00:05:40,519
<b>I'm talking about the mask consultant.</b>

137
00:05:40,519 --> 00:05:42,280
<b>If only they had already said there:</b>

138
00:05:42,280 --> 00:05:43,840
<b>No, it won't happen again.</b>

139
00:05:43,840 --> 00:05:49,239
<b>We will never make a diameter above 750m...</b>

140
00:05:49,239 --> 00:05:51,079
<b>SIf they had done that, but no...</b>

141
00:05:51,079 --> 00:05:52,639
<b>And there's also something else, isn't there?</b>

142
00:05:52,639 --> 00:05:55,039
<b>There was also the problem</b>

143
00:05:55,039 --> 00:05:57,039
<b>of geological faults, right?</b>

144
00:05:57,880 --> 00:05:58,920
<b>Like this one!</b>

145
00:05:58,920 --> 00:06:01,679
<b>So you can't do it here because of the fault.</b>

146
00:06:02,480 --> 00:06:03,760
<b>Could have avoided it, right?</b>

147
00:06:05,199 --> 00:06:06,280
<b>Because many of these mines</b>

148
00:06:06,280 --> 00:06:09,400
<b>were made now, in the 1980s.</b>

149
00:06:09,400 --> 00:06:11,480
<b>I mean, the 90s!</b>

150
00:06:12,719 --> 00:06:16,079
<b>Not even the 90s, but now!</b>

151
00:06:16,079 --> 00:06:20,679
<b>from 2005, 2006, 2008,</b>

152
00:06:20,679 --> 00:06:22,960
<b>recent mines were made. 2010, see?</b>

153
00:06:22,960 --> 00:06:25,239
<b>Unfortunately, the relationship between</b>

154
00:06:25,239 --> 00:06:29,599
<b>the Ministry of Mines and Energy,</b>

155
00:06:29,599 --> 00:06:31,000
<b>the federal government,</b>

156
00:06:31,719 --> 00:06:33,000
<b>and the mining companies,</b>

157
00:06:33,000 --> 00:06:35,039
<b>I'd say in the last 20 years</b>

158
00:06:35,039 --> 00:06:36,400
<b>has always been truly</b>

159
00:06:37,760 --> 00:06:38,360
<b>awful.</b>

160
00:06:38,360 --> 00:06:42,079
<b>To some extent, even irresponsible, isn't it?</b>

161
00:06:42,079 --> 00:06:43,599
<b>See Brumadinho,</b>

162
00:06:43,599 --> 00:06:46,159
<b>see Mariana, they won't let me lie.</b>

163
00:06:46,960 --> 00:06:48,960
<b>And now it's here in Maceió, right?</b>


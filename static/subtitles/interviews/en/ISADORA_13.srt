1
00:00:00,159 --> 00:00:01,639
<b>When they start talking<b>

2
00:00:01,639 --> 00:00:03,719
<b>about what the agreement proposes to maintain,</b>

3
00:00:03,719 --> 00:00:05,840
<b>they immediately speak of the urban order</b>

4
00:00:05,840 --> 00:00:09,880
<b>and so, I find this absolutely unrealistic.</b>

5
00:00:10,840 --> 00:00:13,239
<b>The question is not whether this will be maintained.</b>

6
00:00:13,239 --> 00:00:15,719
<b>The point is that this cannot be maintained.</b>

7
00:00:15,719 --> 00:00:18,000
<b>I say it, I've said it many times</b>

8
00:00:18,000 --> 00:00:19,559
<b>throughout these months.</b>

9
00:00:20,519 --> 00:00:24,440
<b>These neighborhoods, especially...</b>

10
00:00:25,199 --> 00:00:26,360
<b>Come on, what are the neighborhoods</b>

11
00:00:26,360 --> 00:00:27,559
<b>most affected so far?</b>

12
00:00:27,559 --> 00:00:31,239
<b>Mutange is entirely inside...</b>

13
00:00:32,239 --> 00:00:33,360
<b>Entirely!</b>

14
00:00:33,360 --> 00:00:35,000
<b>It's completely inside</b>

15
00:00:35,000 --> 00:00:36,079
<b>the removal perimeter.</b>

16
00:00:37,559 --> 00:00:38,760
<b>So Mutange,</b>

17
00:00:38,760 --> 00:00:41,760
<b>for example, is a neighborhood that will no longer be a neighborhood.</b>

18
00:00:41,760 --> 00:00:43,360
<b>That's a fact.</b>

19
00:00:43,880 --> 00:00:45,679
<b>So how are you going to keep</b>

20
00:00:45,679 --> 00:00:47,079
<b>urban order</b>

21
00:00:47,079 --> 00:00:49,039
<b>in a neighborhood that will no longer be a neighborhood?</b>

22
00:00:49,320 --> 00:00:50,360
<b>There is no way.</b>

23
00:00:50,719 --> 00:00:52,239
<b>It's impossible!</b>

24
00:00:52,239 --> 00:00:53,840
<b>Just impossible.</b>

25
00:00:55,800 --> 00:00:58,360
<b>We're not talking about the physical aspect,</b>

26
00:00:58,360 --> 00:01:00,320
<b>because everyone knows</b>

27
00:01:00,320 --> 00:01:04,079
<b>there will be demolitions, right?</b>

28
00:01:04,079 --> 00:01:06,360
<b>Demolitions will be carried out.</b>

29
00:01:06,360 --> 00:01:07,719
<b>Everybody knows that, right?</b>

30
00:01:07,719 --> 00:01:09,199
<b>Everybody knows, for example,</b>

31
00:01:09,199 --> 00:01:11,800
<b>that everything that's been evacuated</b>

32
00:01:11,800 --> 00:01:15,000
<b>from the slopes will not be reoccupied,</b>

33
00:01:15,000 --> 00:01:16,880
<b>and there will have to be an inspection</b>

34
00:01:16,880 --> 00:01:19,199
<b>and monitoring, so that it is not reoccupied.</b>

35
00:01:19,679 --> 00:01:20,960
<b>So far so good.</b>

36
00:01:20,960 --> 00:01:22,039
<b>Because actually,</b>

37
00:01:22,039 --> 00:01:23,440
<b>those slopes they were a risk</b>

38
00:01:23,440 --> 00:01:26,119
<b>even without any Braskem issue.</b>

39
00:01:26,119 --> 00:01:28,199
<b>They were a risk to people's lives</b>

40
00:01:28,199 --> 00:01:29,079
<b>who occupied there.</b>

41
00:01:29,079 --> 00:01:32,239
<b>The barriers falling had already happened...</b>

42
00:01:32,239 --> 00:01:33,639
<b>It was a precarious area.</b>

43
00:01:33,639 --> 00:01:38,840
<b>They have to be recomposed in their flora,</b>

44
00:01:38,840 --> 00:01:42,000
<b>they have to be recomposed as a vegetal area?</b>

45
00:01:42,000 --> 00:01:43,920
<b>Yes, they have to be. It's a fact, right?</b>

46
00:01:43,920 --> 00:01:45,920
<b>But we know, for example,</b>

47
00:01:45,920 --> 00:01:47,920
<b>that Braskem will not be able to demolish</b>

48
00:01:47,920 --> 00:01:50,440
<b>what is built heritage.</b>

49
00:01:50,559 --> 00:01:53,679
<b>Unless it really becomes</b>

50
00:01:53,679 --> 00:01:57,599
<b>a risk area, due to the fragility of the structure.</b>

51
00:01:57,599 --> 00:01:59,239
<b>So, if Zé Lopes, for example</b>

52
00:01:59,239 --> 00:02:02,599
<b>starts to have its soil sinking,</b>

53
00:02:02,599 --> 00:02:07,639
<b>then a rising of the waters</b>

54
00:02:07,639 --> 00:02:09,239
<b>over there by the lagoon</b>

55
00:02:09,239 --> 00:02:11,239
<b>that puts the structure at risk,</b>

56
00:02:11,239 --> 00:02:13,320
<b>we hope it doesn't happen</b>

57
00:02:13,320 --> 00:02:15,199
<b>due to the patrimonial importance of that building,</b>

58
00:02:15,199 --> 00:02:18,519
<b>but if it becomes fragile</b>

59
00:02:18,519 --> 00:02:21,519
<b>to the point of the structure becoming risky</b>

60
00:02:21,800 --> 00:02:24,960
<b>and it reaches a point</b>

61
00:02:25,000 --> 00:02:27,199
<b>of collapsing,</b>

62
00:02:27,199 --> 00:02:28,840
<b>different measures will be taken</b>

63
00:02:28,840 --> 00:02:30,679
<b>and we don't want to</b>

64
00:02:30,679 --> 00:02:31,800
<b>let that thing be demolished.</b>

65
00:02:31,800 --> 00:02:34,079
<b>There's a solution in cultural heritage,</b>

66
00:02:34,079 --> 00:02:35,079
<b>in built heritage,</b>

67
00:02:35,079 --> 00:02:36,719
<b>called transfer.</b>

68
00:02:36,719 --> 00:02:39,639
<b>Transfer means removing a property</b>

69
00:02:39,639 --> 00:02:42,199
<b>from one place and take it to another.</b>

70
00:02:42,199 --> 00:02:46,199
<b>You take it apart like a Lego game.</b>

71
00:02:46,199 --> 00:02:48,480
<b>You cut it,</b>

72
00:02:48,480 --> 00:02:52,760
<b>mark the pieces</b>

73
00:02:52,760 --> 00:02:54,360
<b>and then reassemble.</b>

74
00:02:54,360 --> 00:02:55,599
<b>This has been done in the world</b>

75
00:02:55,599 --> 00:02:58,480
<b>with real estate of great importance.</b>

76
00:02:58,480 --> 00:03:00,760
<b>It is a possible solution</b>

77
00:03:00,760 --> 00:03:01,840
<b>never adopted in Brazil,</b>

78
00:03:01,840 --> 00:03:03,000
<b>this would be the first case.</b>

79
00:03:03,000 --> 00:03:05,360
<b>We don't want it to come to that, right?</b>

80
00:03:05,360 --> 00:03:08,599
<b>But if there's a risk of that structure collapsing</b>

81
00:03:08,599 --> 00:03:10,800
<b>we would like for it to happen</b>

82
00:03:10,800 --> 00:03:13,719
<b>and we know that the agreement was that</b>

83
00:03:13,719 --> 00:03:15,719
<b>whatever is built cultural heritage,</b>

84
00:03:15,719 --> 00:03:17,440
<b>if there's no risk to the structure,</b>

85
00:03:17,440 --> 00:03:19,000
<b>the company cannot demolish.</b>

86
00:03:19,639 --> 00:03:20,559
<b>You know that.</b>

87
00:03:20,559 --> 00:03:22,159
<b>Agreements have already been signed in this regard.</b>

88
00:03:22,159 --> 00:03:25,119
<b>But a neighborhood is not made</b>

89
00:03:25,960 --> 00:03:28,039
<b>of the properties it owns.</b>

90
00:03:28,039 --> 00:03:32,679
<b>A neighborhood is not made of physical houses.</b>

91
00:03:33,639 --> 00:03:35,280
<b>It is made of people</b>

92
00:03:35,280 --> 00:03:39,400
<b>occupying the properties and living there.</b>

93
00:03:40,039 --> 00:03:42,000
<b>It is made of circulation,</b>

94
00:03:42,000 --> 00:03:43,079
<b>it is made of it all.</b>

95
00:03:43,079 --> 00:03:44,159
<b>So if that area turns into</b>

96
00:03:44,159 --> 00:03:47,760
<b>an area with no people living there</b>

97
00:03:47,760 --> 00:03:51,360
<b>because it cannot become a passing area</b>

98
00:03:51,360 --> 00:03:52,599
<b>where you go...</b>

99
00:03:52,599 --> 00:03:55,079
<b>Like let's say the area turns into</b>

100
00:03:55,079 --> 00:03:58,480
<b>an enlarged version of the municipal park</b>

101
00:03:58,480 --> 00:04:00,360
<b>containing</b>

102
00:04:01,039 --> 00:04:02,719
<b>historic properties for you to visit,</b>

103
00:04:02,719 --> 00:04:04,960
<b>containing cultural attractions,</b>

104
00:04:04,960 --> 00:04:08,559
<b>containing a football field.</b>

105
00:04:10,320 --> 00:04:12,519
<b>Let's say it becomes that.</b>

106
00:04:13,800 --> 00:04:15,880
<b>The urban order has already been broken!</b>

107
00:04:17,039 --> 00:04:19,159
<b>So, like: are you going to keep it urbanistic?</b>

108
00:04:19,159 --> 00:04:21,400
<b>No, this is an illusion.</b>

109
00:04:22,079 --> 00:04:23,960
<b>It'll not maintain the urban order,</b>

110
00:04:23,960 --> 00:04:24,880
<b>that's impossible!</b>

111
00:04:24,880 --> 00:04:26,679
<b>People don't live there anymore.</b>

112
00:04:27,000 --> 00:04:28,320
<b>CSA</b>

113
00:04:29,199 --> 00:04:30,480
<b>has been in that area</b>

114
00:04:31,119 --> 00:04:32,519
<b>for decades.</b>

115
00:04:34,000 --> 00:04:36,519
<b>It is seen in Maceió...</b>

116
00:04:36,519 --> 00:04:39,440
<b>There's a rivalry between CSA and CRB</b>

117
00:04:40,119 --> 00:04:41,719
<b>and it is seen in Maceió,</b>

118
00:04:41,719 --> 00:04:44,639
<b>roughly speaking,</b>

119
00:04:44,639 --> 00:04:47,800
<b>roughly speaking, the CRB is viewed</b>

120
00:04:47,800 --> 00:04:49,760
<b>as the beach team</b>

121
00:04:49,760 --> 00:04:51,760
<b>and CSA as the lagoon team.</b>

122
00:04:51,760 --> 00:04:53,239
<b>Roughly speaking, because we know</b>

123
00:04:53,239 --> 00:04:54,840
<b>fans are everywhere.</b>

124
00:04:54,840 --> 00:04:58,559
<b>But roughly speaking, CSA is the lagoon team</b>

125
00:04:58,559 --> 00:05:03,159
<b>and the relationship of the people of the lagoon region</b>

126
00:05:03,400 --> 00:05:05,440
<b>with CSA, it's impressive.</b>

127
00:05:05,440 --> 00:05:06,679
<b>And I say this to you</b>

128
00:05:06,679 --> 00:05:09,360
<b>like someone walking around,</b>

129
00:05:09,360 --> 00:05:10,599
<b>living there,</b>

130
00:05:10,599 --> 00:05:14,719
<b>and that has walked around Bom Parto on a CSA game day.</b>

131
00:05:16,119 --> 00:05:16,679
<b>Got it?</b>

132
00:05:16,679 --> 00:05:19,559
<b>I walked through the streets where the Quintal Cultural is located</b>

133
00:05:19,559 --> 00:05:21,199
<b>when there was a CSA game.</b>

134
00:05:21,199 --> 00:05:24,400
<b>And I saw the relationship people had with that team.</b>

135
00:05:25,199 --> 00:05:29,480
<b>I saw the list of people who live in the neighborhood</b>

136
00:05:29,480 --> 00:05:31,519
<b>with that team, right?</b>

137
00:05:31,519 --> 00:05:34,000
<b>I saw the relationship of people</b>

138
00:05:34,000 --> 00:05:35,360
<b>in terms of cheering,</b>

139
00:05:35,360 --> 00:05:38,320
<b>in terms of being part of their lives.</b>

140
00:05:39,320 --> 00:05:43,000
<b>I don't even like football,</b>

141
00:05:43,000 --> 00:05:46,840
<b>I'm not even a fan, I'm neither CSA nor CRB,</b>

142
00:05:46,840 --> 00:05:50,159
<b>nor am I a fan of any other football team in Brazil.</b>

143
00:05:50,159 --> 00:05:52,800
<b>I have even missed the Brazilian national team game</b>

144
00:05:52,800 --> 00:05:55,400
<b>in the World Cup finals.</b>

145
00:05:55,400 --> 00:05:57,079
<b>Just to give you an idea.</b>

146
00:05:57,079 --> 00:06:00,199
<b>And I am able to evaluate</b>

147
00:06:00,199 --> 00:06:02,960
<b>the gigantic loss to the people</b>

148
00:06:02,960 --> 00:06:04,840
<b>after CSA got out of there.</b>

149
00:06:06,360 --> 00:06:09,239
<b>The fact that CSA left that place, you see?</b>

150
00:06:09,239 --> 00:06:11,360
<b>So we're talking about something</b>

151
00:06:11,360 --> 00:06:14,079
<b>which may not look like heritage to people,</b>

152
00:06:14,079 --> 00:06:16,880
<b>but it is heritage, it's the heritage of that place.</b>

153
00:06:18,039 --> 00:06:19,440
<b>For the people who live there,</b>

154
00:06:19,440 --> 00:06:20,239
<b>it's the heritage of that place.</b>

155
00:06:20,239 --> 00:06:21,880
<b>For the people of the lagoon region,</b>

156
00:06:21,880 --> 00:06:23,760
<b>it's their lives heritage.</b>

157
00:06:24,400 --> 00:06:29,440
<b>And CSA left and won't go back there.</b>

158
00:06:30,119 --> 00:06:33,440
<b>In terms of well-being, quality of life,</b>

159
00:06:34,039 --> 00:06:35,840
<b>it makes a difference for them.</b>

160
00:06:35,840 --> 00:06:38,360
<b>It makes their lives worse,</b>

161
00:06:38,360 --> 00:06:40,559
<b>it that makes their lives less pleasant</b>

162
00:06:40,559 --> 00:06:42,960
<b>and sadder, in that sense.</b>

163
00:06:42,960 --> 00:06:47,360
<b>I'm only talking about the departure of the CSAs here.</b>

164
00:06:50,119 --> 00:06:52,599
<b>I could talk about countless other things.</b>

165
00:06:52,599 --> 00:06:54,679
<b>I could talk about countless...</b>

166
00:06:54,679 --> 00:06:57,280
<b>I could talk about the Lucena Maranhão Square,</b>

167
00:06:58,800 --> 00:07:02,360
<b>which was the scene of many street parties</b>

168
00:07:02,360 --> 00:07:03,599
<b>for that population over there,</b>

169
00:07:03,599 --> 00:07:06,039
<b>for other people from Maceio?</b>

170
00:07:06,039 --> 00:07:09,880
<b>I could talk about each church...</b>

171
00:07:09,880 --> 00:07:12,519
<b>Like the church in Lucena Maranhão, right?</b>

172
00:07:12,519 --> 00:07:14,880
<b>The Santo Antônio Church, which was closed.</b>

173
00:07:14,880 --> 00:07:16,800
<b>I could talk about Bom Conselho College</b>

174
00:07:16,800 --> 00:07:18,760
<b>where several classes studied.</b>

175
00:07:18,760 --> 00:07:21,840
<b>I could talk about Quintal Cultural.</b>

176
00:07:23,559 --> 00:07:26,599
<b>So, the loss of life...</b>

177
00:07:26,599 --> 00:07:29,559
<b>The loss of life is a staggering thing.</b>

178
00:07:29,559 --> 00:07:34,239
<b>It is something that will stay in the city for generations.</b>

179
00:07:34,239 --> 00:07:36,039
<b>It is something that, for sure,</b>

180
00:07:36,039 --> 00:07:39,519
<b>makes many people's lives sadder,</b>

181
00:07:40,519 --> 00:07:43,159
<b>of those places especially,</b>

182
00:07:43,159 --> 00:07:44,559
<b>and the city as a whole.</b>

183
00:07:44,559 --> 00:07:46,719
<b>It's not a matter of saying like:</b>

184
00:07:46,719 --> 00:07:49,280
<b>let's do a heritage project to rescue it.</b>

185
00:07:49,280 --> 00:07:50,760
<b>We are not going to rescue anything.</b>

186
00:07:51,559 --> 00:07:54,400
<b>Any heritage project in this place</b>

187
00:07:54,400 --> 00:07:56,960
<b>has to be a project that proposes</b>

188
00:07:56,960 --> 00:08:01,159
<b>working with the pain of people's loss,</b>

189
00:08:01,159 --> 00:08:03,039
<b>with the actual pain.</b>

190
00:08:03,039 --> 00:08:05,679
<b>So any heritage education project,</b>

191
00:08:05,679 --> 00:08:06,719
<b>preservation ones,</b>

192
00:08:06,719 --> 00:08:09,840
<b>it has to be a project that considers</b>

193
00:08:09,840 --> 00:08:12,280
<b>the inescapable fact</b>

194
00:08:12,280 --> 00:08:16,439
<b>that we need to reframe</b>

195
00:08:16,439 --> 00:08:17,840
<b>the loss of people.</b>

196
00:08:17,840 --> 00:08:20,279
<b>Reframe memories,</b>

197
00:08:20,279 --> 00:08:22,800
<b>which was done in the Rio Doce valley.</b>

198
00:08:22,800 --> 00:08:26,079
<b>People will have to resignify</b>

199
00:08:26,079 --> 00:08:28,119
<b>their relationship with that place.</b>

200
00:08:28,960 --> 00:08:30,520
<b>A point can be reached</b>

201
00:08:30,520 --> 00:08:32,119
<b>in which the relationship returns to being</b>

202
00:08:32,119 --> 00:08:34,119
<b>a happy relationship.</b>

203
00:08:34,119 --> 00:08:36,600
<b>We can! I fully believe that.</b>

204
00:08:36,600 --> 00:08:39,520
<b>Depending on how you proceed</b>

205
00:08:39,520 --> 00:08:43,920
<b>with what will be done with this place.</b>

206
00:08:43,920 --> 00:08:46,560
<b>And it's not, I'm very clear about that...</b>

207
00:08:46,560 --> 00:08:49,680
<b>It's not making a museum.</b>

208
00:08:50,960 --> 00:08:55,560
<b>A museum with people's memories.</b>

209
00:08:55,560 --> 00:08:56,159
<b>That's not it.</b>

210
00:08:56,159 --> 00:08:57,880
<b>Not that you shouldn't work</b>

211
00:08:57,880 --> 00:08:59,760
<b>with the memories,</b>

212
00:08:59,760 --> 00:09:01,079
<b>not that you shouldn't work</b>

213
00:09:01,079 --> 00:09:04,800
<b>with the museological part,</b>

214
00:09:04,800 --> 00:09:06,479
<b>that's not it! What I mean is that</b>

215
00:09:06,479 --> 00:09:08,840
<b>it's not making a museum out of it</b>

216
00:09:08,840 --> 00:09:12,760
<b>that will reframe these memories</b>

217
00:09:12,760 --> 00:09:13,960
<b>for these people.</b>

218
00:09:13,960 --> 00:09:15,520
<b>But rather showing sensitivity,</b>

219
00:09:15,520 --> 00:09:17,039
<b>showing respect for these memories,</b>

220
00:09:17,039 --> 00:09:21,279
<b>involving people</b>

221
00:09:21,359 --> 00:09:23,960
<b>in a project for that place.</b>

222
00:09:23,960 --> 00:09:28,760
<b>Every time people talk of a project for this place</b>

223
00:09:28,760 --> 00:09:31,439
<b>they never speak</b>

224
00:09:31,439 --> 00:09:35,720
<b>from a previously held meeting,</b>

225
00:09:35,720 --> 00:09:37,960
<b>with people thinking about</b>

226
00:09:37,960 --> 00:09:40,119
<b>a project for this place.</b>

227
00:09:40,119 --> 00:09:42,680
<b>It's always a project that is coming</b>

228
00:09:42,680 --> 00:09:44,760
<b>from someone to...</b>

229
00:09:44,760 --> 00:09:48,760
<b>It's never a project made from</b>

230
00:09:49,399 --> 00:09:52,840
<b>contact with residents,</b>

231
00:09:52,840 --> 00:09:56,760
<b>with people, with visitors,</b>

232
00:09:56,760 --> 00:09:59,159
<b>with the city involved.</b>

233
00:09:59,159 --> 00:10:02,880
<b>It's always someone from within</b>

234
00:10:02,880 --> 00:10:04,640
<b>a government agency,</b>

235
00:10:04,640 --> 00:10:08,199
<b>it's always someone inside a university</b>

236
00:10:08,199 --> 00:10:09,199
<b>in a research group.</b>

237
00:10:09,199 --> 00:10:11,199
<b>It's never something like:</b>

238
00:10:11,199 --> 00:10:13,479
<b>we are doing this from</b>

239
00:10:13,479 --> 00:10:15,800
<b>a close contact with people.</b>

240
00:10:15,800 --> 00:10:17,359
<b>They say</b>

241
00:10:17,359 --> 00:10:19,560
<b>what they want done,</b>

242
00:10:19,600 --> 00:10:21,439
<b>since they won't be able to live in that place anymore.</b>

243
00:10:22,039 --> 00:10:25,479
<b>If by chance they no longer live in these neighborhoods,</b>

244
00:10:25,479 --> 00:10:29,359
<b>what do they believe should be made of it?</b>

245
00:10:29,359 --> 00:10:31,600
<b>What should be done in these places?</b>

246
00:10:31,600 --> 00:10:34,159
<b>It's never like that. It's never like this:</b>

247
00:10:34,159 --> 00:10:36,239
<b>let's do a project workshop</b>

248
00:10:36,239 --> 00:10:38,319
<b>with people</b>

249
00:10:38,319 --> 00:10:42,800
<b>to see what wishes will come out of there,</b>

250
00:10:42,800 --> 00:10:44,920
<b>what wishes are there for this place?</b>

251
00:10:44,920 --> 00:10:45,840
<b>It's never like that.</b>


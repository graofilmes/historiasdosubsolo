1
00:00:00,119 --> 00:00:03,159
<b>When I couldn't take it anymore?</b>

2
00:00:05,360 --> 00:00:09,440
After a planning secretary,

3
00:00:10,199 --> 00:00:13,440
not Professor José de Melo Gomes,

4
00:00:14,119 --> 00:00:16,719
he would never do that.

5
00:00:16,719 --> 00:00:19,119
After a planning secretary

6
00:00:19,119 --> 00:00:20,559
showed me a revolver.

7
00:00:23,000 --> 00:00:24,559
<b>He called me into the office</b>

8
00:00:25,400 --> 00:00:26,920
<b>for a chat.</b>

9
00:00:28,760 --> 00:00:33,559
We talked like friends, we didn't even bring it up.

10
00:00:33,559 --> 00:00:37,199
 And then he casually

11
00:00:37,199 --> 00:00:41,320
opens his briefcase as if looking for papers.

12
00:00:41,320 --> 00:00:45,119
And when he opened it, I saw that a revolver in there.

13
00:00:46,400 --> 00:00:47,960
<b>So my time was up.</b>

14
00:00:48,840 --> 00:00:52,519
But before reaching this point,

15
00:00:53,239 --> 00:00:55,199
what were the clashes like

16
00:00:55,199 --> 00:00:58,639
regarding the Salgema licensing?

17
00:00:58,679 --> 00:00:59,880
<b>Yeah, yeah.</b>

18
00:00:59,880 --> 00:01:02,920
<b>Was there any denial?</b>

19
00:01:03,159 --> 00:01:05,400
When we started issuing opinions

20
00:01:05,400 --> 00:01:10,559
and refusing to give opinions that supported

21
00:01:11,360 --> 00:01:14,800
any authorization for the implantation of Salgema.

22
00:01:15,519 --> 00:01:19,599
It was still in deployment, without authorization.

23
00:01:22,360 --> 00:01:25,239
They managed to deploy, really implant.

24
00:01:26,400 --> 00:01:28,199
And we saw that

25
00:01:28,199 --> 00:01:30,000
the correlation of forces

26
00:01:30,000 --> 00:01:32,159
really is tremendously different.


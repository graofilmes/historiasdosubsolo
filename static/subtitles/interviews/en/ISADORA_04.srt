1
00:00:00,000 --> 00:00:01,880
<b>We have the state of Alagoas,</b>

2
00:00:01,880 --> 00:00:03,679
<b>called "Alagoas"</b>

3
00:00:03,679 --> 00:00:05,880
<b>precisely because of the lagoons.</b>

4
00:00:06,360 --> 00:00:07,840
<b>Alagoas is an old term,</b>

5
00:00:07,840 --> 00:00:10,119
<b>an ancient geographical term</b>

6
00:00:10,119 --> 00:00:11,639
<b>meaning lagoons.</b>

7
00:00:11,639 --> 00:00:13,880
<b>So the place of lagoons,</b>

8
00:00:13,880 --> 00:00:15,599
<b>which is how we're known,</b>

9
00:00:15,599 --> 00:00:19,800
<b>historically, a portion to the south of the captaincy of Pernambuco</b>

10
00:00:19,800 --> 00:00:23,960
<b>distinguished by the abundance of water</b>

11
00:00:23,960 --> 00:00:25,679
<b>and exuberant forests.</b>

12
00:00:25,840 --> 00:00:28,159
<b>So the place of "alagoas" means</b>

13
00:00:28,159 --> 00:00:30,119
<b>the place of lagoons.</b>

14
00:00:31,360 --> 00:00:34,639
<b>The state is known for its profusion of lagoons,</b>

15
00:00:34,639 --> 00:00:37,639
<b>but it has three more significant ones,</b>

16
00:00:37,639 --> 00:00:38,480
<b>and of these three,</b>

17
00:00:38,480 --> 00:00:42,440
<b>two make up the Mundaú-Manguaba lagoon estuary complex,</b>

18
00:00:42,440 --> 00:00:43,519
<b>the CELMM.</b>

19
00:00:43,639 --> 00:00:45,960
<b>So they're two sister lagoons</b>

20
00:00:45,960 --> 00:00:48,760
<b>that communicate</b>

21
00:00:48,760 --> 00:00:52,320
<b>with several channels and several lake islands,</b>

22
00:00:52,320 --> 00:00:54,079
<b>and that have an opening</b>

23
00:00:54,079 --> 00:00:55,679
<b>that connects to the sea.</b>

24
00:00:55,679 --> 00:00:57,880
<b>They're lagunas,</b>

25
00:00:57,880 --> 00:01:00,400
<b>precisely because they communicate with the sea,</b>

26
00:01:00,400 --> 00:01:03,119
<b>but the older term, Alagoas,</b>

27
00:01:03,119 --> 00:01:05,119
<b>is the one that stuck.</b>

28
00:01:05,119 --> 00:01:07,559
<b>So we call them lagoons, a layman's term</b>

29
00:01:07,559 --> 00:01:09,599
<b>for this body of water.</b>

30
00:01:10,639 --> 00:01:13,800
<b>This lagoon complex bathes seven municipalities,</b>

31
00:01:13,800 --> 00:01:15,480
<b>among them, Maceió.</b>

32
00:01:15,480 --> 00:01:17,719
<b>So Maceió is a city</b>

33
00:01:17,719 --> 00:01:19,719
<b>with two shores:</b>

34
00:01:19,719 --> 00:01:20,760
<b>a sea shore,</b>

35
00:01:20,760 --> 00:01:22,800
<b>which is the best known to those</b>

36
00:01:22,800 --> 00:01:23,960
<b>who visit,</b>

37
00:01:23,960 --> 00:01:25,280
<b>and a lagoon shore.</b>

38
00:01:25,599 --> 00:01:27,199
<b>In terms of size,</b>

39
00:01:27,199 --> 00:01:27,920
<b>of perimeter,</b>

40
00:01:27,920 --> 00:01:29,679
<b>they are equivalent.</b>

41
00:01:29,679 --> 00:01:31,400
<b>Their mileage...</b>

42
00:01:31,400 --> 00:01:33,599
<b>Actually, they meet at Barra</b>

43
00:01:33,599 --> 00:01:36,159
<b>precisely because they communicate with the sea.</b>

44
00:01:36,159 --> 00:01:37,360
<b>There comes a point</b>

45
00:01:37,360 --> 00:01:39,039
<b>when those who have come to Maceió,</b>

46
00:01:39,039 --> 00:01:42,320
<b>if they visited what we call Pontal da Barra,</b>

47
00:01:42,320 --> 00:01:44,159
<b>which is a tip,</b>

48
00:01:44,159 --> 00:01:45,000
<b>geografically speaking,</b>

49
00:01:45,000 --> 00:01:47,559
<b>a tip where Maceió tapers off,</b>

50
00:01:47,559 --> 00:01:48,840
<b>tapers off,/b>

51
00:01:49,800 --> 00:01:52,000
<b>and at the end of that tip</b>

52
00:01:52,000 --> 00:01:54,559
<b>is where the lagoon meets the sea</b>

53
00:01:54,559 --> 00:01:55,960
<b>and forms the lagoon bar.</b>

54
00:01:56,760 --> 00:01:59,400
<b>So these two shores taper off</b>

55
00:01:59,400 --> 00:02:00,880
<b>until they become a bar.</b>

56
00:02:02,519 --> 00:02:05,800
<b>There are 24 km</b>

57
00:02:05,800 --> 00:02:07,119
<b>of lake shore in Maceió,</b>

58
00:02:07,119 --> 00:02:09,039
<b>starting with this neighborhood, Pontal da Barra,</b>

59
00:02:09,039 --> 00:02:10,320
<b>where we have the lagoon bar,</b>

60
00:02:10,320 --> 00:02:13,039
<b>up to a neighborhood called Rio Novo,</b>

61
00:02:13,039 --> 00:02:15,440
<b>which is the limit of Maceió with another municipality</b>

62
00:02:15,440 --> 00:02:16,400
<b>called Satuba,</b>

63
00:02:16,400 --> 00:02:18,800
<b>one of the municipalities bathed by the Mundaú lagoon.</b>

64
00:02:19,480 --> 00:02:22,480
<b>So this lagoon, Mundaú,</b>

65
00:02:22,480 --> 00:02:23,599
<b>bathes Maceió.</b>

66
00:02:23,599 --> 00:02:27,239
<b>If I'm not mistaken, there are ten neighborhoods</b>

67
00:02:27,239 --> 00:02:29,320
<b>from Pontal to Rio Novo,</b>

68
00:02:29,559 --> 00:02:30,880
<b>and of these,</b>

69
00:02:30,880 --> 00:02:32,840
<b>some make up</b>

70
00:02:32,840 --> 00:02:34,920
<b>the neighborhoods affected by the mining.</b>


1
00:00:00,920 --> 00:00:03,320
Look, during the activism,

2
00:00:03,320 --> 00:00:08,159
not as much as during

3
00:00:08,159 --> 00:00:10,119
my mandate,

4
00:00:12,400 --> 00:00:14,920
but there was always something in the air

5
00:00:14,920 --> 00:00:16,519
during the activism...

6
00:00:16,519 --> 00:00:18,719
And I started to receive phone calls

7
00:00:19,519 --> 00:00:21,280
<b>with death threats!</b>

8
00:00:22,719 --> 00:00:25,599
Phone calls that were simply a question:

9
00:00:25,599 --> 00:00:27,679
do you want to be like Tiradentes?

10
00:00:28,599 --> 00:00:29,960
<b>They just didn't try</b>

11
00:00:30,639 --> 00:00:32,480
because it was impossible

12
00:00:32,480 --> 00:00:34,760
to demoralize me for corruption.

13
00:00:36,119 --> 00:00:37,599
There is unanimity

14
00:00:37,599 --> 00:00:41,800
on this to date.

15
00:00:43,960 --> 00:00:46,159
I never took

16
00:00:46,159 --> 00:00:50,559
a penny of bribes

17
00:00:50,559 --> 00:00:52,559
or anything else

18
00:00:52,559 --> 00:00:54,800
while I was in office.

19
00:00:58,079 --> 00:00:59,920
<b>I got proposals,</b>

20
00:01:00,920 --> 00:01:01,880
<b>never took them.</b>

21
00:01:03,000 --> 00:01:05,119
So I deduce that

22
00:01:05,119 --> 00:01:08,280
proposals exist, existed,

23
00:01:08,280 --> 00:01:11,440
and that people accept, accepted.

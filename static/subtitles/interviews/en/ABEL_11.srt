1
00:00:00,039 --> 00:00:01,599
<b>Do you believe that, in fact,</b>

2
00:00:01,599 --> 00:00:04,559
<b>rock salt extraction has stopped in here?</b>

3
00:00:04,559 --> 00:00:07,360
<b>I do. I do because</b>

4
00:00:07,559 --> 00:00:10,599
<b>they are heavily supervised.</b>

5
00:00:10,599 --> 00:00:12,519
<b>So they really stopped in 2019,</b>

6
00:00:12,519 --> 00:00:14,320
<b>because this is serious, guys.</b>

7
00:00:15,360 --> 00:00:16,800
<b>If they kept going,</b>

8
00:00:17,440 --> 00:00:18,920
<b>they would all be behind bars.</b>

9
00:00:18,920 --> 00:00:20,400
<b>They would all be arrested.</b>

10
00:00:21,360 --> 00:00:24,639
<b>From the president to the field engineer,</b>

11
00:00:24,639 --> 00:00:26,000
<b>all arrested.</b>

12
00:00:27,039 --> 00:00:29,119
<b>It was very irresponsible of them.</b>

13
00:00:29,119 --> 00:00:31,400
<b>They committed a very serious crime.</b>

14
00:00:31,400 --> 00:00:32,840
<b>I'm not sure you are aware</b>

15
00:00:32,840 --> 00:00:35,239
<b>that this problem right here,</b>

16
00:00:36,119 --> 00:00:38,519
<b>this disastrous mining,</b>

17
00:00:39,280 --> 00:00:43,840
<b>is the biggest environmental crime in urban Brazil.</b>

18
00:00:45,159 --> 00:00:48,800
<b>With nearly 50,000 people affected.</b>


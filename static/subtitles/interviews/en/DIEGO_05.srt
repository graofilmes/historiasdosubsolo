1
00:00:00,291 --> 00:00:01,584
Is it a capital?

2
00:00:01,584 --> 00:00:03,962
Of course, but it is a peripheral region.

3
00:00:05,171 --> 00:00:06,047
When we...

4
00:00:06,047 --> 00:00:09,634
And then unfortunately it's standard,

5
00:00:10,301 --> 00:00:13,555
in all areas that
are impacted by mining,

6
00:00:15,181 --> 00:00:20,061
you have a very strong initial outcry,

7
00:00:20,770 --> 00:00:23,356
especially
when it involves biotic impacts.

8
00:00:23,898 --> 00:00:26,943
Look what a sad thing to say,
but that's the truth.

9
00:00:28,611 --> 00:00:30,739
When it involves biotic impacts,

10
00:00:30,989 --> 00:00:33,366
that is, on the fauna, on the flora,

11
00:00:34,075 --> 00:00:37,704
you have a very strong commotion.

12
00:00:38,246 --> 00:00:39,581
I saw this a lot in Peru.

13
00:00:39,581 --> 00:00:41,374
I saw this a lot in Colombia.

14
00:00:41,374 --> 00:00:43,209
In Mexico.

15
00:00:43,209 --> 00:00:45,170
Here in Brazil as well.

16
00:00:45,295 --> 00:00:48,214
We are in a peripheral neighbourhood and state,

17
00:00:50,717 --> 00:00:52,761
Bebedouro isn't Ponta Verde.

18
00:00:54,179 --> 00:00:55,513
This isn't Ponta Verde.

19
00:00:56,556 --> 00:00:59,726
The level of commotion would be
completely different.

20
00:01:00,477 --> 00:01:02,645
Bom Parto isn't Jatiúca.

21
00:01:03,354 --> 00:01:05,732
It's a different level of commotion.

22
00:01:05,732 --> 00:01:08,860
In these areas,
it's almost as if inequality

23
00:01:10,195 --> 00:01:13,198
mattered when you

24
00:01:14,240 --> 00:01:16,034
emotionally value the impact,

25
00:01:17,160 --> 00:01:19,704
and then what we have to...

26
00:01:19,871 --> 00:01:22,582
And which can be used in favor of

27
00:01:22,916 --> 00:01:25,335
those impacted,

28
00:01:26,127 --> 00:01:30,757
is that where
population density is large,

29
00:01:31,800 --> 00:01:34,052
political pressure can be continuous.

30
00:01:35,762 --> 00:01:37,347
It can be continuous.

31
00:01:37,347 --> 00:01:38,306
It can be continuous,
but the problem is that

32
00:01:38,348 --> 00:01:40,725
with national and international visibility,

33
00:01:40,892 --> 00:01:42,602
at a time
such as a pandemic,

34
00:01:42,602 --> 00:01:45,480
which was the worst possible scenario to happen.

35
00:01:46,856 --> 00:01:51,069
In this case, news can be divisive.

36
00:01:51,319 --> 00:01:53,154
It doesn't aggregate.

37
00:01:53,446 --> 00:01:56,074
So when we have a case like this,

38
00:01:56,074 --> 00:01:58,118
people heard about it and so on,

39
00:01:58,118 --> 00:01:59,953
friends from Cuiabá asked,

40
00:01:59,953 --> 00:02:02,080
"Diego, what's going on in Maceió?" etc.

41
00:02:02,580 --> 00:02:04,999
That was in the past,
they never asked again,

42
00:02:07,127 --> 00:02:08,503
because it fades away,

43
00:02:08,503 --> 00:02:11,798
only those impacted never forget it.

44
00:02:11,798 --> 00:02:16,553
But then: "well, Diego,
so just forget it?" On the contrary!

45
00:02:17,929 --> 00:02:18,805
What's different,

46
00:02:18,805 --> 00:02:22,642
from other municipalities
that were impacted,

47
00:02:22,642 --> 00:02:26,354
but smaller,
is that they don't have demographic strength.

48
00:02:27,272 --> 00:02:29,482
This is very important
to apply pressure.

49
00:02:30,650 --> 00:02:33,486
So it never leaves the news.

50
00:02:33,486 --> 00:02:36,030
Now, from a national and international
point of view,

51
00:02:40,076 --> 00:02:41,161
it's not a priority

52
00:02:41,369 --> 00:02:44,247
for the agenda. For the national agenda.

53
00:02:45,331 --> 00:02:46,374
How would it fit in the agenda?

54
00:02:46,374 --> 00:02:49,335
Only if the sinking had happened

55
00:02:49,669 --> 00:02:52,172
with many buildings, people screaming.

56
00:02:52,881 --> 00:02:55,216
All in film. Wow, it would stay current
to this day.

57
00:02:55,758 --> 00:02:59,220
But that's the most perverse
aspect of the situation,

58
00:02:59,596 --> 00:03:02,599
the impact is diluted.

59
00:03:03,474 --> 00:03:07,812
That's why we see
people developing

60
00:03:07,812 --> 00:03:12,233
for example, anxiety disorders,
depression,

61
00:03:12,233 --> 00:03:15,028
they will probably develop

62
00:03:15,028 --> 00:03:16,237
diabetes.

63
00:03:16,237 --> 00:03:20,700
They'll develop hipertension,
even if they never had hipertension or any of that,

64
00:03:21,409 --> 00:03:25,538
they'll had it.
So for those impacted,

65
00:03:25,997 --> 00:03:27,248
what's more perverse?

66
00:03:27,248 --> 00:03:30,919
And considering it took place
during a pandemic.

67
00:03:31,586 --> 00:03:33,963
That is, things go downhill from there.

68
00:03:35,006 --> 00:03:37,425
That's why I consider

69
00:03:37,425 --> 00:03:40,762
local mobilization to be the best option

70
00:03:41,554 --> 00:03:46,392
here in the city, in the state
and, with luck, it'll go national,

71
00:03:46,392 --> 00:03:49,729
such as with a documentary
that gives greater visibility.


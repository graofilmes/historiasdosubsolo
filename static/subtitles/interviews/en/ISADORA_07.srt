1
00:00:00,000 --> 00:00:04,400
<b>Initially, there was no idea</b>

two
00:00:05,679 --> 00:00:08,079
<b>of what was going on</b>

3
00:00:08,079 --> 00:00:08,880
<b>with these places,</b>

4
00:00:10,000 --> 00:00:11,960
<b>so everyone was taken by surprise,</b>

5
00:00:11,960 --> 00:00:12,960
<b>let's put it this way,</b>

6
00:00:13,800 --> 00:00:15,719
<b>when it started to spread</b>

7
00:00:15,719 --> 00:00:16,519
<b>to other places.</b>

8
00:00:16,519 --> 00:00:18,880
<b>Originally it was Pinheiro,</b>

9
00:00:18,880 --> 00:00:20,840
<b>and everyone started turning around</b>

10
00:00:20,840 --> 00:00:22,719
<b>everybody started to turn around, so to speak.</b>

11
00:00:22,719 --> 00:00:25,159
<b>People started talking,</b>

12
00:00:25,159 --> 00:00:26,239
<b>and when addressing it,</b>

13
00:00:26,239 --> 00:00:27,960
<b>Pinheiro was all they talked about.</b>

14
00:00:28,679 --> 00:00:30,719
<b>Then a lot of knowledge was produced</b>

15
00:00:30,719 --> 00:00:34,079
<b>about what was happening in Pinheiro.</b>

16
00:00:34,079 --> 00:00:36,880
<b>When it started to appear in the lagoon,</b>

17
00:00:36,880 --> 00:00:38,679
<b>there was no knowledge at all.</b>

18
00:00:38,679 --> 00:00:40,199
<b>So much so, that a lot of time has passed</b>

19
00:00:40,199 --> 00:00:42,480
<b>while getting out of the Pinheiro case</b>

20
00:00:42,480 --> 00:00:44,360
<b>for the Braskem case.</b>

21
00:00:44,360 --> 00:00:46,840
<b>And it took a lot of persistance</b>

22
00:00:46,840 --> 00:00:49,280
<b>to stop people from talking about the Pinheiro case</b>

23
00:00:49,280 --> 00:00:50,800
<b>and start talking about the Braskem case.</b>

24
00:00:50,920 --> 00:00:54,400
<b>So I guess that took a while at first</b>

25
00:00:54,400 --> 00:00:56,760
<b>because there wasn't, and to this day there isn't</b>

26
00:00:56,760 --> 00:00:57,960
<b>a lot of knowledge produced</b>

27
00:00:57,960 --> 00:00:59,079
<b>in relation to the lagoon region</b>

28
00:00:59,079 --> 00:01:00,920
<b>and the issue of mining.</b>

29
00:01:01,599 --> 00:01:03,400
<b>The case is much more serious,</b>

30
00:01:03,400 --> 00:01:05,440
<b>so the more severe, the more difficult it is</b>

31
00:01:05,440 --> 00:01:08,400
<b>to gather knowledge</b>

32
00:01:08,400 --> 00:01:10,400
<b>minimally consolidated</b>

33
00:01:10,400 --> 00:01:13,559
<b>to allow you to establish something</b>

34
00:01:13,559 --> 00:01:18,039
<b>regarding a course of action.</b>

35
00:01:18,039 --> 00:01:20,480
<b>Because it happened later</b>

36
00:01:20,480 --> 00:01:21,960
<b>and all at the same time,</b>

37
00:01:21,960 --> 00:01:26,000
<b>it took a while to talk about these places.</b>

38
00:01:26,840 --> 00:01:29,239
<b>It also took a while for the organization to exist</b>

39
00:01:29,239 --> 00:01:30,400
<b>from the residents themselves.</b>

40
00:01:31,119 --> 00:01:33,280
<b>And that's where a factor comes in, which is low income.</b>

41
00:01:33,679 --> 00:01:35,079
<b>Most residents of this region</b>

42
00:01:35,079 --> 00:01:35,920
<b>are low-income.</b>

43
00:01:36,159 --> 00:01:38,079
<b>This influences both</b>

44
00:01:38,079 --> 00:01:40,280
<b>the visibility they manage to have</b>

45
00:01:40,280 --> 00:01:42,360
<b>with media coverage</b>

46
00:01:42,639 --> 00:01:45,360
<b>and the organization.</b>

47
00:01:45,519 --> 00:01:47,519
<b>They face greater difficulties</b>

48
00:01:47,519 --> 00:01:48,480
<b>getting organized</b>

49
00:01:48,480 --> 00:01:51,719
<b>because they won't have enough resources</b>

50
00:01:51,719 --> 00:01:54,400
<b>and they won't have enough visibility</b>

51
00:01:54,400 --> 00:01:56,039
<b>for that.</b>

52
00:01:56,400 --> 00:01:58,840
<b>And the ones who could come along,</b>

53
00:01:58,840 --> 00:02:00,199
<b>the people from Pinheiro,</b>

54
00:02:00,320 --> 00:02:02,639
<b>were very involved</b>

55
00:02:02,639 --> 00:02:04,519
<b>with their needs.</b>

56
00:02:05,079 --> 00:02:07,440
<b>With their need</b>

57
00:02:07,440 --> 00:02:10,119
<b>to organize their lives,</b>

58
00:02:10,119 --> 00:02:12,440
<b>to restructure it,</b>

59
00:02:12,440 --> 00:02:14,840
<b>to receive compensation,</b>

60
00:02:14,840 --> 00:02:16,639
<b>to move houses,</b>

61
00:02:16,639 --> 00:02:18,320
<b>to start their new life</b>

62
00:02:18,320 --> 00:02:21,000
<b>and also to leave it behind,</b>

63
00:02:21,000 --> 00:02:21,920
<b>because picture it,</b>

64
00:02:21,920 --> 00:02:24,159
<b>I followed cases closely,</b>

65
00:02:24,159 --> 00:02:27,639
<b>I followed cases of people close to me</b>

66
00:02:27,639 --> 00:02:29,079
<b>since knowing about it,</b>

67
00:02:29,079 --> 00:02:31,559
<b>when they were still around the problem,</b>

68
00:02:31,559 --> 00:02:33,199
<b>when trouble arrived in their lives,</b>

69
00:02:33,199 --> 00:02:36,119
<b>until trouble came in their lives</b>

70
00:02:36,119 --> 00:02:38,000
<b>until it caught up with them,</b>

71
00:02:38,000 --> 00:02:39,760
<b>until trouble caught up with them,</b>

72
00:02:39,760 --> 00:02:43,679
<b>until they entered and left,</b>

73
00:02:43,679 --> 00:02:45,000
<b>and started a new life.</b>

74
00:02:45,840 --> 00:02:48,360
<b>I followed the suffering of the people</b>

75
00:02:48,360 --> 00:02:49,920
<b>going through it.</b>

76
00:02:50,159 --> 00:02:52,519
<b>There comes a time when you want</b>

77
00:02:52,519 --> 00:02:54,239
<b>to leave it behind,</b>

78
00:02:54,559 --> 00:02:57,280
<b>go live your life,</b>

79
00:02:57,280 --> 00:02:59,719
<b>let it go,</b>

80
00:03:00,159 --> 00:03:02,400
<b>because you get tired.</b>

81
00:03:02,400 --> 00:03:05,400
<b>It is very exhausting,</b>

82
00:03:05,400 --> 00:03:07,199
<b>even emotionally</b>

83
00:03:07,480 --> 00:03:08,679
<b>and financially,</b>

84
00:03:08,679 --> 00:03:10,480
<b>you are in a very fragile situation.</b>

85
00:03:12,159 --> 00:03:13,679
<b>That's why compensations...</b>

86
00:03:13,679 --> 00:03:15,840
<b>They end,</b>

87
00:03:15,840 --> 00:03:17,960
<b>in more than 90% of cases,</b>

88
00:03:17,960 --> 00:03:21,920
<b>being "well received", so to speak,</b>

89
00:03:21,920 --> 00:03:22,840
<b>being accepted.</b>

90
00:03:23,280 --> 00:03:26,360
<b>Because you want to get on with your life,</b>

91
00:03:26,360 --> 00:03:28,840
<b>you want to leave the pain behind.</b>

92
00:03:29,599 --> 00:03:32,239
<b>So when the Mutange people</b>

93
00:03:32,239 --> 00:03:34,239
<b>of Bebedouro and Bom-Parto,</b>

94
00:03:34,239 --> 00:03:36,000
<b>which is the last lagoon neighborhood,</b>

95
00:03:36,000 --> 00:03:37,880
<b>receive this news</b>

96
00:03:38,119 --> 00:03:39,480
<b>and start to go through it,</b>

97
00:03:39,480 --> 00:03:42,599
<b>the people of Pinheiro, who could help,</b>

98
00:03:42,599 --> 00:03:45,960
<b>most of them are wanting to leave it behind.</b>

99
00:03:46,239 --> 00:03:47,679
<b>For all cases,</b>

100
00:03:47,679 --> 00:03:49,440
<b>including those of Pinheiro,</b>

101
00:03:49,440 --> 00:03:51,360
<b>I think it happened as well,</b>

102
00:03:51,360 --> 00:03:52,280
<b>and this is general.</b>

103
00:03:52,280 --> 00:03:55,559
<b>I'm not going to put it just for the lagoon neighborhoods,</b>

104
00:03:56,000 --> 00:03:59,880
<b>I think there was a lot of insensitivity</b>

105
00:04:00,320 --> 00:04:01,440
<b>in dealing with this issue.</b>

106
00:04:01,440 --> 00:04:02,360
<b>That was general.</b>

107
00:04:02,679 --> 00:04:05,280
<b>That was general, it still is today.</b>

108
00:04:05,280 --> 00:04:06,880
<b>When you see, today</b>

109
00:04:06,880 --> 00:04:09,400
<b>or a while ago,</b>

110
00:04:09,400 --> 00:04:12,280
<b>news coming out in the press</b>

111
00:04:12,280 --> 00:04:15,280
<b>about what the city hall was doing</b>

112
00:04:15,280 --> 00:04:16,239
<b>with Braskem,</b>

113
00:04:16,239 --> 00:04:21,320
<b>proposals for what would be done in that region,</b>

114
00:04:21,320 --> 00:04:23,199
<b>urban proposals.</b>

115
00:04:23,199 --> 00:04:25,400
<b>And when you see it get out,</b>

116
00:04:25,400 --> 00:04:27,280
<b>and it came out in the press,</b>

117
00:04:27,280 --> 00:04:30,039
<b>and it was never spoken to the locals</b>

118
00:04:30,039 --> 00:04:31,039
<b>before going to the press,</b>

119
00:04:31,039 --> 00:04:33,039
<b>the residents found out about it through the press!</b>

120
00:04:33,039 --> 00:04:34,280
<b>Even residents</b>

121
00:04:34,280 --> 00:04:37,280
<b>who were just aware</b>

122
00:04:37,280 --> 00:04:40,079
<b>that their living area</b>

123
00:04:40,079 --> 00:04:42,719
<b>was included, per the new map,</b>

124
00:04:42,719 --> 00:04:43,960
<b>in that perimeter.</b>

125
00:04:43,960 --> 00:04:46,480
<b>I've heard it from people: what about my house?</b>

126
00:04:46,800 --> 00:04:48,079
<b>Is my house there?</b>

127
00:04:48,480 --> 00:04:49,800
<b>Is it in that area?</b>

128
00:04:50,679 --> 00:04:52,519
<b>And you keep seeing,</b>

129
00:04:52,519 --> 00:04:54,760
<b>as it came out now recently,</b>

130
00:04:55,519 --> 00:04:58,519
<b>university professors,</b>

131
00:04:58,880 --> 00:05:01,880
<b>UFAL's own FUNDEP,</b>

132
00:05:01,880 --> 00:05:04,960
<b>speaking of proposing</b>

133
00:05:04,960 --> 00:05:06,800
<b>this area be transformed</b>

134
00:05:06,800 --> 00:05:09,800
<b>in a revitalization zone.</b>

135
00:05:09,920 --> 00:05:11,039
<b>Then you say: hold on,</b>

136
00:05:11,039 --> 00:05:13,400
<b>who did you discuss this with?</b>

137
00:05:15,320 --> 00:05:18,719
<b>And then you hear people saying</b>

138
00:05:18,719 --> 00:05:21,679
<b>that there's evidence</b>

139
00:05:21,679 --> 00:05:25,960
<b>that there is ongoing stabilization in the area.</b>

140
00:05:25,960 --> 00:05:26,920
<b>Wait a minute!</b>

141
00:05:26,920 --> 00:05:28,360
<b>What evidence is that?</b>

142
00:05:28,360 --> 00:05:29,840
<b>Where did it come from?</b>

143
00:05:29,840 --> 00:05:31,400
<b>What information did you have access to?</b>

144
00:05:31,400 --> 00:05:33,280
<b>Because as far as anyone knows,</b>

145
00:05:33,280 --> 00:05:37,039
<b>until 2023 there is no way to talk</b>

146
00:05:37,039 --> 00:05:38,599
<b>of stabilizing this area.</b>

147
00:05:39,400 --> 00:05:44,559
<b>In 2023 there will be a clearer notion</b>

148
00:05:45,159 --> 00:05:46,840
<b>really</b>

149
00:05:46,840 --> 00:05:49,840
<b>of how big the problem is.</b>

150
00:05:49,840 --> 00:05:52,159
<b>But we are talking about, two years from now,</b>

151
00:05:52,159 --> 00:05:54,199
<b>we start to have a clearer notion</b>

152
00:05:54,199 --> 00:05:55,840
<b>of the size of the problem.</b>

153
00:05:56,880 --> 00:05:59,199
<b>Why? Because it is expected</b>

154
00:05:59,199 --> 00:06:02,400
<b>that in 2023, possibly,</b>

155
00:06:02,400 --> 00:06:04,480
<b>Pinheiro! Pinheiro</b>

156
00:06:05,119 --> 00:06:09,199
<b>will have its sinking stabilized.</b>

157
00:06:09,559 --> 00:06:13,400
<b>And then what will remain is the lagoon region</b>

158
00:06:13,400 --> 00:06:17,960
<b>with a possible...</b>

159
00:06:19,960 --> 00:06:23,760
<b>Let's put it this way, with a possible</b>

160
00:06:23,760 --> 00:06:24,880
<b>clearer framework.</b>

161
00:06:24,880 --> 00:06:29,039
<b>Because you'll have questions already addressed</b>

162
00:06:29,039 --> 00:06:31,800
<b>and more established knowledge,</b>

163
00:06:31,800 --> 00:06:34,039
<b>some wells already filled</b>

164
00:06:34,039 --> 00:06:36,360
<b>to analyze the impact of this</b>

165
00:06:36,360 --> 00:06:38,639
<b>in the sinking of the remainder, etc.</b>

166
00:06:38,639 --> 00:06:40,119
<b>But that's an "if".</b>

167
00:06:40,559 --> 00:06:41,679
<b>That's an "if".</b>

168
00:06:41,679 --> 00:06:44,320
<b>So maybe. Maybe in 2023</b>

169
00:06:44,320 --> 00:06:46,199
<b>we have a clearer picture.</b>

170
00:06:46,880 --> 00:06:49,239
<b>But the very people who say that</b>

171
00:06:49,239 --> 00:06:50,719
<b>are clear in stating that</b>

172
00:06:50,719 --> 00:06:52,480
<b>there's no way to foresee</b>

173
00:06:52,480 --> 00:06:55,760
<b>when and if there will be sinkholes</b>

174
00:06:55,760 --> 00:06:56,840
<b>in the lagoon region.</b>

175
00:06:56,840 --> 00:06:59,159
<b>Or if there will be a suction phenomenon.</b>

176
00:06:59,159 --> 00:07:01,559
<b>So you can, in the meantime,</b>

177
00:07:01,559 --> 00:07:04,760
<b>have a more drastic sink</b>

178
00:07:04,760 --> 00:07:05,840
<b>in the lagoon.</b>

179
00:07:06,280 --> 00:07:08,000
<b>You can't be sure.</b>

180
00:07:08,000 --> 00:07:09,960
<b>So how are you talking</b>

181
00:07:09,960 --> 00:07:12,960
<b>and making a project</b>

182
00:07:12,960 --> 00:07:16,480
<b>to have a revitalization zone</b>

183
00:07:16,480 --> 00:07:17,360
<b>and whatnot</b>

184
00:07:17,360 --> 00:07:21,320
<b>if you don't even know the area's situation yet?</b>

185
00:07:22,840 --> 00:07:24,440
<b>And how are you doing that?</b>

186
00:07:24,440 --> 00:07:25,719
<b>Every time you do this,</b>

187
00:07:25,719 --> 00:07:29,239
<b>it is done with no clarification</b>

188
00:07:29,239 --> 00:07:31,280
<b>for those who matter first:</b>

189
00:07:31,280 --> 00:07:32,960
<b>the residents of this area.</b>


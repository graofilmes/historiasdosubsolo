1
00:00:00,760 --> 00:00:02,400
<b>The mistake started long ago, right?</b>

2
00:00:02,400 --> 00:00:04,159
<b>Since it was implemented.</b>

3
00:00:04,159 --> 00:00:05,199
<b>Because look:</b>

4
00:00:08,159 --> 00:00:10,800
<b>from... I can't say if it...</b>

5
00:00:10,800 --> 00:00:12,840
<b>started a few years ago</b>

6
00:00:12,840 --> 00:00:14,920
<b>or even longer, who knows?</b>

7
00:00:14,920 --> 00:00:16,039
<b>Firstly because --</b>

8
00:00:16,719 --> 00:00:17,880
<b>I don't know why</b>

9
00:00:17,880 --> 00:00:19,400
<b>they didn't do it at the time,</b>

10
00:00:19,400 --> 00:00:21,440
<b>back in the '70s,</b>

11
00:00:21,440 --> 00:00:24,599
<b>at least at the end of the '70s,</b>

12
00:00:24,599 --> 00:00:27,400
<b>they didn't do what I'm about to say.</b>

13
00:00:29,199 --> 00:00:31,719
<b>They drilled to assess</b>

14
00:00:31,719 --> 00:00:33,440
<b>what kind of rock was there.</b>

15
00:00:34,400 --> 00:00:37,639
<b>The same drilling you do for artesian wells,</b>

16
00:00:38,119 --> 00:00:39,599
<b>just to assess the ground</b>

17
00:00:39,599 --> 00:00:41,280
<b>without checking the resistance of the rock.</b>

18
00:00:41,280 --> 00:00:42,360
<b>This is paramount, guys.</b>

19
00:00:43,280 --> 00:00:44,519
<b>Back then, they didn't assess that.</b>

20
00:00:44,519 --> 00:00:45,960
<b>You can't find</b>

21
00:00:45,960 --> 00:00:50,159
<b>any determination on the degree of resistance</b>

22
00:00:50,159 --> 00:00:52,000
<b>of each rock along the depth.</b>

23
00:00:52,000 --> 00:00:53,039
<b>This is paramount.</b>

24
00:00:53,840 --> 00:00:54,719
<b>And they didn't do it.</b>

25
00:00:54,719 --> 00:00:57,320
<b>They only addressed the first.</b>

26
00:00:57,320 --> 00:00:59,400
<b>They only address the type of oil that keeps increasing. I found</b>
<b>that.</b>

27
00:01:00,159 --> 00:01:02,559
<b>nThey only address the type of oil.</b>

28
00:01:03,519 --> 00:01:04,800
<b>I found that</b>

29
00:01:05,719 --> 00:01:07,039
<b>because there's a doctoral thesis</b>

30
00:01:07,039 --> 00:01:10,039
<b>by Professor Cláudio Pires</b>

31
00:01:10,039 --> 00:01:11,400
<b>that was performed here,</b>

32
00:01:12,000 --> 00:01:13,400
<b>by the way, as far as I know</b>

33
00:01:13,400 --> 00:01:15,039
<b>it was financed by Braskem itself,</b>

34
00:01:16,480 --> 00:01:18,599
<b>and he did this...</b>

35
00:01:18,599 --> 00:01:21,239
<b>As it is a doctoral thesis, very important and all,</b>

36
00:01:21,239 --> 00:01:24,800
<b>it also determined the degree of integrity of the rocks.</b>

37
00:01:24,800 --> 00:01:27,480
<b>That's when I saw</b>

38
00:01:27,480 --> 00:01:28,639
<b>the rock,</b>

39
00:01:28,639 --> 00:01:32,960
<b>the 200 m of rock that precede the salt layer,</b>

40
00:01:32,960 --> 00:01:36,239
<b>this layer measuring</b>

41
00:01:36,239 --> 00:01:38,360
<b>around 750 m and 950 m.</b>

42
00:01:38,360 --> 00:01:41,320
<b>This layer of rock is extremely fragile.</b>

43
00:01:42,440 --> 00:01:45,719
<b>It's severely altered, it is not sound rock.</b>

44
00:01:46,360 --> 00:01:47,719
<b>That's why the mines,</b>

45
00:01:47,719 --> 00:01:50,920
<b>in addition to collapsing down there, began to rise.</b>

46
00:01:52,920 --> 00:01:54,119
<b>Many are 700 m deep,</b>

47
00:01:54,119 --> 00:01:57,719
<b>others rose about 500 m.</b>

48
00:01:57,719 --> 00:01:58,960
<b>I mean, rose 400 m,</b>

49
00:01:58,960 --> 00:02:01,360
<b>they used to measure 950 m and went up 550 m.</b>

50
00:02:01,360 --> 00:02:02,880
<b>They rose 400 m.</b>

51
00:02:03,480 --> 00:02:04,519
<b>That's mine 25.</b>

52
00:02:04,519 --> 00:02:06,880
<b>This mine is close to IMA.</b>

53
00:02:06,880 --> 00:02:07,960
<b>It went up the most.</b>

54
00:02:07,960 --> 00:02:11,280
<b>The others there, where Zé Lopes's house used to be,</b>

55
00:02:11,280 --> 00:02:13,519
<b>many of those have risen</b>

56
00:02:13,519 --> 00:02:16,199
<b>about 200 m, 300 m</b>

57
00:02:16,199 --> 00:02:18,599
<b>more or less, 250 m, give or take.</b>

58
00:02:18,599 --> 00:02:23,159
<b>The good part is that we have a good layer of rock,</b>

59
00:02:23,159 --> 00:02:26,679
<b>a reasonably good one, starting 500 m deep.</b>


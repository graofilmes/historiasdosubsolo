1
00:00:00,760 --> 00:00:04,639
There was a dune field.

2
00:00:05,960 --> 00:00:11,880
One of the most beautiful dune fields in the country.

3
00:00:13,360 --> 00:00:16,400
We have very little of it recorded

4
00:00:16,400 --> 00:00:20,400
in the book on Ecology by Mário Guimarães Ferri.

5
00:00:20,400 --> 00:00:26,239
They were very high dunes, so beautiful.

6
00:00:26,239 --> 00:00:29,320
It was a really beautiful region,

7
00:00:29,320 --> 00:00:30,519
that one,

8
00:00:31,559 --> 00:00:34,639
and like the biome: Restinga.

9
00:00:35,800 --> 00:00:38,519
There's a sandbank there.

10
00:00:39,480 --> 00:00:43,079
As a principle in environmental science,

11
00:00:43,079 --> 00:00:47,440
sandbanks are fragile.

12
00:00:48,239 --> 00:00:50,639
So they're not good

13
00:00:50,639 --> 00:00:54,559
for the implantation of an industry such as this.
1
00:00:00,360 --> 00:00:02,079
<b>Around there --</b>

2
00:00:02,079 --> 00:00:04,639
<b>in that area of the mines,</b>

3
00:00:05,239 --> 00:00:07,239
<b>there are geological faults.</b>

4
00:00:07,960 --> 00:00:08,880
<b>That's the first thing.</b>

5
00:00:08,880 --> 00:00:11,039
<b>A fault is a fissure,</b>

6
00:00:11,039 --> 00:00:14,519
<b>a crack that</b>

7
00:00:15,400 --> 00:00:18,159
<b>has been there for millions and millions of years.</b>

8
00:00:18,159 --> 00:00:20,519
<b>When I said there was a fault there,</b>

9
00:00:20,519 --> 00:00:22,039
<b>people called me crazy.</b>

10
00:00:22,039 --> 00:00:24,440
<b>Then CPI got involved and showed not one,</b>

11
00:00:24,440 --> 00:00:25,320
<b>but two faults!</b>

12
00:00:26,559 --> 00:00:29,480
<b>In that part of Mutange</b>

13
00:00:29,480 --> 00:00:31,960
<b>there's one, according to CPM,</b>

14
00:00:31,960 --> 00:00:34,239
<b>there is one that goes by the foot slope,</b>

15
00:00:34,920 --> 00:00:36,119
<b>and another</b>

16
00:00:36,119 --> 00:00:38,480
<b>closer to the lagoon,</b>

17
00:00:38,480 --> 00:00:39,599
<b>closer.</b>

18
00:00:39,599 --> 00:00:42,800
<b>There, about a hundred meters</b>

19
00:00:42,800 --> 00:00:43,840
<b>from each other.</b>

20
00:00:43,840 --> 00:00:45,800
<b>And the one closer to the lagoon</b>

21
00:00:45,800 --> 00:00:48,039
<b>goes through many mines.</b>

22
00:00:48,039 --> 00:00:50,360
<b>That's the first thing, ok?</b>

23
00:00:50,360 --> 00:00:51,280
<b>That's the first thing.</b>

24
00:00:51,280 --> 00:00:51,960
<b>Secondly:</b>

25
00:00:51,960 --> 00:00:52,480
<b>Look, guys,</b>

26
00:00:53,119 --> 00:00:54,840
<b>rock salt</b>

27
00:00:55,519 --> 00:00:58,119
<b>is a real wild animal, as I like to say.</b>

28
00:00:58,119 --> 00:01:00,400
<b>Rock salt can be a wild animal when you mess with it.</b>

29
00:01:01,079 --> 00:01:03,960
<b>If you change</b>

30
00:01:03,960 --> 00:01:06,400
<b>its temperature and pressure conditions,</b>

31
00:01:06,400 --> 00:01:08,920
<b>especially its pressure,</b>

32
00:01:08,920 --> 00:01:11,760
<b>it starts moving around.</b>

33
00:01:11,760 --> 00:01:15,360
<b>Rock salt has a creeping property.</b>

34
00:01:16,280 --> 00:01:18,559
<b>For example, if you poke a hole in it here,</b>

35
00:01:18,559 --> 00:01:20,159
<b>it tries to close it.</b>

36
00:01:20,159 --> 00:01:22,079
<b>It gets softer,</b>

37
00:01:22,079 --> 00:01:26,199
<b>because you created a void there and released pressure.</b>

38
00:01:26,199 --> 00:01:28,239
<b>The pressure that held it in place.</b>

39
00:01:29,280 --> 00:01:30,519
<b>So you need pressure.</b>

40
00:01:30,519 --> 00:01:32,880
<b>That's why a technique for</b>

41
00:01:32,880 --> 00:01:39,639
<b>good exploration of rock salt</b>

42
00:01:39,639 --> 00:01:42,440
<b>requires pressure</b>

43
00:01:42,440 --> 00:01:43,719
<b>inside that hole</b>

44
00:01:43,719 --> 00:01:45,320
<b>in the cave, you know?</b>

45
00:01:45,320 --> 00:01:48,760
<b>So it does not creep, does not close</b>

46
00:01:49,320 --> 00:01:52,320
<b>and does not lose its properties.</b>

47
00:01:52,320 --> 00:01:54,880
<b>But there are faults,</b>

48
00:01:54,880 --> 00:01:57,199
<b>and these faults</b>

49
00:01:57,199 --> 00:02:02,119
<b>have caused this</b>

50
00:02:02,719 --> 00:02:04,079
<b>very necessary pressure</b>

51
00:02:04,079 --> 00:02:05,159
<b>to fail!</b>

52
00:02:05,159 --> 00:02:07,920
<b>And when it did, the rock started to go soft.</b>

53
00:02:08,639 --> 00:02:10,920
<b>This took place for several decades,</b>

54
00:02:10,920 --> 00:02:13,000
<b>nothing happened overnight, no.</b>

55
00:02:13,000 --> 00:02:16,840
<b>It started back in the extraction</b>

56
00:02:16,840 --> 00:02:19,840
<b>of this saline rock.</b>

57
00:02:20,400 --> 00:02:22,960
<b>Salgema's exploitation began in '75</b>

58
00:02:22,960 --> 00:02:24,599
<b>or '76, around that time...</b>

59
00:02:24,599 --> 00:02:25,719
<b>That's when it started, you know?</b>

60
00:02:25,719 --> 00:02:28,199
<b>But it started to show its claws</b>

61
00:02:28,199 --> 00:02:30,840
<b>about 20 years ago.</b>

62
00:02:31,880 --> 00:02:34,000
<b>Around 2001 or 2002.</b>

63
00:02:34,000 --> 00:02:39,639
<b>They started, due to a pressure failure...</b>

64
00:02:39,639 --> 00:02:41,320
<b>Pressurization,</b>

65
00:02:41,320 --> 00:02:43,719
<b>which is the pressure inside the cave...</b>

66
00:02:43,719 --> 00:02:45,519
<b>It started to fail and leak</b>

67
00:02:45,519 --> 00:02:48,679
<b>and it got soft.</b>

68
00:02:48,679 --> 00:02:50,800
<b>It progressed very slowly.</b>

69
00:02:50,800 --> 00:02:52,960
<b>And it got softer and started creeping...</b>

70
00:02:52,960 --> 00:02:54,440
<b>It gets softer and softer,</b>

71
00:02:54,440 --> 00:02:56,360
<b>almost closing,</b>

72
00:02:56,360 --> 00:02:58,480
<b>towards the center of the hole.</b>

73
00:02:59,320 --> 00:03:01,840
<b>The center of the caves.</b>

74
00:03:01,840 --> 00:03:04,719
<b>This layer started losing resistance.</b>

75
00:03:04,719 --> 00:03:06,559
<b>It got softer,</b>

76
00:03:06,559 --> 00:03:08,239
<b>and started creeping</b>

77
00:03:08,239 --> 00:03:11,480
<b>millimetrically over these years.</b>

78
00:03:11,480 --> 00:03:13,000
<b>Especially for the past 20 years,</b>

79
00:03:13,000 --> 00:03:14,000
<b>it started to creep.</b>

80
00:03:14,760 --> 00:03:16,559
<b>It got softer and started creeping.</b>

81
00:03:16,559 --> 00:03:19,239
<b>The softer it got,</b>

82
00:03:19,239 --> 00:03:21,760
<b>the deeper it sank.</b>

83
00:03:22,920 --> 00:03:24,760
<b>Sinking is the subsidence</b>

84
00:03:24,760 --> 00:03:25,760
<b>you asked about.</b>

85
00:03:26,519 --> 00:03:30,840
<b>So it was exactly in that area of mines</b>

86
00:03:30,840 --> 00:03:32,320
<b>that it started to sink.</b>

87
00:03:32,960 --> 00:03:34,239
<b>Deforming, deforming</b>

88
00:03:34,239 --> 00:03:36,840
<b>millimetrically over the years.</b>

89
00:03:37,599 --> 00:03:41,320
<b>In the first semester of 2019</b>

90
00:03:42,199 --> 00:03:43,320
<b>I went down Pinheiro</b>

91
00:03:43,320 --> 00:03:45,119
<b>and went down there to see Lagoa.</b>

92
00:03:46,039 --> 00:03:48,840
<b>There in Lagoa I went canoeing,</b>

93
00:03:48,840 --> 00:03:51,920
<b>I took a seasoned boatman</b>

94
00:03:51,920 --> 00:03:54,360
<b>that knew everything around.</b>

95
00:03:54,360 --> 00:03:57,039
<b>And he started telling me what it was like here and there</b>

96
00:03:57,039 --> 00:04:00,960
<b>and talked to the family of Zé Lopes,</b>

97
00:04:01,880 --> 00:04:03,199
<b>with his children.</b>

98
00:04:03,920 --> 00:04:05,079
<b>I also spoke with the caretaker</b>

99
00:04:05,079 --> 00:04:06,599
<b>who has been working there for 40 years.</b>

100
00:04:07,280 --> 00:04:09,719
<b>He said: look, this is what it was like...</b>

101
00:04:09,719 --> 00:04:11,880
<b>then, after all that I said:</b>

102
00:04:11,880 --> 00:04:15,079
<b>well, this place is what I call the "Eye of the Storm".</b>

103
00:04:15,079 --> 00:04:16,519
<b>It's around Zé Lopes' house,</b>

104
00:04:16,519 --> 00:04:17,480
<b>where the mines are,</b>

105
00:04:17,480 --> 00:04:18,679
<b>where they are concentrated.</b>

106
00:04:18,679 --> 00:04:19,920
<b>I'll show you.</b>

107
00:04:19,920 --> 00:04:21,239
<b>There, see?</b>

108
00:04:21,239 --> 00:04:23,599
<b>There's a big subsidence there,</b>

109
00:04:23,599 --> 00:04:26,639
<b>I have a slide about that, that I've presented in 2019.</b>

110
00:04:27,199 --> 00:04:29,559
<b>Here, it has sunk between 1.5 m and 2 m</b>

111
00:04:29,559 --> 00:04:31,280
<b>in the last 10 years.</b>

112
00:04:32,199 --> 00:04:35,840
<b>Nature, that very famous journal,</b>

113
00:04:35,840 --> 00:04:40,239
<b>very respected scientific journal,</b>

114
00:04:40,239 --> 00:04:42,239
<b>perhaps the most respected of all,</b>

115
00:04:42,840 --> 00:04:43,880
<b>the British one.</b>

116
00:04:43,880 --> 00:04:46,320
<b>It visited and published</b>

117
00:04:46,320 --> 00:04:47,880
<b>about a month ago, two months at most!</b>

118
00:04:47,880 --> 00:04:51,280
<b>It published a study on</b>

119
00:04:51,280 --> 00:04:53,960
<b>this issue, this tragedy of Maceió.</b>

120
00:04:54,719 --> 00:04:57,239
<b>And it showed that from 2004</b>

121
00:04:57,840 --> 00:05:00,400
<b>to 2020, 16 years,</b>

122
00:05:00,400 --> 00:05:03,000
<b>right where I'm referring to,</b>

123
00:05:03,000 --> 00:05:04,760
<b>where the mines are concentrated,</b>

124
00:05:04,760 --> 00:05:07,559
<b>that place sunk 2 m.</b>

125
00:05:07,559 --> 00:05:11,480
<b>And that is close to what Professor Abel Galindo estimated</b>

126
00:05:11,480 --> 00:05:15,280
<b>with his work in loco, on top of a small canoe.</b>

127
00:05:15,280 --> 00:05:19,079
<b>And these people used several satellites...</b>

128
00:05:19,840 --> 00:05:22,599
<b>Several satellites but also in loco.</b>

129
00:05:22,599 --> 00:05:26,119
<b>They also took drilling results and such.</b>

130
00:05:26,840 --> 00:05:27,480
<b>So there you have it.</b>


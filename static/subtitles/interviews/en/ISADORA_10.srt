1
00:00:00,800 --> 00:00:02,840
<b>Well, there are several questions there.</b>

2
00:00:02,840 --> 00:00:05,960
<b>First, it impacts city planning.</b>

3
00:00:05,960 --> 00:00:07,320
<b>You will suddenly have</b>

4
00:00:07,320 --> 00:00:11,119
<b>an entire population displaced to other areas</b>

5
00:00:11,119 --> 00:00:14,199
<b>and these areas will receive an overload</b>

6
00:00:14,199 --> 00:00:15,480
<b>that was not foreseen.</b>

7
00:00:15,480 --> 00:00:16,639
<b>So they will receive</b>

8
00:00:16,639 --> 00:00:19,000
<b>infrastructure overload, for example.</b>

9
00:00:19,000 --> 00:00:20,880
<b>So you need a backup.</b>

10
00:00:20,880 --> 00:00:23,199
<b>You'll need a backup of all the networks.</b>

11
00:00:23,199 --> 00:00:24,239
<b>You'll have to reinforce</b>

12
00:00:24,239 --> 00:00:26,840
<b>the water supply network,</b>

13
00:00:26,840 --> 00:00:28,719
<b>the electricity network,</b>

14
00:00:28,719 --> 00:00:31,360
<b>urban drainage,</b>

15
00:00:31,360 --> 00:00:32,519
<b>mobility</b>

16
00:00:32,559 --> 00:00:35,400
<b>of bus lines,</b>

17
00:00:35,400 --> 00:00:39,639
<b>even road traffic in general.</b>

18
00:00:39,639 --> 00:00:43,519
<b>All this is impacted by the new residents.</b>

19
00:00:43,519 --> 00:00:49,039
<b>So the tendency is to have real estate expansion</b>

20
00:00:49,039 --> 00:00:51,039
<b>in other areas of Maceió.</b>

21
00:00:51,760 --> 00:00:53,960
<b>It shouldn't be that way</b>

22
00:00:53,960 --> 00:00:56,400
<b>because we could seize...</b>

23
00:00:56,400 --> 00:00:58,840
<b>I used to talk about that when I was at the IAB, right?</b>

24
00:00:58,840 --> 00:01:01,639
<b>I did. Since 2019 I talk</b>

25
00:01:01,639 --> 00:01:05,239
<b>about making this evacuation of people</b>

26
00:01:05,239 --> 00:01:06,320
<b>in a planned way.</b>

27
00:01:06,320 --> 00:01:08,920
<b>In other words, we have areas in Maceió,</b>

28
00:01:08,920 --> 00:01:10,320
<b>neighborhoods in Maceió</b>

29
00:01:10,320 --> 00:01:11,639
<b>that are empty,</b>

30
00:01:11,639 --> 00:01:13,519
<b>with little occupation.</b>

31
00:01:13,519 --> 00:01:16,599
<b>So why not start working</b>

32
00:01:16,599 --> 00:01:18,280
<b>with the real estate market</b>

33
00:01:18,280 --> 00:01:21,519
<b>on a microcredit line,</b>

34
00:01:21,519 --> 00:01:22,960
<b>a line of financing</b>

35
00:01:22,960 --> 00:01:25,159
<b>that favors</b>

36
00:01:25,239 --> 00:01:27,159
<b>the establishment of</b>

37
00:01:27,159 --> 00:01:29,079
<b>real estate projects in these areas?</b>

38
00:01:29,079 --> 00:01:32,119
<b>And you had people</b>

39
00:01:32,119 --> 00:01:34,400
<b>at the same time they left,</b>

40
00:01:34,400 --> 00:01:36,639
<b>you had the occupation of places</b>

41
00:01:36,639 --> 00:01:39,920
<b>where there's already an infrastructure installed, for example.</b>

42
00:01:39,920 --> 00:01:42,119
<b>And with that, you would have,</b>

43
00:01:43,199 --> 00:01:43,880
<b>let's put it this way,</b>

44
00:01:43,880 --> 00:01:47,880
<b>you would have the ease</b>

45
00:01:47,880 --> 00:01:51,800
<b>and the good result of occupying areas</b>

46
00:01:51,800 --> 00:01:53,119
<b>that already have infrastructure</b>

47
00:01:53,119 --> 00:01:55,280
<b>but that were with little occupation,</b>

48
00:01:55,280 --> 00:01:56,360
<b>with little densification,</b>

49
00:01:56,360 --> 00:02:00,840
<b>and thus it would start to positively influence</b>

50
00:02:00,840 --> 00:02:02,360
<b>the urban life of these places,</b>

51
00:02:02,360 --> 00:02:04,599
<b>which would start to receive new residents</b>

52
00:02:04,599 --> 00:02:06,519
<b>and, with that, the new occupation would generate</b>

53
00:02:06,519 --> 00:02:07,599
<b>new dynamics.</b>
<b>But that would need 11.</b>
<b>Public power planning together with AA.</b>

54
00:02:08,000 --> 00:02:11,760
<b>But this would require planning</b>

55
00:02:11,760 --> 00:02:15,800
<b>by the municipal public authorities</b>

56
00:02:15,800 --> 00:02:18,559
<b>together with the federal public authorities, for example.</b>

57
00:02:18,559 --> 00:02:22,199
<b>It would have to start a dialogue,</b>

58
00:02:22,199 --> 00:02:25,400
<b>constitute a dialogue with the federal power</b>

59
00:02:25,400 --> 00:02:28,679
<b>for these projects to be approved</b>

60
00:02:28,679 --> 00:02:31,400
<b>so that there could be</b>

61
00:02:31,400 --> 00:02:35,400
<b>a differentiated line of credit aimed at Maceió.</b>

62
00:02:35,400 --> 00:02:37,119
<b>And this would be justified</b>

63
00:02:37,119 --> 00:02:39,400
<b>precisely by the fact that</b>

64
00:02:39,400 --> 00:02:41,840
<b>it is the greatest urban,</b>

65
00:02:41,840 --> 00:02:43,880
<b>environmental-urban tragedy in national history.</b>

66
00:02:43,880 --> 00:02:45,880
<b>It would be justified</b>

67
00:02:45,920 --> 00:02:47,599
<b>for the federal public power</b>

68
00:02:47,599 --> 00:02:49,280
<b>to cast a different</b>

69
00:02:49,280 --> 00:02:51,000
<b>look at Maceió</b>

70
00:02:51,000 --> 00:02:52,840
<b>in relation to the rest of the country.</b>

71
00:02:52,840 --> 00:02:56,280
<b>Which is another thing we need to touch on here,</b>

72
00:02:56,280 --> 00:02:58,639
<b>which is: what hasn't happened so far?</b>

73
00:02:58,639 --> 00:03:01,119
<b>If you think about it,</b>

74
00:03:01,119 --> 00:03:05,119
<b>until now we haven't had the federal public power</b>

75
00:03:05,119 --> 00:03:08,840
<b>turn to Maceió with a different look.</b>

76
00:03:08,840 --> 00:03:11,440
<b>How is it that I'm having in a city,</b>

77
00:03:11,440 --> 00:03:14,360
<b>in a state, in a capital,</b>

78
00:03:14,360 --> 00:03:16,079
<b>the biggest national urban-environmental tragedy</b>

79
00:03:16,079 --> 00:03:18,920
<b>and I don't have federal public power</b>

80
00:03:18,920 --> 00:03:21,280
<b>turned with its ministries here?</b>

81
00:03:21,280 --> 00:03:24,639
<b>To say: look, let's think together</b>

82
00:03:24,639 --> 00:03:26,599
<b>about what to do?</b>

83
00:03:27,519 --> 00:03:29,159
<b>What to do in Maceio?</b>

84
00:03:29,840 --> 00:03:32,000
<b>And then, you see how much</b>

85
00:03:32,000 --> 00:03:34,239
<b>the issue of visibility influences this.</b>

86
00:03:34,239 --> 00:03:35,360
<b>Because we talk about the visibility</b>

87
00:03:35,360 --> 00:03:37,599
<b>of the poorest places.</b>

88
00:03:37,599 --> 00:03:39,760
<b>that they had difficulty accessing the media.</b>

89
00:03:39,760 --> 00:03:42,000
<b>Maceió is a</b>

90
00:03:42,719 --> 00:03:44,000
<b>state capital</b>

91
00:03:44,000 --> 00:03:46,280
<b>from one of the poorest states in the country,</b>

92
00:03:47,000 --> 00:03:50,719
<b>from a region that is also one of the poorest,</b>

93
00:03:51,559 --> 00:03:53,960
<b>and therefore does not have the visibility that</b>

94
00:03:53,960 --> 00:03:57,239
<b>the country's large urban centers have.</b>

95
00:03:57,239 --> 00:04:01,800
<bIf this tragedy were in Rio de Janeiro,</b>

96
00:04:01,800 --> 00:04:05,639
<b>São Paulo, Minas Gerais</b>

97
00:04:05,639 --> 00:04:07,320
<b>we would already have</b>

98
00:04:07,320 --> 00:04:10,400
<b>all the ministries focused on that place</b>

99
00:04:10,400 --> 00:04:14,119
<b>proposing, each in its own area,</b>

100
00:04:14,119 --> 00:04:15,599
<b>what could be done</b>

101
00:04:15,599 --> 00:04:19,199
<b>to minimize the issue.</b>

102
00:04:20,159 --> 00:04:22,280
<b>From the start, from the start.</b>

103
00:04:23,360 --> 00:04:24,920
<b>And we have proof of that</b>

104
00:04:24,920 --> 00:04:27,679
<b>because we saw it happen in Minas</b>

105
00:04:27,679 --> 00:04:29,800
<b>with the tragedies related to Vale do Rio Doce.</b>

106
00:04:31,199 --> 00:04:33,639
<b>So, we don't have that here.</b>

107
00:04:33,639 --> 00:04:35,679
<b>We see a great silence</b>

108
00:04:35,679 --> 00:04:38,280
<b>on the part of the federal government</b>

109
00:04:38,280 --> 00:04:39,679
<b>regarding what is happening here.</b>

110
00:04:39,679 --> 00:04:40,960
<b>We have seen for a long time</b>

111
00:04:40,960 --> 00:04:44,119
<b>a great silence from the general, national media</b>

112
00:04:44,119 --> 00:04:45,239
<b>about what's happening here.</b>

113
00:04:45,239 --> 00:04:47,400
<b>We see it in part to this today,</b>

114
00:04:47,400 --> 00:04:49,559
<b>because what comes out in the national media</b>

115
00:04:49,559 --> 00:04:51,119
<b>in relation to what happens here</b>

116
00:04:51,119 --> 00:04:53,639
<b>is very, very, very little.</b>

117
00:04:53,639 --> 00:04:56,199
<b>It's very small compared to the size of the tragedy.</b>

118
00:04:57,760 --> 00:05:00,239
<b>In relation to the powers that be,</b>

119
00:05:00,239 --> 00:05:02,159
<b>we see this inoperative posture.</b>

120
00:05:03,679 --> 00:05:06,880
<b>I even think that this should come</b>

121
00:05:06,880 --> 00:05:10,559
<b>as a provocation from our local representatives.</b>

122
00:05:10,559 --> 00:05:13,280
<b>There should have been</b>

123
00:05:13,280 --> 00:05:16,199
<b>a more proactive stance on the part of the city government,</b>

124
00:05:16,199 --> 00:05:18,519
<b>on the part of the state government, a long time ago.</b>

125
00:05:18,519 --> 00:05:20,840
<b>And a more proactive stance by our politicians,</b>

126
00:05:20,840 --> 00:05:23,199
<b>federal deputies and senators</b>

127
00:05:23,199 --> 00:05:24,360
<b>in this regard.</b>

128
00:05:24,360 --> 00:05:26,400
<b>They should bring it up constantly,</b>

129
00:05:26,400 --> 00:05:30,360
<b>establish this dialogue</b>

130
00:05:30,360 --> 00:05:34,079
<b>for specific public policies here.</b>

131
00:05:34,079 --> 00:05:34,119
<b>Specific to this place?</b>

132
00:05:34,840 --> 00:05:38,119
<b>Yes, specific! We are talking about 60 thousand people.</b>

133
00:05:39,559 --> 00:05:41,639
<b>This should have provoked</b>

134
00:05:41,639 --> 00:05:44,880
<b>a series of specific public policies.</b>

135
00:05:44,880 --> 00:05:47,159
<b>From urban public policies</b>

136
00:05:47,159 --> 00:05:48,719
<b>to environmental public policies,</b>

137
00:05:48,719 --> 00:05:53,119
<b>including economic public policies.</b>

138
00:05:53,119 --> 00:05:55,519
<b>You know, all kinds of public policy</b>

139
00:05:55,519 --> 00:05:58,920
<b>should have been established, aimed here.</b>

140
00:05:58,920 --> 00:05:59,559
<b>Aimed here.</b>

141
00:05:59,559 --> 00:06:01,119
<b>To work with this region.</b>


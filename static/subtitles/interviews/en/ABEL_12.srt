1
00:00:00,320 --> 00:00:00,920
<b>You can't put a price on it.</b>

2
00:00:00,920 --> 00:00:03,079
<b>It's a real big tragedy.</b>

3
00:00:03,079 --> 00:00:03,920
<b>A big one!</b>

4
00:00:03,920 --> 00:00:06,039
<b>Some people took it very badly</b>

5
00:00:06,039 --> 00:00:08,000
<b>and got into a deep depression.</b>

6
00:00:08,000 --> 00:00:10,639
<b>A lot has happened,</b>

7
00:00:12,960 --> 00:00:15,199
<b>but it was a tragery for Maceió.</b>

8
00:00:15,800 --> 00:00:17,039
<b>People that...</b>

9
00:00:17,039 --> 00:00:19,960
<b>One hundred and so thousand reais in machinery only.</b>

10
00:00:20,400 --> 00:00:23,039
<b>And they offered ten thousand reais to people.</b>

11
00:00:23,880 --> 00:00:24,840
<b>And so it goes.</b>

12
00:00:24,840 --> 00:00:28,199
<b>The most famous</b>

13
00:00:28,199 --> 00:00:30,559
<b>is the prom girl, Eliana.</b>

14
00:00:30,559 --> 00:00:34,199
<b>The poor girl overspent,</b>

15
00:00:34,199 --> 00:00:36,000
<b>took out a bank loan,</b>

16
00:00:36,000 --> 00:00:38,480
<b>and is in debt for I don't know how much...</b>

17
00:00:38,480 --> 00:00:39,840
<b>60 or 80 thousand.</b>

18
00:00:39,840 --> 00:00:41,280
<b>I don't know precisely, but she does.</b>

19
00:00:41,280 --> 00:00:43,079
<b>You can interview her.</b>

20
00:00:43,079 --> 00:00:44,599
<b>They offered her 10 grand.</b>

21
00:00:44,599 --> 00:00:46,280
<b>Ten thousand reais!</b>

22
00:00:47,480 --> 00:00:48,400
<b>That's peanuts!</b>

23
00:00:49,280 --> 00:00:51,320
<b>And the bakery guys?</b>

24
00:00:52,440 --> 00:00:55,400
<b>It will cost a fortune to settle somewhere else.</b>

25
00:00:57,039 --> 00:01:00,119
<b>Ten thousand is nothing!</b>

26
00:01:00,119 --> 00:01:03,000
<b>If you have a business, that's nothing!</b>

27
00:01:03,000 --> 00:01:07,719
<b>You have to start over, get new customers,</b>

28
00:01:07,719 --> 00:01:09,519
<b>new equipment, new everything.</b>

29
00:01:09,519 --> 00:01:11,159
<b>Ten thousand reais is nothing.</b>

30
00:01:11,159 --> 00:01:12,000
<b>It's a tragedy, isn't it?</b>

31
00:01:12,639 --> 00:01:14,800
<b>There were cases where...</b>

32
00:01:15,480 --> 00:01:18,920
<b>the owners were financially pleased, right?</b>

33
00:01:18,920 --> 00:01:20,199
<b>Some cases.</b>

34
00:01:21,000 --> 00:01:22,239
<b>But in others...</b>

35
00:01:22,239 --> 00:01:23,880
<b>I heard about cases</b>

36
00:01:23,880 --> 00:01:26,079
<b>where the guy expected to get,</b>

37
00:01:26,079 --> 00:01:29,440
<b>for his house of around 500m², 600m²,</b>

38
00:01:29,440 --> 00:01:32,320
<b>he expected at least one and a half million</b>

39
00:01:32,320 --> 00:01:34,800
<b>and they came with, I don't know, 600 reais.</b>

40
00:01:35,599 --> 00:01:37,960
<b>Peanuts when compared to what the house is worth.</b>

41
00:01:38,159 --> 00:01:40,199
<b>Behind Zé Lopes' house, there was a huge place,</b>

42
00:01:40,199 --> 00:01:41,960
<b>pretty, full of fruit trees</b>

43
00:01:41,960 --> 00:01:46,280
<b>that had over 150m, beyond the back wall./b>

44
00:01:46,880 --> 00:01:48,159
<b>At the Lopes' house,</b>

45
00:01:48,159 --> 00:01:49,599
<b>there was over 150m to the back</b>

46
00:01:49,599 --> 00:01:50,840
<b>where the lagoon used to go.</b>

47
00:01:50,840 --> 00:01:54,159
<b>And there was this wonderful place, Dr José Lopes,</b>

48
00:01:54,159 --> 00:01:56,119
<b>where he created everything, with a lot of borders.</b>

49
00:01:56,119 --> 00:02:00,559
<b>Today there is over 2m of water.</b>

50
00:02:01,280 --> 00:02:02,440
<b>2 meters that drowned!</b>


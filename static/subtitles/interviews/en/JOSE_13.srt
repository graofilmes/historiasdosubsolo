1
00:00:00,480 --> 00:00:02,480
So, as I already knew

2
00:00:02,480 --> 00:00:05,960
and had documents and evidence,

3
00:00:05,960 --> 00:00:09,039
that was when I was interviewed

4
00:00:09,039 --> 00:00:13,800
<b>by Wendel for the first time on TV Assembleia.</b>

5
00:00:14,400 --> 00:00:15,519
And that's when I said: look, guys,

6
00:00:15,519 --> 00:00:18,039
that wasn't an earthquake

7
00:00:18,039 --> 00:00:19,920
in the classical sense...

8
00:00:20,760 --> 00:00:23,119
What happened was...

9
00:00:23,119 --> 00:00:25,800
And then I pulled my files.

10
00:00:26,960 --> 00:00:29,280
What happenend was...

11
00:00:29,280 --> 00:00:32,320
I grabbed the opinions of geologists.

12
00:00:33,280 --> 00:00:37,239
The mines haven't been monitored this entire time

13
00:00:37,960 --> 00:00:41,360
and one mine coalesced with the other,

14
00:00:41,360 --> 00:00:42,239
they joined.

15
00:00:43,400 --> 00:00:46,280
<b>And the roof couldn't handle it</b>

16
00:00:46,880 --> 00:00:48,119
<b>and collapsed.</b>

17
00:00:49,039 --> 00:00:50,440
<b>That's what happened.</b>

18
00:00:51,639 --> 00:00:53,119
To my surprise,

19
00:00:53,119 --> 00:00:56,639
not my surprise, to my realization,

20
00:00:56,639 --> 00:00:59,239
I would never say it to my joy, right?

21
00:00:59,239 --> 00:01:04,480
Otávio was there and he saw

22
00:01:04,480 --> 00:01:08,360
when the official DNPM report was presented.

23
00:01:09,199 --> 00:01:13,320
And the official DNPM report confirmed,

24
00:01:14,000 --> 00:01:16,480
with precise data and

25
00:01:16,480 --> 00:01:19,039
accurate calculations,

26
00:01:19,039 --> 00:01:21,360
what happened:

27
00:01:22,000 --> 00:01:24,760
One mine coalesced with another

28
00:01:24,760 --> 00:01:26,679
 and the roof collapsed.

29
00:01:27,320 --> 00:01:29,920
<b>That's the root of the so-called earthquake.</b>


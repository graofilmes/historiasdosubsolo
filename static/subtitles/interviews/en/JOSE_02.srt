1
00:00:00,400 --> 00:00:03,719
I was nominated

2
00:00:03,719 --> 00:00:08,840
planning technician.

3
00:00:09,320 --> 00:00:13,400
It was the way Professor José de Melo Gomes found

4
00:00:13,400 --> 00:00:15,880
to pay me,

5
00:00:16,519 --> 00:00:18,599
<b>because I spent this whole time...</b>

6
00:00:18,639 --> 00:00:21,119
Governor  Suruagy had said to me:

7
00:00:21,119 --> 00:00:23,079
Look, here's another issue:

8
00:00:23,079 --> 00:00:25,880
<b>I can't pay you.</b>

9
00:00:27,880 --> 00:00:28,920
And I accepted.

10
00:00:28,920 --> 00:00:32,639
Then Professor José de Melo Gomes found a way.

11
00:00:33,199 --> 00:00:37,400
So I was hired as a planning technician

12
00:00:37,400 --> 00:00:41,760
at the State Planning Secretariat.

13
00:00:41,760 --> 00:00:47,360
There's never been an official nomination

14
00:00:47,360 --> 00:00:50,400
for me to answer

15
00:00:50,400 --> 00:00:53,599
<b>for nothing on the environment, right?</b>

16
00:00:54,440 --> 00:00:56,679
And in reality, this created an impasse

17
00:00:56,679 --> 00:01:01,320
which was an impasse for Governor Suruagy,

18
00:01:01,320 --> 00:01:04,519
<b>as he had not appointed me,</b>

19
00:01:05,400 --> 00:01:07,760
<b>he couldn't fire me.</b>

20
00:01:08,639 --> 00:01:10,559
<b>Do you understand?</b>

21
00:01:11,760 --> 00:01:14,960
So while he couldn't fire me,

22
00:01:14,960 --> 00:01:19,039
I just kept answering back.

23
00:01:19,719 --> 00:01:24,079
<b>I had documents, I still do.</b>

24
00:01:24,480 --> 00:01:27,440
Him officially forwarding it to me

25
00:01:27,440 --> 00:01:28,880
with his signature.

26
00:01:28,880 --> 00:01:30,280
He had never nominated me,

27
00:01:30,280 --> 00:01:33,000
but forwarding with his signature.

28
00:01:33,000 --> 00:01:37,199
And documents addressed to me

29
00:01:37,199 --> 00:01:40,400
<b>as Secretary of Pollution Control.</b>

30
00:01:40,880 --> 00:01:44,320
So I was perfectly covered,

31
00:01:44,320 --> 00:01:45,760
legally covered.

32
00:01:46,440 --> 00:01:48,800
That's what gave me the resilience

33
00:01:48,800 --> 00:01:50,519
that people ask:

34
00:01:50,519 --> 00:01:54,320
But how did you resist all those pressures?

35
00:01:54,320 --> 00:01:59,320
All the violent bullying that this rock salt sponsored?

36
00:01:59,840 --> 00:02:01,119
<b>I say: Look, guys, I...</b>

37
00:02:02,559 --> 00:02:03,519
<b>I was covered.</b>


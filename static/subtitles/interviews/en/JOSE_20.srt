1
00:00:00,159 --> 00:00:01,639
<b>Project Cata-Vento</b>

2
00:00:02,440 --> 00:00:06,679
was set by the civil defense

3
00:00:07,320 --> 00:00:09,199
<b>of the state of Alagoas</b>

4
00:00:10,599 --> 00:00:11,719
back when

5
00:00:11,719 --> 00:00:13,639
I held the position

6
00:00:13,639 --> 00:00:15,000
of environment coordinator.

7
00:00:16,719 --> 00:00:18,719
<b>It was a project</b>

8
00:00:19,519 --> 00:00:23,239
in case there was...

9
00:00:23,239 --> 00:00:26,039
It's not just a runaway leak,

10
00:00:26,039 --> 00:00:30,159
it's not just Braskem,
it's an environmental risk

11
00:00:30,159 --> 00:00:33,559
calculated by CETESB

12
00:00:33,559 --> 00:00:37,440
and published by Governor Suruagy
in the official gazette.

13
00:00:37,440 --> 00:00:43,159
It has a non-negligible risk

14
00:00:43,159 --> 00:00:48,519
of a major accident releasing

15
00:00:48,519 --> 00:00:52,880
<b>a lot of chlorine in the city of Maceió.</b>

16
00:00:53,920 --> 00:00:56,639
So the civil defense was in charge of

17
00:00:56,639 --> 00:00:58,960
preparing a document for,

18
00:00:58,960 --> 00:01:02,599
in case of a catastrophe,

19
00:01:03,280 --> 00:01:06,400
<b>what should the population do?</b>

20
00:01:07,280 --> 00:01:09,960
Guys, it's called Operação Cata-Vento,

21
00:01:09,960 --> 00:01:11,199
go look for it.

22
00:01:13,639 --> 00:01:17,559
But what does Project Cata-Vento say?

23
00:01:18,440 --> 00:01:21,960
<b>First, it acknowledges that the risk exists.</b>

24
00:01:22,679 --> 00:01:24,679
And then comes a series of things

25
00:01:24,679 --> 00:01:27,920
that I wouldn't be able to
go into too much detail,

26
00:01:27,920 --> 00:01:29,760
but I'll just give you a few examples,

27
00:01:29,760 --> 00:01:31,960
a few brushstrokes,

28
00:01:31,960 --> 00:01:36,400
and see if you'll be amazed or not.

29
00:01:37,400 --> 00:01:38,400
<b>The documents stated:</b>

30
00:01:39,440 --> 00:01:42,079
If the accident takes place

31
00:01:42,079 --> 00:01:44,639
during a match at the Rei Pelé stadium...

32
00:01:47,119 --> 00:01:48,719
<b>Just picture it.</b>

33
00:01:50,679 --> 00:01:53,280
<b>Okay, what should you do?</b>

34
00:01:54,800 --> 00:01:56,920
<b>A calm voice</b>

35
00:01:57,960 --> 00:02:01,119
<b>should broadcast over the speaker:</b>

36
00:02:02,039 --> 00:02:03,239
<b>It happened!</b>

37
00:02:04,559 --> 00:02:06,840
<b>But there's no reason to panic.</b>

38
00:02:08,199 --> 00:02:10,760
<b>Chlorine</b>

39
00:02:10,760 --> 00:02:14,920
is highly manageable

40
00:02:14,920 --> 00:02:16,679
by using water.

41
00:02:17,480 --> 00:02:19,360
So take a handkerchief,

42
00:02:19,360 --> 00:02:21,679
(guys, it's in there!)

43
00:02:22,480 --> 00:02:25,320
wet it and cover your nose.

44
00:02:25,320 --> 00:02:29,159
Everybody must be evacuated to CEPA.

45
00:02:30,480 --> 00:02:32,920
Guys, please look into Operação Cata-Vento.

46
00:02:32,920 --> 00:02:35,159
Please find it.


1
00:00:00,639 --> 00:00:03,400
My name is Abel Galindo,

2
00:00:03,400 --> 00:00:05,679
also known as Professor Abel Galindo,

3
00:00:05,679 --> 00:00:07,920
but my name is Abel Galindo Marques.

4
00:00:08,599 --> 00:00:11,039
I am originally a civil engineer.

5
00:00:12,239 --> 00:00:17,239
But later I graduated in ​​geotechnics

6
00:00:18,119 --> 00:00:20,519
with knowledge in geology.

7
00:00:20,519 --> 00:00:25,880
I was a Professor at UFAL for 38 years.

8
00:00:26,559 --> 00:00:28,480
And among the subjects I taught,

9
00:00:28,480 --> 00:00:30,199
especially in the last 5 years,

10
00:00:30,199 --> 00:00:33,760
was engineering geology.

11
00:00:34,400 --> 00:00:36,639
So this is my background,

12
00:00:36,639 --> 00:00:38,480
specialized in

13
00:00:38,480 --> 00:00:40,480
civil engineering

14
00:00:40,480 --> 00:00:43,320
with graduate, master's degree, etc.,

15
00:00:43,920 --> 00:00:47,920
in ​geotechnics with geology,

16
00:00:47,920 --> 00:00:51,159
as a geotechnical engineer.

17
00:00:51,159 --> 00:00:53,800
I was fortunate to be the first

18
00:00:54,400 --> 00:00:57,760
to graduate here in Alagoas.

19
00:00:57,760 --> 00:00:59,719
That was in '77.

20
00:00:59,719 --> 00:01:01,800
Actually, I started

21
00:01:02,760 --> 00:01:04,639
my graduate studies in '75.

22
00:01:04,639 --> 00:01:06,960
So I was lucky to

23
00:01:06,960 --> 00:01:09,559
get Maceió

24
00:01:09,559 --> 00:01:12,960
with very few buildings,

25
00:01:14,719 --> 00:01:17,000
vertical ones,

26
00:01:17,000 --> 00:01:20,039
buildings with 8, 10, 12 floors

27
00:01:20,039 --> 00:01:20,800
were very few.

28
00:01:20,800 --> 00:01:25,039
There were about 3 or 4 there on the beach, right?

29
00:01:25,039 --> 00:01:27,599
And there was one here, in Farol,

30
00:01:28,679 --> 00:01:29,840
so there were very few buildings.

31
00:01:29,840 --> 00:01:32,360
And since I am specialized

32
00:01:33,239 --> 00:01:35,400
in geotechnics...

33
00:01:35,400 --> 00:01:38,920
Geotechnics covers ​​foundations 

34
00:01:38,920 --> 00:01:42,800
for buildings, bridges, dams,

35
00:01:42,800 --> 00:01:45,480
the stability of slopes,

36
00:01:45,480 --> 00:01:47,400
that's what ​​geotechnics is.

37
00:01:47,400 --> 00:01:49,960
But to be a good geotechnician

38
00:01:49,960 --> 00:01:51,679
you need to have a good grasp,

39
00:01:51,679 --> 00:01:53,599
a great grasp of geology.

40
00:01:54,440 --> 00:01:55,039
Geology.

41
00:01:55,039 --> 00:01:58,159
Because besides teaching about

42
00:01:58,159 --> 00:02:00,719
foundations, stability, etc.

43
00:02:00,719 --> 00:02:03,480
in the engineering course at UFAL,

44
00:02:03,480 --> 00:02:06,000
I also taught, for many years,

45
00:02:06,000 --> 00:02:07,159
engineering geology

46
00:02:07,159 --> 00:02:09,639
because of my parallel knowledge, you know?

47
00:02:10,039 --> 00:02:11,880
Rumor has it, and it is true,

48
00:02:12,760 --> 00:02:14,639
that 95%

49
00:02:14,639 --> 00:02:19,079
of all buildings in Maceió

50
00:02:21,480 --> 00:02:24,400
<b>are designed by Mr. Abel Galindo.</b>


1
00:00:00,333 --> 00:00:04,254
I think all three are valid.

2
00:00:05,088 --> 00:00:08,258
One doesn't cancel out the other,
because it's a tragedy,

3
00:00:08,258 --> 00:00:10,844
because it's... lives that...

4
00:00:12,095 --> 00:00:14,431
bonds that were lost,
lives that were lost,

5
00:00:14,431 --> 00:00:17,642
there are already cases
of lives lost because of this.

6
00:00:17,851 --> 00:00:23,440
You have a lack of monitoring

7
00:00:23,440 --> 00:00:26,943
and control, which has allowed
this situation to occur.

8
00:00:27,402 --> 00:00:29,904
We can typify it.

9
00:00:30,530 --> 00:00:33,575
I don't know if you're going to
discuss legal matters with an expert,

10
00:00:34,075 --> 00:00:37,245
but these issues I raised,

11
00:00:37,245 --> 00:00:39,956
that you need, and mainly

12
00:00:40,957 --> 00:00:45,378
because the allegation was that it is...

13
00:00:46,046 --> 00:00:49,799
Let's say the operation...
No, let's say it started prior to

14
00:00:51,009 --> 00:00:53,470
the Constitution of 1988,

15
00:00:53,470 --> 00:00:56,514
of a series of regulations
that will guide, for example,

16
00:00:56,514 --> 00:00:58,391
environmental licensing, etc.

17
00:00:58,391 --> 00:00:59,809
But that is no excuse.

18
00:01:00,685 --> 00:01:04,189
that is no excuse, because the
economic activity continues to exist.

19
00:01:04,773 --> 00:01:06,900
And you need to adapt.

20
00:01:06,900 --> 00:01:08,526
I think that's where

21
00:01:09,694 --> 00:01:15,116
Braskem's legal counsel sought
to make the agreements.

22
00:01:16,076 --> 00:01:18,536
Because they claimed that "look,
we should've done this and that,

23
00:01:18,536 --> 00:01:21,456
"and the Environmental Impact Report...

24
00:01:21,456 --> 00:01:24,876
"we had several activities, so it was
possible to have more than one license."

25
00:01:25,627 --> 00:01:30,507
Even if you are generous
in reducing bureaucracy,

26
00:01:31,466 --> 00:01:34,177
as an activity

27
00:01:34,677 --> 00:01:39,724
it doesn't matter if you have
3, 4, 5, 6, 7 extraction points. It doesn't matter,

28
00:01:40,350 --> 00:01:42,769
you have to have an
environmental impact report

29
00:01:43,561 --> 00:01:45,939
regarding the extraction,

30
00:01:46,397 --> 00:01:48,983
clearly identifying the points.

31
00:01:48,983 --> 00:01:53,279
Now what happens?
You don't have a document

32
00:01:53,279 --> 00:01:57,075
and especially a summons
after the risk

33
00:01:57,408 --> 00:02:01,121
begins to be identified,
to talk to the population.

34
00:02:01,412 --> 00:02:05,625
So, I don't think I'm responsible
for the law

35
00:02:05,667 --> 00:02:08,795
but I do see a crime point of view
and a disaster, because

36
00:02:08,795 --> 00:02:13,383
as I said, it's not just there
in that area directly affected,

37
00:02:14,217 --> 00:02:17,178
you also have areas that will be affected

38
00:02:17,178 --> 00:02:21,182
by direct influence and that will affect.
It's the rent that will affect

39
00:02:22,392 --> 00:02:22,976
lives.

40
00:02:22,976 --> 00:02:26,354
As I said, people will develop

41
00:02:26,354 --> 00:02:29,607
anxiety or even

42
00:02:29,899 --> 00:02:32,652
pathologies that did not exist.

43
00:02:33,111 --> 00:02:36,114
So you really have hints of
a humanitarian disaster.

44
00:02:37,448 --> 00:02:40,368
So I think these three,
 these three categories

45
00:02:40,368 --> 00:02:44,038
add to the perversity of the situation.


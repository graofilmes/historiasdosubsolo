1
00:00:00,667 --> 00:00:02,669
I don't want these places to recover,

2
00:00:02,669 --> 00:00:03,795
quite honestly,

3
00:00:03,795 --> 00:00:07,841
I want people to be able to recover

4
00:00:08,591 --> 00:00:12,053
because these are risk areas.

5
00:00:13,263 --> 00:00:16,057
Then you have ratings...
This technique

6
00:00:16,933 --> 00:00:18,685
we use, a kernel one,

7
00:00:18,685 --> 00:00:21,354
that we separate the areas by color.

8
00:00:21,354 --> 00:00:24,774
So for example:
the redder, the riskier, etc.

9
00:00:25,608 --> 00:00:28,778
It is an area that cannot have a purpose

10
00:00:29,654 --> 00:00:33,033
as that space was historically used.

11
00:00:35,076 --> 00:00:39,330
The only thing I'd consider
more appropriate

12
00:00:39,414 --> 00:00:41,458
is to really transform that area.

13
00:00:42,333 --> 00:00:45,920
All this area affected in a green forest area.

14
00:00:47,005 --> 00:00:47,881
So the creation of...

15
00:00:48,548 --> 00:00:51,634
a large green area
that will improve the quality

16
00:00:51,634 --> 00:00:54,679
of the microclimate in the region

17
00:00:54,679 --> 00:00:56,431
even in the city,

18
00:00:58,558 --> 00:01:01,352
at least at first.

19
00:01:01,352 --> 00:01:05,065
And so I'm working here, I'm talking to you
with a precautionary principle,

20
00:01:06,191 --> 00:01:09,819
which is that old saying:
snake-bitten dogs are afraid of sausages.

21
00:01:10,070 --> 00:01:11,780
So you can't,

22
00:01:11,780 --> 00:01:15,575
from an urban planning point of view,
destinate

23
00:01:15,909 --> 00:01:19,871
areas that were considered as
areas of risk for housing again,

24
00:01:19,871 --> 00:01:23,041
such as a landfill
which we find in Brazil.

25
00:01:23,583 --> 00:01:26,586
the landfill is deactivated,
so people build on top of it.

26
00:01:26,920 --> 00:01:28,797
Suddenly, an explosion takes place.

27
00:01:29,339 --> 00:01:30,715
Now what I believe

28
00:01:30,715 --> 00:01:32,467
is missing

29
00:01:34,344 --> 00:01:35,929
is more dialogue,

30
00:01:35,929 --> 00:01:38,598
mainly listening in a community way,

31
00:01:39,307 --> 00:01:41,267
because listening individually,

32
00:01:41,643 --> 00:01:44,479
while doing it in a community,
is what these communities

33
00:01:44,854 --> 00:01:47,690
would like from the spaces.

34
00:01:48,066 --> 00:01:53,530
Because one of the darkest
social impacts of mining

35
00:01:53,571 --> 00:01:56,282
is the breaking of ties

36
00:01:56,324 --> 00:01:57,951
between people.

37
00:01:58,201 --> 00:02:00,120
There are people who lived 30 years

38
00:02:00,120 --> 00:02:06,417
and know the life of every neighbor,
you know that the neighbor over there arrived on Friday,

39
00:02:06,459 --> 00:02:09,504
you get a beer in front of the house,
talking with friends...

40
00:02:09,504 --> 00:02:12,132
All these ties are broken,

41
00:02:12,465 --> 00:02:15,760
especially when we start
to scale age.

42
00:02:16,177 --> 00:02:19,764
So from a certain an age,
the impact is even bigger.

43
00:02:20,807 --> 00:02:21,391
Then,

44
00:02:22,308 --> 00:02:25,019
they miss that house, miss the street
they are familiar with.

45
00:02:25,770 --> 00:02:27,897
I think the only way to reduce it,

46
00:02:27,897 --> 00:02:30,191
because the impact is already severe,

47
00:02:31,651 --> 00:02:34,696
but I have the impression that
the only way you can reduce it,

48
00:02:34,696 --> 00:02:37,323
is through group meetings

49
00:02:37,323 --> 00:02:40,869
with the community, where the community
comes to say what they'd like.

50
00:02:41,202 --> 00:02:43,830
But unfortunately that's not what happened.

51
00:02:43,830 --> 00:02:48,960
Compensations are individual,
so you get paid for your house,

52
00:02:49,419 --> 00:02:53,548
and you can buy a house there in...
I don't know, in Graça Torta,

53
00:02:54,507 --> 00:02:57,093
or you will buy one at Biu,

54
00:02:57,093 --> 00:02:59,846
or you'll buy one I don't know where.

55
00:03:00,305 --> 00:03:02,473
And then it breaks.

56
00:03:02,473 --> 00:03:06,561
But I think that
when I do an imagination exercise,

57
00:03:07,020 --> 00:03:10,273
when I imagine that space as one

58
00:03:11,232 --> 00:03:16,279
in some areas, even recreational,
that is, with access to the population

59
00:03:16,613 --> 00:03:20,742
being able to enjoy those spaces,
in some, definitely not in all.

60
00:03:21,534 --> 00:03:24,245
But I can't imagine

61
00:03:24,245 --> 00:03:26,789
making a projection from the point of view

62
00:03:27,582 --> 00:03:31,628
of the impacted because of the following:

63
00:03:33,671 --> 00:03:35,256
People use it much worse than I do,

64
00:03:35,256 --> 00:03:38,635
for example, I'm not from anthropology,
sociology, I don't know,

65
00:03:38,635 --> 00:03:39,886
there's this "place of speech" thing.

66
00:03:40,511 --> 00:03:44,307
I can't imagine it myself

67
00:03:44,974 --> 00:03:48,561
because I'm not directly hit.

68
00:03:50,396 --> 00:03:52,982
And not even direct influence, I still consider

69
00:03:54,192 --> 00:03:55,318
it to be indirect.

70
00:03:56,027 --> 00:03:58,821
So I think this exercise
on my part,

71
00:03:58,821 --> 00:04:02,283
as a researcher and even more so
with the technical area etc.,

72
00:04:02,450 --> 00:04:05,245
I think in practice it has to
come from a place of solidarity,

73
00:04:05,578 --> 00:04:08,623
mainly to show that it's worth investing

74
00:04:11,793 --> 00:04:15,421
in something more communal, to decide
for example the priorities,

75
00:04:16,047 --> 00:04:19,133
because the priorities
need to be individual.

76
00:04:19,467 --> 00:04:22,220
Each and every one has their own characteristics,

77
00:04:22,428 --> 00:04:26,015
but when you reduce or even eliminate

78
00:04:26,224 --> 00:04:28,601
this belonging,

79
00:04:28,601 --> 00:04:33,314
that feeling of belonging to that church
you attended for so many years,

80
00:04:34,274 --> 00:04:36,651
where your daughter was baptized...

81
00:04:36,651 --> 00:04:40,405
Or an Umbanda meeting you attended
with your mother

82
00:04:40,989 --> 00:04:44,534
who is no longer alive
and you want to keep her legacy... It disappeared!

83
00:04:45,118 --> 00:04:49,497
So it's a series of spaces that are
places of coexistence,

84
00:04:49,497 --> 00:04:52,500
of creating emotional bonds, which disappear.

85
00:04:52,500 --> 00:04:54,252
You can only minimize it,

86
00:04:54,711 --> 00:04:56,170
not recover,

87
00:04:56,671 --> 00:04:58,965
if you work in a community way,

88
00:04:59,215 --> 00:05:03,094
not putting everyone
necessarily together.

89
00:05:03,678 --> 00:05:05,471
But you, it's like you...

90
00:05:06,389 --> 00:05:09,809
Identify, and it's not that difficult, you know?
This is what makes me most uncomfortable.

91
00:05:10,143 --> 00:05:13,396
You recommend the groups
and work with them.

92
00:05:13,396 --> 00:05:18,735
It will cost them? They made such an impact
and there was a surplus these past years.

93
00:05:18,943 --> 00:05:20,945
Profit. In other words, it made a profit.
It ended up

94
00:05:21,279 --> 00:05:23,406
taking the revenue and expenses,

95
00:05:23,573 --> 00:05:25,700
an investment like this is low.

96
00:05:25,700 --> 00:05:28,369
But it's not done.
Because it's not the mining standard.

97
00:05:28,870 --> 00:05:33,041
You do this type of
community orchestration

98
00:05:33,708 --> 00:05:36,836
because the more you share,

99
00:05:36,836 --> 00:05:39,380
the more you potentialize conflicts of interest.

100
00:05:40,214 --> 00:05:42,592
And then who benefits in the end

101
00:05:43,301 --> 00:05:46,971
and here in Maceió,
and it's been years and years that I hear this,

102
00:05:47,638 --> 00:05:51,684
are the urban readjustment plans
for these areas.

103
00:05:53,478 --> 00:05:55,730
It's an opportunity.

104
00:05:55,938 --> 00:05:58,691
I won't say that everyone, for example,
who has an idea

105
00:05:58,691 --> 00:06:02,695
such as this
is malicious just to profit,

106
00:06:04,072 --> 00:06:06,115
but the road to hell is paved with good intentions.

107
00:06:08,284 --> 00:06:09,911
And then if you...

108
00:06:09,911 --> 00:06:12,663
because if the person says like:
Diego, I don't work with risk assessment.

109
00:06:14,040 --> 00:06:17,752
"What I want is the development
over there in that area etc.",

110
00:06:17,752 --> 00:06:21,381
I say: look, we're talking
with different parameters.

111
00:06:22,215 --> 00:06:24,842
Because before talking about development,
I'll think about risk.

112
00:06:26,803 --> 00:06:28,763
It's like putting a...

113
00:06:29,013 --> 00:06:33,226
a building next door,
for example a mall etc.

114
00:06:34,060 --> 00:06:36,437
As long as the landfill is deactivated,
that area over there...

115
00:06:37,105 --> 00:06:38,398
Could be a risk area.

116
00:06:39,357 --> 00:06:40,525
In this case

117
00:06:41,275 --> 00:06:43,277
we're talking about soil subsidence.

118
00:06:44,946 --> 00:06:47,615
We're talking about the risk, for example...

119
00:06:48,408 --> 00:06:50,410
For example, a simple thing

120
00:06:50,410 --> 00:06:52,578
for the person watching the documentary to understand:

121
00:06:52,578 --> 00:06:54,831
it's a wall coming down on you.

122
00:06:57,250 --> 00:06:58,501
Ah, Diego, you're being too drastic!

123
00:06:58,501 --> 00:07:02,797
But the researcher's duty in the
Environmental Impact Assessment, EIA,

124
00:07:02,797 --> 00:07:05,550
as I told you, is being dramatic!

125
00:07:05,925 --> 00:07:09,095
It is not? Dramatic...
That's why you have to have a...

126
00:07:09,095 --> 00:07:10,763
That's why the team is multidisciplinary,

127
00:07:11,013 --> 00:07:14,892
because there has to be someone more optimistic
on the team to balance it out.

128
00:07:15,643 --> 00:07:19,605
But from an "affected areas" point of view,

129
00:07:19,981 --> 00:07:23,860
yes there are areas
in which the risk is low.

130
00:07:24,694 --> 00:07:28,739
But you have to work
at the probability level

131
00:07:28,739 --> 00:07:32,118
when you begin to redesign
the urban space.

132
00:07:32,660 --> 00:07:35,496
You have to project this
for 20, 30 years.

133
00:07:36,372 --> 00:07:39,792
And do you really think these areas
will not grow on top of risk areas?

134
00:07:40,668 --> 00:07:42,670
That's a point.
Also...

135
00:07:44,297 --> 00:07:47,925
The technical guarantee issued
by the Brazilian Geological Survey

136
00:07:47,925 --> 00:07:51,137
made it very clear that those areas
shouldn't be busy anymore.

137
00:07:52,388 --> 00:07:54,056
For example, these areas

138
00:07:54,056 --> 00:07:57,185
that were of low risk
could rise to medium risk.

139
00:07:58,769 --> 00:07:59,228
So,

140
00:08:00,271 --> 00:08:03,232
at the end of the day, that's it:
usually the word that comes up

141
00:08:03,900 --> 00:08:08,321
to try... We'll not get into

142
00:08:08,988 --> 00:08:11,157
the word "development" there,

143
00:08:11,407 --> 00:08:13,618
which is a super interesting concept,

144
00:08:14,869 --> 00:08:19,207
since it becomes poorly used,
and generally with the development

145
00:08:19,207 --> 00:08:23,002
for whom, right?
We have to ask that.


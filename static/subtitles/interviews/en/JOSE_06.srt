1
00:00:00,119 --> 00:00:01,920
I've always demanded,

2
00:00:01,920 --> 00:00:05,119
but I haven't just demanded licensing, Rafael.

3
00:00:05,800 --> 00:00:09,920
I have publicly demanded this, always.

4
00:00:09,920 --> 00:00:13,519
I don't want to see the licensing only.

5
00:00:14,280 --> 00:00:18,840
I want to see who signed it.

6
00:00:19,639 --> 00:00:22,320
Because that's where the crime starts.

7
00:00:22,320 --> 00:00:24,440
There was no law,

8
00:00:24,440 --> 00:00:27,599
no environmental legislation in the state of Alagoas.

9
00:00:27,599 --> 00:00:31,559
Environmental legislation was implemented

10
00:00:31,559 --> 00:00:33,159
during my administration

11
00:00:33,159 --> 00:00:38,360
and really progressed in Zé Roberto's legislation.

12
00:00:39,039 --> 00:00:41,760
<b>And then came the constituent.</b>

13
00:00:42,400 --> 00:00:46,440
And the Constitution of the state of Alagoas

14
00:00:46,440 --> 00:00:49,440
now contemplated the environmental axis.

15
00:00:50,079 --> 00:00:53,559
But it's something few people know about.

16
00:00:53,559 --> 00:00:56,519
Check out the Constitution,

17
00:00:56,519 --> 00:01:00,599
it's very interesting.

18
00:01:01,400 --> 00:01:02,559
<b>It's a copy</b>

19
00:01:03,360 --> 00:01:05,679
<b>of the Constitution of Sergipe,</b>

20
00:01:07,199 --> 00:01:09,239
which in turn has misconceptions.

21
00:01:09,239 --> 00:01:12,079
Take it and see, it's fun.

22
00:01:13,639 --> 00:01:18,800
The state of Alagoas has to protect the oceanic islands.

23
00:01:20,079 --> 00:01:24,960
Alagoas never had an oceanic island, guys!

24
00:01:24,960 --> 00:01:28,199
Then came the mangrove

25
00:01:28,199 --> 00:01:31,679
protective legislation,

26
00:01:32,800 --> 00:01:35,199
which composes the legislature

27
00:01:35,199 --> 00:01:39,360
as a conservation bill.

28
00:01:39,960 --> 00:01:43,679
Which means it's possible to use the mangrove

29
00:01:43,679 --> 00:01:45,880
in a rational way,

30
00:01:45,880 --> 00:01:50,760
even artisanal fishermen need it.

31
00:01:50,760 --> 00:01:53,440
You just have to train them for it.

32
00:01:53,440 --> 00:01:56,719
But Deputy Ismael Pereira

33
00:01:56,719 --> 00:02:00,000
was passionate about

34
00:02:00,000 --> 00:02:02,400
defending the environment,

35
00:02:02,400 --> 00:02:05,559
so during that session

36
00:02:05,559 --> 00:02:09,840
the law entered conservationist and left preservationist.

37
00:02:09,960 --> 00:02:12,679
The law for the protection of mangroves in Alagoas

38
00:02:12,679 --> 00:02:14,960
is preservationist.

39
00:02:14,960 --> 00:02:19,119
The mangroves are untouchable.

40
00:02:20,280 --> 00:02:21,599
<b>It never worked.</b>

41
00:02:22,239 --> 00:02:24,559
<b>Right? Never worked.</b>

42
00:02:25,159 --> 00:02:29,239
In fact, in the recent case with Braskem,

43
00:02:29,239 --> 00:02:32,920
Braskem dug mines in mangrove areas.

44
00:02:32,920 --> 00:02:37,400
It destroyed mangroves in Bebedouro.

45
00:02:38,519 --> 00:02:39,840
And nothing was done about it.

46
00:02:39,840 --> 00:02:42,920
<b>Which is illegal.</b>

47
00:02:43,119 --> 00:02:46,440
It happened very close to the Environmental Secretariat

48
00:02:46,440 --> 00:02:47,599
of IMA.

49
00:02:48,239 --> 00:02:51,199
So the institute claimed they had no idea,

50
00:02:51,199 --> 00:02:53,320
but hey, it's pretty close.

51
00:02:53,320 --> 00:02:57,920
I had drone pictures from my house.

52
00:02:57,920 --> 00:03:00,719
The drone was my son, João Maurício. 

53
00:03:01,920 --> 00:03:03,360
He'd go up on the roof

54
00:03:03,360 --> 00:03:05,440
and my house was up there, in Pinheiro,

55
00:03:05,440 --> 00:03:07,239
and he would photograph

56
00:03:07,239 --> 00:03:09,960
the destruction of mangroves by Braskem.

57
00:03:10,719 --> 00:03:13,760
I spoke at the City Council

58
00:03:13,760 --> 00:03:19,119
and, from then on, IMA decided to inspect.

59
00:03:19,800 --> 00:03:21,320
<b>They acknowledged</b>

60
00:03:22,599 --> 00:03:24,079
and claimed it's because

61
00:03:24,079 --> 00:03:27,400
Braskem had sent them a project

62
00:03:27,400 --> 00:03:30,039
and executed another.

63
00:03:30,920 --> 00:03:32,320
<b>And imposed a fine.</b>

64
00:03:32,960 --> 00:03:34,599
<b>R$ 45,000.</b>


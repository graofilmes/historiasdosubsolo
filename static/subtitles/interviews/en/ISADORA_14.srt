1
00:00:00,320 --> 00:00:01,440
<b>The master plan</b>

2
00:00:01,440 --> 00:00:04,039
<b>not yet being delivered</b>

3
00:00:04,039 --> 00:00:05,280
<b>to the City Council</b>

4
00:00:05,280 --> 00:00:10,119
<b>means that nothing regarding Braskem</b>

5
00:00:10,119 --> 00:00:13,960
<b>was included in the review, as far as we know.</b>

6
00:00:15,480 --> 00:00:16,280
<b>Nothing!</b>

7
00:00:16,280 --> 00:00:21,039
<b>I may even be talking nonsense here</b>

8
00:00:21,039 --> 00:00:22,760
<b>and without us knowing</b>

9
00:00:22,760 --> 00:00:29,559
<b>the city hall has inserted this into the bill.</b>

10
00:00:29,559 --> 00:00:31,239
<b>Because we don't know!</b>

11
00:00:32,440 --> 00:00:35,119
<b>Since 2018, we don't know</b>

12
00:00:35,119 --> 00:00:38,320
<b>what was done in the bill.</b>

13
00:00:38,320 --> 00:00:41,760
<b>Look, 2018 is the year when</b>

14
00:00:41,760 --> 00:00:45,480
<b>the whole subsidence issue came public.</b>

15
00:00:48,159 --> 00:00:50,360
<b>So that's exactly the point,</b>

16
00:00:50,360 --> 00:00:51,960
<b>it's exactly the year</b>

17
00:00:51,960 --> 00:00:55,760
<b>in which we lost contact with the review</b>

18
00:00:55,760 --> 00:00:56,519
<b>in practice,</b>

19
00:00:56,519 --> 00:00:58,599
<b>with what was elaborated</b>

20
00:00:58,599 --> 00:01:00,559
<b>and built as a review.</b>

21
00:01:00,559 --> 00:01:02,360
<b>Since 2018,</b>

22
00:01:02,360 --> 00:01:04,159
<b>we don't know of anything that was added.</b>

23
00:01:05,400 --> 00:01:07,920
<b>We don't know both in written terms</b>

24
00:01:07,920 --> 00:01:11,079
<b>and in mapping terms.</b>

25
00:01:11,079 --> 00:01:14,440
<b>We don't know if the maps...</b>

26
00:01:14,440 --> 00:01:18,880
<b>Of course I believe they were added.</b>

27
00:01:18,880 --> 00:01:24,519
<b>I believe the mapping done by CPRM</b>

28
00:01:24,519 --> 00:01:28,559
<b>and all the new discoveries...</b>

29
00:01:28,559 --> 00:01:30,159
<b>That must have gone in.</b>

30
00:01:30,159 --> 00:01:32,000
<b>The city hall, the city hall technicians</b>

31
00:01:32,000 --> 00:01:32,880
<b>must have added it.</b>

32
00:01:32,880 --> 00:01:34,119
<b>They are competent technicians.</b>

33
00:01:34,119 --> 00:01:35,599
<b>But if you ask</b>

34
00:01:36,039 --> 00:01:36,920
<b>what do we know?</b>

35
00:01:36,920 --> 00:01:38,960
<b>And look, I was a member of the board</b>

36
00:01:38,960 --> 00:01:40,400
<b>that reviewed the master plan.</b>

37
00:01:41,079 --> 00:01:42,719
<b>I was with IAB</b>

38
00:01:42,719 --> 00:01:44,920
<b>and IDEAL, as an institution,</b>

39
00:01:44,920 --> 00:01:48,800
<b>held a captive seat on the review board.</b>

40
00:01:49,159 --> 00:01:50,440
<b>So in theory,</b>

41
00:01:50,440 --> 00:01:51,960
<b>us today as IDEAL...</b>

42
00:01:51,960 --> 00:01:54,840
<b>Since this board hasn't been...</b>

43
00:01:55,760 --> 00:01:57,360
<b>dismembered,</b>

44
00:01:57,360 --> 00:01:59,679
<b>it still is, in theory,</b>

45
00:01:59,679 --> 00:02:00,679
<b>a review board.</b>

46
00:02:00,679 --> 00:02:01,559
<b>So in theory, IDEAL</b>

47
00:02:01,559 --> 00:02:03,920
<b>still has a seat on the board.</b>

48
00:02:04,760 --> 00:02:06,920
<b>And since 2018,</b>

49
00:02:06,920 --> 00:02:09,039
<b>it's been 3 years, so</b>

50
00:02:09,039 --> 00:02:11,239
<b>2019, 2020 and 2021</b>

51
00:02:12,119 --> 00:02:14,920
<b>to this day, we don't know</b>

52
00:02:15,800 --> 00:02:16,960
<b>what was added</b>

53
00:02:16,960 --> 00:02:18,119
<b>and what wasn't?</b>

54
00:02:18,119 --> 00:02:20,760
<b>How is the wording of the bill going?</b>

55
00:02:20,760 --> 00:02:22,519
<b>What is settled in terms of follow-up?</b>

56
00:02:22,519 --> 00:02:26,639
<b>How was the Braskem case addressed?</b>

57
00:02:28,880 --> 00:02:30,599
<bWe don't know anything.</b>

58
00:02:30,599 --> 00:02:33,679
<b>But like, truly nothing, zero.</b>

59
00:02:33,679 --> 00:02:37,159
<b>And it's not just the review board.</b>

60
00:02:38,239 --> 00:02:40,760
<b>The review board is bigger than its name:</b>

61
00:02:40,760 --> 00:02:41,719
<b>"Review Board".</b>

62
00:02:42,199 --> 00:02:46,480
<b>The review board involves IDEAL,</b>

63
00:02:46,480 --> 00:02:48,599
<b>an institution such as IAB,</b>

64
00:02:48,599 --> 00:02:50,920
<b>but it involves the City Council, for example.</b>

65
00:02:50,920 --> 00:02:53,440
<b>So if the Review Board doesn't know anything,</b>

66
00:02:53,440 --> 00:02:55,039
<b>and if the bill never reached the Chamber,</b>

67
00:02:55,039 --> 00:02:58,000
<b>the Chamber doesn't know anything either, in theory.</b>

68
00:02:58,960 --> 00:03:00,039
<b>We have an institution</b>

69
00:03:00,039 --> 00:03:01,519
<b>such as the City Council,</b>

70
00:03:01,519 --> 00:03:03,920
<b>which is the highest body of the municipal</b>

71
00:03:03,920 --> 00:03:05,039
<b>legislature,</b>

72
00:03:05,239 --> 00:03:07,920
<b>knowing nothing about the bill,</b>

73
00:03:07,920 --> 00:03:08,920
<b>which is the biggest...</b>

74
00:03:08,920 --> 00:03:11,760
<b>Which governs the entire urban part of the city.</b>

75
00:03:11,760 --> 00:03:13,559
<b>Even if we didn't have</b>

76
00:03:13,559 --> 00:03:15,039
<b>an issue as serious</b>

77
00:03:15,039 --> 00:03:16,400
<b>as the Braskem case going on,</b>

78
00:03:16,400 --> 00:03:18,079
<b>it still wouldn't be justified.</b>

79
00:03:18,079 --> 00:03:20,199
<b>And considering the lastest events,</b>

80
00:03:20,199 --> 00:03:22,039
<b>there is no justification!</b>

81
00:03:22,320 --> 00:03:23,840
<b>With the pandemic outbreak in 2020,</b>

82
00:03:23,840 --> 00:03:27,920
<b>the Master Plan was already 5 years late.</b>

83
00:03:28,360 --> 00:03:30,159
<b>So it wasn't because of the pandemic!</b>

84
00:03:30,159 --> 00:03:32,400
<b>This delay had nothing to do with the pandemic.</b>


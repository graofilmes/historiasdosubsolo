1
00:00:00,079 --> 00:00:01,239
<b>It will depend a lot on us</b>

2
00:00:01,239 --> 00:00:03,599
<b>what these places will be like in 20 or 30 years.</b>

3
00:00:03,599 --> 00:00:05,800
<b>I can tell you my wish.</b>

4
00:00:06,920 --> 00:00:10,079
<b>What I wish these regions be like</b>

5
00:00:10,079 --> 00:00:12,039
<b>20, 30 or 40 from now.</b>

6
00:00:12,039 --> 00:00:14,559
<b>I want these regions,</b>

7
00:00:14,559 --> 00:00:16,840
<b>20, 30 or 40 years from now,</b>

8
00:00:16,840 --> 00:00:20,000
<b>to be regions that are once again occupied.</b>

9
00:00:20,000 --> 00:00:21,519
<b>Occupied by people,</b>

10
00:00:21,519 --> 00:00:24,679
<b>because its problems were solved</b>

11
00:00:26,800 --> 00:00:29,599
<b>both by human ingenuity,</b>

12
00:00:29,599 --> 00:00:30,880
<b>by finding possible</b>

13
00:00:30,880 --> 00:00:32,760
<b>engineering solutions</b>

14
00:00:32,760 --> 00:00:33,760
<b>for the phenomenon,</b>

15
00:00:33,760 --> 00:00:34,559
<b>there are some already,</b>

16
00:00:35,239 --> 00:00:37,039
<b>and by research</b>

17
00:00:37,039 --> 00:00:38,400
<b>on what can be done.</b>

18
00:00:38,400 --> 00:00:40,159
<b>Because I think this</b>

19
00:00:40,159 --> 00:00:41,519
<b>should've been done by now.</b>

20
00:00:42,280 --> 00:00:44,679
<b>There are solutions for some fenomena,</b>

21
00:00:44,679 --> 00:00:46,599
<b>but not for all of them.</b>

22
00:00:46,599 --> 00:00:48,360
<b>So, if there aren't known solutions</b>

23
00:00:48,360 --> 00:00:49,239
<b>to some fenomena,</b>

24
00:00:49,239 --> 00:00:50,599
<b>they should be searched.</b>

25
00:00:51,239 --> 00:00:53,159
<b>So, research areas should've</b>

26
00:00:53,159 --> 00:00:54,800
<b>already been opened by</b>

27
00:00:54,800 --> 00:00:56,639
<b>the federal government,</b>

28
00:00:57,360 --> 00:00:58,760
<b>like, by and large,</b>

29
00:00:58,760 --> 00:01:01,599
<b>to research those phenomena</b>

30
00:01:01,599 --> 00:01:04,400
<b>and solutions to solve them.</b>

31
00:01:04,800 --> 00:01:08,679
<b>So I wish 20, 30 or 40 years from now</b>

32
00:01:08,679 --> 00:01:11,639
<b>they've found solutions to these issues.</b>

33
00:01:12,039 --> 00:01:15,559
<b>The area can be occupied by people again,</b>

34
00:01:15,559 --> 00:01:17,920
<b>and when it's occupied by people again,</b>

35
00:01:17,920 --> 00:01:20,159
<b>that occupation can take place</b>

36
00:01:20,159 --> 00:01:21,360
<b>in a planned</b>

37
00:01:21,360 --> 00:01:23,480
<b>and sustainable manner.</b>

38
00:01:23,480 --> 00:01:26,760
<b>So, may we have neighborhoods</b>

39
00:01:27,559 --> 00:01:29,079
<b>in that place once again.</b>

40
00:01:29,079 --> 00:01:33,239
<b>And neighborhoods respecting physical and</b>

41
00:01:33,239 --> 00:01:34,480
<b>environmental characteristics,</b>

42
00:01:34,480 --> 00:01:37,559
<b>taking advantage of these characteristics,</b>

43
00:01:37,559 --> 00:01:42,239
<b>causing even better occupations to emerge,</b>

44
00:01:42,239 --> 00:01:45,280
<b>prettier, more alive, richer, </b>

45
00:01:45,280 --> 00:01:47,599
<b>more culturally thriving,</b>

46
00:01:47,599 --> 00:01:51,840
<b>because they took care of that place,</b>

47
00:01:51,840 --> 00:01:54,960
<b>took care of the environment,</b>

48
00:01:54,960 --> 00:01:56,320
<b>of the environmental characteristics,</b>

49
00:01:56,320 --> 00:01:58,159
<b>took care of the heritage,</b>

50
00:01:58,159 --> 00:02:00,119
<b>of the people there,</b>

51
00:02:00,119 --> 00:02:02,519
<b>and they've made</b>

52
00:02:02,519 --> 00:02:04,719
<b>the places happier with that.</b>

53
00:02:04,719 --> 00:02:06,000
<b>That's what I want for that place.</b>


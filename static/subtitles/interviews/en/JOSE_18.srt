1
00:00:00,519 --> 00:00:03,159
<b>For example, I have a friend</b>

2
00:00:04,760 --> 00:00:07,639
<b>who thinks differently than me.</b>

3
00:00:07,639 --> 00:00:10,639
<b>He even sent me a message saying:</b>

4
00:00:10,639 --> 00:00:13,000
<b>But Braskem</b>

5
00:00:13,000 --> 00:00:16,000
<b>is needed in Maceió.</b>

6
00:00:16,920 --> 00:00:18,960
<b>I replied: look, I consider you a friend,</b>

7
00:00:18,960 --> 00:00:20,480
<b>so I would like to hear</b>

8
00:00:20,480 --> 00:00:23,480
<b>from you:</b>

9
00:00:23,480 --> 00:00:26,679
<b>why is it necessary?</b>

10
00:00:27,159 --> 00:00:29,800
<b>He said: because Alagoas is a poor state,</b>

11
00:00:29,800 --> 00:00:32,320
<b>so we need it.</b>

12
00:00:33,079 --> 00:00:34,800
<b>Now, you tell me:/b>

13
00:00:35,480 --> 00:00:37,559
<b>for all these years</b>

14
00:00:37,559 --> 00:00:40,159
<b>of Salgema/Braskem,</b>

15
00:00:40,159 --> 00:00:44,400
<b>how did the state of Alagoas become less poor?</b>

16
00:00:46,760 --> 00:00:49,199
<b>And it was less poor</b>

17
00:00:49,199 --> 00:00:52,360
<b>because of Salgema and Braskem?</b>

18
00:00:52,360 --> 00:00:55,360
<b>They who have gone all out,</b>

19
00:00:55,360 --> 00:00:58,360
<b>ending a sandbank/b>

20
00:00:58,360 --> 00:01:02,320
<b>and leading Trapiche and Barra</b>

21
00:01:02,320 --> 00:01:05,320
<b>to immediate devaluation</b>

22
00:01:06,719 --> 00:01:10,280
<b>and to a dangerous neighborhood</b>

23
00:01:10,280 --> 00:01:12,440
<b>like Pontal da Barra?</b>

24
00:01:12,440 --> 00:01:14,320
<b>You see?</b>

25
00:01:14,320 --> 00:01:17,320
<b>So I'm not against development!</b>

26
00:01:17,320 --> 00:01:21,719
<b>The question is: what kind of development?</b>

27
00:01:24,199 --> 00:01:27,199
<b>That question you asked your friend</b>

28
00:01:27,199 --> 00:01:30,400
<b>is the big question we ask here.</b>

29
00:01:30,960 --> 00:01:33,159
<b>Where's this great development</b>

30
00:01:33,159 --> 00:01:35,800
<b>that Salgema promised for Alagoas?</b>

31
00:01:35,800 --> 00:01:38,400
<b>These jobs they claim</b>

32
00:01:38,400 --> 00:01:40,440
<b>to be our big win,</b>

33
00:01:40,440 --> 00:01:41,960
<b>are they worth it?</b>

34
00:01:41,960 --> 00:01:44,440
<b>Where are these resources?</b>

35
00:01:44,440 --> 00:01:46,719
<b>Look, Nina, this job issue,</b>

36
00:01:46,719 --> 00:01:49,360
<b>we didn't neglect it</b>

37
00:01:49,360 --> 00:01:51,119
<b>from the start.</b>

38
00:01:53,760 --> 00:01:57,719
<b>This was Salgema's greatest asset.</b>

39
00:01:58,119 --> 00:02:00,559
<b>We're going to generate,</b>

40
00:02:00,559 --> 00:02:02,840
<b>I don't know, there was a number...</b>

41
00:02:02,840 --> 00:02:05,719
<b>Don't know how many jobs...</b>

42
00:02:06,039 --> 00:02:08,480
<b>And that line caught a lot.</b>

43
00:02:09,400 --> 00:02:12,159
<b>But then,</b>

44
00:02:12,159 --> 00:02:16,440
<b>within the eco-development aspect, we were:</b>

45
00:02:16,440 --> 00:02:22,400
<b>Ok, how many jobs and how many unemployed will it generate?</b>

46
00:02:25,000 --> 00:02:25,880
<b>Why?</b>

47
00:02:27,239 --> 00:02:29,719
<b>How will it create</b>

48
00:02:29,719 --> 00:02:32,519
<b>permanent jobs?</b>

49
00:02:32,840 --> 00:02:34,119
<b>We know</b>

50
00:02:34,119 --> 00:02:38,360
<b>any implementation of this size</b>

51
00:02:38,360 --> 00:02:40,559
<b>immediately attracts</b>

52
00:02:40,559 --> 00:02:44,239
<b>a large number of people</b>

53
00:02:44,239 --> 00:02:47,760
<b>for jobs as construction workers</b>

54
00:02:49,679 --> 00:02:52,559
<b>in the implementation.</b>

55
00:02:52,559 --> 00:02:56,519
<b>On the hoe, on the shovel, whatever is necessary.</b>

56
00:02:57,480 --> 00:02:58,559
<b>It's inevitable.</b>

57
00:02:59,239 --> 00:03:02,239
<b>Now, that's what we get from implementation:</b>

58
00:03:03,039 --> 00:03:05,360
<b>underemployment.</b>

59
00:03:06,159 --> 00:03:08,280
<b>Now it's implemented</b>

60
00:03:08,280 --> 00:03:10,920
<b>and starting to work,</b>

61
00:03:10,920 --> 00:03:14,760
<b>it is highly automated, people!</b>

62
00:03:15,559 --> 00:03:18,039
<b>Fully automated!</b>

63
00:03:18,719 --> 00:03:20,519
<b>It relies on</b>

64
00:03:20,519 --> 00:03:23,280
<b>a relatively very small number</b>

65
00:03:23,280 --> 00:03:26,280
<b>of employees.</b>

66
00:03:27,440 --> 00:03:30,679
<b>We've warned people from the start.</b>

67
00:03:31,719 --> 00:03:34,719
<b>Not to mention the issue of tax breaks, right?</b>

68
00:03:34,719 --> 00:03:37,480
<b>That's right!</b>

69
00:03:37,480 --> 00:03:40,079
<b>It went years</b>

70
00:03:40,079 --> 00:03:43,079
<b>without paying a penny of tax.</b>

71
00:03:43,440 --> 00:03:45,800
<b>I think you know that this</b>

72
00:03:45,800 --> 00:03:51,559
<b>tax agreement was recently redone.</b>

73
00:03:52,320 --> 00:03:55,320
<b>But the secretary of finance,</b>

74
00:03:55,320 --> 00:03:57,280
<b>when questioned,</b>

75
00:03:57,280 --> 00:04:00,039
<b>said: yes, it was.</b>

76
00:04:00,880 --> 00:04:05,679
<b>So really the state makes very small profit.</b>

77
00:04:06,400 --> 00:04:09,079
<b>But I can't say,</b>

78
00:04:09,079 --> 00:04:11,679
<b>because there are secrecy clauses.</b>

79
00:04:13,400 --> 00:04:15,960
<b>By the way, this secrecy clause thing</b>

80
00:04:15,960 --> 00:04:21,079
<b>is one of Braskem's and Salgema's strengths.</b>

81
00:04:21,599 --> 00:04:23,480
<b>Especially Salgema's.</b>

82
00:04:23,480 --> 00:04:26,440
<b>Everytime we pushed for more,</b>

83
00:04:26,440 --> 00:04:29,320
<b>for reports</b>

84
00:04:29,320 --> 00:04:31,840
<b>or flowchart details,</b>

85
00:04:31,840 --> 00:04:34,159
<b>their reply was unshakable:</b>

86
00:04:34,159 --> 00:04:36,280
<b>This is protected</b>

87
00:04:36,280 --> 00:04:38,639
<b>by an industrial secrecy clause.</b>

88
00:04:38,960 --> 00:04:40,480
<b>And it was</b>

89
00:04:42,400 --> 00:04:45,400
<b>So who benefits from it? Who won?</b>

90
00:04:46,519 --> 00:04:47,840
<b>I don't know.</b>

91
00:04:47,840 --> 00:04:50,840
<b>I know some people did.</b>

92
00:04:52,000 --> 00:04:54,159
<b>But it's too soon to talk.</b>


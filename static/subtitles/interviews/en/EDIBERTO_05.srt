1
00:00:00,039 --> 00:00:02,280
<b>Maceió is a lagoon.</b>

two
00:00:02,280 --> 00:00:03,920
<b>It is a sandbank.</b>

3
00:00:05,039 --> 00:00:07,239
<b>What you need to understand is that:</b>

4
00:00:07,239 --> 00:00:09,880
<b>Can you picture Maceió 1000,</b>

5
00:00:09,880 --> 00:00:11,159
<b>2000 years ago?</b>

6
00:00:11,159 --> 00:00:14,840
<b>Maceió is the Mundaú river</b>

7
00:00:16,039 --> 00:00:17,039
<b>reaching the sea.</b>

8
00:00:17,920 --> 00:00:19,960
Between barriers.

9
00:00:19,960 --> 00:00:23,679
It comes along the Mundaú river bed,

10
00:00:23,679 --> 00:00:25,800
passes through Satuba and then

11
00:00:25,800 --> 00:00:29,840
the river puts its waters here in the Atlantic Ocean.

12
00:00:31,079 --> 00:00:32,079
Over time

13
00:00:32,079 --> 00:00:34,679
this river, it creates

14
00:00:34,679 --> 00:00:35,920
its mouth.

15
00:00:35,920 --> 00:00:37,280
It opens

16
00:00:37,280 --> 00:00:40,800
and breaks down barriers left and right.

17
00:00:41,960 --> 00:00:44,800
According to the water flow, it opens its mouth.

18
00:00:45,599 --> 00:00:47,239
But there is a moment when

19
00:00:47,239 --> 00:00:49,760
the volume of water that came from

20
00:00:49,760 --> 00:00:53,280
<b>the headwaters of all these rivers begins to decrease,</b>

21
00:00:55,159 --> 00:00:56,440
the tufted material

22
00:00:56,440 --> 00:00:58,760
that is brought sometime in winter

23
00:00:59,119 --> 00:01:00,400
<b>is now depositing itself.</b>

24
00:01:01,280 --> 00:01:03,280
So, as it settles down

25
00:01:03,280 --> 00:01:08,119
and the east wind brings the sand from the sea,

26
00:01:08,119 --> 00:01:09,920
<b>from the beach,</b>

27
00:01:09,920 --> 00:01:13,599
it consolidates into a mixture

28
00:01:13,599 --> 00:01:16,119
in the most coastal area.

29
00:01:16,119 --> 00:01:17,559
In the innermost area,

30
00:01:17,559 --> 00:01:20,400
<b>the clay of the barrier falls on top of the mud.</b>

31
00:01:21,199 --> 00:01:23,159
So this formation

32
00:01:23,159 --> 00:01:25,360
grows towards

33
00:01:25,360 --> 00:01:28,119
what is now Pontal da Barra

34
00:01:28,119 --> 00:01:30,159
<b>and closes the mouth,</b>

35
00:01:30,159 --> 00:01:31,119
<b>forming the lagoon.</b>

36
00:01:31,119 --> 00:01:33,800
So the lagoon is a closed mouth

37
00:01:33,800 --> 00:01:37,079
with a little vacancy leaving in Barra Nova now,

38
00:01:37,079 --> 00:01:39,199
<b>because the volume of water has decreased.</b>

39
00:01:40,239 --> 00:01:42,840
So this formation is

40
00:01:42,840 --> 00:01:46,719
near the coast, the dunes

41
00:01:46,719 --> 00:01:48,719
formed by the movement of sand.

42
00:01:48,719 --> 00:01:51,360
Those who know the engineering

43
00:01:51,360 --> 00:01:52,760
understand that this beach sand

44
00:01:52,760 --> 00:01:54,480
has excellent compression.

45
00:01:54,480 --> 00:01:57,239
So you see,

46
00:01:57,239 --> 00:02:00,199
you can find buildings in Sobral

47
00:02:01,000 --> 00:02:02,800
<b>because it has sand.</b>

48
00:02:03,360 --> 00:02:04,880
Now go to Ponta Grossa

49
00:02:04,880 --> 00:02:06,840
and look for a three-floor building.

50
00:02:08,480 --> 00:02:09,599
You know why there isn't any?

51
00:02:09,599 --> 00:02:10,800
It doesn't!

52
00:02:10,800 --> 00:02:13,159
None of you, I'm sure, realized this.

53
00:02:14,239 --> 00:02:16,039
<b>It's not difficult in Ponta Grossa</b>

54
00:02:16,039 --> 00:02:17,800
Vergel, Coreia...

55
00:02:17,800 --> 00:02:19,440
Because if you build it there, it sinks.

56
00:02:19,440 --> 00:02:20,639
It goes away.

57
00:02:21,679 --> 00:02:23,480
<b>When they were doing</b>

58
00:02:25,679 --> 00:02:27,079
<b>the expressway,</b>

59
00:02:27,079 --> 00:02:29,119
<b>the landfill over there, what's it called?</b>

60
00:02:29,920 --> 00:02:31,239
The road dike.

61
00:02:31,519 --> 00:02:33,239
It used to be a dyke, now the lagoon's invaded it.

62
00:02:33,480 --> 00:02:34,400
When they dug there,

63
00:02:34,400 --> 00:02:36,599
there was a day when a machine was coming down and sinking.

64
00:02:36,599 --> 00:02:38,360
<b>It hit the softest mud.</b>

65
00:02:38,840 --> 00:02:40,920
<b>They had to tow it with another machine.</b>

66
00:02:40,920 --> 00:02:42,400
But it kept sinking.

67
00:02:42,880 --> 00:02:44,199
If you dig

68
00:02:44,199 --> 00:02:45,239
in Ponta Grossa,

69
00:02:45,239 --> 00:02:47,079
if you dig a meter in winter,

70
00:02:47,079 --> 00:02:48,440
you get water!

71
00:02:48,440 --> 00:02:50,320
The pits

72
00:02:50,320 --> 00:02:52,000
stay forever

73
00:02:52,000 --> 00:02:54,199
<b>with half the water in Ponta Grossa.</b>

74
00:02:55,039 --> 00:02:57,360
I was a resident of Ponta Grossa for 30 years.

75
00:02:57,360 --> 00:02:59,599
So that region over there

76
00:02:59,599 --> 00:03:01,800
is the region of the tuffous material.

77
00:03:01,800 --> 00:03:04,239
And the most coastal region

78
00:03:04,239 --> 00:03:06,559
is the one that managed to compress better,

79
00:03:06,559 --> 00:03:08,320
<b>so you have the dunes.</b>

80
00:03:08,320 --> 00:03:10,440
Then Pontal da Barra and Trapiche

81
00:03:10,440 --> 00:03:14,639
were part of this more coastal area,

82
00:03:14,639 --> 00:03:16,440
but from the sand,

83
00:03:16,440 --> 00:03:19,360
so there predominated the coconut trees.

84
00:03:20,360 --> 00:03:22,480
The coconut trees went deep down there.

85
00:03:22,480 --> 00:03:25,000
Further inland, towards the lagoon,

86
00:03:25,000 --> 00:03:28,039
<b>you have this vegetation on the edge of the lagoon, right?</b>

87
00:03:28,800 --> 00:03:29,480
That...

88
00:03:31,079 --> 00:03:33,719
There are some almond trees, some things like that,

89
00:03:33,719 --> 00:03:37,039
but no, there were no large forests.

90
00:03:37,039 --> 00:03:39,239
There was never a big forest there,

91
00:03:39,239 --> 00:03:41,559
because the type of soil

92
00:03:41,559 --> 00:03:43,800
of the innermost part can't handle it

93
00:03:43,800 --> 00:03:47,280
<b>and the coastline is predominantly of coconut trees.</b>

94
00:03:48,239 --> 00:03:50,280
The street where I lived, Guaicurus,

95
00:03:50,280 --> 00:03:52,400
and Rua da Assembleia,

96
00:03:52,400 --> 00:03:57,360
in this period of 1967, 1968...

97
00:03:57,360 --> 00:04:00,400
<b>It was filled with sand from the dunes.</b>

98
00:04:00,400 --> 00:04:02,000
So these sands from the dunes served

99
00:04:02,000 --> 00:04:04,199
to many landfills and were withdrawn.

100
00:04:04,519 --> 00:04:06,760
The Pontal dunes remain.

101
00:04:07,320 --> 00:04:08,960
The dunes of Sobral

102
00:04:08,960 --> 00:04:11,960
<b>have all been deleted.</b>

103
00:04:12,239 --> 00:04:14,719
It is important to detect this formation

104
00:04:14,719 --> 00:04:18,559
from the source of the Mundaú River, breaking down barriers,

105
00:04:19,119 --> 00:04:21,239
because all that barrier of Bebedouro,

106
00:04:21,239 --> 00:04:22,920
of Mutange...

107
00:04:22,920 --> 00:04:25,400
Cambona, Mutange and Bebedouro.

108
00:04:26,239 --> 00:04:27,559
The head of that barrier

109
00:04:27,559 --> 00:04:29,559
has fallen several times, right?

110
00:04:29,559 --> 00:04:30,960
During the story, right?

111
00:04:30,960 --> 00:04:32,280
She was much more advanced.

112
00:04:32,280 --> 00:04:33,880
Imagine on the other side, by Coqueiro Seco

113
00:04:33,880 --> 00:04:34,920
and Santa Luzia do Norte.

114
00:04:35,119 --> 00:04:36,400
The one we really lost

115
00:04:36,400 --> 00:04:39,280
reached much closer to here.

116
00:04:39,800 --> 00:04:41,800
<b>Then it started distancing itself.</b>

117
00:04:43,920 --> 00:04:46,079
It's precisely because the river tends

118
00:04:46,079 --> 00:04:48,800
to catch the coastal current,

119
00:04:48,800 --> 00:04:51,920
it is North-South, going down.

120
00:04:51,920 --> 00:04:55,599
So, when the river goes out into the sea, it starts

121
00:04:55,599 --> 00:04:57,119
<b>being pulled to the right,</b>

122
00:04:57,119 --> 00:04:58,079
<b>always to the right.</b>

123
00:04:58,519 --> 00:04:59,480
Just so you have an idea,

124
00:04:59,480 --> 00:05:00,480
one of the first

125
00:05:00,480 --> 00:05:02,960
canals left after this...

126
00:05:04,639 --> 00:05:07,360
After this compaction of Maceió

127
00:05:07,360 --> 00:05:09,880
was the Levada canal that you are still seeing today.

128
00:05:09,880 --> 00:05:11,000
The remainders, right?

129
00:05:11,000 --> 00:05:12,239
Now it's a river with raw sewage.

130
00:05:12,679 --> 00:05:15,280
But that canal, it passed through Praça do Pirulito

131
00:05:15,280 --> 00:05:16,199
and went out to the beach.

132
00:05:17,480 --> 00:05:18,800
At the end of Dias Cabral.

133
00:05:19,320 --> 00:05:20,760
That was a canal

134
00:05:21,679 --> 00:05:22,480
now closed.

135
00:05:23,599 --> 00:05:25,800
It was gradually closed, they kept landing,

136
00:05:25,800 --> 00:05:29,239
<b>until the middle of the 19th century</b>

137
00:05:29,239 --> 00:05:31,159
<b>that was a puddle over there.</b>

138
00:05:31,840 --> 00:05:32,920
<b>Mud.</b>

139
00:05:33,519 --> 00:05:35,159
<b>And then they landed.</b>

140
00:05:36,079 --> 00:05:36,599
So...

141
00:05:37,519 --> 00:05:39,320
This formation of Maceió

142
00:05:39,320 --> 00:05:42,320
that makes a soil accommodation barrier fall...

143
00:05:42,760 --> 00:05:44,039
<b>When I made the post</b>

144
00:05:45,719 --> 00:05:48,360
about Salgema,

145
00:05:48,360 --> 00:05:51,519
a person from São Paulo said:

146
00:05:51,519 --> 00:05:54,400
I was a resident of Bebedouro,

147
00:05:54,400 --> 00:05:55,440
close to the bridge.

148
00:05:56,119 --> 00:05:58,920
And when I left in the 1960s or thereabouts,

149
00:05:58,920 --> 00:06:00,400
I don't remember exactly the date.

150
00:06:02,000 --> 00:06:05,599
My father's house was sinking and sinking in the mud

151
00:06:05,599 --> 00:06:08,440
and so we were rebuilding it upwards, right?

152
00:06:08,440 --> 00:06:10,519
As it happens in Brejal today!

153
00:06:10,519 --> 00:06:13,880
At Brejal this phenomenon remains,

154
00:06:13,880 --> 00:06:16,639
so this region below, it's all tuff.

155
00:06:17,239 --> 00:06:18,719
<b>So accommodation is easy.</b>

156
00:06:19,320 --> 00:06:20,280
<b>Any weight on top...</b>

157
00:06:20,280 --> 00:06:21,119
<b>You put...</b>

158
00:06:21,119 --> 00:06:22,599
<b>The road dyke has a weight,</b>

159
00:06:22,599 --> 00:06:24,400
<b>the road dyke when it sinks</b>

160
00:06:24,400 --> 00:06:25,519
<b>the sides go up.</b>

161
00:06:26,960 --> 00:06:28,519
<b>These are phenomena...</b>

162
00:06:29,400 --> 00:06:31,320
<b>That is, Maceió,</b>

163
00:06:31,320 --> 00:06:32,480
<b>a part of Maceio</b>

164
00:06:33,159 --> 00:06:34,159
floats.

165
00:06:34,159 --> 00:06:37,039
It's a raft on soft mud.

166
00:06:37,639 --> 00:06:39,719
Picture something close to this.

167
00:06:39,719 --> 00:06:41,480
That part of towards the lagoon,

168
00:06:41,480 --> 00:06:43,199
<b>the part more towards the sea, no.</b>

169
00:06:43,199 --> 00:06:45,039
<b>This one is much more consolidated.</b>

170
00:06:45,679 --> 00:06:48,079
So understanding these movements

171
00:06:48,079 --> 00:06:51,559
helps to understand some earlier phenomena

172
00:06:51,559 --> 00:06:54,119
around that barrier over there

173
00:06:54,119 --> 00:06:56,480
from Mutange, from Cambona...

174
00:06:57,480 --> 00:06:59,079
It naturally,

175
00:06:59,079 --> 00:07:02,800
if there were no caves, no rock salt,

176
00:07:02,800 --> 00:07:04,559
with the process of erosion...

177
00:07:04,559 --> 00:07:06,199
For example: Gruta do Padre.

178
00:07:06,199 --> 00:07:09,039
Seniors say that

179
00:07:09,039 --> 00:07:11,119
there, close to Caldinho do Vieira,

180
00:07:11,119 --> 00:07:12,559
there was a pond in winter,

181
00:07:12,559 --> 00:07:14,760
it formed a pond

182
00:07:14,760 --> 00:07:17,000
and it went down towards CEPA.

183
00:07:17,000 --> 00:07:19,920
From CEPA, it descended towards the barrier

184
00:07:19,920 --> 00:07:21,639
and broke through the barrier,

185
00:07:21,639 --> 00:07:22,920
forming the Gruta do Padre.

186
00:07:22,920 --> 00:07:24,920
That was a movement of winter waters.

187
00:07:25,519 --> 00:07:27,039
<b>Can you imagine this water seeping in?</b>

188
00:07:27,719 --> 00:07:31,519
She takes the kaolin out of clay,

189
00:07:31,519 --> 00:07:33,000
which glues everything together,

190
00:07:33,000 --> 00:07:34,840
and turns into sand.

191
00:07:35,639 --> 00:07:38,000
Imagine thousands and thousands
of years.

192
00:07:38,000 --> 00:07:39,519
They are natural phenomena

193
00:07:39,519 --> 00:07:42,280
that take several centuries to happen.

194
00:07:42,760 --> 00:07:45,760
<b>But there are natural accommodations of this process.</b>

195
00:07:46,440 --> 00:07:49,800
So, if you already had that possibility,

196
00:07:49,800 --> 00:07:51,440
imagine with caves underneath.

197
00:07:52,079 --> 00:07:53,639
<b>Amazing idea, huh?</b>

198
00:07:53,960 --> 00:07:54,840
Dig and...

199
00:07:54,840 --> 00:07:57,760
And there is still a fault in a tectonic plate.

200
00:07:58,360 --> 00:07:59,679
So I think there...

201
00:07:59,679 --> 00:08:00,679
I'm not technical,

202
00:08:00,679 --> 00:08:02,840
but there are three phenomena at work.

203
00:08:03,760 --> 00:08:05,000
<b>The tectonic fault,</b>

204
00:08:05,360 --> 00:08:06,119
the caves.

205
00:08:06,519 --> 00:08:09,639
The caves and this barrier head

206
00:08:09,639 --> 00:08:11,159
that historically goes...

207
00:08:11,159 --> 00:08:13,119
The water tables are emerging

208
00:08:13,119 --> 00:08:14,960
on the side of the barrier

209
00:08:14,960 --> 00:08:16,479
and taking the material.

210
00:08:16,479 --> 00:08:18,439
Bebedouro had a value

211
00:08:18,439 --> 00:08:19,560
because it was a source of drinking water.

212
00:08:20,000 --> 00:08:22,399
The people of Maceió drank, for many years,

213
00:08:22,399 --> 00:08:23,479
many decades,

214
00:08:23,840 --> 00:08:26,920
the water that the carters went to fetch

215
00:08:26,920 --> 00:08:30,840
in the fountains that flowed there in Mutange, in Cambona.

216
00:08:32,159 --> 00:08:34,079
<b>It was a source of water </b>

217
00:08:34,079 --> 00:08:35,720
<b>in Bebedouro.</b>


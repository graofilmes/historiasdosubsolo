1
00:00:00,542 --> 00:00:02,836
Damages were estimated

2
00:00:05,005 --> 00:00:08,049
and they reached a certain amount.

3
00:00:08,717 --> 00:00:10,301
I don't know if it's because of

4
00:00:10,301 --> 00:00:13,847
my experience with

5
00:00:15,015 --> 00:00:17,642
​​mining impacts assessment...

6
00:00:20,228 --> 00:00:22,689
But I consider it a low amount,

7
00:00:23,148 --> 00:00:27,944
especially... "But Diego,
you keep insisting in this assessment...

8
00:00:28,361 --> 00:00:33,324
"impact prediction and whatnot.
Why does it matter?"

9
00:00:34,534 --> 00:00:36,828
Because when you use it later

10
00:00:36,828 --> 00:00:40,498
to estimate the paying of
damages,

11
00:00:40,999 --> 00:00:44,294
how much should be paid
in social projects etc.,

12
00:00:45,003 --> 00:00:47,464
If you use a certain method
to do the math...

13
00:00:51,760 --> 00:00:56,639
that is, let's say, less rigorous

14
00:00:57,182 --> 00:01:00,727
when evaluating impact...

15
00:01:01,394 --> 00:01:05,356
And I'm not even talking
about institutions yet,

16
00:01:05,982 --> 00:01:11,154
but if you adopt a method...
based solely on

17
00:01:11,154 --> 00:01:13,823
expertise of personnel,

18
00:01:13,823 --> 00:01:17,327
on personnel estimating impact,
which is simple to do,

19
00:01:18,078 --> 00:01:20,705
people who know methodologies

20
00:01:21,331 --> 00:01:23,833
will consider this a low amount.

21
00:01:25,794 --> 00:01:28,046
And that, as I said,

22
00:01:28,088 --> 00:01:31,132
is a standard, but in the case of Maceió,

23
00:01:31,883 --> 00:01:36,638
considering its demographic characteristics,
the impact on the city,

24
00:01:37,472 --> 00:01:40,100
I believe the estimate

25
00:01:40,934 --> 00:01:43,937
was the lowest possible.

26
00:01:43,937 --> 00:01:46,481
If we had a low,
medium and high scale,

27
00:01:47,857 --> 00:01:52,028
because I don't. I admit
I didn't see the methodology.

28
00:01:52,403 --> 00:01:54,864
It wasn't clear in the documentation

29
00:01:55,281 --> 00:01:57,700
which the procedure
and the calculations were,

30
00:01:57,992 --> 00:02:00,662
which indicators were used etc.,

31
00:02:01,871 --> 00:02:04,374
but I considered the result very low.

32
00:02:05,166 --> 00:02:07,585
Theses impacts probably need

33
00:02:07,585 --> 00:02:09,838
to be estimated in a much bigger scale

34
00:02:11,297 --> 00:02:15,093
than just localizing them.
Those are the directly impacted areas.

35
00:02:15,510 --> 00:02:17,720
But there are other cases.

36
00:02:17,720 --> 00:02:21,224
So I don't know, since
I'm in the Management Committee,

37
00:02:21,933 --> 00:02:24,144
now taking part in it,

38
00:02:24,144 --> 00:02:26,896
we've been dealing with it
for some time now.

39
00:02:27,522 --> 00:02:29,899
This is a concern
to the Committee.

40
00:02:30,900 --> 00:02:33,695
For example, where to invest these resources,

41
00:02:33,987 --> 00:02:36,489
what needs to be taken care of.
This is a fear I have,

42
00:02:38,575 --> 00:02:39,742
and it's a real fear,

43
00:02:39,742 --> 00:02:41,411
that the projects are used

44
00:02:41,411 --> 00:02:45,456
at Ponta Verde, Pajuçara, Jatiúca.

45
00:02:46,791 --> 00:02:49,169
"But Diego, it can't be done." But it can!

46
00:02:49,169 --> 00:02:51,337
"Are you sure, Diego? What if this, this and that..."

47
00:02:52,505 --> 00:02:56,593
Because it is very common
for those affected,

48
00:02:56,593 --> 00:02:59,971
those impacted,
not to benefit directly.

49
00:03:01,264 --> 00:03:05,518
And what will that require?
It will require research to identify

50
00:03:05,852 --> 00:03:09,522
exactly where those impacted are,

51
00:03:09,522 --> 00:03:12,483
because otherwise the risk is

52
00:03:12,942 --> 00:03:17,155
for a project resource to hit Ponta Verde,

53
00:03:17,488 --> 00:03:19,324
when it was aimed at those
who live in Mutange.

54
00:03:19,324 --> 00:03:21,784
And even if you have those
with the best intentions

55
00:03:21,993 --> 00:03:25,788
at the Public Prosecutor's Office,
Public Defense,

56
00:03:26,289 --> 00:03:29,500
if you don't establish
a clear partnership

57
00:03:30,418 --> 00:03:33,296
with other civil society institutions,

58
00:03:33,296 --> 00:03:35,298
and I am not saying
Braskem shouldn't participate.

59
00:03:35,465 --> 00:03:37,800
Take part. Bring on. Come in.

60
00:03:38,009 --> 00:03:39,802
It would be nice, right?
To measure the trouble.

61
00:03:40,345 --> 00:03:43,681
If you can't track

62
00:03:44,349 --> 00:03:48,937
the risk of these benefits,
they turn shady,

63
00:03:49,103 --> 00:03:51,022
in the worst sense.

64
00:03:51,814 --> 00:03:53,650
That's my biggest concern

65
00:03:53,650 --> 00:03:56,236
in the Management Committee.

66
00:03:56,236 --> 00:03:59,697
The Public Prosecutor's Office too.
Its role was...

67
00:04:00,657 --> 00:04:02,242
It's not just us.
Let's decide together.

68
00:04:02,617 --> 00:04:03,534
This is a big initiative,

69
00:04:03,743 --> 00:04:06,454
an important one, because

70
00:04:06,454 --> 00:04:09,040
it is an iniative on

71
00:04:10,250 --> 00:04:13,169
transparency, that in political science

72
00:04:13,461 --> 00:04:16,172
they would call accountability.

73
00:04:17,006 --> 00:04:19,842
It's extremely important that it's
an accountability with social purpose.

74
00:04:21,970 --> 00:04:26,724
But Braskem has no control
over this case.

75
00:04:27,141 --> 00:04:31,604
It's just the resource
that is little compared to

76
00:04:31,938 --> 00:04:34,732
the personal injury

77
00:04:34,732 --> 00:04:37,193
caused in the city.


1
00:00:01,119 --> 00:00:03,159
First, the press at the time

2
00:00:03,159 --> 00:00:04,880
didn't have all that freedom, right?

3
00:00:04,880 --> 00:00:07,800
But communication businessmen,

4
00:00:07,800 --> 00:00:10,159
of course, were involved with this project

5
00:00:10,159 --> 00:00:12,519
because it was a federal government project.

6
00:00:12,519 --> 00:00:13,719
Just so you have an idea

7
00:00:14,679 --> 00:00:17,079
of ​​the influence of this
and the consequences, 

8
00:00:17,760 --> 00:00:20,880
Salgema used to be a private company.

9
00:00:20,880 --> 00:00:22,960
It was slowly becoming associated

10
00:00:22,960 --> 00:00:24,559
with other private companies.

11
00:00:25,920 --> 00:00:26,880
Until it got to a point

12
00:00:26,880 --> 00:00:29,119
when the federal government decided to nationalize it.

13
00:00:29,760 --> 00:00:31,960
Nationalized with capital injection.

14
00:00:31,960 --> 00:00:33,159
It was buying the shares,

15
00:00:33,159 --> 00:00:34,360
increasing the capital.

16
00:00:34,880 --> 00:00:36,320
<b>And the others couldn't keep up</b>

17
00:00:36,320 --> 00:00:38,039
<b>and it kept incorporating the company</b>

18
00:00:38,039 --> 00:00:40,800
<b>until it ended up nationalized</b>

19
00:00:41,440 --> 00:00:43,239
<b>and then it was...</b>

20
00:00:43,920 --> 00:00:46,719
I don't remember which it was,

21
00:00:46,719 --> 00:00:48,119
the national development bank
and another agency.

22
00:00:48,599 --> 00:00:51,880
They merged and became known as Norquisa.

23
00:00:52,559 --> 00:00:53,880
Who was the first president of Norquisa

24
00:00:53,880 --> 00:00:56,000
shortly after leaving the presidency of the Republic?

25
00:00:56,000 --> 00:00:57,440
Ernesto Geisel.

26
00:00:58,239 --> 00:01:00,079
Just so you understand

27
00:01:00,079 --> 00:01:02,159
the depth of this game, right?

28
00:01:03,199 --> 00:01:04,920
So, in addition to the attraction

29
00:01:04,920 --> 00:01:07,320
for Alagoas, that there would be development,

30
00:01:07,320 --> 00:01:08,840
industrialization,

31
00:01:08,840 --> 00:01:10,800
purchase of alcohol

32
00:01:10,800 --> 00:01:12,079
from the distilleries plants...

33
00:01:13,119 --> 00:01:15,800
There was a whole economic attraction that convinced,

34
00:01:16,480 --> 00:01:17,400
honestly I think,

35
00:01:17,400 --> 00:01:19,199
the businessmen, government, 

36
00:01:19,199 --> 00:01:20,440
some of them, at least.

37
00:01:22,199 --> 00:01:25,840
And there was the united order to accept and that was it.

38
00:01:25,840 --> 00:01:27,920
So I imagine that the press at the time

39
00:01:28,719 --> 00:01:30,199
would have no way of reacting to that

40
00:01:30,199 --> 00:01:31,519
at all.

41
00:01:31,519 --> 00:01:33,480
It was called into the mess

42
00:01:33,480 --> 00:01:34,800
as always, right?

43
00:01:35,239 --> 00:01:36,559
With rare exceptions

44
00:01:36,559 --> 00:01:38,239
from the alternative press

45
00:01:38,239 --> 00:01:40,000
that denounced at the time,

46
00:01:40,000 --> 00:01:42,800
there were newspapers, Luta Popular and others

47
00:01:42,800 --> 00:01:44,639
with very restricted circulation.

48
00:01:45,480 --> 00:01:48,360
But at the time, there was no WhatsApp,

49
00:01:48,360 --> 00:01:51,039
no Facebook. No social networks to report.

50
00:01:51,679 --> 00:01:54,280
So, of course, the companies,

51
00:01:54,280 --> 00:01:56,199
the Arnon de Melo organization,

52
00:01:56,199 --> 00:01:58,320
the Alagoas newspaper at the time,

53
00:01:58,320 --> 00:01:59,400
and I don't know if the newspaper

54
00:01:59,400 --> 00:02:01,159
was still working...

55
00:02:02,480 --> 00:02:05,000
They were not exactly the center of complaints

56
00:02:05,000 --> 00:02:08,360
against the risk of this.

57
00:02:08,360 --> 00:02:10,079
On the contrary, right?

58
00:02:10,079 --> 00:02:11,719
In most articles, they

59
00:02:13,280 --> 00:02:16,519
dismiss the possibility of development, etc.

60
00:02:16,519 --> 00:02:18,239
And as it is known, for example,

61
00:02:18,239 --> 00:02:20,440
that the issue of employment would not be one of them, right?

62
00:02:21,719 --> 00:02:23,840
Almost all of the technicians weren't local.

63
00:02:24,599 --> 00:02:27,559
And there's a certain impact

64
00:02:27,559 --> 00:02:30,119
to that period from the early '70s

65
00:02:30,119 --> 00:02:31,519
to the mid '70s.

66
00:02:32,559 --> 00:02:35,480
Alagoas is something that deserves
to be studied...

67
00:02:36,239 --> 00:02:38,360
And you're doing it with research,

68
00:02:38,360 --> 00:02:40,119
aren't you? This documentary with research.

69
00:02:41,039 --> 00:02:42,559
Just imagine Alagoas

70
00:02:42,559 --> 00:02:44,679
until the end of the '60s.

71
00:02:45,480 --> 00:02:48,119
The big businesses were run by families.

72
00:02:48,880 --> 00:02:51,119
So people didn't have

73
00:02:51,119 --> 00:02:53,480
very close relations with power.

74
00:02:54,159 --> 00:02:55,440
From that period on,

75
00:02:55,440 --> 00:02:57,559
some phenomena happen in Alagoas.

76
00:02:58,639 --> 00:03:00,960
There is a flood wave,

77
00:03:00,960 --> 00:03:04,039
a mass arrival of technicians connected

78
00:03:04,039 --> 00:03:05,639
<b>to Salgema</b>

79
00:03:05,639 --> 00:03:07,079
who come from Bahia,

80
00:03:07,079 --> 00:03:08,719
from São Paulo, from Rio,

81
00:03:08,719 --> 00:03:09,840
they come from all over the place.

82
00:03:09,840 --> 00:03:11,840
These people will live in Maceió

83
00:03:11,840 --> 00:03:15,400
and they have no political or social ties with

84
00:03:16,079 --> 00:03:18,320
the traditional families of our power.

85
00:03:19,320 --> 00:03:21,599
During this same period, distilleries

86
00:03:21,599 --> 00:03:23,360
and large companies

87
00:03:23,360 --> 00:03:25,199
that work with heavy mechanics

88
00:03:25,199 --> 00:03:27,119
also arrived with technicians from abroad.

89
00:03:28,199 --> 00:03:32,199
And UFAL also created new courses during this period

90
00:03:32,199 --> 00:03:33,760
in the mid-70s,

91
00:03:33,760 --> 00:03:37,320
so several teachers arrive from Ceará,

92
00:03:37,320 --> 00:03:40,079
from Bahia, from Pernambuco

93
00:03:40,079 --> 00:03:42,119
and from Rio.

94
00:03:42,119 --> 00:03:45,920
So many come here to create these courses

95
00:03:45,920 --> 00:03:49,559
in architecture, agronomy and so on.

96
00:03:50,320 --> 00:03:51,559
This mass

97
00:03:51,559 --> 00:03:53,320
of middle-class people

98
00:03:53,320 --> 00:03:54,719
with political training

99
00:03:54,719 --> 00:03:56,840
and a certain technical level

100
00:03:57,639 --> 00:04:00,360
generates great pressure in Maceió

101
00:04:00,360 --> 00:04:00,960
<b>pra que</b>

102
00:04:01,840 --> 00:04:03,960
<b>for the city to start</b>

103
00:04:06,880 --> 00:04:10,039
respecting citizenship, so to speak.

104
00:04:10,039 --> 00:04:13,960
It is exactly from this core that you will find

105
00:04:14,639 --> 00:04:15,920
the use of research

106
00:04:15,920 --> 00:04:19,840
and stand out, acting on the issue of the environment.

107
00:04:20,960 --> 00:04:22,760
This presence of people in Maceió

108
00:04:22,760 --> 00:04:24,559
that didn't have any ties

109
00:04:24,559 --> 00:04:25,639
wasn't like:

110
00:04:25,639 --> 00:04:27,239
I'm not going to say anything because

111
00:04:27,239 --> 00:04:29,320
my daughter is employed by the government!

112
00:04:29,320 --> 00:04:31,320
Or because my dad has a company

113
00:04:31,320 --> 00:04:33,760
that supplies the city of Maceió.

114
00:04:33,760 --> 00:04:36,239
These people, they arrive free of it.

115
00:04:36,239 --> 00:04:39,880
So they have

116
00:04:41,280 --> 00:04:43,639
a freer opinion,

117
00:04:43,840 --> 00:04:45,800
more unencumbered

118
00:04:45,800 --> 00:04:47,039
by any political ties.

119
00:04:47,039 --> 00:04:49,800
And with that comes a respectable

120
00:04:49,800 --> 00:04:51,039
critical mass in Maceió.

121
00:04:51,679 --> 00:04:52,320
Not just that.

122
00:04:52,320 --> 00:04:55,039
These people charge the city hall to pave the street

123
00:04:55,039 --> 00:04:56,599
and denounce it if they don't,

124
00:04:56,599 --> 00:04:57,920
because they're not afraid

125
00:04:57,920 --> 00:05:00,599
of the guy there frowning upon them, the mayor.

126
00:05:00,599 --> 00:05:02,440
The society for the defense of human rights,

127
00:05:02,440 --> 00:05:03,880
when it formed in 1978

128
00:05:03,880 --> 00:05:05,800
at the initial meeting,

129
00:05:05,800 --> 00:05:09,039
I think 80% of those present were professors

130
00:05:09,039 --> 00:05:11,599
or employees from outside Alagoas.

131
00:05:12,599 --> 00:05:15,039
So this is an important issue to be identified

132
00:05:15,039 --> 00:05:17,519
from the sociological point of view

133
00:05:17,519 --> 00:05:21,199
of this formation of this critical mass

134
00:05:21,199 --> 00:05:23,880
of people from Maceio, especially, I'd say,

135
00:05:23,880 --> 00:05:24,960
after people from Alagoas, right?

136
00:05:25,719 --> 00:05:27,639
In this period.

137
00:05:27,639 --> 00:05:29,519
So you can't demand that,

138
00:05:29,519 --> 00:05:31,360
for example, journalists

139
00:05:31,360 --> 00:05:33,559
have this freedom of criticism,

140
00:05:33,559 --> 00:05:35,800
because even if they did, and they did,

141
00:05:35,800 --> 00:05:37,519
they were prevented.

142
00:05:37,519 --> 00:05:39,480
The press was the one who determined 

143
00:05:39,480 --> 00:05:41,840
that it wasn't supposed to be messed with

144
00:05:41,840 --> 00:05:44,039
and there was also censorship, wasn't it?

145
00:05:44,039 --> 00:05:46,639
During this period I worked, in the 1970s,

146
00:05:46,639 --> 00:05:49,079
at the newspaper O Movimento de São Paulo.

147
00:05:49,079 --> 00:05:51,039
And how many times did we have to rush out

148
00:05:51,039 --> 00:05:52,559
to try to save the newspaper

149
00:05:52,559 --> 00:05:54,159
from the federal police arresting

150
00:05:54,159 --> 00:05:55,159
the newspaper, right?

151
00:05:55,960 --> 00:05:58,159
<b>So I saw a whole</b>

152
00:05:59,199 --> 00:06:02,039
inspection on the opinion

153
00:06:02,039 --> 00:06:04,960
about the news on the information.

154
00:06:04,960 --> 00:06:06,480
It wasn't easy, was it?

155
00:06:06,480 --> 00:06:09,360
It wasn't easy, but even so, we managed,

156
00:06:09,360 --> 00:06:11,880
with pamphlets and alternative newspapers,

157
00:06:11,880 --> 00:06:13,719
to try to break through this blockade.

158
00:06:13,719 --> 00:06:16,320
But really the communication of the time

159
00:06:16,320 --> 00:06:18,920
and television, newspapers,

160
00:06:18,920 --> 00:06:21,159
the mainstream press, right? We called... 

161
00:06:22,239 --> 00:06:25,960
They were not critical about it.

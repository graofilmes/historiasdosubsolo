1
00:00:00,000 --> 00:00:00,000
<b>We are working.</b>

2
00:00:00,000 --> 00:00:00,000
<b>Since the beginning of this Braskem crisis</b>

3
00:00:00,000 --> 00:00:00,000
<b>we have a job of</b>

4
00:00:00,000 --> 00:00:00,000
<b>very strong clarification.</b>

5
00:00:00,000 --> 00:00:00,000
<b>What happens in these places</b>

6
00:00:00,000 --> 00:00:00,000
<b>has a very technical side,</b>

7
00:00:00,000 --> 00:00:00,000
<b>which is hard to understand.</b>

8
00:00:00,000 --> 00:00:00,000
<b>It is hard for people to grasp.</b>

9
00:00:00,000 --> 00:00:00,000
<b>You have maps that are released,</b>

10
00:00:00,000 --> 00:00:00,000
<b>updated,</b>

11
00:00:00,000 --> 00:00:00,000
<b>and that are technical information</b>

12
00:00:00,000 --> 00:00:00,000
<b>with geological questions, for example,</b>

13
00:00:00,000 --> 00:00:00,000
<b>and lay people have a hard time interpreting that.</b>

14
00:00:00,000 --> 00:00:00,000
<b>So since the beginning of 2018</b>

15
00:00:00,000 --> 00:00:00,000
<b>we have acted,</b>

16
00:00:00,000 --> 00:00:00,000
<b>a lot on my part for the architecture and urbanism,</b>

17
00:00:00,000 --> 00:00:00,000
<b>to enlighten many people.</b>

18
00:00:00,000 --> 00:00:00,000
<b>I acted, for example...</b>

19
00:00:00,000 --> 00:00:00,000
<b>I attended several events,</b>

20
00:00:00,000 --> 00:00:00,000
<b>several hearings</b>

21
00:00:00,000 --> 00:00:00,000
<b>since the biggest one,</b>

22
00:00:00,000 --> 00:00:00,000
<b>when CPRM really declared</b>

23
00:00:00,000 --> 00:00:00,000
<b>Braskem guilty of the phenomenon,</b>

24
00:00:00,000 --> 00:00:00,000
<b>but also the minor ones that were</b>

25
00:00:00,000 --> 00:00:00,000
<b>held by residents</b>

26
00:00:00,000 --> 00:00:00,000
<b>or by the university.</b>

27
00:00:00,000 --> 00:00:00,000
<b>I was at UFAL</b>

28
00:00:00,000 --> 00:00:00,000
<b>in events that UFAL held</b>

29
00:00:00,000 --> 00:00:00,000
<b>to enlighten the academic community.</b>

30
00:00:00,000 --> 00:00:00,000
<b>I took part in moments</b>

31
00:00:00,000 --> 00:00:00,000
<b>where the Pinheiro Baptist Church</b>

32
00:00:00,000 --> 00:00:00,000
<b>called experts,</b>

33
00:00:00,000 --> 00:00:00,000
<b>called Professor Abel, for example.</b>

34
00:00:00,000 --> 00:00:00,000
<b>A table was held by "Aqui Fora"</b>

35
00:00:00,000 --> 00:00:00,000
<b>at the Quintal Cultural,</b>

36
00:00:00,000 --> 00:00:00,000
<b>I was at the table participating.</b>

37
00:00:00,000 --> 00:00:00,000
<b>So anyway. I acted in clarifying</b>

38
00:00:00,000 --> 00:00:00,000
<b>and as a bridge</b>

39
00:00:00,000 --> 00:00:00,000
<b>between residents and these technicians.</b>

40
00:00:00,000 --> 00:00:00,000
<b>At times, I also acted by</b>

41
00:00:00,000 --> 00:00:00,000
<b>offering my services.</b>

42
00:00:00,000 --> 00:00:00,000
<b>I had a friend who was with his parents</b>

43
00:00:00,000 --> 00:00:00,000
<b>living over there in the Sanatorio area.</b>

44
00:00:00,000 --> 00:00:00,000
<b>And it was very common in that region to...</b>

45
00:00:00,000 --> 00:00:00,000
<b>I talked to the locals and</b>

46
00:00:00,000 --> 00:00:00,000
<b>they did not have title to the land.</b>

47
00:00:00,000 --> 00:00:00,000
<b>They had been there for decades,</b>

48
00:00:00,000 --> 00:00:00,000
<b>but they had never had the concern</b>

49
00:00:00,000 --> 00:00:00,000
<b>to establish their ownership titles,</b>

50
00:00:00,000 --> 00:00:00,000
<b>their formal instrument of property possession.</b>

51
00:00:00,000 --> 00:00:00,000
<b>My friend for one, his parents didn't have it</b>

52
00:00:00,000 --> 00:00:00,000
<b>and they had lived there since he was born.</b>

53
00:00:00,000 --> 00:00:00,000
<b>He had lived there for almost 40 years,</b>

54
00:00:00,000 --> 00:00:00,000
<b>but he didn't have their parents' title to the property.</b>

55
00:00:00,000 --> 00:00:00,000
<b>So I offered to make a basic plan of the property</b>

56
00:00:00,000 --> 00:00:00,000
<b>so they could enter with adverse possession, for example.</b>

57
00:00:00,000 --> 00:00:00,000
<b>In order to have access</b>

58
00:00:00,000 --> 00:00:00,000
<b>to services led by the Public Prosecutor's Office,</b>

59
00:00:00,000 --> 00:00:00,000
<b>they required it.</b>

60
00:00:00,000 --> 00:00:00,000
<b>The Public Prosecutor has established an office</b>

61
00:00:00,000 --> 00:00:00,000
<b>with several services,</b>

62
00:00:00,000 --> 00:00:00,000
<b>but to have access you had to prove</b>

63
00:00:00,000 --> 00:00:00,000
<b>you owned the property.</b>

64
00:00:00,000 --> 00:00:00,000
<b>And legally,</b>

65
00:00:00,000 --> 00:00:00,000
<b>you can only prove it with a title deed.</b>

66
00:00:00,000 --> 00:00:00,000
<b>A Purchase and Sale Agreement is not a deed.</b>

67
00:00:00,000 --> 00:00:00,000
<b>So I volunteered for it.</b>

68
00:00:00,000 --> 00:00:00,000
<b>I offered to another friend</b>

69
00:00:00,000 --> 00:00:00,000
<b>who had to get out of there</b>

70
00:00:00,000 --> 00:00:00,000
<b>and needed to renovate the apartment</b>

71
00:00:00,000 --> 00:00:00,000
<b>to move on with his life.</b>

72
00:00:00,000 --> 00:00:00,000
<b>So I offered to do the job</b>

73
00:00:00,000 --> 00:00:00,000
<b>for a price below the architecture and urban planning market value.</b>

74
00:00:00,000 --> 00:00:00,000
<b>I couldn't do it for free because</b>

75
00:00:00,000 --> 00:00:00,000
<b>I'd have to pay to survey the property.</b>

76
00:00:00,000 --> 00:00:00,000
<b>I had to have someone else with me,</b>

77
00:00:00,000 --> 00:00:00,000
<b>I had to pay an intern.</b>

78
00:00:00,000 --> 00:00:00,000
<b>But I offered a service below market value</b>

79
00:00:00,000 --> 00:00:00,000
<b>so he could renovate the apartment.</b>

80
00:00:00,000 --> 00:00:00,000
<b>I went with a friend who was also a local</b>

81
00:00:00,000 --> 00:00:00,000
<b>to make a property valuation.</b>

82
00:00:00,000 --> 00:00:00,000
<b>So, I volunteered for several things</b>

83
00:00:00,000 --> 00:00:00,000
<b>throughout the process</b>

84
00:00:00,000 --> 00:00:00,000
<b>and IDEAL acted by promoting and clarifying</b>

85
00:00:00,000 --> 00:00:00,000
<b>and publicizing events</b>

86
00:00:00,000 --> 00:00:00,000
<b>that discussed the issue with residents.</b>

87
00:00:00,000 --> 00:00:00,000
<b>In 2019, we did</b>

88
00:00:00,000 --> 00:00:00,000
<b>a hackathon.</b>

89
00:00:00,000 --> 00:00:00,000
<b>I was from the IAB at the time.</b>

90
00:00:00,000 --> 00:00:00,000
<b>IAB is the Architects Institute of Brazil.</b>

91
00:00:00,000 --> 00:00:00,000
<b>I was from IAB Alagoas, I was the president.</b>

92
00:00:00,000 --> 00:00:00,000
<b>I had already been vice president for two terms</b>

93
00:00:00,000 --> 00:00:00,000
<b>and, during that term, I was president of the IAB.</b>

94
00:00:00,000 --> 00:00:00,000
<b>And the IAB pulled along with the UN-habitat,</b>

95
00:00:00,000 --> 00:00:00,000
<b>with UN-habitat personnel here in Maceió,</b>

96
00:00:00,000 --> 00:00:00,000
<b>and together with Detran Alagoas and UNIT</b>

97
00:00:00,000 --> 00:00:00,000
<b>pulled an event that was inside</b>

98
00:00:00,000 --> 00:00:00,000
<b>the international UN urban circuit.</b>

99
00:00:00,000 --> 00:00:00,000
<b>We pulled an event that was called at the time</b>

100
00:00:00,000 --> 00:00:00,000
<b>"meeting of urban thinkers from Alagoas",</b>

101
00:00:00,000 --> 00:00:00,000
<b>and this event had three axes</b>

102
00:00:00,000 --> 00:00:00,000
<b>and each institution was responsible for an axis.</b>

103
00:00:00,000 --> 00:00:00,000
<b>One of the axes was urban resilience</b>

104
00:00:00,000 --> 00:00:00,000
<b>and the IAB was responsible for this axis.</b>

105
00:00:00,000 --> 00:00:00,000
<b>I suggested we focus this entire axis</b>

106
00:00:00,000 --> 00:00:00,000
<b>on the neighborhoods issue, which at the time were three:</b>

107
00:00:00,000 --> 00:00:00,000
<b>Pinheiro and the newly entered</b>

108
00:00:00,000 --> 00:00:00,000
<b>Bebedouro and Mutange.</b>

109
00:00:00,000 --> 00:00:00,000
<b>So I suggested we focus</b>

110
00:00:00,000 --> 00:00:00,000
<b>the issue of urban resilience in these three neighborhoods.</b>

111
00:00:00,000 --> 00:00:00,000
<b>We made a table with the civil police,</b>

112
00:00:00,000 --> 00:00:00,000
<b>with representatives of the residents,</b>

113
00:00:00,000 --> 00:00:00,000
<b>with the university...</b>

114
00:00:00,000 --> 00:00:00,000
<b>So, it was a table that gathered</b>

115
00:00:00,000 --> 00:00:00,000
<b>many points of view on the issue</b>

116
00:00:00,000 --> 00:00:00,000
<b>and we did a hackathon.</b>

117
00:00:00,000 --> 00:00:01,960
<b>We are working.</b>

118
00:00:01,960 --> 00:00:06,199
<b>Since the beginning of this Braskem crisis</b>

119
00:00:06,600 --> 00:00:08,039
<b>we have a job of</b>

120
00:00:08,039 --> 00:00:10,840
<b>very strong</b>

121
00:00:10,840 --> 00:00:12,199
<b>clarification.</b>

122
00:00:12,199 --> 00:00:14,159
<b>What happens in these places</b>

123
00:00:14,159 --> 00:00:15,199
<b>has a very technical side,</b>

124
00:00:15,199 --> 00:00:16,600
<b>which is hard to understand.</b>

125
00:00:16,600 --> 00:00:18,680
<b>It is hard for people to grasp.</b>

126
00:00:18,680 --> 00:00:21,479
<b>You have maps that are released,</b>

127
00:00:21,479 --> 00:00:22,720
<b>updated,</b>

128
00:00:22,720 --> 00:00:24,560
<b>and that are technical information</b>

129
00:00:25,560 --> 00:00:28,319
<b>with geological questions, for example,</b>

130
00:00:28,319 --> 00:00:31,319
<b>and lay people have a hard time interpreting that.</b>

131
00:00:31,800 --> 00:00:33,079
<b>So since it started</b>

132
00:00:33,079 --> 00:00:34,800
<b>in the beginning of 2018</b>

133
00:00:34,800 --> 00:00:36,880
<b>we have acted,</b>

134
00:00:36,880 --> 00:00:39,840
<b>a lot on my part for the architecture and urbanism,</b>

135
00:00:39,840 --> 00:00:42,119
<b>to enlighten many people.</b>

136
00:00:42,119 --> 00:00:44,239
<b>I acted, for example...</b>

137
00:00:44,239 --> 00:00:46,079
<b>I attended all,</b>

138
00:00:46,079 --> 00:00:48,640
<b>not all, but several...</b>

139
00:00:48,640 --> 00:00:50,239
<b>Several events,</b>

140
00:00:50,239 --> 00:00:51,640
<b>several hearings</b>

141
00:00:52,279 --> 00:00:54,680
<b>since the biggest one with the CPRM,</b>

142
00:00:54,680 --> 00:00:55,800
<b>when they really declared</b>

143
00:00:55,800 --> 00:00:57,640
<b>Braskem guilty of the phenomenon,</b>

144
00:00:57,640 --> 00:00:58,720
<b>but also the minor ones</b>

145
00:00:58,720 --> 00:01:00,159
<b>that were held by residents</b>

146
00:01:00,479 --> 00:01:02,479
<b>or by the university. I was at UFAL</b>

147
00:01:02,479 --> 00:01:04,319
<b>in events that UFAL held</b>

148
00:01:04,319 --> 00:01:06,760
<b>to enlighten the academic community.</b>

149
00:01:06,760 --> 00:01:07,800
<b>I have been...</b>

150
00:01:07,800 --> 00:01:09,560
<b>I took part in moments where</b>

151
00:01:09,560 --> 00:01:11,159
<b>the Pinheiro Baptist Church</b>

152
00:01:11,159 --> 00:01:12,359
<b>called experts,</b>

153
00:01:12,359 --> 00:01:14,159
<b>called Professor Abel, for example.</b>

154
00:01:14,159 --> 00:01:15,359
<b>I was... </b>

155
00:01:15,359 --> 00:01:17,439
<b>A table was held by "Aqui Fora"</b>

156
00:01:17,439 --> 00:01:18,560
<b>at the Quintal Cultural,</b>

157
00:01:18,560 --> 00:01:20,279
<b>I was at the table participating.</b>

158
00:01:20,279 --> 00:01:21,199
<b>So anyway.</b>

159
00:01:21,199 --> 00:01:22,840
<b>I acted in clarifying</b>

160
00:01:22,840 --> 00:01:24,720
<b>and as a bridge</b>

161
00:01:24,720 --> 00:01:27,319
<b>between residents and these technicians.</b>

162
00:01:27,319 --> 00:01:29,800
<b>At times, I also acted by</b>

163
00:01:29,800 --> 00:01:32,279
<b>offering my services.</b>

164
00:01:33,039 --> 00:01:35,399
<b>I had a friend who was</b>

165
00:01:35,399 --> 00:01:36,600
<b>with his parents living</b>

166
00:01:36,600 --> 00:01:37,800
<b> over there in the Sanatorio area.</b>

167
00:01:37,800 --> 00:01:39,560
<b>And it was very common in that region to...</b>

168
00:01:39,560 --> 00:01:41,239
<b>I talked to the locals and</b>

169
00:01:41,239 --> 00:01:43,880
<b>they did not have title to the land.</b>

170
00:01:44,880 --> 00:01:46,760
<b>They had been there for decades,</b>

171
00:01:46,760 --> 00:01:49,920
<b>but they had never had the concern</b>

172
00:01:50,199 --> 00:01:53,119
<b>to establish</b>

173
00:01:53,119 --> 00:01:54,479
<b>their ownership titles,</b>

174
00:01:54,479 --> 00:01:56,960
<b>their formal instrument of property possession.</b>

175
00:01:57,600 --> 00:01:59,079
<b>My friend for one,</b>

176
00:01:59,079 --> 00:01:59,920
<b>his parents didn't have it</b>

177
00:01:59,920 --> 00:02:01,279
<b>and they had lived there since he was born.</b>

178
00:02:01,279 --> 00:02:02,760
<b>He had lived there</b>

179
00:02:02,760 --> 00:02:03,880
<b>for almost 40 years,</b>

180
00:02:04,039 --> 00:02:06,279
<b>but he didn't have their parents' title to the property.</b>

181
00:02:06,279 --> 00:02:08,920
<b>So I offered to make a basic plan of the property</b>

182
00:02:08,920 --> 00:02:10,399
<b>so they could enter with adverse possession,</b>

183
00:02:10,399 --> 00:02:11,039
<b>for example.</b>

184
00:02:11,359 --> 00:02:13,760
<b>In order to have access</b>

185
00:02:13,760 --> 00:02:16,560
<b>to services led by the Public Prosecutor's Office,</b>

186
00:02:16,560 --> 00:02:18,079
<b>they required it.</b>

187
00:02:18,079 --> 00:02:19,800
<b>The Public Prosecutor has established an office</b>

188
00:02:19,800 --> 00:02:21,279
<b>with several services,</b>

189
00:02:21,279 --> 00:02:22,520
<b>but to have access</b>

190
00:02:22,520 --> 00:02:24,079
<b>you had to prove you owned the property.</b>

191
00:02:24,079 --> 00:02:25,880
<b>And legally,</b>

192
00:02:25,880 --> 00:02:27,359
<b>you can only prove it with a title deed.</b>

193
00:02:27,720 --> 00:02:29,920
<b>A Purchase and Sale Agreement is not a deed.</b>

194
00:02:29,920 --> 00:02:31,279
<b>So I volunteered for it.</b>

195
00:02:31,279 --> 00:02:31,960
<b>I offered</b>

196
00:02:31,960 --> 00:02:33,920
<b>to another friend who had to get out of there</b>

197
00:02:33,920 --> 00:02:35,760
<b>and needed to renovate the apartment</b>

198
00:02:35,760 --> 00:02:38,600
<b>to move on with his life.</b>

199
00:02:38,600 --> 00:02:41,159
<b>So I offered to do the job</b>

200
00:02:41,159 --> 00:02:44,720
<b>for a price below the architecture and urban planning market value.</b>

201
00:02:44,720 --> 00:02:46,680
<b>I couldn't do it for free because</b>

202
00:02:46,680 --> 00:02:47,560
<b>I'd have to pay</b>

203
00:02:47,560 --> 00:02:49,479
<b>to survey the property.</b>

204
00:02:49,479 --> 00:02:50,600
<b>I had to have someone else with me,</b>

205
00:02:50,600 --> 00:02:51,680
<b>I had to pay an intern.</b>

206
00:02:51,680 --> 00:02:54,239
<b>But I offered a service below market value</b>

207
00:02:54,239 --> 00:02:56,640
<b>so he could renovate the apartment.</b>

208
00:02:57,319 --> 00:02:58,600
<b>I went with a friend</b>

209
00:02:58,600 --> 00:02:59,560
<b>who was also a local</b>

210
00:02:59,560 --> 00:03:01,359
<b>to make a property valuation.</b>

211
00:03:01,359 --> 00:03:02,760
<b>So, I volunteered for several things</b>

212
00:03:02,760 --> 00:03:04,079
<b>throughout the process</b>

213
00:03:04,079 --> 00:03:05,920
<b>and IDEAL acted</b>

214
00:03:05,920 --> 00:03:08,000
<b>by promoting and clarifying</b>

215
00:03:08,000 --> 00:03:11,239
<b>and publicizing events that discussed</b>

216
00:03:11,239 --> 00:03:13,600
<b>the issue with residents.</b>

217
00:03:13,600 --> 00:03:16,319
<b>In 2019, we did</b>

218
00:03:16,319 --> 00:03:17,399
<b>a hackathon.</b>

219
00:03:17,399 --> 00:03:18,479
<b>We promoted a hackathon.</b>

220
00:03:18,479 --> 00:03:20,319
<b>I was from the IAB at the time.</b>

221
00:03:20,319 --> 00:03:23,000
<b>IAB is the Architects Institute of Brazil.</b>

222
00:03:23,000 --> 00:03:24,720
<b>I was from IAB Alagoas, I was the president.</b>

223
00:03:24,720 --> 00:03:27,039
<b>I had already been vice president for two terms</b>

224
00:03:27,039 --> 00:03:29,119
<b>and, during that term, I was president of the IAB.</b>

225
00:03:29,119 --> 00:03:32,479
<b>And the IAB pulled along with the UN-habitat,</b>

226
00:03:32,479 --> 00:03:34,439
<b>with UN-habitat personnel here in Maceió,</b>

227
00:03:34,439 --> 00:03:38,279
<b>and together with Detran Alagoas</b>

228
00:03:38,279 --> 00:03:39,239
<b>and UNIT</b>

229
00:03:39,239 --> 00:03:43,439
<b>pulled an event that was inside the international UN</b>

230
00:03:43,439 --> 00:03:45,600
<b>urban circuit.</b>

231
00:03:45,600 --> 00:03:48,039
<b>We pulled an event that was called at the time</b>

232
00:03:48,039 --> 00:03:51,520
<b>"meeting of urban thinkers from Alagoas",</b>

233
00:03:51,520 --> 00:03:54,239
<b>and this event had three axes</b>

234
00:03:54,239 --> 00:03:56,399
<b>and each institution was responsible for an axis.</b>

235
00:03:56,760 --> 00:03:59,600
<b>One of the axes was urban resilience</b>

236
00:03:59,600 --> 00:04:02,039
<b>and the IAB was responsible for this axis.</b>

237
00:04:02,039 --> 00:04:06,159
<b>I suggested we focus this entire axis</b>

238
00:04:06,159 --> 00:04:07,479
<b>on the neighborhoods issue,</b>

239
00:04:07,479 --> 00:04:09,600
<b>which at the time were three:</b>

240
00:04:09,600 --> 00:04:10,600
<b>Pinheiro</b>

241
00:04:10,600 --> 00:04:14,399
<b>eand the newly entered Bebedouro and Mutange.</b>

242
00:04:14,399 --> 00:04:17,399
<b>So I suggested we focus</b>

243
00:04:17,399 --> 00:04:20,760
<b>the issue of urban resilience in these three neighborhoods.</b>

244
00:04:20,760 --> 00:04:22,119
<b>We made a table</b>

245
00:04:22,119 --> 00:04:24,880
<b>with the civil police,</b>

246
00:04:24,880 --> 00:04:29,279
<b>with representatives of the residents,</b>

247
00:04:29,279 --> 00:04:31,920
<b>with the university...</b>

248
00:04:31,920 --> 00:04:34,479
<b>So, it was a table that gathered</b>

249
00:04:34,479 --> 00:04:36,000
<b>many points of view on the issue</b>

250
00:04:36,000 --> 00:04:37,880
<b>and so we did</b>

251
00:04:37,880 --> 00:04:39,359
<b>a hackathon.</b>


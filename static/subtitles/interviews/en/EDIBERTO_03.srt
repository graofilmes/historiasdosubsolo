1
00:00:00,039 --> 00:00:02,559
Salgema's operating permit

2
00:00:02,559 --> 00:00:04,360
comes from the mid

3
00:00:05,519 --> 00:00:07,320
60s.

4
00:00:07,320 --> 00:00:08,760
I don't remember if
5
00:00:08,760 --> 00:00:11,559
there was an authorization on paper

6
00:00:11,559 --> 00:00:15,840
or when the location was defined if there were any...

7
00:00:15,840 --> 00:00:17,679
But of course the state government authorized it.

8
00:00:17,679 --> 00:00:19,280
Not the slightest doubt they did it.

9
00:00:19,280 --> 00:00:22,760
I don't know if the legal means were

10
00:00:23,280 --> 00:00:25,480
standard ones. Now there's

11
00:00:25,480 --> 00:00:27,800
a land use permit

12
00:00:27,800 --> 00:00:29,599
or any special one for this purpose,

13
00:00:29,599 --> 00:00:31,559
or if there was another document.

14
00:00:31,559 --> 00:00:34,400
Now, when the duplication happened,

15
00:00:34,400 --> 00:00:36,760
the duplication project that caused a reaction,

16
00:00:37,599 --> 00:00:39,159
I remember Governor Suruagy

17
00:00:39,159 --> 00:00:40,679
even proposed a plebiscite

18
00:00:40,679 --> 00:00:42,119
so that the population could decide

19
00:00:42,119 --> 00:00:43,760
whether or not to accept this duplication.

20
00:00:44,639 --> 00:00:46,920
In other words, there was great political strain,

21
00:00:46,920 --> 00:00:48,519
fear increased,

22
00:00:48,519 --> 00:00:51,400
mainly due to the accident in 1982.

23
00:00:52,360 --> 00:00:54,199
There was a whole political game

24
00:00:54,199 --> 00:00:56,679
of dividing responsibilities,

25
00:00:56,679 --> 00:01:00,119
and the legislative assembly itself was involved, right?

26
00:01:00,719 --> 00:01:02,480
I remember I was a councilor.

27
00:01:02,480 --> 00:01:05,800
I took office in January 1983.

28
00:01:06,639 --> 00:01:07,800
And in 1985

29
00:01:07,800 --> 00:01:10,639
I even created a special committee of inquiry

30
00:01:10,639 --> 00:01:13,360
that was an aspect of the municipal PCI.

31
00:01:14,599 --> 00:01:16,119
I don't know how I managed to pass that,

32
00:01:16,119 --> 00:01:17,360
but I did.

33
00:01:18,000 --> 00:01:20,079
Because there was a backlash, of course.

34
00:01:20,079 --> 00:01:22,440
The government's political support base

35
00:01:22,440 --> 00:01:24,960
shouldn't have allowed it, should it?

36
00:01:24,960 --> 00:01:26,599
But we managed to pass it.

37
00:01:27,920 --> 00:01:28,960
But it didn't prosper much.

38
00:01:28,960 --> 00:01:30,159
We started investigations,

39
00:01:30,159 --> 00:01:33,760
there was no technical team able to help us.

40
00:01:33,760 --> 00:01:38,920
We even heard Salgema's direction in the Chamber.

41
00:01:39,760 --> 00:01:42,000
But the focus was

42
00:01:42,000 --> 00:01:44,519
to identify risks,

43
00:01:44,519 --> 00:01:46,159
whether there was risks or not.

44
00:01:46,159 --> 00:01:48,119
If with duplication, that risk would be

45
00:01:48,119 --> 00:01:51,119
doubled, proportionally or not.

46
00:01:51,119 --> 00:01:53,719
The focus of the PCI was to resolve

47
00:01:53,719 --> 00:01:57,760
the questions the population asked.

48
00:01:57,760 --> 00:02:00,639
This climate of insecurity in Maceió,

49
00:02:01,360 --> 00:02:03,119
or, at least, in the nearby neighborhoods

50
00:02:03,119 --> 00:02:05,039
that lived during

51
00:02:05,039 --> 00:02:07,559
this period of implementation

52
00:02:07,559 --> 00:02:08,960
of the duplication

53
00:02:08,960 --> 00:02:10,880
of the diochlorethane plant.

54
00:02:12,920 --> 00:02:15,239
How do I remember this process?

55
00:02:16,239 --> 00:02:17,800
When Governor Suruagy

56
00:02:17,800 --> 00:02:19,880
proposes a plebiscite

57
00:02:19,880 --> 00:02:21,559
and the plebiscite does not happen,

58
00:02:21,559 --> 00:02:22,719
what is the result?

59
00:02:22,719 --> 00:02:25,360
They end up approving it without a plebiscite, without anything.

60
00:02:25,360 --> 00:02:28,559
There was a lot of pressure from the federal government

61
00:02:28,559 --> 00:02:30,039
and it ended up approving it.

62
00:02:30,760 --> 00:02:33,239
And it was duplicated and that's it.

63
00:02:33,239 --> 00:02:35,480
And then it came to a hault.

64
00:02:36,199 --> 00:02:37,920
There was no...

65
00:02:37,920 --> 00:02:40,519
I don't remember if residually

66
00:02:40,519 --> 00:02:42,880
there was anything until '86.

67
00:02:43,480 --> 00:02:44,679
From '86 onward,

68
00:02:44,679 --> 00:02:47,760
I don't remember any movement

69
00:02:47,760 --> 00:02:52,039
against the presence of Salgema

70
00:02:52,039 --> 00:02:53,960
from an environmental point of view.

71
00:02:53,960 --> 00:02:55,719
There was no action.

72
00:02:55,719 --> 00:02:56,519
I don't remember any.

73
00:02:56,519 --> 00:02:58,840
If there was I don't remember.

74
00:02:59,599 --> 00:03:01,000
What technically,

75
00:03:01,000 --> 00:03:02,840
when it was explained at the time

76
00:03:03,639 --> 00:03:05,199
and no one disputed it,

77
00:03:05,199 --> 00:03:06,119
was this:

78
00:03:06,119 --> 00:03:09,840
look, the salt removal won't be very deep

79
00:03:09,840 --> 00:03:11,039
<b>and there is</b>

80
00:03:12,639 --> 00:03:14,559
a whole geological formation

81
00:03:14,559 --> 00:03:16,719
above that protects, so..

82
00:03:16,719 --> 00:03:18,840
and they would remove the rock salt

83
00:03:18,840 --> 00:03:20,679
and add water

84
00:03:20,679 --> 00:03:22,199
to fill the caves.

85
00:03:22,199 --> 00:03:23,280
There would be no risk.

86
00:03:24,000 --> 00:03:26,519
So, this issue doesn't appear at any time,

87
00:03:26,519 --> 00:03:29,320
in any protest, in any document, nothing.

88
00:03:31,880 --> 00:03:33,360
I remember that it was Eric's group,

89
00:03:33,360 --> 00:03:34,920
from journalism at Ufal,

90
00:03:34,920 --> 00:03:37,639
that launched a laboratory newspaper,

91
00:03:37,639 --> 00:03:39,039
very restricted,

92
00:03:39,039 --> 00:03:41,559
in which they speculated about this risk:

93
00:03:41,559 --> 00:03:43,920
If there is a cave, one day it will fall!

94
00:03:43,920 --> 00:03:45,760
But it wasn't a technical study,

95
00:03:45,760 --> 00:03:47,679
something that could

96
00:03:48,400 --> 00:03:50,559
provoke a discussion in society

97
00:03:50,559 --> 00:03:52,039
or technical bodies, etc.

98
00:03:52,039 --> 00:03:52,800
There wasn't...

99
00:03:52,800 --> 00:03:57,400
There's a study by Ivan Fernando de Lima from the 1950s,

100
00:03:58,320 --> 00:04:00,360
I think it's in Geografia de Alagoas.

101
00:04:00,360 --> 00:04:01,320
Ivan Fernandes de Lima...

102
00:04:01,320 --> 00:04:04,039
I'm young, but he was my Geography teacher.

103
00:04:05,960 --> 00:04:06,760
And Ivan

104
00:04:07,480 --> 00:04:10,559
warned of this so-called tectonic fault

105
00:04:10,559 --> 00:04:11,719
that exists in this region,

106
00:04:11,719 --> 00:04:14,599
in this channel of the Rio Mundaú, if I'm not mistaken,

107
00:04:15,000 --> 00:04:18,000
<b>cutting through Maceió.</b>

108
00:04:18,599 --> 00:04:21,039
And when these problems arose,

109
00:04:21,039 --> 00:04:22,920
I remember writing a warning text:

110
00:04:22,920 --> 00:04:24,320
look, we should check that,

111
00:04:24,320 --> 00:04:27,559
because these caves collapsing

112
00:04:27,559 --> 00:04:29,519
could have their origin

113
00:04:29,519 --> 00:04:33,159
in some accommodation around this tectonic fault, right?

114
00:04:33,159 --> 00:04:37,559
And from then on, it started to give in, right?

115
00:04:38,639 --> 00:04:41,000
So anyway the study was conducted

116
00:04:41,000 --> 00:04:44,079
and it was either not taken into account

117
00:04:44,840 --> 00:04:47,920
or there were other studies opposing it

118
00:04:47,920 --> 00:04:48,519
and saying

119
00:04:49,440 --> 00:04:50,639
<b>it didn't affect it, right??</b>


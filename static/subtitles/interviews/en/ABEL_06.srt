1
00:00:00,280 --> 00:00:01,880
<b>Drilling is done</b>

2
00:00:02,119 --> 00:00:03,519
<b>just like in Petrobras</b>

3
00:00:03,519 --> 00:00:06,119
<b>when they drill oil wells.</b>

4
00:00:06,119 --> 00:00:08,239
<b>Or, to be more clear,</b>

5
00:00:08,559 --> 00:00:11,199
<b>like all artesian well companies</b>

6
00:00:11,920 --> 00:00:13,119
<b>drill the ground.</b>

7
00:00:13,920 --> 00:00:16,280
<b>With a drill that goes deep into the ground.</b>

8
00:00:16,280 --> 00:00:18,559
<b>Well, in the case of Salgema,</b>

9
00:00:18,559 --> 00:00:22,320
<b>this drill is about 30 cm</b>

10
00:00:23,360 --> 00:00:25,039
<b>in diameter, right? It's a drill.</b>

11
00:00:25,800 --> 00:00:27,800
<b>And this drill goes into the ground,</b>

12
00:00:27,800 --> 00:00:29,440
<b>into the rocks,</b>

13
00:00:29,440 --> 00:00:31,320
<b>and carries it with it,</b>

14
00:00:31,320 --> 00:00:32,360
<b>pulls it.</b>

15
00:00:32,360 --> 00:00:34,719
<b>Coupled to this drill, two tubes:</b>

16
00:00:35,000 --> 00:00:38,320
<b>one with about 12 cm in diameter</b>

17
00:00:38,320 --> 00:00:39,360
<b>and the other tube of 30cm,</b>

18
00:00:39,360 --> 00:00:41,360
<b>all stuck there.</b>

19
00:00:41,920 --> 00:00:44,320
<b>They are carried, pulled by this drill.</b>

20
00:00:44,320 --> 00:00:46,280
<b>The drill pierces through the rocks</b>

21
00:00:47,039 --> 00:00:49,119
<b>in a washing process,</b>

22
00:00:49,119 --> 00:00:49,920
<b>with water, you know?</b>

23
00:00:49,920 --> 00:00:52,920
<b>It is impossible to drill without water,</b>

24
00:00:52,920 --> 00:00:54,639
<b>because of the temperature,</b>

25
00:00:54,639 --> 00:00:56,159
<b>the abrasion raises it too high.</b>

26
00:00:56,639 --> 00:00:58,480
<b>So the drilling is</b>

27
00:00:58,480 --> 00:01:01,480
<b>just like in an artesian well.</b>

28
00:01:01,480 --> 00:01:02,039
<b>Just the same.</b>

29
00:01:02,039 --> 00:01:05,039
<b>Except that artesian wells aren't that deep, right?</b>

30
00:01:05,039 --> 00:01:06,880
<b>So you drill and drill and drill...</b>

31
00:01:06,880 --> 00:01:10,800
<b>Then when it reaches around 950 m,</b>

32
00:01:10,800 --> 00:01:13,239
<b>you get rock salt.</b>

33
00:01:14,360 --> 00:01:17,360
<b>Tecnically, you get halite.</b>

34
00:01:18,039 --> 00:01:20,320
<b>Whenever you hear halite, think rock salt.</b>

35
00:01:20,320 --> 00:01:22,639
<b>It is the salt from the ground.</b>

36
00:01:22,639 --> 00:01:25,639
<b>it starts to come up at around 950 m.</b>

37
00:01:25,639 --> 00:01:28,039
<b>It is still a little contaminated,</b>

38
00:01:28,039 --> 00:01:30,559
<b>but it gets much nicer after 1000 m,</b>

39
00:01:30,559 --> 00:01:31,840
<b>much purer.</b>

40
00:01:32,199 --> 00:01:34,599
<b>The average thickness of this layer</b>

41
00:01:34,599 --> 00:01:36,960
<b>is around 250 m.</b>

42
00:01:38,000 --> 00:01:41,000
<b>The thickness of the salt layer down there,</b>

43
00:01:42,239 --> 00:01:44,079
<b>250 m, is the average.</b>

44
00:01:44,079 --> 00:01:45,880
<b>So, in one corner it's 200 m,</b>

45
00:01:45,880 --> 00:01:47,079
<b>in another it's 280 m...</b>

46
00:01:47,280 --> 00:01:50,599
<b>The maximum is 280 m, so 250 m would be a good value.</b>

47
00:01:51,000 --> 00:01:52,480
<b>Then it keeps drilling.</b>

48
00:01:52,480 --> 00:01:55,480
<b>The drill enters the salt layer</b>

49
00:01:55,519 --> 00:01:58,519
<b>and keeps drilling, more and more,</b>

50
00:01:58,519 --> 00:02:03,199
<b>until it reaches the base of the saline rock, 250 m away.</b>

51
00:02:04,840 --> 00:02:08,920
<b>Saline rock is like these solvents,</b>

52
00:02:08,960 --> 00:02:11,480
<b>Antacids</b>

53
00:02:11,480 --> 00:02:14,480
<b>and others of the sort...</b>

54
00:02:16,400 --> 00:02:17,840
<b>Effervescent, you know?</b>

55
00:02:17,840 --> 00:02:19,800
<b>It's the same thing. You keep it away from water!!</b>

56
00:02:20,760 --> 00:02:23,840
<b>In its natural state, without anyone messing with it,</b>

57
00:02:23,840 --> 00:02:27,000
<b>it has the resistance of excellent concrete,</b>

58
00:02:28,119 --> 00:02:30,480
<b>hard concrete!</b>

59
00:02:30,480 --> 00:02:32,519
<b>I know you are not familiar, but an engineer knows:</b>

60
00:02:32,760 --> 00:02:34,639
<b>it would be a 22 mpa concrete.</b>

61
00:02:34,960 --> 00:02:36,679
<b>Highy resistant.</b>

62
00:02:37,199 --> 00:02:38,320
<b>It is great concrete.</b>

63
00:02:39,000 --> 00:02:40,360
<b>But when</b>

64
00:02:40,360 --> 00:02:43,079
<b>the water hits the concrete...</b>

65
00:02:44,039 --> 00:02:45,519
<b>Right by the end,</b>

66
00:02:45,519 --> 00:02:49,880
<b>very close to hitting the bottom,</b>

67
00:02:50,599 --> 00:02:54,400
<b>at approximately 1250 m, ok?</b>

68
00:02:54,960 --> 00:02:57,280
<b>It stops drilling</b>

69
00:02:57,280 --> 00:02:59,239
<b>and that jet,</b>

70
00:02:59,719 --> 00:03:02,039
<b>that water jet,</b>

71
00:03:02,440 --> 00:03:06,519
<b>starts circling</b>

72
00:03:06,960 --> 00:03:08,000
<b>more and more,</b>

73
00:03:08,000 --> 00:03:11,960
<b>and as it circulates, it forms the brine.</b>

74
00:03:12,880 --> 00:03:15,880
<b>The salt that used to be a rock begins to liquify.</b>

75
00:03:16,039 --> 00:03:18,519
<b>Then you get salt with water.</b>

76
00:03:18,920 --> 00:03:20,000
<b>Salt with water</b>

77
00:03:20,760 --> 00:03:22,840
<b>that we call a "brine".</b>

78
00:03:23,239 --> 00:03:25,480
<b>Do you remember when I said that there are two tubes:</b>

79
00:03:25,480 --> 00:03:28,239
<b>one of 12 cm and one of 30 cm.</b>

80
00:03:28,840 --> 00:03:29,920
<b>They're there together.</b>

81
00:03:30,000 --> 00:03:33,039
<b>The water goes through the 12 cm tube,</b>

82
00:03:34,000 --> 00:03:34,559
<b>right?</b>

83
00:03:34,719 --> 00:03:36,320
<b>Then when it gets down there</b>

84
00:03:36,320 --> 00:03:38,480
<b>and that mound of water starts to form there,</b>

85
00:03:38,480 --> 00:03:40,079
<b>rock salt, right?</b>

86
00:03:40,079 --> 00:03:42,119
<b>No, sorry, the brine...</b>

87
00:03:43,599 --> 00:03:45,840
<b>That is a closed environment down there,</b>

88
00:03:45,840 --> 00:03:46,880
<b>confined,</b>

89
00:03:46,880 --> 00:03:48,000
<b>with nowhere to go.</b>

90
00:03:48,000 --> 00:03:49,400
<b>So where does it go?</b>

91
00:03:49,400 --> 00:03:51,840
<b>Water is pressured through</b>

92
00:03:51,840 --> 00:03:53,440
<b>the small tube,</b>

93
00:03:53,440 --> 00:03:55,400
<b>then there is the large tube.</b>

94
00:03:55,559 --> 00:03:59,880
<b>Rock salt comes up through the large tube.</b>

95
00:04:00,440 --> 00:04:03,559
<b>By rock salt I mean the brine.</b>

96
00:04:03,800 --> 00:04:04,679
<b>It comes up,</b>

97
00:04:04,679 --> 00:04:06,039
<b>splashes through the surface</b>

98
00:04:06,039 --> 00:04:09,480
<b>and goes to the industry.</b>

99
00:04:09,639 --> 00:04:10,360
<b>Got it?</b>

100
00:04:10,360 --> 00:04:12,159
<b>That's the process.</b>

101
00:04:13,800 --> 00:04:15,480
<b>Now, there's one important detail.</b>

102
00:04:15,480 --> 00:04:18,920
<b>How big is that diameter?</b>

103
00:04:18,920 --> 00:04:21,760
<b>Because it keeps circulating</b>

104
00:04:21,760 --> 00:04:23,280
<b>and getting bigger...</b>

105
00:04:23,280 --> 00:04:25,480
<b>And then, when it reaches</b>

106
00:04:25,480 --> 00:04:26,880
<b>the diameter they want,</b>

107
00:04:26,880 --> 00:04:29,159
<b>instead of throwing water in there,</b>

108
00:04:29,159 --> 00:04:32,400
<b>oil gets in there.</b>

109
00:04:32,400 --> 00:04:33,480
<b>Oil.</b>

110
00:04:34,880 --> 00:04:37,119
<b>An oil under pressure,</b>

111
00:04:37,119 --> 00:04:39,679
<b>and this oil can</b>

112
00:04:39,679 --> 00:04:42,679
<b>reach that wall where it is already...</b>

113
00:04:42,679 --> 00:04:44,960
<b>Where it stopped dissolving...</b>

114
00:04:44,960 --> 00:04:47,000
<b>This oil will waterproof that wall</b>

115
00:04:47,000 --> 00:04:48,719
<b>to not allow</b>

116
00:04:48,719 --> 00:04:50,679
<b>that diameter to increase any more, you know?</b>


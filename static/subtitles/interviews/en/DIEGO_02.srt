1
00:00:00,750 --> 00:00:04,629
I work in the evaluation
of environmental impacts.

2
00:00:05,588 --> 00:00:08,091
My early research and studies

3
00:00:08,091 --> 00:00:11,261
were on mining impact assessment.

4
00:00:12,262 --> 00:00:16,891
But in this specific case,
I work with mining cases

5
00:00:17,392 --> 00:00:20,937
in the Brazilian Amazon in Colombia
and in Peru.

6
00:00:22,397 --> 00:00:26,651
I had a social experience,

7
00:00:26,651 --> 00:00:28,862
I could even say a little

8
00:00:29,696 --> 00:00:34,659
exhausting, for the type of...
it's a standard when it comes to mining,

9
00:00:35,326 --> 00:00:38,288
be it medium or large scale mining.

10
00:00:38,621 --> 00:00:40,206
It has a standard of...

11
00:00:40,874 --> 00:00:43,460
When you take part in it,
you see how it

12
00:00:43,918 --> 00:00:45,003
takes a toll on

13
00:00:46,588 --> 00:00:49,257
the residents of the area. At AIA,

14
00:00:49,841 --> 00:00:53,053
at the Environmental Impact Assessment,
we call them residents...

15
00:00:53,219 --> 00:00:56,222
whether from the directly affected area
or the area of

16
00:00:56,222 --> 00:00:59,017
​​direct or indirect influence and...

17
00:01:00,727 --> 00:01:04,022
I stopped, I got tired, honestly...

18
00:01:05,023 --> 00:01:07,692
with the theme.

19
00:01:07,692 --> 00:01:09,778
Until this case

20
00:01:09,778 --> 00:01:14,824
in Maceió, I, for one, didn't expect
a mining case

21
00:01:14,824 --> 00:01:18,203
that would generate such an impact

22
00:01:18,203 --> 00:01:22,290
with the magnitude it took
especially in Maceió. Here in the city.

23
00:01:22,999 --> 00:01:25,210
Because usually these...

24
00:01:25,502 --> 00:01:28,213
cases, they take place in more remote areas.

25
00:01:28,963 --> 00:01:31,633
They are not urban, for example.

26
00:01:32,675 --> 00:01:36,679
And I just received an invitation, in 2019

27
00:01:36,805 --> 00:01:38,056
I guess, can't remember,

28
00:01:38,723 --> 00:01:41,726
to take part in a

29
00:01:41,726 --> 00:01:44,813
UN Habitat initiative
in their office in Maceió

30
00:01:45,522 --> 00:01:48,149
to participate in what would be the
Meeting of Urban Thinkers

31
00:01:48,900 --> 00:01:53,530
to deal precisely with this case
called the Pinheiro Case.

32
00:01:53,571 --> 00:01:56,282
But let's call it
the Braskem Case,

33
00:01:56,825 --> 00:01:59,994
because when we talk about
the neighborhood or neighborhoods...

34
00:02:00,954 --> 00:02:03,289
We have to name names,

35
00:02:03,289 --> 00:02:06,668
because who is causing the impact, right?
Who made that impact?

36
00:02:08,128 --> 00:02:10,338
That's why I accepted the invitation.

37
00:02:10,463 --> 00:02:14,509
When you accept the invitation, you think:
"am I going back to that research topic?"

38
00:02:15,635 --> 00:02:19,639
But this experience I had...
I had a project

39
00:02:19,639 --> 00:02:23,935
funded by CNPq in 2013
that allowed me to go to mining sites:

40
00:02:24,435 --> 00:02:28,106
Some patterns come up,
as I said.

41
00:02:28,982 --> 00:02:31,651
And I had not done
field research yet.

42
00:02:31,651 --> 00:02:35,029
No data collection,
but I raised some hypotheses.

43
00:02:35,780 --> 00:02:41,035
Such as: what was mining like in an urban area?
What was likely to happen?

44
00:02:42,829 --> 00:02:46,291
There would be an increase
in the reporting of anxiety disorders,

45
00:02:46,583 --> 00:02:50,003
an increase in reported cases
of depression, for example,

46
00:02:50,420 --> 00:02:54,215
an increase in reported cases

47
00:02:54,424 --> 00:02:56,634
of suicidal ideation,

48
00:02:57,927 --> 00:03:00,972
among other problems,
such as an increase in cases of hypertension,

49
00:03:01,139 --> 00:03:05,351
a series of other situations
that have an impact on health. On account of what?

50
00:03:06,186 --> 00:03:10,190
The Braskem case
that hit these neighborhoods

51
00:03:11,065 --> 00:03:14,402
is in an area already
densely populated.

52
00:03:15,195 --> 00:03:17,989
This wasn't an area
that started being occupied

53
00:03:18,406 --> 00:03:20,783
after the expedition of

54
00:03:21,451 --> 00:03:23,870
mining authorizations.

55
00:03:24,162 --> 00:03:26,831
People already lived there.

56
00:03:26,831 --> 00:03:29,209
When I started to minimally observe

57
00:03:29,667 --> 00:03:33,963
what the variables would be
with these indicators, 

58
00:03:33,963 --> 00:03:36,007
I noted: look, this is what's
going to happen.


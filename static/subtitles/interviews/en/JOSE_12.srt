1
00:00:00,039 --> 00:00:03,119
I had spent 20 years

2
00:00:03,119 --> 00:00:05,880
in Bahia.

3
00:00:06,519 --> 00:00:09,960
I had applied for the university there

4
00:00:10,679 --> 00:00:13,159
<b>and was headquartered in Bahia.</b>

5
00:00:13,880 --> 00:00:15,360
But during this whole period

6
00:00:15,360 --> 00:00:17,199
I traveled a lot,

7
00:00:19,039 --> 00:00:22,239
because I was invited to international conferences

8
00:00:22,239 --> 00:00:24,760
to deal with other matters, right?

9
00:00:24,760 --> 00:00:26,960
Just to give you an idea,

10
00:00:26,960 --> 00:00:32,280
I went to Israel by invitation and also to Iran

11
00:00:33,119 --> 00:00:35,440
by invitation.

12
00:00:35,440 --> 00:00:40,599
But I was headquartered in Feira de Santana,

13
00:00:43,119 --> 00:00:43,840
<b>Bahia.</b>

14
00:00:44,639 --> 00:00:48,119
So, after 50 years

15
00:00:48,119 --> 00:00:50,239
of teaching activity,

16
00:00:52,239 --> 00:00:54,599
working in scientific research

17
00:00:54,599 --> 00:00:56,800
with ecology and the environment,

18
00:00:56,800 --> 00:00:58,760
I decided to retire.

19
00:01:00,679 --> 00:01:02,039
<b>I applied for retirement</b>

20
00:01:02,960 --> 00:01:04,480
<b>in Feira de Santana.</b>

21
00:01:05,199 --> 00:01:06,960
There was no reason

22
00:01:06,960 --> 00:01:08,800
other than sentimental

23
00:01:08,800 --> 00:01:10,440
to go back do Maceió.

24
00:01:11,280 --> 00:01:13,440
We had a house in Pinheiro,

25
00:01:13,440 --> 00:01:15,360
which had been our family home

26
00:01:15,360 --> 00:01:17,360
since we got married,

27
00:01:17,360 --> 00:01:21,119
and a place to rest

28
00:01:21,119 --> 00:01:23,519
and have fun on vacation for my children,

29
00:01:23,519 --> 00:01:25,159
who loved Maceió.

30
00:01:25,159 --> 00:01:27,360
And my wife has always had

31
00:01:27,360 --> 00:01:29,599
a very strong

32
00:01:30,280 --> 00:01:31,840
emotional bond with Maceió,

33
00:01:31,840 --> 00:01:33,119
including a family one.

34
00:01:33,119 --> 00:01:36,320
It was sort of intuitive, so I came back.

35
00:01:38,599 --> 00:01:40,639
When it was close to one month

36
00:01:40,639 --> 00:01:42,119
since I was here

37
00:01:42,880 --> 00:01:44,519
<b>at the house in Pinheiro,</b>

38
00:01:45,599 --> 00:01:49,480
look, I take naps, alright?

39
00:01:49,480 --> 00:01:51,920
So I was taking my nap

40
00:01:52,920 --> 00:01:56,239
<b>when I heard a bang.</b>

41
00:01:57,320 --> 00:02:02,199
Guys, it didn't sound as a normal earthquake,

42
00:02:02,199 --> 00:02:04,639
if there's such a thing as a normal earthquake.

43
00:02:04,639 --> 00:02:07,360
It was a bang,

44
00:02:07,360 --> 00:02:12,000
but a really loud, very strong bang.

45
00:02:13,119 --> 00:02:14,480
<b>I didn't know what it was.</b>

46
00:02:15,159 --> 00:02:16,480
So I got up

47
00:02:16,480 --> 00:02:21,000
and I got to the balcony

48
00:02:21,000 --> 00:02:25,519
and called out the family.

49
00:02:25,519 --> 00:02:27,840
And my wife said: Betinha, here!

50
00:02:27,840 --> 00:02:30,800
Hortência, here! João Maurício, here!

51
00:02:30,800 --> 00:02:32,760
So we're all home?

52
00:02:32,760 --> 00:02:35,199
Then I'll go back to sleep.

53
00:02:38,039 --> 00:02:40,159
<b>So I did. Soon after,</b>

54
00:02:40,960 --> 00:02:43,639
the press started reporting

55
00:02:43,639 --> 00:02:48,639
on TV that there had been an earthquake

56
00:02:48,639 --> 00:02:55,320
of magnitude 2.1 and 2.9, something like that,

57
00:02:55,320 --> 00:02:57,320
which is very unusual.


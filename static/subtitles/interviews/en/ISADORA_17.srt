1
00:00:00,199 --> 00:00:04,199
<b>There is a gigantic space</b>

2
00:00:04,199 --> 00:00:07,519
<b>for each one of us to act.</b>

3
00:00:08,000 --> 00:00:10,719
<b>I don't think there is</b>

4
00:00:10,719 --> 00:00:13,039
<b>a more urgent matter today</b>

5
00:00:13,039 --> 00:00:14,480
<b>than to demarcate the city</b>

6
00:00:14,480 --> 00:00:17,440
<b>that requires our commitment.</b>

7
00:00:17,440 --> 00:00:20,199
<b>So, today,</b>

8
00:00:20,199 --> 00:00:21,599
<b>any of us can act</b>

9
00:00:21,599 --> 00:00:24,159
<b>on behalf of places, on behalf of these people.</b>

10
00:00:24,159 --> 00:00:28,199
<b>So any artist who makes their art</b>

11
00:00:28,199 --> 00:00:30,360
<b>and who lends their voice</b>

12
00:00:30,800 --> 00:00:32,760
<b>to talk about this issue,</b>

13
00:00:32,760 --> 00:00:34,960
<b>ant theater play,</b>

14
00:00:34,960 --> 00:00:36,159
<b>any movie,</b>

15
00:00:36,159 --> 00:00:38,360
<b>any music event,</b>

16
00:00:38,360 --> 00:00:41,599
<b>any musician who can take their voice and,</b>

17
00:00:41,599 --> 00:00:44,840
<b>at some point, bring this issue to the fore,</b>

18
00:00:44,840 --> 00:00:46,719
<b>not let this issue be restricted</b>

19
00:00:46,719 --> 00:00:50,480
<b>to the residents of that region.</b>

20
00:00:50,480 --> 00:00:52,639
<b>So, if you're going to your concert,</b>

21
00:00:52,639 --> 00:00:55,360
<b>if you're going to your event,</b>

22
00:00:55,360 --> 00:00:57,039
<b>if you're going to your presentation,</b>

23
00:00:57,039 --> 00:01:00,639
<b>if you're going to our talk show,</b>

24
00:01:01,079 --> 00:01:02,920
<b>take this issue with you!</b>

25
00:01:02,920 --> 00:01:04,960
<b>If you're going to your soccer game,</b>

26
00:01:04,960 --> 00:01:07,239
<b>take this issue with you!</b>

27
00:01:07,239 --> 00:01:10,239
<b>Commit to it,</b>

28
00:01:10,239 --> 00:01:11,559
<b>lend your voice.</b>

29
00:01:11,559 --> 00:01:13,239
<b>This is everybody's issue.</b>

30
00:01:13,239 --> 00:01:15,159
<b>And it is one</b>

31
00:01:15,159 --> 00:01:17,800
<b>that will only get to</b>

32
00:01:17,800 --> 00:01:19,679
<b>where we want</b>

33
00:01:19,679 --> 00:01:20,960
<b>if all of us get involved.</b>

34
00:01:20,960 --> 00:01:22,960
<b>This issue needs to leave</b>

35
00:01:22,960 --> 00:01:25,199
<b>the scope of residents</b>

36
00:01:25,199 --> 00:01:30,679
<b>and reach the one million people who live in Maceió.</b>

37
00:01:30,920 --> 00:01:32,559
<b>And from these one million people,</b>

38
00:01:32,559 --> 00:01:33,920
<b>it should be taken</b>

39
00:01:33,920 --> 00:01:35,760
<b>to the state of Alagoas as a whole,</b>

40
00:01:35,760 --> 00:01:37,159
<b>and from the state of Alagoes as a whole</b>

41
00:01:37,159 --> 00:01:38,760
<b>to Brazil as a whole,</b>

42
00:01:38,760 --> 00:01:40,239
<b>and from Brazil to the world.</b>

43
00:01:40,920 --> 00:01:42,519
<b>We have to speak up</b>

44
00:01:42,519 --> 00:01:44,320
<b>and bring this issue to the fore.</b>

45
00:01:44,320 --> 00:01:46,079
<b>All of us.</b>


1
00:00:00,039 --> 00:00:02,679
The City Council fluctuated

2
00:00:03,280 --> 00:00:04,840
throughout the process.

3
00:00:08,840 --> 00:00:11,519
We had a major decision

4
00:00:11,519 --> 00:00:13,360
from the City Council,

5
00:00:13,360 --> 00:00:17,599
but I wasn't in charge of the Secretariat anymore.

6
00:00:17,599 --> 00:00:19,559
 Around 80, right?

7
00:00:19,559 --> 00:00:22,280
It's the accession of Kátia Born.

8
00:00:23,119 --> 00:00:25,800
Kátia Born was key

9
00:00:25,800 --> 00:00:27,760
to the support, back then.

10
00:00:27,760 --> 00:00:30,920
I was already involved with Anivaldo Miranda

11
00:00:30,920 --> 00:00:32,760
in the movement for life.


1
00:00:00,400 --> 00:00:03,760
<b>Look, this is</b>

2
00:00:03,760 --> 00:00:07,159
<b>an inevitable contradiction,</b>

3
00:00:08,280 --> 00:00:11,719
<b>but it can be greatly minimized</b>

4
00:00:12,719 --> 00:00:14,559
<b>with mitigation measures.</b>

5
00:00:14,559 --> 00:00:16,840
<b>with preventive measures,</b>

6
00:00:16,840 --> 00:00:19,079
<b>it can be greatly minimized.</b>

7
00:00:19,920 --> 00:00:23,159
<b>Indeed, it is on a rise</b>

8
00:00:24,719 --> 00:00:26,320
<b>and has entered a decline,</b>

9
00:00:26,320 --> 00:00:29,079
<b>in a position that we have been following</b>

10
00:00:29,079 --> 00:00:31,719
<b>for quite some time now.</b>

11
00:00:32,400 --> 00:00:35,719
<b>A position launched</b>

12
00:00:35,719 --> 00:00:38,559
<b>by Professor Ignacy Sachs,</b>

13
00:00:38,559 --> 00:00:42,599
<b>who came to Maceió a few times,</b>

14
00:00:43,920 --> 00:00:47,480
<b>the proposal of eco-development.</b>

15
00:00:48,320 --> 00:00:51,559
<b>We are not anti-development.</b>

16
00:00:52,199 --> 00:00:53,599
<b>But then we ask:</b>

17
00:00:53,599 --> 00:00:56,039
<b>what kind of development?</b>


1
00:00:00,079 --> 00:00:03,119
<b>Still talking about this city dynamics.</b>

2
00:00:03,119 --> 00:00:05,119
<b>We saw that the maps</b>

3
00:00:05,119 --> 00:00:08,960
<b>that were produced after the studies progressed,</b>

4
00:00:08,960 --> 00:00:11,039
<b>these maps setting the priority actions,</b>

5
00:00:11,039 --> 00:00:14,039
<b>they have been changing from 2018 to now.</b>

6
00:00:14,039 --> 00:00:17,800
<b>Obviously increasing the affected area every day,</b>

7
00:00:17,800 --> 00:00:21,679
<b>increasing the number of ​families and land to be vacated.</b>

8
00:00:21,679 --> 00:00:24,559
<b>And then it started to appear on these maps</b>

9
00:00:24,559 --> 00:00:26,960
<b>a field of ​​social isolation.</b>

10
00:00:27,840 --> 00:00:29,719
<b>I'd like you to tell us a little bit</b>

11
00:00:29,719 --> 00:00:33,039
<b>about this area that is not displaced,</b>

12
00:00:33,039 --> 00:00:34,599
<b>where people are not removed,</b>

13
00:00:34,599 --> 00:00:38,360
<b>but they become isolated areas.</b>

14
00:00:38,360 --> 00:00:39,639
<b>What's the impact of that?</b>

15
00:00:40,400 --> 00:00:43,079
<b>It's like you have a belt</b>

16
00:00:43,079 --> 00:00:45,800
<b>around the most problematic area.</b>

17
00:00:45,800 --> 00:00:48,880
<b>It is an area of ​​decreased flow.</b>

18
00:00:49,679 --> 00:00:52,199
<b>You start to have...</b>

19
00:00:52,199 --> 00:00:55,559
<b>It's a buffer zone, so to speak, isn't it?</b>

20
00:00:55,559 --> 00:00:56,599
<b>You start to decrease...</b>

21
00:00:56,599 --> 00:00:58,960
<b>Most likely, what will have</b>

22
00:00:58,960 --> 00:01:01,679
<b>the strongest impact on this area in urban terms,</b>

23
00:01:01,679 --> 00:01:03,000
<b>urbanistically speaking,</b>

24
00:01:03,000 --> 00:01:06,079
<b>are the parameters, right?</b>

25
00:01:06,079 --> 00:01:07,679
<b>Urban indexes and parameters.</b>

26
00:01:07,679 --> 00:01:09,599
<b>So you will probably have</b>

27
00:01:09,599 --> 00:01:11,519
<b>low traffic mobility indexes</b>

28
00:01:11,519 --> 00:01:12,840
<b>and parameters.</b>

29
00:01:13,599 --> 00:01:15,239
<b>So, you have to slow down,</b>

30
00:01:15,239 --> 00:01:18,679
<b>restrict the heavy and intense traffic in these areas.</b>

31
00:01:18,679 --> 00:01:21,800
<b>And it should also establish urban parameters</b>

32
00:01:21,800 --> 00:01:25,360
<b>of less density.</b>

33
00:01:25,360 --> 00:01:28,239
<b>And constructions with greater height</b>

34
00:01:28,239 --> 00:01:33,159
<b>will certainly not be released, for example.</b>

35
00:01:33,159 --> 00:01:35,920
<b>So you start to have</b>

36
00:01:35,920 --> 00:01:40,159
<b>an area of ​​low density</b>

37
00:01:40,159 --> 00:01:41,320
<b>and low circulation.</b>

38
00:01:41,320 --> 00:01:43,519
<b>It's most likely to happen in this region.</b>

39
00:01:43,559 --> 00:01:47,000
<b>To ensure that there are no major impacts</b>

40
00:01:47,000 --> 00:01:49,679
<b>from this more intense traffic,</b>

41
00:01:49,679 --> 00:01:51,599
<b>from this more intense construction,</b>

42
00:01:51,599 --> 00:01:53,639
<b>on the edge of the phenomenon</b>

43
00:01:53,639 --> 00:01:55,840
<b>it's a precaution, so to speak.</b>

44
00:01:55,840 --> 00:01:57,000
<b>It is a precaution,</b>

45
00:01:57,000 --> 00:02:00,360
<b>considering that you'll be on the threshold</b>

46
00:02:00,360 --> 00:02:03,320
<b>of an area considered geologically problematic.</b>

47
00:02:04,800 --> 00:02:07,679
<b>Does this also affect the people who live in these areas?</b>

48
00:02:08,119 --> 00:02:11,599
<b>So this affects real estate, for example.</b>

49
00:02:11,599 --> 00:02:13,639
<b>People will have to, like...</b>

50
00:02:15,000 --> 00:02:15,960
<b>How can I say?</b>

51
00:02:15,960 --> 00:02:19,480
<b>For our logic, nowadays</b>

52
00:02:19,480 --> 00:02:21,119
<b>in urban occupation</b>

53
00:02:21,119 --> 00:02:25,880
<b>building high rises is very important,</b>

54
00:02:25,880 --> 00:02:27,960
<b>building housing developments</b>

55
00:02:27,960 --> 00:02:31,320
<b>in buildings is very important.</b>

56
00:02:31,320 --> 00:02:33,039
<b>If you have an area</b>

57
00:02:33,039 --> 00:02:35,400
<b>where buildings cannot be built,</b>

58
00:02:35,400 --> 00:02:36,159
<b>for example,</b>

59
00:02:36,159 --> 00:02:37,760
<b>the tendency is to</b>

60
00:02:37,760 --> 00:02:39,559
<b>devalue properties in that area</b>

61
00:02:39,559 --> 00:02:41,039
<b>for the real estate market.</b>

62
00:02:41,639 --> 00:02:43,760
<b>So people in that area</b>

63
00:02:43,760 --> 00:02:47,480
<b>will have their properties devalued</b>

64
00:02:47,920 --> 00:02:51,159
<b>when these indexes are applied.</b>

65
00:02:51,159 --> 00:02:54,000
<b>Now, for their lives,</b>

66
00:02:54,000 --> 00:02:56,360
<b>I think the effect is much more psychological.</b>

67
00:02:57,199 --> 00:03:01,000
<b>You'll feel on the brink of a problem.</b>

68
00:03:02,880 --> 00:03:04,480
<b>You feel surrounded by a problem.</b>

69
00:03:04,480 --> 00:03:07,199
<b>Look, if my area is a buffer area</b>

70
00:03:07,199 --> 00:03:08,880
<b>and I'm living in a place</b>

71
00:03:08,880 --> 00:03:10,639
<b>where there can't be a lot of traffic</b>

72
00:03:10,639 --> 00:03:12,880
<b>and there can't be a lot of construction,</b>

73
00:03:13,719 --> 00:03:15,519
<b>because I'm on the threshold</b>

74
00:03:15,519 --> 00:03:17,599
<b>of a geologically problematic area,</b>

75
00:03:17,599 --> 00:03:19,480
<b>then the tendency</b>

76
00:03:19,480 --> 00:03:21,679
<b>is for me to always stay feeling insecure.</b>

77
00:03:22,719 --> 00:03:25,000
<b>And that's a very bad thing.</b>

78
00:03:25,000 --> 00:03:27,559
<b>The tendency is that people, for example,</b>

79
00:03:27,559 --> 00:03:30,719
<b>don't feel comfortable investing</b>

80
00:03:30,719 --> 00:03:32,559
<b>in their lives there either.</b>

81
00:03:32,559 --> 00:03:35,360
<b>This can generate psychological problems</b>

82
00:03:35,360 --> 00:03:37,039
<b>and even actual emptying.</b>

83
00:03:37,719 --> 00:03:41,480
<b>The tramway is possibly</b>

84
00:03:41,480 --> 00:03:46,719
<b>a vehicle of greater impact, with a greater flow.</b>

85
00:03:47,159 --> 00:03:48,960
<b>And it goes through these areas.</b>

86
00:03:48,960 --> 00:03:52,480
<b>So what will be the public policy</b>

87
00:03:52,480 --> 00:03:54,719
<b>for the tramway in these places?</b>

88
00:03:55,400 --> 00:03:56,280
<b>Nobody knows yet.</b>

89
00:03:56,280 --> 00:03:59,840
<b>But there is a large portion</b>

90
00:03:59,840 --> 00:04:04,039
<b>of the population that takes the tramway.</b>

91
00:04:05,599 --> 00:04:07,599
<b>So, if there is a restriction</b>

92
00:04:07,599 --> 00:04:09,519
<b>on this modal, which I don't know if there will be,</b>

93
00:04:09,519 --> 00:04:11,840
<b>but if there is a restriction on this modal,</b>

94
00:04:11,840 --> 00:04:15,000
<b>how will this impact those who live in this region?</b>

95
00:04:16,079 --> 00:04:19,800
<b>Bebedouro, Mutange and Bom Parto</b>

96
00:04:19,800 --> 00:04:21,119
<b>are a pathway.</b>

97
00:04:21,119 --> 00:04:24,800
<b>That road that goes up north in Maceió</b>

98
00:04:24,800 --> 00:04:26,599
<b>is actually the only one possible.</b>

99
00:04:27,320 --> 00:04:28,920
<b>There are no other ways.</b>

100
00:04:28,920 --> 00:04:30,280
<b>Everyone says: what about Fernandes Lima?</b>

101
00:04:30,280 --> 00:04:34,880
<b>Fernandes Lima has other axes parallel to it.</b>

102
00:04:35,840 --> 00:04:37,920
<b>Not big ones.</b>

103
00:04:38,480 --> 00:04:40,159
<b>It is the main axis,</b>

104
00:04:40,159 --> 00:04:42,320
<b>but there are others</b>

105
00:04:42,320 --> 00:04:42,920
<b>that you can...</b>

106
00:04:42,920 --> 00:04:46,440
<b>Including that street that goes through</b>

107
00:04:46,440 --> 00:04:49,000
<b>Cambona and through Bom Parto,</b>

108
00:04:49,000 --> 00:04:51,440
<b>Mutange, Bebedouro and goes to</b>

109
00:04:51,440 --> 00:04:52,880
<b>Santo Amélio and so on...</b>

110
00:04:52,880 --> 00:04:55,559
<b>It is an alternative route,</b>

111
00:04:55,559 --> 00:04:58,119
<b>or used to be, to Fernandes Lima.</b>

112
00:04:58,119 --> 00:05:03,679
<b>But there is no alternative to it now.</b>

113
00:05:04,239 --> 00:05:05,679
<b>The biggest impact</b>

114
00:05:05,679 --> 00:05:09,039
<b>is to have people leaving there.</b>

115
00:05:09,039 --> 00:05:10,679
<b>There is no greater impact than that.</b>

116
00:05:12,280 --> 00:05:15,000
<b>But considering the aftermath,</b>

117
00:05:15,000 --> 00:05:18,719
<b>the aftermath of this emptying,</b>

118
00:05:18,719 --> 00:05:20,400
<b>of this displacement,</b>

119
00:05:21,199 --> 00:05:23,679
<b>the impact of this aftermath</b>

120
00:05:23,679 --> 00:05:28,000
<b>is the restrictions for those who stay.</b>

121
00:05:28,000 --> 00:05:30,559
<b>For those who will stay in this area of ​​isolation,</b>

122
00:05:30,559 --> 00:05:31,599
<b>for example.</b>

123
00:05:31,599 --> 00:05:33,760
<b>For those who will be around it.</b>


1
00:00:00,039 --> 00:00:01,960
In the duplication case,

2
00:00:01,960 --> 00:00:05,000
there are also some very interesting things.

3
00:00:07,760 --> 00:00:09,159
<b>Salgema</b>

4
00:00:10,000 --> 00:00:14,079
paid for a TV advertisement

5
00:00:14,079 --> 00:00:17,159
here in the state,

6
00:00:18,320 --> 00:00:21,639
<b>which was a quote by Jacques Cousteau.</b>

7
00:00:23,480 --> 00:00:25,639
<b>Jacques Cousteau would pop on the screen,</b>

8
00:00:26,440 --> 00:00:28,880
and start

9
00:00:28,880 --> 00:00:31,119
giving a speech.

10
00:00:31,880 --> 00:00:34,599
And they based their speech

11
00:00:34,599 --> 00:00:37,400
on Jacques Cousteau's.

12
00:00:39,679 --> 00:00:40,840
<b>It cost nothing.</b>

13
00:00:41,880 --> 00:00:46,199
I was in contact with the Jacques Cousteau foundation

14
00:00:47,039 --> 00:00:48,519
<b>in the United States.</b>

15
00:00:49,159 --> 00:00:52,440
<b>I immediately sent it to them.</b>

16
00:00:53,519 --> 00:00:56,760
So they asked me for all the data.

17
00:00:56,760 --> 00:00:57,960
I sent it.

18
00:00:58,559 --> 00:00:59,719
It was illegal.

19
00:00:59,719 --> 00:01:02,639
They had to pay royalties

20
00:01:02,639 --> 00:01:04,599
to Jacques Cousteau,

21
00:01:04,599 --> 00:01:07,760
and Jacques Cousteau did not authorize

22
00:01:07,760 --> 00:01:10,519
that kind of association.

23
00:01:11,320 --> 00:01:13,320
It's on the news too, it's in the press.

24
00:01:13,320 --> 00:01:15,599
There were several efforts

25
00:01:15,599 --> 00:01:18,320
to convince society.

26
00:01:18,920 --> 00:01:22,320
One of them was this fabricated story

27
00:01:22,320 --> 00:01:25,840
that we didn't understand what duplication was.

28
00:01:28,000 --> 00:01:30,800
Guys, I got it from the start.

29
00:01:30,800 --> 00:01:34,800
I knew that wasn't duplication.

30
00:01:35,519 --> 00:01:37,800
So they created this narrative

31
00:01:37,800 --> 00:01:40,239
and went with it for a long time.

32
00:01:40,239 --> 00:01:42,679
That was a duplication

33
00:01:42,679 --> 00:01:45,039
<b>in volume of production</b>

34
00:01:45,760 --> 00:01:46,880
<b>that was done.</b>

35
00:01:47,320 --> 00:01:50,559
Actually, they doubled the impact, right?

36
00:01:51,639 --> 00:01:53,239
<b>They doubled the impact.</b>


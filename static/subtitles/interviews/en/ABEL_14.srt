1
00:00:01,320 --> 00:00:04,719
<b>Look, that's a tough question</b>

2
00:00:04,719 --> 00:00:06,599
<b>to answer.</b>

3
00:00:06,840 --> 00:00:08,960
<b>In my opinion, at least.</b>

4
00:00:09,920 --> 00:00:11,400
<b>Now, I tell you this:</b>

5
00:00:11,400 --> 00:00:16,760
<b>In Pinheiro, we could have a forest there today.</b>

6
00:00:18,079 --> 00:00:20,599
<b>A park, for people to walk in,</b>

7
00:00:20,599 --> 00:00:22,800
<b>or a real, closed forest.</b>

8
00:00:23,440 --> 00:00:25,840
<b>It would be great for the city!</b>

9
00:00:26,400 --> 00:00:29,119
<b>But Braskem would have to donate it</b>

10
00:00:29,119 --> 00:00:31,159
<b>to the city hall at zero cost.</b>

11
00:00:32,639 --> 00:00:36,199
<b>To make up for all the tragedy it wrought</b>

12
00:00:36,199 --> 00:00:38,400
<b>on 50,000 people.</b>

13
00:00:38,639 --> 00:00:39,719
<b>That's the very least...</b>

14
00:00:40,000 --> 00:00:41,920
<b>All that area in their name,</b>

15
00:00:42,480 --> 00:00:45,840
<b>they could turn it into a forest</b>

16
00:00:45,840 --> 00:00:47,679
<b>for at least the next</b>

17
00:00:47,679 --> 00:00:49,119
<b>ten years.</b>

18
00:00:50,519 --> 00:00:52,280
<b>A big lung for Maceió, right?</b>

19
00:00:52,280 --> 00:00:54,880
<b>Combined with the biomes we already have there...</b>

20
00:00:55,760 --> 00:01:01,480
<b>All that for a mandatory donation to the city hall!</b>

21
00:01:04,039 --> 00:01:05,079
<b>A forest.</b>

22
00:01:06,840 --> 00:01:08,679
<b>I think that's what should be done.</b>

23
00:01:08,679 --> 00:01:09,800
<b>Period.</b>

24
00:01:09,800 --> 00:01:14,239
<b>Then the city hall, if possible,</b>

25
00:01:14,239 --> 00:01:17,239
<b>leaves this forest there or, after ten years, makes a park</b>

26
00:01:17,239 --> 00:01:19,239
<b>by opening some paths in there</b>

27
00:01:19,239 --> 00:01:22,800
<b>like in Trianon.</b>

28
00:01:22,800 --> 00:01:25,000
<b>It's in the middle of Paulista, right?</b>

29
00:01:25,000 --> 00:01:29,320
<b>Or even like Ibirapuera, which I love!</b>

30
00:01:29,519 --> 00:01:31,079
<b>I love Ibirapuera.</b>

31
00:01:31,079 --> 00:01:33,320
<b>There should be a park in here, right?</b>

32
00:01:34,119 --> 00:01:36,679
<b>Professor, this is what you would like</b>

33
00:01:36,679 --> 00:01:39,039
<b>but what do you think will actually happen?</b>

34
00:01:40,559 --> 00:01:43,840
<b>I don't know. Honestly!</b>

35
00:01:43,840 --> 00:01:45,519
<b>I don't know...</b>

36
00:01:45,519 --> 00:01:47,599
<b>One thing I know is that it won't sink!</b>

37
00:01:47,760 --> 00:01:49,599
<b>The Pinheiro will not sink!</b>

38
00:01:49,599 --> 00:01:51,639
<b>On this up-to-date map,</b>

39
00:01:51,639 --> 00:01:55,519
<b>take the most advanced mines</b>

40
00:01:55,519 --> 00:01:58,519
<b>and draw a radius of 950m,</b>

41
00:01:59,480 --> 00:02:03,840
<b>both on the side of Bom-Parto and on the side of Bebedouro.</b>

42
00:02:03,840 --> 00:02:05,840
<b>Take the most advanced mine and trace a radius.</b>

43
00:02:05,840 --> 00:02:08,840
<b>This is based not only on my field observations,</b>

44
00:02:08,840 --> 00:02:11,840
<b>but also on the studies of several</b>

45
00:02:11,840 --> 00:02:13,639
<b>national and international researchers.</b>

46
00:02:13,840 --> 00:02:17,519
<b>That's the affected area.</b>

47
00:02:18,079 --> 00:02:22,719
<b>Now, God knows what will be done in that area.</b>

48
00:02:22,880 --> 00:02:28,280
<b>At least between Belo Horizonte and Fernandes Lima streets,</b>

49
00:02:28,960 --> 00:02:31,960
<b>I don't see any reason, at least,</b>

50
00:02:32,119 --> 00:02:35,320
<b>for people to leave there desperately.</b>

51
00:02:35,920 --> 00:02:38,519
<b>Because the farther they are from the mine,</b>

52
00:02:38,519 --> 00:02:43,199
<b>the smaller the fissures and cracks.</b>

53
00:02:43,199 --> 00:02:47,840
<b>A fissure that reaches there is a very small thing.</b>

54
00:02:47,840 --> 00:02:51,639
<b>The more you move away, the more the effect...</b>

55
00:02:51,639 --> 00:02:54,840
<b>But isn't the rock getting soft?</b>

56
00:02:54,840 --> 00:02:57,280
<b>Yes, but it softens towards the mine.</b>

57
00:02:58,000 --> 00:03:02,039
<b>It doesn't soften towards Fernandes Lima, you know?</b>


1
00:00:00,239 --> 00:00:02,800
<b>Quando a gente estava quase esgotando</b>

2
00:00:02,800 --> 00:00:05,400
<b>isso ainda na coordenação do meio ambiente</b>

3
00:00:05,400 --> 00:00:09,400
<b>quase esgotando as nossas possibilidades</b>

4
00:00:09,400 --> 00:00:11,239
<b>foi que eu descobri...</b>

5
00:00:11,840 --> 00:00:14,800
<b>porque a gente dava essa impressão de:</b>

6
00:00:14,800 --> 00:00:18,679
<b>é o exército!</b>

7
00:00:18,679 --> 00:00:24,000
<b>Porque muitos cientistas e técnicos amigos nossos</b>

8
00:00:24,000 --> 00:00:28,599
<b>passaram a colaborar conosco anonimamente</b>

9
00:00:28,599 --> 00:00:31,920
<b>e nos fornecer dados preciosos.</b>

10
00:00:33,840 --> 00:00:38,280
<b>Dentre eles, um geólogo extremamente competente.</b>

11
00:00:38,280 --> 00:00:41,079
<b> Ele nos passou os estudos dele</b>

12
00:00:41,639 --> 00:00:44,159
<b>e nesses estudos, ele previa</b>

13
00:00:44,159 --> 00:00:48,639
<b>que se não fossem tomados os devidos cuidados</b>

14
00:00:48,639 --> 00:00:51,199
<b>haveria subsidência.</b>

15
00:00:52,519 --> 00:00:55,199
<b>Eu comuniquei isso imediatamente:</b>

16
00:00:56,400 --> 00:00:59,800
<b>que havia risco de subsidência.</b>

17
00:01:00,559 --> 00:01:05,679
<b>E nós apresentamos qual seria uma possível mitigação.</b>

18
00:01:05,679 --> 00:01:06,920
<b>Não é solução</b>

19
00:01:06,920 --> 00:01:08,480
<b>mas em meio em ciência ambiental</b>

20
00:01:08,480 --> 00:01:10,320
<b>a gente trabalha também com mitigação.</b>

21
00:01:10,320 --> 00:01:12,840
<b>Então uma possível mitigação</b>

22
00:01:13,760 --> 00:01:18,960
<b>seria monitorar constantemente</b>

23
00:01:19,679 --> 00:01:20,880
<b>as minas.</b>

24
00:01:21,519 --> 00:01:24,599
<b>Coisa que dizem que está sendo feita</b>

25
00:01:24,599 --> 00:01:26,880
<b>e que foi feita agora.</b>

26
00:01:26,880 --> 00:01:29,880
<b>Mas isso tinha que ser feito no início</b>

27
00:01:29,880 --> 00:01:33,679
<b>e acompanhada pelo órgão do meio ambiente.</b>

28
00:01:34,440 --> 00:01:37,199
<b>Enquanto eu estive no órgão de meio  ambiente</b>

29
00:01:37,199 --> 00:01:39,760
<b>a gente cobrava os relatórios.</b>

30
00:01:40,599 --> 00:01:42,119
<b>Mas podem investigar</b>

31
00:01:42,800 --> 00:01:44,039
<b>depois que eu saí</b>

32
00:01:45,079 --> 00:01:49,199
<b>a salgema nunca encaminhou relatórios</b>

33
00:01:49,199 --> 00:01:52,320
<b>para órgão, é de meio ambiente daqui do estado.</b>


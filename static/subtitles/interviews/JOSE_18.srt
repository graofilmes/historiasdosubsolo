1
00:00:00,519 --> 00:00:03,159
<b>Por exemplo, eu tenho um amigo</b>

2
00:00:04,760 --> 00:00:07,639
<b>que pensa diferente de mim.</b>

3
00:00:07,639 --> 00:00:10,639
<b>E ele chegou a me mandar uma mensagem assim:</b>

4
00:00:10,639 --> 00:00:13,000
<b>Mas a Braskem</b>

5
00:00:13,000 --> 00:00:16,000
<b>é necessária a Maceió.</b>

6
00:00:16,920 --> 00:00:18,960
<b>Aí eu disse: olha, eu sempre lhe considerei</b>

7
00:00:18,960 --> 00:00:20,480
<b>então eu gostaria de ouvir</b>

8
00:00:20,480 --> 00:00:23,480
<b>dessa sua resposta</b>

9
00:00:23,480 --> 00:00:26,679
<b>por quê ela é necessária?</b>

10
00:00:27,159 --> 00:00:29,800
<b>Ele disse: porque alagoas é um estado pobre</b>

11
00:00:29,800 --> 00:00:32,320
<b>então precisa.</b>

12
00:00:33,079 --> 00:00:34,800
<b>Agora, você me explique</b>

13
00:00:35,480 --> 00:00:37,559
<b>nesses anos</b>

14
00:00:37,559 --> 00:00:40,159
<b>de Salgema/Braskem</b>

15
00:00:40,159 --> 00:00:44,400
<b>em o que é que o estado de Alagoas ficou menos pobre?</b>

16
00:00:46,760 --> 00:00:49,199
<b>E ficou menos pobre</b>

17
00:00:49,199 --> 00:00:52,360
<b>por causa da Salgema e da Braskem?</b>

18
00:00:52,360 --> 00:00:55,360
<b>Que já entraram de sola</b>

19
00:00:55,360 --> 00:00:58,360
<b>acabando com uma restinga</b>

20
00:00:58,360 --> 00:01:02,320
<b>e levando os bairros do Trapiche e da Barra</b>

21
00:01:02,320 --> 00:01:05,320
<b>para a desvalorização imediata</b>

22
00:01:06,719 --> 00:01:10,280
<b>e para uma vizinhança perigosa</b>

23
00:01:10,280 --> 00:01:12,440
<b>com o Pontal da Barra?</b>

24
00:01:12,440 --> 00:01:14,320
<b>Está entendendo?</b>

25
00:01:14,320 --> 00:01:17,320
<b>Então não é contra o desenvolvimento!</b>

26
00:01:17,320 --> 00:01:21,719
<b>A pergunta é: que tipo de desenvolvimento?</b>

27
00:01:24,199 --> 00:01:27,199
<b>Essa pergunta que o senhor fez a seu amigo</b>

28
00:01:27,199 --> 00:01:30,400
<b>ela é a grande pergunta que a gente faz aqui.</b>

29
00:01:30,960 --> 00:01:33,159
<b>Aconteceu esse grande desenvolvimento</b>

30
00:01:33,159 --> 00:01:35,800
<b>que a Salgema prometeu pra Alagoas?</b>

31
00:01:35,800 --> 00:01:38,400
<b>Esses empregos que ela diz que são</b>

32
00:01:38,400 --> 00:01:40,440
<b>o grande ganho que nós temos</b>

33
00:01:40,440 --> 00:01:41,960
<b>ele valeu a pena?</b>

34
00:01:41,960 --> 00:01:44,440
<b>Onde é que estão esses recursos?</b>

35
00:01:44,440 --> 00:01:46,719
<b>Olhe, Nina, essa questão dos empregos</b>

36
00:01:46,719 --> 00:01:49,360
<b>nós não negligenciamos</b>

37
00:01:49,360 --> 00:01:51,119
<b>desde o início.</b>

38
00:01:53,760 --> 00:01:57,719
<b>O grande trunfo da Salgema era esse.</b>

39
00:01:58,119 --> 00:02:00,559
<b>Nós vamos gerar</b>

40
00:02:00,559 --> 00:02:02,840
<b>sei lá, tinha um número lá</b>

41
00:02:02,840 --> 00:02:05,719
<b>não sei quantos empregos...</b>

42
00:02:06,039 --> 00:02:08,480
<b>E esse discurso pegou muito.</b>

43
00:02:09,400 --> 00:02:12,159
<b>Mas aí, o nosso discurso</b>

44
00:02:12,159 --> 00:02:16,440
<b>dentro da vertente de ecodesenvolvimento era:</b>

45
00:02:16,440 --> 00:02:22,400
<b>Vai gerar quantos empregos e quantos desempregos?</b>

46
00:02:25,000 --> 00:02:25,880
<b>Por que?</b>

47
00:02:27,239 --> 00:02:29,719
<b>Como é que vai gerar empregos</b>

48
00:02:29,719 --> 00:02:32,519
<b>que permaneçam?</b>

49
00:02:32,840 --> 00:02:34,119
<b>Nós sabemos</b>

50
00:02:34,119 --> 00:02:38,360
<b>que toda a implantação desse porte</b>

51
00:02:38,360 --> 00:02:40,559
<b>ela atrai de imediato</b>

52
00:02:40,559 --> 00:02:44,239
<b>uma quantidade grande de trabalhadores</b>

53
00:02:44,239 --> 00:02:47,760
<b>que vão trabalhar como peões.</b>

54
00:02:49,679 --> 00:02:52,559
<b>Na implantação.</b>

55
00:02:52,559 --> 00:02:56,519
<b>Na inchada, na pá, no que for necessário.</b>

56
00:02:57,480 --> 00:02:58,559
<b>É inevitável.</b>

57
00:02:59,239 --> 00:03:02,239
<b>Agora, isso é na implantação que gera.</b>

58
00:03:03,039 --> 00:03:05,360
<b>Subempregos.</b>

59
00:03:06,159 --> 00:03:08,280
<b>Agora, implantada</b>

60
00:03:08,280 --> 00:03:10,920
<b>e começou a funcionar</b>

61
00:03:10,920 --> 00:03:14,760
<b>ela é altamente automatizada, gente!</b>

62
00:03:15,559 --> 00:03:18,039
<b>Altamente automatizada!</b>

63
00:03:18,719 --> 00:03:20,519
<b>depende de um número</b>

64
00:03:20,519 --> 00:03:23,280
<b>relativamente muito pequeno</b>

65
00:03:23,280 --> 00:03:26,280
<b>de empregados.</b>

66
00:03:27,440 --> 00:03:30,679
<b>Então desde o início nós alertamos pra isso.</b>

67
00:03:31,719 --> 00:03:34,719
<b>Sem falar da questão da renuncia fiscal, não é?</b>

68
00:03:34,719 --> 00:03:37,480
<b>Sim!</b>

69
00:03:37,480 --> 00:03:40,079
<b>Ela passou anos</b>

70
00:03:40,079 --> 00:03:43,079
<b>sem pagar um tostão de imposto.</b>

71
00:03:43,440 --> 00:03:45,800
<b>Acho que vocês sabem que recentemente</b>

72
00:03:45,800 --> 00:03:51,559
<b>foi refeito esse acordo fiscal.</b>

73
00:03:52,320 --> 00:03:55,320
<b>Mas o secretário da fazenda</b>

74
00:03:55,320 --> 00:03:57,280
<b>ao ser questionado</b>

75
00:03:57,280 --> 00:04:00,039
<b>ele disse: realmente foi.</b>

76
00:04:00,880 --> 00:04:05,679
<b>Então realmente o ganho pro estado é muito pequeno.</b>

77
00:04:06,400 --> 00:04:09,079
<b>Mais, eu não posso dizer</b>

78
00:04:09,079 --> 00:04:11,679
<b> pois existem cláiusulas de segredo.</b>

79
00:04:13,400 --> 00:04:15,960
<b>Que, aliás, essa questão de cláusula de segredo</b>

80
00:04:15,960 --> 00:04:21,079
<b>é um dos trunfos da Braskem e da Salgema.</b>

81
00:04:21,599 --> 00:04:23,480
<b>Principalmente da Salgema.</b>

82
00:04:23,480 --> 00:04:26,440
<b>Toda vez que a gente apertava</b>

83
00:04:26,440 --> 00:04:29,320
<b>pra pedir relatórios</b>

84
00:04:29,320 --> 00:04:31,840
<b>ou detalhes no fluxograma</b>

85
00:04:31,840 --> 00:04:34,159
<b>a resposta deles era inevitável:</b>

86
00:04:34,159 --> 00:04:36,280
<b>Isto está protegido</b>

87
00:04:36,280 --> 00:04:38,639
<b>por cláusula de segredo industrial.</b>

88
00:04:38,960 --> 00:04:40,480
<b>E estava mesmo.</b>

89
00:04:42,400 --> 00:04:45,400
<b>Então quem é que ganha com isso? Quem ganhou?</b>

90
00:04:46,519 --> 00:04:47,840
<b>Eu não sei.</b>

91
00:04:47,840 --> 00:04:50,840
<b>Sei que teve gente que ganhou.</b>

92
00:04:52,000 --> 00:04:54,159
<b>Mas está cedo ainda pra falar.</b>


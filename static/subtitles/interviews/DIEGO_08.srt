1
00:00:00,000 --> 00:00:01,251
Mineração e sustentabilidade

2
00:00:01,251 --> 00:00:03,461
é uma contradição de termos.

3
00:00:04,504 --> 00:00:06,756
A mineração

4
00:00:07,048 --> 00:00:09,634
Diego, então você é contra etc e tal.

5
00:00:10,969 --> 00:00:14,514
Eu vou dizer que com o modelo econômico
que a gente vive

6
00:00:15,140 --> 00:00:19,894
altamente dependente de recursos minerais,
a mineração vai ocorrer. Ela vai ocorrer.

7
00:00:21,229 --> 00:00:24,858
Só que qual é o grande problema?
E aí pensando esse caso

8
00:00:25,900 --> 00:00:27,902
é quando o Estado

9
00:00:28,611 --> 00:00:31,031
por meio de seus órgãos licenciadores

10
00:00:31,072 --> 00:00:35,118
ele se exime da responsabilidade
de fazer uma avaliação

11
00:00:36,453 --> 00:00:37,120
absolutamente

12
00:00:37,120 --> 00:00:39,831
criteriosa
sobre os impactos dessa atividade.

13
00:00:40,415 --> 00:00:44,377
E quando a sociedade civil ela não participa
minimamente dos benefícios.

14
00:00:45,170 --> 00:00:49,007
Eu mesmo sou um defensor há muito tempo
de que uma cidade como Maceió

15
00:00:49,340 --> 00:00:53,678
a Braskem tinha que estar pagando royalties
para cada morador aqui da cidade

16
00:00:54,721 --> 00:00:55,805
para cada um.

17
00:00:56,639 --> 00:00:59,768
Quanto menos você tem

18
00:00:59,768 --> 00:01:03,354
investimento na área de monitoramento
e controle ambiental por parte do Estado

19
00:01:04,064 --> 00:01:07,192
e quanto menor
a participação da sociedade civil

20
00:01:08,902 --> 00:01:11,404
Os custos da mineração.

21
00:01:11,863 --> 00:01:14,783
eles ficam ainda maiores
do que os benefícios.

22
00:01:16,201 --> 00:01:17,160
Então

23
00:01:18,078 --> 00:01:20,580
é um grande dilema que existe.

24
00:01:21,164 --> 00:01:23,833
Então, o que é mais fácil?
É você não ter mineração?

25
00:01:23,958 --> 00:01:27,003
Você vai perder a receita?
Vai, vai perder.

26
00:01:27,003 --> 00:01:28,338
O que vai ter no lugar?

27
00:01:28,338 --> 00:01:30,423
As pessoas se perguntam isso.

28
00:01:31,174 --> 00:01:34,594
Elas perguntam isso.
O que é que vai ter no lugar?

29
00:01:34,594 --> 00:01:38,681
e você, quando você não tem uma alternativa
e aí o Brasil está se tornando

30
00:01:38,681 --> 00:01:41,851
um grande, uma grande fazenda, uma grande...

31
00:01:43,103 --> 00:01:45,188
sei lá, uma grande Serra Pelada também.

32
00:01:46,147 --> 00:01:48,316
é só exportação de commodities né?

33
00:01:48,316 --> 00:01:50,610
A gente está se desindustrializando cada vez mais.

34
00:01:51,361 --> 00:01:54,405
Então quando a gente passa a depender
cada vez mais

35
00:01:54,948 --> 00:01:58,576
de atividades de extração primária

36
00:01:58,993 --> 00:02:02,497
como as commodities.
Menores as chances da gente se desenvolver.

37
00:02:03,623 --> 00:02:07,001
E aí até esse desenvolvimento
muitas vezes é confundido com crescimento.

38
00:02:08,086 --> 00:02:11,506
Então por exemplo: a mineração não gera
tantos empregos diretos e indiretos.

39
00:02:12,382 --> 00:02:14,509
Quem gera emprego direto

40
00:02:14,509 --> 00:02:17,470
em uma escala maior
é a pequena e média empresa

41
00:02:18,263 --> 00:02:22,809
que não recebe o mesmo carinho
por exemplo dos órgãos públicos.

42
00:02:23,434 --> 00:02:26,062
Ou seja, terá que baixar o...

43
00:02:26,688 --> 00:02:28,481
aí a gente está até saindo um pouco da sua pergunta

44
00:02:28,481 --> 00:02:31,943
mas tem que baixar imposto
de pequena e média empresa

45
00:02:31,943 --> 00:02:33,444
em escala avassaladora

46
00:02:34,279 --> 00:02:38,616
que é essas que realmente vão gerar
uma externalidade positiva

47
00:02:38,992 --> 00:02:40,410
aumentar empregos.

48
00:02:40,785 --> 00:02:44,080
E a mineração Diego?
Então a mineração tem que acabar etc e tal.

49
00:02:44,706 --> 00:02:47,417
Eu vou ser muito realista:
Não vai. Não vai.

50
00:02:48,835 --> 00:02:51,171
Então o que nós temos que fazer

51
00:02:52,338 --> 00:02:54,757
é colocar diretamente e aproveitar

52
00:02:55,049 --> 00:02:57,886
essas situações produzidas pela mineração

53
00:02:57,886 --> 00:02:59,637
que são esses desastres

54
00:03:00,221 --> 00:03:03,558
e colocar a sociedade civil
diretamente nas empresas

55
00:03:04,184 --> 00:03:06,895
para poder controlar.
Porque só assim

56
00:03:07,979 --> 00:03:09,606
Porque o Estado

57
00:03:09,814 --> 00:03:12,692
o governo, seja o Governo Federal,
seja o Governo Estadual

58
00:03:12,692 --> 00:03:15,904
ele pode por exemplo considerar
que está tudo às mil maravilhas.

59
00:03:16,362 --> 00:03:18,489
A receita está entrando.

60
00:03:18,656 --> 00:03:20,533
Mas tá chegando?

61
00:03:20,533 --> 00:03:22,994
aquela moradora dona Maria

62
00:03:24,037 --> 00:03:26,873
lá do Mutange.
Ela recebeu alguma vez na vida

63
00:03:27,123 --> 00:03:29,709
algum benefício direto a numeração?
Nunca.

64
00:03:30,418 --> 00:03:35,757
Agora a questão é uma mineradora como essa
ela vai criar um conselho

65
00:03:35,757 --> 00:03:38,384
que seja deliberativo?
Porque conselho consultivo...

66
00:03:40,345 --> 00:03:43,473
Agora: um Conselho que seja deliberativo
por exemplo:

67
00:03:44,224 --> 00:03:46,935
para as atividades que ela vai estar

68
00:03:46,935 --> 00:03:48,478
conduzindo agora?

69
00:03:48,478 --> 00:03:50,021
A grande questão é essa.

70
00:03:50,021 --> 00:03:51,522
Que é uma questão factível.

71
00:03:52,023 --> 00:03:53,524
Porque o governo não vai querer perder
receita

72
00:03:53,524 --> 00:03:56,152
Ah não vai? Então a gente
vai trabalhar com o quê?

73
00:03:56,152 --> 00:04:00,823
Então a empresa: cadê o espaço da sociedade
civil ali. Pra esse selo...

74
00:04:01,074 --> 00:04:05,578
SG tá sendo utilizado pelos acionistas

75
00:04:05,662 --> 00:04:08,039
porque a grande questão são os acionistas.

76
00:04:08,623 --> 00:04:11,084
Como assim a empresa está gerando isso?

77
00:04:11,709 --> 00:04:14,212
E teve que pagar o dobro de indenização

78
00:04:15,296 --> 00:04:16,631
extra patrimonial.

79
00:04:16,631 --> 00:04:21,678
Não chegou nem a quinhentos milhões.
Nem quatrocentos milhões de reais.

80
00:04:22,720 --> 00:04:26,266
Os acionistas da Braskem
deram uma bela risada, na minha opnião.

81
00:04:26,557 --> 00:04:30,186
Quando...  que era o valor de indenização

82
00:04:31,813 --> 00:04:33,523
individual.

83
00:04:33,606 --> 00:04:36,067
Eu estou falando dos danos
extra patrimoniais.

84
00:04:37,193 --> 00:04:40,863
Esperamos pelo menos 500 milhões de reais

85
00:04:41,322 --> 00:04:43,700
de danos extra patrimoniais.
Quanto que foi?

86
00:04:45,159 --> 00:04:46,744
Ah Diego, você tem que ser técnico

87
00:04:46,744 --> 00:04:48,663
não pode dar risada.

88
00:04:50,290 --> 00:04:51,708
Não chegou nem a quinhentos milhões.

89
00:04:51,708 --> 00:04:53,584
Ah, chegou a trezentos? Não.

90
00:04:54,335 --> 00:04:55,503
pode ir reduzindo

91
00:04:56,546 --> 00:04:57,463
Então

92
00:04:59,090 --> 00:05:02,135
esse tipo de controle social
é fundamental

93
00:05:02,135 --> 00:05:04,262
porque a mineração vai continuar.

94
00:05:04,846 --> 00:05:08,016
Agora :com participação da sociedade civil ou não?

95
00:05:08,016 --> 00:05:09,892
grande questão.


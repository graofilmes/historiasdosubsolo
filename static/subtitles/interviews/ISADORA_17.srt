1
00:00:00,199 --> 00:00:04,199
<b>Existe aí um espaço gigantesco</b>

2
00:00:04,199 --> 00:00:07,519
<b>para cada uma e cada um de nós atuar.</b>

3
00:00:08,000 --> 00:00:10,719
<b>Eu acho que não existe hoje</b>

4
00:00:10,719 --> 00:00:13,039
<b>uma questão mais urgente</b>

5
00:00:13,039 --> 00:00:14,480
<b>que mais demarca na cidade</b>

6
00:00:14,480 --> 00:00:17,440
<b>que exija de nós o nosso comprometimento.</b>

7
00:00:17,440 --> 00:00:20,199
<b>Então, hoje, qualquer uma</b>

8
00:00:20,199 --> 00:00:21,599
<b>ou qualquer um de nós pode atuar</b>

9
00:00:21,599 --> 00:00:24,159
<b>em prol dos lugares, em prol dessas pessoas.</b>

10
00:00:24,159 --> 00:00:28,199
<b>Então qualquer artista que faça a sua arte</b>

11
00:00:28,199 --> 00:00:30,360
<b>e que empresta a sua voz</b>

12
00:00:30,800 --> 00:00:32,760
<b>para falar dessa questão</b>

13
00:00:32,760 --> 00:00:34,960
<b>qualquer peça de teatro</b>

14
00:00:34,960 --> 00:00:36,159
<b>qualquer filme</b>

15
00:00:36,159 --> 00:00:38,360
<b>qualquer evento de musica</b>

16
00:00:38,360 --> 00:00:41,599
<b>qualquer músico que possa pegar a sua voz</b>

17
00:00:41,599 --> 00:00:44,840
<b>e em algum momento, trazer essa questão à tona</b>

18
00:00:44,840 --> 00:00:46,719
<b>não deixar essa questão ficar restrita</b>

19
00:00:46,719 --> 00:00:50,480
<b>apenas a moradores daquela região.</b>

20
00:00:50,480 --> 00:00:52,639
<b>Então, se você está indo para o seu show</b>

21
00:00:52,639 --> 00:00:55,360
<b>se você está indo para o seu evento</b>

22
00:00:55,360 --> 00:00:57,039
<b>se você está indo para a sua apresentação</b>

23
00:00:57,039 --> 00:01:00,639
<b>se você está indo para o seu programa de comunicação</b>

24
00:01:01,079 --> 00:01:02,920
<b> leve a questão com você!</b>

25
00:01:02,920 --> 00:01:04,960
<b>Se você está indo para o seu jogo de futebol</b>

26
00:01:04,960 --> 00:01:07,239
<b> leve a questão com você!</b>

27
00:01:07,239 --> 00:01:10,239
<b>Vista, a camisa</b>

28
00:01:10,239 --> 00:01:11,559
<b>empreste a sua voz.</b>

29
00:01:11,559 --> 00:01:13,239
<b>Eu acho que essa é uma questão para todo mundo.</b>

30
00:01:13,239 --> 00:01:15,159
<b>É uma questão</b>

31
00:01:15,159 --> 00:01:17,800
<b>que só vai realmente chegar</b>

32
00:01:17,800 --> 00:01:19,679
<b>no que nós queremos que chegue</b>

33
00:01:19,679 --> 00:01:20,960
<b>se todo mundo se envolver.</b>

34
00:01:20,960 --> 00:01:22,960
<b>Essa questão precisa sair</b>

35
00:01:22,960 --> 00:01:25,199
<b>do escopo dos moradores</b>

36
00:01:25,199 --> 00:01:30,679
<b>e precisa ir para o um milhão de pessoas que habita Maceió.</b>

37
00:01:30,920 --> 00:01:32,559
<b>E dessas um milhão de pessoas</b>

38
00:01:32,559 --> 00:01:33,920
<b>é pra ela ser levada</b>

39
00:01:33,920 --> 00:01:35,760
<b>para o estado de Alagoas como um todo</b>

40
00:01:35,760 --> 00:01:37,159
<b>e do estado de Alagoas com um todo</b>

41
00:01:37,159 --> 00:01:38,760
<b>para o Brasil como um todo</b>

42
00:01:38,760 --> 00:01:40,239
<b>e do Brasil para o mundo.</b>

43
00:01:40,920 --> 00:01:42,519
<b>Nós temos que dar voz</b>

44
00:01:42,519 --> 00:01:44,320
<b>e visibilidade a essa questão</b>

45
00:01:44,320 --> 00:01:46,079
<b>todas e todos nós.</b>


1
00:00:00,639 --> 00:00:03,400
Meu nome é Abel Galindo

2
00:00:03,400 --> 00:00:05,679
conhecido como professor Abel Galindo

3
00:00:05,679 --> 00:00:07,920
mas meu nome é Abel Galindo Marques.

4
00:00:08,599 --> 00:00:11,039
A minha formação inicial é engenheiro civil.

5
00:00:12,239 --> 00:00:17,239
Aí depois fiz pós-graduação nessa área de geotecnia

6
00:00:18,119 --> 00:00:20,519
Com conhecimento em geologia.

7
00:00:20,519 --> 00:00:25,880
Fui professor quase 38 anos na UFAL.

8
00:00:26,559 --> 00:00:28,480
E entre as matérias

9
00:00:28,480 --> 00:00:30,199
especialmente nos últimos cinco anos

10
00:00:30,199 --> 00:00:33,760
que eu lecionei foi geologia de engenharia.

11
00:00:34,400 --> 00:00:36,639
Então a minha formação é essa

12
00:00:36,639 --> 00:00:38,480
com a especialidade

13
00:00:38,480 --> 00:00:40,480
egenheiro civil

14
00:00:40,480 --> 00:00:43,320
com pós-graduação, mestrado, etc

15
00:00:43,920 --> 00:00:47,920
nessa área de geotecnia com geologia.

16
00:00:47,920 --> 00:00:51,159
engenheiro geotécnico a

17
00:00:51,159 --> 00:00:53,800
Tive a felicidade de ser o primeiro

18
00:00:54,400 --> 00:00:57,760
ter pós-graduação aqui em Alagoas.

19
00:00:57,760 --> 00:00:59,719
Isso foi em 77.

20
00:00:59,719 --> 00:01:01,800
Na verdade, eu comecei e 75

21
00:01:02,760 --> 00:01:04,639
essa Pós-Graduação.

22
00:01:04,639 --> 00:01:06,960
Então eu tive a Felicidade

23
00:01:06,960 --> 00:01:09,559
de pegar Maceió

24
00:01:09,559 --> 00:01:12,960
com pouquíssimos e pouquíssimos prédios

25
00:01:14,719 --> 00:01:17,000
prédios verticais

26
00:01:17,000 --> 00:01:20,039
prédios com 8, 10, 12 pavimentos

27
00:01:20,039 --> 00:01:20,800
eram pouquíssimos.

28
00:01:20,800 --> 00:01:25,039
Se contar, tinha uns 3 ou 4 ali na praia não é?

29
00:01:25,039 --> 00:01:27,599
E tinha um aqui no Farol

30
00:01:28,679 --> 00:01:29,840
então eram pouquíssimos prédios.

31
00:01:29,840 --> 00:01:32,360
E como a minha especialidade

32
00:01:33,239 --> 00:01:35,400
é geotecnia.

33
00:01:35,400 --> 00:01:38,920
Geotecnia abrange a área de fundações

34
00:01:38,920 --> 00:01:42,800
de edifícios, de pontes, de barragens

35
00:01:42,800 --> 00:01:45,480
a estabilidade das encostas

36
00:01:45,480 --> 00:01:47,400
é isso que é a área de geotecnia.

37
00:01:47,400 --> 00:01:49,960
 Mas para ser um bom geotécnico

38
00:01:49,960 --> 00:01:51,679
tem que ter um conhecimento bom

39
00:01:51,679 --> 00:01:53,599
muito bom em geologia.

40
00:01:54,440 --> 00:01:55,039
Geologia.

41
00:01:55,039 --> 00:01:58,159
Porque, além das matérias

42
00:01:58,159 --> 00:02:00,719
de fundações, de estabilidade, etc

43
00:02:00,719 --> 00:02:03,480
lá no curso de engenharia da UFAL

44
00:02:03,480 --> 00:02:06,000
eu também lecionei muitos anos

45
00:02:06,000 --> 00:02:07,159
geologia de engenharia

46
00:02:07,159 --> 00:02:09,639
por conta do meu conhecimento paralelo, entendeu?

47
00:02:10,039 --> 00:02:11,880
Dizem as boas línguas e é verdade

48
00:02:12,760 --> 00:02:14,639
que 95%

49
00:02:14,639 --> 00:02:19,079
de todos os prédios e edificações de Maceió

50
00:02:21,480 --> 00:02:24,400
<b>tem projeto do senhor Abel Galindo.</b>


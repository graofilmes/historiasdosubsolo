1
00:00:00,119 --> 00:00:01,920
Sempre eu tenho cobrado

2
00:00:01,920 --> 00:00:05,119
mas eu não tenho cobrado somente o licenciamento, Rafael.

3
00:00:05,800 --> 00:00:09,920
Eu tenho cobrado isso publicamente, sempre.

4
00:00:09,920 --> 00:00:13,519
Eu não quero ver só o licenciamento.

5
00:00:14,280 --> 00:00:18,840
Eu quero ver quem assinou o  licenciamento.

6
00:00:19,639 --> 00:00:22,320
Porque o crime começa aí.

7
00:00:22,320 --> 00:00:24,440
Não existia nenhuma lei

8
00:00:24,440 --> 00:00:27,599
nenhuma legislação ambiental no estado de Alagoas.

9
00:00:27,599 --> 00:00:31,559
A legislação ambiental começa a ser implantada

10
00:00:31,559 --> 00:00:33,159
na minha gestão

11
00:00:33,159 --> 00:00:38,360
e avança bastante na legislação do Zé Roberto.

12
00:00:39,039 --> 00:00:41,760
<b>E veio a época da constituinte.</b>

13
00:00:42,400 --> 00:00:46,440
E então na Constituição do estado de Alagoas

14
00:00:46,440 --> 00:00:49,440
entrou a temática de meio ambiente.

15
00:00:50,079 --> 00:00:53,559
Mas é uma coisa que pouca gente também sabe.

16
00:00:53,559 --> 00:00:56,519
A Constituição, peguem para vocês verem

17
00:00:56,519 --> 00:01:00,599
é muito interessante.

18
00:01:01,400 --> 00:01:02,559
<b>Ela é copiada</b>

19
00:01:03,360 --> 00:01:05,679
<b>da Constituição de Sergipe.</b>

20
00:01:07,199 --> 00:01:09,239
Que por sua vez tem equívocos

21
00:01:09,239 --> 00:01:12,079
peguem para você verem, é divertido.

22
00:01:13,639 --> 00:01:18,800
Cabe ao estado de Alagoas proteger as ilhas oceânicas.

23
00:01:20,079 --> 00:01:24,960
Alagoas nunca teve ilha oceânica, minha gente!

24
00:01:24,960 --> 00:01:28,199
Depois veio a legislação

25
00:01:28,199 --> 00:01:31,679
de proteção dos manguezais.

26
00:01:32,800 --> 00:01:35,199
Que ela entra na assembleia

27
00:01:35,199 --> 00:01:39,360
como uma lei conservacionista.

28
00:01:39,960 --> 00:01:43,679
Ou seja, é possível utilizar os manguezais

29
00:01:43,679 --> 00:01:45,880
de uma forma racional

30
00:01:45,880 --> 00:01:50,760
inclusive os pescadores artesanais precisam.

31
00:01:50,760 --> 00:01:53,440
Basta treiná-los para isso.

32
00:01:53,440 --> 00:01:56,719
Mas o deputado Ismael Pereira

33
00:01:56,719 --> 00:02:00,000
na época estava com arroubos

34
00:02:00,000 --> 00:02:02,400
de defesa do meio ambiente

35
00:02:02,400 --> 00:02:05,559
então na seção

36
00:02:05,559 --> 00:02:09,840
 a lei entrou conservacionista e saiu preservacionista.

37
00:02:09,960 --> 00:02:12,679
A lei de proteção dos manguezais de Alagoas

38
00:02:12,679 --> 00:02:14,960
é preservacionista.

39
00:02:14,960 --> 00:02:19,119
Os manguezais são intocáveis.

40
00:02:20,280 --> 00:02:21,599
<b>Nunca funcionou.</b>

41
00:02:22,239 --> 00:02:24,559
<b>Não é? Nunca funcionou.</b>

42
00:02:25,159 --> 00:02:29,239
Inclusive, no caso recente da Braskem

43
00:02:29,239 --> 00:02:32,920
a Braskem cavou Minas em áreas de manguezais

44
00:02:32,920 --> 00:02:37,400
destruiu manguezais ali na região de Bebedouro.

45
00:02:38,519 --> 00:02:39,840
E não foi feito nada

46
00:02:39,840 --> 00:02:42,920
<b>O que é ilegal.</b>

47
00:02:43,119 --> 00:02:46,440
É bem perto da Secretaria de meio ambiente

48
00:02:46,440 --> 00:02:47,599
do IMA.

49
00:02:48,239 --> 00:02:51,199
Então o IMA alegou que não tinha conhecimento

50
00:02:51,199 --> 00:02:53,320
mas olha, é muito próximo.

51
00:02:53,320 --> 00:02:57,920
Eu tinha fotos do drone da minha casa.

52
00:02:57,920 --> 00:03:00,719
O drone da minha casa era meu filho, João Maurício.

53
00:03:01,920 --> 00:03:03,360
Ele subia no telhado

54
00:03:03,360 --> 00:03:05,440
e minha casa era ali no alto, no Pinheiro

55
00:03:05,440 --> 00:03:07,239
e conseguia fotografar

56
00:03:07,239 --> 00:03:09,960
a destruição dos manguezais pela Braskem.

57
00:03:10,719 --> 00:03:13,760
Eu apresentei na Câmara dos vereadores

58
00:03:13,760 --> 00:03:19,119
então a partir disso, o imã resolveu fiscalizar

59
00:03:19,800 --> 00:03:21,320
<b>e reconheceu</b>

60
00:03:22,599 --> 00:03:24,079
e alegou que é porque

61
00:03:24,079 --> 00:03:27,400
a Braskem tinha mandado para eles um projeto

62
00:03:27,400 --> 00:03:30,039
e executado outro.

63
00:03:30,920 --> 00:03:32,320
<b>E aplicou uma multa.</b>

64
00:03:32,960 --> 00:03:34,599
<b>R$ 45.000.</b>


1
00:00:00,039 --> 00:00:02,559
Autorização para funcionamento da Salgema

2
00:00:02,559 --> 00:00:04,360
Ela vem de meados

3
00:00:05,519 --> 00:00:07,320
dos anos 60.

4
00:00:07,320 --> 00:00:08,760
Eu não me lembro se lá

5
00:00:08,760 --> 00:00:11,559
 teve alguma autorização de documento

6
00:00:11,559 --> 00:00:15,840
ou a localização quando foi definida se havia alguma...

7
00:00:15,840 --> 00:00:17,679
Mas claro que o governo do estado autorizou.

8
00:00:17,679 --> 00:00:19,280
Não tenho a menor dúvida que autorizou.

9
00:00:19,280 --> 00:00:22,760
Não sei se o meio legal foi um

10
00:00:23,280 --> 00:00:25,480
uma dessas normais tem hoje

11
00:00:25,480 --> 00:00:27,800
uma autorização de de uso de solo

12
00:00:27,800 --> 00:00:29,599
ou alguma especial pra esse fim

13
00:00:29,599 --> 00:00:31,559
ou se houve outro documento.

14
00:00:31,559 --> 00:00:34,400
Agora, quando houve a duplicação

15
00:00:34,400 --> 00:00:36,760
o projeto de duplicação em que houve reação

16
00:00:37,599 --> 00:00:39,159
eu me lembro que o governador Suruagy

17
00:00:39,159 --> 00:00:40,679
chegou a propor um plebiscito

18
00:00:40,679 --> 00:00:42,119
para a população decidisse

19
00:00:42,119 --> 00:00:43,760
se aceitava ou não essa duplicação.

20
00:00:44,639 --> 00:00:46,920
Ou seja, havia um desgaste político grande

21
00:00:46,920 --> 00:00:48,519
o temor aumentou

22
00:00:48,519 --> 00:00:51,400
em função principalmente, do acidente em 82

23
00:00:52,360 --> 00:00:54,199
E aí houve todo um jogo político

24
00:00:54,199 --> 00:00:56,679
de dividir responsabilidades

25
00:00:56,679 --> 00:01:00,119
e se envolveu a própria assembleia legislativa, não é?

26
00:01:00,719 --> 00:01:02,480
Eu me lembro que eu era vereador.

27
00:01:02,480 --> 00:01:05,800
Eu assumi o mandato em janeiro de 83.

28
00:01:06,639 --> 00:01:07,800
E em 85

29
00:01:07,800 --> 00:01:10,639
 eu que cheguei a criar uma comissão especial de inquérito

30
00:01:10,639 --> 00:01:13,360
que era um aspecto de CPI municipal.

31
00:01:14,599 --> 00:01:16,119
Eu não sei como eu consegui aprovar aquilo

32
00:01:16,119 --> 00:01:17,360
mas eu consegui.

33
00:01:18,000 --> 00:01:20,079
Porque havia uma reação claro.

34
00:01:20,079 --> 00:01:22,440
A base de apoio político do governo

35
00:01:22,440 --> 00:01:24,960
não deveria ter permitido, não é?

36
00:01:24,960 --> 00:01:26,599
Mas a gente conseguiu aprovar.

37
00:01:27,920 --> 00:01:28,960
Mas não prosperou muito.

38
00:01:28,960 --> 00:01:30,159
Começamos as investigações

39
00:01:30,159 --> 00:01:33,760
não havia uma equipe técnica capaz de nos ajudar.

40
00:01:33,760 --> 00:01:38,920
Chegamos a ouvir a direção da Salgema na Câmara.

41
00:01:39,760 --> 00:01:42,000
Mas o foco, era

42
00:01:42,000 --> 00:01:44,519
 a identificação dos riscos.

43
00:01:44,519 --> 00:01:46,159
Se havia risco ou não.

44
00:01:46,159 --> 00:01:48,119
Se com a duplicação, esse risco seria

45
00:01:48,119 --> 00:01:51,119
duplicado, proporcionalmente ou não

46
00:01:51,119 --> 00:01:53,719
era esse o foco da CPI para tirar

47
00:01:53,719 --> 00:01:57,760
essa essa dúvida que a população cobrava.

48
00:01:57,760 --> 00:02:00,639
Esse clima de insegurança aqui Maceió

49
00:02:01,360 --> 00:02:03,119
ou, pelo menos, bairros mais próximos

50
00:02:03,119 --> 00:02:05,039
viveram durante esse

51
00:02:05,039 --> 00:02:07,559
esse período de implantação

52
00:02:07,559 --> 00:02:08,960
da duplicação

53
00:02:08,960 --> 00:02:10,880
da planta de diocloretano.

54
00:02:12,920 --> 00:02:15,239
Eu lembro desse processo como?

55
00:02:16,239 --> 00:02:17,800
Quando o governador Suruagy

56
00:02:17,800 --> 00:02:19,880
ele joga o plebiscito

57
00:02:19,880 --> 00:02:21,559
e o plebiscito não acontece

58
00:02:21,559 --> 00:02:22,719
qul o resultado?

59
00:02:22,719 --> 00:02:25,360
Terminam aprovando sem plebiscito, sem nada.

60
00:02:25,360 --> 00:02:28,559
Houve uma pressão muito grande do governo federal

61
00:02:28,559 --> 00:02:30,039
e terminou aprovando.

62
00:02:30,760 --> 00:02:33,239
E foi duplicado e pronto.

63
00:02:33,239 --> 00:02:35,480
E aí parou tudo.

64
00:02:36,199 --> 00:02:37,920
No teve mais

65
00:02:37,920 --> 00:02:40,519
não me lembro se residualmente

66
00:02:40,519 --> 00:02:42,880
ainda teve alguma coisa até 86.

67
00:02:43,480 --> 00:02:44,679
De 86 em diante

68
00:02:44,679 --> 00:02:47,760
não lembro de nenhum movimento

69
00:02:47,760 --> 00:02:52,039
contra a presença da salgema

70
00:02:52,039 --> 00:02:53,960
do ponto de vista ambiental.

71
00:02:53,960 --> 00:02:55,719
Não havia nenhuma ação.

72
00:02:55,719 --> 00:02:56,519
Não me lembro.

73
00:02:56,519 --> 00:02:58,840
se houve eu não me recordo.

74
00:02:59,599 --> 00:03:01,000
O que tecnicamente

75
00:03:01,000 --> 00:03:02,840
quando foi explicado na época

76
00:03:03,639 --> 00:03:05,199
e ninguém contestou

77
00:03:05,199 --> 00:03:06,119
era o seguinte:

78
00:03:06,119 --> 00:03:09,840
olha, não, vai ser muito profunda a retirada do sal

79
00:03:09,840 --> 00:03:11,039
<b>e há toda uma</b>

80
00:03:12,639 --> 00:03:14,559
uma formação geológica

81
00:03:14,559 --> 00:03:16,719
acima que protege, então...

82
00:03:16,719 --> 00:03:18,840
 e eles retirariam a salgema

83
00:03:18,840 --> 00:03:20,679
 e colocariam água

84
00:03:20,679 --> 00:03:22,199
para ir enchendo as cavernas.

85
00:03:22,199 --> 00:03:23,280
Não haveria risco nenhum.

86
00:03:24,000 --> 00:03:26,519
Então, essa questão não aparece em nenhum momento

87
00:03:26,519 --> 00:03:29,320
em nenhum protesto, em nenhum documento, nada.

88
00:03:31,880 --> 00:03:33,360
Eu me lembro que foi da turma do Eric

89
00:03:33,360 --> 00:03:34,920
do jornalismo lá da Ufal

90
00:03:34,920 --> 00:03:37,639
que eles lançaram um jornal laboratório

91
00:03:37,639 --> 00:03:39,039
muito restrito

92
00:03:39,039 --> 00:03:41,559
em que eles especulavam sobre esse risco:

93
00:03:41,559 --> 00:03:43,920
Se há uma caverna isso um dia cai!

94
00:03:43,920 --> 00:03:45,760
Mas não era um estudo técnico

95
00:03:45,760 --> 00:03:47,679
uma coisa que que conseguisse

96
00:03:48,400 --> 00:03:50,559
provocar uma discussão na sociedade

97
00:03:50,559 --> 00:03:52,039
ou dos órgãos técnicos, etc.

98
00:03:52,039 --> 00:03:52,800
Não houve...

99
00:03:52,800 --> 00:03:57,400
Há um estudo do do Ivan Fernando de Lima, dos anos 50.

100
00:03:58,320 --> 00:04:00,360
Acho que está no Geografia de Alagoas

101
00:04:00,360 --> 00:04:01,320
O Ivan Fernandes de Lima

102
00:04:01,320 --> 00:04:04,039
Eu sou novo, mas ele foi meu professor de Geografia.

103
00:04:05,960 --> 00:04:06,760
E o Ivan

104
00:04:07,480 --> 00:04:10,559
ele alertava para essa chamada falha tectónica

105
00:04:10,559 --> 00:04:11,719
que há nessa região

106
00:04:11,719 --> 00:04:14,599
nessa calha do Rio Mundaú, se não me engano

107
00:04:15,000 --> 00:04:18,000
<b>cortando Maceió.</b>

108
00:04:18,599 --> 00:04:21,039
E quando o surgiram esses problemas

109
00:04:21,039 --> 00:04:22,920
 eu me lembro que eu escrevi um texto alertando:

110
00:04:22,920 --> 00:04:24,320
olha, é bom estudar isso

111
00:04:24,320 --> 00:04:27,559
porque esses colapsos dessas cavernas

112
00:04:27,559 --> 00:04:29,519
podem ter origem

113
00:04:29,519 --> 00:04:33,159
em alguma acomodação em torno dessa falha tectônica, né?

114
00:04:33,159 --> 00:04:37,559
E daí por diante, começou a ceder, né?

115
00:04:38,639 --> 00:04:41,000
Então, de qualquer forma existiu o estudo

116
00:04:41,000 --> 00:04:44,079
e ele ou não foi levado em consideração

117
00:04:44,840 --> 00:04:47,920
ou havia outros estudos se contrapondo

118
00:04:47,920 --> 00:04:48,519
e dizendo que

119
00:04:49,440 --> 00:04:50,639
<b>não afetava, não é?</b>


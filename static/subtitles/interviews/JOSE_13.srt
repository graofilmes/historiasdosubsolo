1
00:00:00,480 --> 00:00:02,480
Então, como eu já sabia

2
00:00:02,480 --> 00:00:05,960
e tinha documentos e tinha evidências

3
00:00:05,960 --> 00:00:09,039
foi quando eu fui a primeira vez entrevistado

4
00:00:09,039 --> 00:00:13,800
<b>pelo Wendel na TV assembleia.</b>

5
00:00:14,400 --> 00:00:15,519
E foi quando eu disse, olha, gente

6
00:00:15,519 --> 00:00:18,039
aquilo ali, não foi terremoto

7
00:00:18,039 --> 00:00:19,920
no sentido clássico, não!

8
00:00:20,760 --> 00:00:23,119
O que aconteceu foi

9
00:00:23,119 --> 00:00:25,800
aí foi nos meus arquivos

10
00:00:26,960 --> 00:00:29,280
olha o que aconteceu foi o seguinte:

11
00:00:29,280 --> 00:00:32,320
peguei os pareceres dos geólogos

12
00:00:33,280 --> 00:00:37,239
As Minas não foram monitoradas esse  tempo todo

13
00:00:37,960 --> 00:00:41,360
e uma mina coalesceu com a outra

14
00:00:41,360 --> 00:00:42,239
juntou.

15
00:00:43,400 --> 00:00:46,280
<b>E o teto não aguentou</b>

16
00:00:46,880 --> 00:00:48,119
<b>e desabou.</b>

17
00:00:49,039 --> 00:00:50,440
<b>Foi isso o que aconteceu.</b>

18
00:00:51,639 --> 00:00:53,119
Para minha surpresa

19
00:00:53,119 --> 00:00:56,639
minha surpresa não, para a minha constatação

20
00:00:56,639 --> 00:00:59,239
jamais diria para minha Alegria, não é?

21
00:00:59,239 --> 00:01:04,480
Otávio estava na ocasião e viu

22
00:01:04,480 --> 00:01:08,360
quando foi apresentado o relatório oficial do DNPM.

23
00:01:09,199 --> 00:01:13,320
E o relatório oficial do DNPM confirmou

24
00:01:14,000 --> 00:01:16,480
com dados precisos

25
00:01:16,480 --> 00:01:19,039
cálculos.

26
00:01:19,039 --> 00:01:21,360
O que aconteceu foi:

27
00:01:22,000 --> 00:01:24,760
Uma mina coalesceu com a outra

28
00:01:24,760 --> 00:01:26,679
 e o teto desabou.

29
00:01:27,320 --> 00:01:29,920
<b>Essa é a origem do chamado terremoto.</b>


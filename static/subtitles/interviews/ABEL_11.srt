1
00:00:00,039 --> 00:00:01,599
<b>O senhor acredita que, de fato</b>

2
00:00:01,599 --> 00:00:04,559
<b>a extração de salgema parou de ser feita aqui?</b>

3
00:00:04,559 --> 00:00:07,360
<b>Acredito. Acredito porque</b>

4
00:00:07,559 --> 00:00:10,599
<b>eles são muito fiscalizados.</b>

5
00:00:10,599 --> 00:00:12,519
<b>Então, em 2019, eles pararam mesmo</b>

6
00:00:12,519 --> 00:00:14,320
<b>porque o negócio é muito sério, gente.</b>

7
00:00:15,360 --> 00:00:16,800
<b>Se eles continuassem</b>

8
00:00:17,440 --> 00:00:18,920
<b>eu acho que não ficar ninguém solto.</b>

9
00:00:18,920 --> 00:00:20,400
<b>Ia ser todo mundo preso.</b>

10
00:00:21,360 --> 00:00:24,639
<b>Do presidente ao engenheiro de campo</b>

11
00:00:24,639 --> 00:00:26,000
<b>todo mundo preso.</b>

12
00:00:27,039 --> 00:00:29,119
<b>É uma irresponsabilidade muito grande.</b>

13
00:00:29,119 --> 00:00:31,400
<b>Eles cometeram um crime muito grande.</b>

14
00:00:31,400 --> 00:00:32,840
<b>Não sei se vocês estão sabendo </b>

15
00:00:32,840 --> 00:00:35,239
<b>que esse problema aqui</b>

16
00:00:36,119 --> 00:00:38,519
<b>dessa mineração desastrosa</b>

17
00:00:39,280 --> 00:00:43,840
<b>é o maior crime ambiental do Brasil urbano.</b>

18
00:00:45,159 --> 00:00:48,800
<b>São quase 50mil pessoas atingidas.</b>


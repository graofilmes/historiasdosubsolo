1
00:00:00,000 --> 00:00:04,400
<b>Inicialmente, não se tinha noção</b>

2
00:00:05,679 --> 00:00:08,079
<b>do que estava acontecendo</b>

3
00:00:08,079 --> 00:00:08,880
<b>com esses lugares</b>

4
00:00:10,000 --> 00:00:11,960
<b>então foi todo mundo pego de surpreza</b>

5
00:00:11,960 --> 00:00:12,960
<b>vamos dizer assim</b>

6
00:00:13,800 --> 00:00:15,719
<b>se que isso começou a se espalhar</b>

7
00:00:15,719 --> 00:00:16,519
<b>pra outros lugares.</b>

8
00:00:16,519 --> 00:00:18,880
<b>Originalmente era o Pinheiro</b>

9
00:00:18,880 --> 00:00:20,840
<b>e todo mundo começou a se voltar</b>

10
00:00:20,840 --> 00:00:22,719
<b>todo mundo passou a se voltar, entre aspas.</b>

11
00:00:22,719 --> 00:00:25,159
<b>Começou-se a falar</b>

12
00:00:25,159 --> 00:00:26,239
<b>quando se falava</b>

13
00:00:26,239 --> 00:00:27,960
<b>se falava só do Pinheiro.</b>

14
00:00:28,679 --> 00:00:30,719
<b>Emtão começou a se produzir muito conhecimento</b>

15
00:00:30,719 --> 00:00:34,079
<b>sobre o que estava acontecendo no Pinheiro,</b>

16
00:00:34,079 --> 00:00:36,880
<b>E quando isso começou a aparecer na lagoa</b>

17
00:00:36,880 --> 00:00:38,679
<b>não se tinha conhecimento nenhum.</b>

18
00:00:38,679 --> 00:00:40,199
<b>Tanto é, que passou muito tempo</b>

19
00:00:40,199 --> 00:00:42,480
<b>pra sair de caso Pinheiro</b>

20
00:00:42,480 --> 00:00:44,360
<b>para caso Braskem.</b>

21
00:00:44,360 --> 00:00:46,840
<b>E foi preciso muita insistência</b>

22
00:00:46,840 --> 00:00:49,280
<b>pras pessoas pararem de falar caso Pinheiro</b>

23
00:00:49,280 --> 00:00:50,800
<b>e falarem caso Braskem.</b>

24
00:00:50,920 --> 00:00:54,400
<b>Então acho que primeiro isso demorou</b>

25
00:00:54,400 --> 00:00:56,760
<b>porque não havia, e até hoje não há</b>

26
00:00:56,760 --> 00:00:57,960
<b>muito conhecimento produzido</b>

27
00:00:57,960 --> 00:00:59,079
<b>em relação à região lagunar</b>

28
00:00:59,079 --> 00:01:00,920
<b>e à questão da mineração.</b>

29
00:01:01,599 --> 00:01:03,400
<b>O caso é muito mais grave</b>

30
00:01:03,400 --> 00:01:05,440
<b>então quanto mais grave, mais difícil</b>

31
00:01:05,440 --> 00:01:08,400
<b>de você ter o conhecimento</b>

32
00:01:08,400 --> 00:01:10,400
<b>minimamente consolidado</b>

33
00:01:10,400 --> 00:01:13,559
<b>que permita você estabelecer algo</b>

34
00:01:13,559 --> 00:01:18,039
<b>em relação ao que fazer.</b>

35
00:01:18,039 --> 00:01:20,480
<b>Como aconteceu depois</b>

36
00:01:20,480 --> 00:01:21,960
<b>e, ao mesmo tempo</b>

37
00:01:21,960 --> 00:01:26,000
<b>demorou pra se falar desses lugares</b>

38
00:01:26,840 --> 00:01:29,239
<b>também demorou pra haver a organização</b>

39
00:01:29,239 --> 00:01:30,400
<b>dos próprios moradores.</b>

40
00:01:31,119 --> 00:01:33,280
<b>E aí entra um fator, que é a baixa renda.</b>

41
00:01:33,679 --> 00:01:35,079
<b>A maioria dos moradores dessa região</b>

42
00:01:35,079 --> 00:01:35,920
<b>é de baixa renda.</b>

43
00:01:36,159 --> 00:01:38,079
<b>Isso tanto influi</b>

44
00:01:38,079 --> 00:01:40,280
<b>na visibilidade que eles conseguem ter</b>

45
00:01:40,280 --> 00:01:42,360
<b>com relação à mídia</b>

46
00:01:42,639 --> 00:01:45,360
<b>como influi em relação à organização.</b>

47
00:01:45,519 --> 00:01:47,519
<b>Eles têm maiores dificuldades</b>

48
00:01:47,519 --> 00:01:48,480
<b>em se organizar</b>

49
00:01:48,480 --> 00:01:51,719
<b>porque não vão ter recurso suficiente</b>

50
00:01:51,719 --> 00:01:54,400
<b>e não vão ter visibilidade suficiente</b>

51
00:01:54,400 --> 00:01:56,039
<b>pra isso.</b>

52
00:01:56,400 --> 00:01:58,840
<b>E quem póderia chegar junto</b>

53
00:01:58,840 --> 00:02:00,199
<b>que é o pessoal do Pinheiro</b>

54
00:02:00,320 --> 00:02:02,639
<b>estava muito envolvido</b>

55
00:02:02,639 --> 00:02:04,519
<b>com a sua necessidade.</b>

56
00:02:05,079 --> 00:02:07,440
<b>Com a sua necessidade</b>

57
00:02:07,440 --> 00:02:10,119
<b>de dar organização à vida</b>

58
00:02:10,119 --> 00:02:12,440
<b>de reestruturar a vida</b>

59
00:02:12,440 --> 00:02:14,840
<b>de receber indenização</b>

60
00:02:14,840 --> 00:02:16,639
<b>de ir pra sua nova casa</b>

61
00:02:16,639 --> 00:02:18,320
<b>de ir pra sua nova vida</b>

62
00:02:18,320 --> 00:02:21,000
<b>e também de deixar isso pra trás</b>

63
00:02:21,000 --> 00:02:21,920
<b>porque, imagina</b>

64
00:02:21,920 --> 00:02:24,159
<b>eu acompanhei casos de perto</b>

65
00:02:24,159 --> 00:02:27,639
<b>companhei casos de pessoas próximas a mim</b>

66
00:02:27,639 --> 00:02:29,079
<b>que desde quando souberam</b>

67
00:02:29,079 --> 00:02:31,559
<b>quando ainda estavam ao redor do problema</b>

68
00:02:31,559 --> 00:02:33,199
<b>quando o problema tinha chegado na vida delas</b>

69
00:02:33,199 --> 00:02:36,119
<b>até chegar o problema na vida delas</b>

70
00:02:36,119 --> 00:02:38,000
<b>até a coisa alcaçá-las</b>

71
00:02:38,000 --> 00:02:39,760
<b>até o problema alcaçá-las</b>

72
00:02:39,760 --> 00:02:43,679
<b>até omento que elas tram e sair</b>

73
00:02:43,679 --> 00:02:45,000
<b>e começar uma vida nova.</b>

74
00:02:45,840 --> 00:02:48,360
<b>Eu acompanhei o que foi o sofrimento das pessoas</b>

75
00:02:48,360 --> 00:02:49,920
<b>passando por isso.</b>

76
00:02:50,159 --> 00:02:52,519
<b>Chega um momento que o que você quer</b>

77
00:02:52,519 --> 00:02:54,239
<b>é deixar isso pra trás</b>

78
00:02:54,559 --> 00:02:57,280
<b>é ir viver a sua vida</b>

79
00:02:57,280 --> 00:02:59,719
<b>deixar isso de mão</b>

80
00:03:00,159 --> 00:03:02,400
<b>porque vcoê se cança.</b>

81
00:03:02,400 --> 00:03:05,400
<b>É algo muito desgastante</b>

82
00:03:05,400 --> 00:03:07,199
<b>inclusive emocionalmente</b>

83
00:03:07,480 --> 00:03:08,679
<b>e financeiramente</b>

84
00:03:08,679 --> 00:03:10,480
<b>você fica numa situação muito frágil.</b>

85
00:03:12,159 --> 00:03:13,679
<b>Por isso que as indenizações</b>

86
00:03:13,679 --> 00:03:15,840
<b>elas acabam</b>

87
00:03:15,840 --> 00:03:17,960
<b>em mais de 90% dos casos</b>

88
00:03:17,960 --> 00:03:21,920
<b>acabam sendo "bem recebidas", vamos dizer assim</b>

89
00:03:21,920 --> 00:03:22,840
<b>sendo aceitas.</b>

90
00:03:23,280 --> 00:03:26,360
<b>Porque você quer continuar com a sua vida</b>

91
00:03:26,360 --> 00:03:28,840
<b>você quer deixar a dor pra traz.</b>

92
00:03:29,599 --> 00:03:32,239
<b>Então quando o pessoal do Mutange</b>

93
00:03:32,239 --> 00:03:34,239
<b>do Bebedouro e do Bom-Parto</b>

94
00:03:34,239 --> 00:03:36,000
<b>que é o ultimo bairro lagunar</b>

95
00:03:36,000 --> 00:03:37,880
<b>que recebe essa notícia</b>

96
00:03:38,119 --> 00:03:39,480
<b>começam a passar por isso</b>

97
00:03:39,480 --> 00:03:42,599
<b>o pessoal do Pinheiro, que poderia ajudar</b>

98
00:03:42,599 --> 00:03:45,960
<b>em boa parte está querendo deixar isso pra traz.</b>

99
00:03:46,239 --> 00:03:47,679
<b>Pra todos os casos</b>

100
00:03:47,679 --> 00:03:49,440
<b>inclusive os do Pinheiro</b>

101
00:03:49,440 --> 00:03:51,360
<b>acho que houve também</b>

102
00:03:51,360 --> 00:03:52,280
<b>e isso é geral</b>

103
00:03:52,280 --> 00:03:55,559
<b>não vou coloccar só para os bairros lagunares</b>

104
00:03:56,000 --> 00:03:59,880
<b>acho que houve muito de insensibilidade</b>

105
00:04:00,320 --> 00:04:01,440
<b>em lidar com essa questão.</b>

106
00:04:01,440 --> 00:04:02,360
<b>Isso foi geral.</b>

107
00:04:02,679 --> 00:04:05,280
<b>Isso foi geral, até hoje é.</b>

108
00:04:05,280 --> 00:04:06,880
<b>Quando você vê, hoje</b>

109
00:04:06,880 --> 00:04:09,400
<b>ou há um tempo atrás</b>

110
00:04:09,400 --> 00:04:12,280
<b>notícias saindo na imprensa</b>

111
00:04:12,280 --> 00:04:15,280
<b>de que a prefeitura estava fazendo</b>

112
00:04:15,280 --> 00:04:16,239
<b>junto à Braskem</b>

113
00:04:16,239 --> 00:04:21,320
<b>propostas para o que seria feito daquela região</b>

114
00:04:21,320 --> 00:04:23,199
<b>propostas urbanísticas.</b>

115
00:04:23,199 --> 00:04:25,400
<b>E quando você vê sair</b>

116
00:04:25,400 --> 00:04:27,280
<b>e isso saiu na imprensa</b>

117
00:04:27,280 --> 00:04:30,039
<b>e nunca foi falado com os moradores</b>

118
00:04:30,039 --> 00:04:31,039
<b>antes de ir à imprensa</b>

119
00:04:31,039 --> 00:04:33,039
<b>os moradores souberam pela imprensa!</b>

120
00:04:33,039 --> 00:04:34,280
<b>Moradores, inclusive</b>

121
00:04:34,280 --> 00:04:37,280
<b>que estava sabendo há pouco tempo</b>

122
00:04:37,280 --> 00:04:40,079
<b>que a sua área de moradia</b>

123
00:04:40,079 --> 00:04:42,719
<b>estava incluída, pelo novo mapa</b>

124
00:04:42,719 --> 00:04:43,960
<b>naquele perímetro.</b>

125
00:04:43,960 --> 00:04:46,480
<b>Eu ouví isso de pessoas: e a minha casa?</b>

126
00:04:46,800 --> 00:04:48,079
<b>A minha casa está aí?</b>

127
00:04:48,480 --> 00:04:49,800
<b>Está nessa área?</b>

128
00:04:50,679 --> 00:04:52,519
<b>E você fica vendo</b>

129
00:04:52,519 --> 00:04:54,760
<b>como saiu agora recentemente</b>

130
00:04:55,519 --> 00:04:58,519
<b>professores de universidade</b>

131
00:04:58,880 --> 00:05:01,880
<b>a própria FUNDEP da UFAL</b>

132
00:05:01,880 --> 00:05:04,960
<b>falando de propor</b>

133
00:05:04,960 --> 00:05:06,800
<b>que essa área seja transformada</b>

134
00:05:06,800 --> 00:05:09,800
<b> numa zona de revitalização.</b>

135
00:05:09,920 --> 00:05:11,039
<b>você diz: espera aí</b>

136
00:05:11,039 --> 00:05:13,400
<b>isso foi dialogado com quem?</b>

137
00:05:15,320 --> 00:05:18,719
<b>E aí você escuta gente dizendo</b>

138
00:05:18,719 --> 00:05:21,679
<b>que ja há evidências</b>

139
00:05:21,679 --> 00:05:25,960
<b>de que há uma estabilização em curso na área.</b>

140
00:05:25,960 --> 00:05:26,920
<b>Espera aí!</b>

141
00:05:26,920 --> 00:05:28,360
<b>Que evidências são essas?</b>

142
00:05:28,360 --> 00:05:29,840
<b>Onde isso saiu?</b>

143
00:05:29,840 --> 00:05:31,400
<b>A que informação você teve acesso?</b>

144
00:05:31,400 --> 00:05:33,280
<b>Porque até onde se sabe</b>

145
00:05:33,280 --> 00:05:37,039
<b>até 2023 não há como se falar</b>

146
00:05:37,039 --> 00:05:38,599
<b>em estabilização dessa área.</b>

147
00:05:39,400 --> 00:05:44,559
<b>Em 2023 vai se ter uma noção mais clara</b>

148
00:05:45,159 --> 00:05:46,840
<b>realmente</b>

149
00:05:46,840 --> 00:05:49,840
<b>de qual é o tamanho do problema.</b>

150
00:05:49,840 --> 00:05:52,159
<b>Mas a gente está falando de daqui há dois anos</b>

151
00:05:52,159 --> 00:05:54,199
<b>começar a ter uma noção mais clara</b>

152
00:05:54,199 --> 00:05:55,840
<b>do tamanho do problema.</b>

153
00:05:56,880 --> 00:05:59,199
<b>Por que? Porque espera-se</b>

154
00:05:59,199 --> 00:06:02,400
<b>que em 2023, possivelmente</b>

155
00:06:02,400 --> 00:06:04,480
<b>o Pinheiro! O Pinheiro</b>

156
00:06:05,119 --> 00:06:09,199
<b>terá tido o afundamento estabilizado.</b>

157
00:06:09,559 --> 00:06:13,400
<b>E aí o que vai restar é a região lagunar</b>

158
00:06:13,400 --> 00:06:17,960
<b>com uma possível...</b>

159
00:06:19,960 --> 00:06:23,760
<b>Vamos dizer assim, com um possível quadro</b>

160
00:06:23,760 --> 00:06:24,880
<b>mais claro.</b>

161
00:06:24,880 --> 00:06:29,039
<b>Porque você vai ter uma questão já encaminhada</b>

162
00:06:29,039 --> 00:06:31,800
<b>e um conhecimento mais estabelecido</b>

163
00:06:31,800 --> 00:06:34,039
<b>alguns poços já preenchidos</b>

164
00:06:34,039 --> 00:06:36,360
<b>pra analizar o impacto disso</b>

165
00:06:36,360 --> 00:06:38,639
<b>no afindamento do restante, etc.</b>

166
00:06:38,639 --> 00:06:40,119
<b>Mas isso é um "se".</b>

167
00:06:40,559 --> 00:06:41,679
<b>Isso é um "se".</b>

168
00:06:41,679 --> 00:06:44,320
<b>Então talvez. Talvez em 2023</b>

169
00:06:44,320 --> 00:06:46,199
<b>a gente tenha um quadro mais claro.</b>

170
00:06:46,880 --> 00:06:49,239
<b>Mas as próprias pessoas que falam isso</b>

171
00:06:49,239 --> 00:06:50,719
<b>são claras em dizer que</b>

172
00:06:50,719 --> 00:06:52,480
<b>não há como precisar</b>

173
00:06:52,480 --> 00:06:55,760
<b>quando e se haverá dolinas</b>

174
00:06:55,760 --> 00:06:56,840
<b>na região lagunar.</b>

175
00:06:56,840 --> 00:06:59,159
<b>Se haverá um fenômeno de sucção,</b>

176
00:06:59,159 --> 00:07:01,559
<b>Então você pode, nesse meio tempo</b>

177
00:07:01,559 --> 00:07:04,760
<b>ter um afundamento mais drástico</b>

178
00:07:04,760 --> 00:07:05,840
<b>na lagoa.</b>

179
00:07:06,280 --> 00:07:08,000
<b>Não dá pra ter certeza.</b>

180
00:07:08,000 --> 00:07:09,960
<b>Então como é que você está falando</b>

181
00:07:09,960 --> 00:07:12,960
<b>e fazendo um projeto</b>

182
00:07:12,960 --> 00:07:16,480
<b>pra ter uma zona de revitalização</b>

183
00:07:16,480 --> 00:07:17,360
<b>ou não sei o quê</b>

184
00:07:17,360 --> 00:07:21,320
<b>se você nem sabe ainda da situação da área?</b>

185
00:07:22,840 --> 00:07:24,440
<b> E como é que você está fazendo isso?</b>

186
00:07:24,440 --> 00:07:25,719
<b>Toda vez que se faz isso</b>

187
00:07:25,719 --> 00:07:29,239
<b>se faz isso sem nenhum tipo de esclarecimento</b>

188
00:07:29,239 --> 00:07:31,280
<b>pra quem primeiro importa</b>

189
00:07:31,280 --> 00:07:32,960
<b>que são os moradores dessa área.</b>


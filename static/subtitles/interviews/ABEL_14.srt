1
00:00:01,320 --> 00:00:04,719
<b>Olha, essa é uma incógnita</b>

2
00:00:04,719 --> 00:00:06,599
<b>muito difícil de dizer.</b>

3
00:00:06,840 --> 00:00:08,960
<b>Na minha opinião, pelo menos</b>

4
00:00:09,920 --> 00:00:11,400
<b> hoje eu digo o seguinte:</b>

5
00:00:11,400 --> 00:00:16,760
<b>lá no Pinheiro, já se poderia fazer uma mata ali.</b>

6
00:00:18,079 --> 00:00:20,599
<b>Um parque, para as pessoas andar ali por dentro</b>

7
00:00:20,599 --> 00:00:22,800
<b>ou uma floresta mesmo, fechada.</b>

8
00:00:23,440 --> 00:00:25,840
<b>Seria ótimo para a cidade!</b>

9
00:00:26,400 --> 00:00:29,119
<b>Isso a Braskem doando à prefeitura</b>

10
00:00:29,119 --> 00:00:31,159
<b>a custo zero, não é?</b>

11
00:00:32,639 --> 00:00:36,199
<b>Para compensar toda a trajédia que ela provocou</b>

12
00:00:36,199 --> 00:00:38,400
<b>nas 50.000 pessoas.</b>

13
00:00:38,639 --> 00:00:39,719
<b>Pelo menos isso...</b>

14
00:00:40,000 --> 00:00:41,920
<b>Toda aquela área que é dela agora</b>

15
00:00:42,480 --> 00:00:45,840
<b>ela transformava a princípio, num primeiro momento</b>

16
00:00:45,840 --> 00:00:47,679
<b>pelo menos pelos próximos dez anos</b>

17
00:00:47,679 --> 00:00:49,119
<b>numa floresta.</b>

18
00:00:50,519 --> 00:00:52,280
<b>Um grande pulmão pra Maceió, não é?</b>

19
00:00:52,280 --> 00:00:54,880
<b>Juntando com os biomas que ja temos alí.</b>

20
00:00:55,760 --> 00:01:01,480
<b>E isso por uma doação obrigatória à prefeitura!</b>

21
00:01:04,039 --> 00:01:05,079
<b>Uma floresta.</b>

22
00:01:06,840 --> 00:01:08,679
<b>Isso é o que eu acho que deve ser feito.</b>

23
00:01:08,679 --> 00:01:09,800
<b>Pronto.</b>

24
00:01:09,800 --> 00:01:14,239
<b>Depois a prefeitura se for possível, deixa aí essa floresta</b>

25
00:01:14,239 --> 00:01:17,239
<b>ou depois de dez anos faz um parque</b>

26
00:01:17,239 --> 00:01:19,239
<b>abrindo uns caminhos ali dentro</b>

27
00:01:19,239 --> 00:01:22,800
<b>como tem no Trianon, lá em plena Paulista, não é?</b>

28
00:01:22,800 --> 00:01:25,000
<b>O Trianon que tem na Paulista, não é verdade?</b>

29
00:01:25,000 --> 00:01:29,320
<b>Ou mais ainda o Ibirapuera, que eu adoro!</b>

30
00:01:29,519 --> 00:01:31,079
<b>Adoro o Ibirapuera.</b>

31
00:01:31,079 --> 00:01:33,320
<b>Aqui devia ter um parque assim, não é?</b>

32
00:01:34,119 --> 00:01:36,679
<b>Professor, isso é o que o senhor gostaria</b>

33
00:01:36,679 --> 00:01:39,039
<b>mas o que é que o senhor acha que de fato vai acontecer?</b>

34
00:01:40,559 --> 00:01:43,840
<b>Eu não sei. Sinceramente!</b>

35
00:01:43,840 --> 00:01:45,519
<b>Eu não sei porque...</b>

36
00:01:45,519 --> 00:01:47,599
<b>Uma coisa que eu sei é que não vai afundar!</b>

37
00:01:47,760 --> 00:01:49,599
<b>O Pinheiro não vai afundar não!</b>

38
00:01:49,599 --> 00:01:51,639
<b>Nesse mapa atual</b>

39
00:01:51,639 --> 00:01:55,519
<b>você pega as minas mais avançadas</b>

40
00:01:55,519 --> 00:01:58,519
<b>e traça um raio de 950m</b>

41
00:01:59,480 --> 00:02:03,840
<b>tanto do lado do Bom-Parto como do lado do Bebedouro.</b>

42
00:02:03,840 --> 00:02:05,840
<b>Pega a mina mais avançada e traça um raio.</b>

43
00:02:05,840 --> 00:02:08,840
<b>Isso baseado não só nas minhas observações de campo</b>

44
00:02:08,840 --> 00:02:11,840
<b>mas também dos estudos de vários pesquisadores</b>

45
00:02:11,840 --> 00:02:13,639
<b>nacionais e internacionais.</b>

46
00:02:13,840 --> 00:02:17,519
<b> Essa área aí é a área afetada.</b>

47
00:02:18,079 --> 00:02:22,719
<b>Agora, o que vai ser feito dessa área aí, só Deus sabe.</b>

48
00:02:22,880 --> 00:02:28,280
<b>Pelo menos entre a rua Belo Horizonte e a Fernandes Lima</b>

49
00:02:28,960 --> 00:02:31,960
<b>eu não vejo razão, pelo menos</b>

50
00:02:32,119 --> 00:02:35,320
<b>para as pessoas saírem desesperadas dalí.</b>

51
00:02:35,920 --> 00:02:38,519
<b>porque quanto mais distante estão da mina</b>

52
00:02:38,519 --> 00:02:43,199
<b>menores são as fissuras e rachaduras.</b>

53
00:02:43,199 --> 00:02:47,840
<b>Uma fissura que chega lá é coisa bem pequena.</b>

54
00:02:47,840 --> 00:02:51,639
<b>Quanto mais vai se afastando, mais o efeito...</b>

55
00:02:51,639 --> 00:02:54,840
<b>Mas a rocha não vai ficando mole?</b>

56
00:02:54,840 --> 00:02:57,280
<b>Sim, mas ela amolece na direção da mina.</b>

57
00:02:58,000 --> 00:03:02,039
<b>Ela não amolece na direção da Fernandes Lima, entendeu?</b>


1
00:00:00,119 --> 00:00:03,159
<b>Quando eu não aguentei mais?</b>

2
00:00:05,360 --> 00:00:09,440
Depois que um secretário de planejamento

3
00:00:10,199 --> 00:00:13,440
não foi o professor José de Melo Gomes

4
00:00:14,119 --> 00:00:16,719
ele jamais se passaria para isso

5
00:00:16,719 --> 00:00:19,119
depois que um secretário de planejamento

6
00:00:19,119 --> 00:00:20,559
me mostrou um revólver.

7
00:00:23,000 --> 00:00:24,559
<b>Me chamou no gabinete</b>

8
00:00:25,400 --> 00:00:26,920
<b>para conversar.</b>

9
00:00:28,760 --> 00:00:33,559
Conversamos como amigos, nem tocamos no assunto.

10
00:00:33,559 --> 00:00:37,199
 Ele, como quem quer e não quer

11
00:00:37,199 --> 00:00:41,320
Ele abriu a pasta como quem estava procurando papéis

12
00:00:41,320 --> 00:00:45,119
e quando abriu a pasta, aí eu vi que tinha um revólver.

13
00:00:46,400 --> 00:00:47,960
<b>Então, tinha chegado a hora.</b>

14
00:00:48,840 --> 00:00:52,519
Mas antes disso, antes de chegar nesse nesse ponto

15
00:00:53,239 --> 00:00:55,199
como é que foram os embates

16
00:00:55,199 --> 00:00:58,639
com relação ao licenciamento da salgema?

17
00:00:58,679 --> 00:00:59,880
<b>Sim, sim.</b>

18
00:00:59,880 --> 00:01:02,920
<b>Houve essa negação?</b>

19
00:01:03,159 --> 00:01:05,400
Quando nós começamos a dar pareceres

20
00:01:05,400 --> 00:01:10,559
e a nos negarmos a dar pareceres que sustentassem

21
00:01:11,360 --> 00:01:14,800
qualquer autorização para a implantação da Salgema.

22
00:01:15,519 --> 00:01:19,599
Ainda estava na fase de implantação, sem autorização.

23
00:01:22,360 --> 00:01:25,239
Eles conseguiram implantar, implantar mesmo.

24
00:01:26,400 --> 00:01:28,199
E a gente viu que, olha

25
00:01:28,199 --> 00:01:30,000
a correlação de forças realmente

26
00:01:30,000 --> 00:01:32,159
é tremendamente diferente.


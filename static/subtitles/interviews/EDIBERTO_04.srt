1
00:00:01,119 --> 00:00:03,159
Primeiro a imprensa, no período

2
00:00:03,159 --> 00:00:04,880
não tinha essa Liberdade toda, né?

3
00:00:04,880 --> 00:00:07,800
Mas os empresários da comunicação

4
00:00:07,800 --> 00:00:10,159
é Claro que estavam envolvidos com esse projeto

5
00:00:10,159 --> 00:00:12,519
porque era um projeto do governo federal.

6
00:00:12,519 --> 00:00:13,719
Pra você ter uma ideia

7
00:00:14,679 --> 00:00:17,079
da influência disso e dos
desdobramentos

8
00:00:17,760 --> 00:00:20,880
era uma empresa privada a Salgema.

9
00:00:20,880 --> 00:00:22,960
Ela Foi lentamente a se associando

10
00:00:22,960 --> 00:00:24,559
 a outras empresas privadas.

11
00:00:25,920 --> 00:00:26,880
Até que chegou um ponto

12
00:00:26,880 --> 00:00:29,119
em que o governo federal resolveu estatiza la.

13
00:00:29,760 --> 00:00:31,960
Estatizou com a injeção de capital.

14
00:00:31,960 --> 00:00:33,159
Foi comprando as ações

15
00:00:33,159 --> 00:00:34,360
foi aumentando o capital.

16
00:00:34,880 --> 00:00:36,320
<b>E os outros não podiam acompanhar</b>

17
00:00:36,320 --> 00:00:38,039
<b>e ela Foi ficando com a empresa</b>

18
00:00:38,039 --> 00:00:40,800
<b>e ela terminou estatizada</b>

19
00:00:41,440 --> 00:00:43,239
<b>e em seguida foi....</b>

20
00:00:43,920 --> 00:00:46,719
Não me lembro qual foi.

21
00:00:46,719 --> 00:00:48,119
O BNDES e mais outro órgão.

22
00:00:48,599 --> 00:00:51,880
Eles se fundiram e rumou a chamada Norquisa.

23
00:00:52,559 --> 00:00:53,880
Quem foi ser o primeiro presidente da Norquisa

24
00:00:53,880 --> 00:00:56,000
Logo após deixar a presidência da República?

25
00:00:56,000 --> 00:00:57,440
Ernesto Geisel.

26
00:00:58,239 --> 00:01:00,079
Pra vocês entenderem

27
00:01:00,079 --> 00:01:02,159
a profundidade desse jogo, não é?

28
00:01:03,199 --> 00:01:04,920
Então, além do atrativo

29
00:01:04,920 --> 00:01:07,320
para Alagoas, de que haveria desenvolvimento

30
00:01:07,320 --> 00:01:08,840
industrialização

31
00:01:08,840 --> 00:01:10,800
compra de álcool das usinas

32
00:01:10,800 --> 00:01:12,079
das destilarias...

33
00:01:13,119 --> 00:01:15,800
Havia todo um atrativo econômico que convencia.

34
00:01:16,480 --> 00:01:17,400
Acho que sinceramente

35
00:01:17,400 --> 00:01:19,199
os empresários, os governantes

36
00:01:19,199 --> 00:01:20,440
alguns deles, pelo menos...

37
00:01:22,199 --> 00:01:25,840
E havia a ordem unida para aceitar e acabou.

38
00:01:25,840 --> 00:01:27,920
Então eu Imagino que a imprensa da época

39
00:01:28,719 --> 00:01:30,199
não teria como reagir a isso

40
00:01:30,199 --> 00:01:31,519
de jeito nenhum.

41
00:01:31,519 --> 00:01:33,480
Ela Foi chamada para dentro do bolo

42
00:01:33,480 --> 00:01:34,800
como sempre foi, né?

43
00:01:35,239 --> 00:01:36,559
Com raras exceções

44
00:01:36,559 --> 00:01:38,239
da imprensa alternativa

45
00:01:38,239 --> 00:01:40,000
daquela que denunciava na época

46
00:01:40,000 --> 00:01:42,800
existiam jornais, o luta popular e outros

47
00:01:42,800 --> 00:01:44,639
de circulação muito restrita.

48
00:01:45,480 --> 00:01:48,360
Mas, na época, não tinha WhatsApp,

49
00:01:48,360 --> 00:01:51,039
não é o Facebook. Redes sociais para denunciar.

50
00:01:51,679 --> 00:01:54,280
Então é Claro que as empresas

51
00:01:54,280 --> 00:01:56,199
a organização Arnon de Melo

52
00:01:56,199 --> 00:01:58,320
o jornal de Alagoas, na época

53
00:01:58,320 --> 00:01:59,400
e eu não sei se o diário

54
00:01:59,400 --> 00:02:01,159
ainda funcionava...

55
00:02:02,480 --> 00:02:05,000
Elas não foram polo exatamente de denúncias

56
00:02:05,000 --> 00:02:08,360
contra o risco disso.

57
00:02:08,360 --> 00:02:10,079
Ao contrário, né?

58
00:02:10,079 --> 00:02:11,719
Na maioria das matérias, elas

59
00:02:13,280 --> 00:02:16,519
jogam para cima a possibilidade de desenvolvimento, etc.

60
00:02:16,519 --> 00:02:18,239
E como é sabido, por exemplo,

61
00:02:18,239 --> 00:02:20,440
 que a questão de emprego não seria uma delas, né?

62
00:02:21,719 --> 00:02:23,840
Os técnicos vieram quase todos de fora.

63
00:02:24,599 --> 00:02:27,559
E há um certo impacto

64
00:02:27,559 --> 00:02:30,119
 nesse período do início dos anos 70

65
00:02:30,119 --> 00:02:31,519
a meados dos anos 70.

66
00:02:32,559 --> 00:02:35,480
É que Alagoas é uma coisa que merece
ser estudada...

67
00:02:36,239 --> 00:02:38,360
E você está fazendo o material com pesquisa

68
00:02:38,360 --> 00:02:40,119
não é isso? Esse Documentário com pesquisa.

69
00:02:41,039 --> 00:02:42,559
É que você imagina em Alagoas

70
00:02:42,559 --> 00:02:44,679
até o final do dos anos 60.

71
00:02:45,480 --> 00:02:48,119
Os grandes negócios eram de famílias daqui.

72
00:02:48,880 --> 00:02:51,119
Então as pessoas não

73
00:02:51,119 --> 00:02:53,480
tinham relações com o poder muito próximas.

74
00:02:54,159 --> 00:02:55,440
A partir desse período

75
00:02:55,440 --> 00:02:57,559
alguns fenômenos acontecem em Alagoas

76
00:02:58,639 --> 00:03:00,960
Há uma onda de cheia

77
00:03:00,960 --> 00:03:04,039
uma chegada  massa de técnicos ligados

78
00:03:04,039 --> 00:03:05,639
<b>à Salgema</b>

79
00:03:05,639 --> 00:03:07,079
que vêm da Bahia

80
00:03:07,079 --> 00:03:08,719
vêm de São Paulo, vêm do Rio

81
00:03:08,719 --> 00:03:09,840
vêm de tudo quanto é lugar.

82
00:03:09,840 --> 00:03:11,840
Essas pessoas vão morar em Maceió

83
00:03:11,840 --> 00:03:15,400
e elas não têm vínculo político nem social com

84
00:03:16,079 --> 00:03:18,320
com as tradicionais famílias do nosso poder.

85
00:03:19,320 --> 00:03:21,599
Nesse mesmo período, chegam as destilarias

86
00:03:21,599 --> 00:03:23,360
e as  grandes empresas

87
00:03:23,360 --> 00:03:25,199
que trabalham com mecânica pesada

88
00:03:25,199 --> 00:03:27,119
 também com técnicos de fora.

89
00:03:28,199 --> 00:03:32,199
E a UFAL cria novos cursos também nesse período

90
00:03:32,199 --> 00:03:33,760
em meados dos anos 70.

91
00:03:33,760 --> 00:03:37,320
E uma leva de professores chegam do Ceará

92
00:03:37,320 --> 00:03:40,079
chegam da Bahia, de Pernambuco

93
00:03:40,079 --> 00:03:42,119
e do rio.

94
00:03:42,119 --> 00:03:45,920
Então muitos vêm para cá para criar esses cursos

95
00:03:45,920 --> 00:03:49,559
de arquitetura, agronomia e vai por aí afora.

96
00:03:50,320 --> 00:03:51,559
Essa massa

97
00:03:51,559 --> 00:03:53,320
de pessoas de classe média

98
00:03:53,320 --> 00:03:54,719
com formação política

99
00:03:54,719 --> 00:03:56,840
com certo nível técnico

100
00:03:57,639 --> 00:04:00,360
ela gera em Maceió uma pressão grande

101
00:04:00,360 --> 00:04:00,960
<b>pra que</b>

102
00:04:01,840 --> 00:04:03,960
<b>a cidade, comece a</b>

103
00:04:06,880 --> 00:04:10,039
respeitar a cidadania, vamos assim dizer.

104
00:04:10,039 --> 00:04:13,960
É exatamente desse núcleo que você vai encontrar

105
00:04:14,639 --> 00:04:15,920
o uso da pesquisa

106
00:04:15,920 --> 00:04:19,840
e se destacaram, atuando na questão do meio ambiente.

107
00:04:20,960 --> 00:04:22,760
Essa presença de pessoas em Maceió

108
00:04:22,760 --> 00:04:24,559
que não tinha um compromisso

109
00:04:24,559 --> 00:04:25,639
ela não era assim:

110
00:04:25,639 --> 00:04:27,239
Eu não vou falar nada porque

111
00:04:27,239 --> 00:04:29,320
 a minha filha está empregada no governo!

112
00:04:29,320 --> 00:04:31,320
Ou porque meu pai  tem uma empresa

113
00:04:31,320 --> 00:04:33,760
que fornece à prefeitura de Maceió.

114
00:04:33,760 --> 00:04:36,239
Essas pessoas, elas chegam livres disso.

115
00:04:36,239 --> 00:04:39,880
Então elas vão para uma opinião

116
00:04:41,280 --> 00:04:43,639
mais livre

117
00:04:43,840 --> 00:04:45,800
mais desempedida de qualquer

118
00:04:45,800 --> 00:04:47,039
vínculo político.

119
00:04:47,039 --> 00:04:49,800
E com isso surge uma massa crítica

120
00:04:49,800 --> 00:04:51,039
respeitável em Maceió.

121
00:04:51,679 --> 00:04:52,320
Não só isso.

122
00:04:52,320 --> 00:04:55,039
Essas pessoas cobram da prefeitura o calçamento da rua

123
00:04:55,039 --> 00:04:56,599
e denunciam se não calçar

124
00:04:56,599 --> 00:04:57,920
porque não têm medo

125
00:04:57,920 --> 00:05:00,599
de o cara ficar com cara feia lá, o prefeito.

126
00:05:00,599 --> 00:05:02,440
A sociedade de defeda dos direitos humanos

127
00:05:02,440 --> 00:05:03,880
quando se formou em 78

128
00:05:03,880 --> 00:05:05,800
na reunião inicial

129
00:05:05,800 --> 00:05:09,039
acho que 80% dos presentes eram professores

130
00:05:09,039 --> 00:05:11,599
ou funcionários de fora de Alagoas.

131
00:05:12,599 --> 00:05:15,039
Então essa é uma questão importante para ser identificada

132
00:05:15,039 --> 00:05:17,519
do ponto de vista sociológico

133
00:05:17,519 --> 00:05:21,199
dessa formação dessa massa crítica

134
00:05:21,199 --> 00:05:23,880
de maceioenses, principalmente, eu diria

135
00:05:23,880 --> 00:05:24,960
depois de alagoanos, né?

136
00:05:25,719 --> 00:05:27,639
Na nesse período.

137
00:05:27,639 --> 00:05:29,519
Então não dá para você cobrar

138
00:05:29,519 --> 00:05:31,360
que, por exemplo, o jornalista

139
00:05:31,360 --> 00:05:33,559
tivesse essa liberdade de crítica

140
00:05:33,559 --> 00:05:35,800
porque mesmo que tivessem, e eles tinham

141
00:05:35,800 --> 00:05:37,519
eles eram impedidos.

142
00:05:37,519 --> 00:05:39,480
A imprensa é que determinava

143
00:05:39,480 --> 00:05:41,840
que não era para mexer nisso

144
00:05:41,840 --> 00:05:44,039
e havia além disso a censura, não é?

145
00:05:44,039 --> 00:05:46,639
Nesse período eu trabalhei, nos anos 70

146
00:05:46,639 --> 00:05:49,079
no jornal O Movimento de São Paulo.

147
00:05:49,079 --> 00:05:51,039
E quantas vezes a gente teve que sair às carreiras

148
00:05:51,039 --> 00:05:52,559
 para tentar salvar o jornal

149
00:05:52,559 --> 00:05:54,159
da apreensão da polícia federal

150
00:05:54,159 --> 00:05:55,159
prendendo o jornal, né?

151
00:05:55,960 --> 00:05:58,159
<b>Então eu via toda uma</b>

152
00:05:59,199 --> 00:06:02,039
uma fiscalização sobre a opinião

153
00:06:02,039 --> 00:06:04,960
sobre a notícia sobre a informação.

154
00:06:04,960 --> 00:06:06,480
Não era facil, né?

155
00:06:06,480 --> 00:06:09,360
Não era fácil, mas mesmo assim a gente conseguia

156
00:06:09,360 --> 00:06:11,880
com panfletos e com jornais alternativos

157
00:06:11,880 --> 00:06:13,719
tentar furar esse bloqueio.

158
00:06:13,719 --> 00:06:16,320
Mas realmente a comunicação da época

159
00:06:16,320 --> 00:06:18,920
e a televisão, jornais

160
00:06:18,920 --> 00:06:21,159
a grande imprensa, né? A gente chamava...

161
00:06:22,239 --> 00:06:25,960
Ela não teve uma postura crítica com relação a isso.


1
00:00:00,920 --> 00:00:03,320
Olhe, durante a militância

2
00:00:03,320 --> 00:00:08,159
não tanto quanto durante a vigência

3
00:00:08,159 --> 00:00:10,119
do meu mandato.

4
00:00:12,400 --> 00:00:14,920
Mas sempre havia aquela coisa no ar

5
00:00:14,920 --> 00:00:16,519
durante a militância

6
00:00:16,519 --> 00:00:18,719
eu passei a receber telefonemas.

7
00:00:19,519 --> 00:00:21,280
<b>de ameaça de morte!</b>

8
00:00:22,719 --> 00:00:25,599
Telefonemas que simplesmente eram uma pergunta

9
00:00:25,599 --> 00:00:27,679
você quer ser um Tiradentes, é?

10
00:00:28,599 --> 00:00:29,960
<b>Só não tentaram</b>

11
00:00:30,639 --> 00:00:32,480
porque era impossível

12
00:00:32,480 --> 00:00:34,760
me desmoralizar por corrupção.

13
00:00:36,119 --> 00:00:37,599
Em relação a isso

14
00:00:37,599 --> 00:00:41,800
até hoje é unanimidade.

15
00:00:43,960 --> 00:00:46,159
Eu jamais

16
00:00:46,159 --> 00:00:50,559
recebi um tostão de suborno

17
00:00:50,559 --> 00:00:52,559
ou de qualquer outra coisa

18
00:00:52,559 --> 00:00:54,800
Enquanto exerci o cargo.

19
00:00:58,079 --> 00:00:59,920
<b>Propostas, recebi.</b>

20
00:01:00,920 --> 00:01:01,880
<b>Não aceitei.</b>

21
00:01:03,000 --> 00:01:05,119
Logo, eu deduzo

22
00:01:05,119 --> 00:01:08,280
que propostas existem, existiram

23
00:01:08,280 --> 00:01:11,440
e que pessoas aceitam, aceitaram.


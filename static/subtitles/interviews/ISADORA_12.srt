1
00:00:00,199 --> 00:00:01,480
<b>Eu diria primeiro</b>

2
00:00:01,480 --> 00:00:04,679
<b>que, infelizmente, isso não é uma coisa em incomum.</b>

3
00:00:06,079 --> 00:00:08,159
<b>Isso não é uma coisa incomum.</b>

4
00:00:08,159 --> 00:00:13,159
<b>Isso é algo que  já foi visto em outras tragédias.</b>

5
00:00:13,159 --> 00:00:15,239
<b>Eu não sei se você lembra daquele filme</b>

6
00:00:15,239 --> 00:00:16,320
<b>Erin Brockovich</b>

7
00:00:17,320 --> 00:00:18,960
<b>quando acontece a tragédia</b>

8
00:00:18,960 --> 00:00:20,880
<b>a primeira coisa que a empresa faz</b>

9
00:00:20,880 --> 00:00:21,880
<b>não sei se você lembra</b>

10
00:00:21,880 --> 00:00:23,840
<b>é que ela oferece comprar a casa.</b>

11
00:00:26,280 --> 00:00:27,559
<b>Então não é uma coisa em incomum.</b>

12
00:00:27,559 --> 00:00:29,559
<b>A primeira coisa que ela faz é</b>

13
00:00:29,559 --> 00:00:31,400
<b>ela oferece comprar a casa</b>

14
00:00:31,400 --> 00:00:33,559
<b>por um valor acima de mercado</b>

15
00:00:34,119 --> 00:00:36,840
<b>e isso seria um espécie de indenização.</b>

16
00:00:38,280 --> 00:00:39,519
<b>Então, isso não é incomum.</b>

17
00:00:40,559 --> 00:00:43,119
<b>Então eu tenho algumas suspeitas...</b>

18
00:00:44,000 --> 00:00:45,599
<b>Primeiro que eu acho que</b>

19
00:00:46,679 --> 00:00:48,320
<b>o Ministério Público Federal</b>

20
00:00:48,320 --> 00:00:49,239
<b>tem uma comissão</b>

21
00:00:49,239 --> 00:00:51,079
<b>que desde o início está tratando disso.</b>

22
00:00:51,079 --> 00:00:52,760
<b>tem uma comissão com que desde o início</b>
<b>tratando disso, né? Tem acordo? Ela</b>
<b>começou de pessoas sérias, assim, uma</b>
<b>força-tarefa de pessoas sérias torne</b>
<b>eras pari tal. São pessoas sérias,</b>
<b>conhecidas já de muito tempo assim, e</b>
<b>eu acho que Eu Acredito é que o</b>
<b>ministro federal deve estar numa</b>
<b>situação bem delicada.</b>

23
00:00:52,760 --> 00:00:54,000
<b>É uma comissão de pessoas sérias</b>

24
00:00:54,000 --> 00:00:55,719
<b>essa é uma força-tarefa de pessoas sérias.</b>

25
00:00:55,719 --> 00:00:57,760
<b>Stranieri, Kaspary e tal...</b>

26
00:00:57,760 --> 00:00:58,599
<b>São pessoas sérias</b>

27
00:00:58,599 --> 00:01:01,039
<b>conhecidas já de muito tempo.</b>

28
00:01:01,039 --> 00:01:03,280
<b>E o que eu acredito</b>

29
00:01:03,280 --> 00:01:05,559
<b>é que o Ministério Público Federal</b>

30
00:01:05,559 --> 00:01:08,559
<b>deve estar numa situação bem delicada.</b>

31
00:01:09,159 --> 00:01:11,119
<b>Porque essa tragédia</b>

32
00:01:11,119 --> 00:01:14,920
<b>ela já tem uma configuração</b>

33
00:01:14,920 --> 00:01:16,039
<b>financeiramente falando</b>

34
00:01:16,039 --> 00:01:18,400
<b>muito significativa</b>

35
00:01:18,400 --> 00:01:20,840
<b>e por enquanto ela não dá mostras</b>

36
00:01:20,840 --> 00:01:24,599
<b>de que ela va diminuir em termos de impacto</b>

37
00:01:24,599 --> 00:01:26,199
<b>ela só aumenta.</b>

38
00:01:26,199 --> 00:01:28,960
<b>Então eu lembro que eu li uma época dessa</b>

39
00:01:28,960 --> 00:01:34,400
<b>que os compromissos financeiros</b>

40
00:01:34,400 --> 00:01:35,960
<b>que a Braskem estava assumindo</b>

41
00:01:36,599 --> 00:01:37,840
<b>com a tragédia</b>

42
00:01:37,840 --> 00:01:39,320
<b>eu li acho que ano passado</b>

43
00:01:39,320 --> 00:01:41,480
<b>já estavam chegando</b>

44
00:01:41,480 --> 00:01:45,159
<b>a 50% do que eram os ativos da empresa.</b>

45
00:01:46,320 --> 00:01:48,000
<b>Eu lembro disso em algum lugar.</b>

46
00:01:50,960 --> 00:01:52,519
<b>É um equilibrio delicado, não é?</b>

47
00:01:52,519 --> 00:01:54,840
<b>Entre você garantir que a empresa</b>

48
00:01:54,840 --> 00:01:57,400
<b>pague tudo o que ela tem que pagar</b>

49
00:01:57,400 --> 00:02:00,400
<b>em termos de indenização justa</b>

50
00:02:00,400 --> 00:02:03,559
<b>e ela não abrir falência</b>

51
00:02:03,559 --> 00:02:05,000
<b>porque se ela abre falência</b>

52
00:02:05,000 --> 00:02:05,920
<b>ela não paga mais nada.</b>

53
00:02:08,440 --> 00:02:10,800
<b>E eu acho que deve haver um receio</b>

54
00:02:10,800 --> 00:02:13,239
<b>grande do Ministério Público Federal</b>

55
00:02:13,239 --> 00:02:15,239
<b>de que isso aconteça.</b>

56
00:02:15,239 --> 00:02:17,159
<b>Eles devem estar tentando encontrar</b>

57
00:02:17,159 --> 00:02:18,840
<b>o tempo todo um equilíbrio aí</b>

58
00:02:18,840 --> 00:02:22,480
<b>de como garantir essa compensação justa</b>

59
00:02:22,480 --> 00:02:23,559
<b>e, ao mesmo tempo</b>

60
00:02:23,559 --> 00:02:25,280
<b>não inviabilizar a empresa</b>

61
00:02:25,280 --> 00:02:26,800
<b>para que ela não abra falência</b>

62
00:02:26,800 --> 00:02:28,239
<b>porque aí quando abre falência</b>

63
00:02:30,880 --> 00:02:31,360
<b>Já foi...</b>

64
00:02:32,480 --> 00:02:33,280
<b>Abriu falência....</b>

65
00:02:33,280 --> 00:02:35,199
<b>Você vê aí a dívida da previdência</b>

66
00:02:35,199 --> 00:02:38,079
<b>que a vale tem a não sei quantas décadas</b>

67
00:02:38,079 --> 00:02:40,280
<b>e que não paga, porque abriu falência.</b>

68
00:02:40,719 --> 00:02:42,159
<b>E em relação à Braskem</b>

69
00:02:42,159 --> 00:02:46,840
<b>se tornar dona desse território</b>

70
00:02:46,840 --> 00:02:48,960
<b>eu vejo isso com muita preocupação.</b>

71
00:02:49,440 --> 00:02:50,840
<b>Na minha opinião</b>

72
00:02:50,840 --> 00:02:53,719
<b>se se trabalhar essa área...</b>

73
00:02:53,719 --> 00:02:55,159
<b>Primeiro, que assim tem que ver...</b>

74
00:02:55,159 --> 00:02:57,360
<b>Eu não tenho muita certeza juridicamente</b>

75
00:02:57,360 --> 00:02:59,920
<b>do que pode ou não pode ser revertido.</b>

76
00:03:00,639 --> 00:03:02,519
<b>Se esses acordos, foram intermediados</b>

77
00:03:02,519 --> 00:03:03,920
<b>pelo Ministério Público Federal</b>

78
00:03:03,920 --> 00:03:05,280
<b>eles podem ser revertidos</b>

79
00:03:05,280 --> 00:03:07,440
<b>em relação ao que já foi definido?</b>

80
00:03:07,440 --> 00:03:08,639
<b>Eu não tenho certeza em elação a isso.</b>

81
00:03:08,639 --> 00:03:12,880
<b>Eu acho, eu acredito que não possa</b>

82
00:03:12,880 --> 00:03:14,840
<b>pela questão de segurança jurídica.</b>

83
00:03:15,199 --> 00:03:16,599
<b>Eu acredito que um acordo firmado</b>

84
00:03:16,599 --> 00:03:18,000
<b>ele está afirmado.</b>

85
00:03:18,000 --> 00:03:21,639
<b>Mas o que eu gostaria que acontecesse?</b>

86
00:03:21,639 --> 00:03:23,039
<b>Primeiro, eu gostaria que isso</b>

87
00:03:23,039 --> 00:03:24,960
<b>que esse território não ficasse para a Braskem.</b>

88
00:03:24,960 --> 00:03:25,880
<b>A primeira coisa</b>

89
00:03:25,880 --> 00:03:26,840
<b>acho que a empresa não deveria</b>

90
00:03:26,840 --> 00:03:28,760
<b>ficar como o proprietário desse território</b>

91
00:03:29,960 --> 00:03:32,480
<b>porque ela destruiu esse território.</b>

92
00:03:32,480 --> 00:03:35,400
<b>Ela não deveria ter direito a um lugar</b>

93
00:03:35,400 --> 00:03:36,239
<b>que ela destruiu.</b>

94
00:03:36,239 --> 00:03:37,519
<b>É a primeira coisa.</b>

95
00:03:37,519 --> 00:03:38,960
<b>A segunda coisa é:</b>

96
00:03:38,960 --> 00:03:40,639
<b>se esse acordo não puder ser revertido</b>

97
00:03:40,639 --> 00:03:41,920
<b>o que eu gostaria</b>

98
00:03:41,920 --> 00:03:44,079
<b>é que ela fosse obrigada.</b>

99
00:03:44,079 --> 00:03:46,960
<b>Sabe quando você é proprietário de um território</b>

100
00:03:46,960 --> 00:03:49,119
<b>e transforma esse território em APP?</b>

101
00:03:50,760 --> 00:03:53,599
<b>Transformar um território app é algo para a vida.</b>

102
00:03:54,880 --> 00:03:57,719
<b>Depois, mesmo que você queira mudar, você não pode.</b>

103
00:03:57,719 --> 00:03:59,760
<b>Mesmo que os seus descendentes queiram mudar</b>

104
00:03:59,760 --> 00:04:00,599
<b>eles não podem.</b>

105
00:04:01,920 --> 00:04:04,159
<b>Existem mecanismos jurídicos</b>

106
00:04:04,159 --> 00:04:05,440
<b>para que a Braskem</b>

107
00:04:05,440 --> 00:04:08,320
<b>mesmo que ela fique como proprietárias dessas áreas</b>

108
00:04:08,320 --> 00:04:11,559
<b>ela seja obrigada a transformar essas áreas</b>

109
00:04:11,559 --> 00:04:13,920
<b>em áreas de usufruto coletivo.</b>


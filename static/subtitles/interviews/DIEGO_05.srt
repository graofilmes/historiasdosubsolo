1
00:00:00,291 --> 00:00:01,584
É capital?

2
00:00:01,584 --> 00:00:03,962
Sem dúvida, mas é uma região periférica.

3
00:00:05,171 --> 00:00:06,047
Quando a gente...

4
00:00:06,047 --> 00:00:09,634
e aí infelizmente é padrão

5
00:00:10,301 --> 00:00:13,555
todas as áreas que são impactadas
pela mineração

6
00:00:15,181 --> 00:00:20,061
você tem um clamor inicial muito forte

7
00:00:20,770 --> 00:00:23,356
em especial
quando envolve impactos bioóticos.

8
00:00:23,898 --> 00:00:26,943
Olha que coisa triste dizer
mas a verdade é essa.

9
00:00:28,611 --> 00:00:30,739
Quando envolve impactos bióticos

10
00:00:30,989 --> 00:00:33,366
ou seja: na fauna, na flora

11
00:00:34,075 --> 00:00:37,704
você tem uma comoção que é muito acentuada.

12
00:00:38,246 --> 00:00:39,581
Eu vi isso muito no Peru.

13
00:00:39,581 --> 00:00:41,374
Eu vi isso muito na Colômbia.

14
00:00:41,374 --> 00:00:43,209
no México.

15
00:00:43,209 --> 00:00:45,170
Aqui no Brasil também.

16
00:00:45,295 --> 00:00:48,214
A gente está num bairro, num estado periférico

17
00:00:50,717 --> 00:00:52,761
Bebedouro não é a Ponta Verde.

18
00:00:54,179 --> 00:00:55,513
Não é a Ponta Verde.

19
00:00:56,556 --> 00:00:59,726
O nível de comoção ia ser
completamente outro.

20
00:01:00,477 --> 00:01:02,645
Bom Parto não é Jatiúca.

21
00:01:03,354 --> 00:01:05,732
O nível de comoção é outro.

22
00:01:05,732 --> 00:01:08,860
Essas áreas, é quase
como se tivesse a desigualdade

23
00:01:10,195 --> 00:01:13,198
importasse também na hora de você valorar

24
00:01:14,240 --> 00:01:16,034
emocionalmente o impacto

25
00:01:17,160 --> 00:01:19,704
e aí o que nós temos que...

26
00:01:19,871 --> 00:01:22,582
mas aí pode ser usado a favor

27
00:01:22,916 --> 00:01:25,335
dos impactados e das impactadas

28
00:01:26,127 --> 00:01:30,757
é que nós temos numa área
que a densidade populacional é grande

29
00:01:31,800 --> 00:01:34,052
a pressão política pode ser contínua.

30
00:01:35,762 --> 00:01:37,347
Ela pode ser contínua

31
00:01:37,347 --> 00:01:38,306
Ela pode ser contínua
mas o problema é que com

32
00:01:38,348 --> 00:01:40,725
a visibilidade nacional e internacional

33
00:01:40,892 --> 00:01:42,602
num momento, por exemplo:
de pandemia

34
00:01:42,602 --> 00:01:45,480
que foi pior cenário possível para ocorrer.

35
00:01:46,856 --> 00:01:51,069
Nesse caso, por que notícia ela divide.

36
00:01:51,319 --> 00:01:53,154
Ela não soma

37
00:01:53,446 --> 00:01:56,074
então quando temos um caso como esse

38
00:01:56,074 --> 00:01:58,118
as pessoas ouviram falar, e tal

39
00:01:58,118 --> 00:01:59,953
os amigos lá de cuiabá

40
00:01:59,953 --> 00:02:02,080
Diego, o que está acontecendo aí em Maceió? Etc.

41
00:02:02,580 --> 00:02:04,999
Isso foi no passado, nunca mais perguntaram

42
00:02:07,127 --> 00:02:08,503
por que passa

43
00:02:08,503 --> 00:02:11,798
só não passa para quem é impactado.

44
00:02:11,798 --> 00:02:16,553
Então, mas: a Diego
então entrega desiste tudo? Ao contrário.

45
00:02:17,929 --> 00:02:18,805
O que é diferente

46
00:02:18,805 --> 00:02:22,642
por exemplo de outros municípios
que foram impactados

47
00:02:22,642 --> 00:02:26,354
mas menores.
É que não têm a força demográfica.

48
00:02:27,272 --> 00:02:29,482
Isso é muito importante para você
pressionar.

49
00:02:30,650 --> 00:02:33,486
Para não sair do noticiário.

50
00:02:33,486 --> 00:02:36,030
Agora, do ponto de vista nacional
 e internacional

51
00:02:40,076 --> 00:02:41,161
não é prioritário

52
00:02:41,369 --> 00:02:44,247
para a agenda. A agenda nacional.

53
00:02:45,331 --> 00:02:46,374
Seria como?

54
00:02:46,374 --> 00:02:49,335
Seria se por exemplo:
o afundamento tivesse acontecido

55
00:02:49,669 --> 00:02:52,172
vários prédios afundando, gente gritando

56
00:02:52,881 --> 00:02:55,216
Isso filmado. Nossa, ia estar até hoje.

57
00:02:55,758 --> 00:02:59,220
Mas esse é o aspecto mais perverso
da situação

58
00:02:59,596 --> 00:03:02,599
porque o impacto ele é diluído.

59
00:03:03,474 --> 00:03:07,812
E é isso o que faz por exemplo
a gente observar a pessoa que desenvolveu

60
00:03:07,812 --> 00:03:12,233
por exemplo: transtorno de ansiedade,
depressão

61
00:03:12,233 --> 00:03:15,028
provavelmente
ela vai começar a desenvolver

62
00:03:15,028 --> 00:03:16,237
diabetes.

63
00:03:16,237 --> 00:03:20,700
Ela vai ter por exemplo: hipertensão,
nunca teve hipertensão, nunca teve nada disso

64
00:03:21,409 --> 00:03:25,538
começa a desenvolver
e aí o que para os impactados e impactadas

65
00:03:25,997 --> 00:03:27,248
o que é mais perverso?

66
00:03:27,248 --> 00:03:30,919
É que isso foi somado
justamente no período da pandemia.

67
00:03:31,586 --> 00:03:33,963
Ou seja: o que é ruim tende a piorar.

68
00:03:35,006 --> 00:03:37,425
E aí eu tenho a impressão que

69
00:03:37,425 --> 00:03:40,762
o melhor é uma mobilização local

70
00:03:41,554 --> 00:03:46,392
aqui no município no estado e com sorte
isso vai bater em nível nacional

71
00:03:46,392 --> 00:03:49,729
por exemplo: com um documentário
que dê visibilidade maior.


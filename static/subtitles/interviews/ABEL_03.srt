1
00:00:00,000 --> 00:00:01,400
<b>03 de maio de 2018</b>

2
00:00:01,400 --> 00:00:03,840
<b> é aquilo que eu chamo de "acorda Maceió"!</b>

3
00:00:04,360 --> 00:00:06,119
<b> Foi quando começou a estourar tudo.</b>

4
00:00:06,119 --> 00:00:09,000
<b>Foi quando veio a...</b>

5
00:00:09,000 --> 00:00:11,360
<b>Foi quando se envolveu todo mundo</b>

6
00:00:11,360 --> 00:00:14,679
<b>e veio especialmente a CPRM.</b>

7
00:00:15,280 --> 00:00:18,719
<b>A grade CPRM que fez um trabalho maravilhoso</b>

8
00:00:19,159 --> 00:00:22,400
<b>e eu tive a honra de participar desse trabalho.</b>

9
00:00:22,800 --> 00:00:26,159
<b>Eu tive muitas reuniões com eles aqui no meu escritório</b>

10
00:00:26,599 --> 00:00:28,760
<b>e aí</b>

11
00:00:29,320 --> 00:00:30,599
<b>em 2019</b>

12
00:00:30,599 --> 00:00:34,360
<b>eu acho que foi dia 9 de maio de 2019</b>

13
00:00:34,360 --> 00:00:39,239
<b>a CPRM brilhantemente apresentou um relatório</b>

14
00:00:39,239 --> 00:00:41,159
<b>contundente</b>

15
00:00:41,599 --> 00:00:45,840
<b>dizendo exatamente quem era o culpado por tudo isso:</b>

16
00:00:45,840 --> 00:00:47,519
<b>Braskem.</b>

17
00:00:47,960 --> 00:00:49,599
<b>E quando eu acusei a Braskem</b>

18
00:00:49,599 --> 00:00:51,440
<b>em março de 2018</b>

19
00:00:51,440 --> 00:00:53,519
<b>muitos colegas me chamaram de louco!</b>

20
00:00:54,440 --> 00:00:59,000
<b>Mais ou menos entre 10 e 15 de março</b>

21
00:00:59,000 --> 00:01:02,360
<b>teve uma grande reunião no CREA</b>

22
00:01:03,360 --> 00:01:06,360
<b>e eram cinco expositores lá no CREA.</b>

23
00:01:06,360 --> 00:01:10,880
<b>O auditório do CREA estava super, super, superlotado!</b>

24
00:01:11,519 --> 00:01:13,360
<b>E eram cinco expositores:</b>

25
00:01:13,920 --> 00:01:16,920
<b>O professor Abel Galindo e mais quatro.</b>

26
00:01:16,920 --> 00:01:20,039
<b>Os outros quatro, defensores da Braskem</b>

27
00:01:20,039 --> 00:01:21,280
<b>dizendo que a Braskem</b>

28
00:01:21,280 --> 00:01:23,320
<b>não tinha nada a ver com o que estava acontecendo.</b>

29
00:01:23,440 --> 00:01:25,119
<b>Absolutamente nada.</b>

30
00:01:25,320 --> 00:01:27,920
<b>E foi justamente essa negação</b>

31
00:01:27,920 --> 00:01:30,440
<b>que mecheu comigo.</b>

32
00:01:31,320 --> 00:01:34,320
<b>Essa negação eu não aceitei de jeito nenhum.</b>

33
00:01:34,320 --> 00:01:37,119
<b>Então lá na hora eu expuz</b>

34
00:01:37,119 --> 00:01:39,960
<b>mostrei as bases</b>

35
00:01:39,960 --> 00:01:42,400
<b>a razão de porque eu achava que era a Braskem,</b>

36
00:01:42,840 --> 00:01:44,119
<b>que o problema estava lá embaixo</b>

37
00:01:44,119 --> 00:01:45,840
<b>abriu um metro de profundidade.</b>

38
00:01:45,840 --> 00:01:47,360
<b>E não era encima.</b>

39
00:01:47,360 --> 00:01:50,360
<b>Aí começaram aquela baboseira toda</b>

40
00:01:50,360 --> 00:01:52,639
<b>dizendo que era problema de</b>

41
00:01:52,639 --> 00:01:54,480
<b>drenagem na superfície</b>

42
00:01:54,480 --> 00:01:58,119
<b>e que teriam bacias endorreicas ..</b>

43
00:01:58,119 --> 00:01:59,519
<b>Bacia endorreica é</b>

44
00:01:59,519 --> 00:02:03,760
<b>uma parte baixa daquela área</b>

45
00:02:03,760 --> 00:02:05,320
<b>onde se junta água.</b>

46
00:02:05,320 --> 00:02:07,159
<b>E isso não tinha nada a ver.</b>

47
00:02:07,159 --> 00:02:08,719
<b>O problema eu ja vinha</b>

48
00:02:08,719 --> 00:02:12,079
<b>eu ja vinha vivendo esse problema desde 2010!</b>

49
00:02:12,760 --> 00:02:14,480
<b>Apesar que em 2010...</b>

50
00:02:14,480 --> 00:02:17,840
<b>Apesar que em 2010 eu via aquela rachadura no chão</b>

51
00:02:18,159 --> 00:02:19,440
<b>foi a primeira vez que eu vi</b>

52
00:02:19,440 --> 00:02:20,360
<b>no chão</b>

53
00:02:20,599 --> 00:02:22,280
<b>e os prédios rachados também</b>

54
00:02:22,280 --> 00:02:24,960
<b>mas sinceramente naquele ano</b>

55
00:02:24,960 --> 00:02:27,320
<b>eu não sabia exatamente aquela rachadura</b>

56
00:02:27,320 --> 00:02:28,320
<b>o que era.</b>

57
00:02:28,320 --> 00:02:29,079
<b>Eu não sabia.</b>

58
00:02:29,320 --> 00:02:31,840
<b>Eu achava que era um problema de retração do solo</b>

59
00:02:31,840 --> 00:02:33,679
<b>devido a água ali.</b>

60
00:02:33,880 --> 00:02:35,559
<b>Mas depois começaram a aparecer</b>

61
00:02:35,559 --> 00:02:37,760
<b>rachaduras em partes mais altas!</b>

62
00:02:37,920 --> 00:02:39,960
<b>Onde não tinha acúmulo nenhum de água.</b>

63
00:02:41,360 --> 00:02:42,480
<b>Não tinha acúmulo nenhum de água!</b>

64
00:02:42,480 --> 00:02:43,639
<b>E aí, como é que fica?</b>

65
00:02:43,920 --> 00:02:48,239
<b>E os defensores de que era uma área</b>

66
00:02:48,239 --> 00:02:50,159
<b>mais baixa</b>

67
00:02:50,159 --> 00:02:52,679
<b>uma área que acumulava água no inverno</b>

68
00:02:52,679 --> 00:02:53,960
<b>que não tinha uma boa drenagem?</b>

69
00:02:54,079 --> 00:02:57,679
<b>Era tudo uma conversa sem fundamentação.</b>

70
00:02:58,719 --> 00:03:00,519
<b>Eles certamente não gostam que eu diga</b>

71
00:03:00,519 --> 00:03:01,480
<b>mas eu tenho de dizer:</b>

72
00:03:01,480 --> 00:03:02,639
<b>sem fundamentação!</b>

73
00:03:02,920 --> 00:03:06,000
<b>Foram cinco formas de avaliar o problema</b>

74
00:03:06,000 --> 00:03:07,239
<b>de estudar o problema</b>

75
00:03:07,239 --> 00:03:10,760
<b>foram cinco metodos diferentes que a CPRM apresentou</b>

76
00:03:10,920 --> 00:03:12,280
<b>aí pronto!</b>

77
00:03:12,280 --> 00:03:13,719
<b>O pessoal começou a dizer:</b>

78
00:03:13,719 --> 00:03:15,480
<b>Está vendo o que o professor Abel já dizia?</b>

79
00:03:15,519 --> 00:03:17,000
<b>Está vendo o que o professor Abel já dizia?</b>

80
00:03:17,000 --> 00:03:17,760
<b>Está vendo?</b>

81
00:03:18,159 --> 00:03:21,800
<b>E mesmo assim vieram</b>

82
00:03:21,800 --> 00:03:25,639
<b>antes disso vieram com um relatório lá</b>

83
00:03:25,639 --> 00:03:26,800
<b>de Huston.</b>

84
00:03:26,800 --> 00:03:29,119
<b>Também teve relatórios aqui do Brasil.</b>

85
00:03:29,119 --> 00:03:31,199
<b>Teve também de Oxford.</b>

86
00:03:31,199 --> 00:03:34,039
<b>Trouxeram um bocado de relatórios, todos furados!</b>

87
00:03:34,039 --> 00:03:35,880
<b>E eu descartei todos eles.</b>

88
00:03:36,360 --> 00:03:39,599
<b>No final a Braskem assumiu, não é?</b>

89
00:03:40,119 --> 00:03:41,280
<b>Assumiu!</b>

90
00:03:42,199 --> 00:03:44,199
<b>E hoje, pelo que eu sei</b>

91
00:03:44,199 --> 00:03:47,280
<b>já passam dos dez bilhões de reais</b>

92
00:03:47,280 --> 00:03:48,719
<b>que eles estão gastando</b>

93
00:03:48,719 --> 00:03:49,960
<b>com um problema que eles diziam</b>

94
00:03:49,960 --> 00:03:51,440
<b>que não tinha nada a ver com eles.</b>


1
00:00:00,280 --> 00:00:01,719
<b>O meu envolvimento </b>

2
00:00:01,719 --> 00:00:04,880
<b>com esse problema dada Braskem</b>

3
00:00:04,880 --> 00:00:08,519
<b> esse problema dessa mineração desastrosa</b>

4
00:00:09,280 --> 00:00:11,679
<b>o meu envolvimento se deu justamente porque</b>

5
00:00:14,440 --> 00:00:16,719
<b>começar a aparecer fissuras, rachaduras</b>

6
00:00:16,719 --> 00:00:19,199
<b>em casas e prédios lá no Pinheiro</b>

7
00:00:19,199 --> 00:00:20,880
<b>foi onde tudo começou</b>

8
00:00:20,880 --> 00:00:22,599
<b>e o pessoal</b>

9
00:00:22,599 --> 00:00:24,960
<b>sempre que tinha problemas de edificação</b>

10
00:00:24,960 --> 00:00:26,199
<b>eles me chamavam</b>

11
00:00:26,920 --> 00:00:29,079
<b>me convidavam para ver o problema.</b>

12
00:00:29,079 --> 00:00:30,159
<b>Então começou aí. </b>

13
00:00:30,159 --> 00:00:32,880
<b>Então eu comecei a ver esse problema em 2010.</b>

14
00:00:34,400 --> 00:00:37,519
<b>O acorda Maceió aconteceu em 2018</b>

15
00:00:37,519 --> 00:00:39,719
<b>que foi o terremoto.</b>

16
00:00:40,599 --> 00:00:43,559
<b>Em 2010, começou no Jardim Acácia.</b>

17
00:00:44,199 --> 00:00:46,320
<b>Quando eu fui chamado para ver</b>

18
00:00:46,320 --> 00:00:48,239
<b>dois prédios rachados</b>

19
00:00:48,239 --> 00:00:49,559
<b> e aquela rachadura.</b>

20
00:00:49,559 --> 00:00:52,280
<b>A rachadura no chão, ninguém nem dava importância.</b>

21
00:00:53,239 --> 00:00:54,559
<b>Eu olhei também, eu disse:</b>

22
00:00:54,559 --> 00:00:57,360
<b>deve ser da irrigação do solo.</b>

23
00:00:57,360 --> 00:01:01,960
<b>Isso faz parte do parte, mas depois, logo em 2000...</b>

24
00:01:01,960 --> 00:01:03,480
<b>Um pouquinho depois...</b>

25
00:01:03,480 --> 00:01:06,360
<b>2013, 2015, por exemplo... 2014 2015...</b>

26
00:01:06,360 --> 00:01:08,360
<b>Aí outras casas</b>

27
00:01:09,320 --> 00:01:11,320
<b>casas de partes altas</b>

28
00:01:11,320 --> 00:01:14,559
<b>da parte alta do Pinheiro, rachadas!</b>

29
00:01:15,440 --> 00:01:17,079
<b>Depois outras e outras</b>

30
00:01:17,079 --> 00:01:18,480
<b>iam me chamando para eu ver</b>

31
00:01:18,480 --> 00:01:20,360
<b>inclusive de reforço de fundações</b>

32
00:01:20,360 --> 00:01:22,000
<b>numa dessas casas aí</b>

33
00:01:22,000 --> 00:01:23,960
<b>e voltou rachado do mesmo jeito.</b>

34
00:01:23,960 --> 00:01:25,360
<b>Eu digo, epa!</b>

35
00:01:26,000 --> 00:01:29,519
<b>Isso foi antes de acontecer o terremoto.</b>

36
00:01:30,360 --> 00:01:31,480
<b>Tem casa que eu fiz reforço</b>

37
00:01:31,480 --> 00:01:32,800
<b>duas vezes de fundações.</b>

38
00:01:34,119 --> 00:01:38,239
<b>Casas cujo solo</b>

39
00:01:38,239 --> 00:01:40,239
<b>que eu conheço na Palma da mão</b>

40
00:01:40,920 --> 00:01:44,039
<b>naquele solo podia botar ali um prédio </b>

41
00:01:44,039 --> 00:01:45,000
<b> sem problema nenhum</b>

42
00:01:45,000 --> 00:01:46,880
<b>de 4 ou 5 pavimentos</b>

43
00:01:46,880 --> 00:01:49,000
<b>com a fundação</b>

44
00:01:49,000 --> 00:01:51,360
<b>na mesma profundidade que tinha a casa!</b>

45
00:01:52,079 --> 00:01:55,000
<b>Teve uma casa térrea rachando!</b>


1
00:00:00,360 --> 00:00:02,079
<b>Por ali passa</b>

2
00:00:02,079 --> 00:00:04,639
<b>naquela área das minas</b>

3
00:00:05,239 --> 00:00:07,239
<b>passam falhas geológicas.</b>

4
00:00:07,960 --> 00:00:08,880
<b>O primeiro ponto é esse.</b>

5
00:00:08,880 --> 00:00:11,039
<b>A falha geológica é  uma fissura</b>

6
00:00:11,039 --> 00:00:14,519
<b>uma rachadura que</b>

7
00:00:15,400 --> 00:00:18,159
<b>que  existe ali a milhões e milhões de anos.</b>

8
00:00:18,159 --> 00:00:20,519
<b>Quando eu disse que tinha falha geológica</b>

9
00:00:20,519 --> 00:00:22,039
<b>me disseram que era o maluco.</b>

10
00:00:22,039 --> 00:00:24,440
<b>Aí veio a CPI e mostrou que não tinha uma não</b>

11
00:00:24,440 --> 00:00:25,320
<b>tinham duas!</b>

12
00:00:26,559 --> 00:00:29,480
<b>Ali naquela parte do Mutange</b>

13
00:00:29,480 --> 00:00:31,960
<b>tem uma que passa, segundo o CPM</b>

14
00:00:31,960 --> 00:00:34,239
<b>tem uma que passa pelo pé da encosta</b>

15
00:00:34,920 --> 00:00:36,119
<b>e tem outra que passa</b>

16
00:00:36,119 --> 00:00:38,480
<b>mais próximo da lagoa</b>

17
00:00:38,480 --> 00:00:39,599
<b>mais próximo.</b>

18
00:00:39,599 --> 00:00:42,800
<b>Alí a uns cem, cento e poucos metros</b>

19
00:00:42,800 --> 00:00:43,840
<b>de uma da outra.</b>

20
00:00:43,840 --> 00:00:45,800
<b>E essa que passa mais próximo da Lagoa</b>

21
00:00:45,800 --> 00:00:48,039
<b>passa por várias Minas.</b>

22
00:00:48,039 --> 00:00:50,360
<b>Primeiro ponto é, está certo?</b>

23
00:00:50,360 --> 00:00:51,280
<b>Primeiro ponto é esse.</b>

24
00:00:51,280 --> 00:00:51,960
<b>Segundo ponto:</b>

25
00:00:51,960 --> 00:00:52,480
<b>Olha, gente</b>

26
00:00:53,119 --> 00:00:54,840
<b>a rocha salina</b>

27
00:00:55,519 --> 00:00:58,119
<b>eu digo que é um verdadeiro bicho animal.</b>

28
00:00:58,119 --> 00:01:00,400
<b>Bicho vivo, a rocha salina, quando mexe com ela.</b>

29
00:01:01,079 --> 00:01:03,960
<b>Se você mudar as condições</b>

30
00:01:03,960 --> 00:01:06,400
<b>de temperatura e pressão dela</b>

31
00:01:06,400 --> 00:01:08,920
<b>especialmente a pressão dela</b>

32
00:01:08,920 --> 00:01:11,760
<b>ela começa a se movimentar.</b>

33
00:01:11,760 --> 00:01:15,360
<b>Ela tem uma propriedade de fluência.</b>

34
00:01:16,280 --> 00:01:18,559
<b>Por exemplo, se você faz um buraco nela aqui</b>

35
00:01:18,559 --> 00:01:20,159
<b>ela tenta fechar.</b>

36
00:01:20,159 --> 00:01:22,079
<b>Ela começa a ficar mole</b>

37
00:01:22,079 --> 00:01:26,199
<b>porque você criou um vazio ali e aliviou a pressão.</b>

38
00:01:26,199 --> 00:01:28,239
<b>A pressão que sustentava ela aqui.</b>

39
00:01:29,280 --> 00:01:30,519
<b>Então você tem que ter uma pressão.</b>

40
00:01:30,519 --> 00:01:32,880
<b>Por isso que a técnica</b>

41
00:01:32,880 --> 00:01:39,639
<b>da boa exploração de sal-gema.</b>

42
00:01:39,639 --> 00:01:42,440
<b>Você tem que manter pressão</b>

43
00:01:42,440 --> 00:01:43,719
<b>ali dentro daquele buraco</b>

44
00:01:43,719 --> 00:01:45,320
<b>daquela caverna, sabe?</b>

45
00:01:45,320 --> 00:01:48,760
<b>Porque é para ela não andar, para ela não fechar</b>

46
00:01:49,320 --> 00:01:52,320
<b>e não começar a perder as suas propriedades.</b>

47
00:01:52,320 --> 00:01:54,880
<b>Mas como tem as falhas geológicas</b>

48
00:01:54,880 --> 00:01:57,199
<b>e por conta dessas falhas</b>

49
00:01:57,199 --> 00:02:02,119
<b>então essa condição de pressão lá embaixo</b>

50
00:02:02,719 --> 00:02:04,079
<b>super necessária</b>

51
00:02:04,079 --> 00:02:05,159
<b>falhou!</b>

52
00:02:05,159 --> 00:02:07,920
<b>E quando falhou, aí a rocha começou a ficar mole.</b>

53
00:02:08,639 --> 00:02:10,920
<b>Isso ao longo das dezenas de anos</b>

54
00:02:10,920 --> 00:02:13,000
<b>isso não é de imediato, não.</b>

55
00:02:13,000 --> 00:02:16,840
<b>Começou lá na extração</b>

56
00:02:16,840 --> 00:02:19,840
<b>dessa rocha salina.</b>

57
00:02:20,400 --> 00:02:22,960
<b>A exploração do salgema começou em 75</b>

58
00:02:22,960 --> 00:02:24,599
<b>ou 76, por aí assim...</b>

59
00:02:24,599 --> 00:02:25,719
<b>Foi quando começou, né?</b>

60
00:02:25,719 --> 00:02:28,199
<b>Mas ela começou a mostrar as unhas</b>

61
00:02:28,199 --> 00:02:30,840
<b>já há 20 anos atrás.</b>

62
00:02:31,880 --> 00:02:34,000
<b>Mais ou menos em 2001ou 2002</b>

63
00:02:34,000 --> 00:02:39,639
<b>Começou então, devido à falha de pressurização...</b>

64
00:02:39,639 --> 00:02:41,320
<b>Pressurização</b>

65
00:02:41,320 --> 00:02:43,719
<b>que é a pressão lá dentro da caverna...</b>

66
00:02:43,719 --> 00:02:45,519
<b>Começou a falhar e sair</b>

67
00:02:45,519 --> 00:02:48,679
<b>e ela começou a se ficar molezinha.</b>

68
00:02:48,679 --> 00:02:50,800
<b>Isso de forma bem lenta.</b>

69
00:02:50,800 --> 00:02:52,960
<b>e ela foi ficando mole e foi andando</b>

70
00:02:52,960 --> 00:02:54,440
<b>Então ela vai ficando mole, mole, mole</b>

71
00:02:54,440 --> 00:02:56,360
<b>em direção a fechar</b>

72
00:02:56,360 --> 00:02:58,480
<b>em direção ao miolo do buraco.</b>

73
00:02:59,320 --> 00:03:01,840
<b>Ao centro das cavernas.</b>

74
00:03:01,840 --> 00:03:04,719
<b>Ela Foi então perdendo resistência, essa camada.</b>

75
00:03:04,719 --> 00:03:06,559
<b>Foi ficando fluida.</b>

76
00:03:06,559 --> 00:03:08,239
<b>Foi então fluindo.</b>

77
00:03:08,239 --> 00:03:11,480
<b>Isso milimetricamente ao longo desses anos.</b>

78
00:03:11,480 --> 00:03:13,000
<b>Especialmente de 20 anos para cá</b>

79
00:03:13,000 --> 00:03:14,000
<b>ela foi fluindo.</b>

80
00:03:14,760 --> 00:03:16,559
<b>Foi ficando mole e fluidificando.</b>

81
00:03:16,559 --> 00:03:19,239
<b>À medida que ela foi ficando mole</b>

82
00:03:19,239 --> 00:03:21,760
<b>ela vai afundando.</b>

83
00:03:22,920 --> 00:03:24,760
<b>O afundamento é a subsidência</b>

84
00:03:24,760 --> 00:03:25,760
<b>que você perguntou.</b>

85
00:03:26,519 --> 00:03:30,840
<b>Então foi exatamente em toda aquela área de Minas</b>

86
00:03:30,840 --> 00:03:32,320
<b>que ela foi afundando.</b>

87
00:03:32,960 --> 00:03:34,239
<b>Deformando, deformando</b>

88
00:03:34,239 --> 00:03:36,840
<b>milimetricamente ao longo dos anos.</b>

89
00:03:37,599 --> 00:03:41,320
<b>Em 2019, primeiro semestre</b>

90
00:03:42,199 --> 00:03:43,320
<b>eu desci do Pinheiro</b>

91
00:03:43,320 --> 00:03:45,119
<b>e fui lá para baixo para ver a Lagoa.</b>

92
00:03:46,039 --> 00:03:48,840
<b>Lá na Lagoa eu andei de canoa</b>

93
00:03:48,840 --> 00:03:51,920
<b>peguei um barqueiro muito antigo</b>

94
00:03:51,920 --> 00:03:54,360
<b>que conhecia tudo ali</b>

95
00:03:54,360 --> 00:03:57,039
<b>E ele começou a me dizer como era aqui, como era lá</b>

96
00:03:57,039 --> 00:04:00,960
<b>e falei com a família do Zé Lopes</b>

97
00:04:01,880 --> 00:04:03,199
<b>com os filhos do Zé Lopes.</b>

98
00:04:03,920 --> 00:04:05,079
<b>Falei também com o caseiro</b>

99
00:04:05,079 --> 00:04:06,599
<b>que há 40 anos trabalha lá.</b>

100
00:04:07,280 --> 00:04:09,719
<b>Ele disse: olha, doutor, aqui era assim...</b>

101
00:04:09,719 --> 00:04:11,880
<b>aí depois de tudo isso eu disse:</b>

102
00:04:11,880 --> 00:04:15,079
<b>bom, aqui, que é onde eu chamo de "Olho do Furacão"</b>

103
00:04:15,079 --> 00:04:16,519
<b>fica alí  em torno da casa do Zé Lopes</b>

104
00:04:16,519 --> 00:04:17,480
<b>onde estão as minas</b>

105
00:04:17,480 --> 00:04:18,679
<b>a concentração de minas</b>

106
00:04:18,679 --> 00:04:19,920
<b>eu vou mostrar para vocês</b>

107
00:04:19,920 --> 00:04:21,239
<b>está ali, entendeu?</b>

108
00:04:21,239 --> 00:04:23,599
<b>Então teve uma cessão grande aqui</b>

109
00:04:23,599 --> 00:04:26,639
<b>e isso eu tenho slide, que eu apresentei lá em 2019</b>

110
00:04:27,199 --> 00:04:29,559
<b>aqui, afundou entre 1,5m e 2m</b>

111
00:04:29,559 --> 00:04:31,280
<b>nos últimos 10 anos.</b>

112
00:04:32,199 --> 00:04:35,840
<b>A Nature, aquela aquela revista famosíssima</b>

113
00:04:35,840 --> 00:04:40,239
<b>revista certifica muitíssimo respeitada</b>

114
00:04:40,239 --> 00:04:42,239
<b>talvez seja a mais respeitada</b>

115
00:04:42,840 --> 00:04:43,880
<b>inglesa.</b>

116
00:04:43,880 --> 00:04:46,320
<b>Ela veio e publicou agora</b>

117
00:04:46,320 --> 00:04:47,880
<b>tem 1mês, 2 meses no máximo!</b>

118
00:04:47,880 --> 00:04:51,280
<b>Publicou agora um estudo sobre</b>

119
00:04:51,280 --> 00:04:53,960
<b>esse problema, essa tragédia aqui em Maceió.</b>

120
00:04:54,719 --> 00:04:57,239
<b>E mostrou que de 2004</b>

121
00:04:57,840 --> 00:05:00,400
<b>até 2020, 16 anos.</b>

122
00:05:00,400 --> 00:05:03,000
<b>Ali onde estou e me referindo</b>

123
00:05:03,000 --> 00:05:04,760
<b>onde está a concentração das Minas</b>

124
00:05:04,760 --> 00:05:07,559
<b>afundou 2m.</b>

125
00:05:07,559 --> 00:05:11,480
<b>E aí vem mais ou menos na conta do professor Abel Galindo.</b>

126
00:05:11,480 --> 00:05:15,280
<b>Que foi feita in loco, encima de uma canoazinha.</b>

127
00:05:15,280 --> 00:05:19,079
<b>E esse pessoal utilizado vários satélites...</b>

128
00:05:19,840 --> 00:05:22,599
<b>Vários satélites e também in loco</b>

129
00:05:22,599 --> 00:05:26,119
<b>eles também pegaram resultados de perfurações e tal.</b>

130
00:05:26,840 --> 00:05:27,480
<b>Então é isso.</b>


1
00:00:00,159 --> 00:00:01,639
<b>Quando começam a falar</b>

2
00:00:01,639 --> 00:00:03,719
<b>do que o acordo se propõe a manter</b>

3
00:00:03,719 --> 00:00:05,840
<b>eles falam logo da ordem urbanística</b>

4
00:00:05,840 --> 00:00:09,880
<b> e assim, eu acho isso absolutamente fantasioso.</b>

5
00:00:10,840 --> 00:00:13,239
<b>A questão não é se isso vai ser mantido.</b>

6
00:00:13,239 --> 00:00:15,719
<b>A questão é que isso não tem como ser mantido. </b>

7
00:00:15,719 --> 00:00:18,000
<b>Eu falo isso, eu falei isso várias vezes</b>

8
00:00:18,000 --> 00:00:19,559
<b>Já ao longo desses desses meses.</b>

9
00:00:20,519 --> 00:00:24,440
<b>Que esses bairros, especialmente...</b>

10
00:00:25,199 --> 00:00:26,360
<b>Vamos lá, quais são os bairros</b>

11
00:00:26,360 --> 00:00:27,559
<b>mais afetados até agora?</b>

12
00:00:27,559 --> 00:00:31,239
<b>O Mutange está inteiramente dentro...</b>

13
00:00:32,239 --> 00:00:33,360
<b>Inteiramente!</b>

14
00:00:33,360 --> 00:00:35,000
<b>Está todo inserido dentro</b>

15
00:00:35,000 --> 00:00:36,079
<b>do perímetro de remoção.</b>

16
00:00:37,559 --> 00:00:38,760
<b>Então o Mutange</b>

17
00:00:38,760 --> 00:00:41,760
<b>por exemplo é um bairro que vai deixar de ser bairro</b>

18
00:00:41,760 --> 00:00:43,360
<b>isso é um fato.</b>

19
00:00:43,880 --> 00:00:45,679
<b>Então como é que você vai manter</b>

20
00:00:45,679 --> 00:00:47,079
<b>a ordem urbanística</b>

21
00:00:47,079 --> 00:00:49,039
<b>em um bairro que vai deixar de ser bairro?</b>

22
00:00:49,320 --> 00:00:50,360
<b>Não há como.</b>

23
00:00:50,719 --> 00:00:52,239
<b>Não há como é impossível!</b>

24
00:00:52,239 --> 00:00:53,840
<b>Isso é impossível.</b>

25
00:00:55,800 --> 00:00:58,360
<b>Não estamos falando da parte física</b>

26
00:00:58,360 --> 00:01:00,320
<b>porque todo mundo sabe</b>

27
00:01:00,320 --> 00:01:04,079
<b>que vão existir demolições, né? </b>

28
00:01:04,079 --> 00:01:06,360
<b>Vão ser feita demolições.</b>

29
00:01:06,360 --> 00:01:07,719
<b>Isso todo mundo sabe, né?</b>

30
00:01:07,719 --> 00:01:09,199
<b>Todo mundo sabe, por exemplo</b>

31
00:01:09,199 --> 00:01:11,800
<b>todo mundo sabe que tudo que foi evacuado </b>

32
00:01:11,800 --> 00:01:15,000
<b>das encostas não vai ser reocupado</b>

33
00:01:15,000 --> 00:01:16,880
<b>e que vai ter que haver uma fiscalização</b>

34
00:01:16,880 --> 00:01:19,199
<b>e monitoramento, para que não seja reocupado.</b>

35
00:01:19,679 --> 00:01:20,960
<b>Até aí tudo bem.</b>

36
00:01:20,960 --> 00:01:22,039
<b>Porque assim, de fato,</b>

37
00:01:22,039 --> 00:01:23,440
<b>aquelas enconstas elas eram um risco</b>

38
00:01:23,440 --> 00:01:26,119
<b>mesmo que não houvesse a questão Braskem</b>

39
00:01:26,119 --> 00:01:28,199
<b>elas eram um risco à vida das pessoas</b>

40
00:01:28,199 --> 00:01:29,079
<b>que ocupavam ali.</b>

41
00:01:29,079 --> 00:01:32,239
<b>Já tinha acontecido queda de barreiras...</b>

42
00:01:32,239 --> 00:01:33,639
<b>Era uma área precária.</b>

43
00:01:33,639 --> 00:01:38,840
<b>É elas têm que ser recompostas na su  flora</b>

44
00:01:38,840 --> 00:01:42,000
<b>elas tinham de ser  recompostas enquanto área vegetal?</b>

45
00:01:42,000 --> 00:01:43,920
<b>SIm, têm de ser. Fato, né?</b>

46
00:01:43,920 --> 00:01:45,920
<b>Mas nós sabemos, por exemplo</b>

47
00:01:45,920 --> 00:01:47,920
<b>que a Braskem não vai poder demolir</b>

48
00:01:47,920 --> 00:01:50,440
<b>o que é património edificado.</b>

49
00:01:50,559 --> 00:01:53,679
<b>A não ser que se torne realmente</b>

50
00:01:53,679 --> 00:01:57,599
<b>área de risco, pela fragilidade da estrutura.</b>

51
00:01:57,599 --> 00:01:59,239
<b>Então, se o Zé Lopes, por exemplo</b>

52
00:01:59,239 --> 00:02:02,599
<b>ele passar a ter um afundamento de solo</b>

53
00:02:02,599 --> 00:02:07,639
<b>ele passar a ter uma subida das águas</b>

54
00:02:07,639 --> 00:02:09,239
<b>ali da lagoa</b>

55
00:02:09,239 --> 00:02:11,239
<b>que coloque em risco a estrutura</b>

56
00:02:11,239 --> 00:02:13,320
<b>que a gente torce para que não aconteça</b>

57
00:02:13,320 --> 00:02:15,199
<b>pela importância patrimonial daquele prédio.</b>

58
00:02:15,199 --> 00:02:18,519
<b>Mas se chegar a ter uma fragilização</b>

59
00:02:18,519 --> 00:02:21,519
<b>a ponto da estrutura tornar-se de risco</b>

60
00:02:21,800 --> 00:02:24,960
<b>e aquilo chegar num ponto</b>

61
00:02:25,000 --> 00:02:27,199
<b>de poder desabar</b>

62
00:02:27,199 --> 00:02:28,840
<b>vaai ter se tomada uma outra medida</b>

63
00:02:28,840 --> 00:02:30,679
<b>e inclusive a gente não quer</b>

64
00:02:30,679 --> 00:02:31,800
<b>que aquilo ali seja demolido.</b>

65
00:02:31,800 --> 00:02:34,079
<b>Existe uma solução em patrimônio cultural</b>

66
00:02:34,079 --> 00:02:35,079
<b>patrimônio edificado</b>

67
00:02:35,079 --> 00:02:36,719
<b>que se chama trasladação.</b>

68
00:02:36,719 --> 00:02:39,639
<b>Trasladação, é você retirar um imóvel </b>

69
00:02:39,639 --> 00:02:42,199
<b>de um de um lugar e levar para outro lugar.</b>

70
00:02:42,199 --> 00:02:46,199
<b>Você desmonta ele como fosse um  joguinho de lego.</b>

71
00:02:46,199 --> 00:02:48,480
<b>Você recorta ele</b>

72
00:02:48,480 --> 00:02:52,760
<b>marca os os pedaços</b>

73
00:02:52,760 --> 00:02:54,360
<b>e depois remonta.</b>

74
00:02:54,360 --> 00:02:55,599
<b>Isso já foi feito no mundo</b>

75
00:02:55,599 --> 00:02:58,480
<b>com imóveis de muita importância.</b>

76
00:02:58,480 --> 00:03:00,760
<b>É uma solução possível</b>

77
00:03:00,760 --> 00:03:01,840
<b>nunca feita no Brasil</b>

78
00:03:01,840 --> 00:03:03,000
<b>seria o primeiro caso.</b>

79
00:03:03,000 --> 00:03:05,360
<b>A gente não quer que chegue nisso, né?</b>

80
00:03:05,360 --> 00:03:08,599
<b>Mas se houver risco daquela estrutura desabar</b>

81
00:03:08,599 --> 00:03:10,800
<b>é o que nós gostariamos que acontecesse</b>

82
00:03:10,800 --> 00:03:13,719
<b>e nós sabemos que o acordo firmado foi que</b>

83
00:03:13,719 --> 00:03:15,719
<b>o que for patrimônio cultural edificado</b>

84
00:03:15,719 --> 00:03:17,440
<b>se não houver risco à estrutura</b>

85
00:03:17,440 --> 00:03:19,000
<b> a empresa não pode demolir.</b>

86
00:03:19,639 --> 00:03:20,559
<b>Sabe se disso.</b>

87
00:03:20,559 --> 00:03:22,159
<b>Há acordos firmados já nesse sentido.</b>

88
00:03:22,159 --> 00:03:25,119
<b>Mas um bairro não é feito</b>

89
00:03:25,960 --> 00:03:28,039
<b>dos imóveis que ele possui.</b>

90
00:03:28,039 --> 00:03:32,679
<b>Um bairro não é feito das casas físicas.</b>

91
00:03:33,639 --> 00:03:35,280
<b>Ele é feito das pessoas</b>

92
00:03:35,280 --> 00:03:39,400
<b>ocupando os imóveis e vivendo ali.</b>

93
00:03:40,039 --> 00:03:42,000
<b>Ele é feito da circulação</b>

94
00:03:42,000 --> 00:03:43,079
<b>ele é feito disso tudo.</b>

95
00:03:43,079 --> 00:03:44,159
<b>Então se aquela área virar</b>

96
00:03:44,159 --> 00:03:47,760
<b>uma área sem pessoas morando</b>

97
00:03:47,760 --> 00:03:51,360
<b>porque não pode virar uma área de passagem</b>

98
00:03:51,360 --> 00:03:52,599
<b>onde você vai...</b>

99
00:03:52,599 --> 00:03:55,079
<b>Tipo vamos dizer que a área vire</b>

100
00:03:55,079 --> 00:03:58,480
<b>uma versão ampliada do parque municipal</b>

101
00:03:58,480 --> 00:04:00,360
<b>que contenha</b>

102
00:04:01,039 --> 00:04:02,719
<b>imóveis históricos para você visitar</b>

103
00:04:02,719 --> 00:04:04,960
<b>que contenha atrações culturais</b>

104
00:04:04,960 --> 00:04:08,559
<b>que contenha um campo de futebol.</b>

105
00:04:10,320 --> 00:04:12,519
<b>Vamos dizer que ela se torne isso.</b>

106
00:04:13,800 --> 00:04:15,880
<b>A ordem urbanística já foi rompida!</b>

107
00:04:17,039 --> 00:04:19,159
<b>Então, assim: vai manter urbanistica?</b>

108
00:04:19,159 --> 00:04:21,400
<b>Não, isso é uma ilusão.</b>

109
00:04:22,079 --> 00:04:23,960
<b>Não vai manter a ordem urbanística</b>

110
00:04:23,960 --> 00:04:24,880
<b>isso é impossível!</b>

111
00:04:24,880 --> 00:04:26,679
<b>As pessoas não moram mais lá.</b>

112
00:04:27,000 --> 00:04:28,320
<b>O CSA</b>

113
00:04:29,199 --> 00:04:30,480
<b>existe naquela área </b>

114
00:04:31,119 --> 00:04:32,519
<b>há décadas.</b>

115
00:04:34,000 --> 00:04:36,519
<b>Ele é encarado em Maceió</b>

116
00:04:36,519 --> 00:04:39,440
<b>existe uma rivalidade entre CSA e CRB</b>

117
00:04:40,119 --> 00:04:41,719
<b>e ele é encarado em Maceió</b>

118
00:04:41,719 --> 00:04:44,639
<b>falando a grosso modo</b>

119
00:04:44,639 --> 00:04:47,800
<b>a grosso modo, o CRB é encarado</b>

120
00:04:47,800 --> 00:04:49,760
<b>como o time da praia</b>

121
00:04:49,760 --> 00:04:51,760
<b>e o CSA como o time da lagoa.</b>

122
00:04:51,760 --> 00:04:53,239
<b>A grosso modo, porque nós sabemos</b>

123
00:04:53,239 --> 00:04:54,840
<b>que tem torcedor em tudo que é lugar.</b>

124
00:04:54,840 --> 00:04:58,559
<b>Mas a grosso modo, o CSA é o time da Lagoa</b>

125
00:04:58,559 --> 00:05:03,159
<b>e a relação das pessoas da região lagunar</b>

126
00:05:03,400 --> 00:05:05,440
<b>com o CSA, é impressionante.</b>

127
00:05:05,440 --> 00:05:06,679
<b>e eu digo isso para você</b>

128
00:05:06,679 --> 00:05:09,360
<b>como alguém que anda por ali</b>

129
00:05:09,360 --> 00:05:10,599
<b>convive por ali</b>

130
00:05:10,599 --> 00:05:14,719
<b>e que andou pelo bom parto em dia de jogo do CSA.</b>

131
00:05:16,119 --> 00:05:16,679
<b>Entendeu?</b>

132
00:05:16,679 --> 00:05:19,559
<b>Eu andei pelas ruas onde fica o quintal cultural</b>

133
00:05:19,559 --> 00:05:21,199
<b>quando tinha jogo CSA.</b>

134
00:05:21,199 --> 00:05:24,400
<b>E eu vi a relação das pessoas com aquele time.</b>

135
00:05:25,199 --> 00:05:29,480
<b>Eu vi a relação de quem mora ali na vizinhança</b>

136
00:05:29,480 --> 00:05:31,519
<b>com aquele time, não é?</b>

137
00:05:31,519 --> 00:05:34,000
<b>Eu vi a relação das pessoas</b>

138
00:05:34,000 --> 00:05:35,360
<b>em termos de torcer</b>

139
00:05:35,360 --> 00:05:38,320
<b>em termos de ser parte da vida delas.</b>

140
00:05:39,320 --> 00:05:43,000
<b>Eu nem curto futebol</b>

141
00:05:43,000 --> 00:05:46,840
<b>eu nem sou fã, nem sou CSA nem CRB</b>

142
00:05:46,840 --> 00:05:50,159
<b>nem sou fã de nenhum outro time de futebol do Brasil.</b>

143
00:05:50,159 --> 00:05:52,800
<b>eu Já perdi jogo da seleção Brasileira</b>

144
00:05:52,800 --> 00:05:55,400
<b>em final de Copa do Mundo.</b>

145
00:05:55,400 --> 00:05:57,079
<b>Pra você ter uma ideia, assim.</b>

146
00:05:57,079 --> 00:06:00,199
<b>E eu sou capaz de avaliar</b>

147
00:06:00,199 --> 00:06:02,960
<b>a perda gigantesca para as pessoas</b>

148
00:06:02,960 --> 00:06:04,840
<b>do CSA sair dalí.</b>

149
00:06:06,360 --> 00:06:09,239
<b>Do fato de o CSA saur dalí, entendeu?</b>

150
00:06:09,239 --> 00:06:11,360
<b>Então nós estamos falando de algo</b>

151
00:06:11,360 --> 00:06:14,079
<b>que pode não parecer patrimônio para as pessoas</b>

152
00:06:14,079 --> 00:06:16,880
<b>mas é patrimônio, é patrimônio daquele lugar.</b>

153
00:06:18,039 --> 00:06:19,440
<b>Para as pessoas que estão alí</b>

154
00:06:19,440 --> 00:06:20,239
<b>é patrimônio daquele lugar.</b>

155
00:06:20,239 --> 00:06:21,880
<b>para as pessoas da região lagunar</b>

156
00:06:21,880 --> 00:06:23,760
<b>é patrimônio da vida delas.</b>

157
00:06:24,400 --> 00:06:29,440
<b>E o CSA saiu e não vai voltar para alí.</b>

158
00:06:30,119 --> 00:06:33,440
<b>Em termos de de bem-estar, qualidade de vida.</b>

159
00:06:34,039 --> 00:06:35,840
<b>Para elas, faz diferença.</b>

160
00:06:35,840 --> 00:06:38,360
<b>É algo que piora a vida delas</b>

161
00:06:38,360 --> 00:06:40,559
<b>algo que torna a vida delas menos agradável</b>

162
00:06:40,559 --> 00:06:42,960
<b>e mais triste, nesse sentido.</b>

163
00:06:42,960 --> 00:06:47,360
<b>Estou falando aqui somente da saída dos CSA.</b>

164
00:06:50,119 --> 00:06:52,599
<b>Eu poderia falar de inúmeras outras coisas</b>

165
00:06:52,599 --> 00:06:54,679
<b>Eu poderia falar de inúmeras...</b>

166
00:06:54,679 --> 00:06:57,280
<b>Eu poderia falar da praça Lucena Maranhão</b>

167
00:06:58,800 --> 00:07:02,360
<b>que foi palco de quantas festas de rua</b>

168
00:07:02,360 --> 00:07:03,599
<b>para aquela população ali</b>

169
00:07:03,599 --> 00:07:06,039
<b>para outras pessoas de Maceió?</b>

170
00:07:06,039 --> 00:07:09,880
<b>Eu poderia falar de cada de cada igreja</b>

171
00:07:09,880 --> 00:07:12,519
<b>como a própria igreha da Lucena Maranhão, né?</b>

172
00:07:12,519 --> 00:07:14,880
<b>A Igreja Santo Antônio, que foi fechada.</b>

173
00:07:14,880 --> 00:07:16,800
<b>Eu poderia falar do colégio Bom Conselho</b>

174
00:07:16,800 --> 00:07:18,760
<b>onde turmas e turmas estudaram.</b>

175
00:07:18,760 --> 00:07:21,840
<b>Eu poderia falar do Quintal Cultural.</b>

176
00:07:23,559 --> 00:07:26,599
<b>Então assim, a perda de de vida</b>

177
00:07:26,599 --> 00:07:29,559
<b>a perda de vida é algo impressionante.</b>

178
00:07:29,559 --> 00:07:34,239
<b>É algo que vai ficar por gerações na cidade.</b>

179
00:07:34,239 --> 00:07:36,039
<b>É algo que com certeza</b>

180
00:07:36,039 --> 00:07:39,519
<b>torna mais triste a vida de várias pessoas</b>

181
00:07:40,519 --> 00:07:43,159
<b>daqueles lugares, especialmente</b>

182
00:07:43,159 --> 00:07:44,559
<b>e da cidade como um todo.</b>

183
00:07:44,559 --> 00:07:46,719
<b>Não é uma questão de dizer assim:</b>

184
00:07:46,719 --> 00:07:49,280
<b>vamos fazer um projeto patrimônio para resgatar.</b>

185
00:07:49,280 --> 00:07:50,760
<b>Nós não vamos resgatar nada.</b>

186
00:07:51,559 --> 00:07:54,400
<b>Qualquer projeto patrimônio nesse lugar</b>

187
00:07:54,400 --> 00:07:56,960
<b>tem que ser um projeto que se proponha</b>

188
00:07:56,960 --> 00:08:01,159
<b>a trabalhar com a dor da perda das pessoas</b>

189
00:08:01,159 --> 00:08:03,039
<b>com a dor mesmo.</b>

190
00:08:03,039 --> 00:08:05,679
<b>Então qualquer projeto de educação patrimonial</b>

191
00:08:05,679 --> 00:08:06,719
<b>de preservação</b>

192
00:08:06,719 --> 00:08:09,840
<b>tem que ser um projeto que considere</b>

193
00:08:09,840 --> 00:08:12,280
<b>o fato inescapável</b>

194
00:08:12,280 --> 00:08:16,439
<b>de que nós precisamos ressignificar</b>

195
00:08:16,439 --> 00:08:17,840
<b>a perda das pessoas.</b>

196
00:08:17,840 --> 00:08:20,279
<b>Ressignificar as memórias.</b>

197
00:08:20,279 --> 00:08:22,800
<b>que foi o que foi feito no vale do Rio Doce.</b>

198
00:08:22,800 --> 00:08:26,079
<b>As pessoas vão ter que ressignificar</b>

199
00:08:26,079 --> 00:08:28,119
<b>a sua relação com aquele lugar.</b>

200
00:08:28,960 --> 00:08:30,520
<b>Pode-se chegar a um ponto</b>

201
00:08:30,520 --> 00:08:32,119
<b>em que a relação volte a ser</b>

202
00:08:32,119 --> 00:08:34,119
<b>uma relação feliz.</b>

203
00:08:34,119 --> 00:08:36,600
<b>Pode! Eu Acredito plenamente nisso.</b>

204
00:08:36,600 --> 00:08:39,520
<b>Dependendo de como se proceda</b>

205
00:08:39,520 --> 00:08:43,920
<b>com o que vai ser feito com esse lugar.</b>

206
00:08:43,920 --> 00:08:46,560
<b>E não é, sou muito clara em relação a isso...</b>

207
00:08:46,560 --> 00:08:49,680
<b>Não é fazendo um museu.</b>

208
00:08:50,960 --> 00:08:55,560
<b>O museu das memórias das pessoas.</b>

209
00:08:55,560 --> 00:08:56,159
<b>Não é isso.</b>

210
00:08:56,159 --> 00:08:57,880
<b>Não que você não deva trabalhar com c</b>

211
00:08:57,880 --> 00:08:59,760
<b>com as memórias</b>

212
00:08:59,760 --> 00:09:01,079
<b>não que você não deva trabalhar</b>

213
00:09:01,079 --> 00:09:04,800
<b>com a parte museológica.</b>

214
00:09:04,800 --> 00:09:06,479
<b>Não é isso! O que eu quero dizer é que</b>

215
00:09:06,479 --> 00:09:08,840
<b>não é fazer um museu disso</b>

216
00:09:08,840 --> 00:09:12,760
<b>que vai ressignificar essas memórias</b>

217
00:09:12,760 --> 00:09:13,960
<b>para essas pessoas.</b>

218
00:09:13,960 --> 00:09:15,520
<b>É a sensibilidade no trato</b>

219
00:09:15,520 --> 00:09:17,039
<b>é o respeito com essas memórias</b>

220
00:09:17,039 --> 00:09:21,279
<b>é envolver as pessoas</b>

221
00:09:21,359 --> 00:09:23,960
<b>num projeto para esse lugar.</b>

222
00:09:23,960 --> 00:09:28,760
<b>Toda vez que se fala de projeto para esse lugar</b>

223
00:09:28,760 --> 00:09:31,439
<b>nunca se fala</b>

224
00:09:31,439 --> 00:09:35,720
<b>de uma reunião feita previamente</b>

225
00:09:35,720 --> 00:09:37,960
<b>com as pessoas para pensar</b>

226
00:09:37,960 --> 00:09:40,119
<b>o projeto pra esse lugar.</b>

227
00:09:40,119 --> 00:09:42,680
<b>É sempre um projeto que está vindo</b>

228
00:09:42,680 --> 00:09:44,760
<b>de alguém para...</b>

229
00:09:44,760 --> 00:09:48,760
<b>Nunca é um projeto que foi feito a partir </b>

230
00:09:49,399 --> 00:09:52,840
<b>do contato com moradores</b>

231
00:09:52,840 --> 00:09:56,760
<b>com pessoas, com visitantes</b>

232
00:09:56,760 --> 00:09:59,159
<b>com a participação da cidade.</b>

233
00:09:59,159 --> 00:10:02,880
<b>é sempre alguém de dentro de</b>

234
00:10:02,880 --> 00:10:04,640
<b>um órgão do poder público</b>

235
00:10:04,640 --> 00:10:08,199
<b>é sempre alguém dentro de uma universidade</b>

236
00:10:08,199 --> 00:10:09,199
<b>num grupo de pesquisas.</b>

237
00:10:09,199 --> 00:10:11,199
<b>Nunca é algo que é dito assim:</b>

238
00:10:11,199 --> 00:10:13,479
<b>nós, estamos fazendo isso a partir</b>

239
00:10:13,479 --> 00:10:15,800
<b>de um contato com as pessoas</b>

240
00:10:15,800 --> 00:10:17,359
<b>para que elas digam</b>

241
00:10:17,359 --> 00:10:19,560
<b>o que elas querem que seja feito</b>

242
00:10:19,600 --> 00:10:21,439
<b>já que elas não vão poder mais morar nesse lugar.</b>

243
00:10:22,039 --> 00:10:25,479
<b>Se acaso não vão poder mais viver nesses bairros</b>

244
00:10:25,479 --> 00:10:29,359
<b>o que elas acreditam que deve existir?</b>

245
00:10:29,359 --> 00:10:31,600
<b>O que deve ser feito nesses lugares?</b>

246
00:10:31,600 --> 00:10:34,159
<b>Nunca é assim. Nunca é assim:</b>

247
00:10:34,159 --> 00:10:36,239
<b>vamos fazer uma oficina de projeto</b>

248
00:10:36,239 --> 00:10:38,319
<b>com as pessoas</b>

249
00:10:38,319 --> 00:10:42,800
<b>pra ver o que vai sair daí de desejo</b>

250
00:10:42,800 --> 00:10:44,920
<b>de vontade expressa pra esse lugar?</b>

251
00:10:44,920 --> 00:10:45,840
<b>Nunca é assim.</b>


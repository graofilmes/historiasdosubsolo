1
00:00:00,760 --> 00:00:02,400
<b>O erro começou lá atrás, não é?</b>

2
00:00:02,400 --> 00:00:04,159
<b>Desde que foi implantado.</b>

3
00:00:04,159 --> 00:00:05,199
<b>Porque, olha:</b>

4
00:00:08,159 --> 00:00:10,800
<b>de... eu não posso dizer se foi...</b>

5
00:00:10,800 --> 00:00:12,840
<b>de alguns anos para cá</b>

6
00:00:12,840 --> 00:00:14,920
<b>até há mais tempo não é?</b>

7
00:00:14,920 --> 00:00:16,039
<b>Primeiro que</b>

8
00:00:16,719 --> 00:00:17,880
<b>eu não sei por que</b>

9
00:00:17,880 --> 00:00:19,400
<b>não foi feito isso na época</b>

10
00:00:19,400 --> 00:00:21,440
<b>lá na década de 70</b>

11
00:00:21,440 --> 00:00:24,599
<b> pelo menos na década, no fina da décadal de 70.</b>

12
00:00:24,599 --> 00:00:27,400
<b>Não fizeram o que eu vou dizer agora.</b>

13
00:00:29,199 --> 00:00:31,719
<b>As perfurações que foram feitas</b>

14
00:00:31,719 --> 00:00:33,440
<b>para dizer qual é o tipo de rocha</b>

15
00:00:34,400 --> 00:00:37,639
<b>foi perfuração igual se faz para poço artesiano</b>

16
00:00:38,119 --> 00:00:39,599
<b>que apenas vê qual o tipo de área</b>

17
00:00:39,599 --> 00:00:41,280
<b>mas não vê a resistência da rocha.</b>

18
00:00:41,280 --> 00:00:42,360
<b> Isso é fundamental, gente.</b>

19
00:00:43,280 --> 00:00:44,519
<b>Na época, não foi determinado.</b>

20
00:00:44,519 --> 00:00:45,960
<b>Não consta em nenhum canto</b>

21
00:00:45,960 --> 00:00:50,159
<b>que eles determinaram o grau de resistência</b>

22
00:00:50,159 --> 00:00:52,000
<b>de cada rocha ao longo da profundidade.</b>

23
00:00:52,000 --> 00:00:53,039
<b>Isso é fundamental.</b>

24
00:00:53,840 --> 00:00:54,719
<b>Não foi feito isso.</b>

25
00:00:54,719 --> 00:00:57,320
<b>Foi feito apenas esse primeiro ponto</b>

26
00:00:57,320 --> 00:00:59,400
<b>Então está lá só o tipo de óleo só aumentam. Eu descobri</b>
<b>isso.</b>

27
00:01:00,159 --> 00:01:02,559
<b>ntão está lá só o tipo de óleo.</b>

28
00:01:03,519 --> 00:01:04,800
<b>Eu descobri isso</b>

29
00:01:05,719 --> 00:01:07,039
<b>porque tem uma tese de doutorado</b>

30
00:01:07,039 --> 00:01:10,039
<b>do professor Cláudio Pires</b>

31
00:01:10,039 --> 00:01:11,400
<b>que foi feito aqui</b>

32
00:01:12,000 --> 00:01:13,400
<b>inclusive, pelo que eu sei</b>

33
00:01:13,400 --> 00:01:15,039
<b>foi financiado pela própria Braskem</b>

34
00:01:16,480 --> 00:01:18,599
<b>e ele fez essa...</b>

35
00:01:18,599 --> 00:01:21,239
<b>Como é uma tese doutorado, muito importante e tal</b>

36
00:01:21,239 --> 00:01:24,800
<b>também determinou o grau de integridade das rochas.</b>

37
00:01:24,800 --> 00:01:27,480
<b>Foi aí que eu vi que</b>

38
00:01:27,480 --> 00:01:28,639
<b>a rocha</b>

39
00:01:28,639 --> 00:01:32,960
<b>os 200m de rocha que antecede a camada de sal</b>

40
00:01:32,960 --> 00:01:36,239
<b>essa camada de onde ela mais ou menos</b>

41
00:01:36,239 --> 00:01:38,360
<b>entre 750m e 950m. </b>

42
00:01:38,360 --> 00:01:41,320
<b>Essa camada de rocha é extremamente frágil.</b>

43
00:01:42,440 --> 00:01:45,719
<b>É extremamente alterada, não é, rocha sã.</b>

44
00:01:46,360 --> 00:01:47,719
<b>É por isso que a as Minas</b>

45
00:01:47,719 --> 00:01:50,920
<b>além de desabar lá embaixo, começaram a subir.</b>

46
00:01:52,920 --> 00:01:54,119
<b>Tem muita a 700m de profundidade</b>

47
00:01:54,119 --> 00:01:57,719
<b>tem as que subiu até 500m.</b>

48
00:01:57,719 --> 00:01:58,960
<b>Quer dizer, subiu 400m</b>

49
00:01:58,960 --> 00:02:01,360
<b>estava a 950m e subiu até 550m.</b>

50
00:02:01,360 --> 00:02:02,880
<b>Subiu 400m.</b>

51
00:02:03,480 --> 00:02:04,519
<b>É a mina 25.</b>

52
00:02:04,519 --> 00:02:06,880
<b>Fica perto do IMA, essa mina.</b>

53
00:02:06,880 --> 00:02:07,960
<b>Foi a que mais subiu.</b>

54
00:02:07,960 --> 00:02:11,280
<b>E as outras ali, onde era a casa do Zé Lopes ali</b>

55
00:02:11,280 --> 00:02:13,519
<b>muitas delas ali subiu</b>

56
00:02:13,519 --> 00:02:16,199
<b>nessa fase de de de 200m, 300m</b>

57
00:02:16,199 --> 00:02:18,599
<b>mais ou menos isso, 250m por aí.</b>

58
00:02:18,599 --> 00:02:23,159
<b>A felicidade é que nós temos camada de rocha boa</b>

59
00:02:23,159 --> 00:02:26,679
<b>razoavelmente boa, a partir de 500m.</b>


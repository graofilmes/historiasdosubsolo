1
00:00:00,559 --> 00:00:02,280
<b>Quando me perguntava</b>

2
00:00:02,280 --> 00:00:04,039
<b>lá no segundo semestre de 2018</b>

3
00:00:04,039 --> 00:00:06,280
<b>nas minhas palestras, no meus encontros:</b>

4
00:00:07,159 --> 00:00:08,400
<b>Professor, e aí, qual a solução?</b>

5
00:00:08,400 --> 00:00:09,840
<b>A solução é aterrar.</b>

6
00:00:11,039 --> 00:00:12,800
<b>A solução é aterrar os buracos todinhos</b>

7
00:00:12,800 --> 00:00:13,519
<b>as cavernas.</b>

8
00:00:14,639 --> 00:00:16,239
<b>Areia, pedregulho, o que for.</b>

9
00:00:17,360 --> 00:00:18,039
<b>Está sendo feito.</b>

10
00:00:18,920 --> 00:00:21,280
<b>E é a solução que se tem pelo mundo afora.</b>

11
00:00:22,000 --> 00:00:23,880
<b>Agora, não pense que vai</b>

12
00:00:23,880 --> 00:00:25,880
<b>mesmo aterrando</b>

13
00:00:25,880 --> 00:00:28,239
<b>vai demorar ainda muito tempo para estabilizar</b>

14
00:00:28,239 --> 00:00:29,599
<b>porque tem uns vazios, não é?</b>

15
00:00:29,599 --> 00:00:31,639
<b>Você está bombeando areia</b>

16
00:00:32,679 --> 00:00:34,480
<b>e a areia vai ficando lá</b>

17
00:00:34,480 --> 00:00:37,280
<b>mas ela não fica consolidada.</b>

18
00:00:39,440 --> 00:00:40,280
<b>Eu gostaria muito</b>

19
00:00:40,280 --> 00:00:43,280
<b>que tivessem injetado areia com cimento.</b>

20
00:00:44,920 --> 00:00:45,920
<b>Eu trabalho com isso.</b>

21
00:00:47,480 --> 00:00:48,480
<b>Argamassa.</b>

22
00:00:49,800 --> 00:00:52,280
<b>Mas só que é dez vezes mais caro.</b>

23
00:00:53,840 --> 00:00:57,159
<b>Aí estão injetando a areia</b>

24
00:00:58,239 --> 00:01:01,559
<b>Só que essa Areia vai ficar lá</b>

25
00:01:02,360 --> 00:01:03,519
<b>para adensar.</b>

26
00:01:04,480 --> 00:01:06,400
<b>Então não vai ser de um dia pro outro.</b>

27
00:01:06,400 --> 00:01:09,039
<b>Encheu a mina já ficou resolvido?</b>

28
00:01:09,039 --> 00:01:09,960
<b>Não, não é assim.</b>

29
00:01:10,239 --> 00:01:13,519
<b>Ainda vai levar um bom tempo.</b>

30
00:01:14,199 --> 00:01:15,960
<b>Mas são 35 minas! E aí?</b>

31
00:01:16,679 --> 00:01:18,000
<b>É gente, mas tem um estudo que</b>

32
00:01:18,000 --> 00:01:19,480
<b>e eu concordo com isso</b>

33
00:01:19,480 --> 00:01:20,800
<b>tem estudos que mostram</b>

34
00:01:20,800 --> 00:01:23,960
<b>que muitas minas vão se auto-aterrar.</b>

35
00:01:25,199 --> 00:01:26,280
<b>O que já está acontecendo</b>

36
00:01:26,280 --> 00:01:27,280
<b>em algumas delas.</b>

37
00:01:27,880 --> 00:01:29,000
<b>Então, pelo menos</b>

38
00:01:29,000 --> 00:01:31,039
<b>possivelmente, e eu vou dar um chute aqui</b>

39
00:01:31,039 --> 00:01:34,679
<b>dessas Minas que precisam ser injetadas</b>

40
00:01:34,679 --> 00:01:37,239
<b>são talvez no máximo a metade</b>

41
00:01:37,239 --> 00:01:38,519
<b>porque o resto...</b>

42
00:01:38,519 --> 00:01:39,840
<b>Aalgumas estão...</b>

43
00:01:39,840 --> 00:01:41,519
<b>E se você monitorar algumas</b>

44
00:01:41,519 --> 00:01:43,760
<b>que estão totalmente dentro do sal</b>

45
00:01:43,760 --> 00:01:50,519
<b>e não têm a falha geológica passando nelas...</b>

46
00:01:50,519 --> 00:01:52,599
<b>Então, essa que estão totalmente dentro do sal</b>

47
00:01:52,599 --> 00:01:55,679
<b>e que estão comportadas, algumas delas...</b>

48
00:01:55,760 --> 00:01:56,519
<b>Aí, essas minas</b>

49
00:01:57,199 --> 00:02:00,199
<b>podem simplesmente ser estabilizada através de pressão.</b>

50
00:02:01,320 --> 00:02:03,280
<b>Porque o ambiente</b>

51
00:02:03,280 --> 00:02:05,400
<b>o estado dela permite isso.</b>

52
00:02:05,400 --> 00:02:07,280
<b>Ser estabilizadas através de pressão.</b>

53
00:02:07,280 --> 00:02:10,280
<b>É claro que essa aí vai ter que ser monitorada</b>

54
00:02:11,159 --> 00:02:12,639
<b>permanentemente.</b>

55
00:02:12,639 --> 00:02:16,480
<b>Aí cabe agora à Agência Nacional de Mineração</b>

56
00:02:16,480 --> 00:02:21,360
<b>acompanhar rigorosamente essa monitoração, não é?</b>

57
00:02:22,000 --> 00:02:22,760
<b>Então é assim</b>

58
00:02:22,760 --> 00:02:27,199
<b>parte das minas será estabilizada por esse processo</b>

59
00:02:27,199 --> 00:02:28,199
<b>aquelas que estão...</b>

60
00:02:28,199 --> 00:02:29,519
<b>Mas isso, gente, olha...</b>

61
00:02:30,480 --> 00:02:32,559
<b>Vocês podem ficar tranquilos com o seguinte:</b>

62
00:02:32,559 --> 00:02:35,679
<b>hoje eu digo que a relação entre</b>

63
00:02:35,679 --> 00:02:37,400
<b>a Agência Nacional de Mineração</b>

64
00:02:37,400 --> 00:02:40,000
<b>e a mineradora Braskem</b>

65
00:02:40,760 --> 00:02:43,119
<b>é bem...</b>

66
00:02:43,119 --> 00:02:46,239
<b>Acabou aquela relação de compadre e comadre, entendeu?</b>

67
00:02:46,840 --> 00:02:49,559
<b>Isso aí era até 2018....</b>

68
00:02:49,559 --> 00:02:53,440
<b>Depois o bicho começou a pegar</b>

69
00:02:53,440 --> 00:02:57,800
<b>começou a pegar e aí hoje ANM tem cobrado</b>

70
00:02:57,800 --> 00:02:59,000
<b>eu tenho relatórios</b>

71
00:02:59,000 --> 00:03:02,360
<b>e a ANM está cobrando mesmo, forte.</b>


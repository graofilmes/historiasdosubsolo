1
00:00:00,000 --> 00:00:01,880
<b>Nós temos o estado de Alagoas</b>

2
00:00:01,880 --> 00:00:03,679
<b>chamado "Alagoas"</b>

3
00:00:03,679 --> 00:00:05,880
<b>justamente por causa das lagoas.</b>

4
00:00:06,360 --> 00:00:07,840
<b>Alagoas é um termo antigo</b>

5
00:00:07,840 --> 00:00:10,119
<b>um termo geográfico antigo</b>

6
00:00:10,119 --> 00:00:11,639
<b>que significa lagoas.</b>

7
00:00:11,639 --> 00:00:13,880
<b>Então o lugar das alagoas</b>

8
00:00:13,880 --> 00:00:15,599
<b>que é como nós passamos a ser conhecidos</b>

9
00:00:15,599 --> 00:00:19,800
<b>hiostoricamente uma porção ao sul da capitaia de Pernambuco</b>

10
00:00:19,800 --> 00:00:23,960
<b>diferenciada pela presença abundante da água</b>

11
00:00:23,960 --> 00:00:25,679
<b>e de matas exuberantes.</b>

12
00:00:25,840 --> 00:00:28,159
<b>Então o lugar das alagoas significa</b>

13
00:00:28,159 --> 00:00:30,119
<b>o lugar das lagoas.</b>

14
00:00:31,360 --> 00:00:34,639
<b>O estado é conhecido pela profusão de lagoas</b>

15
00:00:34,639 --> 00:00:37,639
<b>mas tem três lagoas que são mais significativas</b>

16
00:00:37,639 --> 00:00:38,480
<b>e dessas três</b>

17
00:00:38,480 --> 00:00:42,440
<b>duas formam o Complexo Estuarino Lagunar Mundaú/Manguaba</b>

18
00:00:42,440 --> 00:00:43,519
<b>que é o CELMM.</b>

19
00:00:43,639 --> 00:00:45,960
<b>Emtão são duas lagoas irmãs</b>

20
00:00:45,960 --> 00:00:48,760
<b>que se comunicam entre elas</b>

21
00:00:48,760 --> 00:00:52,320
<b>com vários canais e várias ilhas lacustres</b>

22
00:00:52,320 --> 00:00:54,079
<b>e que têm abertura</b>

23
00:00:54,079 --> 00:00:55,679
<b>têm comunicação com o mar.</b>

24
00:00:55,679 --> 00:00:57,880
<b>São lagunas, no termo técnico</b>

25
00:00:57,880 --> 00:01:00,400
<b>justamente porque têm comunicação com o mar</b>

26
00:01:00,400 --> 00:01:03,119
<b>mas o termo mais antigo: Alagoas</b>

27
00:01:03,119 --> 00:01:05,119
<b>foi o que ficou</b>

28
00:01:05,119 --> 00:01:07,559
<b>então nós chamamos de lagoas, que é o termo leigo</b>

29
00:01:07,559 --> 00:01:09,599
<b>pra essa massa de água.</b>

30
00:01:10,639 --> 00:01:13,800
<b>Esse complexo lagunar banha sete municípios</b>

31
00:01:13,800 --> 00:01:15,480
<b>dentre eles, Maceió.</b>

32
00:01:15,480 --> 00:01:17,719
<b>Então Maceió pe uma cidade</b>

33
00:01:17,719 --> 00:01:19,719
<b>que tem duas orlas</b>

34
00:01:19,719 --> 00:01:20,760
<b>uma orla marítima</b>

35
00:01:20,760 --> 00:01:22,800
<b>que é a mais conhecida de quem vem pra cá</b>

36
00:01:22,800 --> 00:01:23,960
<b>como visitante</b>

37
00:01:23,960 --> 00:01:25,280
<b>e uma orla lagunar.</b>

38
00:01:25,599 --> 00:01:27,199
<b>Em termos de tamanho</b>

39
00:01:27,199 --> 00:01:27,920
<b>de perímetro</b>

40
00:01:27,920 --> 00:01:29,679
<b>elas são equivalentes.</b>

41
00:01:29,679 --> 00:01:31,400
<b>A quilometragem...</b>

42
00:01:31,400 --> 00:01:33,599
<b>Inclusive elas se encontram na Barra</b>

43
00:01:33,599 --> 00:01:36,159
<b>justamente porque têm comunicação com o mar</b>

44
00:01:36,159 --> 00:01:37,360
<b>chega um ponto</b>

45
00:01:37,360 --> 00:01:39,039
<b>quem ja veio aqui pra Maceió</b>

46
00:01:39,039 --> 00:01:42,320
<b>se conheceu o que nós chamamos de Pontal da Barra</b>

47
00:01:42,320 --> 00:01:44,159
<b>que é uma ponta mesmo</b>

48
00:01:44,159 --> 00:01:45,000
<b>geograficamente falando</b>

49
00:01:45,000 --> 00:01:47,559
<b>uma ponta onde Maeió vai afinando</b>

50
00:01:47,559 --> 00:01:48,840
<b>vai afinando</b>

51
00:01:49,800 --> 00:01:52,000
<b>e naquele finalzinho daquela ponta</b>

52
00:01:52,000 --> 00:01:54,559
<b>é onde a lagoa encontra com o mar</b>

53
00:01:54,559 --> 00:01:55,960
<b>e forma a barra da lagoa.</b>

54
00:01:56,760 --> 00:01:59,400
<b>Então essas duas orlas vão se afinando</b>

55
00:01:59,400 --> 00:02:00,880
<b>até chegar num ponto que é a barra</b>

56
00:02:02,519 --> 00:02:05,800
<b>São 24km de margem</b>

57
00:02:05,800 --> 00:02:07,119
<b>da lagoa em Maceió</b>

58
00:02:07,119 --> 00:02:09,039
<b>começando por esse bairro, que é o Pontal da Barra</b>

59
00:02:09,039 --> 00:02:10,320
<b>onde tem a barra da lagoa</b>

60
00:02:10,320 --> 00:02:13,039
<b>e indo até um bairro chamado Rio Novo</b>

61
00:02:13,039 --> 00:02:15,440
<b>que é o limite de Maceió com outro município</b>

62
00:02:15,440 --> 00:02:16,400
<b>chamado Satuba</b>

63
00:02:16,400 --> 00:02:18,800
<b>que é um dos municípios banhados pela lagoa Mundaú</b>

64
00:02:19,480 --> 00:02:22,480
<b>Então essa lagoa, que é a lagoa Mundaú</b>

65
00:02:22,480 --> 00:02:23,599
<b>banha Maceió</b>

66
00:02:23,599 --> 00:02:27,239
<b>se eu não me engano são dez bairros</b>

67
00:02:27,239 --> 00:02:29,320
<b>do Pontal até Rio Novo</b>

68
00:02:29,559 --> 00:02:30,880
<b>e desses bairros</b>

69
00:02:30,880 --> 00:02:32,840
<b>alguns bairros fazem parte</b>

70
00:02:32,840 --> 00:02:34,920
<b>desses bairros afetados pela mineração</b>


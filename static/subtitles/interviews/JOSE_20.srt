1
00:00:00,159 --> 00:00:01,639
<b>Projeto Cata-Vento</b>

2
00:00:02,440 --> 00:00:06,679
Foi um projeto elaborado pela defesa civil

3
00:00:07,320 --> 00:00:09,199
<b>do estado de Alagoas</b>

4
00:00:10,599 --> 00:00:11,719
na época

5
00:00:11,719 --> 00:00:13,639
em que eu estava na coordenação

6
00:00:13,639 --> 00:00:15,000
do meio ambiente.

7
00:00:16,719 --> 00:00:18,719
<b>Era um projeto</b>

8
00:00:19,519 --> 00:00:23,239
para se houvesse

9
00:00:23,239 --> 00:00:26,039
não é vazamento fugitivo, não é

10
00:00:26,039 --> 00:00:30,159
a Braskem, ela tem um risco ambiental

11
00:00:30,159 --> 00:00:33,559
calculado inclusive pela CETESB

12
00:00:33,559 --> 00:00:37,440
publicado pelo governador Suruagy no diário oficial

13
00:00:37,440 --> 00:00:43,159
ela tem um risco não negligenciável

14
00:00:43,159 --> 00:00:48,519
de um acidente forte liberar

15
00:00:48,519 --> 00:00:52,880
<b>muito cloro, na cidade de Maceió.</b>

16
00:00:53,920 --> 00:00:56,639
Então a defesa civil foi encarregada

17
00:00:56,639 --> 00:00:58,960
de preparar um documento para

18
00:00:58,960 --> 00:01:02,599
se houver essa catástrofe

19
00:01:03,280 --> 00:01:06,400
<b>o que é que a população deve fazer?</b>

20
00:01:07,280 --> 00:01:09,960
Gente, chama-se Operação Cata-Vento,

21
00:01:09,960 --> 00:01:11,199
vão atrás.

22
00:01:13,639 --> 00:01:17,559
Mas o que é que diz o projeto Cata-Vento?

23
00:01:18,440 --> 00:01:21,960
<b>Primeiro, constata que o risco existe.</b>

24
00:01:22,679 --> 00:01:24,679
E aí vem uma série de coisas que

25
00:01:24,679 --> 00:01:27,920
que não daria para detalhar muito agora

26
00:01:27,920 --> 00:01:29,760
mas vou dar apenas alguns exemplos

27
00:01:29,760 --> 00:01:31,960
algumas pinceladas

28
00:01:31,960 --> 00:01:36,400
e ver se vocês vão ficar pasmos ou não.

29
00:01:37,400 --> 00:01:38,400
<b>Aí dizia assim:</b>

30
00:01:39,440 --> 00:01:42,079
Se o acidente for num dia

31
00:01:42,079 --> 00:01:44,639
de jogo no estádio rei Pelé.

32
00:01:47,119 --> 00:01:48,719
<b>Vocês estão imaginando a cena.</b>

33
00:01:50,679 --> 00:01:53,280
<b>Tá, o que é que se deve fazer?</b>

34
00:01:54,800 --> 00:01:56,920
<b>Uma voz tranquila</b>

35
00:01:57,960 --> 00:02:01,119
<b>deve transmitir pelo alto-falante:</b>

36
00:02:02,039 --> 00:02:03,239
<b>Aconteceu!</b>

37
00:02:04,559 --> 00:02:06,840
<b>Mas não há motivo para pânico.</b>

38
00:02:08,199 --> 00:02:10,760
<b>Por que o cloro</b>

39
00:02:10,760 --> 00:02:14,920
é facilmente é contornável

40
00:02:14,920 --> 00:02:16,679
pelo uso da água.

41
00:02:17,480 --> 00:02:19,360
Então, peguem em um lenço

42
00:02:19,360 --> 00:02:21,679
(gente, está lá!)

43
00:02:22,480 --> 00:02:25,320
molhem e coloque no nariz.

44
00:02:25,320 --> 00:02:29,159
A população deve ser evacuada para o CEPA.

45
00:02:30,480 --> 00:02:32,920
Gente, vão atrás da Operação Cata-Vento

46
00:02:32,920 --> 00:02:35,159
por favor, localizem.


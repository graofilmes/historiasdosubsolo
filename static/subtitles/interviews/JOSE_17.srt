1
00:00:00,400 --> 00:00:03,760
<b>Olha, essa contradição</b>

2
00:00:03,760 --> 00:00:07,159
<b>é uma contradição inevitável</b>

3
00:00:08,280 --> 00:00:11,719
<b>mas ela pode ser muito minimizada</b>

4
00:00:12,719 --> 00:00:14,559
<b>com medidas de mitigação</b>

5
00:00:14,559 --> 00:00:16,840
<b>com medidas preventivas</b>

6
00:00:16,840 --> 00:00:19,079
<b>pode ser muito minimizada.</b>

7
00:00:19,920 --> 00:00:23,159
<b>Realmente, ela se encontra em alta</b>

8
00:00:24,719 --> 00:00:26,320
<b>e entrou em baixa</b>

9
00:00:26,320 --> 00:00:29,079
<b>numa posição que a gente vinha seguindo</b>

10
00:00:29,079 --> 00:00:31,719
<b>e vem seguindo há algum tempo</b>

11
00:00:32,400 --> 00:00:35,719
<b>que é a posição que foi deslanchada</b>

12
00:00:35,719 --> 00:00:38,559
<b>pelo professor Ignacy Sachs</b>

13
00:00:38,559 --> 00:00:42,599
<b>que veio algumas vezes aqui em Maceió.</b>

14
00:00:43,920 --> 00:00:47,480
<b>Que é a proposta do ecodesenvolvimento.</b>

15
00:00:48,320 --> 00:00:51,559
<b>Nós não somos contra o desenvolvimento.</b>

16
00:00:52,199 --> 00:00:53,599
<b>Agora, nós perguntamos:</b>

17
00:00:53,599 --> 00:00:56,039
<b>que tipo de desenvolvimento?</b>


1
00:00:00,559 --> 00:00:03,400
Eu fazia o meu mestrado

2
00:00:03,400 --> 00:00:04,880
em Ecologia

3
00:00:04,880 --> 00:00:07,559
na Universidade Federal do Rio de Janeiro.

4
00:00:08,840 --> 00:00:10,400
No tempo que eu morei no Rio

5
00:00:10,400 --> 00:00:12,880
eu era muito amigo de Cacá Diegues

6
00:00:12,880 --> 00:00:16,559
frequentava o apartamento dele.

7
00:00:17,079 --> 00:00:19,119
<b>Ele ainda era casado com Nara Leão.</b>

8
00:00:19,719 --> 00:00:22,159
Nós também ficamos muito amigos

9
00:00:22,159 --> 00:00:26,599
e o Cacá me disse

10
00:00:26,599 --> 00:00:28,840
que Divaldo Suruagy

11
00:00:28,840 --> 00:00:33,039
tinha acabado de ser eleito governador de Alagoas

12
00:00:33,039 --> 00:00:36,679
e Cacá estava entusiasmado porque leu um

13
00:00:37,599 --> 00:00:38,639
<b>uma entrevista dele.</b>

14
00:00:39,400 --> 00:00:43,559
Ele falando que faria uma Alagoas ecológica

15
00:00:43,559 --> 00:00:44,760
ou alguma coisa assim.

16
00:00:46,119 --> 00:00:49,880
Então claro que eu também fiquei bastante entusiasmado.

17
00:00:50,599 --> 00:00:53,280
E aí eu mandei um cartão para Suruagy

18
00:00:53,280 --> 00:00:57,400
dando os parabéns pela entrevista.

19
00:00:59,320 --> 00:01:03,360
Então Suruagy me convidou para uma

20
00:01:03,360 --> 00:01:05,559
<b>Gostaria de me entrevistar.</b>

21
00:01:06,400 --> 00:01:08,840
<b>Então, numa das minhas vindas a Maceió.</b>

22
00:01:12,519 --> 00:01:13,639
<b>Ele me convidou</b>

23
00:01:13,679 --> 00:01:17,039
<b>ele estava montando o secretariado dele</b>

24
00:01:17,800 --> 00:01:20,440
e ele me convidou para uma entrevista

25
00:01:20,440 --> 00:01:22,159
na Assembleia Legislativa.

26
00:01:23,199 --> 00:01:24,360
<b>Então eu fui. </b>

27
00:01:24,360 --> 00:01:26,760
<b>Foi uma entrevista muito boa.</b>

28
00:01:28,000 --> 00:01:29,840
Divaldo Suruagy

29
00:01:29,840 --> 00:01:32,880
sejam quais sejam as divergências...

30
00:01:33,320 --> 00:01:34,519
<b>Mas ele era um lorde.</b>

31
00:01:34,519 --> 00:01:36,239
<b>Ele era um diplomata.</b>

32
00:01:36,800 --> 00:01:38,320
<b>Ele era muito bem educado.</b>

33
00:01:39,760 --> 00:01:41,800
E então, na ocasião

34
00:01:41,800 --> 00:01:48,239
ele me falou que gostaria que eu assumisse

35
00:01:49,599 --> 00:01:53,599
alguma coisa relacionada com meio ambiente e ecologia

36
00:01:53,599 --> 00:01:54,960
aqui em Alagoas.

37
00:01:55,960 --> 00:01:58,000
Então também eu falei que gostaria

38
00:01:58,000 --> 00:02:00,599
embora eu estivesse cursando o mestrado

39
00:02:00,599 --> 00:02:03,760
mas eu poderia muito bem interromper por algum tempo

40
00:02:03,760 --> 00:02:05,679
 e então eu viria para aqui

41
00:02:05,679 --> 00:02:07,920
porque aqui não havia nada

42
00:02:07,920 --> 00:02:11,599
absolutamente nada

43
00:02:11,599 --> 00:02:15,000
em relação à proteção ambiental.

44
00:02:15,920 --> 00:02:17,679
<b>Então ele me disse o seguinte:</b>

45
00:02:21,599 --> 00:02:23,280
Queria indicar meu nome

46
00:02:23,280 --> 00:02:26,840
para uma secretaria que já existia

47
00:02:26,840 --> 00:02:28,199
e ninguém sabia.

48
00:02:29,519 --> 00:02:33,960
Chamava-se Secretaria Executiva de Controle da Poluição.

49
00:02:34,880 --> 00:02:39,920
Tinha sido criada por iniciativa da Marinha

50
00:02:39,920 --> 00:02:42,320
aqui em Alagoas.

51
00:02:43,440 --> 00:02:46,320
Mas foi criada, inclusive legalmente

52
00:02:46,320 --> 00:02:47,599
e ficou dormindo.

53
00:02:47,960 --> 00:02:50,039
<b>Ninguém nunca a tinha acordado.</b>

54
00:02:50,599 --> 00:02:55,360
A indicação do meu nome era considerada problemática.

55
00:02:56,639 --> 00:02:58,760
Havia dois nomes

56
00:02:58,760 --> 00:03:04,719
<b>que o establishment militar da época</b>

57
00:03:07,519 --> 00:03:09,440
Não viam com bons olhos

58
00:03:09,440 --> 00:03:11,639
na montagem da secretaria dele.

59
00:03:13,559 --> 00:03:16,199
Um era de uma pessoa fantástica

60
00:03:16,199 --> 00:03:18,760
com quem eu vim a trabalhar depois

61
00:03:18,760 --> 00:03:21,519
o professor José de Melo Gomes

62
00:03:21,519 --> 00:03:23,559
e o outro era o meu.

63
00:03:25,280 --> 00:03:28,159
Mas, por via das dúvidas

64
00:03:28,159 --> 00:03:32,719
então ele não tinha como fazer uma indicação oficial.

65
00:03:34,559 --> 00:03:35,840
<b>Eu assumiria</b>

66
00:03:35,840 --> 00:03:39,840
<b>ficaria respondendo de fato</b>

67
00:03:41,599 --> 00:03:43,000
<b>pela secretaria</b>

68
00:03:43,000 --> 00:03:46,000
<b>e vamos construir....</b>

69
00:03:46,880 --> 00:03:48,519
Mas não tinha nem uma sala

70
00:03:48,519 --> 00:03:50,400
onde nós pudéssemos ficar.

71
00:03:50,679 --> 00:03:52,199
Equipe? Nenhuma!

72
00:03:52,920 --> 00:03:58,800
Então, tudo, eu comecei a tentar e a conseguir.

73
00:04:00,639 --> 00:04:03,199
Então eu comecei a me preocupar

74
00:04:03,199 --> 00:04:05,760
com uma possível equipe mínima

75
00:04:05,760 --> 00:04:07,480
que funcionou.

76
00:04:07,840 --> 00:04:10,360
As pessoas não sabiam que era uma equipe mínima

77
00:04:10,360 --> 00:04:12,840
e achavam que era uma equipe máxima.

78
00:04:13,559 --> 00:04:14,599
Era uma equipe máxima

79
00:04:14,599 --> 00:04:15,920
em termos realmente de

80
00:04:15,920 --> 00:04:20,079
de doação, de entusiasmo e de tudo.

81
00:04:20,599 --> 00:04:25,639
Foi quando entrou José Roberto

82
00:04:25,639 --> 00:04:30,559
que depois veio a ser secretário, ocupar o IMA

83
00:04:31,880 --> 00:04:36,760
<b>e um rapaz chamado Osvaldo Viegas.</b>

84
00:04:37,280 --> 00:04:40,719
Então a secretaria que existia no papel

85
00:04:40,719 --> 00:04:42,639
e que eu passei a ocupar de fato

86
00:04:42,639 --> 00:04:46,280
ela chamava se Secretaria Executiva...

87
00:04:46,280 --> 00:04:49,280
<b>Era a Secretaria de Controle da Poluição.</b>

88
00:04:49,800 --> 00:04:52,360
E eu era o Secretário Executivo

89
00:04:52,360 --> 00:04:56,199
de Controle da Poluição do Estado de Alagoas.

90
00:04:57,559 --> 00:04:59,639
Isso durou muito tempo

91
00:04:59,639 --> 00:05:03,360
até que através do professor José de Melo Gome

92
00:05:03,360 --> 00:05:04,639
foi criada

93
00:05:06,000 --> 00:05:08,119
ainda não era a Secretaria do Meio Ambiente

94
00:05:08,119 --> 00:05:11,000
era Coordenação do Meio Ambiente

95
00:05:11,000 --> 00:05:14,039
<b>e ele me convidou para assumir.</b>

96
00:05:15,280 --> 00:05:16,920
Convidou a nossa equipe

97
00:05:16,920 --> 00:05:20,199
e aí nós fomos com o entusiasmo

98
00:05:20,199 --> 00:05:23,960
do qual nós estávamos embutidos, envolvidos

99
00:05:23,960 --> 00:05:28,400
já sabendo que tínhamos uma guerra pela frente

100
00:05:28,400 --> 00:05:31,559
que era a implantação da Salgema

101
00:05:31,559 --> 00:05:34,760
que foi a nossa prioridade maior na
época.

102
00:05:35,599 --> 00:05:39,639
A criação da Secretaria de Controle da Poluição

103
00:05:39,639 --> 00:05:42,840
foi 1974.

104
00:05:44,320 --> 00:05:48,000
A criação da coordenação do meio ambiente

105
00:05:48,000 --> 00:05:50,039
salvo engano

106
00:05:50,039 --> 00:05:53,920
porque eu já tenho direito a...

107
00:05:55,000 --> 00:05:59,400
<b>Tô com muita coisa acumulada na memória.</b>

108
00:05:59,679 --> 00:06:02,719
<b>Mas, salvo engano, foi em 1976.</b>


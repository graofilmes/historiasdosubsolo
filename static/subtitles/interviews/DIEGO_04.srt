1
00:00:00,375 --> 00:00:02,293
O rompimento de uma barragem

2
00:00:02,293 --> 00:00:04,546
você vê nitidamente o antes e o depois.

3
00:00:04,838 --> 00:00:06,297
aconteceu o rompimento

4
00:00:06,840 --> 00:00:09,384
e aí vêm todas

5
00:00:09,384 --> 00:00:13,096
toda a consequência nefasta na situação.

6
00:00:13,888 --> 00:00:16,933
Só que no caso de Maceió.
E por isso que esse caso de Maceió

7
00:00:16,933 --> 00:00:20,228
que é um caso que a gente fala: sui generes.

8
00:00:20,562 --> 00:00:23,314
Ele tem um caráter muito ruim
pela magnitude

9
00:00:23,815 --> 00:00:26,901
que a mineração em áreas urbanas
em cidades menores acontece.

10
00:00:27,569 --> 00:00:31,322
Peixoto de Azevedo no Mato Grosso
é um caso

11
00:00:31,531 --> 00:00:33,199
mas esse caso aqui:

12
00:00:33,950 --> 00:00:37,662
Ele é um caso que envolve

13
00:00:37,662 --> 00:00:39,914
um impacto que ele é

14
00:00:39,914 --> 00:00:41,499
diluído

15
00:00:41,750 --> 00:00:44,377
ou seja o desastre

16
00:00:44,377 --> 00:00:45,503
ele não tem um dia

17
00:00:45,503 --> 00:00:47,047
ele vai ter até um marco

18
00:00:47,464 --> 00:00:49,841
quando ocorreu aquele tremor

19
00:00:49,841 --> 00:00:52,552
mas não é suficiente

20
00:00:52,552 --> 00:00:55,180
então as pessoas continuarão a morar nos seus locais.

21
00:00:55,722 --> 00:00:58,099
As rachaduras foram surgindo aos poucos

22
00:00:58,975 --> 00:01:01,186
em algumas áreas

23
00:01:02,479 --> 00:01:03,772
ali dos bairros.

24
00:01:03,772 --> 00:01:06,107
O que acontece: começa a afundar.

25
00:01:07,192 --> 00:01:09,277
Só que não é tudo de uma vez.

26
00:01:09,277 --> 00:01:11,654
é isso do ponto de vista

27
00:01:11,905 --> 00:01:14,783
para quem está fazendo avaliação de impacto na saúde

28
00:01:15,533 --> 00:01:19,245
Isso é tão ruim quanto um desastre que ocorreu...

29
00:01:19,954 --> 00:01:22,082
e aí por exemplo tantos mortos de imediato

30
00:01:23,291 --> 00:01:24,125
porque?

31
00:01:24,125 --> 00:01:25,752
porque ele é cumulativo.

32
00:01:26,711 --> 00:01:28,755
Então as pessoas passam a viver uma tensão

33
00:01:29,756 --> 00:01:31,091
e isso vai afetar

34
00:01:31,091 --> 00:01:32,467
por isso aquelas hipóteses

35
00:01:32,759 --> 00:01:33,510
que eu levantei

36
00:01:33,510 --> 00:01:35,261
vai aumentar o transtorno de ansiedade

37
00:01:35,845 --> 00:01:37,305
vai aumentar a depressão

38
00:01:37,305 --> 00:01:39,641
vai aumentar a ideação suicida

39
00:01:40,225 --> 00:01:42,644
vai aumentar a tentativa de suicídio

40
00:01:42,644 --> 00:01:44,687
vão aumentar os casos de suicídio.

41
00:01:44,938 --> 00:01:46,064
com moradores

42
00:01:46,815 --> 00:01:49,317
então um conjunto de fatores

43
00:01:49,317 --> 00:01:51,653
que implica

44
00:01:51,653 --> 00:01:54,656
como a situação do bairro

45
00:01:54,656 --> 00:01:55,698
dos bairros

46
00:01:56,032 --> 00:01:57,325
ela é única

47
00:01:57,325 --> 00:02:01,579
do um ponto de vista de comunidades
que foram afetadas diretamente

48
00:02:01,579 --> 00:02:04,791
por uma atividade econômica como a mineração.

49
00:02:05,333 --> 00:02:09,963
O que eu posso assegurar é que
não há caso registrado

50
00:02:10,713 --> 00:02:11,798
numa capital

51
00:02:12,423 --> 00:02:16,636
com uma população que ultrapassa
por exemplo 400 mil habitantes. Maceió.

52
00:02:17,178 --> 00:02:20,807
E aí se a gente perde a região
metropolitana o que é isso que eu falo.

53
00:02:21,224 --> 00:02:22,642
A área de influência direta

54
00:02:22,642 --> 00:02:24,644
porque não é só o Pinheiro, Mutange

55
00:02:24,978 --> 00:02:27,730
Bom Parto, Bebedouro que vão ser atingidos.

56
00:02:28,398 --> 00:02:30,984
Você tem as áreas de influência direta também

57
00:02:31,484 --> 00:02:33,278
então a Pitanguinha vai ser atingida

58
00:02:33,278 --> 00:02:34,696
Gruta de Lourdes vai ser atingida.

59
00:02:35,405 --> 00:02:38,366
Isso afetapor exemplo as vias de acesso à cidade

60
00:02:38,908 --> 00:02:41,828
Essa área de influência direta
ela vai aumentar

61
00:02:42,287 --> 00:02:44,414
ela vai chegar por exemplo onde eu estou: Jatiúca.

62
00:02:44,956 --> 00:02:48,626
Porque o acesso viário vai ser diretamente prejudicado.

63
00:02:49,627 --> 00:02:53,506
Então não há no mundo
um caso dessa magnitude

64
00:02:54,424 --> 00:02:56,759
que possa ser feita uma comparação

65
00:02:57,343 --> 00:03:01,347
com esse caso da Braskem
porque no final das contas

66
00:03:01,347 --> 00:03:04,392
a Braskem está comprando. Tá indenizado né?

67
00:03:05,310 --> 00:03:07,729
os moradores e todas aquelas residências

68
00:03:08,104 --> 00:03:09,856
aquelas pessoas que têm histórias

69
00:03:10,231 --> 00:03:14,819
viveram ali 20, 30, 60 anos etc.

70
00:03:14,819 --> 00:03:16,487
Elas sedem

71
00:03:17,113 --> 00:03:19,908
o espaço e o espaço
a gente sabe que tem memória.

72
00:03:20,658 --> 00:03:22,285
Elas cedem

73
00:03:22,619 --> 00:03:23,661
aquele espaço

74
00:03:24,078 --> 00:03:26,080
e aquilo vai ser transformado em algo.

75
00:03:26,706 --> 00:03:29,751
Então esse caso realmente de Maceió
é um caso

76
00:03:30,585 --> 00:03:32,253
é um caso único.

77
00:03:33,296 --> 00:03:35,215
O paralelo. À magnitude

78
00:03:35,215 --> 00:03:36,633
quando a gente vai fazer comparação

79
00:03:36,674 --> 00:03:39,344
a gente tem que comparar o que é comparável

80
00:03:39,344 --> 00:03:42,347
não tem parâmetro nessa magnitude.

81
00:03:42,430 --> 00:03:43,264
Não tem.


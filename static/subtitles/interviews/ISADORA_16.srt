1
00:00:00,079 --> 00:00:01,239
<b>Vai depender muito de nós</b>

2
00:00:01,239 --> 00:00:03,599
<b>o que esses lugares serão daqui a 20 ou 30 anos.</b>

3
00:00:03,599 --> 00:00:05,800
<b>Eu posso dizer para você o que eu desejo.</b>

4
00:00:06,920 --> 00:00:10,079
<b>Como eu desejo que sejam essas regiões</b>

5
00:00:10,079 --> 00:00:12,039
<b>daqui a 20, 30 ou 40 anos.</b>

6
00:00:12,039 --> 00:00:14,559
<b>Eu desejo que essas regiões</b>

7
00:00:14,559 --> 00:00:16,840
<b>daqui a 20, 30 ou 40 anos</b>

8
00:00:16,840 --> 00:00:20,000
<b>sejam regiões que voltem a ter ocupação</b>

9
00:00:20,000 --> 00:00:21,519
<b>ocupação de pessoas</b>

10
00:00:21,519 --> 00:00:24,679
<b>porque tiveram seus problemas solucionados</b>

11
00:00:26,800 --> 00:00:29,599
<b>tanto pela pela engenhosidade humana</b>

12
00:00:29,599 --> 00:00:30,880
<b>de encontrar soluções</b>

13
00:00:30,880 --> 00:00:32,760
<b>de engenharia possíveis</b>

14
00:00:32,760 --> 00:00:33,760
<b>para o fenômeno.</b>

15
00:00:33,760 --> 00:00:34,559
<b>E algumas já existem.</b>

16
00:00:35,239 --> 00:00:37,039
<b>Como pela pesquisa</b>

17
00:00:37,039 --> 00:00:38,400
<b>em relação ao que pode ser feito.</b>

18
00:00:38,400 --> 00:00:40,159
<b>Porque eu acho que esse é um ponto que</b>

19
00:00:40,159 --> 00:00:41,519
<b>já deveria estar acontecendo, inclusive.</b>

20
00:00:42,280 --> 00:00:44,679
<b>Há soluções para uma parte dos fenômenos</b>

21
00:00:44,679 --> 00:00:46,599
<b>mas não há soluções para uma parte.</b>

22
00:00:46,599 --> 00:00:48,360
<b>Então, se não há soluções conhecidas</b>

23
00:00:48,360 --> 00:00:49,239
<b>para uma parte dos fenômenos</b>

24
00:00:49,239 --> 00:00:50,599
<b>elas tinham que ser pesquisadas.</b>

25
00:00:51,239 --> 00:00:53,159
<b>Então, já deveria ter sido aberta</b>

26
00:00:53,159 --> 00:00:54,800
<b>pelo poder público federal</b>

27
00:00:54,800 --> 00:00:56,639
<b>linhas de pesquisa</b>

28
00:00:57,360 --> 00:00:58,760
<b>assim, a rodo</b>

29
00:00:58,760 --> 00:01:01,599
<b>para se pesquisar sobre aqueles fenômenos</b>

30
00:01:01,599 --> 00:01:04,400
<b>e sobre soluções para resolvê-los.</b>

31
00:01:04,800 --> 00:01:08,679
<b>Então eu desejo que daqui a 20, 30 ou 40 anos</b>

32
00:01:08,679 --> 00:01:11,639
<b>tenham se encontrado soluções para essa questão.</b>

33
00:01:12,039 --> 00:01:15,559
<b>A área possa voltar a ser ocupada por pessoas</b>

34
00:01:15,559 --> 00:01:17,920
<b>e ao voltar a ser ocupado por pessoas</b>

35
00:01:17,920 --> 00:01:20,159
<b>que essa ocupação possa se dar</b>

36
00:01:20,159 --> 00:01:21,360
<b>de maneira planejada</b>

37
00:01:21,360 --> 00:01:23,480
<b>e em moldes sustentáveis.</b>

38
00:01:23,480 --> 00:01:26,760
<b>Então, que nós tenhamos futuramente bairros</b>

39
00:01:27,559 --> 00:01:29,079
<b>novamente naquele lugar.</b>

40
00:01:29,079 --> 00:01:33,239
<b>E bairros respeitando as características físicas</b>

41
00:01:33,239 --> 00:01:34,480
<b>ambientais</b>

42
00:01:34,480 --> 00:01:37,559
<b>e tomando partido dessas características</b>

43
00:01:37,559 --> 00:01:42,239
<b>fazendo com que surjam ocupações ainda melhores</b>

44
00:01:42,239 --> 00:01:45,280
<b>mais bonitas, mais vivas, mais ricas</b>

45
00:01:45,280 --> 00:01:47,599
<b>mais pulsantes, culturalmente falando</b>

46
00:01:47,599 --> 00:01:51,840
<b>porque se importaram em cuidar daquele lugar</b>

47
00:01:51,840 --> 00:01:54,960
<b>em cuidar do ambiente</b>

48
00:01:54,960 --> 00:01:56,320
<b>das características ambientais</b>

49
00:01:56,320 --> 00:01:58,159
<b>em cuidar do patrimônio</b>

50
00:01:58,159 --> 00:02:00,119
<b>em cuidar das pessoas dali</b>

51
00:02:00,119 --> 00:02:02,519
<b>e com isso tenham feito</b>

52
00:02:02,519 --> 00:02:04,719
<b>os lugares mais felizes</b>

53
00:02:04,719 --> 00:02:06,000
<b> é isso que eu desejo paraquele lugar.</b>


1
00:00:00,079 --> 00:00:03,119
<b>Ainda falando sobre essa dinâmica da cidade.</b>

2
00:00:03,119 --> 00:00:05,119
<b>A gente viu que os mapas</b>

3
00:00:05,119 --> 00:00:08,960
<b>que foram produzidos após o andamento dos estudos</b>

4
00:00:08,960 --> 00:00:11,039
<b>esses mapas de linhas de ações prioritárias</b>

5
00:00:11,039 --> 00:00:14,039
<b>eles foram se modificando de 2018 para cá.</b>

6
00:00:14,039 --> 00:00:17,800
<b>Obviamente aumentando cada dia mais a área afetada</b>

7
00:00:17,800 --> 00:00:21,679
<b>aumentando a área de famílias e lotes a serem desocupados.</b>

8
00:00:21,679 --> 00:00:24,559
<b>E aí passou a aparecer nesses mapas</b>

9
00:00:24,559 --> 00:00:26,960
<b>uma área de isolamento social.</b>

10
00:00:27,840 --> 00:00:29,719
<b>Eu queria que você falasse um pouquinho pra gente</b>

11
00:00:29,719 --> 00:00:33,039
<b>o que é essa área que não é deslocada</b>

12
00:00:33,039 --> 00:00:34,599
<b>em que pessoas não são removidas</b>

13
00:00:34,599 --> 00:00:38,360
<b>mas passam a se tornar áreas isoladas.</b>

14
00:00:38,360 --> 00:00:39,639
<b>Qual o impacto disso?</b>

15
00:00:40,400 --> 00:00:43,079
<b>É como se você tivesse um cinturão</b>

16
00:00:43,079 --> 00:00:45,800
<b>em torno da da área mais problemática.</b>

17
00:00:45,800 --> 00:00:48,880
<b>É uma área de diminuição de fluxo.</b>

18
00:00:49,679 --> 00:00:52,199
<b>Você começa a ter...</b>

19
00:00:52,199 --> 00:00:55,559
<b>É uma área de amortecimento, digamos assim, não é?</b>

20
00:00:55,559 --> 00:00:56,599
<b>Você começa a diminuir...</b>

21
00:00:56,599 --> 00:00:58,960
<b>Muito provavelmente o que vai incidir</b>

22
00:00:58,960 --> 00:01:01,679
<b>com mais força nessa área, em termos urbanísticos</b>

23
00:01:01,679 --> 00:01:03,000
<b>falando urbanisticamente</b>

24
00:01:03,000 --> 00:01:06,079
<b>são os parâmetros, né?</b>

25
00:01:06,079 --> 00:01:07,679
<b>Os índices e parâmetros urbanísticos.</b>

26
00:01:07,679 --> 00:01:09,599
<b>Então você vai ter provavelmente</b>

27
00:01:09,599 --> 00:01:11,519
<b>índices e parâmetros de mobilidade</b>

28
00:01:11,519 --> 00:01:12,840
<b>de pouco tráfego.</b>

29
00:01:13,599 --> 00:01:15,239
<b>Então, deve-se diminuir</b>

30
00:01:15,239 --> 00:01:18,679
<b>restringir o tráfego pesado e intenso nessas áreas.</b>

31
00:01:18,679 --> 00:01:21,800
<b>E deve-se também estabelecer parâmetros urbanísticos </b>

32
00:01:21,800 --> 00:01:25,360
<b>de menor adensamento e</b>

33
00:01:25,360 --> 00:01:28,239
<b>com certeza não vão ser liberadas</b>

34
00:01:28,239 --> 00:01:33,159
<b>as construções com maior altura, por exemplo.</b>

35
00:01:33,159 --> 00:01:35,920
<b>Então você começa a ter um a ter</b>

36
00:01:35,920 --> 00:01:40,159
<b>uma área de baixo adensamento </b>

37
00:01:40,159 --> 00:01:41,320
<b>e de baixa circulação.</b>

38
00:01:41,320 --> 00:01:43,519
<b>É o mais provável que aconteça nessa região.</b>

39
00:01:43,559 --> 00:01:47,000
<b>Para garantir que não haja maiores impactos</b>

40
00:01:47,000 --> 00:01:49,679
<b>desse tráfego mais intenso</b>

41
00:01:49,679 --> 00:01:51,599
<b>dessa construção mais intensa</b>

42
00:01:51,599 --> 00:01:53,639
<b>na borda do fenômeno</b>

43
00:01:53,639 --> 00:01:55,840
<b>é uma precaução, digamos assim.</b>

44
00:01:55,840 --> 00:01:57,000
<b>É uma precaução</b>

45
00:01:57,000 --> 00:02:00,360
<b>considerando que você vai estar no limiar</b>

46
00:02:00,360 --> 00:02:03,320
<b>de uma área considerada problemática geologicamente.</b>

47
00:02:04,800 --> 00:02:07,679
<b>Isso afeta também as pessoas que moram nessas áreas?</b>

48
00:02:08,119 --> 00:02:11,599
<b>Então isso afeta os imóveis, por exemplo.</b>

49
00:02:11,599 --> 00:02:13,639
<b>As pessoas vão ter, assim...</b>

50
00:02:15,000 --> 00:02:15,960
<b>Como é que eu posso dizer?</b>

51
00:02:15,960 --> 00:02:19,480
<b>Para a nossa lógica, hoje </b>

52
00:02:19,480 --> 00:02:21,119
<b>de ocupação urbana</b>

53
00:02:21,119 --> 00:02:25,880
<b>em que valoriza-se muito construir espigões</b>

54
00:02:25,880 --> 00:02:27,960
<b>valoriza-se muito construir</b>

55
00:02:27,960 --> 00:02:31,320
<b>empreendimentos habitacionais em edifícios</b>

56
00:02:31,320 --> 00:02:33,039
<b>você ter uma área </b>

57
00:02:33,039 --> 00:02:35,400
<b>em que não vão poder ser construído edifícios</b>

58
00:02:35,400 --> 00:02:36,159
<b>por exemplo</b>

59
00:02:36,159 --> 00:02:37,760
<b>a tendência disso é desvalorizar</b>

60
00:02:37,760 --> 00:02:39,559
<b>desvalorizar para o mercado imobiliário</b>

61
00:02:39,559 --> 00:02:41,039
<b>os imóveis naquela área.</b>

62
00:02:41,639 --> 00:02:43,760
<b>Então as pessoas daquela área</b>

63
00:02:43,760 --> 00:02:47,480
<b>elas vão ter os seus imóveis desvalorizados</b>

64
00:02:47,920 --> 00:02:51,159
<b>no momento em que esses índices forem aplicados.</b>

65
00:02:51,159 --> 00:02:54,000
<b>Agora, para a vida delas</b>

66
00:02:54,000 --> 00:02:56,360
<b>eu acho que o efeito é muito mais psicológico.</b>

67
00:02:57,199 --> 00:03:01,000
<b>Você vai se sentir no limiar de um problema.</b>

68
00:03:02,880 --> 00:03:04,480
<b>Você vai sentindo rodeado por um problema.</b>

69
00:03:04,480 --> 00:03:07,199
<b>Olha, se a minha área é uma área de amortecimento</b>

70
00:03:07,199 --> 00:03:08,880
<b>e eu estou morando num lugar</b>

71
00:03:08,880 --> 00:03:10,639
<b>em que não pode haver muito tráfego</b>

72
00:03:10,639 --> 00:03:12,880
<b>e não pode haver muita construção</b>

73
00:03:13,719 --> 00:03:15,519
<b>Porque eu estou no limiar</b>

74
00:03:15,519 --> 00:03:17,599
<b>de uma área problemática geologicamente</b>

75
00:03:17,599 --> 00:03:19,480
<b>então a tendência</b>

76
00:03:19,480 --> 00:03:21,679
<b>é que eu fique sempre me sentindo insegura.</b>

77
00:03:22,719 --> 00:03:25,000
<b>E isso é algo muito ruim.</b>

78
00:03:25,000 --> 00:03:27,559
<b>A tendência é que as pessoas, por exemplo</b>

79
00:03:27,559 --> 00:03:30,719
<b>elas também não se sintam confortáveis para investir</b>

80
00:03:30,719 --> 00:03:32,559
<b>na vida delas ali.</b>

81
00:03:32,559 --> 00:03:35,360
<b>Isso pode gerar desde problemas psicológicos</b>

82
00:03:35,360 --> 00:03:37,039
<b>até um esvaziamento de fato.</b>

83
00:03:37,719 --> 00:03:41,480
<b>O VLT, ele possivelmente</b>

84
00:03:41,480 --> 00:03:46,719
<b>é um veículo de maior impacto, de grande fluxo.</b>

85
00:03:47,159 --> 00:03:48,960
<b>E ele passa por essas áreas.</b>

86
00:03:48,960 --> 00:03:52,480
<b>Então qual vai ser a política pública</b>

87
00:03:52,480 --> 00:03:54,719
<b>para o VLT nesses lugares?</b>

88
00:03:55,400 --> 00:03:56,280
<b>Ninguém sabe ainda.</b>

89
00:03:56,280 --> 00:03:59,840
<b>Mas sabe-se que há uma grande parcela</b>

90
00:03:59,840 --> 00:04:04,039
<b> da população desses lugares que circula pelo VLT. </b>

91
00:04:05,599 --> 00:04:07,599
<b>Então, se houver restrição</b>

92
00:04:07,599 --> 00:04:09,519
<b>a esse modal, que eu não sei se vai ter</b>

93
00:04:09,519 --> 00:04:11,840
<b>mas se houver restrição a esse modal</b>

94
00:04:11,840 --> 00:04:15,000
<b>como isso vai impactar a que mora nessa região?</b>

95
00:04:16,079 --> 00:04:19,800
<b>Bebedouro, Mutange e Bom Parto</b>

96
00:04:19,800 --> 00:04:21,119
<b>são um caminho.</b>

97
00:04:21,119 --> 00:04:24,800
<b>Aquela via que segue para o norte de Maceió</b>

98
00:04:24,800 --> 00:04:26,599
<b>é, inclusive, um caminho único.</b>

99
00:04:27,320 --> 00:04:28,920
<b>Não há outros caminhos.</b>

100
00:04:28,920 --> 00:04:30,280
<b>Todo mundo fala: e a Fernandes Lima?</b>

101
00:04:30,280 --> 00:04:34,880
<b>A Fernandes Lima tem outros eixos paralelos a ela.</b>

102
00:04:35,840 --> 00:04:37,920
<b>Não são grandes eixos.</b>

103
00:04:38,480 --> 00:04:40,159
<b>Ela é o eixo principal</b>

104
00:04:40,159 --> 00:04:42,320
<b>mas há outros eixos</b>

105
00:04:42,320 --> 00:04:42,920
<b>que você consegue...</b>

106
00:04:42,920 --> 00:04:46,440
<b>Inclusive, aquela rua que passa pelo</b>

107
00:04:46,440 --> 00:04:49,000
<b>pela cambona e passa pelo Bom Parto</b>

108
00:04:49,000 --> 00:04:51,440
<b>Mutange, Bebedouro e vai até</b>

109
00:04:51,440 --> 00:04:52,880
<b>Santo Amélio e por aí vai...</b>

110
00:04:52,880 --> 00:04:55,559
<b>Ela é uma rota alternativa</b>

111
00:04:55,559 --> 00:04:58,119
<b>ou era, à Fernandes Lima.</b>

112
00:04:58,119 --> 00:05:03,679
<b>Mas a ela, a ela mesma, não há alternativa.</b>

113
00:05:04,239 --> 00:05:05,679
<b>O maior impacto mesmo</b>

114
00:05:05,679 --> 00:05:09,039
<b>é o fato das pessoas saírem de lá.</b>

115
00:05:09,039 --> 00:05:10,679
<b>Não há impacto maior que isso.</b>

116
00:05:12,280 --> 00:05:15,000
<b>Mas considerando o depois </b>

117
00:05:15,000 --> 00:05:18,719
<b>o depois desse esvaziamento</b>

118
00:05:18,719 --> 00:05:20,400
<b>o depois desse deslocamento</b>

119
00:05:21,199 --> 00:05:23,679
<b>o impacto do depois disso</b>

120
00:05:23,679 --> 00:05:28,000
<b>é de fato o impacto das restrições para quem fica.</b>

121
00:05:28,000 --> 00:05:30,559
<b>Para quem ficará nessa nessa área de isolamento</b>

122
00:05:30,559 --> 00:05:31,599
<b>por exemplo.</b>

123
00:05:31,599 --> 00:05:33,760
<b>Para quem ficará no entorno disso.</b>


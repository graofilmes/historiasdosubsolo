1
00:00:00,079 --> 00:00:02,559
<b>Boa parte da região</b>

2
00:00:02,559 --> 00:00:06,719
<b> ela se encontra destruída</b>

3
00:00:06,719 --> 00:00:09,320
<b>como se fosse um bombardeio.</b>

4
00:00:10,519 --> 00:00:14,119
<b>Então, não há como recuperar.</b>

5
00:00:15,239 --> 00:00:17,880
<b>Isso é causa perdida</b>

6
00:00:19,119 --> 00:00:23,480
<b>Ao passo que o programa que foi apresentado</b>

7
00:00:23,480 --> 00:00:25,719
<b>de recuperação</b>

8
00:00:26,360 --> 00:00:29,039
<b>ele contém falhas muito grandes.</b>

9
00:00:29,840 --> 00:00:31,920
<b>Ele é um programa</b>

10
00:00:31,920 --> 00:00:36,039
<b>que não precisa nem uma análise mais profunda.</b>

11
00:00:37,039 --> 00:00:41,079
<b>Tem partes inteiramente inviáveis.</b>

12
00:00:41,079 --> 00:00:42,559
<b>Inviáveis!</b>

13
00:00:42,559 --> 00:00:44,400
<b>Mesmo que haja</b>

14
00:00:44,400 --> 00:00:48,360
<b>ou que houvesse a melhor das intenções.</b>

15
00:00:49,519 --> 00:00:50,480
<b>Então não.</b>

16
00:00:52,480 --> 00:00:54,679
<b>Eu chego a fazer até uma...</b>

17
00:00:54,679 --> 00:00:56,800
<b>Cheguei até a fazer uma proposta</b>

18
00:00:57,599 --> 00:01:00,519
<b>que acho que foi um pouco</b>

19
00:01:00,519 --> 00:01:03,639
<b>da minha fase anárquica que aflorou:</b>

20
00:01:04,559 --> 00:01:10,079
<b>ao invés de ir para Alemanha</b>

21
00:01:11,440 --> 00:01:15,119
<b>pra ver os Campos de Concentração</b>

22
00:01:16,559 --> 00:01:18,960
<b>por que não transformar essa área</b>

23
00:01:18,960 --> 00:01:21,000
<b>em um atrativo turístico?</b>

24
00:01:21,960 --> 00:01:22,480
<b>Não é?</b>

25
00:01:23,239 --> 00:01:24,440
<b>Vamos mostrar</b>

26
00:01:24,440 --> 00:01:27,159
<b>como um atrativo turístico</b>

27
00:01:27,159 --> 00:01:31,679
<b>como foi possível bombardear sem bombas</b>

28
00:01:31,679 --> 00:01:34,000
<b>uma área inteira da cidade!</b>

29
00:01:36,119 --> 00:01:39,079
<b>Então, quanto à recuperação</b>

30
00:01:39,719 --> 00:01:43,559
<b>eu sou totalmente pessimista.</b>

31
00:01:43,559 --> 00:01:45,960
<b>Com os dados que eu dispunha</b>

32
00:01:45,960 --> 00:01:48,599
<b>eu comecei a ver que não havia mesmo</b>

33
00:01:48,599 --> 00:01:50,320
<b>nenhuma segurança</b>

34
00:01:50,320 --> 00:01:53,159
<b>principalmente depois que eu acordei</b>

35
00:01:53,159 --> 00:01:56,519
<b>e minha casa tinha sido pichada pela prefeitura.</b>

36
00:01:57,280 --> 00:02:00,440
<b>Gente, eu digo, isso é coisa que se fazia na Alemanha</b>

37
00:02:00,440 --> 00:02:03,280
<b>para os judeus deixarem as casas!</b>

38
00:02:03,280 --> 00:02:04,639
<b>Era um código</b>

39
00:02:04,639 --> 00:02:08,320
<b>que depois eu vinha saber o que é que era</b>

40
00:02:08,320 --> 00:02:10,960
<b>mas eu não tinha dado autorização nenhuma</b>

41
00:02:10,960 --> 00:02:13,360
<b>e minha casa estava pichada.</b>

42
00:02:13,360 --> 00:02:17,880
<b>Ou seja, o valor dela já tinha despencado.</b>

43
00:02:19,199 --> 00:02:21,039
<b>Então eu resolvi</b>

44
00:02:21,039 --> 00:02:24,480
<b>para proteger a minha família, principalmente</b>

45
00:02:24,480 --> 00:02:27,400
<b>me tornar um refugiado ambiental.</b>

46
00:02:28,679 --> 00:02:33,400
<b>Então eu deixei a casa e nós migramos.</b>

47
00:02:33,400 --> 00:02:38,079
<b>Somos, é refugiados ambientais.</b>

48
00:02:38,760 --> 00:02:41,719
<b>A Braskem nos fez uma proposta</b>

49
00:02:43,480 --> 00:02:46,239
<b>muito aquém do valor da casa.</b>

50
00:02:47,119 --> 00:02:49,960
<b>Mas foi o que o nosso advogado aconselhou:</b>

51
00:02:50,760 --> 00:02:51,400
<b>peguem.</b>

52
00:02:52,800 --> 00:02:55,960
<b>Peguem, porque se não pegar agora</b>

53
00:02:55,960 --> 00:03:00,719
<b>vai rolar na justiça por 10 anos, 15 anos</b>

54
00:03:00,719 --> 00:03:02,239
<b>ninguém sabe.</b>

55
00:03:03,119 --> 00:03:04,480
<b>Então nós pegamos.</b>

56
00:03:05,639 --> 00:03:11,159
<b>Nós tivemos realmente a nossa casa</b>

57
00:03:11,960 --> 00:03:15,039
<b>entregue compulsoriamente</b>

58
00:03:15,039 --> 00:03:17,960
<b>porque foi compulsoriamente, à Braskem.</b>

59
00:03:18,599 --> 00:03:22,599
<b>Casa e terreno hoje são da Braskem.</b>

60
00:03:23,239 --> 00:03:27,599
<b>Eu sempre usei a linguagem técnico-científica.</b>

61
00:03:27,599 --> 00:03:30,880
<b>Há uma probabilidade alta</b>

62
00:03:31,559 --> 00:03:33,920
<b>de que isso aconteça.</b>

63
00:03:34,519 --> 00:03:36,360
<b>Mas os fenômenos são muito dinâmicos.</b>

64
00:03:36,360 --> 00:03:39,000
<b>A situação mudou muito</b>

65
00:03:40,239 --> 00:03:41,840
<b>daqueles inícios para cá</b>

66
00:03:42,639 --> 00:03:46,119
<b>inclusive as situações lá embaixo mesmo.</b>

67
00:03:46,760 --> 00:03:49,159
<b>Segundo o professor Abel Galindo</b>

68
00:03:49,159 --> 00:03:51,440
<b>e eu respeito muito a opinião</b>

69
00:03:51,440 --> 00:03:53,039
<b>do professor Abel Galindo</b>

70
00:03:53,039 --> 00:03:54,760
<b>porque ele trabalha realmente</b>

71
00:03:54,760 --> 00:03:57,239
<b>com dados dos relatórios.</b>

72
00:03:57,239 --> 00:03:59,079
<b>Ultimamente, ele tem trabalhado</b>

73
00:03:59,079 --> 00:04:02,159
<b>com os dados que a Braskem fornece</b>

74
00:04:02,960 --> 00:04:04,519
<b>nos quais</b>

75
00:04:04,519 --> 00:04:06,239
<b>eu, particularmente</b>

76
00:04:06,239 --> 00:04:08,199
<b>não tenho muita confiança</b>

77
00:04:08,199 --> 00:04:09,719
<b>mas o professor Abel</b>

78
00:04:09,719 --> 00:04:11,119
<b>eu acho que ele consegue filtrar.</b>

79
00:04:12,320 --> 00:04:17,440
<b>Então, segundo ele, houve uma é reacomodação</b>

80
00:04:18,239 --> 00:04:21,000
<b>lá embaixo mesmo</b>

81
00:04:21,000 --> 00:04:27,119
<b>e aquela possibilidade inicial que existia do colapso</b>

82
00:04:27,880 --> 00:04:33,320
<b>do desabamento até rápido que podia acontecer</b>

83
00:04:33,320 --> 00:04:37,719
<b>felizmente parece que está afastada</b>

84
00:04:38,639 --> 00:04:39,840
<b>momentaneamente</b>

85
00:04:39,840 --> 00:04:43,039
<b>porque essas coisas mudam com muita facilidade.</b>

86
00:04:44,199 --> 00:04:49,239
<b>Agora, rachaduras continuam existindo</b>

87
00:04:49,239 --> 00:04:51,960
<b>nas casas e nas ruas.</b>

88
00:04:52,760 --> 00:04:56,840
<b>Rachaduras continuarão a existir</b>

89
00:04:56,840 --> 00:04:58,199
<b>e a se ampliar</b>

90
00:04:58,920 --> 00:05:01,280
<b>nas ruas e nas casas.</b>

91
00:05:02,000 --> 00:05:03,639
<b>Essa probabilidade</b>

92
00:05:03,639 --> 00:05:05,960
<b> é uma probabilidade alta?</b>

93
00:05:05,960 --> 00:05:07,360
<b>Não, é altíssima!</b>

94
00:05:08,639 --> 00:05:10,119
<b>O perigo passou, então</b>

95
00:05:10,119 --> 00:05:13,320
<b>a ser transferido, com os dados novos</b>

96
00:05:13,320 --> 00:05:16,360
<b>para a região de Bebedouro, próximo à lagoa</b>

97
00:05:18,039 --> 00:05:20,880
<b>Onde foi prevista</b>

98
00:05:20,880 --> 00:05:26,000
<b>e desde o início eu manifestei as minhas dúvidas</b>

99
00:05:26,000 --> 00:05:29,079
<b>que haveria realmente um colapso</b>

100
00:05:29,079 --> 00:05:31,199
<b>na região das lagoas</b>

101
00:05:31,199 --> 00:05:35,760
<b>intenso, forte e rápido.</b>

102
00:05:36,480 --> 00:05:37,880
<b>Com um grande desabamento</b>

103
00:05:37,880 --> 00:05:39,599
<b>uma grande subsidência.</b>

104
00:05:41,000 --> 00:05:42,320
<b>O professor Abel hoje</b>

105
00:05:42,320 --> 00:05:44,760
<b>coloca isso também em dúvida.</b>

106
00:05:44,760 --> 00:05:46,679
<b>O que ele mantém</b>

107
00:05:46,679 --> 00:05:48,880
<b>e isso é completamente inegável</b>

108
00:05:48,880 --> 00:05:51,400
<b>porque é visto pelos olhos</b>

109
00:05:51,400 --> 00:05:55,639
<b>pelo meu drone se consegue ver muito bem.</b>

110
00:05:57,280 --> 00:05:59,239
<b>O que está previsto é que tem toda...</b>

111
00:05:59,239 --> 00:06:02,239
<b>E isso aí, a probabilidade altíssima!</b>

112
00:06:02,239 --> 00:06:06,119
<b>Toda aquela área de Bebedouro</b>

113
00:06:06,119 --> 00:06:09,960
<b>entre, mais ou menos, o IMA</b>

114
00:06:09,960 --> 00:06:12,000
<b>e o colégio Bom Conselho</b>

115
00:06:12,000 --> 00:06:15,559
<b>dentro de um tempo relativamente curto</b>

116
00:06:15,559 --> 00:06:17,559
<b>ela será totalmente destruída.</b>


1
00:00:00,079 --> 00:00:02,440
Eu sou uma pessoa bastante intuitiva

2
00:00:02,440 --> 00:00:04,519
mas não foi por intuição

3
00:00:04,519 --> 00:00:06,000
nem foi por revelação

4
00:00:06,000 --> 00:00:07,480
não foi por nada disso.

5
00:00:08,559 --> 00:00:10,159
Em termos de ciência

6
00:00:10,159 --> 00:00:12,440
eu sou muito racional.

7
00:00:13,199 --> 00:00:15,800
Eu quero evidências e fatos

8
00:00:15,800 --> 00:00:18,719
e análise racional.

9
00:00:20,639 --> 00:00:23,639
<b>Hipóteses testáveis e tudo mais.</b>

10
00:00:24,239 --> 00:00:26,800
Quando eu vi a proposta

11
00:00:26,800 --> 00:00:30,480
que era a proposta ainda da salgema

12
00:00:30,480 --> 00:00:33,000
mas sua proposta que já estava no papel.

13
00:00:33,519 --> 00:00:35,840
<b>Então eu disse logo, olha:</b>

14
00:00:36,800 --> 00:00:40,519
Se eu sou o secretário de controle da poluição

15
00:00:40,519 --> 00:00:43,960
eu já dou não ao parecer.

16
00:00:45,199 --> 00:00:50,719
Lançaram ácido clorídrico à vontade

17
00:00:50,719 --> 00:00:54,239
ali no cais

18
00:00:54,239 --> 00:00:59,000
e a desculpa era que o mar é dilui tudo, não é?

19
00:00:59,760 --> 00:01:04,480
<b>Eu sei que o mar de lama</b>

20
00:01:06,280 --> 00:01:09,199
termina diluindo tudo mesmo, não é?

21
00:01:09,199 --> 00:01:11,320
Mas outro problema de poluição

22
00:01:11,320 --> 00:01:12,519
que está no fluxograma

23
00:01:12,519 --> 00:01:15,079
pode pegar no fluxograma até hoje.

24
00:01:15,960 --> 00:01:18,119
<b>Indústria de cloro-soda</b>

25
00:01:18,719 --> 00:01:21,440
<b>não tem como evitar.</b>

26
00:01:22,320 --> 00:01:25,079
Por mais bem intencionada que ela seja.

27
00:01:25,079 --> 00:01:29,000
São as emanações fugitivas.

28
00:01:29,760 --> 00:01:32,719
<b>Na própria produção, no processo</b>

29
00:01:33,280 --> 00:01:35,719
inevitavelmente ocorrem

30
00:01:35,719 --> 00:01:38,440
emanações de cloro.

31
00:01:39,239 --> 00:01:42,039
Ocorreram ao longo desses anos todos.

32
00:01:42,039 --> 00:01:44,840
Vocês sabem que nós tivemos problema no Pontal.

33
00:01:46,360 --> 00:01:48,559
Então, desde o início

34
00:01:49,119 --> 00:01:50,800
a minha posição foi essa:

35
00:01:50,800 --> 00:01:53,440
Não, não dá.

36
00:01:53,440 --> 00:01:57,760
Eles confiavam muito na ignorância tecnológica

37
00:01:57,760 --> 00:01:59,360
do estado de Alagoas.

38
00:02:00,920 --> 00:02:02,280
E quando começaram a ver

39
00:02:02,280 --> 00:02:04,840
que nós não éramos tão ignorantes assim, não!

40
00:02:04,840 --> 00:02:07,920
Não era opinião que eram as estávamos dando.

41
00:02:08,599 --> 00:02:11,880
Nós estávamos dando pareceres técnicos

42
00:02:11,880 --> 00:02:15,880
cientificamente embasados, não é?

43
00:02:15,880 --> 00:02:19,599
Aí eles começaram a festa.


1
00:00:00,320 --> 00:00:01,440
<b>O plano diretor</b>

2
00:00:01,440 --> 00:00:04,039
<b>não ter sido ainda entregue</b>

3
00:00:04,039 --> 00:00:05,280
<b>para a Câmara de Vereadores</b>

4
00:00:05,280 --> 00:00:10,119
<b>significa que nada da questão da Braskem</b>

5
00:00:10,119 --> 00:00:13,960
<b>foi inserido na revisão até onde a gente saiba.</b>

6
00:00:15,480 --> 00:00:16,280
<b>Nada!</b>

7
00:00:16,280 --> 00:00:21,039
<b>Eu posso até estar falando uma tolice aqui</b>

8
00:00:21,039 --> 00:00:22,760
<b>e sem a gente saber</b>

9
00:00:22,760 --> 00:00:29,559
<b>a prefeitura ter inserido isso no projeto de lei.</b>

10
00:00:29,559 --> 00:00:31,239
<b>Porque a gente não sabe!</b>

11
00:00:32,440 --> 00:00:35,119
<b>Desde 2018 que a gente não sabe</b>

12
00:00:35,119 --> 00:00:38,320
<b>o que foi feito no projeto de lei.</b>

13
00:00:38,320 --> 00:00:41,760
<b>Olha, 2018 é o ano que começou</b>

14
00:00:41,760 --> 00:00:45,480
<b>toda a questão da subsidência vindo à tona.</b>

15
00:00:48,159 --> 00:00:50,360
<b>Então é exatamente o ponto</b>

16
00:00:50,360 --> 00:00:51,960
<b>é exatamente o ano</b>

17
00:00:51,960 --> 00:00:55,760
<b>em que nós perdemos contato com a revisão</b>

18
00:00:55,760 --> 00:00:56,519
<b>na prática.</b>

19
00:00:56,519 --> 00:00:58,599
<b>Com o que foi elaborado</b>

20
00:00:58,599 --> 00:01:00,559
<b>e construído enquanto revisão.</b>

21
00:01:00,559 --> 00:01:02,360
<b>Então de 2018 para cá</b>

22
00:01:02,360 --> 00:01:04,159
<b>nós não sabemos de nada que foi inserido.</b>

23
00:01:05,400 --> 00:01:07,920
<b>Nós não sabemos tanto em termos escritos</b>

24
00:01:07,920 --> 00:01:11,079
<b>quando em termos de mapeamento.</b>

25
00:01:11,079 --> 00:01:14,440
<b>Nós não sabemos se os mapas...</b>

26
00:01:14,440 --> 00:01:18,880
<b>Claro que eu acredito que entraram.</b>

27
00:01:18,880 --> 00:01:24,519
<b>Eu acredito que o mapeamento feito pelo CPRM</b>

28
00:01:24,519 --> 00:01:28,559
<b>e todas as novas descobertas...</b>

29
00:01:28,559 --> 00:01:30,159
<b>Isso deve ter entrado.</b>

30
00:01:30,159 --> 00:01:32,000
<b>A prefeitura, os técnicos da prefeitura</b>

31
00:01:32,000 --> 00:01:32,880
<b>devem ter colocado.</b>

32
00:01:32,880 --> 00:01:34,119
<b>São técnicos competentes.</b>

33
00:01:34,119 --> 00:01:35,599
<b>Mas se você perguntar</b>

34
00:01:36,039 --> 00:01:36,920
<b>o que a gente sabe?</b>

35
00:01:36,920 --> 00:01:38,960
<b>E, olha, eu era membro do conselho</b>

36
00:01:38,960 --> 00:01:40,400
<b>de revisão do plano diretor.</b>

37
00:01:41,079 --> 00:01:42,719
<b>Eu era com o IAB</b>

38
00:01:42,719 --> 00:01:44,920
<b>e o IDEAL, como instituição</b>

39
00:01:44,920 --> 00:01:48,800
<b>tinha cadeira cativa no concelho de revisão.</b>

40
00:01:49,159 --> 00:01:50,440
<b>Então, teoricamente</b>

41
00:01:50,440 --> 00:01:51,960
<b>nós hoje como IDEAL...</b>

42
00:01:51,960 --> 00:01:54,840
<b>Como esse conselho, ele não foi</b>

43
00:01:55,760 --> 00:01:57,360
<b>desconfigurado</b>

44
00:01:57,360 --> 00:01:59,679
<b>ele ainda está, teoricamente</b>

45
00:01:59,679 --> 00:02:00,679
<b>como conselho de revisão.</b>

46
00:02:00,679 --> 00:02:01,559
<b>Então teoricamente, o IDEAL</b>

47
00:02:01,559 --> 00:02:03,920
<b>ainda tem cadeira nesse conselho.</b>

48
00:02:04,760 --> 00:02:06,920
<b>E de 2018 para cá</b>

49
00:02:06,920 --> 00:02:09,039
<b>já faz 3 anos, portanto</b>

50
00:02:09,039 --> 00:02:11,239
<b>2019, 2020 e 2021</b>

51
00:02:12,119 --> 00:02:14,920
<b>até hoje, nós não sabemos</b>

52
00:02:15,800 --> 00:02:16,960
<b>o que foi inserido</b>

53
00:02:16,960 --> 00:02:18,119
<b>e o que não foi inserido?</b>

54
00:02:18,119 --> 00:02:20,760
<b>Como está a redação do projeto de lei?</b>

55
00:02:20,760 --> 00:02:22,519
<b>O que há em termos de acompanhamento?</b>

56
00:02:22,519 --> 00:02:26,639
<b>Com entrou a questão do caso Braskem ?</b>

57
00:02:28,880 --> 00:02:30,599
<b>Não se sabe nada.</b>

58
00:02:30,599 --> 00:02:33,679
<b>Mas assim, nada é nada, zero.</b>

59
00:02:33,679 --> 00:02:37,159
<b>E não é só o conselho de revisão.</b>

60
00:02:38,239 --> 00:02:40,760
<b>O conselho de revisão é maior do que esse nome:</b>

61
00:02:40,760 --> 00:02:41,719
<b>"Conselho de Revisão".</b>

62
00:02:42,199 --> 00:02:46,480
<b>O conselho de revisão, ele envolve o IDEAL</b>

63
00:02:46,480 --> 00:02:48,599
<b>envolve uma instituição do peso do IAB</b>

64
00:02:48,599 --> 00:02:50,920
<b>mas envolve a Câmara de Vereadores, por exemplo.</b>

65
00:02:50,920 --> 00:02:53,440
<b>Então se o Conselho de Revisão não sabe de nada</b>

66
00:02:53,440 --> 00:02:55,039
<b>e se o projeto de lei não chegou na câmara</b>

67
00:02:55,039 --> 00:02:58,000
<b>teoricamente, a Câmara também não sabe de nada.</b>

68
00:02:58,960 --> 00:03:00,039
<b>Nós temos uma instituição</b>

69
00:03:00,039 --> 00:03:01,519
<b> como a Câmara de Vereadores municipal</b>

70
00:03:01,519 --> 00:03:03,920
<b>que é a maior instância do legislativo</b>

71
00:03:03,920 --> 00:03:05,039
<b>do município</b>

72
00:03:05,239 --> 00:03:07,920
<b>que não sabe nada sobre o projeto de lei</b>

73
00:03:07,920 --> 00:03:08,920
<b>que é o maior...</b>

74
00:03:08,920 --> 00:03:11,760
<b>Que rege toda a parte urbanística do município.</b>

75
00:03:11,760 --> 00:03:13,559
<b>mesmo que nós não tivéssemos</b>

76
00:03:13,559 --> 00:03:15,039
<b>uma questão tão grave</b>

77
00:03:15,039 --> 00:03:16,400
<b>como o caso Braskem acontecendo</b>

78
00:03:16,400 --> 00:03:18,079
<b>isso não se justificaria</b>

79
00:03:18,079 --> 00:03:20,199
<b>E diante do que está acontecendo</b>

80
00:03:20,199 --> 00:03:22,039
<b>aí é que não se justifica mesmo!</b>

81
00:03:22,320 --> 00:03:23,840
<b>Quando a pandemia começou em 2020</b>

82
00:03:23,840 --> 00:03:27,920
<b> já tinha 5 anos de atraso do Plano Diretor.</b>

83
00:03:28,360 --> 00:03:30,159
<b>Então não foi por causa da pandemia!</b>

84
00:03:30,159 --> 00:03:32,400
<b>Não teve nada a ver com a pandemia, esse atraso.</b>


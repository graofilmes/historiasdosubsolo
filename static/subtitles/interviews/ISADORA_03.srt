1
00:00:00,000 --> 00:00:00,000
<b>A ideia era chamar moradores</b>

2
00:00:00,000 --> 00:00:00,000
<b>ou pessoas que tivessem vínculo com o lugar</b>

3
00:00:00,000 --> 00:00:00,000
<b>e propor soluções para questões</b>

4
00:00:00,000 --> 00:00:00,000
<b>que os moradores julgassem importantes.</b>

5
00:00:00,000 --> 00:00:00,000
<b>Alí nós não queríamos...</b>

6
00:00:00,000 --> 00:00:00,000
<b>só sabíamos que tinham várias questões</b>

7
00:00:00,000 --> 00:00:00,000
<b>desde infraestrutura, passando por... Enfim.</b>

8
00:00:00,000 --> 00:00:00,000
<b>Mas a ideia era:</b>

9
00:00:00,000 --> 00:00:00,000
<b>de que os moradores estavam</b>

10
00:00:00,000 --> 00:00:00,000
<b>sentindo necessidade nesse momento?</b>

11
00:00:00,000 --> 00:00:00,000
<b>E aí nós chamamos</b>

12
00:00:00,000 --> 00:00:00,000
<b>e demos prioridade em chamar moradores</b>

13
00:00:00,000 --> 00:00:00,000
<b>para estarem lá</b>

14
00:00:00,000 --> 00:00:00,000
<b>mas era aberto a qualquer participante.</b>

15
00:00:00,000 --> 00:00:00,000
<b>Então quando os projetos surgiram</b>

16
00:00:00,000 --> 00:00:00,000
<b>surgiram seis grupos</b>

17
00:00:00,000 --> 00:00:00,000
<b>seis projetos foram apresentados</b>

18
00:00:00,000 --> 00:00:00,000
<b>e foram cinco premiados.</b>

19
00:00:00,000 --> 00:00:00,000
<b>A ideia é que nesse projeto eles iriam</b>

20
00:00:00,000 --> 00:00:00,000
<b>receber mentoria do SEBRAE.</b>

21
00:00:00,000 --> 00:00:00,000
<b>E naquele momento, um dos projetos premiados</b>

22
00:00:00,000 --> 00:00:00,000
<b>um dos projetos premiados foi o Histórias do Subsolo</b>

23
00:00:00,000 --> 00:00:00,000
<b>que foi super interessante</b>

24
00:00:00,000 --> 00:00:00,000
<b>porque na época nós estávamos</b>

25
00:00:00,000 --> 00:00:00,000
<b>o projeto estava concorrendo a uma seleção</b>

26
00:00:00,000 --> 00:00:00,000
<b>e no dia seguinte ia apresentar um "pitch"</b>

27
00:00:00,000 --> 00:00:00,000
<b>então no mesmo dia</b>

28
00:00:00,000 --> 00:00:00,000
<b>quando o projeto foi aprovado nós fizemos uma matéria</b>

29
00:00:00,000 --> 00:00:00,000
<b>mandamos o link</b>

30
00:00:00,000 --> 00:00:00,000
<b>eu sei que o projeto apresentou essa premiação</b>

31
00:00:00,000 --> 00:00:00,000
<b>então nós fizemos parte dessa história</b>

32
00:00:00,000 --> 00:00:00,000
<b>com muito orgulho!</b>

33
00:00:00,000 --> 00:00:00,000
<b>É um pedacinho pequenininho</b>

34
00:00:00,000 --> 00:00:00,000
<b>mas fazemos parte com muito orgulho.</b>

35
00:00:00,279 --> 00:00:03,239
<b>A ideia era chamar moradores</b>

36
00:00:03,239 --> 00:00:05,920
<b>ou pessoas que tivessem vínculo com o lugar</b>

37
00:00:05,920 --> 00:00:08,640
<b>e propor soluções</b>

38
00:00:08,640 --> 00:00:11,399
<b>para questões que os moradores julgassem importantes.</b>

39
00:00:11,399 --> 00:00:13,520
<b>Alí nós não queriamos catar</b>

40
00:00:13,520 --> 00:00:15,119
<b>mas nós sabíamos que tinham várias questões</b>

41
00:00:15,119 --> 00:00:17,399
<b>de infraestrutura, passando por... Enfim...</b>

42
00:00:17,399 --> 00:00:20,279
<b>Mas a ideia era: do que os moradores</b>

43
00:00:20,279 --> 00:00:22,159
<b>tinham sentido de necessidade nesse momento?</b>

44
00:00:22,600 --> 00:00:25,800
<b>E aí nós demos prioridade</b>

45
00:00:25,800 --> 00:00:28,159
<b>a chamar moradores pra estarem lá</b>

46
00:00:28,439 --> 00:00:30,199
<b>mas era aberto pra qualquer participante.</b>

47
00:00:30,439 --> 00:00:32,039
<b>Então quando os projetos surgiram</b>

48
00:00:32,039 --> 00:00:33,479
<b>surgiram seis grupos</b>

49
00:00:33,479 --> 00:00:35,680
<b>seis projetos foram apresentados</b>

50
00:00:35,920 --> 00:00:39,640
<b>e aí foram cinco premiados.</b>

51
00:00:39,640 --> 00:00:42,640
<b>A idéia é que esses projetos iam receber mentoria do SEBRAE</b>

52
00:00:43,239 --> 00:00:45,399
<b>E naquele momento</b>

53
00:00:45,399 --> 00:00:48,399
<b>um dos projetos premiados foi o Histórias do Subsolo</b>

54
00:00:48,920 --> 00:00:51,920
<b>Foi super interessante porque na época</b>

55
00:00:51,920 --> 00:00:57,039
<b>o projeto estava concorrendo a uma seleção</b>

56
00:00:57,039 --> 00:00:59,520
<b>e no dia seguinte ia apresentar um pitch.</b>

57
00:00:59,520 --> 00:01:02,359
<b>Então no mesmo dia quando o projeto foi aprovado</b>

58
00:01:02,359 --> 00:01:03,640
<b>nós fizemos uma matéria</b>

59
00:01:03,640 --> 00:01:05,720
<b>mandamos o link.</b>

60
00:01:05,720 --> 00:01:09,960
<b>Eu sei que o projeto apresentou essa premiação</b>

61
00:01:09,960 --> 00:01:12,840
<b>então nós fizemos parte dessa história com muito orgulho</b>

62
00:01:12,840 --> 00:01:14,439
<b>é um pedacinho pequenininho</b>

63
00:01:14,439 --> 00:01:15,840
<b>mas fazemos parte com muito orgulho</b>


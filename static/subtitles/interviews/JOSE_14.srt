1
00:00:00,519 --> 00:00:01,559
<b>Eu cheguei lá</b>

2
00:00:02,679 --> 00:00:04,519
<b>e estava aquele aparato, não é, Otávio?</b>

3
00:00:05,280 --> 00:00:08,000
<b>Era um aparato, não é?</b>

4
00:00:08,719 --> 00:00:11,039
<b>Mas, eu na minha fé democrática</b>

5
00:00:11,679 --> 00:00:14,199
<b>eu fui entrar.</b>

6
00:00:15,639 --> 00:00:17,559
<b>Quando eu fui entrar, aí me disseram:</b>

7
00:00:17,559 --> 00:00:19,800
<b>olha, tem uma lista de pessoas</b>

8
00:00:19,800 --> 00:00:21,880
<b>que podem e não podem entrar.</b>

9
00:00:23,119 --> 00:00:25,119
<b>Eu digo, então procure meu nome na lista.</b>

10
00:00:25,960 --> 00:00:29,039
<b>Aí, procuraram para lá e para cá, para lá e para cá</b>

11
00:00:29,039 --> 00:00:30,480
<b>e disse: Infelizmente</b>

12
00:00:30,480 --> 00:00:33,519
<b>o seu  nome não consta da lista de quem pode entrar.</b>

13
00:00:34,639 --> 00:00:38,960
<b>Então mandei chamar o pessoal é que estava organizando</b>

14
00:00:38,960 --> 00:00:41,679
<b>e digo: olha, gente eu estou aqui barrado</b>

15
00:00:41,679 --> 00:00:43,840
<b> eu não posso entrar.</b>

16
00:00:44,639 --> 00:00:47,800
<b>Olha, está tudo coordenado pela polícia federal</b>

17
00:00:47,800 --> 00:00:50,320
<b>vocês sabiam, não é Octávio?</b>

18
00:00:50,320 --> 00:00:53,039
<b>Tudo condenado pela polícia federal.</b>

19
00:00:53,039 --> 00:00:55,360
<b>Então nós vamos chamar a polícia federal</b>

20
00:00:55,360 --> 00:00:59,280
<b>se a polícia federal autorizar, aí o senhor entra.</b>

21
00:01:01,119 --> 00:01:03,280
<b>Aí veio um policial federal.</b>

22
00:01:05,639 --> 00:01:06,199
<b>Olha</b>

23
00:01:07,280 --> 00:01:10,440
<b>um veludo de gente.</b>

24
00:01:10,440 --> 00:01:14,440
<b>Eu já conheço todo esse pessoal</b>

25
00:01:14,440 --> 00:01:16,599
<b>desde o tempo da ditadura.</b>

26
00:01:17,400 --> 00:01:20,280
<b>Um pessoal muito bem treinado, não é?</b>

27
00:01:20,960 --> 00:01:22,880
<b>mas um veludo de gente</b>

28
00:01:22,880 --> 00:01:25,480
<b>um homem extremamente educado.</b>

29
00:01:26,119 --> 00:01:27,639
<b>E aí me disse: olha</b>

30
00:01:27,639 --> 00:01:29,599
<b>oficialmente, o senhor não pode entrar</b>

31
00:01:31,400 --> 00:01:37,000
<b>mas, falou bastante educado, bastante delicado</b>

32
00:01:37,000 --> 00:01:40,119
<b>disse: eu vou autorizar que o senhor entre.</b>

33
00:01:41,199 --> 00:01:43,400
<b>Mas, Octávio, essa você não sabe.</b>

34
00:01:43,400 --> 00:01:48,559
<b>Mas olha, é com a condição de o senhor ficar calado</b>

35
00:01:48,559 --> 00:01:50,280
<b>e não fazer estardalhaço.</b>

36
00:01:52,159 --> 00:01:53,559
<b>Eu fiquei calado, Octávio?</b>

37
00:01:54,320 --> 00:01:55,519
<b>Não!</b>

38
00:01:55,519 --> 00:01:57,400
<b>Fiz estardalhaço?</b>

39
00:01:59,159 --> 00:01:59,679
<b>Fez!</b>

40
00:01:59,679 --> 00:02:00,519
<b>É assim.</b>

41
00:02:01,559 --> 00:02:03,840
<b>Mas não era pública, a audiência?</b>

42
00:02:03,840 --> 00:02:06,639
<b>é, mas estavam querendo barrar vários moradores</b>

43
00:02:06,639 --> 00:02:08,159
<b>com essa desculpa.</b>

44
00:02:08,159 --> 00:02:09,599
<b>Algumas pessoas só?</b>

45
00:02:09,599 --> 00:02:12,079
<b>Não, eram várias pessoas!</b>

46
00:02:12,679 --> 00:02:16,000
<b>Era uma lista organizada previamente.</b>

47
00:02:16,000 --> 00:02:17,960
<b>De pessoas não benquistas?</b>

48
00:02:17,960 --> 00:02:22,440
<b>Não! Era quem estava autorizado a entrar.</b>

49
00:02:23,559 --> 00:02:26,800
<b>Aí, se não estivesse na lista, não entrava.</b>

50
00:02:27,679 --> 00:02:29,760
<b>Então o meu nome não constava da lista.</b>


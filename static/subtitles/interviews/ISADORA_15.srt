1
00:00:00,079 --> 00:00:01,719
<b>Maceió não é uma cidade justa</b>

2
00:00:01,719 --> 00:00:03,039
<b>Maceió não é uma cidade saudável</b>

3
00:00:03,039 --> 00:00:04,880
<b>Maceió não é uma cidade sustentável.</b>

4
00:00:06,239 --> 00:00:08,360
<b>Isso em todos os sentidos.</b>

5
00:00:08,360 --> 00:00:10,599
<b>Maceió não é uma cidade correta.</b>

6
00:00:11,280 --> 00:00:12,880
<b>Maceió não é uma cidade justa</b>

7
00:00:12,880 --> 00:00:16,400
<b>porque há dois pesos e duas medidas</b>

8
00:00:16,400 --> 00:00:17,920
<b>em relação a quem tem dinheiro</b>

9
00:00:17,920 --> 00:00:18,840
<b>e a quem não tem</b>

10
00:00:18,840 --> 00:00:22,000
<b>do ponto de vista do poder público municipal.</b>

11
00:00:22,000 --> 00:00:23,719
<b>Nós já vimos isso acontecer várias vezes</b>

12
00:00:23,719 --> 00:00:25,719
<b>Maceió não é uma cidade justa</b>

13
00:00:25,719 --> 00:00:28,280
<b>porque há diferença de investimento</b>

14
00:00:28,280 --> 00:00:30,760
<b>em relação às áreas de maior renda</b>

15
00:00:30,760 --> 00:00:31,800
<b>e as baixa renda</b>

16
00:00:31,800 --> 00:00:32,880
<b>e isso é fato!</b>

17
00:00:32,880 --> 00:00:34,880
<b>Então Maceió não é uma cidade justa</b>

18
00:00:34,880 --> 00:00:37,679
<b>em termos de gestão pública.</b>

19
00:00:37,679 --> 00:00:39,000
<b>Maceió não é uma cidade justa</b>

20
00:00:39,000 --> 00:00:42,280
<b>em termos de divisão de investimentos.</b>

21
00:00:42,440 --> 00:00:43,760
<b>Maceió não é uma cidade justa</b>

22
00:00:43,760 --> 00:00:46,679
<b>em termos de políticas públicas</b>

23
00:00:46,679 --> 00:00:48,599
<b>do acesso a políticas públicas.</b>

24
00:00:48,599 --> 00:00:50,360
<b>Nós tivemos a tempos atrás</b>

25
00:00:50,360 --> 00:00:52,760
<b>um surto de bicho-de-pé na região lagunar</b>

26
00:00:55,000 --> 00:00:56,800
<b>e foi preciso o Ministério Público</b>

27
00:00:56,800 --> 00:00:59,000
<b>obrigar a prefeitura</b>

28
00:00:59,000 --> 00:01:02,360
<b>a colocar os seus programas de saúde</b>

29
00:01:02,360 --> 00:01:03,679
<b>na orla da lagoa.</b>

30
00:01:03,679 --> 00:01:05,360
<b>Como se isso não fosse uma obrigação.</b>

31
00:01:06,920 --> 00:01:07,880
<b>Então não.</b>

32
00:01:07,880 --> 00:01:09,519
<b>Maceió não é uma cidade justa</b>

33
00:01:09,519 --> 00:01:11,559
<b>e Maceió não é uma cidade saudável</b>

34
00:01:11,559 --> 00:01:14,039
<b>porque se você tem pessoas</b>

35
00:01:14,039 --> 00:01:17,719
<b>que enfrentam surtos de bicho-de-pé</b>

36
00:01:18,519 --> 00:01:20,840
<b>essas pessoas não tem como estar saudáveis.</b>

37
00:01:22,440 --> 00:01:24,159
<b>Se você tem pessoas</b>

38
00:01:24,159 --> 00:01:26,840
<b>que são obrigadas a sair das suas casas</b>

39
00:01:26,840 --> 00:01:29,559
<b>sem poder participar, inclusive</b>

40
00:01:29,559 --> 00:01:31,280
<b>do que vai ser proposto para as áreas</b>

41
00:01:31,280 --> 00:01:32,440
<b>de onde elas estão saindo.</b>

42
00:01:32,440 --> 00:01:35,039
<b>Então Maceió não é uma cidade saudável.</b>

43
00:01:35,039 --> 00:01:37,400
<b>E não é uma cidade sustentável</b>

44
00:01:37,400 --> 00:01:39,960
<b>porque se você tem asfalto</b>

45
00:01:39,960 --> 00:01:42,639
<b>sendo colocado numa rua que vai dar na praia</b>

46
00:01:44,920 --> 00:01:46,880
<b>numa rua que é residencial</b>

47
00:01:46,880 --> 00:01:48,480
<b>de fluxo local</b>

48
00:01:49,280 --> 00:01:50,320
<b>e que vai dar na praia...</b>

49
00:01:50,320 --> 00:01:54,079
<b>Então se você implanta uma solução</b>

50
00:01:54,079 --> 00:01:56,239
<b>que é de impermeabilização do solo</b>

51
00:01:57,000 --> 00:01:59,840
<b>numa área com características ambientais evidentes</b>

52
00:01:59,840 --> 00:02:01,360
<b>então Maceió não é sustentável.</b>

53
00:02:01,360 --> 00:02:05,480
<b>Então a luta é por gestões mais democráticas</b>

54
00:02:05,480 --> 00:02:07,480
<b>porque nós também não somos</b>

55
00:02:07,480 --> 00:02:08,559
<b>uma cidade democrática.</b>

56
00:02:10,159 --> 00:02:12,960
<b>Então a luta é por gestões mais democráticas.</b>

57
00:02:12,960 --> 00:02:14,480
<b>Gestões democráticas</b>

58
00:02:14,480 --> 00:02:17,400
<b>tenderão a nos levar</b>

59
00:02:17,400 --> 00:02:21,599
<b>a cidades mais justas, mais saudáveis e mais sustentáveis.</b>


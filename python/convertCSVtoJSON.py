import os
import csv
import json

currentPath = os.getcwd()


def csv_to_json():
    data_dict = {}
    data_dict_en = {}

    with open(currentPath + '/python/mapa.csv', encoding='utf-8') as csv_file_handler:
        csv_reader = csv.DictReader(csv_file_handler)

        for row in csv_reader:
            if row['page'] not in data_dict:
                data_dict[row['page']] = {'links': []}

            if 'x' in row['links']:
                if row['type'] != '':
                    data_dict[row['page']]['links'].append({
                        'type': row['type'],
                        'title': row['title'],
                        'assets': []
                    })

                data_dict[row['page']]['links'][-1]['assets'].append({
                    'name': row['name'],
                    'credit': row['credit'],
                    'date': row['date'],
                    'alt': row['alt'],
                    'src': row['src'],
                    'srt': row['srt']
                })
            else:
                data_dict[row['page']][row['type']] = row['title']

            # export separate texts for EN

            if row['page'] not in data_dict_en:
                data_dict_en[row['page']] = {'links': []}

            if 'x' in row['links']:
                if row['type'] != '':
                    data_dict_en[row['page']]['links'].append({
                        'type': row['type'],
                        'title': row['title-en'],
                        'assets': []
                    })

                data_dict_en[row['page']]['links'][-1]['assets'].append({
                    'name': row['name'],
                    'credit': row['credit-en'],
                    'date': row['date'],
                    'alt': row['alt-en'],
                    'src': row['src'],
                    'srt': row['srt']
                })
            else:
                data_dict_en[row['page']][row['type']] = row['title-en']

    for chapter in data_dict:
        with open(currentPath + '/src/data/pt/' + chapter + '.json', 'w', encoding='utf-8') as json_file_handler:
            # Step 4
            json_file_handler.write(json.dumps(data_dict[chapter], indent=4))

    for chapter in data_dict_en:
        with open(currentPath + '/src/data/en/' + chapter + '.json', 'w', encoding='utf-8') as json_file_handler:
            # Step 4
            json_file_handler.write(json.dumps(
                data_dict_en[chapter], indent=4))

    print('Done')


csv_to_json()

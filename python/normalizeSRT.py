import os
import pysrt
from pathlib import Path

currentPath = os.getcwd()

for child in Path(currentPath + '/static/subtitles/interviews/').rglob('*.srt'):
    if child.is_file():
        subs = pysrt.open(
            child)
        for sub in subs:
            if sub.start.hours == 1:
                subs.shift(hours=-1)
        subs.save(child, encoding='utf-8')

for child in Path(currentPath + '/static/subtitles/interviews/en/').rglob('*.srt'):
    if child.is_file():
        subs = pysrt.open(
            child)
        for sub in subs:
            if sub.start.hours == 1:
                subs.shift(hours=-1)
        subs.save(child, encoding='utf-8')
